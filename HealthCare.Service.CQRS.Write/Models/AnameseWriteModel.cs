﻿using System;

namespace HealthCare.Infra.CQRS.Write.Models
{
    public class AnameseWriteModel
    {
        public Guid Id { get; set; }
        public Guid Ciad2Id { get; set; }
        public string QueixaPrincipal { get; set; }
        public string Descricao { get; set; }
        public Guid AtendimentoId { get; set; }
    }
}

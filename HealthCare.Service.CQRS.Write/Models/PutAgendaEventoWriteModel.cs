﻿using System;

namespace HealthCare.Service.CQRS.Write.Models
{
    public class PutAgendaEventoWriteModel
    {
        public Guid Id { get; set; }
        public Guid AgendaId { get; set; }
        public Guid PacienteId { get; set; }
        public Guid ServicoId { get; set; }
    }
}
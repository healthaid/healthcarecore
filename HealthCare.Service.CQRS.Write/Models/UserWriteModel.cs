﻿using System;

namespace HealthCare.Infra.CQRS.Write.Models
{
    public class UserWriteModel
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public string ProfissionalId { get; set; }
    
    }
}

﻿using System;

namespace HealthCare.Service.CQRS.Write.Models
{
    public class EtniaWriteModel
    {
        public Guid Id { get; set; }
        public string Descricao { get; set; }
        public bool Ativo { get; set; }
    }
}
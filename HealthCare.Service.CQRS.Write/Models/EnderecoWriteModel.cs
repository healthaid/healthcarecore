﻿using System;

namespace HealthCare.Service.CQRS.Write.Models
{
    /// <summary>
    /// Modelo de insert para o endereço
    /// </summary>
    public class EnderecoWriteModel
    {
        /// <summary>
        /// Cep do endereço
        /// </summary>
        public string Cep { get; set; }

        /// <summary>
        /// Nome do logradouro
        /// </summary>
        public string Logradouro { get; set; }

        /// <summary>
        /// Numero da casa
        /// </summary>
        public string Numero { get; set; }

        /// <summary>
        /// Complemento do endereço
        /// </summary>
        public string Complemento { get; set; }

        /// <summary>
        /// Bairro
        /// </summary>
        public string Bairro { get; set; }

        /// <summary>
        /// Municipio
        /// </summary>
        public string Cidade { get; set; }

        /// <summary>
        /// Estado
        /// </summary>
        public string Uf { get; set; }

        /// <summary>
        /// Codigo Municipio
        /// </summary>
        public string CodigoIbgeCidade { get; set; }

        public string CodigoLogradouro { get; set; }

    }
}
﻿using System;

namespace HealthCare.Infra.CQRS.Write.Models
{
    public class TemplatesAtestadoWriteModel
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public string Conteudo { get; set; }
        public Guid AtestadoId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace HealthCare.Infra.CQRS.Write.Models
{
    public class ImagemAtendimentoWriteModel
    {
        public Guid Id { get; set; }

        public string Observacao { get; set; }

        public List<Byte[]> Imagens { get; set; }

        public Guid AtendimentoId { get; set; }
    }
}

﻿using System;

namespace HealthCare.Service.CQRS.Write.Models
{
    public class CNSWriteModel
    {
        public string Numero { get; set; }
        public bool Principal { get; set; }
        public bool Ativo { get; set; }
        public Guid Id { get; set; }
        public Guid PacienteId { get; set; }
    }
}
﻿using System;

namespace HealthCare.Infra.CQRS.Write.Models
{
    public class DocumentoWriteModel
    {
        public Guid Id { get; set; }
        public string TipoDocumento { get; set; }
        public string Valor { get; set; }
        public Guid PacienteId { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;

namespace HealthCare.Infra.CQRS.Write.Models
{
    public class ResultadoExameWriteModel
    {
        public Guid Id { get; set; }
        public Guid AtendimentoId { get; set; }
        public Guid ExameId { get; set; }
        public Guid PacienteId { get; set; }
        public List<ResultadoElementoExameWriteModel> Elementos { get; set; }
    }
}
﻿using System;

namespace HealthCare.Service.CQRS.Write.Models
{
    public class ProfissaoWriteModel
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public bool Ativo { get; set; }
    }
}
﻿using System;

namespace HealthCare.Infra.CQRS.Write.Models
{
    public class ExameWriteModel
    {
        public Guid Id { get; set; }
        public Guid AtendimentoId { get; set; }
    }
}

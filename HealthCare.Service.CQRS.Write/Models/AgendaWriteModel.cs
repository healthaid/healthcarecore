﻿using System;
using System.Collections.Generic;

namespace HealthCare.Service.CQRS.Write.Models
{
    public class AgendaWriteModel
    {
        public string Nome;
        public DateTime DataInicio;
        public DateTime DataFim;
        public string HoraInicio;
        public string HoraFim;
        public Guid ProfissionalId;
        public string HoraInicioIntervalo;
        public string HoraFimIntervalo;
        public List<DayOfWeek> DiasDaSemana;
        public Guid UnidadeId;
    }
}
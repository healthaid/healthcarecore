﻿using System;

namespace HealthCare.Service.CQRS.Write.Models
{
    public class ConvenioWriteModel
    {
        public Guid Id { get; set; }
        public string NumeroCarteira { get; set; }
        public string Acomocadao { get; set; }
        public DateTime Validade { get; set; }
        public Guid PacienteId { get; set; }
        public Guid PlanoId { get; set; }
        public string PacienteNome { get; set; }
        public string PlanoNome { get; set; }
    }
}
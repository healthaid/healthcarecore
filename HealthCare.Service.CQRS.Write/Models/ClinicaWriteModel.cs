﻿using System;
namespace HealthCare.Infra.CQRS.Write.Models
{
    public class ClinicaWriteModel
    {
        public Guid Id { get; set; }

        public string Nome { get; set; }
    }
}

﻿using System;
using System.ComponentModel.DataAnnotations;

namespace HealthCare.Service.CQRS.Write.Models
{
    public class ServicoWriteModel
    {
        public Guid Id;
        [Required(ErrorMessage = "Descrição é obrigatório")]
        public string Descricao;
        public bool Ativo;
    }
}
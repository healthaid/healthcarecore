﻿using System;

namespace HealthCare.Infra.CQRS.Write.Models
{
    public class ResultadoElementoExameWriteModel
    {
        public Guid Id { get; set; }
        public string Valor { get; set; }
    }
}
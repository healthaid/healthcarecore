﻿using System;

namespace HealthCare.Service.CQRS.Write.Models
{
    public class UFWriteModel
    {
        public Guid Id { get; set; }
        public string Descricao { get; set; }
    }
}

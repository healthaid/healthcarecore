﻿using System;
namespace HealthCare.Infra.CQRS.Write.Models
{
    public class AtestadoWriteModel
    {
        public Guid Id { get; set; }
        public string Descricao { get; set; }
        public Guid AtendimentoId { get; set; }
    }
}

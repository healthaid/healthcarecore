﻿using System;
using System.Collections.Generic;

namespace HealthCare.Infra.CQRS.Write.Models
{
    public class HipoteseDiagnosticaWriteModel 
    {
        public Guid Id { get; set; }

        public List<string> Cids { get; set; }

        public Guid AtendimentoId { get; set; }

        public string Observacao { get; set; }
    }
}

﻿namespace HealthCare.Service.CQRS.Write.Models
{
    public class AuthenticateViewWriteModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}

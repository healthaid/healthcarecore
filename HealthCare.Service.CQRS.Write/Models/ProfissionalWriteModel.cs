﻿using System;

namespace HealthCare.Service.CQRS.Write.Models
{
    public class ProfissionalWriteModel
    {
        public Guid Id { get; set; }
        public Guid ProfissionalId { get; set; }
        public string Nome { get; set; }
        public string CPF { get; set; }
        public string CNS { get; set; }
        public string Sexo { get; set; }
        public string NomePai { get; set; }
        public string NomeMae { get; set; }
        public string Nascimento { get; set; }
        public string CBO { get; set; }
        public string Naturalidade { get; set; }
        public string Nacionalidade { get; set; }
        public Guid UnidadeId { get; set; }
        public Guid UsuarioId { get; set; }
        public Guid[] Servicos { get; set; }
        public Guid[] Roles { get; set; }
        public string CRM { get; set; }
    }
}
﻿
using System;
namespace HealthCare.Infra.CQRS.Write.Models
{
    public class AtendimentoWriteModel
    {
        public Guid Id { get; set; }
        public Guid PacienteId { get; set; }
        public Guid AgendaId { get; set; }
    }
}

﻿using System;
namespace HealthCare.Infra.CQRS.Write.Models
{
    public class LaudoWriteModel
    {
        public Guid Id { get; set; }
        public Guid ExameId { get; set; }
        public Guid UsuarioId { get; set; }
    }
}

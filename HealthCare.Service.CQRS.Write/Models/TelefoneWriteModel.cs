﻿using System;

namespace HealthCare.Infra.CQRS.Write.Models
{
    public class TelefoneWriteModel
    {
        public Guid Id { get; set; }

        public string Numero { get; set; }
        public Guid TipoTelefoneId { get; set; }
        public Guid PacienteId { get; set; }
    }
}

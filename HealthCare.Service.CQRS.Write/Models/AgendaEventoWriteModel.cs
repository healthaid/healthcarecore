﻿using HealthCare.Domain.Entities.DbClasses.Cadastro;
using System;

namespace HealthCare.Service.CQRS.Write.Models
{
    public class AgendaEventoWriteModel
    {
        //public Agenda Agenda { get; set; }
        public Paciente Paciente { get; set; }
        public Servico Servico { get; set; }
        public string AgendaNome { get; set; }
        public DateTime HoraInicio { get; set; }
        public DateTime HoraFim { get; set; }

    }

    public class AgendaEventoTesteModel
    {
        public Guid id { get; set; }
        public string title { get; set; }
        public DateTime start { get; set; }
        public DateTime end { get; set; }
        public Guid PacienteId { get; set; }
        public Guid ServicoId { get; set; }
        public Guid ProfissionalId { get; set; }
        public string profissionalNome { get; set; }
        public string pacienteNome { get; set; }
        public bool allDay { get; set; }
        public string color { get; set; }
        public bool stick { get; set; }
    }
}
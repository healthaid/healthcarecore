﻿using HealthCare.Infra.CQRS.Messaging.Handling;
using System;

namespace HealthCare.Infra.CQRS.Write.Commands
{
    public class SaveExameAtendimentoCommand : ICommand
    {
        public System.Guid Id
        {
            get 
            {
                return Guid.NewGuid();
            }
        }

        public Guid ExameAtendimentoId { get; set; }
        public Guid AtendimentoId { get; set; }
        public string ExameDescritivo { get; set; }
        public string Procedimento { get; set; }
        public string CodigoProcedimento { get; set; }
        public string ObservacaoMedico { get; set; }
        public DateTime DataSolicitacao { get; set; }
        public DateTime? DataExecucao { get; set; }
        public string ObservacaoExecutor { get; set; }
        public Guid SolicitanteId { get; set; }
        public Guid ExecutorId { get; set; }
        public bool Realizado { get; set; }
        public string DefinicaoExame { get; set; }
        /// <summary>
        /// Id do Usuário que disparou o command
        /// </summary>
        public Guid CommandUsuarioId { get; set; }

    }
}

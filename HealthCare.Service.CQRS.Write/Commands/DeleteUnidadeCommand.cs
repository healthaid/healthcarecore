﻿using HealthCare.Infra.CQRS.Messaging.Handling;
using System;

namespace HealthCare.Infra.CQRS.Write.Commands
{
    public class DeleteUnidadeCommand : ICommand
    {
        public Guid Id
        {
            get
            {
                return Guid.NewGuid();
            }
        }

        public Guid UnidadeId { get; set; }
        /// <summary>
        /// Id do Usuário que disparou o command
        /// </summary>
        public Guid CommandUsuarioId { get; set; }
    }
}

﻿
using AutoMapper;
using HealtCare.Infra.Common.Validations;
using HealthCare.Domain.Entities.DbClasses.Atendimento;
using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.Data.Context;
using HealthCare.Service.CQRS.Write.Commands;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace HealthCare.Infra.CQRS.Write.Commands.CommandsHandlers
{
    public class ServicoSocialHandler : ICommandHandler<CreateServicoSocialCommand>
    {
        private HealthCareDbContext repository;
        IMapper mapper;        
        public ServicoSocialHandler(HealthCareDbContext context, IMapper mapper)
        {
            this.repository = context;
            this.mapper = mapper;
        }

        public void Dispose()
        {
            repository?.Dispose();
        }

        public void Handle(CreateServicoSocialCommand command)
        {
        }

        public object HandleWithReturn(CreateServicoSocialCommand command)
        {
            Validation validation = new Validation();
            try
            {
                ServicoSocial ServicoSocial = new ServicoSocial();
                LoggerFactory factory = new LoggerFactory();
                if (command.ServicoSocialId == Guid.Empty)
                {
                    if (command.AcolhimentoId == Guid.Empty)
                    {
                        /// Cria o acolhimento 
                        /// ---------------------------------------------------------------
                        SaveAcolhimentoCommand acolhimento = new SaveAcolhimentoCommand();
                        acolhimento.SavePacienteCommand = new SavePacienteCommand();
                        acolhimento.SavePacienteCommand.PacienteId = command.PacienteId;
                        acolhimento.ProfissionalId = command.ProfissionalId;
                        AcolhimentoHandler handler = new AcolhimentoHandler(repository, mapper, factory.CreateLogger<AcolhimentoHandler>());
                        var retorno = handler.HandleWithReturn(acolhimento);

                        if (retorno.GetType() == typeof(Validation))
                            return (Validation)retorno;
                        else
                            ServicoSocial.Acolhimento = repository.Query<Acolhimento>().FirstOrDefault(x => x.Id == ((Guid)retorno)); // .fir repositoryAcolhimento.Get((Guid)retorno);
                    }
                    else
                        ServicoSocial.Acolhimento = repository.Query<Acolhimento>().FirstOrDefault(x => x.Id == command.AcolhimentoId);
                    ServicoSocial.Profissional = repository.Query<Profissional>().FirstOrDefault(x => x.Id == command.ProfissionalId);

                    ServicoSocial.InicioAtendimento = command.InicioAtendimento; //DateTime.Now;
                    ServicoSocial.FimAtendimento = null;


                    repository.Entry(ServicoSocial).State = EntityState.Added;
                    repository.ServicoSocial.Add(ServicoSocial);
                    repository.SaveChanges();

                    /// ----------------------------------------------------------------------
                    /// Adiciona o Procedimento defatult 
                    /// ----------------------------------------------------------------------
                    AddProcedimentoAtendimentoCommand procedimento = new AddProcedimentoAtendimentoCommand();
                    procedimento.CodigoProcedimento = "0301010048"; /// CONSULTA DE PROFISSIONAIS DE NIVEL SUPERIOR NA ATENÇÃO ESPECIALIZADA (EXCETO MÉDICO)
                    procedimento.AtendimentoId = ServicoSocial.Id;
                    AtendimentoHandler handlerAddProcedimento = new AtendimentoHandler(this.repository, factory.CreateLogger<AtendimentoHandler>());
                    handlerAddProcedimento.HandleWithReturn(procedimento);
                    
                }
                else
                {
                    ServicoSocial = repository.Query<ServicoSocial>().Include(x => x.Acolhimento).FirstOrDefault(x => x.Id == command.ServicoSocialId);
                    ServicoSocial.Profissional = repository.Query<Profissional>().FirstOrDefault(x => x.Id == command.ProfissionalId);

                    ServicoSocial.Intervencao = command.Intervencao;
                    ServicoSocial.Historico = command.Historico;

                    repository.Entry(ServicoSocial).State = EntityState.Modified;
                    repository.SaveChanges();
                }

                return ServicoSocial.Acolhimento.Id;
            }
            catch (Exception ex)
            {
                if (ex is GeneralErrors)
                {
                    foreach (var item in ((GeneralErrors)ex).Errors)
                    {
                        validation.AddValidationMessage(new ValidationMessage(item));
                    }
                }
                validation.AddValidationMessage(new ValidationMessage(ex.Message));
                return validation;
            }
        }
    }
}

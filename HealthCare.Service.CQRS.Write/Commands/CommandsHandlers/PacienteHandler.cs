﻿using AutoMapper;
using HealtCare.Infra.Common.Validations;
using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.CQRS.Write.Commands;
using HealthCare.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace HealthCare.Service.CQRS.Write.Commands.CommandsHandlers
{
    public class PacienteHandler :
         ICommandHandler<CreatePacienteCommand>,
        ICommandHandler<SavePacienteCommand>,
        ICommandHandler<DeletePacienteCommand>
    {
        IMapper mapper;
        HealthCareDbContext repository;
        public PacienteHandler(HealthCareDbContext contextFactory, IMapper mapper)
        {
            this.repository = contextFactory;
            this.mapper = mapper;
        }

        public void Handle(DeletePacienteCommand command)
        {
            //throw new NotImplementedException();
        }

        public void Handle(CreatePacienteCommand command)
        {
            //throw new NotImplementedException();
        }

        public void Handle(SavePacienteCommand command)
        {
            //throw new NotImplementedException();
        }

        public object HandleWithReturn(DeletePacienteCommand command)
        {
            throw new NotImplementedException();
            //Validation validation = new Validation();
            //Paciente paciente = this.repository.Get(command.PacienteId);
            //paciente.IsDeleted = true;
            //paciente.Ativo = false;
            //try
            //{
            //    //this.repository.Update(paciente);
            //    return validation;
            //}
            //catch (Exception ex)
            //{
            //    if (ex is GeneralErrors)
            //    {
            //        foreach (var item in ((GeneralErrors)ex).Errors)
            //        {
            //            validation.AddValidationMessage(new ValidationMessage(item));
            //        }
            //    }
            //    validation.AddValidationMessage(new ValidationMessage(ex.Message));
            //    return validation;
            //}
        }

        public object HandleWithReturn(SavePacienteCommand command)
        {
            Validation v = new Validation();
            Paciente p = mapper.Map<Paciente>(command);
            try
            {
                Validation validation = ValidaPaciente(command, repository);
                if (validation.IsValid)
                {
                    if (command.PacienteId == Guid.Empty)
                    {
                        p.Unidade = repository.Unidade.Find(command.UnidadeId);
                        p.Obito = false;
                        p.Ativo = true;
                        p.IsDeleted = false;
                        p.DataNascimento = null;

                        if (p.PendenteCadastro == false)
                            p.PendenteCadastro = false;
                        else
                            p.PendenteCadastro = true;

                        p.Naturalidade = repository.Cidade.Find(command.NaturalidadePaciente);
                        p.CodigoPaciente = this.GeraCodigoPaciente(repository);//Membership.GeneratePassword(7, 0);
                        repository.Entry(p).State = EntityState.Added;
                        repository.Paciente.Add(p);
                        repository.SaveChanges();
                    }
                    else
                    {
                        Paciente paciente = repository.Query<Paciente>().Include("Endereco").FirstOrDefault(x => x.Id == command.PacienteId);
                        paciente.Cns = p.Cns;
                        paciente.CPF = p.CPF;
                        paciente.DataNascimento = p.DataNascimento;
                        paciente.Email = p.Email;

                        if (paciente.Endereco == null)
                            paciente.Endereco = new Endereco();

                        paciente.Endereco.Bairro = command.Endereco.Bairro;
                        paciente.Endereco.CEP = command.Endereco.Cep;
                        paciente.Endereco.Cidade = command.Endereco.Cidade;
                        paciente.Endereco.CodigoIbgeCidade = command.Endereco.CodigoIbgeCidade;
                        paciente.Endereco.Complemento = command.Endereco.Complemento;
                        paciente.Endereco.Logradouro = command.Endereco.Logradouro;
                        paciente.Endereco.Numero = command.Endereco.Numero;
                        paciente.Endereco.UF = command.Endereco.Uf;
                        paciente.Endereco.CodigoLogradouro = command.Endereco.CodigoLogradouro;

                        paciente.EstadoCivil = p.EstadoCivil;
                        paciente.Etinia = p.Etinia;
                        paciente.Nome = p.Nome;
                        paciente.NomeMae = p.NomeMae;
                        paciente.RG = p.RG;
                        paciente.Sexo = p.Sexo;
                        paciente.TelefoneCelular = p.TelefoneCelular;
                        paciente.TelefoneFixo = p.TelefoneFixo;
                        paciente.TelefoneTrabalho = p.TelefoneTrabalho;
                        paciente.Unidade = repository.Unidade.Find(command.UnidadeId);
                        paciente.Nacionalidade = command.Nacionalidade;
                        paciente.Etinia = command.Etinia;
                        paciente.Naturalidade = repository.Cidade.Find(command.NaturalidadePaciente);
                        paciente.Profissao = command.Profissao;
                        paciente.PendenteCadastro = command.PendenteCadastro;
                        paciente.Raca = command.Raca;
                        repository.Entry(paciente).State = EntityState.Modified;
                        repository.SaveChanges();
                    }
                    return p.Id;
                }
                return validation;
            }
            catch (Exception ex)
            {
                if (ex is DatabaseValidationErrors)
                {
                    var dbEx = (DatabaseValidationErrors)ex;
                    foreach (var error in dbEx.Errors)
                    {
                        v.AddValidationMessage(new ValidationMessage(error));
                    }

                }
                if (ex is GeneralErrors)
                {
                    foreach (var item in ((GeneralErrors)ex).Errors)
                    {
                        v.AddValidationMessage(new ValidationMessage(item));
                    }
                }
                v.AddValidationMessage(new ValidationMessage(ex.Message));
                return v;
            }
        }

        private string GeraCodigoPaciente(HealthCareDbContext context)

        {
            int? ultimoCodigo;
            if (context.Paciente.Any() == true)
                ultimoCodigo = context.Paciente.ToList().Max(x => Convert.ToInt32(x.CodigoPaciente));
            else
                ultimoCodigo = 0;
            if (ultimoCodigo != null)
                ultimoCodigo = ultimoCodigo + 1;
            else
                ultimoCodigo = 1;

            string codigoPaciente = ultimoCodigo.ToString();
            for (int i = ultimoCodigo.ToString().Length; i < 14; i++)
                codigoPaciente = "0" + codigoPaciente;

            return codigoPaciente;
        }

        public object HandleWithReturn(CreatePacienteCommand command)
        {
            //throw new NotImplementedException();
            throw null;
        }

        private Validation ValidaPaciente(SavePacienteCommand command, HealthCareDbContext context)
        {
            Validation v = new Validation();

            if (string.IsNullOrWhiteSpace(command.Nome))
                v.AddValidationMessage(new ValidationMessage("Nome é obrigatório!"));
            if (command.UnidadeId == Guid.Empty)
                v.AddValidationMessage(new ValidationMessage("Unidade é obrigatória!"));

            //if (string.IsNullOrWhiteSpace(command.CNS) && command.VindoAcolhimento == false)
            //    v.AddValidationMessage(new ValidationMessage("O CNS do paciente é obrigatório!"));

            if (!command.DataNascimento.HasValue && command.VindoAcolhimento == false)
                v.AddValidationMessage(new ValidationMessage("A data de nascimento do paciente é obrigatória!"));

            if (string.IsNullOrWhiteSpace(command.Sexo) && command.VindoAcolhimento == false)
                v.AddValidationMessage(new ValidationMessage("O sexo do paciente é obrigatório!"));

            //if (command.Endereco != null && string.IsNullOrWhiteSpace(command.Endereco.CodigoIbgeCidade) && command.VindoAcolhimento == false)
            //    v.AddValidationMessage(new ValidationMessage("Preencha o endereço do paciente procurando pelo CEP para fins de faturamento!"));


            Int64 cpf = 0;
            if (!string.IsNullOrWhiteSpace(command.CPF))
            {
                command.CPF = command.CPF.Replace(".", "");
                command.CPF = command.CPF.Replace("-", "");

                if (!Int64.TryParse(command.CPF, out cpf))
                    v.AddValidationMessage(new ValidationMessage("CPF inválido!"));
                else if (!ValidaCPF(command.CPF))
                    v.AddValidationMessage(new ValidationMessage("CPF inválido!"));
                else if (command.PacienteId == Guid.Empty && context.Paciente.Any(x => x.CPF == command.CPF) == true)
                    v.AddValidationMessage(new ValidationMessage("CPF já utilizado!"));
            }

            if (!string.IsNullOrWhiteSpace(command.CNS) && command.VindoAcolhimento == false)
            {
                if (!ChecarCNS(command.CNS))
                    v.AddValidationMessage(new ValidationMessage("CNS Inválido"));
                else
                {
                    if (command.PacienteId == Guid.Empty && context.Paciente.Any(x => x.Cns == command.CNS) == true)
                        v.AddValidationMessage(new ValidationMessage("CNS já cadastrado"));
                }
            }

            return v;
        }

        private bool ValidaCPF(string cpf)
        {
            int[] multiplicador1 = new int[9] { 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplicador2 = new int[10] { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            string tempCpf;
            string digito;
            int soma;
            int resto;
            cpf = cpf.Trim();
            cpf = cpf.Replace(".", "").Replace("-", "");
            if (cpf.Length != 11)
                return false;
            tempCpf = cpf.Substring(0, 9);
            soma = 0;

            for (int i = 0; i < 9; i++)
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador1[i];
            resto = soma % 11;
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = resto.ToString();
            tempCpf = tempCpf + digito;
            soma = 0;
            for (int i = 0; i < 10; i++)
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador2[i];
            resto = soma % 11;
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = digito + resto.ToString();
            return cpf.EndsWith(digito);
        }

        private bool ChecarCNS(string cns)
        {
            bool result = false;
            cns = cns.Trim();
            if ((cns.Substring(0, 1) == "8") || (cns.Substring(0, 1) == "9"))
            {
                result = checaNumeroProvisorio(cns);
            }
            else
            {
                result = checaNumeroDefinitivo(cns);
            }
            return result;
        }

        private bool checaNumeroDefinitivo(string cns)
        {
            bool result = false;
            try
            {
                cns = cns.Trim();

                if (cns.Trim().Length == 15)
                {
                    float resto, soma;
                    soma = ((Convert.ToInt64(cns.Substring(0, 1))) * 15) +
                            ((Convert.ToInt64(cns.Substring(1, 1))) * 14) +
                            ((Convert.ToInt64(cns.Substring(2, 1))) * 13) +
                            ((Convert.ToInt64(cns.Substring(3, 1))) * 12) +
                            ((Convert.ToInt64(cns.Substring(4, 1))) * 11) +
                            ((Convert.ToInt64(cns.Substring(5, 1))) * 10) +
                            ((Convert.ToInt64(cns.Substring(6, 1))) * 9) +
                            ((Convert.ToInt64(cns.Substring(7, 1))) * 8) +
                            ((Convert.ToInt64(cns.Substring(8, 1))) * 7) +
                            ((Convert.ToInt64(cns.Substring(9, 1))) * 6) +
                            ((Convert.ToInt64(cns.Substring(10, 1))) * 5) +
                            ((Convert.ToInt64(cns.Substring(11, 1))) * 4) +
                            ((Convert.ToInt64(cns.Substring(12, 1))) * 3) +
                            ((Convert.ToInt64(cns.Substring(13, 1))) * 2) +
                            ((Convert.ToInt64(cns.Substring(14, 1))) * 1);

                    resto = soma % 11;

                    result = (resto == 0);
                }

            }
            catch (Exception)
            {
                result = false;
            }

            return result;
        }

        //private bool checaNumeroProvisorio(string cns)
        //{
        //    bool result = false;

        //    try
        //    {
        //        if (cns.Trim().Length == 15)
        //        {

        //            float resto, soma, dv;

        //            string pis = string.Empty;
        //            string resultado = string.Empty;

        //            pis = cns.Substring(0, 11);

        //            soma = ((Convert.ToInt64(pis.Substring(0, 1))) * 15) +
        //                    ((Convert.ToInt64(pis.Substring(1, 1))) * 14) +
        //                    ((Convert.ToInt64(pis.Substring(2, 1))) * 13) +
        //                    ((Convert.ToInt64(pis.Substring(3, 1))) * 12) +
        //                    ((Convert.ToInt64(pis.Substring(4, 1))) * 11) +
        //                    ((Convert.ToInt64(pis.Substring(5, 1))) * 10) +
        //                    ((Convert.ToInt64(pis.Substring(6, 1))) * 9) +
        //                    ((Convert.ToInt64(pis.Substring(7, 1))) * 8) +
        //                    ((Convert.ToInt64(pis.Substring(8, 1))) * 7) +
        //                    ((Convert.ToInt64(pis.Substring(9, 1))) * 6) +
        //                    ((Convert.ToInt64(pis.Substring(10, 1))) * 5);
        //            resto = soma % 11;
        //            dv = 11 - resto;

        //            if (dv == 11)
        //            {
        //                dv = 0;
        //            }
        //            if (dv == 10)
        //            {
        //                soma = ((Convert.ToInt64(pis.Substring(0, 1))) * 15) +
        //                        ((Convert.ToInt64(pis.Substring(1, 1))) * 14) +
        //                        ((Convert.ToInt64(pis.Substring(2, 1))) * 13) +
        //                        ((Convert.ToInt64(pis.Substring(3, 1))) * 12) +
        //                        ((Convert.ToInt64(pis.Substring(4, 1))) * 11) +
        //                        ((Convert.ToInt64(pis.Substring(5, 1))) * 10) +
        //                        ((Convert.ToInt64(pis.Substring(6, 1))) * 9) +
        //                        ((Convert.ToInt64(pis.Substring(7, 1))) * 8) +
        //                        ((Convert.ToInt64(pis.Substring(8, 1))) * 7) +
        //                        ((Convert.ToInt64(pis.Substring(9, 1))) * 6) +
        //                        ((Convert.ToInt64(pis.Substring(10, 1))) * 5) + 2;

        //                resto = soma % 11;
        //                dv = 11 - resto;
        //                resultado = pis + "001" + Convert.ToString(Convert.ToInt16(dv)).Trim();
        //            }
        //            else
        //            {
        //                resultado = pis + "000" + Convert.ToString(Convert.ToInt16(dv)).Trim();
        //            }


        //            result = cns.Equals(resultado);

        //        }
        //    }
        //    catch (Exception)
        //    {
        //        result = false;
        //    }

        //    return result;
        //}

        // Converted from UnityScript to C# at http://www.M2H.nl/files/js_to_c.php - by Mike Hergaarden
        // Do test the code! You usually need to change a few small bits.

        bool checaNumeroProvisorio(string cns)
        {
            bool result = false;

            try
            {
                cns = cns.Trim();

                if (cns.Trim().Length == 15)
                {



                    float resto, soma;

                    soma = ((Convert.ToInt64(cns.Substring(0, 1))) * 15) +
                            ((Convert.ToInt64(cns.Substring(1, 1))) * 14) +
                            ((Convert.ToInt64(cns.Substring(2, 1))) * 13) +
                            ((Convert.ToInt64(cns.Substring(3, 1))) * 12) +
                            ((Convert.ToInt64(cns.Substring(4, 1))) * 11) +
                            ((Convert.ToInt64(cns.Substring(5, 1))) * 10) +
                            ((Convert.ToInt64(cns.Substring(6, 1))) * 9) +
                            ((Convert.ToInt64(cns.Substring(7, 1))) * 8) +
                            ((Convert.ToInt64(cns.Substring(8, 1))) * 7) +
                            ((Convert.ToInt64(cns.Substring(9, 1))) * 6) +
                            ((Convert.ToInt64(cns.Substring(10, 1))) * 5) +
                            ((Convert.ToInt64(cns.Substring(11, 1))) * 4) +
                            ((Convert.ToInt64(cns.Substring(12, 1))) * 3) +
                            ((Convert.ToInt64(cns.Substring(13, 1))) * 2) +
                            ((Convert.ToInt64(cns.Substring(14, 1))) * 1);

                    resto = soma % 11;

                    result = (resto == 0);
                }

            }
            catch (Exception)
            {
                result = false;
            }


            return result;
        }

        public void Dispose()
        {
            repository?.Dispose();
        }

        ///
        //bool checaNumeroProvisorio(string cns)
        //{

        //    string pis;

        //    Int64 resto;

        //    Int64 dv;

        //    Int64 soma;

        //    Int64 resultado;

        //    Int64 result;

        //    result = 0;

        //    pis = cns.Substring(0, 15);

        //    if (pis == "")
        //    {

        //        return false;

        //    }

        //    if ((cns.Substring(0, 1) != "7") && (cns.Substring(0, 1) != "8") && (cns.Substring(0, 1) != "9"))
        //    {

        //        //alert("Atenção! Número Provisório inválido!");

        //        return false;

        //    }

        //    soma = ((Int64.Parse(pis.Substring(0))) * 15)

        //    + ((Int64.Parse(pis.Substring(1))) * 14)

        //    + ((Int64.Parse(pis.Substring(2))) * 13)

        //    + ((Int64.Parse(pis.Substring(3))) * 12)

        //    + ((Int64.Parse(pis.Substring(4))) * 11)

        //    + ((Int64.Parse(pis.Substring(5))) * 10)

        //    + ((Int64.Parse(pis.Substring(6))) * 9)

        //    + ((Int64.Parse(pis.Substring(7))) * 8)

        //    + ((Int64.Parse(pis.Substring(8))) * 7)

        //    + ((Int64.Parse(pis.Substring(9))) * 6)

        //    + ((Int64.Parse(pis.Substring(10))) * 5)

        //    + ((Int64.Parse(pis.Substring(11))) * 4)

        //    + ((Int64.Parse(pis.Substring(12))) * 3)

        //    + ((Int64.Parse(pis.Substring(13))) * 2)

        //    + ((Int64.Parse(pis.Substring(14))) * 1);

        //    resto = soma % 11;

        //    if (resto == 0)
        //    {

        //        return true;

        //    }

        //    else
        //    {

        //        //alert("Atenção! Número Provisório inválido!");

        //        return false;

        //    }

        //}

    }
}
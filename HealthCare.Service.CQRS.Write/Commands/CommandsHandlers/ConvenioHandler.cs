﻿using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;

namespace HealthCare.Infra.CQRS.Write.Commands.CommandsHandlers
{
    public class ConvenioHandler : ICommandHandler<CreateConvenioCommand>, ICommandHandler<UpdateConvenioCommand>
    {
        private readonly HealthCareDbContext repository;

        public ConvenioHandler(HealthCareDbContext contextFactory)
        {
            repository = contextFactory;
        }

        public void Dispose()
        {
            repository?.Dispose();
        }

        public void Handle(CreateConvenioCommand command)
        {
            try
            {
                using (var transaction = repository.Database.BeginTransaction())
                {
                    //Convenio Convenio = new Convenio();
                    //Convenio.Id = Guid.NewGuid();
                    //Convenio.Acomocadao = command.Acomocadao;
                    //Convenio.NumeroCarteira = command.NumeroCarteira;
                    //Convenio.Paciente = repositoryPaciente.Get(command.PacienteId);
                    //Convenio.Validade = command.Validade;
                    //Convenio.Plano = repositoryPlano.Get(command.PlanoId);

                    //Convenio.RowVersion = BitConverter.GetBytes(DateTime.Now.Date.Ticks);
                    //repository.Entry(Convenio).State = EntityState.Added;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Handle(UpdateConvenioCommand command)
        {
            try
            {
                using (var transaction = repository.Database.BeginTransaction())
                {
                    //Convenio Convenio = repository.con(command.Id);
                    //Convenio.Acomocadao = command.Acomocadao;
                    //Convenio.NumeroCarteira = command.NumeroCarteira;
                    //Convenio.Paciente = repositoryPaciente.Get(command.PacienteId);
                    //Convenio.Validade = command.Validade;
                    //Convenio.Plano = repositoryPlano.Get(command.PlanoId);

                    //Convenio.RowVersion = BitConverter.GetBytes(DateTime.Now.Date.Ticks);

                    //this.repository.Update(Convenio);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public object HandleWithReturn(UpdateConvenioCommand command)
        {
            throw new NotImplementedException();
        }

        public object HandleWithReturn(CreateConvenioCommand command)
        {
            throw new NotImplementedException();
        }
    }
}

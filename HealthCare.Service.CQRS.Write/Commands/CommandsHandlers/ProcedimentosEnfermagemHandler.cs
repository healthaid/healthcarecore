﻿using AutoMapper;
using HealtCare.Infra.Common.Validations;
using HealthCare.Domain.Entities.DbClasses.Atendimento;
using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace HealthCare.Infra.CQRS.Write.Commands.CommandsHandlers
{
    public class ProcedimentosEnfermagemHandler : ICommandHandler<SaveProcedimentosEnfermagemCommand>, ICommandHandler<DeleteProcedimentosEnfermagemCommand>
    {
        HealthCareDbContext repository;
        private IMapper mapper;
        public ProcedimentosEnfermagemHandler(HealthCareDbContext contextFactory, IMapper mapper)
        {
            this.repository = contextFactory;
            this.mapper = mapper;
        }

        public void Handle(SaveProcedimentosEnfermagemCommand command)
        {
            //throw new NotImplementedException();
        }

        public object HandleWithReturn(SaveProcedimentosEnfermagemCommand command)
        {
            Validation v = ValidarProcedimentosEnfermagem(command);
            if (v.IsValid)
            {
                try
                {
                    using (var transaction = repository.Database.BeginTransaction())
                    {
                        ProcedimentosEnfermagem procedimentosEnfermagem = mapper.Map<ProcedimentosEnfermagem>(command);
                        procedimentosEnfermagem.Procedimento = repository.Procedimento.FirstOrDefault(x => command.CodigoProcedimento == command.CodigoProcedimento);
                        procedimentosEnfermagem.Evolucao = repository.EvolucaoEnfermagem.FirstOrDefault(x => x.Id == command.EvolucaoEnfermagemId);

                        repository.Entry(procedimentosEnfermagem).State = EntityState.Added;
                        repository.ProcedimentosEnfermagem.Add(procedimentosEnfermagem);
                        repository.SaveChanges();

                        transaction.Commit();
                        return procedimentosEnfermagem.Id;
                    }
                }
                catch (Exception ex)
                {
                    if (ex is GeneralErrors)
                    {
                        foreach (var item in ((GeneralErrors)ex).Errors)
                        {
                            v.AddValidationMessage(new ValidationMessage(item));
                        }
                    }
                    v.AddValidationMessage(new ValidationMessage(ex.Message));
                    return v;
                }
            }
            else
                return v;
        }

        private Validation ValidarProcedimentosEnfermagem(SaveProcedimentosEnfermagemCommand command)
        {
            Validation v = new Validation();

            return v;
        }


        public void Handle(DeleteProcedimentosEnfermagemCommand command)
        {
        }
        public object HandleWithReturn(DeleteProcedimentosEnfermagemCommand command)
        {
            Validation v = new Validation();
            try
            {
                using (var transaction = repository.Database.BeginTransaction())
                {
                    if (command.DeleteId == Guid.Empty)
                        v.AddValidationMessage(new ValidationMessage("Nenhum id informado para exclusão!"));
                    else
                    {
                        var procedimentoEnfermagem = repository.ProcedimentosEnfermagem.FirstOrDefault(x => x.Id == command.DeleteId);
                        repository.Entry(procedimentoEnfermagem).State = EntityState.Deleted;
                        repository.SaveChanges();
                    }

                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                if (ex is GeneralErrors)
                {
                    foreach (var item in ((GeneralErrors)ex).Errors)
                    {
                        v.AddValidationMessage(new ValidationMessage(item));
                    }
                }
                v.AddValidationMessage(new ValidationMessage(ex.Message));
            }
            return v;
        }

        public void Dispose()
        {
            repository?.Dispose();
        }
    }
}
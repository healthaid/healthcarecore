﻿using HealthCare.Data.Repositories.Implementations.Atendimento;
using HealthCare.Domain.Entities.Atendimento;
using HealthCare.Infra.CQRS.Messaging;
using System;
namespace HealthCare.Infra.CQRS.Write.Commands.CommandsHandlers
{
    public class LaudoHandler : ICommandHandler<CreateLaudoCommand>
    {

        private readonly LaudoRepository repository;
        private readonly ResultadoExameRepository repositoryExame;

        public LaudoHandler(LaudoRepository repositorio, ResultadoExameRepository repositoryExame)
        {
            this.repository = repositorio;
            this.repositoryExame = repositoryExame;
        }

        public void Handle(CreateLaudoCommand command)
        {
            Laudo Laudo = new Laudo();
            Laudo.Id = Guid.NewGuid(); //command.profissionalId;
            Laudo.Exame = repositoryExame.Get(command.EcoCargiogramaId);
            //Laudo.Usuario = 
            Laudo.RowVersion = BitConverter.GetBytes(DateTime.Now.Date.Ticks);

            this.repository.Insert(Laudo);
        }

        public object HandleWithReturn(CreateLaudoCommand command)
        {
            throw new NotImplementedException();
        }
    }
}

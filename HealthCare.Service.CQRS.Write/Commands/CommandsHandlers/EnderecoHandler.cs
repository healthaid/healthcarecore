﻿using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.Data.Context;
using System;
using HealthCare.Domain.Entities.DbClasses.Cadastro;
using Microsoft.EntityFrameworkCore;

namespace HealthCare.Infra.CQRS.Write.Commands.CommandsHandlers
{
    public class EnderecoHandler : ICommandHandler<CreateEnderecoCommand>, ICommandHandler<UpdateEnderecoCommand>
    {
        private readonly HealthCareDbContext repository;

        public EnderecoHandler(HealthCareDbContext contextFactory)
        {
            repository = contextFactory;
        }

        public void Dispose()
        {
            repository?.Dispose();
        }

        public void Handle(CreateEnderecoCommand command)
        {
            try
            {
                using (var transaction = repository.Database.BeginTransaction())
                {
                    Endereco Endereco = new Endereco();
                    Endereco.CEP = command.CEP;
                    Endereco.Complemento = command.Complemento;
                    Endereco.Logradouro = command.Logradouro;
                    Endereco.Numero = command.Numero;

                    Endereco.RowVersion = BitConverter.GetBytes(DateTime.Now.Date.Ticks);
                    repository.Entry(Endereco).State = EntityState.Added;
                    repository.Endereco.Add(Endereco);
                    repository.SaveChanges();
                    transaction.Commit();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Handle(UpdateEnderecoCommand command)
        {
            try
            {
                using (var transaction = repository.Database.BeginTransaction())
                {
                    Endereco Endereco = repository.Endereco.Find(command.Id);
                    Endereco.CEP = command.CEP;
                    Endereco.Complemento = command.Complemento;
                    Endereco.Logradouro = command.Logradouro;
                    Endereco.Numero = command.Numero;

                    Endereco.RowVersion = BitConverter.GetBytes(DateTime.Now.Date.Ticks);
                    repository.Entry(Endereco).State = EntityState.Modified;
                    repository.SaveChanges();
                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public object HandleWithReturn(UpdateEnderecoCommand command)
        {
            throw new NotImplementedException();
        }

        public object HandleWithReturn(CreateEnderecoCommand command)
        {
            throw new NotImplementedException();
        }
    }
}

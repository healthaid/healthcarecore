﻿using HealtCare.Infra.Common.Validations;
using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using HealthCare.Domain.Entities.DbClasses.Cadastro;
using AutoMapper;

namespace HealthCare.Infra.CQRS.Write.Commands.CommandsHandlers
{
    public class LeitoHandler : ICommandHandler<SaveLeitoCommand>, ICommandHandler<DeleteLeitoCommand>
    {
        //private readonly LeitoRepository repository;
        //private readonly EnfermariaRepository repositoryEnfermaria;

        //public LeitoHandler(LeitoRepository _repositorio, EnfermariaRepository _repositoryEnfermaria)
        //{
        //    this.repository = _repositorio;
        //    this.repositoryEnfermaria = _repositoryEnfermaria;
        //}
        HealthCareDbContext repository;
        private IMapper mapper;
        public LeitoHandler(HealthCareDbContext contextFactory, IMapper mapper )
        {
            this.repository = contextFactory;
            this.mapper = mapper;
        }


        public void Handle(SaveLeitoCommand command)
        {
            //throw new NotImplementedException();
        }

        public object HandleWithReturn(SaveLeitoCommand command)
        {
            Validation v = ValidarLeito(command);
            Leito Leito = mapper.Map<Leito>(command);
            if (v.IsValid)
            {
                try
                {
                    using (var transaction = repository.Database.BeginTransaction())
                    {
                        if (command.LeitoId == Guid.Empty)
                        {
                            Leito.Ativo = true;
                            Leito.Enfermaria = repository.Enfermaria.FirstOrDefault(x => x.Id == command.EnfermariaId);

                            repository.Entry(Leito).State = EntityState.Added;
                            repository.Leito.Add(Leito);
                            repository.SaveChanges();
                        }
                        else
                        {
                            Leito = repository.Leito.FirstOrDefault(x => x.Id == command.LeitoId);
                            Leito.Ativo = command.Ativo;
                            Leito.Nome = command.Nome;
                            Leito.TipoLeito = (TipoLeito)command.TipoLeito;

                            repository.Entry(Leito).State = EntityState.Modified;
                            repository.SaveChanges();
                        }

                        transaction.Commit();

                        return Leito.Id;
                    }
                }
                catch (Exception ex)
                {
                    if (ex is GeneralErrors)
                    {
                        foreach (var item in ((GeneralErrors)ex).Errors)
                        {
                            v.AddValidationMessage(new ValidationMessage(item));
                        }
                    }
                    v.AddValidationMessage(new ValidationMessage(ex.Message));
                    return v;
                }
            }
            else
                return v;
        }

        private Validation ValidarLeito(SaveLeitoCommand command)
        {
            Validation v = new Validation();

            if (command.EnfermariaId == Guid.Empty)
                v.AddValidationMessage(new ValidationMessage("Enfermaria é obrigatório!"));
            if (string.IsNullOrWhiteSpace(command.Nome))
                v.AddValidationMessage(new ValidationMessage("Nome é obrigatório!"));
            if (command.EnfermariaId == Guid.Empty)
                v.AddValidationMessage(new ValidationMessage("Clínica é obrigatória!"));
            if (command.TipoLeito < 0)
                v.AddValidationMessage(new ValidationMessage("Informa o tipo de leito!"));

            return v;
        }


        public void Handle(DeleteLeitoCommand command)
        {
        }

        public object HandleWithReturn(DeleteLeitoCommand command)
        {
            Validation v = new Validation();
            try
            {
                using (var transaction = repository.Database.BeginTransaction())
                {
                    if (command.LeitoId == Guid.Empty)
                        v.AddValidationMessage(new ValidationMessage("Nenhum id informado para exclusão!"));
                    else
                    {
                        var leito = repository.Leito.FirstOrDefault(x => x.Id == command.LeitoId);
                        repository.Entry(leito).State = EntityState.Deleted;
                        repository.SaveChanges();
                    }
                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                if (ex is GeneralErrors)
                {
                    foreach (var item in ((GeneralErrors)ex).Errors)
                    {
                        v.AddValidationMessage(new ValidationMessage(item));
                    }
                }
                v.AddValidationMessage(new ValidationMessage(ex.Message));
            }
            return v;
        }

        public void Handle(RealizarExameCommand command)
        {

        }

        public void Dispose()
        {
            repository?.Dispose();
        }
    }
}
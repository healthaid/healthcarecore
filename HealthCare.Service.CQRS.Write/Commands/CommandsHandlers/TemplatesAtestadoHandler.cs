﻿using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.Data.Context;
using System;

namespace HealthCare.Infra.CQRS.Write.Commands.CommandsHandlers
{
    public class TemplatesAtestadoHandler : ICommandHandler<CreateTemplatesAtestadoCommand>
    {
        HealthCareDbContext context;
        public TemplatesAtestadoHandler(HealthCareDbContext contextFactory)
        {
            context = contextFactory;
        }

        public void Dispose()
        {
            context?.Dispose();
        }

        public void Handle(CreateTemplatesAtestadoCommand command)
        {
            try
            {
                //TemplatesAtestado TemplatesAtestado = new TemplatesAtestado();
                //TemplatesAtestado.Id = Guid.NewGuid();
                //TemplatesAtestado.Nome = command.Nome;
                //TemplatesAtestado.Conteudo = command.Conteudo;
                //if (command.AtestadoId != Guid.Empty)
                //    TemplatesAtestado.Atestado = this.atestadoRepository.Get(command.AtestadoId);

                //this.repository.Insert(TemplatesAtestado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Handle(UpdateTemplatesAtestadoCommand command)
        {
            try
            {
                //TemplatesAtestado TemplatesAtestado = repository.Get(command.Id);

                //TemplatesAtestado.Nome = command.Nome;
                //TemplatesAtestado.Conteudo = command.Conteudo;
                //if (command.AtestadoId != Guid.Empty)
                //    TemplatesAtestado.Atestado = this.atestadoRepository.Get(command.AtestadoId);

                //this.repository.Insert(TemplatesAtestado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public object HandleWithReturn(CreateTemplatesAtestadoCommand command)
        {
            throw new NotImplementedException();
        }
    }
}

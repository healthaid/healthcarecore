﻿using HealthCare.Domain.Entities.DbClasses.Atendimento;
using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace HealthCare.Infra.CQRS.Write.Commands.CommandsHandlers
{
    public class AnamneseEventoHandler : ICommandHandler<CreateAnamneseCommand>
    {
        private readonly HealthCareDbContext repository;
        public AnamneseEventoHandler(HealthCareDbContext contextFactory)
        {
            this.repository = contextFactory;
        }

        public void Dispose()
        {
            repository?.Dispose();
        }

        public void Handle(CreateAnamneseCommand command)
        {
            try
            {
                Anamnese anamnese = new Anamnese();

                if (command.AnamneseId == Guid.Empty)
                    anamnese.Atendimento = repository.Atendimento.Find(command.AtendimentoId);
                else
                    anamnese = repository.Anamnese.Include("Atendimento").Where(x => x.Id == command.AnamneseId).FirstOrDefault();

                anamnese.Descricao = command.Descricao;
                if (command.Ciap2Id == Guid.Empty)
                    anamnese.QueixaPrincipal = command.QueixaPrincipal;
                else
                {
                    anamnese.Ciap2 = repository.Ciap2.Find(command.Ciap2Id);
                    anamnese.QueixaPrincipal = anamnese.Ciap2.Descricao;
                }


                if (anamnese.Id == Guid.Empty)
                {
                    repository.Entry(anamnese).State = EntityState.Added;
                    repository.Anamnese.Add(anamnese);
                    repository.SaveChanges();
                }
                else
                {
                    repository.Entry(anamnese).State = EntityState.Modified;
                    repository.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Handle(UpdateAnanmneseCommand command)
        {
            try
            {
                using (var transaction = repository.Database.BeginTransaction())
                {
                    Anamnese anamnese = repository.Anamnese.Find(command.AnamneseId);

                    anamnese.Atendimento = repository.Atendimento.Find(command.AtendimentoId);
                    anamnese.Descricao = command.Descricao;
                    if (command.Ciap2Id == Guid.Empty)
                        anamnese.QueixaPrincipal = command.QueixaPrincipal;
                    else
                        anamnese.Ciap2 = repository.Ciap2.Find(command.Ciap2Id);

                    repository.Entry(anamnese).State = EntityState.Modified;
                    repository.SaveChanges();
                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public Validation HandleWithReturn(CreateAnamneseCommand command)
        {
            throw new NotImplementedException();
        }

        object ICommandHandler<CreateAnamneseCommand>.HandleWithReturn(CreateAnamneseCommand command)
        {
            throw new NotImplementedException();
        }
    }
}

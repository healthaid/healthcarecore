﻿using AutoMapper;
using HealtCare.Infra.Common.Validations;
using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace HealthCare.Infra.CQRS.Write.Commands.CommandsHandlers
{
    public class EnfermariaHandler : ICommandHandler<SaveEnfermariaCommand>, ICommandHandler<DeleteEnfermariaCommand>
    {
        private readonly HealthCareDbContext repository;
        private IMapper mapper;

        public EnfermariaHandler(HealthCareDbContext contextFactory, IMapper mapper )
        {
            repository = contextFactory;
            this.mapper = mapper;
        }


        public void Handle(SaveEnfermariaCommand command)
        {
            //throw new NotImplementedException();
        }

        public object HandleWithReturn(SaveEnfermariaCommand command)
        {
            Validation v = ValidarEnfermaria(command);
            Enfermaria enfermaria = mapper.Map<Enfermaria>(command);

            if (v.IsValid)
            {
                try
                {
                    using (var transaction = repository.Database.BeginTransaction())
                    {
                        if (command.EnfermariaId == Guid.Empty)
                        {
                            enfermaria.Ativo = true;
                            /// Obtém a unidade
                            /// ----------------------------
                            enfermaria.Unidade = repository.Unidade.Find(command.UnidadeId);

                            /// Insere Enfermaria
                            /// ----------------------------
                            repository.Entry(enfermaria).State = EntityState.Added;
                            repository.Enfermaria.Add(enfermaria);
                            repository.SaveChanges();
                            transaction.Commit();
                        }
                        else
                        {
                            enfermaria = repository.Enfermaria.Find(command.EnfermariaId);
                            enfermaria.Ativo = command.Ativo;
                            enfermaria.Nome = command.Nome;
                            enfermaria.Local = command.Local;

                            repository.Entry(enfermaria).State = EntityState.Modified;
                            repository.SaveChanges();
                            transaction.Commit();
                        }
                        return enfermaria.Id;
                    }
                }
                catch (Exception ex)
                {
                    if (ex is GeneralErrors)
                    {
                        foreach (var item in ((GeneralErrors)ex).Errors)
                        {
                            v.AddValidationMessage(new ValidationMessage(item));
                        }
                    }
                    v.AddValidationMessage(new ValidationMessage(ex.Message));
                    return v;
                }
            }
            else
                return v;
        }

        private Validation ValidarEnfermaria(SaveEnfermariaCommand command)
        {
            Validation v = new Validation();

            //if (string.IsNullOrWhiteSpace(command))
            //    v.AddValidationMessage(new ValidationMessage("Nome é obrigatório!"));
            return v;
        }


        public void Handle(DeleteEnfermariaCommand command)
        {
        }

        public object HandleWithReturn(DeleteEnfermariaCommand command)
        {
            Validation v = ValidaExclusao(command);
            try
            {
                if (v.IsValid)
                {
                    using (var transaction = repository.Database.BeginTransaction())
                    {
                        if (command.EnfermariaId == Guid.Empty)
                        {
                            v.AddValidationMessage(new ValidationMessage("Nenhum id informado para exclusão!"));
                            transaction.Dispose();
                        }
                        else
                        {
                            var entity = repository.Enfermaria.Find(command.EnfermariaId);
                            repository.Entry(entity).State = EntityState.Deleted;
                            repository.SaveChanges();
                            transaction.Commit();
                        }
                        return command.EnfermariaId;
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex is GeneralErrors)
                {
                    foreach (var item in ((GeneralErrors)ex).Errors)
                    {
                        v.AddValidationMessage(new ValidationMessage(item));
                    }
                }
                v.AddValidationMessage(new ValidationMessage(ex.Message));
            }
            return v;
        }

        private Validation ValidaExclusao(DeleteEnfermariaCommand command)
        {
            Validation v = new Validation();

            //if (repository.GetEnfermariaComLeitos(command.EnfermariaId).Leitos.Count > 0)
            //    v.AddValidationMessage(new ValidationMessage("Não é possível excluir pois há leitos vinculado"));

            return v;
        }

        public void Handle(RealizarExameCommand command)
        {

        }

        public void Dispose()
        {
            repository?.Dispose();
        }
    }
}
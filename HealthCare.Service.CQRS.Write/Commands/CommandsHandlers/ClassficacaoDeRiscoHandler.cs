﻿using AutoMapper;
using HealtCare.Infra.Common.Validations;
using HealthCare.Domain.Entities.DbClasses.Atendimento;
using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.CQRS.Write.Models;
using HealthCare.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace HealthCare.Infra.CQRS.Write.Commands.CommandsHandlers
{
    public class ClassficacaoDeRiscoHandler : ICommandHandler<SaveClassificacaoDeRiscoCommand>, ICommandHandler<AtualizarClassificacaoRiscoCommand>
    {
        private readonly HealthCareDbContext repository;
        private IMapper mapper;

        public ClassficacaoDeRiscoHandler(HealthCareDbContext contextFactory, IMapper mapper)
        {
            this.repository = contextFactory;
            this.mapper = mapper;
        }

        public void Handle(SaveClassificacaoDeRiscoCommand command)
        {
            //throw new NotImplementedException();
        }

        public object HandleWithReturn(SaveClassificacaoDeRiscoCommand command)
        {
            Validation v = ValidarClassificacaoDeRisco(command);
            //Validation v = new Validation();
            ClassificacaoDeRisco cr = mapper.Map<ClassificacaoDeRisco>(command);
            if (v.IsValid)
            {
                try
                {
                    using (var transaction = repository.Database.BeginTransaction())
                    {

                        /// Faz referência aos Sinais Vitais
                        /// --------------------------------
                        ICommandHandler<SaveSinaisVitaisCommand> svHandler = new SinaisVitaisHandler(repository, mapper);

                        /// Carrega acolhimento 
                        /// -------------------
                        cr.Acolhimento = repository.Query<Acolhimento>().FirstOrDefault(x => x.Id == command.AcolhimentoId);

                        /// Altera o serviço (Especialidade Médica)
                        /// ---------------------------------------
                        cr.Acolhimento.Servico = repository.Query<Servico>().FirstOrDefault(x => x.Id == command.EspecialidadeMedica);
                        cr.EspecialidadeMedica = cr.Acolhimento.Servico.Descricao;

                        if (command.ClassficacaoDeRiscoId == Guid.Empty)
                        {
                            ///// Salva Sinais Vitais
                            /// --------------------
                            cr.SinaisVitais = (SinaisVitais)svHandler.HandleWithReturn(command.SinaisVitais);

                            /// Obtém o Risco
                            /// --------------------
                            cr.Risco = repository.Query<Risco>().FirstOrDefault(x => x.Descricao == command.Classificacao);

                            cr.DataClassificacao = DateTime.Today;
                            cr.Profissional = repository.Profissional.FirstOrDefault(x => x.Id == command.ProfissionalId);

                            /// Insere classficação de risco
                            /// ----------------------------
                            repository.Entry(cr).State = EntityState.Added;
                            repository.ClassificacaoDeRisco.Add(cr);
                            repository.SaveChanges();

                            /// Gera um 'Atendimento_Procedimento'???? ou uma Classificação Procedimento? 
                            /// 03.01.06.011 - 8 - ACOLHIMENTO COM CLASSIFICAÇÃO DE RISCO
                        }
                        else
                        {
                            ClassificacaoDeRisco ClassificacaoDeRisco = repository.ClassificacaoDeRisco.Find(command.ClassficacaoDeRiscoId);
                            ClassificacaoDeRisco.Acolhimento = repository.Query<Acolhimento>().FirstOrDefault(x => x.Id == command.AcolhimentoId);
                            ClassificacaoDeRisco.Risco = repository.Query<Risco>().FirstOrDefault(x => x.Descricao == command.Classificacao);
                            ClassificacaoDeRisco.Alergia = command.Alergia;
                            ClassificacaoDeRisco.CausaExterna = command.CausaExterna;
                            ClassificacaoDeRisco.DoencasPreexistentes = command.DoencasPreexistentes;
                            ClassificacaoDeRisco.MedicamentosEmUso = command.MedicamentosEmUso;
                            ClassificacaoDeRisco.NivelDeConsciencia = command.NivelDeConsciencia;
                            ClassificacaoDeRisco.Observacao = command.Observacao;
                            ClassificacaoDeRisco.Queixa = command.Queixa;

                            cr.DataClassificacao = DateTime.Today;
                            cr.Profissional = repository.Profissional.FirstOrDefault(x => x.Id == command.ProfissionalId);

                            /// Atualização de Sinais Vitais
                            /// ----------------------------
                            ClassificacaoDeRisco.SinaisVitais = (SinaisVitais)svHandler.HandleWithReturn(command.SinaisVitais);

                            repository.Entry(ClassificacaoDeRisco).State = EntityState.Modified;
                            repository.SaveChanges();
                        }

                        transaction.Commit();

                        return cr.Id;
                    }

                }
                catch (Exception ex)
                {
                    if (ex is GeneralErrors)
                    {
                        foreach (var item in ((GeneralErrors)ex).Errors)
                        {
                            v.AddValidationMessage(new ValidationMessage(item));
                        }
                    }
                    v.AddValidationMessage(new ValidationMessage(ex.Message));
                    return v;
                }
            }
            else
                return v;
        }

        private Validation ValidarClassificacaoDeRisco(SaveClassificacaoDeRiscoCommand command)
        {
            Validation v = new Validation();

            if (string.IsNullOrWhiteSpace(command.Classificacao))
                v.AddValidationMessage(new ValidationMessage("Risco é obrigatório!"));

            return v;
        }


        public void Handle(AtualizarClassificacaoRiscoCommand command)
        {

        }

        public object HandleWithReturn(AtualizarClassificacaoRiscoCommand command)
        {
            Validation v = new Validation();
            if (v.IsValid)
            {
                try
                {
                    using (var transaction = repository.Database.BeginTransaction())
                    {
                        ClassificacaoDeRisco classificacaoDeRisco;

                        if (command.ClassificacaoDeRiscoId == Guid.Empty)
                        {
                            classificacaoDeRisco = new ClassificacaoDeRisco();
                            classificacaoDeRisco.Acolhimento = repository.Query<Acolhimento>().FirstOrDefault(x => x.Id == command.AcolhimentoId);
                        }
                        else
                            classificacaoDeRisco = repository.Query<ClassificacaoDeRisco>().FirstOrDefault(x => x.Id == command.ClassificacaoDeRiscoId);

                        classificacaoDeRisco.Risco = repository.Query<Risco>().FirstOrDefault(x => x.Descricao == command.Classificacao); // repositoryRisco.GetAll().FirstOrDefault(x => x.Descricao == command.Classificacao);

                        if (classificacaoDeRisco.Id == Guid.Empty)
                        {
                            repository.Entry(classificacaoDeRisco).State = EntityState.Added;
                            repository.ClassificacaoDeRisco.Add(classificacaoDeRisco);
                            repository.SaveChanges();
                        }
                        else
                        {
                            repository.Entry(classificacaoDeRisco).State = EntityState.Modified;
                            repository.SaveChanges();
                        }


                        transaction.Commit();

                        return classificacaoDeRisco.Id;
                    }
                }
                catch (Exception ex)
                {
                    if (ex is GeneralErrors)
                    {
                        foreach (var item in ((GeneralErrors)ex).Errors)
                        {
                            v.AddValidationMessage(new ValidationMessage(item));
                        }
                    }
                    v.AddValidationMessage(new ValidationMessage(ex.Message));
                    return v;
                }
            }
            else
                return v;
        }

        public void Dispose()
        {
            repository?.Dispose();
        }
    }
}
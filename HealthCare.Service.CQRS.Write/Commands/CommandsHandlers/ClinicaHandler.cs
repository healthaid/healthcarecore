﻿using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using HealthCare.Domain.Entities.DbClasses.Cadastro;
using System;

namespace HealthCare.Infra.CQRS.Write.Commands.CommandsHandlers
{
    public class ClinicaHandler : ICommandHandler<CreateClinicaCommand>
    {
        private readonly HealthCareDbContext repository;
        public ClinicaHandler(HealthCareDbContext contextFactory)
        {
            repository = contextFactory;
        }

        public void Dispose()
        {
            repository?.Dispose();
        }

        public void Handle(CreateClinicaCommand command)
        {
            try
            {
                using (var transaction = repository.Database.BeginTransaction())
                {
                    Clinica Clinica = new Clinica();
                    Clinica.Id = Guid.NewGuid();
                    Clinica.Nome = command.Nome;

                    Clinica.RowVersion = BitConverter.GetBytes(DateTime.Now.Date.Ticks);

                    repository.Entry(Clinica).State = EntityState.Added;
                    repository.Clinica.Add(Clinica);
                    repository.SaveChanges();
                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Handle(UpdateClinicaCommand command)
        {
            try
            {
                using (var transaction = repository.Database.BeginTransaction())
                {
                    Clinica Clinica = repository.Clinica.Find(command.Id);
                    Clinica.Nome = command.Nome;

                    Clinica.RowVersion = BitConverter.GetBytes(DateTime.Now.Date.Ticks);
                    repository.Entry(Clinica).State = EntityState.Modified;
                    repository.SaveChanges();
                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public object HandleWithReturn(CreateClinicaCommand command)
        {
            throw new NotImplementedException();
        }

        public object HandleWithReturn(UpdateClinicaCommand command)
        {
            throw new NotImplementedException();
        }
    }
}

﻿
using AutoMapper;
using HealtCare.Infra.Common.Validations;
using HealthCare.Domain.Entities.DbClasses.Atendimento;
using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace HealthCare.Infra.CQRS.Write.Commands.CommandsHandlers
{
    public class SinaisVitaisHandler : ICommandHandler<SaveSinaisVitaisCommand>//, ICommandHandler<SaveSinaisVitaisCommand>
    {

        protected HealthCareDbContext repository;
        protected IMapper mapper;
        public SinaisVitaisHandler(HealthCareDbContext contextFactory, IMapper mapper)
        {
            this.repository = contextFactory;
            this.mapper = mapper;
        }

        public void Dispose()
        {
            repository?.Dispose();
        }

        public void Handle(SaveSinaisVitaisCommand command)
        {
            //throw new NotImplementedException();
        }

        public object HandleWithReturn(SaveSinaisVitaisCommand command)
        {
            //Validation v = ValidarSinaisVitais(command);
            Validation v = new Validation();
            SinaisVitais sv = mapper.Map<SinaisVitais>(command);
            if (v.IsValid)
            {
                try
                {
                    if (command.SaveSinaisVitaisId == Guid.Empty)
                    {
                        repository.Entry(sv).State = EntityState.Added;
                        repository.SinaisVitais.Add(sv);
                        repository.SaveChanges();
                    }
                    else
                    {
                        SinaisVitais SinaisVitais = repository.Query<SinaisVitais>().FirstOrDefault(x => x.Id == command.SaveSinaisVitaisId);
                        SinaisVitais.Peso = command.Peso;
                        SinaisVitais.Pressao = command.Pressao;
                        SinaisVitais.Pulso = command.Pulso;
                        SinaisVitais.Temperatura = command.Temperatura;
                        SinaisVitais.FrequenciaRespiratoria = command.FrequenciaRespiratoria;
                        SinaisVitais.GlicemiaCapilar = command.GlicemiaCapilar;
                        SinaisVitais.Altura = command.Altura;

                        repository.Entry(SinaisVitais).State = EntityState.Modified;
                        repository.SaveChanges();
                    }

                    return sv;
                }
                catch (Exception ex)
                {
                    if (ex is GeneralErrors)
                    {
                        foreach (var item in ((GeneralErrors)ex).Errors)
                        {
                            v.AddValidationMessage(new ValidationMessage(item));
                        }
                    }
                    v.AddValidationMessage(new ValidationMessage(ex.Message));
                    return v;
                }
            }
            else
                return v;
        }

        private Validation ValidarSinaisVitais(SaveSinaisVitaisCommand command)
        {
            return null;
        }

    }
}
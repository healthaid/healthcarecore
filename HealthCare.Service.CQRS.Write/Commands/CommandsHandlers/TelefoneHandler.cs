﻿using HealthCare.Data.Repositories.Definitions.Cadastro;
using HealthCare.Data.Repositories.Implementations.Cadastro;
using HealthCare.Domain.Entities;
using HealthCare.Domain.Entities.Cadastro;
using HealthCare.Infra.CQRS.EventSourcing;
using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Service.CQRS.Write.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Write.Commands.CommandsHandlers
{
    public class TelefoneHandler : ICommandHandler<CreateTelefoneCommand>, ICommandHandler<UpdateTelefoneCommand>
    {


        public TelefoneHandler(TelefoneRepository repositorio, TipoTelefoneRepository repositoryTipoTelefone, PacienteRepository repositoryPaciente)
        {

            this.repository = repositorio;
            this.repositoryPaciente = repositoryPaciente;
            this.repositoryTipoTelefone = repositoryTipoTelefone;
        }

        public void Handle(CreateTelefoneCommand command)
        {
            Telefone Telefone = new Telefone();
            Telefone.Id = Guid.NewGuid();
            Telefone.Numero = command.Numero;
            Telefone.Paciente = repositoryPaciente.Get(command.PacienteId);
            Telefone.TipoTelefone = repositoryTipoTelefone.Get(command.TipoTelefoneId);
            
            Telefone.RowVersion = BitConverter.GetBytes(DateTime.Now.Date.Ticks);

            this.repository.Insert(Telefone);
        }

        public void Handle(UpdateTelefoneCommand command)
        {
            Telefone Telefone = repository.Get(command.Id);
            Telefone.Numero = command.Numero;
            Telefone.Paciente = repositoryPaciente.Get(command.PacienteId);
            Telefone.TipoTelefone = repositoryTipoTelefone.Get(command.TipoTelefoneId);

            Telefone.RowVersion = BitConverter.GetBytes(DateTime.Now.Date.Ticks);

            this.repository.Update(Telefone);
        }

        public object HandleWithReturn(UpdateTelefoneCommand command)
        {
            throw new NotImplementedException();
        }

        public object HandleWithReturn(CreateTelefoneCommand command)
        {
            throw new NotImplementedException();
        }
    }
}

﻿using HealthCare.Data.Repositories.Definitions.Agenda;
using HealthCare.Data.Repositories.Implementations.Agenda;
using HealthCare.Data.Repositories.Implementations.Cadastro;
using HealthCare.Domain.Entities;
using HealthCare.Domain.Entities.Agenda;
using HealthCare.Domain.Entities.Cadastro;
using HealthCare.Infra.CQRS.EventSourcing;
using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Service.CQRS.Write.Commands;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Write.Commands.CommandsHandlers
{
    public class AgendaEventoHandler :
        ICommandHandler<CreateAgendaEventoCommand>, ICommandHandler<UpdateAgendaEventoCommand>, ICommandHandler<CancelarAgendamentoCommand>
    {

        private readonly AgendaEventoRepository repository;
        private readonly AgendaRepository agendaRepository;
        private readonly ServicoRepository servicoRepository;
        private readonly PacienteRepository pacienteRepository;


        public AgendaEventoHandler(AgendaEventoRepository repositorio, AgendaRepository agendaRepository, ServicoRepository servicoRepository, PacienteRepository pacienteRepository)
        {
            this.repository = repositorio;
            this.agendaRepository = agendaRepository;
            this.servicoRepository = servicoRepository;
            this.pacienteRepository = pacienteRepository;
        }

        public object HandleWithReturn(CreateAgendaEventoCommand command)
        {
            Validation v = new Validation();

            if (command.PacienteId == Guid.Empty)
                v.AddValidationMessage(new ValidationMessage("Paciente é obrigatório!"));
            if (command.ServicoId == Guid.Empty)
                v.AddValidationMessage(new ValidationMessage("Serviço é obrigatório!"));

            if (v.IsValid)
            {
                try
                {
                    AgendaEvento AgendaEvento = this.repository.Get(command.Id);/// .Get(command.Id);
                    AgendaEvento.Agenda = agendaRepository.Get(command.AgendaId);
                    AgendaEvento.Paciente = pacienteRepository.Get(command.PacienteId);
                    AgendaEvento.Servico = servicoRepository.Get(command.ServicoId);
                    AgendaEvento.RowVersion = BitConverter.GetBytes(DateTime.Now.Date.Ticks);

                    this.repository.Insert(AgendaEvento);
                }
                catch (Exception ex)
                {
                    if (ex is DbEntityValidationException)
                    {
                        var dbEx = (DbEntityValidationException)ex;
                        foreach (var validationErrors in dbEx.EntityValidationErrors)
                        {
                            foreach (var validationError in validationErrors.ValidationErrors)
                            {
                                v.AddValidationMessage(new ValidationMessage(string.Format("Property: {0} Error: {1}",
                                                        validationError.PropertyName,
                                                        validationError.ErrorMessage)));
                            }
                        }
                    }
                    v.AddValidationMessage(new ValidationMessage(ex.Message));
                }
            }
            return v;
        }

        public object HandleWithReturn(UpdateAgendaEventoCommand command)
        {
            Validation v = new Validation();

            if (command.Id == Guid.Empty)
                v.AddValidationMessage(new ValidationMessage("Evento é obrigatório!"));
            if (command.PacienteId == Guid.Empty)
                v.AddValidationMessage(new ValidationMessage("Paciente é obrigatório!"));
            if (command.ServicoId == Guid.Empty)
                v.AddValidationMessage(new ValidationMessage("Serviço é obrigatório!"));

            if (v.IsValid)
            {
                try
                {
                    AgendaEvento AgendaEvento = this.repository.ObterAgendaEvento(command.Id);

                    AgendaEvento.Paciente = pacienteRepository.Get(command.PacienteId);
                    AgendaEvento.Servico = servicoRepository.Get(command.ServicoId);

                    this.repository.Update(AgendaEvento);
                }
                catch (Exception ex)
                {
                    if (ex is DbEntityValidationException)
                    {
                        var dbEx = (DbEntityValidationException)ex;
                        foreach (var validationErrors in dbEx.EntityValidationErrors)
                        {
                            foreach (var validationError in validationErrors.ValidationErrors)
                            {
                                v.AddValidationMessage(new ValidationMessage(string.Format("Property: {0} Error: {1}",
                                                        validationError.PropertyName,
                                                        validationError.ErrorMessage)));
                            }
                        }
                    }
                    v.AddValidationMessage(new ValidationMessage(ex.Message));
                }
            }
            return v;
        }

        public object HandleWithReturn(CancelarAgendamentoCommand command)
        {
            Validation v = new Validation();

            if (command.Id == Guid.Empty)
                v.AddValidationMessage(new ValidationMessage("Evento é obrigatório!"));

            if (v.IsValid)
            {
                try
                {
                    AgendaEvento AgendaEvento = this.repository.ObterAgendaEvento(command.Id);// this.repository.Get(command.Id);

                    AgendaEvento.Servico = null;
                    AgendaEvento.Paciente = null;

                    this.repository.Update(AgendaEvento);
                }
                catch (Exception ex)
                {
                    if (ex is DbEntityValidationException)
                    {
                        var dbEx = (DbEntityValidationException)ex;
                        foreach (var validationErrors in dbEx.EntityValidationErrors)
                        {
                            foreach (var validationError in validationErrors.ValidationErrors)
                            {
                                v.AddValidationMessage(new ValidationMessage(string.Format("Property: {0} Error: {1}",
                                                        validationError.PropertyName,
                                                        validationError.ErrorMessage)));
                            }
                        }
                    }
                    v.AddValidationMessage(new ValidationMessage(ex.Message));
                }
            }
            return v;
        }

        public void Handle(CancelarAgendamentoCommand command)
        {
            throw new NotImplementedException();
        }

        public void Handle(UpdateAgendaEventoCommand command)
        {
            throw new NotImplementedException();
        }

        public void Handle(CreateAgendaEventoCommand command)
        {
            throw new NotImplementedException();
        }
    }
}
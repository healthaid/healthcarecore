﻿using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.Data.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using HealthCare.Domain.Entities.DbClasses.Atendimento;
using HealthCare.Domain.Entities.DbClasses.Cadastro;

namespace HealthCare.Infra.CQRS.Write.Commands.CommandsHandlers
{
    public class ResultadoExameHandler : ICommandHandler<CreateResultadoExameCommand>
    {
        HealthCareDbContext repository;
        public ResultadoExameHandler(HealthCareDbContext contextFactory)
        {
            this.repository = contextFactory;
        }

        public void Dispose()
        {
            repository?.Dispose();
        }

        public void Handle(CreateResultadoExameCommand command)
        {

            using (var transaction = repository.Database.BeginTransaction())
            {
                ResultadoExame ResultadoExame = new ResultadoExame();

                ResultadoExame.Id = command.Id;
                ResultadoExame.Exame = repository.Query<Exame>().FirstOrDefault(x => x.Id == command.ExameId);

                if (command.AtendimentoId != Guid.Empty)
                    ResultadoExame.Atendimento = repository.Query<Atendimento>().FirstOrDefault(x => x.Id == command.AtendimentoId);
                if (command.PacienteId != Guid.Empty)
                    ResultadoExame.Paciente = repository.Query<Paciente>().FirstOrDefault(x => x.Id == command.PacienteId);

                foreach (KeyValuePair<Guid, string> elemento in command.ElementosResultadoExame)
                {
                    ResultadoExameElemento resultadoExameElemento = new ResultadoExameElemento();
                    resultadoExameElemento.Id = Guid.NewGuid();
                    //resultadoExameElemento.ElementoExame = repository.ResultadoExameElemento  repositoryElementoExame.Get(elemento.Key);
                    resultadoExameElemento.Descricao = elemento.Value;

                    ResultadoExame.Elementos.Add(resultadoExameElemento);
                }

                //this.repository.Insert(ResultadoExame);
                transaction.Commit();

            }
        }



        public object HandleWithReturn(CreateResultadoExameCommand command)
        {
            throw new NotImplementedException();
        }
    }
}

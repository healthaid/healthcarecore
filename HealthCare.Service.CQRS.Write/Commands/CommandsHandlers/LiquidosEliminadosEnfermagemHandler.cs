﻿using HealtCare.Infra.Common.Validations;
using HealthCare.Domain.Entities.DbClasses.Atendimento;
using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace HealthCare.Infra.CQRS.Write.Commands.CommandsHandlers
{
    public class LiquidosEliminadosEnfermagemHandler : ICommandHandler<SaveLiquidosEliminadosEnfermagemCommand>, ICommandHandler<DeleteLiquidoEliminadosEnfermagemCommand>
    {
        HealthCareDbContext repository;
        public LiquidosEliminadosEnfermagemHandler(HealthCareDbContext contextFactory)
        {
            this.repository = contextFactory;
        }

        public void Handle(SaveLiquidosEliminadosEnfermagemCommand command)
        {
            //throw new NotImplementedException();
        }

        public object HandleWithReturn(SaveLiquidosEliminadosEnfermagemCommand command)
        {
            Validation v = ValidarLiquidosEliminadosEnfermagem(command);
            if (v.IsValid)
            {
                try
                {
                    using (var transaction = repository.Database.BeginTransaction())
                    {
                        LiquidosEliminadosEnfermagem liquidosEliminados = new LiquidosEliminadosEnfermagem();
                        liquidosEliminados.LiquidosEliminados = repository.LiquidosEliminados.FirstOrDefault(x => x.Id == command.LiquidosEliminadosId);
                        liquidosEliminados.evolucaoEnfermagem = repository.EvolucaoEnfermagem.FirstOrDefault(x => x.Id == command.EvolucaoEnfermagemId);

                        repository.Entry(liquidosEliminados).State = EntityState.Added;
                        repository.LiquidosEliminadosEnfermagem.Add(liquidosEliminados);
                        repository.SaveChanges();

                        transaction.Commit();

                        return liquidosEliminados.Id;
                    }
                }
                catch (Exception ex)
                {
                    if (ex is GeneralErrors)
                    {
                        foreach (var item in ((GeneralErrors)ex).Errors)
                        {
                            v.AddValidationMessage(new ValidationMessage(item));
                        }
                    }
                    v.AddValidationMessage(new ValidationMessage(ex.Message));
                    return v;
                }
            }
            else
                return v;
        }

        private Validation ValidarLiquidosEliminadosEnfermagem(SaveLiquidosEliminadosEnfermagemCommand command)
        {
            Validation v = new Validation();

            return v;
        }


        public void Handle(DeleteLiquidoEliminadosEnfermagemCommand command)
        {
        }
        public object HandleWithReturn(DeleteLiquidoEliminadosEnfermagemCommand command)
        {
            Validation v = new Validation();
            try
            {
                using (var transaction = repository.Database.BeginTransaction())
                {
                    if (command.DeleteId == Guid.Empty)
                        v.AddValidationMessage(new ValidationMessage("Nenhum id informado para exclusão!"));
                    else
                    {
                        LiquidosEliminadosEnfermagem rm = repository.LiquidosEliminadosEnfermagem.FirstOrDefault(x => x.Id == command.DeleteId);
                        repository.Entry(rm).State = EntityState.Deleted;
                        repository.SaveChanges();
                    }

                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                if (ex is GeneralErrors)
                {
                    foreach (var item in ((GeneralErrors)ex).Errors)
                    {
                        v.AddValidationMessage(new ValidationMessage(item));
                    }
                }
                v.AddValidationMessage(new ValidationMessage(ex.Message));
            }
            return v;
        }

        public void Dispose()
        {
            repository?.Dispose();
        }
    }
}
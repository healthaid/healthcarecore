﻿using HealtCare.Infra.Common.Validations;
using HealthCare.Domain.Entities.DbClasses.Admin;
using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HealthCare.Infra.CQRS.Write.Commands.CommandsHandlers
{

    public class ProfissionalHandler : ICommandHandler<CreateProfissionalCommand>, ICommandHandler<DeleteProfissionalCommand>, ICommandHandler<FirstAcessCommand>
    {
        HealthCareDbContext repository;
        public ProfissionalHandler(HealthCareDbContext context)
        {
            this.repository = context;
        }

        public ProfissionalHandler()
        {

        }

        public void Handle(CreateProfissionalCommand command)
        {
        }

        public object HandleWithReturn(CreateProfissionalCommand command)
        {
            Validation v = ValidaProfissional(command);
            if (v.IsValid)
            {
                try
                {
                    using (var transaction = repository.Database.BeginTransaction())
                    {
                        Profissional p = new Profissional();
                        Guid[] servicos = command.Servicos;
                        Guid[] roles = command.Roles;
                        if (command.ProfissionalId == Guid.Empty)
                        {
                            p.CNS = command.CNS;
                            p.CPF = command.CPF;
                            p.Nacionalidade = command.Nacionalidade;
                            p.Nascimento = command.Nascimento;
                            p.Naturalidade = command.Naturalidade;
                            p.Nome = command.Nome;
                            p.NomeMae = command.NomeMae;
                            p.NomePai = command.NomePai;
                            p.Sexo = command.Sexo;
                            p.IsDeleted = false;
                            p.RowVersion = BitConverter.GetBytes(DateTime.Now.Date.Ticks);
                            command.CBO = command.CBO.Split(' ').ElementAt(0);
                            p.Cbo = repository.Cbo.Where(x => x.Codigo == command.CBO).FirstOrDefault();
                            p.Unidade = repository.Unidade.Find(command.UnidadeId);
                            p.CRM = command.CRM;

                            p.ProfissionalServicos = new List<ProfissionalServico>();

                            foreach (var servico in servicos)
                                p.ProfissionalServicos.Add(new ProfissionalServico() {ProfissionalId = p.Id, ServicoId = servico });



                            Usuario u = new Usuario();
                            u.Nome = p.Nome;
                            u.PasswordHash = p.CPF;
                            u.PrimeiroLogin = true;
                            u.Email = command.Email;
                            u.UserName = command.UserName;
                            u.Profissional = p;

                            repository.Entry(p).State = EntityState.Added;
                            repository.Profissional.Add(p);
                            repository.Entry(u).State = EntityState.Added;
                            repository.Users.Add(u);
                            repository.SaveChanges();
                            foreach (var role in roles)
                            {
                                UserRole userRole = new UserRole { RoleId = role, UserId = u.Id };
                                repository.Entry(userRole).State = EntityState.Added;
                                repository.UserRoles.Add(userRole);
                            }

                           
                            repository.SaveChanges();
                            transaction.Commit();
                            return p.Id;
                            //  string body = string.Format(@"<html>
                            //                                  <body>
                            //                                   <p> Olá, {0} bem vindo ao sistema Health Care</p>
                            //                                   <P> Essa aqui é o seu usuário e senha para logar no sistema</p>
                            //                                   <p><b> {1} </b></p>
                            //                                   <p><b> {2} </b></p>
                            //                                  </body>
                            //                               </html>", u.Nome, u.Email, u.PasswordHash);
                            //new MailUtil().SendEmail(u.Email,body,body);
                        }
                        else
                        {
                            p = repository.Profissional.Include("ProfissionalServicos").FirstOrDefault(x => x.Id == command.ProfissionalId);
                            p.CNS = command.CNS;
                            p.CPF = command.CPF;
                            p.Nacionalidade = command.Nacionalidade;
                            p.Nascimento = command.Nascimento;
                            p.Naturalidade = command.Naturalidade;
                            p.Nome = command.Nome;
                            p.NomeMae = command.NomeMae;
                            p.NomePai = command.NomePai;
                            p.Sexo = command.Sexo;
                            p.IsDeleted = false;
                            p.Unidade = repository.Unidade.Find(command.UnidadeId);

                            command.CBO = command.CBO.Split(' ').ElementAt(0);
                            p.Cbo = repository.Cbo.Where(x => x.Codigo == command.CBO).FirstOrDefault();
                            p.CRM = command.CRM;

                            repository.Entry(p).State = EntityState.Modified;
                            repository.SaveChanges();



                            Usuario u = repository.Users.Where(x => x.Profissional.Id == p.Id).FirstOrDefault();
                            u = repository.Users.Include("Roles").Where(x => x.Id == u.Id).FirstOrDefault();
                            u.Nome = p.Nome;
                            u.Email = command.Email;
                            u.UserName = command.UserName;
                            u.Roles.Clear();
                            repository.SaveChanges();

                            foreach (var role in roles)
                            {
                                UserRole userRole = new UserRole { RoleId = role, UserId = u.Id };
                                repository.Entry(userRole).State = EntityState.Added;
                                repository.UserRoles.Add(userRole);
                            }
                            repository.Entry(u).State = EntityState.Modified;
                            repository.SaveChanges();
                            transaction.Commit();
                            return p.Id;
                        }
                    }
                }
                catch (Exception ex)
                {
                    if (ex is DatabaseValidationErrors)
                    {
                        var dbEx = (DatabaseValidationErrors)ex;
                        foreach (var error in dbEx.Errors)
                        {
                            v.AddValidationMessage(new ValidationMessage(error));
                        }

                    }
                    if (ex is GeneralErrors)
                    {
                        foreach (var item in ((GeneralErrors)ex).Errors)
                        {
                            v.AddValidationMessage(new ValidationMessage(item));
                        }
                    }
                    v.AddValidationMessage(new ValidationMessage(ex.Message));
                    return v;
                }
            }
            else
                return v;
        }

        private Validation ValidaProfissional(CreateProfissionalCommand command)
        {
            Validation v = new Validation();

            if (string.IsNullOrWhiteSpace(command.Nome))
                v.AddValidationMessage(new ValidationMessage("Nome é obrigatório!"));
            if (command.UnidadeId == Guid.Empty)
                v.AddValidationMessage(new ValidationMessage("Unidade é obrigatória!"));


            if (!string.IsNullOrWhiteSpace(command.CPF))
            {
                Int64 cpf = 0;
                if (!Int64.TryParse(command.CPF, out cpf))
                    v.AddValidationMessage(new ValidationMessage("CPF inválido!"));
                else if (!ValidaCPF(command.CPF))
                    v.AddValidationMessage(new ValidationMessage("CPF inválido!"));
                //else if (command.ProfissionalId == Guid.Empty && this.repository.GetAll().FirstOrDefault(x => x.CPF == command.CPF) != null)
                //    v.AddValidationMessage(new ValidationMessage("CPF já utilizado!"));
            }

            if (!string.IsNullOrWhiteSpace(command.CNS))
            {
                if (!ChecarCNS(command.CNS))
                    v.AddValidationMessage(new ValidationMessage("CNS inválido!"));
                else
                {
                    if (command.ProfissionalId == Guid.Empty && this.repository.Profissional.FirstOrDefault(x => x.CNS == command.CNS) != null)
                        v.AddValidationMessage(new ValidationMessage("CNS já cadastrado"));
                }
            }
            else
                v.AddValidationMessage(new ValidationMessage("CNS obrigatório!"));

            if (string.IsNullOrWhiteSpace(command.CBO))
                v.AddValidationMessage(new ValidationMessage("CBO obrigatório!"));

            return v;
        }

        private bool ValidaCPF(string cpf)
        {
            int[] multiplicador1 = new int[9] { 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplicador2 = new int[10] { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            string tempCpf;
            string digito;
            int soma;
            int resto;
            cpf = cpf.Trim();
            cpf = cpf.Replace(".", "").Replace("-", "");
            if (cpf.Length != 11)
                return false;
            tempCpf = cpf.Substring(0, 9);
            soma = 0;

            for (int i = 0; i < 9; i++)
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador1[i];
            resto = soma % 11;
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = resto.ToString();
            tempCpf = tempCpf + digito;
            soma = 0;
            for (int i = 0; i < 10; i++)
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador2[i];
            resto = soma % 11;
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = digito + resto.ToString();
            return cpf.EndsWith(digito);
        }

        private bool ChecarCNS(string cns)
        {
            bool result = false;
            cns = cns.Trim();
            if ((cns.Substring(0, 1) == "8") || (cns.Substring(0, 1) == "9"))
            {
                result = checaNumeroProvisorio(cns);
            }
            else
            {
                result = checaNumeroDefinitivo(cns);
            }
            return result;
        }

        private bool checaNumeroDefinitivo(string cns)
        {
            bool result = false;
            try
            {
                cns = cns.Trim();

                if (cns.Trim().Length == 15)
                {
                    float resto, soma;
                    soma = ((Convert.ToInt64(cns.Substring(0, 1))) * 15) +
                            ((Convert.ToInt64(cns.Substring(1, 1))) * 14) +
                            ((Convert.ToInt64(cns.Substring(2, 1))) * 13) +
                            ((Convert.ToInt64(cns.Substring(3, 1))) * 12) +
                            ((Convert.ToInt64(cns.Substring(4, 1))) * 11) +
                            ((Convert.ToInt64(cns.Substring(5, 1))) * 10) +
                            ((Convert.ToInt64(cns.Substring(6, 1))) * 9) +
                            ((Convert.ToInt64(cns.Substring(7, 1))) * 8) +
                            ((Convert.ToInt64(cns.Substring(8, 1))) * 7) +
                            ((Convert.ToInt64(cns.Substring(9, 1))) * 6) +
                            ((Convert.ToInt64(cns.Substring(10, 1))) * 5) +
                            ((Convert.ToInt64(cns.Substring(11, 1))) * 4) +
                            ((Convert.ToInt64(cns.Substring(12, 1))) * 3) +
                            ((Convert.ToInt64(cns.Substring(13, 1))) * 2) +
                            ((Convert.ToInt64(cns.Substring(14, 1))) * 1);

                    resto = soma % 11;

                    result = (resto == 0);
                }

            }
            catch (Exception)
            {
                result = false;
            }

            return result;
        }

        private bool checaNumeroProvisorio(string cns)
        {
            bool result = false;

            try
            {
                if (cns.Trim().Length == 15)
                {

                    float resto, soma, dv;

                    string pis = string.Empty;
                    string resultado = string.Empty;

                    pis = cns.Substring(0, 11);

                    soma = ((Convert.ToInt64(pis.Substring(0, 1))) * 15) +
                            ((Convert.ToInt64(pis.Substring(1, 1))) * 14) +
                            ((Convert.ToInt64(pis.Substring(2, 1))) * 13) +
                            ((Convert.ToInt64(pis.Substring(3, 1))) * 12) +
                            ((Convert.ToInt64(pis.Substring(4, 1))) * 11) +
                            ((Convert.ToInt64(pis.Substring(5, 1))) * 10) +
                            ((Convert.ToInt64(pis.Substring(6, 1))) * 9) +
                            ((Convert.ToInt64(pis.Substring(7, 1))) * 8) +
                            ((Convert.ToInt64(pis.Substring(8, 1))) * 7) +
                            ((Convert.ToInt64(pis.Substring(9, 1))) * 6) +
                            ((Convert.ToInt64(pis.Substring(10, 1))) * 5);
                    resto = soma % 11;
                    dv = 11 - resto;

                    if (dv == 11)
                    {
                        dv = 0;
                    }
                    if (dv == 10)
                    {
                        soma = ((Convert.ToInt64(pis.Substring(0, 1))) * 15) +
                                ((Convert.ToInt64(pis.Substring(1, 1))) * 14) +
                                ((Convert.ToInt64(pis.Substring(2, 1))) * 13) +
                                ((Convert.ToInt64(pis.Substring(3, 1))) * 12) +
                                ((Convert.ToInt64(pis.Substring(4, 1))) * 11) +
                                ((Convert.ToInt64(pis.Substring(5, 1))) * 10) +
                                ((Convert.ToInt64(pis.Substring(6, 1))) * 9) +
                                ((Convert.ToInt64(pis.Substring(7, 1))) * 8) +
                                ((Convert.ToInt64(pis.Substring(8, 1))) * 7) +
                                ((Convert.ToInt64(pis.Substring(9, 1))) * 6) +
                                ((Convert.ToInt64(pis.Substring(10, 1))) * 5) + 2;

                        resto = soma % 11;
                        dv = 11 - resto;
                        resultado = pis + "001" + Convert.ToString(Convert.ToInt16(dv)).Trim();
                    }
                    else
                    {
                        resultado = pis + "000" + Convert.ToString(Convert.ToInt16(dv)).Trim();
                    }


                    result = cns.Equals(resultado);

                }
            }
            catch (Exception)
            {
                result = false;
            }

            return result;
        }

        public void Handle(FirstAcessCommand command)
        {
            try
            {
                using (var transaction = repository.Database.BeginTransaction())
                {
                    Profissional profissional = repository.Profissional.Find(command.ProfissionalId);
                    profissional.Cbo = repository.Cbo.Where(x => x.Codigo == command.ProfissionalCbo).FirstOrDefault();
                    profissional.CPF = command.ProfissionalCpf.Replace(".", "").Replace("-", "");
                    profissional.Unidade = new Unidade();
                    profissional.Unidade.NomeUnidade = command.NomeUnidade;
                    profissional.Unidade.NomeFantasia = command.NomeUnidadeFantasia;
                    profissional.Unidade.DataAcreditacao = DateTime.Now;
                    repository.Entry(profissional.Unidade).State = EntityState.Added;
                    repository.Unidade.Add(profissional.Unidade);
                    repository.Update(profissional);

                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public object HandleWithReturn(FirstAcessCommand command)
        {
            throw new NotImplementedException();
        }

        public void Handle(DeleteProfissionalCommand command)
        {
            throw new NotImplementedException();
        }

        public object HandleWithReturn(DeleteProfissionalCommand command)
        {
            Validation validation = new Validation();
            try
            {
                using (var transaction = repository.Database.BeginTransaction())
                {
                    Profissional Profissional = repository.Profissional.Find(command.ProfissionalId);
                    Profissional.IsDeleted = true;
                    repository.Update(Profissional);
                    return validation;
                }

            }
            catch (Exception ex)
            {
                if (ex is GeneralErrors)
                {
                    foreach (var item in ((GeneralErrors)ex).Errors)
                    {
                        validation.AddValidationMessage(new ValidationMessage(item));
                    }
                }
                validation.AddValidationMessage(new ValidationMessage(ex.Message));
                return validation;
            }
        }

        public void Dispose()
        {
            repository?.Dispose();
        }
    }
}
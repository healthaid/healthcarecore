﻿using AutoMapper;
using HealtCare.Infra.Common.Validations;
using HealthCare.Domain.Entities.DbClasses.Atendimento;
using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.Data.Context;
using HealthCare.Service.CQRS.Write.Commands.CommandsHandlers;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace HealthCare.Infra.CQRS.Write.Commands.CommandsHandlers
{
    enum StatusAcolhimento
    {
        PreCadastro,
        Recepcao
    }
    public class AcolhimentoHandler : ICommandHandler<SaveAcolhimentoCommand> 
    {
        private IMapper mapper;
        private HealthCareDbContext repository;
        private ILogger<AcolhimentoHandler> logger;

        public AcolhimentoHandler(HealthCareDbContext contextFactory, IMapper mapper, ILogger<AcolhimentoHandler> logger )
        {
            this.repository = contextFactory;
            this.mapper = mapper;
            this.logger = logger;
        }

        public void Dispose()
        {
            repository?.Dispose();
        }

        public void Handle(SaveAcolhimentoCommand command)
        {

        }

        public object HandleWithReturn(SaveAcolhimentoCommand command)
        {
            Validation v = ValidarAcolhimento(command);
            if (v.IsValid)
            {
                try
                {
                    
                    using (var transaction = repository.Database.BeginTransaction())
                    {
                        logger.LogInformation("Iniciado save de acolhimento");
                        Acolhimento acolhimento = mapper.Map<Acolhimento>(command);
                        if (command.AcolhimentoId == Guid.Empty)
                        {
                            Guid idPaciente = command.SavePacienteCommand.PacienteId;
                            if (command.SavePacienteCommand.PacienteId == Guid.Empty)
                            {
                                command.SavePacienteCommand.PendenteCadastro = true;
                                command.SavePacienteCommand.VindoAcolhimento = true;
                                /// Cria o mínimo necessário para paciente
                                /// --------------------------------------
                                PacienteHandler pacienteHandler = new PacienteHandler(repository, mapper);

                                /// PACIENTE acolhido fica PENDENTE de cadastro
                                /// ----------------------------------------------------
                                command.SavePacienteCommand.PendenteCadastro = true;

                                object objReturned = pacienteHandler.HandleWithReturn(command.SavePacienteCommand);
                                if (objReturned.GetType() == typeof(Validation))
                                {
                                    return (Validation)objReturned;
                                }
                                else
                                    idPaciente = (Guid)objReturned;
                            }
                            else
                                acolhimento.Paciente = repository.Query<Paciente>().FirstOrDefault(x => x.Id == command.SavePacienteCommand.PacienteId);

                            /// Cria o Acolhimento
                            /// --------------------------------------
                            acolhimento.Status = (int)StatusAcolhimento.PreCadastro;
                            acolhimento.DataHora =  DateTime.Now;
                            acolhimento.Paciente = repository.Paciente.Find(idPaciente);
                            acolhimento.Profissional = repository.Profissional.Find(command.ProfissionalId);
                            if (command.ServicoId != Guid.Empty)
                                acolhimento.Servico = repository.Servico.Find(command.ServicoId);

                            /// Gera o número de boletim
                            /// -------------------------
                            acolhimento.CodigoBoletim = GeraNumeroBoletim(repository);

                            repository.Entry(acolhimento).State = EntityState.Added;
                            repository.Acolhimento.Add(acolhimento);
                            repository.SaveChanges();
                        }
                        else
                        {
                            acolhimento = repository.Acolhimento.Find(command.AcolhimentoId);
                            acolhimento.DataHora = DateTime.Now;
                            acolhimento.Paciente = repository.Paciente.Find(command.SavePacienteCommand.Id);
                            acolhimento.Profissional = repository.Profissional.Find(command.ProfissionalId);
                            acolhimento.Servico = repository.Servico.Find(command.ServicoId);
                            repository.Entry(acolhimento).State = EntityState.Modified;
                            repository.SaveChanges();
                        }
                        transaction.Commit();
                        logger.LogInformation("Terminado save de acolhimento");
                        return acolhimento.Id;
                    }
                    
                }
                catch (Exception ex)
                {
                    if (ex is DatabaseValidationErrors)
                    {
                        var dbEx = (DatabaseValidationErrors)ex;
                        foreach (var error in dbEx.Errors)
                        {
                            v.AddValidationMessage(new ValidationMessage(error));
                        }

                    }
                    if (ex is GeneralErrors)
                    {
                        foreach (var item in ((GeneralErrors)ex).Errors)
                        {
                            v.AddValidationMessage(new ValidationMessage(item));
                        }
                    }
                    v.AddValidationMessage(new ValidationMessage(ex.Message));
                    return v;
                }
            }
            else
                return v;
        }

        private string GeraNumeroBoletim(HealthCareDbContext context)
        {
            int? ultimoCodigo;
            if (context.Acolhimento.Any())
                ultimoCodigo = int.Parse(context.Acolhimento.Max(x => x.CodigoBoletim));
            else
                ultimoCodigo = 0;
            if (ultimoCodigo != null)
                ultimoCodigo = ultimoCodigo + 1;
            else
                ultimoCodigo = 1;

            string codigoPaciente = ultimoCodigo.ToString();
            for (int i = ultimoCodigo.ToString().Length; i < 14; i++)
                codigoPaciente = "0" + codigoPaciente;

            return codigoPaciente;
        }

        private Validation ValidarAcolhimento(SaveAcolhimentoCommand command)
        {
            Validation v = new Validation();

            if (command.SavePacienteCommand == null)
            {
                if (string.IsNullOrWhiteSpace(command.SavePacienteCommand.Nome))
                    v.AddValidationMessage(new ValidationMessage("Paciente é obrigatório"));
            }

            if (command.ServicoId == null)
                v.AddValidationMessage(new ValidationMessage("Serviço é obrigatório"));

            return v;
        }
    }
}

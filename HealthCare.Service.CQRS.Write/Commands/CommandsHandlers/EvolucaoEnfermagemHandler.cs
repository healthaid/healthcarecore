﻿using HealtCare.Infra.Common.Validations;
using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.Data.Context;
using System;
using System.Linq;
using HealthCare.Domain.Entities.DbClasses.Atendimento;
using Microsoft.EntityFrameworkCore;
using AutoMapper;

namespace HealthCare.Infra.CQRS.Write.Commands.CommandsHandlers
{
    public class EvolucaoEnfermagemHandler : ICommandHandler<SaveEvolucaoEnfermagemCommand>
    {
        HealthCareDbContext repository;
        private IMapper mapper;
        public EvolucaoEnfermagemHandler(HealthCareDbContext contextFactory, IMapper mapper)
        {
            this.repository = contextFactory;
            this.mapper = mapper;
        }

        public void Dispose()
        {
            repository?.Dispose();
        }

        public void Handle(SaveEvolucaoEnfermagemCommand command)
        {
            //throw new NotImplementedException();
        }

        public object HandleWithReturn(SaveEvolucaoEnfermagemCommand command)
        {
            Validation v = ValidarEvolucaoEnfermagem(command);
            EvolucaoEnfermagem EvolucaoEnfermagem = mapper.Map<EvolucaoEnfermagem>(command);
            if (v.IsValid)
            {
                try
                {
                    using (var transaction = repository.Database.BeginTransaction())
                    {
                        if (command.EvolucaoEnfermagemId == Guid.Empty)
                        {
                            EvolucaoEnfermagem.Profissional = repository.Profissional.Find(command.ProfissionalId);
                            EvolucaoEnfermagem.MovimentacaoLeito = repository.MovimentacaoLeito.Find(command.MovimentacaoLeitoId);
                            EvolucaoEnfermagem.HoraEvolucao = DateTime.Now;
                            EvolucaoEnfermagem.NumAtendimento = GeraNumAtendimentoEnfermagem(repository);
                            repository.Entry(EvolucaoEnfermagem).State = EntityState.Added;
                            repository.EvolucaoEnfermagem.Add(EvolucaoEnfermagem);
                            repository.SaveChanges();
                        }
                        else
                        {
                            EvolucaoEnfermagem = repository.EvolucaoEnfermagem.Find(command.EvolucaoEnfermagemId);
                            EvolucaoEnfermagem.HoraEvolucao = DateTime.Now;
                            EvolucaoEnfermagem.Evolucao = command.Evolucao;
                            EvolucaoEnfermagem.ObservacaoSinaisVitais = command.ObservacaoSinaisVitais;
                            EvolucaoEnfermagem.Peso = command.Peso;
                            EvolucaoEnfermagem.Pressao = command.Pressao;
                            EvolucaoEnfermagem.Pulso = command.Pulso;
                            EvolucaoEnfermagem.FrequenciaRespitaroria = command.FrequenciaRespitaroria;
                            EvolucaoEnfermagem.Temperatura = command.Temperatura;
                            repository.Entry(EvolucaoEnfermagem).State = EntityState.Modified;
                            repository.SaveChanges();
                        }
                        transaction.Commit();
                        return EvolucaoEnfermagem.Id;
                    }
                }
                catch (Exception ex)
                {
                    if (ex is GeneralErrors)
                    {
                        foreach (var item in ((GeneralErrors)ex).Errors)
                        {
                            v.AddValidationMessage(new ValidationMessage(item));
                        }
                    }
                    v.AddValidationMessage(new ValidationMessage(ex.Message));
                    return v;
                }
            }
            else
                return v;
        }

        private string GeraNumAtendimentoEnfermagem(HealthCareDbContext context)
        {
            int? ultimoCodigo;
            if (context.EvolucaoEnfermagem.Any())
                ultimoCodigo = context.EvolucaoEnfermagem.ToList().Max(x => Convert.ToInt32(x.NumAtendimento));
            else ultimoCodigo = 0;

            if (ultimoCodigo != null)
                ultimoCodigo = ultimoCodigo + 1;
            else
                ultimoCodigo = 1;

            string codigoPaciente = ultimoCodigo.ToString();
            for (int i = ultimoCodigo.ToString().Length; i < 14; i++)
                codigoPaciente = "0" + codigoPaciente;

            return codigoPaciente;
        }

        private Validation ValidarEvolucaoEnfermagem(SaveEvolucaoEnfermagemCommand command)
        {
            Validation v = new Validation();

            //if (string.IsNullOrWhiteSpace(command.Nome))
            //    v.AddValidationMessage(new ValidationMessage("Nome é obrigatório!"));
            //if (command.EnfermariaId == Guid.Empty)
            //    v.AddValidationMessage(new ValidationMessage("Clínica é obrigatória!"));
            //if (command.TipoEvolucaoEnfermagem < 0)
            //    v.AddValidationMessage(new ValidationMessage("Informa o tipo de EvolucaoEnfermagem!"));

            return v;
        }

    }
}
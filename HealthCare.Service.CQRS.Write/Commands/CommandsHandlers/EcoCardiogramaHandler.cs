﻿using HealthCare.Domain.Entities.DbClasses.Atendimento;
using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;

namespace HealthCare.Infra.CQRS.Write.Commands.CommandsHandlers
{
    public class ExameHandler : ICommandHandler<CreateExameCommand>
    {
        private readonly HealthCareDbContext repository;

        public ExameHandler(HealthCareDbContext contextFactory)
        {
            this.repository = contextFactory;
        }

        public void Dispose()
        {
            repository?.Dispose();
        }

        public void Handle(CreateExameCommand command)
        {
            try
            {
                using (var transaction = repository.Database.BeginTransaction())
                {
                    ResultadoExame Exame = new ResultadoExame();
                    Exame.Id = Guid.NewGuid();
                    Exame.Atendimento = repository.Atendimento.Find(command.AtendimentoId);
                    Exame.RowVersion = BitConverter.GetBytes(DateTime.Now.Date.Ticks);
                    repository.Entry(Exame).State = EntityState.Added;
                    repository.ResultadoExame.Add(Exame);
                    repository.SaveChanges();
                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public object HandleWithReturn(CreateExameCommand command)
        {
            throw new NotImplementedException();
        }
    }
}

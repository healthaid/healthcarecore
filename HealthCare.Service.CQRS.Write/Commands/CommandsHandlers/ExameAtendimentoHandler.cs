﻿using HealtCare.Infra.Common.Validations;
using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.Data.Context;
using System;
using System.Linq;
using HealthCare.Domain.Entities.DbClasses.Atendimento;
using Microsoft.EntityFrameworkCore;
using AutoMapper;

namespace HealthCare.Infra.CQRS.Write.Commands.CommandsHandlers
{
    public class ExameAtendimentoHandler : ICommandHandler<SaveExameAtendimentoCommand>, ICommandHandler<DeleteExameAtendimentoCommand>, ICommandHandler<RealizarExameCommand>
    {
        HealthCareDbContext repository;
        private IMapper mapper;

        public ExameAtendimentoHandler(HealthCareDbContext contextFactory, IMapper mapper)
        {
            this.repository = contextFactory;
            this.mapper = mapper;
        }


        public void Handle(SaveExameAtendimentoCommand command)
        {
            //throw new NotImplementedException();
        }

        public object HandleWithReturn(SaveExameAtendimentoCommand command)
        {
            Validation v = ValidarExameAtendimento(command);
            ExameAtendimento ExameAtendimento = mapper.Map<ExameAtendimento>(command);
            if (v.IsValid)
            {
                try
                {
                    using (var transaction = repository.Database.BeginTransaction())
                    {
                        if (command.ExameAtendimentoId == Guid.Empty)
                        {
                            /// Carrega Atendimento
                            /// -------------------
                            ExameAtendimento.Atendimento = repository.Atendimento.Find(command.AtendimentoId);

                            ExameAtendimento.Exame = command.ExameDescritivo;
                            ExameAtendimento.CodigoProcedimento = command.CodigoProcedimento;
                            ExameAtendimento.DataSolicitacao = command.DataSolicitacao;
                            ExameAtendimento.DefinicaoExame = command.DefinicaoExame;
                            ExameAtendimento.Procedimento = command.Procedimento;
                            ExameAtendimento.ObservacaoMedico = command.ObservacaoMedico;
                            ExameAtendimento.Realizado = false;
                            

                            if (command.SolicitanteId != Guid.Empty)
                                ExameAtendimento.Medico = repository.Profissional.Find(command.SolicitanteId);

                            /// Insere ExameAtendimento
                            /// ----------------------------
                            repository.Entry(ExameAtendimento).State = EntityState.Added;
                            repository.ExameAtendimento.Add(ExameAtendimento);
                            repository.SaveChanges();
                        }
                        else
                        {
                            ExameAtendimento = repository.ExameAtendimento.Find(command.ExameAtendimentoId);
                            ExameAtendimento.DataExecucao = DateTime.Now;
                            if (command.ExecutorId != Guid.Empty)
                            {
                                ExameAtendimento.Executor = repository.Profissional.Find(command.ExecutorId);
                                ExameAtendimento.Realizado = true;
                            }
                            else
                            {
                                ExameAtendimento.ObservacaoExecutor = command.ObservacaoExecutor;
                                ExameAtendimento.ObservacaoMedico = command.ObservacaoMedico;
                                ExameAtendimento.Procedimento = command.Procedimento;
                            }

                            if (command.SolicitanteId != Guid.Empty)
                                ExameAtendimento.Medico = repository.Profissional.Find(command.SolicitanteId);

                            repository.Entry(ExameAtendimento).State = EntityState.Modified;
                            repository.SaveChanges();
                        }
                        transaction.Commit();
                    }
                    return ExameAtendimento.Id;
                }
                catch (Exception ex)
                {
                    if (ex is GeneralErrors)
                    {
                        foreach (var item in ((GeneralErrors)ex).Errors)
                        {
                            v.AddValidationMessage(new ValidationMessage(item));
                        }
                    }
                    v.AddValidationMessage(new ValidationMessage(ex.Message));
                    return v;
                }
            }
            else
                return v;
        }

        private Validation ValidarExameAtendimento(SaveExameAtendimentoCommand command)
        {
            Validation v = new Validation();
            if (string.IsNullOrWhiteSpace(command.ExameDescritivo))
                v.AddValidationMessage(new ValidationMessage("Exame é obrigatório!"));

            return v;
        }


        public void Handle(DeleteExameAtendimentoCommand command)
        {
        }

        public object HandleWithReturn(DeleteExameAtendimentoCommand command)
        {
            Validation v = new Validation();
            try
            {
                using (var transaction = repository.Database.BeginTransaction())
                {
                    if (command.ExameAtendimentoId == Guid.Empty)
                    {
                        v.AddValidationMessage(new ValidationMessage("Nenhum id informado para exclusão!"));
                        transaction.Commit();
                    }
                    else
                    {
                        var entity = repository.ExameAtendimento.Find(command.ExameAtendimentoId);
                        repository.Entry(entity).State = EntityState.Deleted;
                        repository.SaveChanges();
                        transaction.Commit();
                        return entity.Id;
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex is GeneralErrors)
                {
                    foreach (var item in ((GeneralErrors)ex).Errors)
                    {
                        v.AddValidationMessage(new ValidationMessage(item));
                    }
                }
                v.AddValidationMessage(new ValidationMessage(ex.Message));
            }
            return v;
        }

        public void Handle(RealizarExameCommand command)
        {

        }

        public object HandleWithReturn(RealizarExameCommand command)
        {
            Validation v = new Validation();
            if (command.ExameAtendimentoId.Equals(Guid.Empty))
            {
                v.AddValidationMessage(new ValidationMessage("Selecione um item no grid!"));
                return v;
            }
            else
            {
                try
                {
                    using (var transaction = repository.Database.BeginTransaction())
                    {
                        ExameAtendimento ExameAtendimento = repository.ExameAtendimento.Find(command.ExameAtendimentoId);
                        ExameAtendimento.DataExecucao = DateTime.Now;
                        ExameAtendimento.Executor = repository.Profissional.Find(command.ExecutorId);
                        ExameAtendimento.Realizado = command.Realizado;
                        ExameAtendimento.ObservacaoExecutor = command.ObservacaoExecutor;
                        repository.Entry(ExameAtendimento).State = EntityState.Modified;
                        repository.SaveChanges();
                        transaction.Commit();
                        return command.ExameAtendimentoId;
                    }
                }
                catch (Exception ex)
                {
                    if (ex is GeneralErrors)
                    {
                        foreach (var item in ((GeneralErrors)ex).Errors)
                        {
                            v.AddValidationMessage(new ValidationMessage(item));
                        }
                    }
                    v.AddValidationMessage(new ValidationMessage(ex.Message));
                    return v;
                }
            }
        }

        public void Dispose()
        {
            repository?.Dispose();
        }
    }
}
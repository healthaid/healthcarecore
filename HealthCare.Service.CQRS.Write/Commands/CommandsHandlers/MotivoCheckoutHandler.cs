﻿using AutoMapper;
using HealtCare.Infra.Common.Validations;
using HealthCare.Domain.Entities.DbClasses.Atendimento;
using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace HealthCare.Infra.CQRS.Write.Commands.CommandsHandlers
{
    public class MotivoCheckoutHandler : ICommandHandler<SaveMotivoCheckoutCommand>, ICommandHandler<DeleteMotivoCheckoutCommand>
    {
        HealthCareDbContext repository;
        private IMapper mapper;

        public MotivoCheckoutHandler(HealthCareDbContext contextFactory, IMapper mapper)
        {
            this.repository = contextFactory;
            this.mapper = mapper;
        }

        public void Handle(SaveMotivoCheckoutCommand command)
        {

        }
        public object HandleWithReturn(SaveMotivoCheckoutCommand command)
        {
            Validation v = new Validation();

            if (string.IsNullOrWhiteSpace(command.Descricao))
                v.AddValidationMessage(new ValidationMessage("Descrição é obrigatório"));

            if (v.IsValid)
            {
                try
                {
                    using (var transaction = repository.Database.BeginTransaction())
                    {
                        MotivoCheckout checkout = mapper.Map<MotivoCheckout>(command);
                        checkout.Id = Guid.NewGuid();
                        if (command.MotivoCheckoutId == Guid.Empty)
                        {
                            checkout.Tipo = repository.TipoMotivoChekout.FirstOrDefault(x => x.Id == command.TipoMotivoCheckoutId);

                            repository.Entry(checkout).State = EntityState.Added;
                            repository.MotivoCheckout.Add(checkout);
                            repository.SaveChanges();
                        }
                        else
                        {
                            checkout = repository.MotivoCheckout.FirstOrDefault(x => x.Id == command.MotivoCheckoutId);
                            checkout.Tipo = repository.TipoMotivoChekout.FirstOrDefault(x => x.Id == command.TipoMotivoCheckoutId);
                            checkout.Descricao = command.Descricao;
                            checkout.Ativo = command.Ativo;

                            repository.Entry(checkout).State = EntityState.Modified;
                            repository.SaveChanges();
                        }

                        transaction.Commit();

                        return command;
                    }

                }
                catch (Exception ex)
                {
                    if (ex is GeneralErrors)
                    {
                        foreach (var item in ((GeneralErrors)ex).Errors)
                        {
                            v.AddValidationMessage(new ValidationMessage(item));
                        }
                    }
                    v.AddValidationMessage(new ValidationMessage(ex.Message));
                    return v;
                }
            }
            else
                return v;
        }

        public void Handle(DeleteMotivoCheckoutCommand command)
        {

        }

        public object HandleWithReturn(DeleteMotivoCheckoutCommand command)
        {
            Validation v = new Validation();

            if (v.IsValid)
            {
                try
                {

                    using (var transaction = repository.Database.BeginTransaction())
                    {
                        MotivoCheckout mc = repository.MotivoCheckout.FirstOrDefault(x => x.Id == command.MotivoCheckoutId);
                        repository.Entry(mc).State = EntityState.Deleted;
                        repository.SaveChanges();

                        transaction.Commit();
                        return command;
                    }

                }
                catch (Exception ex)
                {
                    if (ex is GeneralErrors)
                    {
                        foreach (var item in ((GeneralErrors)ex).Errors)
                        {
                            v.AddValidationMessage(new ValidationMessage(item));
                        }
                    }
                    v.AddValidationMessage(new ValidationMessage(ex.Message));
                    return v;
                }
            }
            else
                return v;
        }

        public void Dispose()
        {
            repository?.Dispose();
        }
    }
}

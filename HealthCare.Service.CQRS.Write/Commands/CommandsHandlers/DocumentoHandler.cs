﻿using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.Data.Context;
using System;

namespace HealthCare.Infra.CQRS.Write.Commands.CommandsHandlers
{
    public class DocumentoHandler : ICommandHandler<CreateDocumentoCommand>, ICommandHandler<UpdateDocumentoCommand>
    {
        private readonly HealthCareDbContext context;
        public DocumentoHandler(HealthCareDbContext contextFactory)
        {
            context = contextFactory;
        }

        public void Dispose()
        {
            context?.Dispose();
        }

        public void Handle(CreateDocumentoCommand command)
        {
            //Documento Documento = new Documento();
            //Documento.Id = Guid.NewGuid();
            //Documento.Valor = command.Valor;
            //Documento.TipoDocumento = command.TipoDocumento;
            //Documento.Paciente = repositoryPaciente.Get(command.PacienteId);

            //Documento.RowVersion = BitConverter.GetBytes(DateTime.Now.Date.Ticks);

            //this.repository.Insert(Documento);
        }

        public void Handle(UpdateDocumentoCommand command)
        {
            //Documento Documento = repository.Get(command.Id);
            //Documento.Valor = command.Valor;
            //Documento.TipoDocumento = command.TipoDocumento;
            //Documento.Paciente = repositoryPaciente.Get(command.PacienteId);

            //Documento.RowVersion = BitConverter.GetBytes(DateTime.Now.Date.Ticks);

            //this.repository.Update(Documento);
        }

        public object HandleWithReturn(UpdateDocumentoCommand command)
        {
            throw new NotImplementedException();
        }

        public object HandleWithReturn(CreateDocumentoCommand command)
        {
            throw new NotImplementedException();
        }
    }
}

﻿
using HealthCare.Data.Repositories.Implementations.Cadastro;
using HealthCare.Domain.Entities.Cadastro;
using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Infra.CQRS.Messaging;
using System;
namespace HealthCare.Infra.CQRS.Write.Commands.CommandsHandlers
{
    public class PlanoHandler :  ICommandHandler<CreatePlanoCommand>, ICommandHandler<UpdatePlanoCommand>
    {

        private readonly PlanoRepository repository;

        public PlanoHandler(PlanoRepository repositorio)
        {
            this.repository = repositorio;
        }

        public PlanoHandler()
        {

        }   

        public void Handle(CreatePlanoCommand command)
        {
            Plano p = new Plano();
            p.Nome = command.Nome;
            p.Ativo = command.Ativo;
            p.RowVersion = BitConverter.GetBytes(DateTime.Now.Date.Ticks);
            this.repository.Insert(p);
        }

        public void Handle(UpdatePlanoCommand command)
        {
            Plano p = repository.Get(command.Id);
            p.Nome = command.Nome;
            p.Ativo = command.Ativo;
            p.RowVersion = BitConverter.GetBytes(DateTime.Now.Date.Ticks);
            this.repository.Insert(p);
        }

        public object HandleWithReturn(CreatePlanoCommand command)
        {
            throw new NotImplementedException();
        }

        public object HandleWithReturn(UpdatePlanoCommand command)
        {
            throw new NotImplementedException();
        }

    }
}

﻿using HealthCare.Domain.Entities.DbClasses.Atendimento;
using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HealthCare.Infra.CQRS.Write.Commands.CommandsHandlers
{
    public class AtestadoHandler : ICommandHandler<CreateAtestadoCommand>, ICommandHandler<UpdateAtestadoCommand>
    {
        private readonly HealthCareDbContext repository;

        public AtestadoHandler(HealthCareDbContext contextFactory)
        {
            this.repository = contextFactory;
        }

        public void Dispose()
        {
            repository?.Dispose();
        }

        public void Handle(CreateAtestadoCommand command)
        {
            try
            {
                using (var transaction = repository.Database.BeginTransaction())
                {
                    Atestados Atestado = new Atestados();

                    Atestado.Atendimento = repository.Atendimento.Find(command.AtendimentoId);
                    Atestado.Descricao = command.Descricao;
                    Atestado.CID = repository.CID.Where(x => x.Codigo == command.CodigoCid).FirstOrDefault();
                    Atestado.Template = new List<TemplatesAtestado>();
                    Atestado.Dias = command.Dias;
                    repository.Atestados.Add(Atestado);
                    repository.SaveChanges();
                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public void Handle(UpdateAtestadoCommand command)
        {
            try
            {
                using (var transaction = repository.Database.BeginTransaction())
                {
                    Atestados Atestado = repository.Atestados.Find(command.AtestadoId);

                    Atestado.Descricao = command.Descricao;
                    Atestado.CID = repository.CID.Where(x => x.Codigo == command.CodigoCid).FirstOrDefault();
                    Atestado.Dias = command.Dias;

                    Atestado.Template = new List<TemplatesAtestado>();
                    Atestado.Template.Add(repository.TemplatesAtestado.Find(command.TemplateId));
                    repository.Entry(Atestado).State = EntityState.Modified;
                    repository.SaveChanges();
                    transaction.Commit();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }



        public object HandleWithReturn(CreateAtestadoCommand command)
        {
            throw new NotImplementedException();
        }

        public object HandleWithReturn(UpdateAtestadoCommand command)
        {
            throw new NotImplementedException();
        }
    }
}

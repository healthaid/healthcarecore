﻿using HealtCare.Infra.Common.Validations;
using HealthCare.Domain.Entities.DbClasses.Atendimento;
using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace HealthCare.Infra.CQRS.Write.Commands.CommandsHandlers
{
    public class EvolucaoHandler : ICommandHandler<SaveEvolucaoCommand>
    {
        HealthCareDbContext repository;

        public EvolucaoHandler(HealthCareDbContext contextFactory)
        {
            this.repository = contextFactory;
        }

        public void Dispose()
        {
            repository?.Dispose();
        }

        public void Handle(SaveEvolucaoCommand command)
        {
            //throw new NotImplementedException();
        }

        public object HandleWithReturn(SaveEvolucaoCommand command)
        {
            Validation v = ValidarEvolucao(command);
            Evolucao evolucao = new Evolucao();
            if (v.IsValid)
            {
                try
                {
                    using (var transaction = repository.Database.BeginTransaction())
                    {
                        if (command.EvolucaoId == Guid.Empty)
                        {
                            evolucao.Atemdimento = repository.Atendimento.Find(command.AtendimentoId);
                            evolucao.Medico = repository.Profissional.Find(command.ProfissionalId);
                            evolucao.DataHoraEvolucao = command.DataHoraEvolucao;
                            evolucao.Descricao = command.Descricao;
                            if (!string.IsNullOrWhiteSpace(command.DiagnosticoUm))
                                evolucao.PrimeiroDiagnostico = repository.Query<CID>().FirstOrDefault(x => x.Codigo == command.DiagnosticoUm);
                            if (!string.IsNullOrWhiteSpace(command.DiagnosticoDois))
                                evolucao.SegundoDiagnostico = repository.Query<CID>().FirstOrDefault(x => x.Codigo == command.DiagnosticoDois);

                            repository.Entry(evolucao).State = EntityState.Added;
                            repository.Evolucao.Add(evolucao);
                            /// Obtém os procedimentos
                            /// ----------------------------------------------------------------------------------------------------------
                            evolucao.EvolucaoProcedimentos = new System.Collections.Generic.List<EvolucaoProcedimento>();
                            foreach (var procedimento in command.Procedimentos)
                            {
                                string codigo = procedimento.Split(' ')[0];
                                var procEntity = repository.Procedimento.FirstOrDefault(x => x.CO_PROCEDIMENTO == codigo);
                                evolucao.EvolucaoProcedimentos.Add(new EvolucaoProcedimento { EvolucaoId = evolucao.Id, ProcedimentoId = procEntity.Id });
                            }
                            repository.SaveChanges();

                        }
                        else
                        {
                            evolucao = repository.Evolucao.Find(command.EvolucaoId);
                            evolucao.DataHoraEvolucao = command.DataHoraEvolucao;
                            evolucao.Descricao = command.Descricao;
                            evolucao.Quantidade = command.Quantidade;
                            //evolucao.Procedimento = repositoryProcedimento.GetProcedimentoByCodigo(command.ProcedimentoId);
                            evolucao.Atemdimento = repository.Atendimento.Find(command.AtendimentoId);
                            evolucao.Medico = repository.Profissional.Find(command.ProfissionalId);
                            repository.Entry(evolucao).State = EntityState.Modified;
                            repository.SaveChanges();
                        }
                        transaction.Commit();
                        return evolucao.Id;
                    }
                }
                catch (Exception ex)
                {
                    if (ex is GeneralErrors)
                    {
                        foreach (var item in ((GeneralErrors)ex).Errors)
                        {
                            v.AddValidationMessage(new ValidationMessage(item));
                        }
                    }
                    v.AddValidationMessage(new ValidationMessage(ex.Message));
                    return v;
                }
            }
            else
                return v;
        }

        private Validation ValidarEvolucao(SaveEvolucaoCommand command)
        {
            Validation v = new Validation();

            //if (string.IsNullOrWhiteSpace(command.Nome))
            //    v.AddValidationMessage(new ValidationMessage("Nome é obrigatório!"));
            //if (command.EnfermariaId == Guid.Empty)
            //    v.AddValidationMessage(new ValidationMessage("Clínica é obrigatória!"));
            //if (command.TipoEvolucao < 0)
            //    v.AddValidationMessage(new ValidationMessage("Informa o tipo de Evolucao!"));

            return v;
        }

    }
}
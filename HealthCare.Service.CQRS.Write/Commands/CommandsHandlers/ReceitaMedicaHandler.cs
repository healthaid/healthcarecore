﻿
using AutoMapper;
using HealtCare.Infra.Common.Validations;
using HealthCare.Domain.Entities.DbClasses.Atendimento;
using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace HealthCare.Infra.CQRS.Write.Commands.CommandsHandlers
{
    public class ReceitaMedicaHandler : ICommandHandler<SaveReceitaMedicaCommand>, ICommandHandler<DeleteReceitaMedicaCommand>
    {
        HealthCareDbContext repository;
        private IMapper mapper;
        public ReceitaMedicaHandler(HealthCareDbContext contextFacotory, IMapper mapper)
        {
            this.repository = contextFacotory;
            this.mapper = mapper;
        }


        public void Handle(SaveReceitaMedicaCommand command)
        {
            //throw new NotImplementedException();
        }

        public object HandleWithReturn(SaveReceitaMedicaCommand command)
        {
            Validation v = ValidarReceitaMedica(command);

            ReceitaMedica rm = mapper.Map<ReceitaMedica>(command);

            if (v.IsValid)
            {
                try
                {

                    using (var transaction = repository.Database.BeginTransaction())
                    {
                        if (command.ReceitaMedicaId == Guid.Empty)
                        {
                            /// Carrega Atendimento
                            /// -------------------
                            rm.Atendimento = repository.Atendimento.FirstOrDefault(x => x.Id == command.AtendimentoId);

                            /// Insere Receita Médica
                            /// ----------------------------
                            repository.Entry(rm).State = EntityState.Added;
                            repository.ReceitaMedica.Add(rm);
                            repository.SaveChanges();
                        }
                        else
                        {
                            rm = repository.ReceitaMedica.FirstOrDefault(x => x.Id == command.ReceitaMedicaId);
                            rm.Dose = command.Dose;
                            rm.Duracao = command.Duracao;
                            rm.Intervalo = command.Intervalo;
                            rm.Medicamento = command.Medicamento;
                            rm.Unidade = command.Unidade;
                            rm.ViaAdministracao = command.ViaAdministracao;
                            repository.Entry(rm).State = EntityState.Modified;
                            repository.SaveChanges();
                        }

                        transaction.Commit();
                        return rm.Id;
                    }
                }
                catch (Exception ex)
                {
                    if (ex is GeneralErrors)
                    {
                        foreach (var item in ((GeneralErrors)ex).Errors)
                        {
                            v.AddValidationMessage(new ValidationMessage(item));
                        }
                    }
                    v.AddValidationMessage(new ValidationMessage(ex.Message));
                    return v;
                }
            }
            else
                return v;
        }

        private Validation ValidarReceitaMedica(SaveReceitaMedicaCommand command)
        {
            Validation v = new Validation();
            if (string.IsNullOrWhiteSpace(command.Medicamento))
                v.AddValidationMessage(new ValidationMessage("Medicamento é obrigatório!"));
            if (string.IsNullOrWhiteSpace(command.Dose))
                v.AddValidationMessage(new ValidationMessage("Dose é obrigatória!"));
            if (string.IsNullOrWhiteSpace(command.Duracao))
                v.AddValidationMessage(new ValidationMessage("Duracao é obrigatório!"));
            if (string.IsNullOrWhiteSpace(command.Intervalo))
                v.AddValidationMessage(new ValidationMessage("Intervalo é obrigatória!"));
            if (string.IsNullOrWhiteSpace(command.Unidade))
                v.AddValidationMessage(new ValidationMessage("Unidade é obrigatório!"));
            if (string.IsNullOrWhiteSpace(command.ViaAdministracao))
                v.AddValidationMessage(new ValidationMessage("Via de administração é obrigatória!"));

            return v;

        }


        public void Handle(DeleteReceitaMedicaCommand command)
        {
        }

        public object HandleWithReturn(DeleteReceitaMedicaCommand command)
        {
            Validation v = new Validation();
            try
            {
                using (var transaction = repository.Database.BeginTransaction())
                {
                    ReceitaMedica rm = new ReceitaMedica();
                    if (command.DeleteId == Guid.Empty)
                        v.AddValidationMessage(new ValidationMessage("Nenhum id informado para exclusão!"));
                    else
                    {
                        rm = repository.ReceitaMedica.FirstOrDefault(x => x.Id == command.DeleteId);
                        repository.Entry(rm).State = EntityState.Deleted;
                        repository.SaveChanges();
                    }
                    transaction.Commit();
                    return rm.Id;
                }
            }
            catch (Exception ex)
            {
                v.AddValidationMessage(new ValidationMessage(ex.Message));
            }
            return v;
        }

        public void Dispose()
        {
            repository?.Dispose();
        }
    }
}
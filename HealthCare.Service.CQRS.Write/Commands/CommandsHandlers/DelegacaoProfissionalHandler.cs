﻿using AutoMapper;
using HealtCare.Infra.Common.Validations;
using HealthCare.Domain.Entities.DbClasses.Atendimento;
using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.Data.Context;
using HealthCare.Service.CQRS.Write.Commands.CommandsHandlers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace HealthCare.Service.CQRS.Write.Commands.CommandsHandlers
{
    public class DelegacaoProfissionalHandler : ICommandHandler<SaveDelegacaoProfissionalCommand>
    {
        IMapper mapper;
        HealthCareDbContext repository;
        public DelegacaoProfissionalHandler(HealthCareDbContext contextFactory, IMapper mapper)
        {
            this.repository = contextFactory;
            this.mapper = mapper;
        }

        public void Dispose()
        {
            repository?.Dispose();
        }

        public void Handle(SaveDelegacaoProfissionalCommand command)
        {

        }

        public object HandleWithReturn(SaveDelegacaoProfissionalCommand command)
        {
            Validation v = ValidarDelegacao(command);
            if (v.IsValid)
            {
                try
                {
                    using (var transaction = repository.Database.BeginTransaction())
                    {
                        DelegacaoProfissional delegacao = mapper.Map<DelegacaoProfissional>(command);
                        if (command.DelegacaoProfissionalId == Guid.Empty)
                        {
                            delegacao.Delegador = repository.Users.Include("Profissional").FirstOrDefault(x => x.Profissional.Id == command.DelegadorId);
                            delegacao.Delegado = repository.Users.Include("Profissional").FirstOrDefault(x => x.Profissional.Id == command.DelegadoId);

                            var delegacaoExistente = repository.DelegacaoProfissional.Include("Delegador").Include("Delegado").FirstOrDefault(x => x.Delegador.Id == delegacao.Delegador.Id && x.Delegado.Id == delegacao.Delegado.Id && x.DataDestituicao == null);
                            if (delegacaoExistente != null)
                            {
                                v.AddValidationMessage(new ValidationMessage("Esta delegação se encontra ativa!"));
                                return v;
                            }

                            delegacao.DataDelegacao = DateTime.Now;
                            delegacao.DataDestituicao = null;

                            repository.Entry(delegacao).State = EntityState.Added;
                            repository.DelegacaoProfissional.Add(delegacao);
                            repository.SaveChanges();
                        }
                        else
                        {
                            delegacao = repository.DelegacaoProfissional.FirstOrDefault(x => x.Id == command.DelegacaoProfissionalId);
                            delegacao.DataDestituicao = DateTime.Now;

                            repository.Entry(delegacao).State = EntityState.Modified;
                            repository.SaveChanges();
                        }
                        transaction.Commit();
                        return delegacao.Id;
                    }
                }
                catch (Exception ex)
                {
                    if (ex is DatabaseValidationErrors)
                    {
                        var dbEx = (DatabaseValidationErrors)ex;
                        foreach (var error in dbEx.Errors)
                        {
                            v.AddValidationMessage(new ValidationMessage(error));
                        }

                    }
                    if (ex is GeneralErrors)
                    {
                        foreach (var item in ((GeneralErrors)ex).Errors)
                        {
                            v.AddValidationMessage(new ValidationMessage(item));
                        }
                    }
                    v.AddValidationMessage(new ValidationMessage(ex.Message));
                    return v;
                }
            }
            else
                return v;
        }

        private Validation ValidarDelegacao(SaveDelegacaoProfissionalCommand command)
        {
            Validation validation = new Validation();

            if (command.DelegacaoProfissionalId == Guid.Empty)
            {
                if (command.DelegadoId == Guid.Empty)
                    validation.AddValidationMessage(new ValidationMessage("Informe o Profissional Delegador!"));
                if (command.DelegadorId == Guid.Empty)
                    validation.AddValidationMessage(new ValidationMessage("Informe o Profissional Delegado!"));
            }

            return validation;
        }
    }

}
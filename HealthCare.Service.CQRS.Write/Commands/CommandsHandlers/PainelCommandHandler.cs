﻿using AutoMapper;
using HealtCare.Infra.Common.Utils;
using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace HealthCare.Infra.CQRS.Write.Commands.CommandsHandlers
{
    public class PainelCommandHandler : ICommandHandler<InsertItemPainelCommand>, ICommandHandler<UpdateItensPainelCommand>
    {
        HealthCareDbContext repository;
        public PainelCommandHandler(HealthCareDbContext contextFactory, IMapper mapper)
        {
            this.repository = contextFactory;
            ViewModelUtils.mapper = mapper;
        }

        public void Dispose()
        {
            repository?.Dispose();
        }

        public void Handle(UpdateItensPainelCommand command)
        {
            try
            {
                using (var transaction = repository.Database.BeginTransaction())
                {
                    foreach (Guid guid in command.Ids)
                    {
                        Painel p = repository.Painel.FirstOrDefault(x => x.Id == guid);
                        p.Atendido = true;

                        repository.Entry(p).State = EntityState.Modified;
                        repository.SaveChanges();

                    }
                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Handle(InsertItemPainelCommand command)
        {
            try
            {
                using (var transaction = repository.Database.BeginTransaction())
                {
                    Painel painel = ViewModelUtils.ToModel(command);

                    painel.DataInsercao = DateTime.Now;
                    painel.Atendido = false;

                    repository.Entry(painel).State = EntityState.Added;
                    repository.Painel.Add(painel);
                    repository.SaveChanges();

                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public object HandleWithReturn(UpdateItensPainelCommand command)
        {
            throw new NotImplementedException();
        }

        public object HandleWithReturn(InsertItemPainelCommand command)
        {
            throw new NotImplementedException();
        }
    }
}

﻿

using HealthCare.Data.Repositories.Implementations.Atendimento;
using HealthCare.Domain.Entities.Atendimento;
using HealthCare.Infra.CQRS.Messaging;
using System;
using System.Collections.Generic;

namespace HealthCare.Infra.CQRS.Write.Commands.CommandsHandlers
{
    public class ImagemAtendimentoHandler : ICommandHandler<CreateImagemAtendimentoCommand> , ICommandHandler<UpdateImagemAtendimentoCommand>
    {
        
        private readonly ImagemAtendimentoRepository repository;
        private readonly AtendimentoRepository repositoryAtendimento;
        private readonly ImagemRepository repositoryImagem;

        public ImagemAtendimentoHandler(ImagemAtendimentoRepository repository, AtendimentoRepository repositoryAtendimento, ImagemRepository repositoryImagem)
        {
            this.repository = repository;
            this.repositoryAtendimento = repositoryAtendimento;
            this.repositoryImagem = repositoryImagem;
        }

        public void Handle(CreateImagemAtendimentoCommand command)
        {
            ImagemAtendimento ImagemAtendimento = new ImagemAtendimento();
            ImagemAtendimento.Observacao = command.Observacao;
            ImagemAtendimento.Atendimento = repositoryAtendimento.Get(command.AtendimentoId);
            if (command.Imagens.Count > 0)
                ImagemAtendimento.Imagens = new List<Imagem>();

            foreach (Byte[] Imagem in command.Imagens)
            {
                Imagem novaImagem = new Domain.Entities.Atendimento.Imagem();
                novaImagem.Image = Imagem;
                ImagemAtendimento.Imagens.Add(novaImagem);
            }
            this.repository.Insert(ImagemAtendimento);
        }

        public void Handle(UpdateImagemAtendimentoCommand command)
        {
            ImagemAtendimento ImagemAtendimento = repository.Get(command.Id);
            ImagemAtendimento.Observacao = command.Observacao;
            ImagemAtendimento.Atendimento = repositoryAtendimento.Get(command.AtendimentoId);

            if (command.Imagens.Count > 0)
                ImagemAtendimento.Imagens = new List<Imagem>();

            var imagens = repositoryImagem.Find(x => x.ImagemAtendimento.Id == command.Id);
            foreach (var imagem in imagens)
                this.repositoryImagem.Delete(imagem.Id);

            foreach (Byte[] Imagem in command.Imagens)
            {
                Imagem novaImagem = new Domain.Entities.Atendimento.Imagem();
                novaImagem.Image = Imagem;
                ImagemAtendimento.Imagens.Add(novaImagem);
            }
            this.repository.Update(ImagemAtendimento);
        }

        public object HandleWithReturn(CreateImagemAtendimentoCommand command)
        {
            throw new System.NotImplementedException();
        }

        //public void Handle(UpdateImagemAtendimentoCommand command)
        //{
        //    ImagemAtendimento ImagemAtendimento = repository.Get(command.Id);
        //    ImagemAtendimento.Observacao = command.Observacao;
        //    ImagemAtendimento.Atendimento = repositoryAtendimento.Get(command.AtendimentoId);
            
        //    if (command.Imagems.Count > 0)
        //        ImagemAtendimento.Imagems = new List<Imagem>();

        //    foreach (Guid Imagem in command.Imagems)
        //        ImagemAtendimento.Imagems.Add(repositoryImagem.Get(Imagem));

        //    this.repository.Update(ImagemAtendimento);
        //}

        //public Validation HandleWithReturn(UpdateImagemAtendimentoCommand command)
        //{
        //    throw new NotImplementedException();
        //}


        public object HandleWithReturn(UpdateImagemAtendimentoCommand command)
        {
            throw new NotImplementedException();
        }
    }
}

﻿using HealtCare.Infra.Common.Validations;
using HealthCare.Infra.CQRS.Messaging;
using System;

namespace HealthCare.Infra.CQRS.Write.Commands.CommandsHandlers
{
    public class SetorClinicaHandler : ICommandHandler<SaveSetorClinicaCommand>, ICommandHandler<DeleteSetorClinicaCommand>
            

        public SetorClinicaHandler(SetorClinicaRepository _repository)
        {
            this.repository = _repository;
        }
        public void Handle(SaveSetorClinicaCommand command)
        {

        }
        public object HandleWithReturn(SaveSetorClinicaCommand command)
        {
            Validation v = new Validation();

            if (string.IsNullOrWhiteSpace(command.Descricao))
                v.AddValidationMessage(new ValidationMessage("Descrição é obrigatório"));

            if (v.IsValid)
            {
                try
                {
                    SetorClinica checkout = AutoMapper.Mapper.Map<SetorClinica>(command);
                    if (command.SetorClinicaId == Guid.Empty)
                    {
                        this.repository.Insert(checkout);
                    }
                    else
                    {
                        checkout = this.repository.Get(command.SetorClinicaId);
                        checkout.Descricao = command.Descricao;
                        checkout.Ativo = command.Ativo;

                        this.repository.Update(checkout);
                    }

                    return command;
                }
                catch (Exception ex)
                {
                    if (ex is DbEntityValidationException)
                    {
                        var dbEx = (DbEntityValidationException)ex;
                        foreach (var validationErrors in dbEx.EntityValidationErrors)
                        {
                            foreach (var validationError in validationErrors.ValidationErrors)
                            {
                                v.AddValidationMessage(new ValidationMessage(string.Format("Property: {0} Error: {1}",
                                                        validationError.PropertyName,
                                                        validationError.ErrorMessage)));
                            }
                        }
                    }
                    if (ex is GeneralErrors)
                    {
                        foreach (var item in ((GeneralErrors)ex).Errors)
                        {
                            v.AddValidationMessage(new ValidationMessage(item));
                        }
                    }
                    v.AddValidationMessage(new ValidationMessage(ex.Message));
                    return v;
                }
            }
            else
                return v;
        }

        public void Handle(DeleteSetorClinicaCommand command)
        {

        }

        public object HandleWithReturn(DeleteSetorClinicaCommand command)
        {
            Validation v = new Validation();

            if (v.IsValid)
            {
                try
                {
                    this.repository.Delete(command.SetorClinicaId);
                    return command;
                }
                catch (Exception ex)
                {
                    if (ex is DbEntityValidationException)
                    {
                        var dbEx = (DbEntityValidationException)ex;
                        foreach (var validationErrors in dbEx.EntityValidationErrors)
                        {
                            foreach (var validationError in validationErrors.ValidationErrors)
                            {
                                v.AddValidationMessage(new ValidationMessage(string.Format("Property: {0} Error: {1}",
                                                        validationError.PropertyName,
                                                        validationError.ErrorMessage)));
                            }
                        }
                    }
                    if (ex is GeneralErrors)
                    {
                        foreach (var item in ((GeneralErrors)ex).Errors)
                        {
                            v.AddValidationMessage(new ValidationMessage(item));
                        }
                    }
                    v.AddValidationMessage(new ValidationMessage(ex.Message));
                    return v;
                }
            }
            else
                return v;
        }
    }
}

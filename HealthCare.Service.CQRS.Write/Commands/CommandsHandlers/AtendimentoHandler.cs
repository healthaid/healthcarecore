﻿using HealtCare.Infra.Common.Validations;
using HealthCare.Domain.Entities.DbClasses.Atendimento;
using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace HealthCare.Infra.CQRS.Write.Commands.CommandsHandlers
{
    public class AtendimentoHandler : ICommandHandler<CreateAtendimentoCommand>,
                                      ICommandHandler<UpdateAtendimentoCommand>,
                                      ICommandHandler<AddProcedimentoAtendimentoCommand>,
                                      ICommandHandler<DeleteAtendimentoProcedimentoCommand>
    {

        private readonly HealthCareDbContext repository;
        private readonly ILogger<AtendimentoHandler> logger;
        public AtendimentoHandler(HealthCareDbContext contextFactory, ILogger<AtendimentoHandler> logger)
        {
            this.repository = contextFactory;
            this.logger = logger;
        }

        public void Handle(CreateAtendimentoCommand command)
        {
        }

        public void Handle(UpdateAtendimentoCommand command)
        {
        }

        public object HandleWithReturn(UpdateAtendimentoCommand command)
        {
            throw new NotImplementedException();
        }

        public object HandleWithReturn(CreateAtendimentoCommand command)
        {
            Validation validation = Validate(command);
            try
            {
                using (var transaction = repository.Database.BeginTransaction())
                {
                    logger.LogInformation("Iniciando save de atendimento");
                    Atendimento atendimento = new Atendimento();
                    /// TODO: checar duplicidade de atendimento
                    if (command.AtendimentoId == Guid.Empty)
                    {
                        atendimento.Acolhimento = repository.Acolhimento.Find(command.AcolhimentoId);
                        atendimento.Profissional = repository.Profissional.Find(command.ProfissionalId);
                        atendimento.InicioAtendimento = command.InicioAtendimento;
                        atendimento.FimAtendimento = null;

                        repository.Entry(atendimento).State = EntityState.Added;
                        repository.Atendimento.Add(atendimento);
                        repository.SaveChanges();

                        transaction.Commit();
                        /// ----------------------------------------------------------------------
                        /// Adiciona o Procedimento defatult
                        /// ----------------------------------------------------------------------
                        AddProcedimentoAtendimentoCommand procedimento = new AddProcedimentoAtendimentoCommand();
                        procedimento.CodigoProcedimento = "0301060029"; /// ATENDIMENTO DE URGENCIA C/ OBSERVACAO ATE 24 HORAS EM ATENCAO ESPECIALIZADA
                        procedimento.NomeProcedimento = "ATENDIMENTO DE URGENCIA C/ OBSERVACAO ATE 24 HORAS EM ATENCAO ESPECIALIZADA";
                        procedimento.AtendimentoId = atendimento.Id;

                        HandleWithReturn(procedimento);

                        logger.LogInformation("Terminando save de atendimento");
                        return atendimento.Id;

                    }
                    else
                    {
                        logger.LogInformation("Iniciando atualização do atendimento: " + command.AtendimentoId.ToString());
                        atendimento = repository.Atendimento.Find(command.AtendimentoId);

                        atendimento.Profissional = repository.Profissional.Find(command.ProfissionalId);

                        /// Cria Anamnese
                        /// ----------------------------------
                        if (command.Anamnese != null)
                        {
                            /// Busca ciap pela descrição
                            /// --------------------------------------
                            Ciap2 ciap2 = repository.Ciap2.Where(x => x.Descricao == command.Anamnese.QueixaPrincipal).FirstOrDefault();
                            if (ciap2 != null)
                                command.Anamnese.Ciap2Id = ciap2.Id;

                            AnamneseEventoHandler hand = new AnamneseEventoHandler(repository);
                            command.Anamnese.AtendimentoId = atendimento.Id;
                            hand.Handle(command.Anamnese);
                        }

                        /// Cids
                        /// -------------------------------------------
                        if (!string.IsNullOrWhiteSpace(command.PrimeiroDiagnostico))
                            atendimento.PrimeiroDiagnostico = repository.CID.FirstOrDefault(x => x.Codigo == command.PrimeiroDiagnostico);

                        if (!string.IsNullOrWhiteSpace(command.SegundoDiagnostico))
                            atendimento.SegundoDiagnostico = repository.CID.FirstOrDefault(x => x.Codigo == command.SegundoDiagnostico);

                        /// Hipótese Diagnóstica
                        /// -------------------------------------------
                        HipoteseDiagnostica hipotese = repository.HipoteseDiagnostica.Where(x => x.Atendimento.Id == command.AtendimentoId).FirstOrDefault();
                        if (hipotese == null)
                            hipotese = new HipoteseDiagnostica();
                        hipotese.Observacao = command.ObsevacaoHipoteseDiagnostica;
                        hipotese.Atendimento = atendimento;
                        if (hipotese.Id == Guid.Empty)
                        {
                            repository.Entry(hipotese).State = EntityState.Added;
                            repository.HipoteseDiagnostica.Add(hipotese);
                            repository.SaveChanges();
                        }
                        else
                        {
                            repository.Entry(hipotese).State = EntityState.Modified;
                            repository.SaveChanges();

                        }

                        atendimento.ExameFisico = command.ExameFisico;
                        repository.Entry(atendimento).State = EntityState.Modified;
                        repository.SaveChanges();
                        transaction.Commit();
                        logger.LogInformation("Terminando atualização do atendimento: " + command.AtendimentoId.ToString());
                        return atendimento.Id;
                    }
                }
            }
            catch (Exception ex)
            {

                if (ex is GeneralErrors)
                {
                    foreach (var item in ((GeneralErrors)ex).Errors)
                    {
                        validation.AddValidationMessage(new ValidationMessage(item));
                    }
                }
                validation.AddValidationMessage(new ValidationMessage(ex.Message));
                return validation;
            }
        }

        private Validation Validate(CreateAtendimentoCommand command)
        {
            Validation validation = new Validation();

            if (command.ProfissionalId == Guid.Empty)
                validation.AddValidationMessage(new ValidationMessage("Profissional é obrigatório"));

            return validation;
        }

        public void Handle(AddProcedimentoAtendimentoCommand command)
        {
            throw new NotImplementedException();
        }

        public object HandleWithReturn(AddProcedimentoAtendimentoCommand command)
        {
            Validation validation = new Validation();
            try
            {
                validation = ValidateProcedimentoAtendimentoCommand(command);
                if (validation.IsValid)
                {
                    AtendimentoProcedimento atendimentoProcedimento = new AtendimentoProcedimento();
                    atendimentoProcedimento.AtendimentoId = command.AtendimentoId;
                    atendimentoProcedimento.CodigoProcedimento = command.CodigoProcedimento;
                    atendimentoProcedimento.NomeProcedimento = command.NomeProcedimento;
                    atendimentoProcedimento.VLSA = command.VLSA;
                    atendimentoProcedimento.QtDiasPermanencia = atendimentoProcedimento.QtDiasPermanencia;
                    atendimentoProcedimento.VLSP = atendimentoProcedimento.VLSP;

                    repository.AtendimentoProcedimento.Add(atendimentoProcedimento);
                    repository.SaveChanges();

                    return atendimentoProcedimento.Id;
                }
                else
                    return validation;

            }
            catch (Exception ex)
            {
                if (ex is GeneralErrors)
                {
                    foreach (var item in ((GeneralErrors)ex).Errors)
                    {
                        validation.AddValidationMessage(new ValidationMessage(item));
                    }
                }
                return validation;
            }
        }

        public Validation ValidateProcedimentoAtendimentoCommand(AddProcedimentoAtendimentoCommand command)
        {
            Validation validation = new Validation();
            if (command.AtendimentoId == Guid.Empty)
                validation.AddValidationMessage(new ValidationMessage("Commando não possui o identificador do atendimento"));

            if (string.IsNullOrWhiteSpace(command.CodigoProcedimento))
                validation.AddValidationMessage(new ValidationMessage("Commando não possui o identificador do procedimento"));

            return validation;
        }

        public void Handle(DeleteAtendimentoProcedimentoCommand command)
        {

        }

        public object HandleWithReturn(DeleteAtendimentoProcedimentoCommand command)
        {
            try
            {
                Validation validation = ValidaExclusao(command);

                if (validation.IsValid)
                {
                    using (var transaction = repository.Database.BeginTransaction())
                    {
                        AtendimentoProcedimento atendimentoProcedimento = repository.AtendimentoProcedimento.FirstOrDefault(x => x.AtendimentoId == command.AtendimentoId && x.CodigoProcedimento == command.CodigoProcedimento);

                        repository.Remove(atendimentoProcedimento);
                        repository.SaveChanges();

                        transaction.Commit();

                        return atendimentoProcedimento.Id;
                    }
                }
                else
                    return validation;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private Validation ValidaExclusao(DeleteAtendimentoProcedimentoCommand command)
        {
            Validation validacao = new Validation();

            if (command.CodigoProcedimento == "0301060029")
                validacao.AddValidationMessage(new ValidationMessage("Procedimento padrão não pode ser excluído!"));

            return validacao;
        }

        public void Dispose()
        {
            repository?.Dispose();
        }
    }
}

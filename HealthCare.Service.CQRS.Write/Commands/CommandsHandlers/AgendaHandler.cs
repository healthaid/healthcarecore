﻿using HealtCare.Infra.Common.Validations;
using HealthCare.Data.Repositories.Definitions.Agenda;
using HealthCare.Data.Repositories.Implementations.Agenda;
using HealthCare.Data.Repositories.Implementations.Cadastro;
using HealthCare.Domain.Entities;
using HealthCare.Domain.Entities.Agenda;
using HealthCare.Domain.Entities.Cadastro;
using HealthCare.Infra.CQRS.EventSourcing;
using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Service.CQRS.Write.Commands;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Write.Commands.CommandsHandlers
{
    public class AgendaHandler : ICommandHandler<CreateAgendaCommand>
    {

        private readonly AgendaRepository repository;
        private readonly AgendaEventoRepository repositoryEvento;
        private readonly ProfissionalRepository repositoryProfissional;
        private readonly UnidadeRepository repositoryUnidade;

        public AgendaHandler(AgendaRepository repositorio, ProfissionalRepository repositoryProfissional, AgendaEventoRepository repositoryEvento, UnidadeRepository repositoryUnidade)
        {
            this.repository = repositorio;
            this.repositoryProfissional = repositoryProfissional;
            this.repositoryEvento = repositoryEvento;
            this.repositoryUnidade = repositoryUnidade;
        }

        public void Handle(CreateAgendaCommand command)
        {
            // throw new NotImplementedException();
        }

        public object HandleWithReturn(CreateAgendaCommand command)
        {
            Validation v = new Validation();

            if (command.Id == Guid.Empty)
                v.AddValidationMessage(new ValidationMessage("Evento é obrigatório!"));

            if (command.ProfissionalId == Guid.Empty)
                v.AddValidationMessage(new ValidationMessage("Profissional é obrigatório!"));
            if (command.DataInicio == new DateTime())
                v.AddValidationMessage(new ValidationMessage("Data de inicio é obrigatória!"));
            else if (command.DataFim == new DateTime())
                v.AddValidationMessage(new ValidationMessage("Data de fim é obrigatória!"));
            else if (command.HoraInicio == new DateTime())
                v.AddValidationMessage(new ValidationMessage("Hora de inicio é obrigatória!"));
            else if (command.HoraFim == new DateTime())
                v.AddValidationMessage(new ValidationMessage("Hora Fim é obrigatória!"));
            else if (command.DiasDaSemana.Count <= 0)
                v.AddValidationMessage(new ValidationMessage("Informe os dias de semana!"));
            else if (command.DataFim < command.DataInicio)
                v.AddValidationMessage(new ValidationMessage("Data fim menor que data inicial!"));
            else if (command.HoraInicioIntervalo.Hour < command.HoraInicio.Hour || command.HoraInicioIntervalo.Hour > command.HoraFim.Hour)
                v.AddValidationMessage(new ValidationMessage("Horário de intervalo fora da faixa!"));
            else if (command.HoraFimIntervalo.Hour < command.HoraInicio.Hour || command.HoraFimIntervalo.Hour > command.HoraFim.Hour)
                v.AddValidationMessage(new ValidationMessage("Horário de intervalo fora da faixa!"));

            if (v.IsValid)
            {
                try
                {
                    if ((command.DataFim - command.DataInicio).Days >= 0)
                    {
                        if ((command.HoraFim - command.HoraInicio).Hours > 0)
                        {
                            Agenda agenda = new Agenda();
                            agenda.Id = Guid.NewGuid(); //command.profissionalId;
                            agenda.IsDeleted = false;
                            agenda.ProfissionalId = command.ProfissionalId;
                            agenda.Profissional = repositoryProfissional.Get(command.ProfissionalId);
                            agenda.RowVersion = BitConverter.GetBytes(DateTime.Now.Date.Ticks);
                            agenda.HoraInicio = new DateTime(command.DataInicio.Year, command.DataInicio.Month, command.DataInicio.Day, command.HoraInicio.Hour, command.HoraInicio.Minute, 0);
                            agenda.HoraFim = new DateTime(command.DataFim.Year, command.DataFim.Month, command.DataFim.Day, command.HoraFim.Hour, command.HoraFim.Minute, 0);
                            agenda.Nome = string.Format("Agenda - {0}", agenda.Profissional.Nome);
                            //agenda.Unidade = repositoryUnidade.Get(command.UnidadeId);

                            for (DateTime date = agenda.HoraInicio; agenda.HoraFim.CompareTo(date) > 0; date = date.AddDays(1))
                            {
                                if (command.DiasDaSemana.Contains(date.DayOfWeek))
                                {
                                    for (DateTime hour = command.HoraInicio; command.HoraFim.TimeOfDay > hour.TimeOfDay; hour = hour.AddMinutes(60))
                                    {
                                        /// Seta as variaveis de hora e inicio
                                        /// ----------------------------------
                                        DateTime horaInicio = new DateTime(date.Year, date.Month, date.Day, hour.Hour, hour.Minute, 0);
                                        DateTime horaFim = horaInicio.AddMinutes(60);

                                        /// Caso haja horario de inicio e intervalo setados
                                        /// ---------------------------------------------------
                                        if (command.HoraFimIntervalo != new DateTime() && command.HoraInicioIntervalo != new DateTime())
                                        {
                                            //if ((hour < command.HoraInicioIntervalo || (hour >= command.HoraFimIntervalo)) && hour < command.HoraFim)
                                            if (!(hour >= command.HoraInicioIntervalo && hour < command.HoraFimIntervalo))
                                            {
                                                /// Verifica se já não existe agendamento nessa data
                                                /// -----------------------------------------------                                                
                                                if (!repositoryEvento.ExisteAgendaEvento(horaFim, horaInicio, agenda.ProfissionalId))
                                                {
                                                    AgendaEvento agendaEvento = new AgendaEvento();
                                                    agendaEvento.Id = Guid.NewGuid();
                                                    agendaEvento.HoraInicio = horaInicio;
                                                    agendaEvento.HoraFim = agendaEvento.HoraInicio.AddMinutes(60);
                                                    agendaEvento.RowVersion = BitConverter.GetBytes(DateTime.Now.Date.Ticks);
                                                    agenda.AgendaEvento.Add(agendaEvento);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            /// Verifica se já não existe agendamento nessa data
                                            /// -----------------------------------------------                                                
                                            if (!repositoryEvento.ExisteAgendaEvento(horaFim, horaInicio, agenda.ProfissionalId))
                                            {
                                                AgendaEvento agendaEvento = new AgendaEvento();
                                                agendaEvento.Id = Guid.NewGuid();
                                                agendaEvento.HoraInicio = horaInicio;
                                                agendaEvento.HoraFim = horaFim;
                                                agendaEvento.RowVersion = BitConverter.GetBytes(DateTime.Now.Date.Ticks);

                                                agenda.AgendaEvento.Add(agendaEvento);
                                            }
                                        }
                                    }
                                }
                            }
                            if (agenda.AgendaEvento.Count > 0)
                                this.repository.Insert(agenda);
                            else
                            {
                                v.AddValidationMessage(new ValidationMessage("Não há datas válidas para o período informado!"));
                                return v;
                            }
                                
                        }
                    }
                    return command;
                }
                catch (Exception ex)
                {
                    if (ex is DbEntityValidationException)
                    {
                        var dbEx = (DbEntityValidationException)ex;
                        foreach (var validationErrors in dbEx.EntityValidationErrors)
                        {
                            foreach (var validationError in validationErrors.ValidationErrors)
                            {
                                v.AddValidationMessage(new ValidationMessage(string.Format("Property: {0} Error: {1}",
                                                        validationError.PropertyName,
                                                        validationError.ErrorMessage)));
                            }
                        }
                    }
                    if (ex is GeneralErrors)
                    {
                        foreach (var item in ((GeneralErrors)ex).Errors)
                        {
                            v.AddValidationMessage(new ValidationMessage(item));
                        }
                    }
                    v.AddValidationMessage(new ValidationMessage(ex.Message));
                    return v;
                }
            }
            else
                return v;
        }
    }
}

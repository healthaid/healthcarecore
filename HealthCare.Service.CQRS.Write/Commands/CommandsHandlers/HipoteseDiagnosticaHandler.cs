﻿using HealthCare.Domain.Entities.DbClasses.Atendimento;
using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HealthCare.Infra.CQRS.Write.Commands.CommandsHandlers
{
    public class HipoteseDiagnosticaHandler : ICommandHandler<CreateHipoteseDiagnosticaCommand>, ICommandHandler<UpdateHipoteseDiagnosticaCommand>
    {
        HealthCareDbContext repository;

        public HipoteseDiagnosticaHandler(HealthCareDbContext contextFactory)
        {
            this.repository = contextFactory;
        }

        public void Handle(CreateHipoteseDiagnosticaCommand command)
        {
            try
            {
                using (var transaction = repository.Database.BeginTransaction())
                {
                    HipoteseDiagnostica hipoteseDiagnostica = new HipoteseDiagnostica();
                    hipoteseDiagnostica.Observacao = command.Observacao;
                    hipoteseDiagnostica.Atendimento = repository.Atendimento.Find(command.AtendimentoId);

                    repository.Entry(hipoteseDiagnostica).State = EntityState.Added;
                    repository.HipoteseDiagnostica.Add(hipoteseDiagnostica);
                    repository.SaveChanges();

                    if (command.Cids.Count > 0)
                    {
                        hipoteseDiagnostica.HipoteseDiagnosticaCids = new List<HipoteseDiagnosticaCid>();
                        foreach (string cid in command.Cids)
                            hipoteseDiagnostica.HipoteseDiagnosticaCids.Add(new HipoteseDiagnosticaCid() {CidId = Guid.Parse(cid), HipoteseDiagnosticaId = hipoteseDiagnostica.Id });
                    }

                    repository.Entry(hipoteseDiagnostica).State = EntityState.Modified;
                    repository.SaveChanges();
                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public object HandleWithReturn(CreateHipoteseDiagnosticaCommand command)
        {
            throw new System.NotImplementedException();
        }

        public void Handle(UpdateHipoteseDiagnosticaCommand command)
        {
            try
            {
                using (var transaction = repository.Database.BeginTransaction())
                {
                    HipoteseDiagnostica hipoteseDiagnostica = repository.HipoteseDiagnostica.Find(command.Id);
                    hipoteseDiagnostica.Observacao = command.Observacao;
                    hipoteseDiagnostica.Atendimento = repository.Atendimento.Find(command.AtendimentoId);
                    hipoteseDiagnostica.HipoteseDiagnosticaCids.Clear();

                    repository.Entry(hipoteseDiagnostica).State = EntityState.Modified;
                    repository.SaveChanges();

                    if (command.Cids.Count > 0)
                    {
                        hipoteseDiagnostica.HipoteseDiagnosticaCids = new List<HipoteseDiagnosticaCid>();
                        foreach (string cid in command.Cids)
                            hipoteseDiagnostica.HipoteseDiagnosticaCids.Add(new HipoteseDiagnosticaCid() { CidId = Guid.Parse(cid), HipoteseDiagnosticaId = hipoteseDiagnostica.Id });
                    }
                    repository.Entry(hipoteseDiagnostica).State = EntityState.Modified;
                    repository.SaveChanges();
                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public object HandleWithReturn(UpdateHipoteseDiagnosticaCommand command)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            repository?.Dispose();
        }
    }
}
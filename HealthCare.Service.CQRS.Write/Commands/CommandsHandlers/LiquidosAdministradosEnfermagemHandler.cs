﻿using HealtCare.Infra.Common.Validations;
using HealthCare.Domain.Entities.DbClasses.Atendimento;
using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
namespace HealthCare.Infra.CQRS.Write.Commands.CommandsHandlers
{
    public class LiquidosAdministradosEnfermagemHandler : ICommandHandler<SaveLiquidosAdministradosEnfermagemCommand>, ICommandHandler<DeleteLiquidosAdministradosEnfermagemCommand>
    {

        HealthCareDbContext repository;
        public LiquidosAdministradosEnfermagemHandler(HealthCareDbContext contextFactory)
        {
            this.repository = contextFactory;
        }

        public void Handle(SaveLiquidosAdministradosEnfermagemCommand command)
        {
            //throw new NotImplementedException();
        }

        public object HandleWithReturn(SaveLiquidosAdministradosEnfermagemCommand command)
        {
            Validation v = ValidarLiquidosAdministradosEnfermagem(command);
            if (v.IsValid)
            {
                try
                {
                    using (var transaction = repository.Database.BeginTransaction())
                    {
                        LiquidosAdministradosEnfermagem liquidosAdministrados = new LiquidosAdministradosEnfermagem();
                        liquidosAdministrados.LiquidosAdministrados = repository.LiquidosAdministrados.FirstOrDefault(x => x.Id == command.LiquidosAdministradosId);
                        liquidosAdministrados.EvolucaoEnfermagem = repository.EvolucaoEnfermagem.FirstOrDefault(x => x.Id == command.EvolucaoEnfermagemId);

                        repository.Entry(liquidosAdministrados).State = EntityState.Added;
                        repository.LiquidosAdministradosEnfermagem.Add(liquidosAdministrados);
                        repository.SaveChanges();

                        transaction.Commit();

                        return liquidosAdministrados.Id;
                    }
                }
                catch (Exception ex)
                {
                    if (ex is GeneralErrors)
                    {
                        foreach (var item in ((GeneralErrors)ex).Errors)
                        {
                            v.AddValidationMessage(new ValidationMessage(item));
                        }
                    }
                    v.AddValidationMessage(new ValidationMessage(ex.Message));
                    return v;
                }
            }
            else
                return v;
        }

        private Validation ValidarLiquidosAdministradosEnfermagem(SaveLiquidosAdministradosEnfermagemCommand command)
        {
            Validation v = new Validation();

            return v;
        }


        public void Handle(DeleteLiquidosAdministradosEnfermagemCommand command)
        {
        }
        public object HandleWithReturn(DeleteLiquidosAdministradosEnfermagemCommand command)
        {
            Validation v = new Validation();
            try
            {
                using (var transaction = repository.Database.BeginTransaction())
                {
                    if (command.DeleteId == Guid.Empty)
                        v.AddValidationMessage(new ValidationMessage("Nenhum id informado para exclusão!"));
                    else
                    {
                        LiquidosAdministradosEnfermagem le = repository.LiquidosAdministradosEnfermagem.FirstOrDefault(x => x.Id == command.DeleteId);
                        repository.Entry(le).State = EntityState.Deleted;
                        repository.SaveChanges();
                    }
                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                if (ex is GeneralErrors)
                {
                    foreach (var item in ((GeneralErrors)ex).Errors)
                    {
                        v.AddValidationMessage(new ValidationMessage(item));
                    }
                }
                v.AddValidationMessage(new ValidationMessage(ex.Message));
            }
            return v;
        }

        public void Dispose()
        {
            repository?.Dispose();
        }
    }
}
﻿using HealthCare.Domain.Entities.DbClasses.Admin;
using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.CQRS.Read.Interfaces.Cadastro;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.Data.Context;
using HealthCare.Service.CQRS.Write.Commands;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HealthCare.Infra.CQRS.Write.Commands.CommandsHandlers
{
    /// <summary>
    /// Classe que realiza o controle dos comandos relacionados a entidade Usuário
    /// </summary>
    public class UserHandler : ICommandHandler<CreateUserCommand>,
        ICommandHandler<SetUserFirstLoginFalse>,
        ICommandHandler<RedefineUserPasswordCommand>,
        ICommandHandler<UpdateUserCommand>,
        ICommandHandler<ResetPasswordCommand>
    {
        private HealthCareDbContext repository;

        /// <summary>
        /// Construtor da classe
        /// </summary>
        /// <param name="usuarioRepository"></param>
        public UserHandler(HealthCareDbContext contextFactory)
        {
            repository = contextFactory;
        }

        public void Handle(SetUserFirstLoginFalse command)
        {
            try
            {
                using (var transaction = repository.Database.BeginTransaction())
                {
                    Usuario usuario = repository.Users.Find(command.UserId);
                    usuario.PrimeiroLogin = false;
                    repository.Entry(usuario).State = EntityState.Modified;
                    repository.SaveChanges();
                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public object HandleWithReturn(SetUserFirstLoginFalse command)
        {
            throw new NotImplementedException();
        }

        public void Handle(CreateUserCommand command)
        {
            throw new NotImplementedException();
        }

        public object HandleWithReturn(CreateUserCommand command)
        {
            Validation validation = new Validation();
            try
            {
                Usuario user = new Usuario()
                {
                    Nome = command.Name,
                    Email = command.Email,
                    UserName = command.Email,
                    PasswordHash = command.Password,
                    EmailConfirmed = false,
                    LockoutEnabled = false,
                    AccessFailedCount = 10,
                    TwoFactorEnabled = false,
                    PhoneNumberConfirmed = false,
                    PrimeiroLogin = true,
                };
                user.Profissional = new Profissional();
                user.Profissional.Nome = user.Nome;
                using (var transaction = repository.Database.BeginTransaction())
                {
                    repository.Entry(user.Profissional).State = EntityState.Added;
                    repository.Profissional.Add(user.Profissional);
                    repository.Entry(user).State = EntityState.Added;
                    repository.Users.Add(user);
                    List<Role> roles = repository.Query<Role>().ToList();
                    foreach (var item in roles)
                    {
                        UserRole role = new UserRole { RoleId = item.Id, UserId = user.Id };
                        repository.Entry(role).State = EntityState.Added;
                        repository.UserRoles.Add(role);
                    }
                    //repository.Users.Attach(user);
                    repository.SaveChanges();
                    transaction.Commit();
                }

            }
            catch (Exception ex)
            {
                validation.AddValidationMessage(new ValidationMessage(ex.Message));
            }
            return validation;
        }

        public void Handle(RedefineUserPasswordCommand command)
        {
            try
            {
                if (command.UsuarioId != null)
                {
                    using (var transaction = repository.Database.BeginTransaction())
                    {
                        Usuario usuario = repository.Users.Find(command.UsuarioId);
                        usuario.PasswordHash = command.NovaSenha;
                        usuario.PrimeiroLogin = false;
                        repository.Entry(usuario).State = EntityState.Modified;
                        repository.SaveChanges();
                        transaction.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public object HandleWithReturn(RedefineUserPasswordCommand command)
        {
            throw new NotImplementedException();
        }

        public void Handle(UpdateUserCommand command)
        {
            throw new NotImplementedException();
        }

        public object HandleWithReturn(UpdateUserCommand command)
        {
            Usuario usuario = repository.Query<Usuario>().FirstOrDefault(x => x.Id == command.UserId);
            Validation validation = ValidateUpdateUserCommand(command, usuario);
            if (validation.IsValid)
            {
                using (var transaction = repository.Database.BeginTransaction())
                {
                    usuario.UserName = command.UserName;
                    usuario.PasswordHash = command.NewPassword;
                    repository.Entry(usuario).State = EntityState.Modified;
                    repository.SaveChanges();
                    transaction.Commit();
                    return usuario.Id;
                }
            }
            else
                return validation;
        }

        public Validation ValidateUpdateUserCommand(UpdateUserCommand command, Usuario usuario)
        {
            Validation validation = new Validation();

            if (command.ActualPassword != usuario.PasswordHash)
                validation.AddValidationMessage(new ValidationMessage("Senha atual errada"));

            if (command.NewPassword != command.ConfirmNewPassord)
                validation.AddValidationMessage(new ValidationMessage("Nova senha e confirmação da nova senha estão diferentes"));

            return validation;
        }

        public void Handle(ResetPasswordCommand command)
        {
            Usuario user = repository.Users.Include(x => x.Profissional).Where(x => x.UserName == command.UserName).FirstOrDefault();
            user.PasswordHash = user.Profissional.CPF;
            using (var transaction = repository.Database.BeginTransaction())
            {
                repository.Entry(user).State = EntityState.Modified;
                repository.SaveChanges();
                transaction.Commit();
            }
        }

        public object HandleWithReturn(ResetPasswordCommand command)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            repository?.Dispose();
        }
    }
}

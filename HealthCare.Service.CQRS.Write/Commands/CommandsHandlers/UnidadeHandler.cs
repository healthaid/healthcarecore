﻿using AutoMapper;
using HealtCare.Infra.Common.Validations;
using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace HealthCare.Infra.CQRS.Write.Commands.CommandsHandlers
{
    public class UnidadeHandler : ICommandHandler<SaveUnidadeCommand>, ICommandHandler<DeleteUnidadeCommand>
    {
        private HealthCareDbContext repository;
        private IMapper mapper;
        public UnidadeHandler(HealthCareDbContext contextFactory, IMapper mapper)
        {
            this.repository = contextFactory;
            this.mapper = mapper;
        }

        private Validation ValidaUnidade(SaveUnidadeCommand command)
        {
            Validation v = new Validation();

            if (command.BairroId == Guid.Empty)
                v.AddValidationMessage(new ValidationMessage("Bairro é obrigatório!"));
            if (command.CidadeId == Guid.Empty)
                v.AddValidationMessage(new ValidationMessage("Cidade é obrigatória!"));
            if (command.UFId == Guid.Empty)
                v.AddValidationMessage(new ValidationMessage("UF é obrigatório!"));

            if (string.IsNullOrWhiteSpace(command.NomeFantasia))
                v.AddValidationMessage(new ValidationMessage("Nome é obrigatório!"));
            if (string.IsNullOrWhiteSpace(command.CodigoUnidade))
                v.AddValidationMessage(new ValidationMessage("Codigo é obrigatório!"));
            if (string.IsNullOrWhiteSpace(command.CNES))
                v.AddValidationMessage(new ValidationMessage("CNES é obrigatório!"));
            if (string.IsNullOrWhiteSpace(command.Situacao))
                v.AddValidationMessage(new ValidationMessage("Situação é obrigatório!"));

            return v;
        }

        public object HandleWithReturn(DeleteUnidadeCommand command)
        {
            Validation validation = new Validation();
            //Unidade.Situacao = "Inativa";
            try
            {
                Unidade Unidade = repository.Unidade.Find(command.UnidadeId);
                using (var transaction = repository.Database.BeginTransaction())
                {
                    repository.Entry(Unidade).State = EntityState.Modified;
                    repository.SaveChanges();

                    transaction.Commit();
                    return validation;
                }
            }
            catch (Exception ex)
            {
                if (ex is GeneralErrors)
                {
                    foreach (var item in ((GeneralErrors)ex).Errors)
                    {
                        validation.AddValidationMessage(new ValidationMessage(item));
                    }
                }
                validation.AddValidationMessage(new ValidationMessage(ex.Message));
                return validation;
            }
        }

        public object HandleWithReturn(SaveUnidadeCommand command)
        {
            Validation v = new Validation();
            Unidade u = mapper.Map<Unidade>(command);
            try
            {
                using (var transaction = repository.Database.BeginTransaction())
                {
                    if (command.UnidadeId == Guid.Empty)
                    {
                        repository.Entry(u).State = EntityState.Added;
                        repository.Unidade.Add(u);
                        repository.SaveChanges();
                    }
                    else
                    {
                        Unidade unidade = repository.Query<Unidade>().FirstOrDefault(x => x.Id == command.UnidadeId);
                        unidade.Cnpj = command.Cnpj;
                        unidade.NomeFantasia = command.NomeFantasia;
                        unidade.CNES = command.CNES;
                        unidade.CodigoUnidade = command.CodigoUnidade;
                        unidade.DataAcreditacao = command.DataAcreditacao;
                        unidade.NomeUnidade = command.NomeUnidade;
                        unidade.Sigla = command.Sigla;
                        unidade.Situacao = command.Situacao;

                        unidade.Endereco = unidade.Endereco;

                        repository.Entry(unidade).State = EntityState.Modified;
                        repository.SaveChanges();
                    }

                    transaction.Commit();
                }
                return u.Id;
            }
            catch (Exception ex)
            {
                if (ex is GeneralErrors)
                {
                    foreach (var item in ((GeneralErrors)ex).Errors)
                    {
                        v.AddValidationMessage(new ValidationMessage(item));
                    }
                }
                v.AddValidationMessage(new ValidationMessage(ex.Message));
                return v;
            }
        }

        public void Handle(SaveUnidadeCommand command)
        {
            // throw new NotImplementedException();
        }

        public void Handle(DeleteUnidadeCommand command)
        {

        }

        public void Dispose()
        {
            repository?.Dispose();
        }
    }
}

﻿using HealtCare.Infra.Common.Validations;
using HealthCare.Domain.Entities.DbClasses.Atendimento;
using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace HealthCare.Infra.CQRS.Write.Commands.CommandsHandlers
{
    public class CheckoutHandler : ICommandHandler<CreateCheckoutCommand>
    {
        private readonly HealthCareDbContext repository;

        public CheckoutHandler(HealthCareDbContext contextFactory)
        {
            repository = contextFactory;
        }

        public void Dispose()
        {
            repository?.Dispose();
        }

        public void Handle(CreateCheckoutCommand command)
        {
            throw new NotImplementedException();
        }

        public object HandleWithReturn(CreateCheckoutCommand command)
        {
            Validation validation = new Validation();
            try
            {
                using (var transaction = repository.Database.BeginTransaction())
                {
                    if (command.AtendimentoId != Guid.Empty)
                    {
                        Atendimento atendimento = repository.Atendimento.Find(command.AtendimentoId);

                        Checkout checkout = new Checkout();

                        if (command.CheckoutId != Guid.Empty)
                            checkout = repository.Checkout.FirstOrDefault(x => x.Id == command.CheckoutId);
                        else
                            atendimento.FimAtendimento = DateTime.Now;

                        checkout.DiagnosticoDois = command.DiagnosticoDois;
                        checkout.DiagnosticoUm = command.DiagnosticoUm;
                        checkout.Observacao = command.Observacao;
                        checkout.MotivoCheckout = repository.MotivoCheckout.Find(command.MotivoCheckout.MotivoCheckoutId);
                        atendimento.Checkout = checkout;

                        /// Libera o leito casa haja internação para o paciente
                        /// ---------------------------------------------------
                        var movimentacao = repository.MovimentacaoLeito.Include("Internacao.Atendimento").Include("LeitoAtual").Where(x => x.Internacao.Atendimento.Id == command.AtendimentoId).OrderByDescending(x => x.DataMovimentacao).FirstOrDefault();
                        if (movimentacao != null)
                        {
                            var leito = repository.Leito.Find(movimentacao.LeitoAtual.Id);
                            leito.Ocupado = false;
                            repository.Entry(leito).State = EntityState.Modified;
                            repository.SaveChanges();
                        }

                        repository.Entry(atendimento).State = EntityState.Modified;
                        repository.SaveChanges();
                        transaction.Commit();
                        return atendimento.Id;
                    }
                    else
                    {
                        validation.AddValidationMessage(new ValidationMessage("Checkout não está associado ao atendimento"));
                        return validation;
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex is GeneralErrors)
                {
                    foreach (var item in ((GeneralErrors)ex).Errors)
                    {
                        validation.AddValidationMessage(new ValidationMessage(item));
                    }
                }
                validation.AddValidationMessage(new ValidationMessage(ex.Message));
                return validation;
            }
        }
    }
}

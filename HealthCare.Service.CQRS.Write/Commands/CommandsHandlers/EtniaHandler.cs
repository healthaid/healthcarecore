﻿using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.Data.Context;
using System;
using HealthCare.Domain.Entities.DbClasses.Cadastro;

namespace HealthCare.Infra.CQRS.Write.Commands.CommandsHandlers
{
    public class EtniaHandler : ICommandHandler<CreateEtniaCommand>
    {
        HealthCareDbContext repository;

        public EtniaHandler(HealthCareDbContext contextFactory)
        {
            this.repository = contextFactory;
        }

        public void Dispose()
        {
            repository?.Dispose();
        }

        public void Handle(CreateEtniaCommand command)
        {
            try
            {
                using (var transaction = repository.Database.BeginTransaction())
                {
                    Etnia Etnia = new Etnia();
                    Etnia.Id = Guid.NewGuid(); //command.profissionalId;
                    Etnia.Descricao = command.Descricao;
                    Etnia.Ativo = command.Ativo;
                    Etnia.RowVersion = BitConverter.GetBytes(DateTime.Now.Date.Ticks);

                    //this.repository.Insert(Etnia);
                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public object HandleWithReturn(CreateEtniaCommand command)
        {
            throw new NotImplementedException();
        }
    }
}

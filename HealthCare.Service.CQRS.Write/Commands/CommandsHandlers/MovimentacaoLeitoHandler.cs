﻿using HealtCare.Infra.Common.Validations;
using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.Data.Context;
using System;
using System.Linq;
using HealthCare.Domain.Entities.DbClasses.Atendimento;
using Microsoft.EntityFrameworkCore;
using HealthCare.Domain.Entities.DbClasses.Cadastro;
using AutoMapper;

namespace HealthCare.Infra.CQRS.Write.Commands.CommandsHandlers
{
    public class MovimentacaoLeitoHandler : ICommandHandler<SaveMovimentacaoLeitoCommand>, ICommandHandler<SavePermutaCommand>, ICommandHandler<SaveTransferenciaLeitoCommand>
    {
        HealthCareDbContext repository;
        private IMapper mapper;
        public MovimentacaoLeitoHandler(HealthCareDbContext contextFactory, IMapper mapper)
        {
            this.repository = contextFactory;
            this.mapper = mapper;
        }

        public void Handle(SaveMovimentacaoLeitoCommand command)
        {
            //throw new NotImplementedException();
        }

        public object HandleWithReturn(SaveMovimentacaoLeitoCommand command)
        {
            Validation v = ValidarMovimentacaoLeito(command);
            MovimentacaoLeito MovimentacaoLeito = mapper.Map<MovimentacaoLeito>(command);
            if (v.IsValid)
            {
                try
                {
                    if (command.MovimentacaoLeitoId == Guid.Empty)
                    {
                        /// Registra movimentação para um determinado leito 
                        /// ------------------------------------------------------------------------------------------------
                        MovimentacaoLeito.DataMovimentacao = DateTime.Now;
                        MovimentacaoLeito.Clinica = repository.Servico.FirstOrDefault(x => x.Id == command.SetorClinicaId);
                        MovimentacaoLeito.Internacao = repository.Internacao.FirstOrDefault(x => x.Id == command.InternacaoId);
                        MovimentacaoLeito.LeitoAtual = repository.Leito.FirstOrDefault(x => x.Id == command.LeitoId);

                        repository.Entry(MovimentacaoLeito).State = EntityState.Added;
                        repository.MovimentacaoLeito.Add(MovimentacaoLeito);
                        repository.SaveChanges();

                        /// Ocupa o leito
                        /// ------------------------------------------------------------------------------------------------
                        var leito = repository.Leito.FirstOrDefault(x => x.Id == command.LeitoId);
                        leito.Ocupado = true;
                        repository.Entry(leito).State = EntityState.Modified;
                        repository.SaveChanges();
                    }
                    else
                    {
                        repository.Entry(MovimentacaoLeito).State = EntityState.Modified;
                        repository.SaveChanges();
                    }

                    return MovimentacaoLeito.Id;
                }
                catch (Exception ex)
                {
                    if (ex is GeneralErrors)
                    {
                        foreach (var item in ((GeneralErrors)ex).Errors)
                        {
                            v.AddValidationMessage(new ValidationMessage(item));
                        }
                    }
                    v.AddValidationMessage(new ValidationMessage(ex.Message));
                    return v;
                }
            }
            else
                return v;
        }

        private Validation ValidarMovimentacaoLeito(SaveMovimentacaoLeitoCommand command)
        {
            Validation v = new Validation();

            if (command.InternacaoId == Guid.Empty)
                v.AddValidationMessage(new ValidationMessage("Internação é obrigatório!"));
            if (command.LeitoId == Guid.Empty)
                v.AddValidationMessage(new ValidationMessage("Leito é obrigatório!"));

            return v;
        }

        public void Handle(RealizarExameCommand command)
        {

        }

        public void Handle(SavePermutaCommand command)
        {

        }

        public object HandleWithReturn(SavePermutaCommand command)
        {
            Validation v = new Validation();
            if (v.IsValid)
            {
                try
                {
                    using (var transaction = repository.Database.BeginTransaction())
                    {
                        /// Obtém os dados da movimentação atual
                        /// -------------------------------------------------------------------------------
                        MovimentacaoLeito movimentacaoAtual = repository.MovimentacaoLeito.Include("LeitoAtual").Include("Internacao").FirstOrDefault(x => x.Id == command.movimentacaoOrigemId);
                        /// Altera a clínica da movimentação atual
                        /// -------------------------------------------------------------------------------
                        movimentacaoAtual.Clinica = repository.Servico.FirstOrDefault(x => x.Id == command.ClinicaDestino);

                        /// Obtém os dados da movimentação Destino
                        /// -------------------------------------------------------------------------------
                        MovimentacaoLeito movimentacaoDestino = repository.MovimentacaoLeito.Include("LeitoAtual").Include("Internacao").FirstOrDefault(x => x.Id == command.movimentacaoDestinoId); // this.repository.GetMovimentacaoLeito(command.movimentacaoDestinoId);

                        /// Cria a nova movimentação e carrega os dados a partir da movimentação atual
                        /// -------------------------------------------------------------------------------
                        MovimentacaoLeito movimentacaoPermutaA = new MovimentacaoLeito();
                        movimentacaoPermutaA.Internacao = movimentacaoAtual.Internacao;
                        movimentacaoPermutaA.LeitoAtual = repository.Leito.FirstOrDefault(x => x.Id == movimentacaoDestino.LeitoAtual.Id);
                        movimentacaoPermutaA.UltimaMovimentacao = movimentacaoAtual;
                        movimentacaoPermutaA.DataMovimentacao = DateTime.Now;
                        movimentacaoPermutaA.Clinica = repository.Servico.FirstOrDefault(x => x.Id == movimentacaoDestino.Clinica.Id);

                        repository.Entry(movimentacaoPermutaA).State = EntityState.Added;
                        repository.MovimentacaoLeito.Add(movimentacaoPermutaA);
                        repository.SaveChanges();

                        /// Cria a nova movimentação e carrega os dados a partir da movimentação atual
                        /// -------------------------------------------------------------------------------
                        MovimentacaoLeito movimentacaoPermutaB = new MovimentacaoLeito();
                        movimentacaoPermutaB.Internacao = movimentacaoDestino.Internacao;
                        movimentacaoPermutaB.LeitoAtual = repository.Leito.FirstOrDefault(x => x.Id == movimentacaoAtual.LeitoAtual.Id);
                        movimentacaoPermutaB.UltimaMovimentacao = movimentacaoDestino;
                        movimentacaoPermutaB.DataMovimentacao = DateTime.Now;
                        movimentacaoPermutaB.Clinica = repository.Servico.FirstOrDefault(x => x.Id == movimentacaoAtual.Clinica.Id);

                        repository.Entry(movimentacaoPermutaB).State = EntityState.Added;
                        repository.MovimentacaoLeito.Add(movimentacaoPermutaB);
                        repository.SaveChanges();


                        transaction.Commit();

                        return movimentacaoDestino.Id;
                    }
                }
                catch (Exception ex)
                {
                    if (ex is GeneralErrors)
                    {
                        foreach (var item in ((GeneralErrors)ex).Errors)
                        {
                            v.AddValidationMessage(new ValidationMessage(item));
                        }
                    }
                    v.AddValidationMessage(new ValidationMessage(ex.Message));
                    return v;
                }
            }
            else
                return v;
        }

        private void CriaMovimentacao(MovimentacaoLeito movimentacaoAtual, Guid leito)
        {
            ///// Cria a nova movimentação e carrega os dados a partir da movimentação atual
            ///// -------------------------------------------------------------------------------
            //MovimentacaoLeito novaMovimentacao = new MovimentacaoLeito();
            //novaMovimentacao.Internacao = movimentacaoAtual.Internacao;
            //novaMovimentacao.LeitoAtual = this.repositoryLeito.Get(leito);
            //novaMovimentacao.UltimaMovimentacao = movimentacaoAtual;
            //novaMovimentacao.DataMovimentacao = DateTime.Now;
            //novaMovimentacao.Clinica = this.repositoryServico.Get(movimentacaoAtual.Clinica.Id);

            //this.repository.Insert(novaMovimentacao);

        }

        public void Handle(SaveTransferenciaLeitoCommand command)
        {
        }

        public object HandleWithReturn(SaveTransferenciaLeitoCommand command)
        {
            Validation v = new Validation();
            if (v.IsValid)
            {
                try
                {
                    using (var transaction = repository.Database.BeginTransaction())
                    {
                        /// Obtém os dados da movimentação atual
                        /// -------------------------------------------------------------------------------
                        MovimentacaoLeito movimentacaoAtual = repository.MovimentacaoLeito.Include("LeitoAtual").Include("Internacao").FirstOrDefault(x => x.Id == command.movimentacaoId);

                        /// Cria a nova movimentação e carrega os dados a partir da movimentação atual
                        /// -------------------------------------------------------------------------------
                        MovimentacaoLeito novaMovimentacao = new MovimentacaoLeito();
                        novaMovimentacao.Internacao = movimentacaoAtual.Internacao;
                        novaMovimentacao.LeitoAtual = repository.Leito.FirstOrDefault(x => x.Id == command.leitoDestinoId);
                        novaMovimentacao.UltimaMovimentacao = movimentacaoAtual;
                        novaMovimentacao.DataMovimentacao = DateTime.Now;

                        repository.Entry(novaMovimentacao).State = EntityState.Added;
                        repository.MovimentacaoLeito.Add(novaMovimentacao);
                        repository.SaveChanges();

                        /// Seta o leito anterior como livre
                        /// -------------------------------------------------------------------------------
                        Leito leitoLiberado = repository.Query<Leito>().FirstOrDefault(x => x.Id == movimentacaoAtual.LeitoAtual.Id);
                        leitoLiberado.Ocupado = false;
                        repository.Entry(leitoLiberado).State = EntityState.Modified;
                        repository.SaveChanges();

                        /// Seta como ocupado o leito de destino
                        /// -------------------------------------------------------------------------------
                        Leito leitoDestino = repository.Leito.FirstOrDefault(x => x.Id == novaMovimentacao.LeitoAtual.Id);
                        leitoDestino.Ocupado = true;
                        repository.Entry(leitoDestino).State = EntityState.Modified;
                        repository.SaveChanges();

                        transaction.Commit();

                        return novaMovimentacao.Id;
                    }
                }
                catch (Exception ex)
                {
                    if (ex is GeneralErrors)
                    {
                        foreach (var item in ((GeneralErrors)ex).Errors)
                        {
                            v.AddValidationMessage(new ValidationMessage(item));
                        }
                    }
                    v.AddValidationMessage(new ValidationMessage(ex.Message));
                    return v;
                }
            }
            else
                return v;
        }

        public void Dispose()
        {
            repository?.Dispose();
        }
    }
}
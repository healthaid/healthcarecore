﻿using HealtCare.Infra.Common.Validations;
using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.Data.Context;
using System;
using System.Linq;
using HealthCare.Domain.Entities.DbClasses.Atendimento;
using Microsoft.EntityFrameworkCore;
using AutoMapper;

namespace HealthCare.Infra.CQRS.Write.Commands.CommandsHandlers
{
    public class InternacaoHandler : ICommandHandler<SaveInternacaoCommand>
    {
        HealthCareDbContext repository;
        private IMapper mapper;
        public InternacaoHandler(HealthCareDbContext contextFactory, IMapper mapper)
        {
            this.repository = contextFactory;
            this.mapper = mapper;
        }

        public void Handle(SaveInternacaoCommand command)
        {
            //throw new NotImplementedException();
        }

        public object HandleWithReturn(SaveInternacaoCommand command)
        {
            Validation v = ValidarInternacao(command);
            Internacao Internacao = mapper.Map<Internacao>(command);
            if (v.IsValid)
            {
                try
                {
                    using (var transaction = repository.Database.BeginTransaction())
                    {
                        if (command.InternacaoId == Guid.Empty)
                        {
                            /// Registra internação
                            /// -------------------------------------------------------------------------------------------
                            Internacao.DataInternacao = DateTime.Now;
                            Internacao.Atendimento = repository.Atendimento.Include("Acolhimento.Servico").FirstOrDefault(x => x.Id == command.AtendimentoId);

                            repository.Entry(Internacao).State = EntityState.Added;
                            repository.Internacao.Add(Internacao);
                            repository.SaveChanges();

                            /// Faz a inclusão do registro em movimentaçao de Leito
                            /// -------------------------------------------------------------------------------------------
                            SaveMovimentacaoLeitoCommand movimentacao = new SaveMovimentacaoLeitoCommand();
                            movimentacao.InternacaoId = Internacao.Id;
                            movimentacao.LeitoId = command.LeitoId;

                            /// Obtém a clínica
                            /// -------------------------------------------------------------------------------------------
                            if (Internacao.Atendimento.Acolhimento.Servico != null)
                            {
                                var clinica = repository.Servico.FirstOrDefault(x => x.Id == Internacao.Atendimento.Acolhimento.Servico.Id);
                                movimentacao.SetorClinicaId = clinica.Id;
                            }
                            MovimentacaoLeitoHandler handlerMovimentacao = new MovimentacaoLeitoHandler(repository, mapper);

                            object retorno = handlerMovimentacao.HandleWithReturn(movimentacao);
                            if (retorno.GetType() != typeof(Guid))
                                return (ValidationMessage)retorno;
                        }
                        transaction.Commit();

                        return Internacao.Id;

                    }
                }
                catch (Exception ex)
                {
                    if (ex is GeneralErrors)
                    {
                        foreach (var item in ((GeneralErrors)ex).Errors)
                        {
                            v.AddValidationMessage(new ValidationMessage(item));
                        }
                    }
                    v.AddValidationMessage(new ValidationMessage(ex.Message));
                    return v;
                }
            }
            else
                return v;
        }

        private Validation ValidarInternacao(SaveInternacaoCommand command)
        {
            Validation v = new Validation();

            if (command.AtendimentoId == null)
                v.AddValidationMessage(new ValidationMessage("Nome é obrigatório!"));

            return v;
        }

        public void Handle(RealizarExameCommand command)
        {

        }

        public void Dispose()
        {
            repository?.Dispose();
        }
    }
}
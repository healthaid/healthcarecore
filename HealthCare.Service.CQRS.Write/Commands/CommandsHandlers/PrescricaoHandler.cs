﻿using HealtCare.Infra.Common.Validations;
using HealthCare.Infra.Common.Enums;
using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.Data.Context;
using System;
using System.Linq;
using HealthCare.Domain.Entities.DbClasses.Atendimento;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using Microsoft.Extensions.Logging;

namespace HealthCare.Infra.CQRS.Write.Commands.CommandsHandlers
{
    public class PrescricaoHandler : ICommandHandler<SavePrescricaoCommand>, ICommandHandler<DeletePrescricaoCommand>, ICommandHandler<SalaMedicamentoCommand>
    {
        HealthCareDbContext repository;
        private IMapper mapper;
        public PrescricaoHandler(HealthCareDbContext contextFactory, IMapper mapper)
        {
            this.repository = contextFactory;
            this.mapper = mapper;
        }

        public void Handle(SavePrescricaoCommand command)
        {
            //throw new NotImplementedException();
        }

        public object HandleWithReturn(SavePrescricaoCommand command)
        {
            Validation v = ValidarPrescricao(command);
            Guid prescricaoId = Guid.Empty;
            if (v.IsValid)
            {
                try
                {
                    using (var transaction = repository.Database.BeginTransaction())
                    {
                        if (command.PrescricaoId == Guid.Empty)
                        {

                            int quantidadePrescricoes = 1;

                            /// Calcula a prescrição;
                            /// ---------------------
                            if (command.DuracaoPrescricao == eDuracaoPrescricao.Hora)
                            {
                                if (command.IntervaloPrescricao == ePeriodoPrescricao.Minuto)
                                    quantidadePrescricoes = Convert.ToInt32((Convert.ToInt32(command.Duracao) * 60) / Convert.ToInt32(command.Intervalo));
                                else
                                    quantidadePrescricoes = Convert.ToInt32((Convert.ToInt32(command.Duracao)) / Convert.ToInt32(command.Intervalo));
                            }

                            for (int i = 0; i < quantidadePrescricoes; i++)
                            {
                                Prescricao Prescricao = mapper.Map<Prescricao>(command);

                                /// Carrega Atendimento
                                /// -------------------
                                Prescricao.Atendimento = repository.Atendimento.FirstOrDefault(x => x.Id == command.AtendimentoId);
                                Prescricao.EnvioEnfermagem = DateTime.Now;

                                if (command.SolicitanteId != Guid.Empty)
                                    Prescricao.Medico = repository.Profissional.FirstOrDefault(x => x.Id == command.SolicitanteId);

                                /// Insere classficação de risco
                                /// ----------------------------
                                repository.Entry(Prescricao).State = EntityState.Added;
                                repository.Prescricao.Add(Prescricao);
                                repository.SaveChanges();

                                prescricaoId = Prescricao.Id;
                            }

                        }
                        else
                        {
                            Prescricao Prescricao = repository.Prescricao.FirstOrDefault(x => x.Id == command.PrescricaoId);
                            Prescricao.Dose = command.Dose;
                            Prescricao.Duracao = command.Duracao;
                            Prescricao.Intervalo = command.Intervalo;
                            Prescricao.Medicamento = command.Medicamento;
                            Prescricao.Unidade = command.Unidade;
                            Prescricao.ViaAdministracao = command.ViaAdministracao;
                            Prescricao.ObservacaoMedico = command.ObservacaoMedico;
                            Prescricao.StatusEnfermagem = command.StatusEnfermagem;
                            Prescricao.ObservacaoEnfermagem = command.ObservacaoEnfermagem;

                            if (command.ProfissionalId != Guid.Empty)
                            {
                                Prescricao.Enfermeiro = repository.Profissional.FirstOrDefault(x => x.Id == command.ProfissionalId);
                                Prescricao.RetornoEnfermagem = DateTime.Now;
                            }

                            if (command.SolicitanteId != Guid.Empty)
                                Prescricao.Medico = repository.Profissional.FirstOrDefault(x => x.Id == command.SolicitanteId);

                            repository.Entry(Prescricao).State = EntityState.Modified;
                            repository.SaveChanges();
                        }

                        transaction.Commit();
                        return prescricaoId;
                    }
                }
                catch (Exception ex)
                {
                    if (ex is GeneralErrors)
                    {
                        foreach (var item in ((GeneralErrors)ex).Errors)
                        {
                            v.AddValidationMessage(new ValidationMessage(item));
                        }
                    }
                    v.AddValidationMessage(new ValidationMessage(ex.Message));
                    return v;
                }
            }
            else
                return v;
        }

        private void AdicionaProcedimento(Guid atendimentoId)
        {
            AddProcedimentoAtendimentoCommand addProcedimentoCommand = new AddProcedimentoAtendimentoCommand();
            addProcedimentoCommand.AtendimentoId = atendimentoId;
            addProcedimentoCommand.CodigoProcedimento = "0301100012";
            addProcedimentoCommand.NomeProcedimento = "ADMINISTRACAO DE MEDICAMENTOS NA ATENCAO ESPECIALIZADA.";
            LoggerFactory factory = new LoggerFactory();
            AtendimentoHandler handlerProcedimento = new AtendimentoHandler(repository, factory.CreateLogger<AtendimentoHandler>());
            handlerProcedimento.HandleWithReturn(addProcedimentoCommand);
        }

        private bool PrimeiraAplicacaoPrescricao(Guid idAtendimento)
        {
            return repository.Prescricao.Include("Atendimento").FirstOrDefault(x => x.Atendimento.Id == idAtendimento && x.StatusEnfermagem == "true") == null;
        }

        private Validation ValidarPrescricao(SavePrescricaoCommand command)
        {
            Validation v = new Validation();
            if (string.IsNullOrWhiteSpace(command.Medicamento))
                v.AddValidationMessage(new ValidationMessage("Medicamento é obrigatório!"));
            if (string.IsNullOrWhiteSpace(command.Dose))
                v.AddValidationMessage(new ValidationMessage("Dose é obrigatória!"));
            if (string.IsNullOrWhiteSpace(command.Duracao))
                v.AddValidationMessage(new ValidationMessage("Duracao é obrigatório!"));
            if (string.IsNullOrWhiteSpace(command.Intervalo))
                v.AddValidationMessage(new ValidationMessage("Intervalo é obrigatório!"));
            if (string.IsNullOrWhiteSpace(command.Unidade))
                v.AddValidationMessage(new ValidationMessage("Unidade é obrigatório!"));
            if (string.IsNullOrWhiteSpace(command.ViaAdministracao))
                v.AddValidationMessage(new ValidationMessage("Via de administração é obrigatória!"));

            return v;

        }


        public void Handle(DeletePrescricaoCommand command)
        {
        }

        public object HandleWithReturn(DeletePrescricaoCommand command)
        {
            Validation v = ValidaExclusao(command);
            try
            {
                Prescricao prescricao = repository.Prescricao.Include("Atendimento.Checkout").FirstOrDefault(x => x.Id == command.DeleteId);
                if (prescricao.StatusEnfermagem == "true" && prescricao.Atendimento.Checkout == null)
                    v.AddValidationMessage(new ValidationMessage("Não é possível excluir uma prescição já executada"));

                if (v.IsValid)
                {
                    using (var transaction = repository.Database.BeginTransaction())
                    {
                        repository.Entry(prescricao).State = EntityState.Deleted;
                        repository.SaveChanges();

                        transaction.Commit();
                        return prescricao.Id;
                    }
                }
                else
                    return v;
            }
            catch (Exception ex)
            {
                if (ex is GeneralErrors)
                {
                    foreach (var item in ((GeneralErrors)ex).Errors)
                    {
                        v.AddValidationMessage(new ValidationMessage(item));
                    }
                }
                v.AddValidationMessage(new ValidationMessage(ex.Message));
            }
            return v;
        }

        private Validation ValidaExclusao(DeletePrescricaoCommand command)
        {
            Validation validation = new Validation();

            if (command.DeleteId == Guid.Empty)
                validation.AddValidationMessage(new ValidationMessage("Nenhum id informado para exclusão!"));


            return validation;
        }

        public void Handle(SalaMedicamentoCommand command)
        {
            //throw new NotImplementedException();
        }

        public object HandleWithReturn(SalaMedicamentoCommand command)
        {
            Validation v = new Validation();

            if (command.PrescricaoId == Guid.Empty)
                v.AddValidationMessage(new ValidationMessage("Selecione um item no grid!"));


            if (v.IsValid)
            {
                try
                {
                    using (var transaction = repository.Database.BeginTransaction())
                    {

                        Prescricao Prescricao = repository.Prescricao.Include("Atendimento").FirstOrDefault(x => x.Id == command.PrescricaoId);// .Get(command.PrescricaoId);
                        Prescricao.ObservacaoEnfermagem = command.ObservacaoEnfermagem;
                        Prescricao.Enfermeiro = repository.Profissional.FirstOrDefault(x => x.Id == command.ProfissionalId);
                        Prescricao.RetornoEnfermagem = DateTime.Now;
                        Prescricao.StatusEnfermagem = command.StatusEnfermagem;

                        /// Se for a primeira aplicação da prescrição para aquele atendimento
                        if (command.StatusEnfermagem == "true" && PrimeiraAplicacaoPrescricao(Prescricao.Atendimento.Id))
                            AdicionaProcedimento(Prescricao.Atendimento.Id);

                        repository.Entry(Prescricao).State = EntityState.Modified;
                        repository.SaveChanges();

                        transaction.Commit();

                        return Prescricao.Id;
                    }
                }
                catch (Exception ex)
                {
                    if (ex is GeneralErrors)
                    {
                        foreach (var item in ((GeneralErrors)ex).Errors)
                        {
                            v.AddValidationMessage(new ValidationMessage(item));
                        }
                    }
                    v.AddValidationMessage(new ValidationMessage(ex.Message));
                    return v;
                }
            }
            else
                return v;
        }

        public void Dispose()
        {
            repository?.Dispose();
        }
    }
}
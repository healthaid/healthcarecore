﻿using AutoMapper;
using HealtCare.Infra.Common.Validations;
using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace HealthCare.Infra.CQRS.Write.Commands.CommandsHandlers
{
    public class ServicoHandler : ICommandHandler<CreateServicoCommand>, ICommandHandler<DeleteServicoCommand>
    {
        HealthCareDbContext repository;
        private IMapper mapper;
        public ServicoHandler(HealthCareDbContext contextFactory, IMapper mapper)
        {
            this.repository = contextFactory;
            this.mapper = mapper;
        }

        public void Handle(CreateServicoCommand command)
        {

        }

        public object HandleWithReturn(CreateServicoCommand command)
        {
            Validation v = new Validation();

            if (string.IsNullOrWhiteSpace(command.Descricao))
                v.AddValidationMessage(new ValidationMessage("Nome é obrigatório!"));

            if (v.IsValid)
            {
                try
                {
                    using (var transaction = repository.Database.BeginTransaction())
                    {
                        Servico servico = mapper.Map<Servico>(command);
                        if (command.ServicoId == Guid.Empty)
                        {
                            servico.IsDeleted = false;
                            servico.RowVersion = BitConverter.GetBytes(DateTime.Now.Date.Ticks);
                            repository.Entry(servico).State = EntityState.Added;
                            repository.Servico.Add(servico);
                            repository.SaveChanges();
                        }
                        else
                        {
                            servico = repository.Query<Servico>().FirstOrDefault(x => x.Id == command.ServicoId);
                            servico.Descricao = command.Descricao;
                            servico.IsDeleted = false;
                            servico.RowVersion = BitConverter.GetBytes(DateTime.Now.Date.Ticks);
                            repository.Entry(servico).State = EntityState.Modified;
                            repository.SaveChanges();
                        }
                        transaction.Commit();
                    }

                }
                catch (Exception ex)
                {
                    if (ex is GeneralErrors)
                    {
                        foreach (var item in ((GeneralErrors)ex).Errors)
                        {
                            v.AddValidationMessage(new ValidationMessage(item));
                        }
                    }
                    v.AddValidationMessage(new ValidationMessage(ex.Message));
                    return v;
                }
                return command;
            }
            else
                return v;
        }



        object ICommandHandler<CreateServicoCommand>.HandleWithReturn(CreateServicoCommand command)
        {
            throw new NotImplementedException();
        }


        public object HandleWithReturn(DeleteServicoCommand command)
        {
            Validation validation = new Validation();
            try
            {
                    using (var transaction = repository.Database.BeginTransaction())
                    {
                        Servico Servico = repository.Query<Servico>().FirstOrDefault(x => x.Id == command.ServicoId);
                        Servico.IsDeleted = true;
                        repository.Entry(Servico).State = EntityState.Modified;
                        repository.SaveChanges();

                        transaction.Commit();

                        return validation;
                    }
            }
            catch (Exception ex)
            {
                if (ex is GeneralErrors)
                {
                    foreach (var item in ((GeneralErrors)ex).Errors)
                    {
                        validation.AddValidationMessage(new ValidationMessage(item));
                    }
                }
                validation.AddValidationMessage(new ValidationMessage(ex.Message));
                return validation;
            }
        }

        public void Handle(DeleteServicoCommand command)
        {

        }

        public void Dispose()
        {
            repository?.Dispose();
        }
    }
}

﻿using HealthCare.Infra.CQRS.Messaging.Handling;
using HealthCare.Service.CQRS.Write.Models;
using System;
using System.ComponentModel.DataAnnotations;

namespace HealthCare.Service.CQRS.Write.Commands
{
    public abstract class AbstractCommand
    {
        public Guid CommandUsuarioId { get; set; }
    }




    /// <summary>
    /// Modelo de escrita para o paciente
    /// </summary>
    public class SavePacienteCommand : AbstractCommand, ICommand
    {

        public SavePacienteCommand()
        {
            this.Id = Guid.NewGuid();
        }
        /// <summary>
        /// Identificador do comando
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Identificador do paciente
        /// </summary>
        public Guid PacienteId { get; set; }

        /// <summary>
        /// Nome do paciente
        /// </summary>
        [Required]
        public string Nome { get; set; }

        /// <summary>
        /// Nome da mãe do paciente
        /// </summary>
        [Required]
        public string NomeMae { get; set; }

        /// <summary>
        /// Email do paciente
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Data de nascimento do paciente
        /// </summary>
        public DateTime? DataNascimento { get; set; }

        /// <summary>
        /// Principal telefone do paciente
        /// </summary>
        public string TelefoneCelular { get; set; }

        /// <summary>
        /// Telefone fixo de referencia do paciente
        /// </summary>
        public string TelefoneFixo { get; set; }

        /// <summary>
        /// Telefone do trabalho do paciente
        /// </summary>
        public string TelefoneTrabalho { get; set; }

        /// <summary>
        /// Endereco do paciente
        /// </summary>
        public EnderecoWriteModel Endereco { get; set; }

        /// <summary>
        /// Sexo do paciente
        /// </summary>
        public string Sexo { get; set; }

        /// <summary>
        /// Numero da carteira do plano de saude 
        /// </summary>
        public string NumeroCarteira { get; set; }

        /// <summary>
        /// Nome do plano de saúde 
        /// </summary>
        public string Plano { get; set; }

        /// <summary>
        /// Acomodação do paciente pelo plano
        /// </summary>
        public string Acomodacao { get; set; }

        /// <summary>
        /// Validade do plano de saúde
        /// </summary>
        public DateTime? Validade { get; set; }


        /// <summary>
        /// Cpf do paciente
        /// </summary>
        public string CPF { get; set; }

        /// <summary>
        /// Rg do paciente
        /// </summary>
        public string RG { get; set; }

        /// <summary>
        /// Número do cartao nacional de saúde do paciente
        /// </summary>
        public string CNS { get; set; }


        /// <summary>
        /// Etinia do paciente
        /// </summary>
        public string Etinia { get; set; }


        /// <summary>
        /// Estado civil do paciente
        /// </summary>
        public string EstadoCivil { get; set; }

        /// <summary>
        /// Escolaridade do paciente
        /// </summary>
        public string Escolaridade { get; set; }

        /// <summary>
        /// Profissão do paciente
        /// </summary>
        public string Profissao { get; set; }

        /// <summary>
        /// Unidade do paciente
        /// </summary>
        public Guid UnidadeId { get; set; }

        /// <summary>
        /// Sinaliza se o cadastro foi feito apenas pelo acolhimento
        /// </summary>
        public bool PendenteCadastro { get; set; }

        /// <summary>
        /// Estado de nascimento do paciente
        /// </summary>
        public Guid NaturalidadePaciente { get; set; }

        /// <summary>
        /// País de origem do paciente
        /// </summary>
        public string Nacionalidade { get; set; }

        /// <summary>
        /// Se o paciente estiver sendo salvo apartir do acolhimento, algumas regras de validação mudarão
        /// </summary>
        public bool VindoAcolhimento { get; set; }
        /// Raça
        /// </summary>
        public string Raca { get; set; }
        /// <summary>
        /// Id do Usuário que disparou o command
        /// </summary>
        public Guid CommandUsuarioId { get; set; }

    }
}

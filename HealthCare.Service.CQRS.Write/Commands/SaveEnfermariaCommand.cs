﻿using HealthCare.Infra.CQRS.Messaging.Handling;
using System;

namespace HealthCare.Infra.CQRS.Write.Commands
{
    public class SaveEnfermariaCommand : ICommand
    {
        public SaveEnfermariaCommand()
        {
            this.Id = Guid.NewGuid();
        }

        public Guid Id { get; set; }
        public Guid EnfermariaId { get; set; }
        public Guid UnidadeId { get; set; }
        public string Nome { get; set; }
        public string Local { get; set; }
        public bool  Ativo { get; set; }
        /// <summary>
        /// Id do Usuário que disparou o command
        /// </summary>
        public Guid CommandUsuarioId { get; set; }
    }
}
﻿using HealthCare.Infra.CQRS.Messaging.Handling;
using System;
using System.Collections.Generic;

namespace HealthCare.Infra.CQRS.Write.Commands
{
    public class SaveEvolucaoCommand : ICommand
    {
        public SaveEvolucaoCommand()
        {
            this.Id = Guid.NewGuid();
        }
        public Guid Id { get; set; }
        public Guid EvolucaoId { get; set; }
        public DateTime DataHoraEvolucao { get; set; }
        public string Descricao { get; set; }
        public string DiagnosticoUm { get; set; }
        public string DiagnosticoDois { get; set; }
        public int Quantidade { get; set; }
        public Guid AtendimentoId { get; set; }
        public Guid ProfissionalId { get; set; }
        public int EvolucaoTipo { get; set; }
        public List<string> Procedimentos { get; set; }
        /// <summary>
        /// Id do Usuário que disparou o command
        /// </summary>
        public Guid CommandUsuarioId { get; set; }
    }
}

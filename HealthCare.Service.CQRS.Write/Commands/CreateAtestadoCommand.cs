﻿using HealthCare.Infra.CQRS.Messaging.Handling;
using System;

namespace HealthCare.Infra.CQRS.Write.Commands
{
    public class CreateAtestadoCommand : ICommand
    {
        public CreateAtestadoCommand()
        {
            this.Id = Guid.NewGuid();
        }

        public Guid Id { get; set; }
        public Guid AtestadoId { get; set; }
        public string Descricao { get; set; }
        public string Dias { get; set; }
        public Guid AtendimentoId { get; set; }
        public Guid TemplateId { get; set; }
        public string CodigoCid { get; set; }
        
        /// <summary>
        /// Id do Usuário que disparou o command
        /// </summary>
        public Guid CommandUsuarioId { get; set; }
    }
}

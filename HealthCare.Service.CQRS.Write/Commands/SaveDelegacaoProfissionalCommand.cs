﻿using System;
using HealthCare.Infra.CQRS.Messaging.Handling;

namespace HealthCare.Service.CQRS.Write.Commands
{
    public class SaveDelegacaoProfissionalCommand : ICommand
    {
        public Guid Id
        {
            get
            {
                return Guid.NewGuid();
            }
        }

        public Guid DelegacaoProfissionalId { get; set; }
        public Guid DelegadorId { get; set; }
        public Guid DelegadoId { get; set; }
        public DateTime DataDelegacao { get; set; }
        public DateTime? DataDestituicao { get; set; }

        /// <summary>
        /// Id do Usuário que disparou o command
        /// </summary>
        public Guid CommandUsuarioId { get; set; }
    }
}
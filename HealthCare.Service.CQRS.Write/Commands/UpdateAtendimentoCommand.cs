﻿using HealthCare.Infra.CQRS.Messaging.Handling;
using System;

namespace HealthCare.Infra.CQRS.Write.Commands
{
    public class UpdateAtendimentoCommand : ICommand
    {
        public Guid Id { get; set; }
        public Guid PacienteId { get; set; }
        public Guid AgendaId { get; set; }
        public Guid UnidadeId { get; set; }
        /// <summary>
        /// Id do Usuário que disparou o command
        /// </summary>
        public Guid CommandUsuarioId { get; set; }
    }
}

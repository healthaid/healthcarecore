﻿using HealthCare.Infra.CQRS.Messaging.Handling;
using System;

namespace HealthCare.Infra.CQRS.Write.Commands
{
    public class CreateProfissionalCommand : ICommand
    {
        public Guid Id
        {
            get
            {
                return Guid.NewGuid();
            }
        }

        public Guid ProfissionalId { get; set; }

        public string Nome { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string CPF { get; set; }
        public string CNS { get; set; }
        public string Sexo { get; set; }
        public string NomePai { get; set; }
        public string NomeMae { get; set; }
        public DateTime? Nascimento { get; set; }
        public string CBO { get; set; }
        public string Naturalidade { get; set; }
        public string Nacionalidade { get; set; }
        public Guid UnidadeId { get; set; }
        public Guid[] Servicos { get; set; }
        public Guid[] Roles { get; set; }
        public String CRM { get; set; }
        /// <summary>
        /// Id do Usuário que disparou o command
        /// </summary>
        public Guid CommandUsuarioId { get; set; }
    }
}

﻿using HealthCare.Infra.CQRS.Messaging.Handling;
using System;

namespace HealthCare.Infra.CQRS.Write.Commands
{
    public class SaveProcedimentosEnfermagemCommand : ICommand
    {
        public System.Guid Id
        {
            get
            {
                return Guid.NewGuid();
            }
        }

        public Guid EvolucaoEnfermagemId { get; set; }
        public string CodigoProcedimento { get; set; }
        public int  Quantidade { get; set; }
        public string Observacao { get; set; }
        /// <summary>
        /// Id do Usuário que disparou o command
        /// </summary>
        public Guid CommandUsuarioId { get; set; }
    }
}

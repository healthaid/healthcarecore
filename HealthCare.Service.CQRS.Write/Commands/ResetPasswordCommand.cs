﻿using HealthCare.Infra.CQRS.Messaging.Handling;
using System;
using System.Collections.Generic;
using System.Text;

namespace HealthCare.Service.CQRS.Write.Commands
{
    public class ResetPasswordCommand : ICommand
    {
        public System.Guid Id
        {
            get
            {
                return Guid.NewGuid();
            }
        }

        public string UserName { get; set; }

        public Guid CommandUsuarioId { get; set; }
    }
}

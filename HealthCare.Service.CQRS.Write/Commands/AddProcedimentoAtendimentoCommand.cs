﻿using HealthCare.Infra.CQRS.Messaging.Handling;
using System;

namespace HealthCare.Infra.CQRS.Write.Commands
{
    /// <summary>
    /// Commando para adicionar o procedimento ao atendimento
    /// </summary>
    public class AddProcedimentoAtendimentoCommand : ICommand
    {
        public Guid Id
        {
            get
            {
                return Guid.NewGuid();
            }
        }

        /// <summary>
        /// Identificador do atendimento
        /// </summary>
        public Guid AtendimentoId { get; set; }


        /// <summary>
        /// Identificador do procedimento
        /// </summary>
        public string CodigoProcedimento { get; set; }

        /// <summary>
        /// Descrição do procedimento
        /// </summary>
        public string NomeProcedimento { get; set; }

        /// <summary>
        /// Valor pago ao serviço amnbulatorial para o procedimento
        /// </summary>
        public string VLSA { get; set; }

        /// <summary>
        /// Quantidade dias máximos de permanência para faturamento
        /// </summary>
        public string QtDiasPermanencia { get; set; }

        /// <summary>
        /// Valor pago ao serviço profissional para o procedimento
        /// </summary>
        public string VLSP { get; set; }
        /// <summary>
        /// Id do Usuário que disparou o command
        /// </summary>
        public Guid CommandUsuarioId { get; set; }
    }
}

﻿using HealthCare.Infra.CQRS.Messaging.Handling;
using System;

namespace HealthCare.Infra.CQRS.Write.Commands
{
    public class SaveEvolucaoEnfermagemCommand : ICommand
    {
        public Guid Id
        {
            get
            {
                return Guid.NewGuid();
            }
        }
        public Guid EvolucaoEnfermagemId { get; set; }
        public Guid MovimentacaoLeitoId { get; set; }
        public Guid ProfissionalId { get; set; }
        public string NumAtendimento { get; set; }
        public string Evolucao { get; set; }
        public string ObservacaoSinaisVitais { get; set; }
        public string Peso { get; set; }
        public string Pressao { get; set; }
        public string Pulso { get; set; }
        public string FrequenciaRespitaroria { get; set; }
        public string Temperatura { get; set; }
        /// <summary>
        /// Id do Usuário que disparou o command
        /// </summary>
        public Guid CommandUsuarioId { get; set; }

    }
}

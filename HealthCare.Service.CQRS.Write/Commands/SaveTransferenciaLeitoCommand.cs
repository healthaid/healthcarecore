﻿using HealthCare.Infra.CQRS.Messaging.Handling;
using System;

namespace HealthCare.Infra.CQRS.Write.Commands
{
    public class SaveTransferenciaLeitoCommand : ICommand
    {
        public SaveTransferenciaLeitoCommand()
        {
            this.Id = Guid.NewGuid();
        }

        public Guid Id { get; set; }
        public Guid movimentacaoId { get; set; }
        public Guid internacaoId { get; set; }
        public Guid leitoDestinoId { get; set; }
        /// <summary>
        /// Id do Usuário que disparou o command
        /// </summary>
        public Guid CommandUsuarioId { get; set; }
        //public Guid CommandUsuarioId { get; set; }
    }
}

﻿using HealthCare.Infra.CQRS.Messaging.Handling;
using System;

namespace HealthCare.Infra.CQRS.Write.Commands
{
    public class SaveMovimentacaoLeitoCommand : ICommand
    {
        public SaveMovimentacaoLeitoCommand()
        {
            this.Id = Guid.NewGuid();
        }
        public Guid Id { get; set; }

        public Guid MovimentacaoLeitoId { get; set; }
        public Guid SetorClinicaId { get; set; }
        public Guid InternacaoId { get; set; }
        public Guid LeitoId { get; set; }
        public Guid UltimaMovimentacaoId { get; set; }
        /// <summary>
        /// Id do Usuário que disparou o command
        /// </summary>
        public Guid CommandUsuarioId { get; set; }

    }
}

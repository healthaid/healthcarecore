﻿using HealthCare.Infra.CQRS.Messaging.Handling;
using System;


namespace HealthCare.Infra.CQRS.Write.Commands
{
    public class CreateAgendaEventoCommand : ICommand
    {
        public CreateAgendaEventoCommand()
        {
            this.Id = Guid.NewGuid();
        }

        public Guid Id { get; set; }
        public Guid AgendaId { get; set; }
        public Guid ServicoId { get; set; }
        public Guid PacienteId { get; set; }
        /// <summary>
        /// Id do Usuário que disparou o command
        /// </summary>
        public Guid CommandUsuarioId { get; set; }

    }
}

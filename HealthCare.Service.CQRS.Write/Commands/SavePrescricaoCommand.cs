﻿using HealthCare.Infra.Common.Enums;
using HealthCare.Infra.CQRS.Messaging.Handling;
using System;

namespace HealthCare.Infra.CQRS.Write.Commands
{
    public class SavePrescricaoCommand : ICommand
    {
        public Guid PrescricaoId { get; set; }
        public string Dose { get; set; }
        public string Intervalo { get; set; }
        public ePeriodoPrescricao IntervaloPrescricao { get; set; }
        public string Duracao { get; set; }
        public eDuracaoPrescricao DuracaoPrescricao { get; set; }
        public string Unidade { get; set; }
        public string ViaAdministracao { get; set; }
        public string Medicamento { get; set; }
        public Guid AtendimentoId { get; set; }
        public string ObservacaoMedico { get; set; }
        public string StatusEnfermagem { get; set; }
        public string ObservacaoEnfermagem { get; set; }
        public Guid SolicitanteId { get; set; }
        public Guid ProfissionalId { get; set; }
        /// <summary>
        /// Id do Usuário que disparou o command
        /// </summary>
        public Guid CommandUsuarioId { get; set; }
        public Guid Id
        {
            get
            {
                return Guid.NewGuid();
            }
        }
    }
}

﻿using HealthCare.Infra.CQRS.Messaging.Handling;
using System;

namespace HealthCare.Infra.CQRS.Write.Commands
{
    public class SaveMotivoCheckoutCommand : ICommand
    {

        public SaveMotivoCheckoutCommand()
        {
            Id = Guid.NewGuid();
        }
        public Guid MotivoCheckoutId { get; set; }
        public Guid Id { get; set; }
        public Guid TipoMotivoCheckoutId { get; set; }
        public string Descricao { get; set; }
        public bool Ativo { get; set; }
        /// <summary>
        /// Id do Usuário que disparou o command
        /// </summary>
        public Guid CommandUsuarioId { get; set; }
    }
}

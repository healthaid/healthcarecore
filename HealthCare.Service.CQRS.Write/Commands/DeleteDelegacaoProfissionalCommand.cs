﻿using HealthCare.Infra.CQRS.Messaging.Handling;
using System;
using System.Collections.Generic;
using System.Text;

namespace HealthCare.Service.CQRS.Write.Commands
{
    public class DeleteDelegacaoProfissionalCommand : ICommand
    {
        public Guid Id { get; set; }

        public Guid DeleteDelegacaoProfissionalId { get; set; }

        public Guid CommandUsuarioId { get; set; }
    }
}
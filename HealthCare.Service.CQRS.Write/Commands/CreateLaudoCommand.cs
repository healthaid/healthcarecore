﻿using HealthCare.Infra.CQRS.Messaging.Handling;
using System;

namespace HealthCare.Infra.CQRS.Write.Commands
{
    public class CreateLaudoCommand : ICommand
    {

        public Guid Id { get; set; }

        public string Numero { get; set; }
        public Guid EcoCargiogramaId { get; set; }
        public Guid UsuarioId { get; set; }

        /// <summary>
        /// Usuário que disparou o comando
        /// </summary>
        public Guid CommandUsuarioId { get; set; }
    }
}

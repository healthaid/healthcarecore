﻿using HealthCare.Infra.CQRS.Messaging.Handling;
using System;
using System.Collections.Generic;

namespace HealthCare.Infra.CQRS.Write.Commands
{
    public class CreateResultadoExameCommand : ICommand
    {
        public CreateResultadoExameCommand()
        {
            this.Id = Guid.NewGuid();
        }

        public Guid Id { get; set; }

        public Guid AtendimentoId { get; set; }
        public Guid ExameId { get; set; }
        public Guid PacienteId { get; set; }
        public IDictionary<Guid, string> ElementosResultadoExame { get; set; }
        /// <summary>
        /// Id do Usuário que disparou o command
        /// </summary>
        public Guid CommandUsuarioId { get; set; }

    }
}

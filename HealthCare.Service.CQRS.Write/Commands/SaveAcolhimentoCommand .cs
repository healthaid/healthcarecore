﻿using HealthCare.Infra.CQRS.Messaging.Handling;
using HealthCare.Service.CQRS.Write.Commands;
using System;

namespace HealthCare.Infra.CQRS.Write.Commands
{
    public class SaveAcolhimentoCommand : AbstractCommand, ICommand
    {
        public SaveAcolhimentoCommand()
        {
            this.Id = Guid.NewGuid();
        }
        public Guid Id { get; set; }
        public Guid AcolhimentoId { get; set; }
        public DateTime DataHora { get; set; }
        public SavePacienteCommand SavePacienteCommand { get; set; }
        public int Status { get; set; }
        public Guid ProfissionalId { get; set; }
        public Guid ServicoId { get; set; }
        public string CodigoBoletim { get; set; }
        public bool Prioridade { get; set; }
        /// <summary>
        /// Id do Usuário que disparou o command
        /// </summary>
        public Guid CommandUsuarioId { get; set; }
    }
}

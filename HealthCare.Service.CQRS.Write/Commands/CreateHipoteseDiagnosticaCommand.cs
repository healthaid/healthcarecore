﻿using HealthCare.Infra.CQRS.Messaging.Handling;
using System;
using System.Collections.Generic;

namespace HealthCare.Infra.CQRS.Write.Commands
{
    public class CreateHipoteseDiagnosticaCommand : ICommand
    {

        public CreateHipoteseDiagnosticaCommand()
        {
            Id = Guid.NewGuid();
        }

        public Guid Id { get; set; }
        public List<string> Cids { get; set; }
        public Guid AtendimentoId { get; set; }
        public string Observacao { get; set; }
        /// <summary>
        /// Id do Usuário que disparou o command
        /// </summary>
        public Guid CommandUsuarioId { get; set; }
    }
}

﻿using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Infra.CQRS.Messaging.Handling;
using System;
using System.Collections.Generic;

namespace HealthCare.Service.CQRS.Write.Commands
{
    public class CreateAgendaCommand : ICommand
    {
        public CreateAgendaCommand()
        {
            this.Id = Guid.NewGuid();
        }

        public Guid Id { get; set; }

        public Guid ProfissionalId { get; set; }
        public DateTime DataInicio { get; set; }
        public DateTime DataFim { get; set; }
        public DateTime HoraInicio { get; set; }
        public DateTime HoraFim { get; set; }
        public Profissional Profissional { get; set; }
        public DateTime HoraInicioIntervalo;
        public DateTime HoraFimIntervalo;
        public List<DayOfWeek> DiasDaSemana;
        public Guid UnidadeId { get; set; }
        /// <summary>
        /// Id do Usuário que disparou o command
        /// </summary>
        public Guid CommandUsuarioId { get; set; }
    }
}

﻿using HealthCare.Infra.CQRS.Messaging.Handling;
using System;

namespace HealthCare.Infra.CQRS.Write.Commands
{
    public class CreateExameCommand : ICommand
    {
        public CreateExameCommand()
        {
            this.Id = Guid.NewGuid();
        }

        public Guid Id { get; set; }
        public Guid AtendimentoId { get; set; }
        /// <summary>
        /// Id do Usuário que disparou o command
        /// </summary>
        public Guid CommandUsuarioId { get; set; }
    }
}

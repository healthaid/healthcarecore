﻿using HealthCare.Infra.CQRS.Messaging.Handling;
using System;

namespace HealthCare.Infra.CQRS.Write.Commands
{
    public class SaveLeitoCommand: ICommand
    {
        public SaveLeitoCommand()
        {
            this.Id = Guid.NewGuid();
        }

        public Guid Id { get; set; }
        public Guid LeitoId { get; set; }
        public string Nome { get; set; }
        public bool Ativo { get; set; }
        public int TipoLeito { get; set; }
        public Guid EnfermariaId { get; set; }
        /// <summary>
        /// Id do Usuário que disparou o command
        /// </summary>
        public Guid CommandUsuarioId { get; set; }
    }
}

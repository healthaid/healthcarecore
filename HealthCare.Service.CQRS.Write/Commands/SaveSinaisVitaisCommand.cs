﻿using HealthCare.Infra.CQRS.Messaging.Handling;
using System;

namespace HealthCare.Infra.CQRS.Write.Commands
{
    public class SaveSinaisVitaisCommand : ICommand
    {
        public Guid Id
        {
            get
            {
                return Guid.NewGuid();
            }
        }
        public Guid SaveSinaisVitaisId { get; set; }
        public string Peso { get; set; }
        public string Pressao { get; set; }
        public string Pulso { get; set; }
        public string FrequenciaRespiratoria { get; set; }
        public string Temperatura { get; set; }
        public string Altura { get; set; }
        public string GlicemiaCapilar { get; set; }
        /// <summary>
        /// Id do Usuário que disparou o command
        /// </summary>
        public Guid CommandUsuarioId { get; set; }
    }
}

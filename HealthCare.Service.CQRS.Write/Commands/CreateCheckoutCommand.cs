﻿using HealthCare.Infra.CQRS.Messaging.Handling;
using System;

namespace HealthCare.Infra.CQRS.Write.Commands
{
    public class CreateCheckoutCommand : ICommand
    {
        public Guid Id
        {
            get
            {
                return Guid.NewGuid();
            }
        }

        public Guid CheckoutId { get; set; }

        public Guid AtendimentoId { get; set; }

        public string Procedimento { get; set; }

        public string DiagnosticoUm { get; set; }

        public string DiagnosticoDois { get; set; }

        public string Observacao { get; set; }

        public SaveMotivoCheckoutCommand MotivoCheckout { get; set; }
        /// <summary>
        /// Id do Usuário que disparou o command
        /// </summary>
        public Guid CommandUsuarioId { get; set; }
    }
}

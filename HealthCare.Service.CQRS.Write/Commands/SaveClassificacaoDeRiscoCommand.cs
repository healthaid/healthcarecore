﻿using HealthCare.Infra.CQRS.Messaging.Handling;
using System;

namespace HealthCare.Infra.CQRS.Write.Commands
{
    public class SaveClassificacaoDeRiscoCommand : ICommand
    {
        public Guid Id
        {
            get
            {
                return Guid.NewGuid();
            }
        }

        public Guid ClassficacaoDeRiscoId { get; set; }
        public Guid AcolhimentoId { get; set; }
        public SaveSinaisVitaisCommand SinaisVitais { get; set; }
        public string NivelDeConsciencia { get; set; }
        public string Queixa { get; set; }
        public string CausaExterna { get; set; }
        public string DoencasPreexistentes { get; set; }
        public string MedicamentosEmUso { get; set; }
        public string Alergia { get; set; }
        public string Observacao { get; set; }
        public Guid EspecialidadeMedica { get; set; }
        public string Classificacao{ get; set; }
        public Guid ProfissionalId { get; set; }
        /// <summary>
        /// Id do Usuário que disparou o command
        /// </summary>
        public Guid CommandUsuarioId { get; set; }


    }
}

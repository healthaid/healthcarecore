﻿using HealthCare.Infra.CQRS.Messaging.Handling;
using System;

namespace HealthCare.Infra.CQRS.Write.Commands
{
    public class UpdateConvenioCommand : ICommand
    {
        public Guid Id { get; set; }
        public string NumeroCarteira { get; set; }
        public string Acomocadao { get; set; }
        public DateTime Validade { get; set; }
        public Guid PacienteId { get; set; }
        public Guid PlanoId { get; set; }
        /// <summary>
        /// Id do Usuário que disparou o command
        /// </summary>
        public Guid CommandUsuarioId { get; set; }
    }
}

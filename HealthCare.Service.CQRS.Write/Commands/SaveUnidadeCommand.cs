﻿using HealthCare.Infra.CQRS.Messaging.Handling;
using HealthCare.Service.CQRS.Write.Models;
using System;

namespace HealthCare.Infra.CQRS.Write.Commands
{
    public class SaveUnidadeCommand : ICommand
    {
        public Guid Id { get; set; }

        public Guid UnidadeId { get; set; }

        public string NomeUnidade { get; set; }
        public string NomeFantasia { get; set; }
        public string CodigoUnidade { get; set; }
        public string Sigla { get; set; }
        public string CNES { get; set; }
        public DateTime DataAcreditacao { get; set; }
        public string Situacao { get; set; }
        public string Cnpj { get; set; }
        public string CEP { get; set; }
        public string Logradouro { get; set; }
        public string Numero { get; set; }
        public string Complemento { get; set; }
        public Guid BairroId { get; set; }
        public Guid CidadeId { get; set; }
        public Guid PacienteId { get; set; }
        public Guid UFId { get; set; }
        public string CidadeNome { get; set; }
        public string BairroNome { get; set; }
        public string PacienteNome { get; set; }
        public EnderecoWriteModel Endereco { get; set; }
        public bool IsDeleted { get; set; }
        public bool Ativo { get; set; }
        /// <summary>
        /// Id do Usuário que disparou o command
        /// </summary>
        public Guid CommandUsuarioId { get; set; }
    }
}

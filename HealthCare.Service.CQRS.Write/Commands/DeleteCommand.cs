﻿using HealthCare.Infra.CQRS.Messaging.Handling;
using System;

namespace HealthCare.Infra.CQRS.Write.Commands
{
    public class DeleteReceitaMedicaCommand : ICommand
    {
        public Guid Id
        {
            get { return Guid.NewGuid(); }
        }

        public Guid DeleteId;
        /// <summary>
        /// Id do Usuário que disparou o command
        /// </summary>
        public Guid CommandUsuarioId { get; set; }
    }
}

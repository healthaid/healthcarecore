﻿using HealthCare.Infra.CQRS.Messaging.Handling;
using System;

namespace HealthCare.Infra.CQRS.Write.Commands
{
    public class CreateAnamneseCommand : ICommand
    {
        public CreateAnamneseCommand()
        {
            this.Id = Guid.NewGuid();
        }
        public Guid Id { get; set; }
        public Guid AnamneseId { get; set; }
        public Guid Ciap2Id { get; set; }
        public Guid AtendimentoId { get; set; }
        public string Descricao { get; set; }
        public String QueixaPrincipal { get; set; }
        /// <summary>
        /// Id do Usuário que disparou o command
        /// </summary>
        public Guid CommandUsuarioId { get; set; }


    }
}

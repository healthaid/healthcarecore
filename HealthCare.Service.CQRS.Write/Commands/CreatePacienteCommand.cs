﻿using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Infra.CQRS.Messaging.Handling;
using System;
using System.Collections.Generic;

namespace HealthCare.Service.CQRS.Write.Commands
{
    public class CreatePacienteCommand : ICommand
    {
        public CreatePacienteCommand()
        {
            this.Id = Guid.NewGuid();
            CNS = new List<CNS>();
            Convenio = new List<Convenio>();
            Enderecos = new List<Endereco>();
        }

        public Guid Id { get; set; }

        public string Nome { get; set; }

        public string NomeMae { get; set; }

        public DateTime DataNascimento { get; set; }
        public string Sexo { get; set; }
        public string Email { get; set; }
        public byte Foto { get; set; }
        public string CPF { get; set; }
        public string RG { get; set; }
        public bool Obito { get; set; }
        public bool Ativo { get; set; }
        public bool PendenteCadastro { get; set; }
        public Naturalidade NaturalidadeId { get; set; }
        public Guid EtniaId { get; set; }
        public IList<CNS> CNS { get; set; }
        public Guid EstadoCivilId { get; set; }
        public Guid EscolaridadeId { get; set; }
        public Guid ProfissaoId { get; set; }
        public List<Convenio> Convenio { get; set; }
        public List<Endereco> Enderecos { get; set; }
        /// <summary>
        /// Id do Usuário que disparou o command
        /// </summary>
        public Guid CommandUsuarioId { get; set; }

    }
}

﻿using HealthCare.Infra.CQRS.Messaging.Handling;
using System;

namespace HealthCare.Infra.CQRS.Write.Commands
{
    public class CreateTelefoneCommand : ICommand
    {

        public Guid Id { get; set; }

        public string Numero { get; set; }
        public Guid TipoTelefoneId { get; set; }
        public Guid PacienteId { get; set; }
        /// <summary>
        /// Id do Usuário que disparou o command
        /// </summary>
        public Guid CommandUsuarioId { get; set; }
    }
}

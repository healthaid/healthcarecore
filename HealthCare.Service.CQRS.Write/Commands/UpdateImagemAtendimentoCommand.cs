﻿using HealthCare.Infra.CQRS.Messaging.Handling;
using System;
using System.Collections.Generic;

namespace HealthCare.Infra.CQRS.Write.Commands
{
    public class UpdateImagemAtendimentoCommand : ICommand
    {
        public Guid Id {get;set;}
        public Guid ImagemAtendimentoId { get; set; }
        public string Observacao { get; set; }
        public List<Byte[]> Imagens { get; set; }
        public Guid AtendimentoId { get; set; }
        /// <summary>
        /// Id do Usuário que disparou o command
        /// </summary>
        public Guid CommandUsuarioId { get; set; }
    }
}

﻿using HealthCare.Infra.CQRS.Messaging.Handling;
using System;

namespace HealthCare.Infra.CQRS.Write.Commands
{
    public class UpdateUnidadeCommand : ICommand
    {
        public Guid IdUnidade { get; set; }
        public string NomeUnidade { get; set; }
        public string NomeFantasia { get; set; }
        public string CodigoUnidade { get; set; }
        public string Sigla { get; set; }
        public string CNES { get; set; }
        public DateTime DataAcreditacao { get; set; }
        public string Situacao { get; set; }
        public Guid Endereco { get; set; }
        public string CEP { get; set; }
        public string Logradouro { get; set; }
        public string Numero { get; set; }
        public string Complemento { get; set; }
        public Guid BairroId { get; set; }
        public Guid CidadeId { get; set; }
        public Guid PacienteId { get; set; }
        public Guid UFId { get; set; }
        public string CidadeNome { get; set; }
        public string BairroNome { get; set; }
        public string PacienteNome { get; set; }
        public Guid Id { get; set; }
        /// <summary>
        /// Id do Usuário que disparou o command
        /// </summary>
        public Guid CommandUsuarioId { get; set; }
    }
}

﻿using HealthCare.Infra.CQRS.Messaging.Handling;
using System;

namespace HealthCare.Infra.CQRS.Write.Commands
{
    public class SalaMedicamentoCommand : ICommand
    {
        public Guid PrescricaoId { get; set; }

        public Guid ProfissionalId { get; set; }

        public string ObservacaoEnfermagem { get; set; }

        public string StatusEnfermagem { get; set; }

        public Guid Id
        {
            get
            {
                return Guid.NewGuid();
            }
        }
        /// <summary>
        /// Id do Usuário que disparou o command
        /// </summary>
        public Guid CommandUsuarioId { get; set; }
    }
}

﻿using System;
using HealthCare.Infra.CQRS.Messaging.Handling;

namespace HealthCare.Infra.CQRS.Write.Commands
{
    public class UpdateEnderecoCommand : ICommand
    {
        public Guid Id { get; set; }

        public string CEP { get; set; }
        public string Logradouro { get; set; }
        public string Numero { get; set; }
        public string Complemento { get; set; }
        public Guid BairroId { get; set; }
        public Guid CidadeId { get; set; }
        public Guid PacienteId { get; set; }
        /// <summary>
        /// Id do Usuário que disparou o command
        /// </summary>
        public Guid CommandUsuarioId { get; set; }

    }
}

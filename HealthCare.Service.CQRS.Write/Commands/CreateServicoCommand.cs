﻿using HealthCare.Infra.CQRS.Messaging.Handling;
using System;

namespace HealthCare.Infra.CQRS.Write.Commands
{
    public class CreateServicoCommand: ICommand
    {
      
        /// <summary>
        /// Identificador do comando
        /// </summary>
        public Guid Id
        {
            get
            {
                return Guid.NewGuid();
            }
        }

        public Guid ServicoId { get; set; }

        public string Descricao { get; set; }

        public bool Ativo { get; set; }
        /// <summary>
        /// Id do Usuário que disparou o command
        /// </summary>
        public Guid CommandUsuarioId { get; set; }

    }
}

﻿using HealthCare.Infra.CQRS.Messaging.Handling;
using System;

namespace HealthCare.Infra.CQRS.Write.Commands
{

    public class CreateAtendimentoCommand : ICommand
    {
        public CreateAtendimentoCommand()
        {
            this.Id = Guid.NewGuid();
        }

        public Guid Id { get; set; }
        public Guid AtendimentoId { get; set; }
        public DateTime InicioAtendimento { get; set; }
        public DateTime? FimAtendimento { get; set; }
        public bool Checkout { get; set; }
        public string ExameFisico { get; set; }
        public Guid AcolhimentoId { get; set; }
        public string Observacao { get; set; }
        public SaveMotivoCheckoutCommand MotivoCheckout { get; set; }
        public Guid ProfissionalId { get; set; }
        public Guid MotivoConsultaId { get; set; }
        public string Procedimento { get; set; }
        public string DiagnosticoUm { get; set; }
        public string DiagnosticoDois { get; set; }
        public CreateAnamneseCommand Anamnese { get; set; }
        public Guid CidHipoteseDiagnostica { get; set; }
        public string ObsevacaoHipoteseDiagnostica { get; set; }
        public string PrimeiroDiagnostico { get; set; }
        public string SegundoDiagnostico { get; set; }
        /// <summary>
        /// Id do Usuário que disparou o command
        /// </summary>
        public Guid CommandUsuarioId { get; set; }

    }
}

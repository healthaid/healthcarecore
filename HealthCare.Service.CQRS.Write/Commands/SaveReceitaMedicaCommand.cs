﻿using HealthCare.Infra.CQRS.Messaging.Handling;
using System;

namespace HealthCare.Infra.CQRS.Write.Commands
{
    public class SaveReceitaMedicaCommand : ICommand
    {
        public Guid ReceitaMedicaId { get; set; }
        public string Dose { get; set; }
        public string Intervalo { get; set; }
        public string Duracao { get; set; }
        public string Unidade { get; set; }
        public string ViaAdministracao { get; set; }
        public string Medicamento { get; set; }

        public Guid AtendimentoId { get; set; }

        public Guid Id
        {
            get 
            {
                return Guid.NewGuid();
            }
        }
        /// <summary>
        /// Id do Usuário que disparou o command
        /// </summary>
        public Guid CommandUsuarioId { get; set; }
    }
}

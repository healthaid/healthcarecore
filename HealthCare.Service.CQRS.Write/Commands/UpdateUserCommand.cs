﻿using HealthCare.Infra.CQRS.Messaging.Handling;
using System;

namespace HealthCare.Infra.CQRS.Write.Commands
{
    public class UpdateUserCommand : ICommand
    {
        /// <summary>
        /// Id do comando
        /// </summary>
        public Guid Id
        {
            get
            {
                return Guid.NewGuid();
            }
        }

        /// <summary>
        /// Identificador do usuário
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// UserName do usuário
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Senha Atual
        /// </summary>
        public string ActualPassword { get; set; }

        /// <summary>
        /// Nova senha
        /// </summary>
        public string NewPassword { get; set; }

        /// <summary>
        /// Confirmação da nova senha
        /// </summary>
        public string ConfirmNewPassord { get; set; }

        /// <summary>
        /// Id do Usuário
        /// </summary>
        public Guid CommandUsuarioId { get; set; }
    }
}

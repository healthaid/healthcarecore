﻿using HealthCare.Infra.CQRS.Messaging.Handling;
using System;

namespace HealthCare.Infra.CQRS.Write.Commands
{
    /// <summary>
    /// Comando que é executado quando o usuário termina a configuração do primeiro acesso
    /// </summary>
    public class FirstAcessCommand : ICommand
    {
        /// <summary>
        /// Construtor da classe
        /// </summary>
        public FirstAcessCommand()
        {
            this.Id = Guid.NewGuid();
        }

        public Guid Id  { get; set; }

        /// <summary>
        /// Identificador do profissional logado
        /// </summary>
        public Guid ProfissionalId { get; set; }

        /// <summary>
        /// Login do usuário
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Identificador do usuário logado
        /// </summary>
        public Guid UsuarioId { get; set; }

        /// <summary>
        /// Cpf do profissional
        /// </summary>
        public string ProfissionalCpf { get; set; }

        /// <summary>
        /// Cbo do profissional
        /// </summary>
        public string ProfissionalCbo { get; set; }

        /// <summary>
        /// Nome da unidade 
        /// </summary>
        public string NomeUnidade { get; set; }

        /// <summary>
        /// Nome fantasia da unidade
        /// </summary>
        public string NomeUnidadeFantasia { get; set; }

        public Guid CommandUsuarioId { get; set; }

    }
}

﻿using HealthCare.Infra.CQRS.Messaging.Handling;
using System;

namespace HealthCare.Infra.CQRS.Write.Commands
{
    public class RedefineUserPasswordCommand : ICommand
    {
        public Guid Id
        {
            get
            {
                return Guid.NewGuid();
            }
        }

        public string NovaSenha { get; set; }

        public Guid UsuarioId { get; set; }

        public Guid CommandUsuarioId { get; set; }
    }
}

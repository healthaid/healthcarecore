﻿using HealthCare.Infra.CQRS.Messaging.Handling;
using System;

namespace HealthCare.Infra.CQRS.Write.Commands
{
    /// <summary>
    /// Comando usado para criar um usuário
    /// </summary>
    public class CreateUserCommand : ICommand
    {
        /// <summary>
        /// Construtor da classe
        /// </summary>
        public CreateUserCommand()
        {
            this.Id = Guid.NewGuid();
        }

        /// <summary>
        /// Identificador do registro
        /// </summary>
        public Guid Id{ get; set; }

        /// <summary>
        /// Nome do usuário
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Email do usuário
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Senha do usuário
        /// </summary>
        public string Password { get; set; }
        /// <summary>
        /// Id do Usuário que disparou o command
        /// </summary>
        public Guid CommandUsuarioId { get; set; }
    }
}

﻿using HealthCare.Infra.CQRS.Messaging.Handling;
using System;

namespace HealthCare.Infra.CQRS.Write.Commands
{
    public class SavePermutaCommand : ICommand
    {
        public SavePermutaCommand()
        {
            this.Id = Guid.NewGuid();
        }

        public Guid Id { get; set; }
        public Guid movimentacaoOrigemId { get; set; }
        public Guid movimentacaoDestinoId { get; set; }
        public Guid ClinicaDestino { get; set; }
        /// <summary>
        /// Id do Usuário que disparou o command
        /// </summary>
        public Guid CommandUsuarioId { get; set; }
        //public Guid CommandUsuarioId { get; set; }
    }
}

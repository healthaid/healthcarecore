﻿using HealthCare.Infra.CQRS.EventSourcing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Write.Events
{
    public class PacienteCreated : VersionedEvent
    {
        public string Nome { get; set; }

        public string NomeMae { get; set; }
    }
}

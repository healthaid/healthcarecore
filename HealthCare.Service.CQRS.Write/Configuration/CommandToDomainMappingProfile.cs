﻿using AutoMapper;
using HealthCare.Domain.Entities.DbClasses.Atendimento;
using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Infra.CQRS.Write.Commands;
using HealthCare.Service.CQRS.Write.Commands;
using HealthCare.Service.CQRS.Write.Models;

namespace HealthCare.Infra.CQRS.Write.Configuration
{
    public class CommandToDomainMappingProfile : Profile
    { 
        public CommandToDomainMappingProfile()
        {
            CreateMap<SavePacienteCommand, Paciente>().ForMember(dest => dest.Id, opts => opts.MapFrom(src => src.PacienteId));
            CreateMap<SaveUnidadeCommand, Unidade>().ForMember(dest => dest.Id, opts => opts.MapFrom(src => src.UnidadeId));
            CreateMap<CreateServicoCommand, Servico>().ForMember(dest => dest.Id, opts => opts.MapFrom(src => src.ServicoId));
            CreateMap<CreateProfissionalCommand, Profissional>().ForMember(dest => dest.Id, opts => opts.MapFrom(src => src.ProfissionalId));
            CreateMap<EnderecoWriteModel, Endereco>();
            CreateMap<SaveUnidadeCommand, Unidade>();
            CreateMap<SaveAcolhimentoCommand, Acolhimento>();
            CreateMap<SaveClassificacaoDeRiscoCommand, ClassificacaoDeRisco>();
            CreateMap<SaveSinaisVitaisCommand, SinaisVitais>();
            CreateMap<CreateAtendimentoCommand, Atendimento>();
            CreateMap<SaveMotivoCheckoutCommand, MotivoCheckout>();
            CreateMap<SaveSetorClinicaCommand, SetorClinica>();
            CreateMap<SaveReceitaMedicaCommand, ReceitaMedica>();
            CreateMap<SavePrescricaoCommand, Prescricao>();
            CreateMap<SaveExameAtendimentoCommand, ExameAtendimento>();
            CreateMap<SaveEnfermariaCommand, Enfermaria>();
            CreateMap<SaveLeitoCommand, Leito>();
            CreateMap<SaveInternacaoCommand, Internacao>();
            CreateMap<SaveMovimentacaoLeitoCommand, MovimentacaoLeito>();
            CreateMap<SaveEvolucaoCommand, Evolucao>();

            CreateMap<SaveEvolucaoEnfermagemCommand, EvolucaoEnfermagem>();
            CreateMap<SaveLiquidosAdministradosEnfermagemCommand, LiquidosAdministradosEnfermagem>();
            CreateMap<SaveLiquidosEliminadosEnfermagemCommand, LiquidosEliminadosEnfermagem>();
            CreateMap<SaveProcedimentosEnfermagemCommand, ProcedimentosEnfermagem>();
            CreateMap<InsertItemPainelCommand, Painel>();
            CreateMap<SaveDelegacaoProfissionalCommand, DelegacaoProfissional>();
        }
    }
}

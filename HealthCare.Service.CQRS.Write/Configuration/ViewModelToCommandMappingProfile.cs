﻿using AutoMapper;
using HealthCare.Infra.CQRS.Write.Commands;
using HealthCare.Infra.CQRS.Write.Models;
using HealthCare.Service.CQRS.Write.Commands;
using HealthCare.Service.CQRS.Write.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Write.Configuration
{
    public class ViewModelToCommandMappingProfile : Profile
    {
        public ViewModelToCommandMappingProfile()
        {
           CreateMap<UserWriteModel, CreateUserCommand>();
           CreateMap<PutAgendaEventoWriteModel, UpdateAgendaEventoCommand>();
           CreateMap<CancelarAgendaEventoWriteModel, CancelarAgendamentoCommand>();
           CreateMap<AgendaWriteModel, CreateAgendaCommand>();
           CreateMap<ProfissionalWriteModel, CreateProfissionalCommand>().ForMember(s => s.Servicos, c => c.MapFrom(m => m.Servicos));
           CreateMap<SaveUnidadeCommand, UnidadeUpdateModel>();
        }
    }
}

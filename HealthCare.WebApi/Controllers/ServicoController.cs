﻿using HealthCare.Infra.Common.Extensions;
using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.CQRS.Write.Commands;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HealthCare.WebApi.Controllers
{
    [EnableCors("AllowCors")]
    [Route("api/[controller]")]
    public class ServicoController : Controller
    {

        private readonly ICommandBus bus;
        private readonly IServicoDao read;

        public ServicoController(ICommandBus bus, IServicoDao read)
        {
            this.bus = bus;
            this.read = read;
        }

        [HttpGet("[action]")]
        public IActionResult ListarServicos(string filtro)
        {
            ListResult result = read.ListarServicos(filtro);
            var data = result.ToListResult();
            data.AllowActions = true;
            if (data.Columns.Count != 0)
            {
                data.Columns.FirstOrDefault(x => x.Name == "Id").AllowVisible = false;
                data.Columns.FirstOrDefault(x => x.Name == "Descricao").ColumnWidth = "80px";
                data.Columns.FirstOrDefault(x => x.Name == "Descricao").DisplayName = "Descrição";
                data.Columns.FirstOrDefault(x => x.Name == "IsDeleted").ColumnWidth = "80px";
                data.Columns.FirstOrDefault(x => x.Name == "IsDeleted").DisplayName = "Inativo";
            }
            return Json(data);
        }

        [HttpGet("[action]")]
        public ServicoReadModel ObterServico(Guid id)
        {
            return read.ObterServicoPorId(id);
        }

        [HttpPost("[action]")]
        public IActionResult SalvarServico([FromBody]CreateServicoCommand model)
        {
            try
            {
                object objReturned = bus.SendWithReturn(model);
                if (objReturned.GetType() == typeof(Validation))
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(((Validation)objReturned).Messages);
                }
                return Json(objReturned);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpPut("[action]")]
        public void AtualizarServico([FromBody]CreateServicoCommand model)
        {
            //UpdateServicoCommand c = new UpdateServicoCommand();
            //c.Id = model.Id;
            //c.Nome = model.Descricao;
            //c.Ativo = model.Ativo;
            //bus.Send(c);
        }

        [HttpGet("[action]")]
        public IActionResult ListarTodosServicos()
        {
            IList<ServicoReadModel> lista= read.ListarTodosServicos();
            
            return Json(lista);
        }

        [HttpDelete("[action]")]
        public IActionResult DeleteServico(string ServicoId)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(ServicoId) == false)
                {
                    DeleteServicoCommand command = new DeleteServicoCommand();
                    command.ServicoId = Guid.Parse(ServicoId);
                    object objReturned = bus.SendWithReturn(command);
                    if (objReturned.GetType() != typeof(Validation))
                        return Json(StatusCodes.Status200OK);
                    else
                    {
                        return Json(((Validation)objReturned).Messages);
                    }
                }
                else
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(StatusCodes.Status400BadRequest);
                }
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }
    }
}

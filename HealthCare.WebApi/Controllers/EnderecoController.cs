﻿using HealthCare.Infra.Common.Extensions;
using HealthCare.Infra.Common.Result;
using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Service.CQRS.Read.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HealthCare.WebApi.Controllers
{
    [EnableCors("AllowCors")]
    [Route("api/[controller]")]
    public class EnderecoController : Controller
    {
        /// <summary>
        /// Interface para o serviço de leitura do endereço
        /// </summary>
        private readonly IEnderecoDao read;


        /// <summary>
        /// Construtor da classe
        /// </summary>
        /// <param name="read"></param>
        public EnderecoController(IEnderecoDao read)
        {
            this.read = read;

        }

        /// <summary>
        /// Método que retorna um endereço com base em um cep
        /// </summary>
        /// <param name="cep">Cep do endereço</param>
        /// <returns>Retorna um HttpResponseMessage contendo os dados da request</returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetEndereco(string cep)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(cep))
                {
                    EnderecoReadViewModel model = await read.GetEnderecoByCepAsync(cep);
                    if (model != null)
                        return Json(model);
                    else
                        return Json(StatusCodes.Status404NotFound);
                }
                else
                {
                    return Json(StatusCodes.Status400BadRequest);
                }
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }


    }



}



﻿using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.CQRS.Write.Commands;
using HealthCare.Infra.CQRS.Write.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace HealthCare.WebApi.Controllers
{
    [EnableCors("AllowCors")]
    [Route("api/[controller]")]
    public class TelefoneController : Controller
    {
        private readonly ICommandBus bus;
        
        public TelefoneController(ICommandBus bus)
        {
            this.bus = bus;
            //this.read = read;
        }

        [HttpPost("[action]")]
        public void CadastrarTelefone([FromBody]TelefoneWriteModel model)
        {
            CreateTelefoneCommand c = new CreateTelefoneCommand();
            c.Numero = model.Numero;
            c.PacienteId = model.PacienteId;
            c.TipoTelefoneId = model.TipoTelefoneId;
            bus.Send(c);
        }


        [HttpPut("[action]")]
        public void AtualizarTelefone([FromBody]TelefoneWriteModel model)
        {
            UpdateTelefoneCommand c = new UpdateTelefoneCommand();
            c.Id = model.Id;
            c.Numero = model.Numero;
            c.PacienteId = model.PacienteId;
            c.TipoTelefoneId = model.TipoTelefoneId;
            bus.Send(c);
        }

    }
}

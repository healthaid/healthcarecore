﻿using HealthCare.Infra.Common.Extensions;
using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.CQRS.Write.Commands;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Net;


namespace HealthCare.WebApi.Controllers
{
    [EnableCors("AllowCors")]
    [Route("api/[controller]")]
    public class UnidadeController : Controller
    {
        private readonly ICommandBus bus;
        private readonly IUnidadeDao read;
        private readonly IBairroDao _readBairro;
        private readonly ICidadeDao _readCidade;
        private readonly IUFDao _readUF;


        public UnidadeController(ICommandBus bus, IUnidadeDao read,IBairroDao readBairro, ICidadeDao readCidade, IUFDao _readUF )
        {
            this.bus = bus;
            this.read = read;
            this._readBairro = readBairro;
            this._readCidade = readCidade;
            this._readUF = _readUF;
        }


        [HttpGet("[action]")]
        public IActionResult ListarUnidades(string filtro)
        {
            ListResult result = read.ListarUnidades(filtro);
            var data = result.ToListResult();
            data.AllowActions = true;
            if (data.Columns.Count != 0)
            {
                data.Columns.FirstOrDefault(x => x.Name == "Id").AllowVisible = false;
                data.Columns.FirstOrDefault(x => x.Name == "NomeUnidade").ColumnWidth = "80px";
                data.Columns.FirstOrDefault(x => x.Name == "NomeUnidade").DisplayName = "Nome";
                data.Columns.FirstOrDefault(x => x.Name == "Sigla").ColumnWidth = "80px";
                data.Columns.FirstOrDefault(x => x.Name == "Sigla").DisplayName = "Sigla";
                data.Columns.FirstOrDefault(x => x.Name == "Situacao").ColumnWidth = "80px";
                data.Columns.FirstOrDefault(x => x.Name == "Situacao").DisplayName = "Ativo";
            }
            return Json(data);
        }

        [HttpPost("[action]")]
        public IActionResult SalvarUnidade([FromBody]SaveUnidadeCommand model)
        {
            try
            {
                if (model != null)
                {
                    if (!string.IsNullOrWhiteSpace(model.Cnpj))
                    {
                        model.Cnpj = model.Cnpj.Replace(".", "").Replace("/", "").Replace("-", "");
                    }
                    
                    object objReturned = bus.SendWithReturn(model);
                    if (objReturned.GetType() == typeof(Validation))
                    {
                        HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                        return Json(((Validation)objReturned).Messages);
                    }
                    return Json(objReturned);
                }
                else
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(string.Empty);
                }
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpGet("[action]")]
        public IActionResult GetUnidadeById(string unidadeId)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(unidadeId) == false)
                {
                    UnidadeReadModel Unidade = read.GetUnidadeById(Guid.Parse(unidadeId));
                    return Json(Unidade);
                }
                else
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(StatusCodes.Status200OK);
                }
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpDelete("[action]")]
        public IActionResult DeleteUnidade(string UnidadeId)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(UnidadeId) == false)
                {
                    DeleteUnidadeCommand command = new DeleteUnidadeCommand();
                    command.UnidadeId = Guid.Parse(UnidadeId);
                    object objReturned = bus.SendWithReturn(command);
                    if (objReturned.GetType() != typeof(Validation))
                        return Json(StatusCodes.Status200OK);
                    else
                    {
                        return Json(((Validation)objReturned).Messages);
                    }
                }
                else
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(StatusCodes.Status400BadRequest);
                }
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }
    }
}

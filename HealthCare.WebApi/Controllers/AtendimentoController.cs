﻿using HealthCare.Infra.Common.Extensions;
using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.CQRS.Read.Interfaces.Atendimento;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.CQRS.Write.Commands;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Http;
using System.IO;
using HealthCare.Service.CQRS.Read.Models;

namespace HealthCare.WebApi.Controllers
{
    [EnableCors("AllowCors")]
    [Route("api/[controller]")]
    public class AtendimentoController : Controller
    {
        private readonly ICommandBus bus;
        private readonly IAtendimentoDto read;
        private readonly ICiap2Dao readCiap2;
        private readonly IProcedimentoDto readProcedimento;
        private readonly IAcolhimentoDTO readAcolhimento;
        private readonly ICidDao readCid;

        public AtendimentoController(ICommandBus bus, IAtendimentoDto read, ICiap2Dao readCiap2, IProcedimentoDto readProcedimento, IAcolhimentoDTO readAcolhimento, ICidDao readCid)
        {
            this.bus = bus;
            this.read = read;
            this.readCiap2 = readCiap2;
            this.readProcedimento = readProcedimento;
            this.readAcolhimento = readAcolhimento;
            this.readCid = readCid;
        }

        [HttpPost("[action]")]
        public IActionResult SalvarAtendimento([FromBody]CreateAtendimentoCommand model)
        {
            try
            {
                object objReturned = bus.SendWithReturn(model);
                if (objReturned.GetType() == typeof(Validation))
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(((Validation)objReturned).Messages);
                }
                return Json(objReturned);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpGet("[action]")]
        public IActionResult ObterAtendimento(string acolhimentoId)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(acolhimentoId) == false)
                {
                    AtendimentoReadModel atendimento = read.ObterAtendimentoPorAcolhimento(Guid.Parse(acolhimentoId));
                    return Json(atendimento);
                }
                else
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(StatusCodes.Status400BadRequest);
                }
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpGet("[action]")]
        public IActionResult GetAtendimentoById(string atendimentoId)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(atendimentoId) == false)
                {
                    AtendimentoReadModel atendimento = read.ObterAtendimentoPorId(Guid.Parse(atendimentoId));
                    return Json(atendimento);
                }
                else
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(StatusCodes.Status400BadRequest);
                }
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex);
            }
        }

        [HttpGet("[action]")]
        public IActionResult ListarAtendimentosDoDia(Guid idUnidade, Guid idProfissional, string dataInicio, string dataFim)
        {
            DateTime? inicio = null;
            DateTime? fim = null;

            if (!string.IsNullOrWhiteSpace(dataInicio))
                inicio = new DateTime(Convert.ToInt32(dataInicio.Substring(6, 4)), Convert.ToInt32(dataInicio.Substring(3, 2)), Convert.ToInt32(dataInicio.Substring(0, 2)));

            if (!string.IsNullOrWhiteSpace(dataFim))
            {
                fim = new DateTime(Convert.ToInt32(dataFim.Substring(6, 4)), Convert.ToInt32(dataFim.Substring(3, 2)), Convert.ToInt32(dataFim.Substring(0, 2)));
                fim = fim.Value.AddDays(1); //necessário por causa do horario
            }

            ListResult result = read.ListarAtendimentosDoDia(idUnidade, idProfissional, inicio, fim);
            var data = result.ToListResult();

            if (data.Columns.Count != 0)
            {
                data.Columns.FirstOrDefault(x => x.Name == "Id").AllowVisible = false;
                data.Columns.FirstOrDefault(x => x.Name == "AcolhimentoId").AllowVisible = false;
                data.Columns.FirstOrDefault(x => x.Name == "FimAtendimento").AllowVisible = false;
                data.Columns.FirstOrDefault(x => x.Name == "Checkout").AllowVisible = false;
                data.Columns.FirstOrDefault(x => x.Name == "Observacao").AllowVisible = false;
                data.Columns.FirstOrDefault(x => x.Name == "InicioAtendimento").ColumnWidth = "50px";
                data.Columns.FirstOrDefault(x => x.Name == "InicioAtendimento").DisplayName = "Hora do Atendimento";
                data.Columns.FirstOrDefault(x => x.Name == "Nome").ColumnWidth = "50px";
                data.Columns.FirstOrDefault(x => x.Name == "Nome").DisplayName = "Paciente";
                data.Columns.FirstOrDefault(x => x.Name == "Sexo").ColumnWidth = "50px";
                data.Columns.FirstOrDefault(x => x.Name == "Sexo").DisplayName = "Sexo";
                data.Columns.FirstOrDefault(x => x.Name == "Idade").ColumnWidth = "50px";
                data.Columns.FirstOrDefault(x => x.Name == "Idade").DisplayName = "Idade";
                data.Columns.FirstOrDefault(x => x.Name == "Boletim").DisplayName = "Boletim";
            }
            return Json(data);
        }

        [HttpGet("[action]")]
        public IActionResult GetAtendimentosByPaciente(Guid pacienteId, Guid unidadeId)
        {
            ListResult result = read.GetAtendimentosByPaciente(pacienteId, unidadeId);

            var data = result.ToListResult();

            if (data.Columns.Count != 0)
            {
                data.Columns.FirstOrDefault(x => x.Name == "Id").AllowVisible = false;
                data.Columns.FirstOrDefault(x => x.Name == "AcolhimentoId").AllowVisible = false;
                data.Columns.FirstOrDefault(x => x.Name == "InicioAtendimento").DisplayName = "Início do Atendimento";
                data.Columns.FirstOrDefault(x => x.Name == "Profissional").DisplayName = "Profissional";
                data.Columns.FirstOrDefault(x => x.Name == "Diagnostico").DisplayName = "Diagnóstico";
                data.Columns.FirstOrDefault(x => x.Name == "MotivoAlta").DisplayName = "Alta";
                data.Columns.FirstOrDefault(x => x.Name == "FimAtendimento").DisplayName = "Fim do Atendimento";
            }
            return Json(data);
        }

        [HttpGet("[action]")]
        public IActionResult GetHistoricoAtendimento(string atendimentoId)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(atendimentoId) == false)
                {
                    HistoricoAtendimentoReadModel atendimento = read.GetHistoricoAtendimento(Guid.Parse(atendimentoId));
                    return Json(atendimento);
                }
                else
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(StatusCodes.Status400BadRequest);
                }
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }
        


        [HttpGet("[action]")]
        public IActionResult ListarInternacoes(string unidadeId)
        {
            ListResult result = read.ListarInternacoes(Guid.Parse(unidadeId));
            var data = result.ToListResult();

            if (data.Columns.Count != 0)
            {
                data.Columns.FirstOrDefault(x => x.Name == "acolhimentoId").AllowVisible = false;
                data.Columns.FirstOrDefault(x => x.Name == "Paciente").ColumnWidth = "100px";
                data.Columns.FirstOrDefault(x => x.Name == "Paciente").DisplayName = "Paciente";
                data.Columns.FirstOrDefault(x => x.Name == "Enfermaria").ColumnWidth = "100px";
                data.Columns.FirstOrDefault(x => x.Name == "Enfermaria").DisplayName = "Enfermaria";
                data.Columns.FirstOrDefault(x => x.Name == "Leito").ColumnWidth = "100px";
                data.Columns.FirstOrDefault(x => x.Name == "Leito").DisplayName = "Leito";
            }
            return Json(data);
        }

        [HttpGet("[action]")]
        public IActionResult ListarInternacoesComAlta(string unidadeId)
        {
            ListResult result = read.ListarInternacoesComAlta(Guid.Parse(unidadeId));
            var data = result.ToListResult();

            if (data.Columns.Count != 0)
            {
                data.Columns.FirstOrDefault(x => x.Name == "acolhimentoId").AllowVisible = false;
                data.Columns.FirstOrDefault(x => x.Name == "CodigoBoletim").AllowVisible = false;
                data.Columns.FirstOrDefault(x => x.Name == "Paciente").ColumnWidth = "100px";
                data.Columns.FirstOrDefault(x => x.Name == "Paciente").DisplayName = "Paciente";
                data.Columns.FirstOrDefault(x => x.Name == "Enfermaria").ColumnWidth = "100px";
                data.Columns.FirstOrDefault(x => x.Name == "Enfermaria").DisplayName = "Enfermaria";
                data.Columns.FirstOrDefault(x => x.Name == "Leito").ColumnWidth = "100px";
                data.Columns.FirstOrDefault(x => x.Name == "Leito").DisplayName = "Leito";
            }
            return Json(data);
        }


        [HttpGet("[action]")]
        public IActionResult FilaParaAtendimento(string unidadeId)
        {
            if (!string.IsNullOrWhiteSpace(unidadeId))
            {
                ListResult result = read.GetFilaParaAtendimento(Guid.Parse(unidadeId));
                var data = result.ToListResult();
                if (data.Columns.Count != 0)
                {
                    data.Columns.FirstOrDefault(x => x.Name == "Id").AllowVisible = false;
                    data.Columns.FirstOrDefault(x => x.Name == "PacienteId").AllowVisible = false;
                    data.Columns.FirstOrDefault(x => x.Name == "HoraAcolhimento").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "HoraAcolhimento").DisplayName = "Hora";
                    data.Columns.FirstOrDefault(x => x.Name == "NomePaciente").ColumnWidth = "100px";
                    data.Columns.FirstOrDefault(x => x.Name == "NomePaciente").DisplayName = "Paciente";
                    data.Columns.FirstOrDefault(x => x.Name == "Sexo").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "Sexo").AllowVisible = false;
                    data.Columns.FirstOrDefault(x => x.Name == "Idade").AllowVisible = false;
                    data.Columns.FirstOrDefault(x => x.Name == "Sexo").DisplayName = "Sexo";
                    data.Columns.FirstOrDefault(x => x.Name == "Risco").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "Risco").DisplayName = "Risco";
                    data.Columns.FirstOrDefault(x => x.Name == "Risco").Type = "html";
                    data.Columns.FirstOrDefault(x => x.Name == "Idade").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "Idade").DisplayName = "Idade";
                    data.Columns.FirstOrDefault(x => x.Name == "Servico").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "Servico").DisplayName = "Serviço";
                    data.Columns.FirstOrDefault(x => x.Name == "Prioridade").DisplayName = "Prioridade";
                    data.Columns.FirstOrDefault(x => x.Name == "Boletim").DisplayName = "Boletim";
                    data.Columns.FirstOrDefault(x => x.Name == "TempoAtendimento").DisplayName = "Tempo";
                }
                return Json(data);
            }
            else
                return Json(StatusCodes.Status400BadRequest);
        }

        [HttpGet("[action]")]
        public IActionResult ObterBoletimPorAcolhimento(string acolhinentoId)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(acolhinentoId) == false)
                {
                    BoletimReadModel paciente = read.ObterBoletimPorAcolhimento(Guid.Parse(acolhinentoId));
                    return Json(paciente);
                }
                else
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(StatusCodes.Status400BadRequest);
                }
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex);
            }
        }

        [HttpGet("[action]")]
        public IActionResult ListarProcedimentos(string descricao)
        {
            try
            {
                var retorno = readProcedimento.ListarTodosProcedimentos(descricao).AsQueryable().Select(x => x.CO_PROCEDIMENTO + " - " + x.NO_PROCEDIMENTO).ToArray();
                return Json(retorno);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex);
            }
        }

        [HttpGet("[action]")]
        public IActionResult ListarProcedimentosPorCID(string codigo)
        {
            try
            {
                var retorno = readProcedimento.ListarProcedimentosPorCID(codigo).AsQueryable().Select(x => x.CO_PROCEDIMENTO + " - " + x.NO_PROCEDIMENTO).ToArray();
                return Json(retorno);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpGet]
        [Route("ListarProcedimentosPorDescricaoeCID")]
        public Array ListarProcedimentosPorDescricaoeCID(string codigo, string texto)
        {
            try
            {
                var retorno = readProcedimento.ListarProcedimentosPorCID(codigo, texto).AsQueryable();
                if (retorno.Count() > 0)
                    return retorno.Select(x => x.CO_PROCEDIMENTO + " - " + x.NO_PROCEDIMENTO).ToArray();
                else
                    return null;
            }
            catch (Exception)
            {
                throw;
            }
        }


        [HttpGet("[action]")]
        public IActionResult ListarProcedimentosInJsonFormat(string descricao)
        {
            try
            {
                List<ProcedimentoReadModel> procedimentos = readProcedimento.ListarTodosProcedimentos(descricao);
                return Json(procedimentos);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.Message);
            }
        }

        [HttpGet("[action]")]
        public IActionResult GetHeaderAtendimento(string acolhimentoId)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(acolhimentoId))
                    acolhimentoId = Guid.Empty.ToString();

                HeaderAtendimentoReadModel header = readAcolhimento.ObterInformacoesHeaderAtendimento(Guid.Parse(acolhimentoId));
                return Json(header);

            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpPost("[action]")]
        public IActionResult AddProcedimento([FromBody]AddProcedimentoAtendimentoCommand command)
        {
            try
            {
                if (command == null)
                    return Json(StatusCodes.Status400BadRequest);
                else
                {
                    object retorno = this.bus.SendWithReturn(command);
                    if (retorno.GetType() == typeof(Validation))
                    {
                        HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                        return Json(((Validation)retorno).Messages);
                    }
                    else
                        return Json(StatusCodes.Status200OK);
                }
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex);
            }
        }

        [HttpPost("[action]")]
        public IActionResult DeleteProcedimento([FromBody]DeleteAtendimentoProcedimentoCommand command)
        {
            try
            {
                if (command == null)
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(StatusCodes.Status400BadRequest);
                }
                else
                {
                    object objReturned = bus.SendWithReturn(command);
                    return Json(StatusCodes.Status200OK);
                }
            }
            catch (Exception ex)
            {
                return Json(new ResponseMessage(StatusCodes.Status500InternalServerError, ex));
            }
        }

        [HttpGet("[action]")]
        public IActionResult ProcedimentosByAtendimento(Guid atendimentoId)
        {
            try
            {
                if (atendimentoId == Guid.Empty)
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(StatusCodes.Status400BadRequest);
                }
                else
                {
                    ListResult result = read.GetProcedimentosByAtendimento(atendimentoId);
                    var data = result.ToListResult();
                    if (data.Columns.Count != 0)
                    {
                        data.Columns.Where(x => x.Name == "Id").FirstOrDefault().AllowVisible = false;
                        data.Columns.FirstOrDefault(x => x.Name == "CO_PROCEDIMENTO").DisplayName = "Código Procedimento";
                        data.Columns.FirstOrDefault(x => x.Name == "NO_PROCEDIMENTO").DisplayName = "Nome Procedimento";
                    }
                    return Json(data);
                }
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.Message);
            }
        }

        [HttpGet("[action]")]
        public IActionResult GetProcedimentosByAtendimentoDataServico(string inicio, string fim, Guid idservico)
        {
            try
            {
                DateTime dinicio = new DateTime(Convert.ToInt32(inicio.Substring(6, 4)), Convert.ToInt32(inicio.Substring(3, 2)), Convert.ToInt32(inicio.Substring(0, 2)));
                DateTime dfim = new DateTime(Convert.ToInt32(fim.Substring(6, 4)), Convert.ToInt32(fim.Substring(3, 2)), Convert.ToInt32(fim.Substring(0, 2)));
                if (idservico == Guid.Empty)
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(StatusCodes.Status400BadRequest);
                }
                else
                {
                    ListResult result = read.EstatistaOdontoProcedimentoDario(dinicio, dfim, idservico);
                    var data = result.ToListResult();
                    if (data.Columns.Count != 0)
                    {
                        data.Columns.GroupBy(x => x.Name == "DataAtendimento");
                        data.Columns.FirstOrDefault(x => x.Name == "Procedimentos").DisplayName = "Procedimentos";

                    }
                    return Json(data);
                }
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.Message);
            }
        }

        [HttpGet("[action]")]
        public IActionResult ListarPacientesRegistradosNoDia(string inicio, string fim, Guid UnidadeId)
        {
            DateTime dinicio = new DateTime(Convert.ToInt32(inicio.Substring(6, 4)), Convert.ToInt32(inicio.Substring(3, 2)), Convert.ToInt32(inicio.Substring(0, 2)));
            DateTime dfim = new DateTime(Convert.ToInt32(fim.Substring(6, 4)), Convert.ToInt32(fim.Substring(3, 2)), Convert.ToInt32(fim.Substring(0, 2)));
            ListResult result = read.ListarPacientesRegistradosNoDia(dinicio, dfim, UnidadeId);
            var data = result.ToListResult();

            return Json(data);
        }

        [HttpGet("[action]")]
        public IActionResult ListarProducaoDiariaPorSetor(string inicio, string fim, Guid UnidadeId)
        {
            DateTime dinicio = new DateTime(Convert.ToInt32(inicio.Substring(6, 4)), Convert.ToInt32(inicio.Substring(3, 2)), Convert.ToInt32(inicio.Substring(0, 2)));
            DateTime dfim = new DateTime(Convert.ToInt32(fim.Substring(6, 4)), Convert.ToInt32(fim.Substring(3, 2)), Convert.ToInt32(fim.Substring(0, 2)));

            ListResult result = read.ListarProducaoDiariaPorSetor(dinicio, dfim, UnidadeId);
            var data = result.ToListResult();

            return Json(data);
        }

        [HttpGet("[action]")]
        public IActionResult ListarFilaEspera(Guid UnidadeId)
        {
            ListResult result = read.ListarFilaEspera(UnidadeId);
            var data = result.ToListResult();

            return Json(data);
        }

        [HttpGet("[action]")]
        public IActionResult ListarPendentesERealizados(string inicio, string fim, Guid UnidadeId)
        {
            DateTime dinicio = new DateTime(Convert.ToInt32(inicio.Substring(6, 4)), Convert.ToInt32(inicio.Substring(3, 2)), Convert.ToInt32(inicio.Substring(0, 2)));
            DateTime dfim = new DateTime(Convert.ToInt32(fim.Substring(6, 4)), Convert.ToInt32(fim.Substring(3, 2)), Convert.ToInt32(fim.Substring(0, 2)));
            ListResult result = read.ListarPendentesERealizados(dinicio, dfim, UnidadeId);
            var data = result.ToListResult();

            return Json(data);
        }

        [HttpGet("[action]")]
        public IActionResult ListaNominalDeObitos(string inicio, string fim, Guid UnidadeId)
        {

            DateTime dinicio = new DateTime(Convert.ToInt32(inicio.Substring(6, 4)), Convert.ToInt32(inicio.Substring(3, 2)), Convert.ToInt32(inicio.Substring(0, 2)));
            DateTime dfim = new DateTime(Convert.ToInt32(fim.Substring(6, 4)), Convert.ToInt32(fim.Substring(3, 2)), Convert.ToInt32(fim.Substring(0, 2)));
            ListResult result = read.ListaNominalDeObitos(dinicio, dfim, UnidadeId);
            var data = result.ToListResult();

            return Json(data);
        }

        [HttpGet("[action]")]
        public IActionResult EstatisticaAtendimentoPorProfissional(string inicio, string fim, Guid UnidadeId)
        {
            DateTime dinicio = new DateTime(Convert.ToInt32(inicio.Substring(6, 4)), Convert.ToInt32(inicio.Substring(3, 2)), Convert.ToInt32(inicio.Substring(0, 2)));
            DateTime dfim = new DateTime(Convert.ToInt32(fim.Substring(6, 4)), Convert.ToInt32(fim.Substring(3, 2)), Convert.ToInt32(fim.Substring(0, 2)));
            ListResult result = read.EstatisticaAtendimentoPorProfissional(dinicio, dfim, UnidadeId);
            var data = result.ToListResult();

            return Json(data);
        }

        [HttpGet("[action]")]
        public IActionResult EstatisticaAtendimentoTotalPorProfissional(string inicio, string fim, Guid UnidadeId)
        {
            DateTime dinicio = new DateTime(Convert.ToInt32(inicio.Substring(6, 4)), Convert.ToInt32(inicio.Substring(3, 2)), Convert.ToInt32(inicio.Substring(0, 2)));
            DateTime dfim = new DateTime(Convert.ToInt32(fim.Substring(6, 4)), Convert.ToInt32(fim.Substring(3, 2)), Convert.ToInt32(fim.Substring(0, 2)));
            ListResult result = read.EstatisticaAtendimentoTotalPorProfissional(dinicio, dfim, UnidadeId);
            var data = result.ToListResult();

            return Json(data);
        }

        [HttpGet("[action]")]
        public IActionResult EstatisticaMensalProntoAtendimento(int mes, int ano, Guid UnidadeId)
        {
            ListResult result = read.EstatisticaMensalProntoAtendimento(mes, ano, UnidadeId);
            var data = result.ToListResult();

            return Json(data);
        }

        [HttpGet("[action]")]
        public IActionResult ListarDiagnosticosMaisAtendidos(string inicio, string fim, Guid UnidadeId)
        {
            DateTime dinicio = new DateTime(Convert.ToInt32(inicio.Substring(6, 4)), Convert.ToInt32(inicio.Substring(3, 2)), Convert.ToInt32(inicio.Substring(0, 2)));
            DateTime dfim = new DateTime(Convert.ToInt32(fim.Substring(6, 4)), Convert.ToInt32(fim.Substring(3, 2)), Convert.ToInt32(fim.Substring(0, 2)));
            ListResult result = read.ListarDiagnosticosMaisAtendidos(dinicio, dfim, UnidadeId);
            var data = result.ToListResult();

            return Json(data);
        }

        [HttpGet("[action]")]
        public IActionResult ListarProducaoResumoAtendimento(string inicio, string fim, Guid UnidadeId)
        {
            DateTime dinicio = new DateTime(Convert.ToInt32(inicio.Substring(6, 4)), Convert.ToInt32(inicio.Substring(3, 2)), Convert.ToInt32(inicio.Substring(0, 2)));
            DateTime dfim = new DateTime(Convert.ToInt32(fim.Substring(6, 4)), Convert.ToInt32(fim.Substring(3, 2)), Convert.ToInt32(fim.Substring(0, 2)));
            ListResult result = read.ListarProducaoResumoAtendimento(dinicio, dfim, UnidadeId);
            var data = result.ToListResult();

            return Json(data);
        }

        [HttpGet("[action]")]
        public IActionResult ListarSPAMunicipioBairroFaixaEtaria(string inicio, string fim, string municipio)
        {
            try
            {
                DateTime dinicio = new DateTime(Convert.ToInt32(inicio.Substring(6, 4)), Convert.ToInt32(inicio.Substring(3, 2)), Convert.ToInt32(inicio.Substring(0, 2)));
                DateTime dfim = new DateTime(Convert.ToInt32(fim.Substring(6, 4)), Convert.ToInt32(fim.Substring(3, 2)), Convert.ToInt32(fim.Substring(0, 2)));
                ListResult result = read.ListarSPAMunicipioBairroFaixaEtaria(dinicio, dfim, municipio);
                var data = result.ToListResult();
                return Json(data);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }

        }

        [HttpGet("[action]")]
        public IActionResult ListarProntoAtendimentoTempoPermanencia(string inicio, string fim, Guid UnidadeId)
        {
            try
            {
                DateTime dinicio = new DateTime(Convert.ToInt32(inicio.Substring(6, 4)), Convert.ToInt32(inicio.Substring(3, 2)), Convert.ToInt32(inicio.Substring(0, 2)));
                DateTime dfim = new DateTime(Convert.ToInt32(fim.Substring(6, 4)), Convert.ToInt32(fim.Substring(3, 2)), Convert.ToInt32(fim.Substring(0, 2)));
                ListResult result = read.ListarProntoAtendimentoTempoPermanencia(dinicio, dfim, UnidadeId);
                var data = result.ToListResult();
                return Json(data);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex);
            }

        }


        [HttpGet("[action]")]
        public IActionResult ListarRegistroHoraUrgenciaClinica(string inicio, string fim, Guid UnidadeId)
        {
            try
            {
                DateTime dinicio = new DateTime(Convert.ToInt32(inicio.Substring(6, 4)), Convert.ToInt32(inicio.Substring(3, 2)), Convert.ToInt32(inicio.Substring(0, 2)));
                DateTime dfim = new DateTime(Convert.ToInt32(fim.Substring(6, 4)), Convert.ToInt32(fim.Substring(3, 2)), Convert.ToInt32(fim.Substring(0, 2)));
                ListResult result = read.ListarRegistroHoraUrgenciaClinica(dinicio, dfim, UnidadeId);
                var data = result.ToListResult();
                return Json(data);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex);
            }

        }


        [HttpGet("[action]")]
        public IActionResult ListarSpaGrupoDiagnosticoSexoFaixaEtaria(string inicio, string fim, Guid UnidadeId)
        {
            try
            {
                DateTime dinicio = new DateTime(Convert.ToInt32(inicio.Substring(6, 4)), Convert.ToInt32(inicio.Substring(3, 2)), Convert.ToInt32(inicio.Substring(0, 2)));
                DateTime dfim = new DateTime(Convert.ToInt32(fim.Substring(6, 4)), Convert.ToInt32(fim.Substring(3, 2)), Convert.ToInt32(fim.Substring(0, 2)));
                ListResult result = read.ListarSpaDiagnosticoSexoFaixaEtaria(dinicio, dfim, UnidadeId);
                var data = result.ToListResult();
                return Json(data);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }

        }

        [HttpGet("[action]")]
        public IActionResult ListarPacientesAtendidosEspecialidadeRisco(string inicio, string fim, Guid UnidadeId)
        {
            try
            {
                ListResult result = read.ListarPacientesAtendidosEspecialidadeRisco(DateTime.Parse(inicio), DateTime.Parse(fim), UnidadeId);
                var data = result.ToListResult();
                return Json(data);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex);
            }

        }

        [HttpGet("[action]")]
        public IActionResult ListarPacientesInternadosMais24Horas(Guid UnidadeId, string paciente, string enfermariaId, string LeitoId, string dataInternacao, string profissionalId)
        {
            try
            {
                DateTime? dataP = null;
                if (!string.IsNullOrWhiteSpace(dataInternacao))
                    dataP = DateTime.Parse(dataInternacao);

                ListResult result = read.ListarPacientesInternadosMais24Horas(UnidadeId, paciente,
                                                                              string.IsNullOrWhiteSpace(enfermariaId) ? Guid.Empty : new Guid(enfermariaId),
                                                                              string.IsNullOrWhiteSpace(LeitoId) ? Guid.Empty : new Guid(LeitoId),
                                                                              dataP,
                                                                              string.IsNullOrWhiteSpace(profissionalId) ? Guid.Empty : new Guid(profissionalId));
                var data = result.ToListResult();
                return Json(data);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }

        }

        [HttpGet("[action]")]
        public IActionResult ListarProducaoDiariaPorExame(int mes, int ano, Guid UnidadeId, String classificacao)
        {
            try
            {
                ListResult result = read.ListarProducaoDiariaPorExame(mes, ano, UnidadeId, classificacao);
                var data = result.ToListResult();
                return Json(data);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }

        }

        [HttpGet("[action]")]
        public IActionResult ListarEstatisticaProducaoExames(string dataInicio, string dataFim, Guid UnidadeId, String classificacao)
        {
            try
            {
                DateTime dinicio = new DateTime(Convert.ToInt32(dataInicio.Substring(6, 4)), Convert.ToInt32(dataInicio.Substring(3, 2)), Convert.ToInt32(dataInicio.Substring(0, 2)));
                DateTime dfim = new DateTime(Convert.ToInt32(dataFim.Substring(6, 4)), Convert.ToInt32(dataFim.Substring(3, 2)), Convert.ToInt32(dataFim.Substring(0, 2)));
                ListResult result = read.ListarEstatisticaProducaoExames(dinicio, dfim, UnidadeId, classificacao);
                var data = result.ToListResult();
                return Json(data);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }

        }

        [HttpGet("[action]")]
        public IActionResult ListarMapaDiarioLeitos(Guid UnidadeId)
        {
            try
            {
                ListResult result = read.ListarMapaDiarioLeitos(UnidadeId);
                var data = result.ToListResult();
                return Json(data);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }

        }

        [HttpGet("[action]")]
        public IActionResult ExtratoContaPaciente(string codigoBoletim)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(codigoBoletim))
                {
                    ExtratoContaPacienteReadModel atendimento = read.ExtratoContaPaciente(codigoBoletim);
                    return Json(atendimento);
                }
                else
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(StatusCodes.Status400BadRequest);
                }
            }
            catch (Exception ex)
            {
                return Json(new ResponseMessage(StatusCodes.Status500InternalServerError, ex));
            }
        }

        [HttpGet("[action]")]
        public IActionResult ListarCensoDiarioUrgenciaEmergencia(Guid unidadeID)
        {
            try
            {
                ListResult result = read.ListarCensoDiarioUrgenciaEmergencia(unidadeID);
                var data = result.ToListResult();
                return Json(data);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpPost("[action]")]
        public void CriaScript()
        {
            // Example #1
            // Read the file as one string.


            //System.IO.StreamReader r = new System.IO.StreamReader("", );

            StringBuilder sb = new StringBuilder();
            string sqlline, line;

            using (var stream = new FileStream(@"C:\Users\tiago\Downloads\TabelaUnificada_201703_v1703031708\tb_procedimento.txt", FileMode.Open))
            {
                using (var reader = new StreamReader(stream, Encoding.GetEncoding("iso-8859-1")))
                {
                    while ((line = reader.ReadLine()) != null)
                    {
                        sqlline = "INSERT INTO[dbo].[Procedimento] ([Id] ,[CO_PROCEDIMENTO] ,[NO_PROCEDIMENTO] ,[TP_COMPLEXIDADE] ,[TP_SEXO] ,[QT_MAXIMA_EXECUCAO] ,[QT_DIAS_PERMANENCIA] ,[QT_PONTOS] ,[VL_IDADE_MINIMA] ,[VL_IDADE_MAXIMA] ,[VL_SH] ,[VL_SA] ,[VL_SP] ,[CO_FINANCIAMENTO] ,[CO_RUBRICA] ,[QT_TEMPO_PERMANENCIA],[DT_COMPETENCIA] ) VALUES (";
                        sqlline += " newid(), '";
                        sqlline += line.Substring(0, 10) + "','";
                        sqlline += line.Substring(10, 250) + "','";
                        sqlline += line.Substring(260, 1) + "','";
                        sqlline += line.Substring(261, 1) + "','";
                        sqlline += line.Substring(262, 4) + "','";
                        sqlline += line.Substring(266, 4) + "','";
                        sqlline += line.Substring(270, 4) + "','";
                        sqlline += line.Substring(274, 4) + "','";
                        sqlline += line.Substring(278, 4) + "','";
                        sqlline += line.Substring(282, 10) + "','";
                        sqlline += line.Substring(292, 10) + "','";
                        sqlline += line.Substring(302, 10) + "','";
                        sqlline += line.Substring(312, 2) + "','";
                        sqlline += line.Substring(314, 6) + "','";
                        sqlline += line.Substring(320, 4) + "','";
                        sqlline += line.Substring(324, 6) + "')";

                        sb.AppendLine(sqlline);
                    }
                }
            }
        }

        [HttpPost("[action]")]
        public void CriaScriptCid()
        {
            StringBuilder sb = new StringBuilder();
            string sqlline, line;
            using (var stream = new FileStream(@"C:\Users\tiago\Downloads\TabelaUnificada_201703_v1703031708\tb_cid.txt", FileMode.Open))
            {
                using (var reader = new System.IO.StreamReader(stream, Encoding.GetEncoding("iso-8859-1")))
                {
                    while ((line = reader.ReadLine()) != null)
                    {
                        sqlline = "INSERT INTO[dbo].[Cid] ([Id] ,[Codigo] ,[Descricao] ,[Agravo] ,[Sexo] ,[Estadio] ,[CamposIrradiados]) VALUES (";
                        sqlline += " newid(), '";
                        sqlline += line.Substring(0, 4) + "','";
                        sqlline += line.Substring(4, 100) + "','";
                        sqlline += line.Substring(104, 1) + "','";
                        sqlline += line.Substring(105, 1) + "','";
                        sqlline += line.Substring(106, 1) + "','";
                        sqlline += line.Substring(107, 4) + "')";
                        sb.AppendLine(sqlline);
                    }
                }
            }
        }

        [HttpPost("[action]")]
        public string CriaScriptRelacionamento()
        {
            StringBuilder sb = new StringBuilder();
            string sqlline, line;

            using (var stream = new FileStream(@"C:\Faturamento\rl_procedimento_cid.txt", FileMode.Open))
            {
                using (var reader = new System.IO.StreamReader(stream, Encoding.GetEncoding("iso-8859-1")))
                {
                    while ((line = reader.ReadLine()) != null)
                    {
                        sqlline = "INSERT INTO procedimentocid (ProcedimentoId ,CidId) VALUES (";
                        sqlline += "(SELECT ID FROM PROCEDIMENTO WHERE CO_PROCEDIMENTO = '" + line.Substring(0, 10) + "'), ";
                        sqlline += "(SELECT ID FROM CID WHERE CODIGO = '" + line.Substring(10, 4) + "')";
                        sqlline += ");";
                        sb.AppendLine(sqlline);
                    }
                }

                return sb.ToString();
            }
        }

        [HttpPost("[action]")]
        public string CriaScriptRelacionamentoFaltantes()
        {
            StringBuilder sb = new StringBuilder();
            string sqlline, line;
            var length = 3;
            var cids = this.readCid.ListarCids();

            using (var stream = new FileStream(@"C:\Faturamento\rl_procedimento_cid.txt", FileMode.Open))
            {
                using (var reader = new System.IO.StreamReader(stream, Encoding.GetEncoding("iso-8859-1")))
                {
                    while ((line = reader.ReadLine()) != null)
                    {
                        if (line.Length == 16)
                            length = 4;

                        var cid = cids.FirstOrDefault(x => x.Codigo == line.Substring(10, length));

                        if (cid != null)
                        {
                            if (cid.Procedimentos.FirstOrDefault(x => x.CO_PROCEDIMENTO == line.Substring(0, 10)) == null)
                            {
                                sqlline = "INSERT INTO[dbo].[PROCEDIMENTOCID] ([Procedimento_Id] ,[Cid_Id]) VALUES (";
                                sqlline += "(SELECT ID FROM PROCEDIMENTO WHERE CO_PROCEDIMENTO = '" + line.Substring(0, 10) + "'), ";
                                sqlline += "(SELECT ID FROM CID WHERE CODIGO = '" + line.Substring(11, length) + "')";
                                sqlline += ")";
                                sb.AppendLine(sqlline);
                            }
                        }

                    }
                }

                return sb.ToString();
            }
        }

        [HttpGet("[action]")]
        public IActionResult ListarAtendimentosFinalizados(Guid idUnidade, Guid idProfissional, string dataInicio, string dataFim)
        {
            DateTime? inicio = null;
            DateTime? fim = null;

            if (!string.IsNullOrWhiteSpace(dataInicio)) 
                inicio = new DateTime(Convert.ToInt32(dataInicio.Substring(6, 4)), Convert.ToInt32(dataInicio.Substring(3, 2)), Convert.ToInt32(dataInicio.Substring(0, 2)));

            if (!string.IsNullOrWhiteSpace(dataFim))
            {
                fim = new DateTime(Convert.ToInt32(dataFim.Substring(6, 4)), Convert.ToInt32(dataFim.Substring(3, 2)), Convert.ToInt32(dataFim.Substring(0, 2)));
                fim = fim.Value.AddDays(1); //necessário por causa do horario
            }

            ListResult result = read.ListarAtendimentosFinalizados(idUnidade, idProfissional, inicio, fim);
            var data = result.ToListResult();

            if (data.Columns.Count != 0)
            {
                data.Columns.FirstOrDefault(x => x.Name == "Id").AllowVisible = false;
                data.Columns.FirstOrDefault(x => x.Name == "ProfissionalId").AllowVisible = false;
                data.Columns.FirstOrDefault(x => x.Name == "AcolhimentoId").AllowVisible = false;
                data.Columns.FirstOrDefault(x => x.Name == "Checkout").AllowVisible = false;
                data.Columns.FirstOrDefault(x => x.Name == "Observacao").AllowVisible = false;
                data.Columns.FirstOrDefault(x => x.Name == "Idade").AllowVisible = false;
                data.Columns.FirstOrDefault(x => x.Name == "Sexo").AllowVisible = false;
                data.Columns.FirstOrDefault(x => x.Name == "InicioAtendimento").ColumnWidth = "50px";
                data.Columns.FirstOrDefault(x => x.Name == "InicioAtendimento").DisplayName = "Início do Atendimento";
                data.Columns.FirstOrDefault(x => x.Name == "FimAtendimento").DisplayName = "Fim do Atendimento";
                data.Columns.FirstOrDefault(x => x.Name == "Boletim").DisplayName = "Boletim";
                data.Columns.FirstOrDefault(x => x.Name == "Nome").ColumnWidth = "50px";
                data.Columns.FirstOrDefault(x => x.Name == "Profissional").DisplayName = "Profissional";
                data.Columns.FirstOrDefault(x => x.Name == "Nome").ColumnWidth = "50px";
                data.Columns.FirstOrDefault(x => x.Name == "Nome").DisplayName = "Paciente";;
                
            }
            return Json(data);
        }


        [HttpGet("[action]")]
        public IActionResult ListarAtendimentosFinalizadosPorPaciente(Guid idUnidade, string filtro)
        {
            ListResult result = read.ListarAtendimentosFinalizados(idUnidade, filtro);
            var data = result.ToListResult();

            if (data.Columns.Count != 0)
            {
                data.Columns.FirstOrDefault(x => x.Name == "Id").AllowVisible = false;
                data.Columns.FirstOrDefault(x => x.Name == "ProfissionalId").AllowVisible = false;
                data.Columns.FirstOrDefault(x => x.Name == "AcolhimentoId").AllowVisible = false;
                data.Columns.FirstOrDefault(x => x.Name == "Checkout").AllowVisible = false;
                data.Columns.FirstOrDefault(x => x.Name == "Observacao").AllowVisible = false;
                data.Columns.FirstOrDefault(x => x.Name == "Idade").AllowVisible = false;
                data.Columns.FirstOrDefault(x => x.Name == "Sexo").AllowVisible = false;
                data.Columns.FirstOrDefault(x => x.Name == "InicioAtendimento").ColumnWidth = "50px";
                data.Columns.FirstOrDefault(x => x.Name == "InicioAtendimento").DisplayName = "Início do Atendimento";
                data.Columns.FirstOrDefault(x => x.Name == "FimAtendimento").DisplayName = "Fim do Atendimento";
                data.Columns.FirstOrDefault(x => x.Name == "Boletim").DisplayName = "Boletim";
                data.Columns.FirstOrDefault(x => x.Name == "Nome").ColumnWidth = "50px";
                data.Columns.FirstOrDefault(x => x.Name == "Profissional").DisplayName = "Profissional";
                data.Columns.FirstOrDefault(x => x.Name == "Nome").ColumnWidth = "50px";
                data.Columns.FirstOrDefault(x => x.Name == "Nome").DisplayName = "Paciente"; ;

            }
            return Json(data);
        }

    }
}
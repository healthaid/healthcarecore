﻿using HealthCare.Infra.Common.Extensions;
using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.CQRS.Write.Commands;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;


namespace HealthCare.WebApi.Controllers
{
    [EnableCors("AllowCors")]
    [Route("api/[controller]")]
    public class LeitoController : Controller
    {
        private readonly ICommandBus bus;
        private readonly ILeitoDao read;
        private readonly IEnfermariaDao readEnfermaria;

        public LeitoController(ICommandBus _bus, ILeitoDao _read, IEnfermariaDao readEnfermaria)
        {
            this.bus = _bus;
            this.read = _read;
            this.readEnfermaria = readEnfermaria;
        }

        [HttpPost("[action]")]
        public IActionResult SalvarLeito([FromBody]SaveLeitoCommand model)
        {
            try
            {
                object objReturned = bus.SendWithReturn(model);
                if (objReturned.GetType() == typeof(Validation))
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(((Validation)objReturned).Messages);
                }
                return Json(objReturned);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpGet("[action]")]
        public IActionResult ObterLeitoPorId(string id)
        {
            try
            {
                LeitoReadModel receita = read.GetLeitoById(Guid.Parse(id));
                return Json(receita);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpDelete("[action]")]
        public IActionResult DeleteLeito(string id)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(id) == false)
                {
                    DeleteLeitoCommand command = new DeleteLeitoCommand();
                    command.LeitoId = Guid.Parse(id);
                    object objReturned = bus.SendWithReturn(command);
                    if (objReturned.GetType() != typeof(Validation))
                        return Json(StatusCodes.Status200OK);
                    else
                    {
                        return Json(((Validation)objReturned).Messages);
                    }
                }
                else
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(StatusCodes.Status400BadRequest);
                }
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpGet("[action]")]
        public IActionResult ListarLeitos()
        {
            try
            {
                ListResult result = read.ListaLeitos(true);
                var data = result.ToListResult();

                if (data.Columns.Count != 0)
                {
                    data.Columns.FirstOrDefault(x => x.Name == "Id").AllowVisible = false;
                    data.Columns.FirstOrDefault(x => x.Name == "Nome").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "Nome").DisplayName = "Exame";
                }
                return Json(data);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpGet("[action]")]
        public IActionResult ListarLeitosDisponiveis()
        {
            try
            {
                ListResult result = read.ListarLeitosDisponiveis();
                var data = result.ToListResult();

                if (data.Columns.Count != 0)
                {
                    data.Columns.FirstOrDefault(x => x.Name == "Id").AllowVisible = false;
                    data.Columns.FirstOrDefault(x => x.Name == "Nome").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "Nome").DisplayName = "Exame";
                    data.Columns.FirstOrDefault(x => x.Name == "TipoLeitoPorExtenso").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "TipoLeitoPorExtenso").DisplayName = "Tipo de Leito";
                    data.Columns.FirstOrDefault(x => x.Name == "Enfermaria").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "Enfermaria").DisplayName = "Enfermaria";
                }
                return Json(data);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpGet("[action]")]
        public IActionResult ListarLeitosPorEnfermaria(Guid enfermariaId)
        {
            try
            {
                ListResult result = read.ListarLeitosPorEnfermaria(enfermariaId, true);
                var data = result.ToListResult();

                if (data.Columns.Count != 0)
                {
                    data.Columns.FirstOrDefault(x => x.Name == "Id").AllowVisible = false;
                    data.Columns.FirstOrDefault(x => x.Name == "Nome").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "Nome").DisplayName = "Exame";
                }
                return Json(data);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpGet("[action]")]
        public IActionResult ListarLeitosDisponiveisPorEnfermaria(string unidadeId)
        {
            try
            {
                ListResult result = read.ListarLeitosDisponiveisPorEnfermaria(Guid.Parse(unidadeId));
                var data = result.ToListResult();
                return Json(data);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpGet("[action]")]
        public IActionResult ListarLeitosOcupadosPorEnfermaria(string id)
        {
            try
            {
                ListResult result = read.ListarLeitosOcupadosPorEnfermaria(Guid.Parse(id));
                var data = result.ToListResult();
                return Json(data);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpGet("[action]")]
        public IActionResult ListarLeitosOcupadosParaPermuta(string id, string pacienteSelecionadoId)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(id) && !string.IsNullOrWhiteSpace(pacienteSelecionadoId))
                {
                    ListResult result = read.ListarLeitosOcupadosParaPermuta(Guid.Parse(id), Guid.Parse(pacienteSelecionadoId));
                    var data = result.ToListResult();
                    return Json(data);
                }
                else
                    return Json(string.Empty);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }
    }
}
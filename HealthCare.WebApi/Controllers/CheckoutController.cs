﻿using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.CQRS.Write.Commands;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;

namespace HealthCare.WebApi.Controllers
{
    [EnableCors("AllowCors")]
    [Route("api/[controller]")]
    public class CheckoutController : Controller
    {
        private readonly ICommandBus bus;

        public CheckoutController(ICommandBus bus)
        {
            this.bus = bus;
        }

        [HttpPost("[action]")]
        public IActionResult SalvarCheckout([FromBody]CreateCheckoutCommand command)
        {
            try
            {
                if (command != null)
                {
                    object obj = bus.SendWithReturn(command);
                    if (obj.GetType() == typeof(Validation))
                        return Json(((Validation)obj).Messages);
                    else
                        return Json(new ResponseMessage(StatusCodes.Status200OK));
                }
                else
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(StatusCodes.Status400BadRequest);
                }
            }
            catch (Exception ex)
            {
                return Json("Ocorreu um erro na hora de salvar o checkout: " + ex.Message);
            }
        }
    }
}

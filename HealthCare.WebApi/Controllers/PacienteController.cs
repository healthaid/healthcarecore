﻿using HealthCare.Infra.Common.Extensions;
using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.CQRS.Write.Commands;
using HealthCare.Service.CQRS.Write.Commands;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HealthCare.WebApi.Controllers
{

    public class InterceptionAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext actionContext)
        {
            var x = "This is my custom line of code I need executed before any of the controller actions";
            base.OnActionExecuting(actionContext);
        }
    }

    [EnableCors("AllowCors")]
    [Route("api/[controller]")]
    public class PacienteController : Controller
    {

        private readonly ICommandBus bus;
        private readonly IPacienteDao read;
        private readonly ICidadeDao readCidade;
        private readonly IBairroDao readBairro;
        private readonly ICNSDao readCNS;
        private readonly IEnderecoDao readEndereco;
        private readonly IEscolaridadeDao readEscolaridade;
        private readonly IEstadoCivilDao readEstadoCivil;
        private readonly IEtniaDao readEtnia;
        private readonly INaturalidadeDao readNaturalidade;
        private readonly IPlanoDao readPlano;
        private readonly IProfissaoDao readProfissao;
        private readonly IUFDao readUF;
        private readonly IPaisDao readPais;


        public PacienteController(ICommandBus bus, IPacienteDao read, ICidadeDao readCidade, IBairroDao readBairro, ICNSDao readCNS, IEnderecoDao readEndereco,
                                  IEscolaridadeDao readEscolaridade, IEstadoCivilDao readEstadoCivil, IEtniaDao readEtnia, INaturalidadeDao readNaturalidade,
                                  IPlanoDao readPlano, IProfissaoDao readProfissao, IUFDao readUF, IPaisDao readPais)
        {
            this.bus = bus;
            this.read = read;
            this.readCidade = readCidade;
            this.readBairro = readBairro;
            this.readCNS = readCNS;
            this.readEndereco = readEndereco;
            this.readEscolaridade = readEscolaridade;
            this.readEstadoCivil = readEstadoCivil;
            this.readEtnia = readEtnia;
            this.readNaturalidade = readNaturalidade;
            this.readPlano = readPlano;
            this.readProfissao = readProfissao;
            this.readUF = readUF;
            this.readPais = readPais;
        }

        [HttpPost("[action]")]
        public IActionResult SalvarPaciente([FromBody]SavePacienteCommand model)
        {
            try
            {
                object objReturned = bus.SendWithReturn(model);
                if (objReturned.GetType() == typeof(Validation))
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(((Validation)objReturned).Messages);
                }
                return Json(objReturned);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpGet("[action]")]
        public IActionResult ListaPacientes(string filtro, string unidadeId)
        {
            try
            {
                ListResult result = read.ListarPacientes(filtro, Guid.Parse(unidadeId));
                var data = result.ToListResult();
                data.AllowActions = true;
                if (data.Columns.Count != 0)
                {
                    data.Columns.FirstOrDefault(x => x.Name == "Id").AllowVisible = false;
                    data.Columns.FirstOrDefault(x => x.Name == "Nome").ColumnWidth = "200px";
                    data.Columns.FirstOrDefault(x => x.Name == "Nome").DisplayName = "Nome";
                    data.Columns.FirstOrDefault(x => x.Name == "NomeMae").DisplayName = "Nome da Mãe";
                    data.Columns.FirstOrDefault(x => x.Name == "DataNascimento").DisplayName = "Data de Nascimento";
                }
                return Json(data);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpGet("[action]")]
        public IActionResult GetPacienteById(string pacienteId)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(pacienteId) == false)
                {
                    PacienteReadModel paciente = read.GetPacienteById(Guid.Parse(pacienteId));
                    return Json(paciente);
                }
                else
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(StatusCodes.Status400BadRequest);
                }
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpGet("[action]")]
        public IActionResult GetPacienteByAtendimentoId(string atendimentoId)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(atendimentoId) == false)
                {
                    PacienteReadModel paciente = read.GetPacienteByAtendimentoId(Guid.Parse(atendimentoId));
                    return Json(paciente);
                }
                else
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(StatusCodes.Status400BadRequest);
                }
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpDelete("[action]")]
        public IActionResult DeletePaciente(string pacienteId)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(pacienteId) == false)
                {
                    DeletePacienteCommand command = new DeletePacienteCommand();
                    command.PacienteId = Guid.Parse(pacienteId);
                    object objReturned = bus.SendWithReturn(command);
                    if (objReturned.GetType() != typeof(Validation))
                        return Json(StatusCodes.Status200OK);
                    else
                    {
                        return Json(((Validation)objReturned).Messages);
                    }
                }
                else
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(StatusCodes.Status400BadRequest);
                }
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpGet("[action]")]
        public IQueryable<PacienteReadModel> ListarPacientes(string unidadeId)
        {
            var pacientes = read.ListarPacientesAtivos(Guid.Parse(unidadeId)).AsQueryable();
            return pacientes;
        }


        [HttpGet("[action]")]
        public IList<CidadeReadModel> ListaCidades(Guid idUf)
        {
            try
            {
                return readCidade.ListarCidadeporUF(idUf);
                //ListResult result = readCidade.ListarCidadesporUF(idUf);
                //var data = result.ToListResult();
                //data.AllowActions = true;
                //if (data.Columns.Count != 0)
                //{
                //    data.Columns.FirstOrDefault(x => x.Name == "Id").AllowVisible = false;
                //    data.Columns.FirstOrDefault(x => x.Name == "Nome").ColumnWidth = "200px";
                //    data.Columns.FirstOrDefault(x => x.Name == "Nome").DisplayName = "Descricao";
                //}
                //return Request.CreateResponse(HttpStatusCode.OK, data);
            }
            catch (Exception ex)
            {
                throw ex;
                //return Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpGet("[action]")]
        public IList<UFReadModel> ListaUF()
        {
            try
            {
                return readUF.ListarUFs();
                //ListResult result = 
                //var data = result.ToListResult();
                //data.AllowActions = true;
                //if (data.Columns.Count != 0)
                //{
                //    data.Columns.FirstOrDefault(x => x.Name == "Id").AllowVisible = false;
                //    data.Columns.FirstOrDefault(x => x.Name == "Descricao").ColumnWidth = "200px";
                //    data.Columns.FirstOrDefault(x => x.Name == "Descricao").DisplayName = "Descricao";
                //}
                //return Request.CreateResponse(HttpStatusCode.OK, data);
            }
            catch (Exception ex)
            {
                throw ex;
                //return Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpGet("[action]")]
        public IActionResult ListarPais()
        {
            try
            {
                ListResult result = readPais.ListaPaises();
                var data = result.ToListResult();
                data.AllowActions = true;
                if (data.Columns.Count != 0)
                {
                    data.Columns.FirstOrDefault(x => x.Name == "Id").AllowVisible = false;
                    data.Columns.FirstOrDefault(x => x.Name == "Descricao").ColumnWidth = "200px";
                    data.Columns.FirstOrDefault(x => x.Name == "Descricao").DisplayName = "Descricao";
                }
                return Json(data);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }
    }
}

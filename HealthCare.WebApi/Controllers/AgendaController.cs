﻿using HealtCare.Infra.Common.Utils;
using HealthCare.Data.Messaging;
using HealthCare.Data.Messaging.Implementation;
using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.CQRS.Messaging.Handling;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.CQRS.Serialization;
using HealthCare.Infra.CQRS.Write.Commands;
using HealthCare.Infra.Setup.App_Start;
using HealthCare.Infra.Setup.IoC.Configurations;
using HealthCare.Service.CQRS.Write.Commands;
using HealthCare.Service.CQRS.Write.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.OData;
using HealthCare.Infra.Common.Extensions;
namespace HealthCare.WebApi.Controllers
{
    public class AgendaController : ApiController
    {

        private readonly ICommandBus bus;
        private readonly IAgendaDao read;
        private readonly IAgendaEventoDao readEvento;
        private readonly IPacienteDao readPaciente;
        private readonly IProfissionalDao readProfissional;
        private readonly IServicoDao readServico;

        public AgendaController(ICommandBus bus, IAgendaDao read, IAgendaEventoDao readEvento, IPacienteDao readPaciente, IProfissionalDao readProfissional, IServicoDao readServico)
        {
            this.bus = bus;
            this.read = read;
            this.readEvento = readEvento;
            this.readPaciente = readPaciente;
            this.readProfissional = readProfissional;
            this.readServico = readServico;
        }



        [HttpGet]
        [Route("ListarAgendas")]
        public IQueryable<AgendaReadViewModel> ListarAgendas(string unidadeId)
        {
            return read.ListarAgendas(Guid.Parse(unidadeId)).AsQueryable();
        }


        [HttpGet]
        [EnableQuery]
        [Route("ListarEventosPorMes")]
        public IQueryable<AgendaEventoTesteModel> ListarEventosPorMes(int mes, int ano, string unidade)
        {
            var ret = (from a in readEvento.ListarEventosPorMes(mes, ano, Guid.Parse(unidade))
                       select new AgendaEventoTesteModel
                       {
                           start = a.HoraInicio,
                           end = a.HoraFim,
                           title = string.Format("{0} - {1}", a.HoraInicio, a.Agenda.Profissional.Nome), //  a.Agenda != null ? a.Agenda.Nome : string.Empty,
                           id = a.Id,
                           PacienteId = a.Paciente != null ? a.Paciente.Id : Guid.Empty,
                           ServicoId = a.Servico != null ? a.Servico.Id : Guid.Empty,
                           profissionalNome = a.Agenda.Profissional.Nome//(a.Agenda != null && a.Agenda.Profissional != null) ? a.Agenda.Profissional.Nome : string.Empty
                       }).AsQueryable();

            return ret;
        }

        [HttpGet]
        [EnableQuery]
        [Route("ListarEventosPorAno")]
        public IQueryable<AgendaEventoTesteModel> ListarEventosPorAno(int ano, string unidadeId)
        {

            var ret = (from a in readEvento.ListarEventosPorAno(ano, Guid.Parse(unidadeId))
                       select new AgendaEventoTesteModel
                       {
                           start = a.HoraInicio,
                           end = a.HoraFim,
                           //title = string.Format("{0} <br> {1}  {2}", a.Agenda.HoraInicio.ToString("HH:mm"), a.Agenda.Profissional.Nome, (a.Paciente != null ? "<b>" + a.Paciente.Nome + "</b>" : string.Empty)), //  a.Agenda != null ? a.Agenda.Nome : string.Empty,s
                           title = string.Format("{0} - {1}", a.HoraInicio.ToString("HH:mm"), a.Agenda.Profissional.Nome),
                           id = a.Id,
                           PacienteId = a.Paciente != null ? a.Paciente.Id : Guid.Empty,
                           ServicoId = a.Servico != null ? a.Servico.Id : Guid.Empty,
                           profissionalNome = (a.Agenda != null && a.Agenda.Profissional != null) ? a.Agenda.Profissional.Nome : string.Empty,
                           ProfissionalId = a.Agenda.ProfissionalId,
                           allDay = false,
                           color = a.Paciente != null ? System.Drawing.Color.LightBlue.Name : System.Drawing.Color.LightGreen.Name,
                           stick = true
                       }).AsQueryable();

            return ret;
        }

        private Color GetRandColor()
        {
            return System.Drawing.Color.LightGreen;
            //    Random r = new Random(DateTime.Now.Millisecond);

            //    System.Drawing.Color[] colours = 
            //{
            //    System.Drawing.Color.Yellow, 
            //    System.Drawing.Color.LightGreen, 
            //    System.Drawing.Color.LightCyan,
            //    System.Drawing.Color.LightSalmon,  
            //    System.Drawing.Color.LightSkyBlue
            //};

            //    int i = r.Next(0, colours.Length - 1);

            //    return colours[i];
        }

        [HttpGet]
        [EnableQuery]
        [Route("ListarEventos")]
        public IQueryable<AgendaEventoTesteModel> ListarEventos(Guid? agendaId = null)
        {
            //agendaId = 
            var ret = (from a in readEvento.ListarEventos(new Guid("2C8DFA9A-EC21-E611-A56C-005056C00008"))
                       select new AgendaEventoTesteModel
                       {
                           start = a.HoraInicio,
                           end = a.HoraFim,
                           title = a.Agenda != null ? a.Agenda.Nome : string.Empty,
                           id = a.Id,
                           PacienteId = a.Paciente != null ? a.Paciente.Id : Guid.Empty,
                           ServicoId = a.Servico != null ? a.Servico.Id : Guid.Empty,
                           profissionalNome = (a.Agenda != null && a.Agenda.Profissional != null) ? a.Agenda.Profissional.Nome : string.Empty
                       }).AsQueryable();

            return ret;
        }

        [HttpGet]
        [EnableQuery]
        [Route("ListarEventosPorProfissional")]
        public IQueryable<AgendaEventoWriteModel> ListarEventosPorProfissional(Guid profissionalId)
        {
            return (from a in readEvento.ListarEventosPorProfissional(profissionalId)
                    select new AgendaEventoWriteModel
                    {
                        HoraFim = a.HoraFim,
                        HoraInicio = a.HoraInicio,
                        AgendaNome = a.Agenda.Nome
                    }).AsQueryable();
        }

        [HttpGet]
        [Route("ListarEventosPorPaciente")]
        public IQueryable<AgendaEventoWriteModel> ListarEventosPorPaciente(Guid pacienteId)
        {
            return (from a in readEvento.ListarEventosPorPaciente(pacienteId)
                    select new AgendaEventoWriteModel
                    {
                        HoraFim = a.HoraFim,
                        HoraInicio = a.HoraInicio,
                        AgendaNome = a.Agenda.Nome
                    }).AsQueryable();
        }

        [HttpPost]
        [Route("CadastrarAgenda")]
        public HttpResponseMessage CadastrarAgenda(AgendaWriteModel model)
        {
            try
            {
                object objReturned = bus.SendWithReturn(ViewModelUtils.ToViewModel(model));
                if (objReturned.GetType() == typeof(Validation))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, ((Validation)objReturned).Messages);
                }
                return Request.CreateResponse(HttpStatusCode.OK, objReturned);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpPut]
        [Route("AtualizarEvento")]
        public HttpResponseMessage AtualizarEvento(PutAgendaEventoWriteModel model)
        {
            Validation v = bus.SendWithReturn(ViewModelUtils.ToModel(model));
            if (v.IsValid)
                return Request.CreateResponse(HttpStatusCode.OK);
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, v.Messages);
        }

        [HttpPut]
        [Route("CancelarAgendamento")]
        public HttpResponseMessage CancelarAgendamento(CancelarAgendaEventoWriteModel model)
        {
            Validation v = bus.SendWithReturn(ViewModelUtils.ToModel(model));
            if (v.IsValid)
                return Request.CreateResponse(HttpStatusCode.OK);
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, v.Messages);
        }

        [HttpGet]
        [EnableQuery]
        [Route("ListarProfissional")]
        public IQueryable<ProfissionalWriteModel> ListarProfissional(string unidadeId)
        {

            var profissionais = (from a in readProfissional.ListarProfissional(Guid.Parse(unidadeId))
                                 select new ProfissionalWriteModel
                                 {
                                     Id = a.Id
                                  ,
                                     Nome = a.Nome
                                 }).AsQueryable();

            return profissionais;

        }

        [HttpGet]
        [EnableQuery]
        [Route("ListarServico")]
        public IQueryable<CreateServicoCommand> ListarServicos()
        {

            var servico = (from a in readServico.ListarServicos()
                           select new CreateServicoCommand
                           {
                               ServicoId = a.Id
                             ,
                               Descricao = a.Descricao
                           }).AsQueryable();

            return servico;
        }

        [HttpGet]
        [EnableQuery]
        [Route("ListarServicosPorProfissional")]
        public IList<CreateServicoCommand> ListarServicosPorProfissional(Guid idProfissional)
        {
            var servico = (from a in readServico.ListarServicosPorProfissional(idProfissional)
                           select new CreateServicoCommand
                           {
                               ServicoId = a.Id
                             ,
                               Descricao = a.Descricao
                           }).ToList();

            return servico;
        }

        [HttpGet]
        [EnableQuery]
        [Route("ListarFilaProfissional")]
        public HttpResponseMessage ListarFilaProfissional(string idProfissional)
        {
            try
            {
                ListResult result = readEvento.ListarFilaProfissional(Guid.Parse(idProfissional));
                var data = result.ToListResult();
                data.AllowActions = true;
                if (data.Columns.Count != 0)
                {
                    data.Columns.FirstOrDefault(x => x.Name == "Id").AllowVisible = false;
                    data.Columns.FirstOrDefault(x => x.Name == "Nome").ColumnWidth = "200px";
                    data.Columns.FirstOrDefault(x => x.Name == "Nome").DisplayName = "Paciente";
                    data.Columns.FirstOrDefault(x => x.Name == "Descricao").DisplayName = "Serviço";
                    data.Columns.FirstOrDefault(x => x.Name == "HoraInicio").DisplayName = "Hora";
                }
                return Request.CreateResponse(HttpStatusCode.OK, data);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
            }
        }


        [HttpGet]
        [EnableQuery]
        [Route("ListarEventosPorSemana")]
        public IQueryable<AgendaEventoTesteModel> ListarEventosPorSemana(string inicio, string fim, string profissionalId)
        {
            DateTime dataInicio = Convert.ToDateTime(inicio.Split('G').ElementAt(0));
            DateTime dataFim = Convert.ToDateTime(fim.Split('G').ElementAt(0));
            Guid id = Guid.Parse(profissionalId);

            var ret = (from a in readEvento.ListarEventosPorSemana(dataInicio, dataFim, id)
                       select new AgendaEventoTesteModel
                       {
                           start = a.HoraInicio,
                           end = a.HoraFim,
                           //title = string.Format("{0} <br> {1}  {2}", a.Agenda.HoraInicio.ToString("HH:mm"), a.Agenda.Profissional.Nome, (a.Paciente != null ? "<b>" + a.Paciente.Nome + "</b>" : string.Empty)), //  a.Agenda != null ? a.Agenda.Nome : string.Empty,s
                           title = string.Format("{0} - {1}", a.HoraInicio.ToString("HH:mm"), a.Agenda.Profissional.Nome),
                           id = a.Id,
                           PacienteId = a.Paciente != null ? a.Paciente.Id : Guid.Empty,
                           ServicoId = a.Servico != null ? a.Servico.Id : Guid.Empty,
                           profissionalNome = (a.Agenda != null && a.Agenda.Profissional != null) ? a.Agenda.Profissional.Nome : string.Empty,
                           ProfissionalId = a.Agenda.ProfissionalId,
                           allDay = false,
                           color = a.Paciente != null ? System.Drawing.Color.LightBlue.Name : System.Drawing.Color.LightGreen.Name,
                           stick = true
                       }).AsQueryable();

            return ret;
        }

    }
}

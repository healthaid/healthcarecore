﻿using HealthCare.Infra.Common.Extensions;
using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.CQRS.Read.Interfaces.Atendimento;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.CQRS.Write.Commands;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;


namespace HealthCare.WebApi.Controllers
{
    [EnableCors("AllowCors")]
    [Route("api/[controller]")]
    public class EvolucaoEnfermagemController : Controller
    {
        private readonly ICommandBus bus;
        private readonly IEvolucaoEnfermagemDto read;
        private readonly ILiquidosAdministradosDto readLiquidosAdministrados;
        private readonly ILiquidosEliminadosDto readLiquidosEliminados;


        public EvolucaoEnfermagemController(ICommandBus _bus, IEvolucaoEnfermagemDto _read, ILiquidosAdministradosDto _readLiquidosAdministrados, ILiquidosEliminadosDto _readLiquidosEliminados)
        {
            this.bus = _bus;
            this.read = _read;
            this.readLiquidosAdministrados = _readLiquidosAdministrados;
            this.readLiquidosEliminados = _readLiquidosEliminados;
        }

        [HttpPost("[action]")]
        public IActionResult SalvarEvolucaoEnfermagem([FromBody]SaveEvolucaoEnfermagemCommand model)
        {
            try
            {
                object objReturned = bus.SendWithReturn(model);
                if (objReturned.GetType() == typeof(Validation))
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(((Validation)objReturned).Messages);
                }
                return Json(objReturned);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpPost("[action]")]
        public IActionResult SalvarLiquidosAdministradosEnfermagem([FromBody]SaveLiquidosAdministradosEnfermagemCommand model)
        {
            try
            {
                object objReturned = bus.SendWithReturn(model);
                if (objReturned.GetType() == typeof(Validation))
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(((Validation)objReturned).Messages);
                }
                return Json(objReturned);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpPost("[action]")]
        public IActionResult SalvarLiquidosEliminadosEnfermagem([FromBody]SaveLiquidosEliminadosEnfermagemCommand model)
        {
            try
            {
                object objReturned = bus.SendWithReturn(model);
                if (objReturned.GetType() == typeof(Validation))
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(((Validation)objReturned).Messages);
                }
                return Json(objReturned);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpPost("[action]")]
        public IActionResult DeleteLiquidosEliminados([FromBody]DeleteLiquidoEliminadosEnfermagemCommand model)
        {
            try
            {
                object objReturned = bus.SendWithReturn(model);
                if (objReturned.GetType() == typeof(Validation))
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    Validation v = (Validation)objReturned;
                    if (v.Messages.Count() > 0)
                        return Json(((Validation)objReturned).Messages);
                }
                return Json(new ResponseMessage(StatusCodes.Status200OK, objReturned));
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpPost("[action]")]
        public IActionResult DeleteLiquidosAdministrados([FromBody]DeleteLiquidosAdministradosEnfermagemCommand model)
        {
            try
            {
                object objReturned = bus.SendWithReturn(model);
                if (objReturned.GetType() == typeof(Validation))
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    Validation v = (Validation)objReturned;
                    if (v.Messages.Count() > 0)
                        return Json(((Validation)objReturned).Messages);
                }
                return Json(objReturned);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpPost("[action]")]
        public IActionResult DeleteProcedimentosPorEnfermagem([FromBody]DeleteProcedimentosEnfermagemCommand model)
        {
            try
            {
                object objReturned = bus.SendWithReturn(model);
                if (objReturned.GetType() == typeof(Validation))
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(((Validation)objReturned).Messages);
                }
                return Json(objReturned);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpPost("[action]")]
        public IActionResult SalvarProcedimentosEnfermagem([FromBody]SaveProcedimentosEnfermagemCommand model)
        {
            try
            {
                object objReturned = bus.SendWithReturn(model);
                if (objReturned.GetType() == typeof(Validation))
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(((Validation)objReturned).Messages);
                }
                return Json(objReturned);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpGet("[action]")]
        public IActionResult ObterEvolucaoEnfermagemPorId(string id)
        {
            try
            {
                EvolucaoEnfermagemReadModel receita = read.GetEvolucaoEnfermagemById(Guid.Parse(id));
                return Json(receita);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpGet("[action]")]
        public IActionResult ListaHistoricoEnfermagem(string id)
        {
            try
            {
                ListResult result = read.ListaHistoricoEnfermagem(Guid.Parse(id));
                var data = result.ToListResult();

                if (data.Columns.Count != 0)
                {
                    data.Columns.FirstOrDefault(x => x.Name == "HoraEvolucao").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "HoraEvolucao").DisplayName = "Data";
                    data.Columns.FirstOrDefault(x => x.Name == "Evolucao").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "Evolucao").DisplayName = "Evolução";
                    data.Columns.FirstOrDefault(x => x.Name == "NomeProfissional").ColumnWidth = "100px";
                    data.Columns.FirstOrDefault(x => x.Name == "NomeProfissional").DisplayName = "Profissional";
                }
                return Json(data);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpGet("[action]")]
        public IActionResult ListaLiquidosAdministrados()
        {
            try
            {
                var data = readLiquidosAdministrados.ListarLiquidosAdministrados();
                return Json(data);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpGet("[action]")]
        public IActionResult ListaLiquidosAdministradosPorEvolucao(Guid id)
        {
            try
            {
                ListResult result = read.ListaLiquidosAdministrados(id);
                var data = result.ToListResult();

                if (data.Columns.Count != 0)
                {
                    data.Columns.FirstOrDefault(x => x.Name == "Id").AllowVisible = false;
                    data.Columns.FirstOrDefault(x => x.Name == "Descricao").ColumnWidth = "100px";
                    data.Columns.FirstOrDefault(x => x.Name == "Descricao").DisplayName = "Descrição";
                }
                return Json(data);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpGet("[action]")]
        public IActionResult ListaLiquidosEliminadosPorEvolucao(Guid id)
        {
            try
            {
                ListResult result = read.ListaLiquidosEliminados(id);
                var data = result.ToListResult();

                if (data.Columns.Count != 0)
                {
                    data.Columns.FirstOrDefault(x => x.Name == "Id").AllowVisible = false;
                    data.Columns.FirstOrDefault(x => x.Name == "Descricao").ColumnWidth = "100px";
                    data.Columns.FirstOrDefault(x => x.Name == "Descricao").DisplayName = "Descrição";
                }
                return Json(data);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpGet("[action]")]
        public IActionResult ListaProcedimentosPorEvolucao(Guid id)
        {
            try
            {
                ListResult result = read.ListaProcedimentosPorEvolucao(id);
                var data = result.ToListResult();

                if (data.Columns.Count != 0)
                {
                    data.Columns.FirstOrDefault(x => x.Name == "Id").AllowVisible = false;
                    data.Columns.FirstOrDefault(x => x.Name == "DescricaoProcedimento").ColumnWidth = "100px";
                    data.Columns.FirstOrDefault(x => x.Name == "DescricaoProcedimento").DisplayName = "Procedimento";
                    data.Columns.FirstOrDefault(x => x.Name == "Observacao").ColumnWidth = "100px";
                    data.Columns.FirstOrDefault(x => x.Name == "Observacao").DisplayName = "Observação";
                    data.Columns.FirstOrDefault(x => x.Name == "Quantidade").ColumnWidth = "100px";
                    data.Columns.FirstOrDefault(x => x.Name == "Quantidade").DisplayName = "Quantidade";
                }
                return Json(data);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpGet("[action]")]
        public IActionResult ListarLiquidosEliminados()
        {
            try
            {
                var data = readLiquidosEliminados.ListarLiquidosEliminados();
                return Json(data);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }
    }
}
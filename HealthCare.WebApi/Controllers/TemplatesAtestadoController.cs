﻿using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.CQRS.Write.Commands;
using HealthCare.Infra.CQRS.Write.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;

namespace HealthCare.WebApi.Controllers
{
    [EnableCors("AllowCors")]
    [Route("api/[controller]")]
    public class TemplatesAtestadoController : Controller
    {
        private readonly ICommandBus bus;
        private readonly ITemplatesAtestadoDto read;

        public TemplatesAtestadoController(ICommandBus bus, ITemplatesAtestadoDto read)
        {
            this.bus = bus;
            this.read = read;
        }

        [HttpGet("[action]")]
        public TemplatesAtestadoReadViewModel GetTemplatesAtestadoPorId(Guid id)
        {
            var R = read.ObterTemplatesAtestado(id);
            return R;
        }


        [HttpPost("[action]")]
        public void SalvarTemplatesAtestado([FromBody]TemplatesAtestadoWriteModel model)
        {
            if (model.Id == Guid.Empty)
            {
                CreateTemplatesAtestadoCommand createCommand = new CreateTemplatesAtestadoCommand();
                //createCommand.Id = model.Id;
                createCommand.Conteudo = model.Conteudo;
                createCommand.Nome = model.Nome;
                bus.Send(createCommand);
            }
            else
            {
                UpdateTemplatesAtestadoCommand updateCommand = new UpdateTemplatesAtestadoCommand();
                updateCommand.Id = model.Id;
                updateCommand.Conteudo = model.Conteudo;
                updateCommand.Nome = model.Nome;
                bus.Send(updateCommand);
            }
        }

     
    }
}
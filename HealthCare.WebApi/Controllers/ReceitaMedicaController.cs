﻿using HealthCare.Infra.Common.Extensions;
using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.CQRS.Read.Interfaces.Atendimento;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.CQRS.Write.Commands;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace HealthCare.WebApi.Controllers
{
    [EnableCors("AllowCors")]
    [Route("api/[controller]")]
    public class ReceitaMedicaController : Controller
    {
        private readonly ICommandBus bus;
        private readonly IAtendimentoDto readAtendimento;
        private readonly IReceitaMedicaDto read;

        public ReceitaMedicaController(ICommandBus _bus, IAtendimentoDto _readAtendimento, IReceitaMedicaDto _read)
        {
            this.bus = _bus;
            this.readAtendimento = _readAtendimento;
            this.read = _read;
        }

        [HttpPost("[action]")]
        public IActionResult SalvarReceitaMedica([FromBody]SaveReceitaMedicaCommand model)
        {
            try
            {
                object objReturned = bus.SendWithReturn(model);
                if (objReturned.GetType() == typeof(Validation))
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(((Validation)objReturned).Messages);
                }
                return Json(objReturned);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpGet("[action]")]
        public IActionResult ObterReceitaMedicaPorId(string id)
        {
            try
            {
                ReceitaMedicaReadModel receita = read.ObterReceitaMedicaPorId(Guid.Parse(id));
                return Json(receita);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpGet("[action]")]
        public IActionResult ListarReceitaMedicaPorAtendimento(string atendimentoId)
        {
            try
            {
                ListResult result = read.ObterReceitasMedicaPorAtendimentoId(Guid.Parse(atendimentoId));
                var data = result.ToListResult();

                if (data.Columns.Count != 0)
                {
                    data.Columns.FirstOrDefault(x => x.Name == "Id").AllowVisible = false;
                    data.Columns.FirstOrDefault(x => x.Name == "Medicamento").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "Medicamento").DisplayName = "Medicamento";
                    data.Columns.FirstOrDefault(x => x.Name == "Dose").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "Dose").DisplayName = "Dose";
                    data.Columns.FirstOrDefault(x => x.Name == "Unidade").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "Unidade").DisplayName = "Unidade";
                    data.Columns.FirstOrDefault(x => x.Name == "ViaAdministracao").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "ViaAdministracao").DisplayName = "Via de Administração";
                    data.Columns.FirstOrDefault(x => x.Name == "Intervalo").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "Intervalo").DisplayName = "Intervalo";
                    data.Columns.FirstOrDefault(x => x.Name == "Duracao").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "Duracao").DisplayName = "Duração";
                }
                return Json(data);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpDelete("[action]")]
        public IActionResult DeleteReceitaMedica(string id)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(id) == false)
                {
                    DeleteReceitaMedicaCommand command = new DeleteReceitaMedicaCommand();
                    command.DeleteId = Guid.Parse(id);
                    object objReturned = bus.SendWithReturn(command);
                    if (objReturned.GetType() != typeof(Validation))
                        return Json(StatusCodes.Status200OK);
                    else
                        return Json(((Validation)objReturned).Messages);
                }
                else
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(StatusCodes.Status400BadRequest);
                }
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpGet("[action]")]
        public IActionResult ImpressaoReceita(string atendimentoId)
        {
            try
            {
                var data = read.ImpressaoReceita(Guid.Parse(atendimentoId));
                return Json(data);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }


    }
}
﻿using HealthCare.Infra.Common.Extensions;
using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.CQRS.Read.Interfaces.Cadastro;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Service.CQRS.Read.Interfaces.Cadastro;
using HealthCare.Service.CQRS.Read.Models;
using HealthCare.Service.CQRS.Write.Commands;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HealthCare.WebApi.Controllers
{
    [EnableCors("AllowCors")]
    [Route("api/[controller]")]
    public class DelegacaoProfissionalController : Controller
    {
        private readonly ICommandBus bus;
        private readonly ICboDao readCbo;
        private readonly IDelegacaoProfissionalDao readDelegacaoProfissional;

        public DelegacaoProfissionalController(ICommandBus _bus, ICboDao _readCbo, IDelegacaoProfissionalDao _readDelegacaoProfissional)
        {
            this.bus = _bus;
            this.readCbo = _readCbo;
            this.readDelegacaoProfissional = _readDelegacaoProfissional;
        }

        [HttpPost("[action]")]
        public IActionResult SalvarDelegacaoProfissional([FromBody]SaveDelegacaoProfissionalCommand model)
        {
            try
            {
                object objReturned = bus.SendWithReturn(model);
                if (objReturned.GetType() == typeof(Validation))
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(((Validation)objReturned).Messages);
                }
                return Json(objReturned);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex);
            }
        }

        [HttpGet("[action]")]
        public IList<CboReadModel> ListarTodasCbos()
        {
            return readCbo.ListarTodasCbos();
        }

        [HttpGet("[action]")]
        public IActionResult ListarDelegacoes(string unidadeId)
        {
            ListResult result = readDelegacaoProfissional.ListarDelegacoesAtivas(Guid.Parse(unidadeId));
            var data = result.ToListResult();
            if (data.Columns.Count != 0)
            {
                data.Columns.FirstOrDefault(x => x.Name == "Id").AllowVisible = false;
                data.Columns.FirstOrDefault(x => x.Name == "Delegado").DisplayName = "Delegado";
                data.Columns.FirstOrDefault(x => x.Name == "CboDelegado").DisplayName = "Cbo";
                data.Columns.FirstOrDefault(x => x.Name == "Delegador").DisplayName = "Delegador";
                data.Columns.FirstOrDefault(x => x.Name == "CboDelegador").DisplayName = "Cbo";
                data.Columns.FirstOrDefault(x => x.Name == "DataDelegacaoFormatada").DisplayName = "Data Delegação";
                data.Columns.FirstOrDefault(x => x.Name == "DataDestituicaoFormatada").AllowVisible = false;
            }
            return Json(data);
        }

        [HttpPost("[action]")]
        public IActionResult ExcluirDelegacaoProfissional(string idDelegacaoProfissional)
        {
            try
            {
                SaveDelegacaoProfissionalCommand delegacaoProfissional = new SaveDelegacaoProfissionalCommand();
                delegacaoProfissional.DelegacaoProfissionalId = Guid.Parse(idDelegacaoProfissional);
                object objReturned = bus.SendWithReturn(delegacaoProfissional);
                if (objReturned.GetType() == typeof(Validation))
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(((Validation)objReturned).Messages);
                }
                return Json(objReturned);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex);
            }
        }
    }
}

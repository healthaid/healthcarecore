﻿using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.CQRS.Write.Commands;
using HealthCare.Infra.CQRS.Write.Models;
using HealthCare.Service.CQRS.Write.Models;
using System;
using System.Linq;
using System.Web.Http;
using System.Web.Http.OData;

namespace HealthCare.WebApi.Controllers
{
    public class DocumentoController : ApiController
    {

        private readonly ICommandBus bus;


        public DocumentoController(ICommandBus bus)
        {
            this.bus = bus;
            //this.read = read;
        }



        [HttpPost]
        [Route("CadastrarDocumento")]
        public void CadastrarDocumento(DocumentoWriteModel model)
        {
            CreateDocumentoCommand c = new CreateDocumentoCommand();
            c.Valor = c.Valor;
            c.TipoDocumento = c.TipoDocumento;
            c.PacienteId = model.PacienteId;
            bus.Send(c);
        }

        //[HttpGet]
        //[EnableQuery]
        //[Route("ListarDocumento")]
        //public IQueryable<DocumentoWriteModel> ListarDocumento(Guid pacienteId)
        //{
        //    return (from c in read.ListarDocumentosPorPaciente(pacienteId)
        //            select new DocumentoWriteModel
        //            {
        //                Logradouro = c.Logradouro
        //              ,
        //                CEP = c.CEP
        //              ,
        //                Numero = c.Numero
        //              ,
        //                Complemento = c.Complemento
        //              ,
        //                CidadeId = c.Bairro.Cidade.Id
        //              ,
        //                BairroId = c.Bairro.Id
        //              ,
        //                PacienteId = c.Paciente.Id
        //              ,
        //                CidadeNome = c.Bairro.Cidade.Nome
        //              ,
        //                BairroNome = c.Bairro.Nome
        //              ,
        //                PacienteNome = c.Paciente.Nome
        //            }).AsQueryable();
        //}

        [HttpPut]
        [Route("AtualizarDocumento")]
        public void AtualizarDocumento(DocumentoWriteModel model)
        {
            UpdateDocumentoCommand c = new UpdateDocumentoCommand();
            c.Id = model.Id;
            c.Valor = model.Valor;
            c.TipoDocumento = model.TipoDocumento;
            c.PacienteId = model.PacienteId;
            bus.Send(c);
        }

    }
}

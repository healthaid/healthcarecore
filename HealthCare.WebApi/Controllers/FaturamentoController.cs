﻿using HealthCare.Service.CQRS.Read.Interfaces.Faturamento;
using HealthCare.Service.CQRS.Read.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace HealthCare.WebApi.Controllers
{
    [EnableCors("AllowCors")]
    [Route("api/[controller]")]
    public class FaturamentoController : Controller
    {
        public IFaturamentoDao faturamentoDao;
        public FaturamentoController(IFaturamentoDao faturamento)
        {
            this.faturamentoDao = faturamento;
        }

        [HttpGet("[action]")]
        public IActionResult GetListFaturamentoCompetencia(string unidadeId, int mes, int ano)
        {
            try
            {
                List<FaturamentoModel> listaFaturamento = faturamentoDao.ListaFaturamento(unidadeId, mes, ano);
                return Json(listaFaturamento);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }


        [HttpGet("[action]")]
        public IActionResult GetListParaFaturamentoCompetencia(string unidadeId, int mes, int ano)
        {
            try
            {
                List<FaturamentoModel> listaFaturamento = faturamentoDao.ListaAtendimentosParaFaturamento(unidadeId, mes, ano);
                return Json(listaFaturamento);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }


        [HttpGet("[action]")]
        public IActionResult ListaParaFaturamentoPorPeriodo(string unidadeId, string inicio, string fim)
        {
            try
            {
                DateTime dinicio = new DateTime(Convert.ToInt32(inicio.Substring(6, 4)), Convert.ToInt32(inicio.Substring(3, 2)), Convert.ToInt32(inicio.Substring(0, 2)));
                DateTime dfim = new DateTime(Convert.ToInt32(fim.Substring(6, 4)), Convert.ToInt32(fim.Substring(3, 2)), Convert.ToInt32(fim.Substring(0, 2)));

                List<FaturamentoModel> listaFaturamento = faturamentoDao.ListaParaFaturamentoPorPeriodo(unidadeId, dinicio, dfim);
                return Json(listaFaturamento);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

    }
}

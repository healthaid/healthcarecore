﻿using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.CQRS.Read.Interfaces.Atendimento;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.CQRS.Write.Commands;
using System;
using HealthCare.Infra.Common.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using System.Linq;

namespace HealthCare.WebApi.Controllers
{
    [EnableCors("AllowCors")]
    [Route("api/[controller]")]
    public class EnfermariaController : Controller
    {
        private readonly ICommandBus bus;
        private readonly IEnfermariaDao read;

        public EnfermariaController(ICommandBus _bus, IEnfermariaDao _read)
        {
            this.bus = _bus;
            this.read = _read;
        }

        [HttpPost("[action]")]
        public IActionResult SalvarEnfermaria([FromBody]SaveEnfermariaCommand model)
        {
            try
            {
                object objReturned = bus.SendWithReturn(model);
                if (objReturned.GetType() == typeof(Validation))
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(((Validation)objReturned).Messages);
                }
                return Json(objReturned);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpGet("[action]")]
        public IActionResult ObterEnfermariaPorId(string id)
        {
            try
            {
                EnfermariaReadModel receita = read.GetEnfermariaById(Guid.Parse(id));
                return Json(receita);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex);
            }
        }

        [HttpDelete("[action]")]
        public IActionResult DeleteEnfermaria(string id)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(id) == false)
                {
                    DeleteEnfermariaCommand command = new DeleteEnfermariaCommand();
                    command.EnfermariaId = Guid.Parse(id);
                    object objReturned = bus.SendWithReturn(command);
                    if (objReturned.GetType() == typeof(Validation))
                        return Json(((Validation)objReturned).Messages);
                    else
                    {
                        //HttpContext.Response.StatusCode = StatusCodes.Status304NotModified;
                        return Json(StatusCodes.Status200OK);
                        
                    }
                }
                else
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(StatusCodes.Status400BadRequest);
                }
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpGet("[action]")]
        public IActionResult ListarEnfermarias(string unidadeId)
        {
            try
            {
                ListResult result = read.ListaEnfermarias(true, Guid.Parse(unidadeId));
                var data = result.ToListResult();

                if (data.Columns.Count != 0)
                {
                    data.Columns.FirstOrDefault(x => x.Name == "Id").AllowVisible = false;
                    data.Columns.FirstOrDefault(x => x.Name == "Nome").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "Nome").DisplayName = "Exame";
                }
                return Json(data);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }
    }
}
﻿using HealthCare.Infra.Common.Extensions;
using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.CQRS.Read.Interfaces.Atendimento;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.CQRS.Write.Commands;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace HealthCare.WebApi.Controllers
{
    [EnableCors("AllowCors")]
    [Route("api/[controller]")]
    public class AcolhimentoController : Controller
    {
        private readonly ICommandBus bus;
        private readonly IAcolhimentoDTO read;
        private readonly ILogger<AcolhimentoController> logger;

        public AcolhimentoController(ICommandBus _bus, IAcolhimentoDTO _read, ILogger<AcolhimentoController> _logger)
        {
            this.bus = _bus;
            this.read = _read;
            this.logger = _logger;
        }

        [HttpPost("[action]")]
        public IActionResult SalvarAcolhimento([FromBody]SaveAcolhimentoCommand model)
        {
            try
            {
                object objReturned = bus.SendWithReturn(model);
                if (objReturned.GetType() == typeof(Validation))
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(((Validation)objReturned).Messages);
                }
                return Json(objReturned);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex);
            }
        }

        [HttpGet("[action]")]
        public IActionResult FilaAcolhimento(string unidade)
        {
            logger.LogInformation("Iniciada Consulta de Fila de Acolhimento");
            ListResult result = read.FilaAcolhimento(Guid.Parse(unidade));
            logger.LogInformation("Terminada Consulta de Fila de Acolhimento");
            var data = result.ToListResult();
            if (data.Columns.Count != 0)
            {
                data.Columns.FirstOrDefault(x => x.Name == "Id").AllowVisible = false;
                data.Columns.FirstOrDefault(x => x.Name == "PacienteId").AllowVisible = false;
                data.Columns.FirstOrDefault(x => x.Name == "HoraAcolhimento").ColumnWidth = "50px";
                data.Columns.FirstOrDefault(x => x.Name == "HoraAcolhimento").DisplayName = "Hora";
                data.Columns.FirstOrDefault(x => x.Name == "Boletim").AllowVisible = false;
                data.Columns.FirstOrDefault(x => x.Name == "NomePaciente").ColumnWidth = "100px";
                data.Columns.FirstOrDefault(x => x.Name == "NomePaciente").DisplayName = "Paciente";
                data.Columns.FirstOrDefault(x => x.Name == "Sexo").ColumnWidth = "50px";
                data.Columns.FirstOrDefault(x => x.Name == "Sexo").AllowVisible = false;
                data.Columns.FirstOrDefault(x => x.Name == "Idade").AllowVisible = false;
                data.Columns.FirstOrDefault(x => x.Name == "Sexo").DisplayName = "Sexo";
                data.Columns.FirstOrDefault(x => x.Name == "Risco").ColumnWidth = "50px";
                data.Columns.FirstOrDefault(x => x.Name == "Risco").DisplayName = "Risco";
                data.Columns.FirstOrDefault(x => x.Name == "Risco").AllowVisible = false;
                data.Columns.FirstOrDefault(x => x.Name == "Idade").ColumnWidth = "50px";
                data.Columns.FirstOrDefault(x => x.Name == "Idade").DisplayName = "Idade";
                data.Columns.FirstOrDefault(x => x.Name == "Servico").ColumnWidth = "50px";
                data.Columns.FirstOrDefault(x => x.Name == "Servico").DisplayName = "Serviço";
                data.Columns.FirstOrDefault(x => x.Name == "Prioridade").DisplayName = "Prioridade";
            }
            return Json(data);
        }

        [HttpGet("[action]")]
        public IActionResult ObterAcolhimento(string acolhimentoId)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(acolhimentoId) == false)
                {
                    AcolhimentoReadModel acolhimento = read.ObterAcolhimentoPorId(Guid.Parse(acolhimentoId));
                    return Json(acolhimento);
                }
                else
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(StatusCodes.Status400BadRequest);
                }
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpGet("[action]")]
        public IActionResult GetFilaClassificacaoRisco(string unidadeId)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(unidadeId))
                {
                    ListResult result = read.GetClassificacaoRiscoList(Guid.Parse(unidadeId));
                    var data = result.ToListResult();
                    if (data.Columns.Count != 0)
                    {
                        data.Columns.FirstOrDefault(x => x.Name == "Id").AllowVisible = false;
                        data.Columns.FirstOrDefault(x => x.Name == "PacienteId").AllowVisible = false;
                        data.Columns.FirstOrDefault(x => x.Name == "HoraAcolhimento").ColumnWidth = "50px";
                        data.Columns.FirstOrDefault(x => x.Name == "HoraAcolhimento").DisplayName = "Hora";
                        data.Columns.FirstOrDefault(x => x.Name == "NomePaciente").ColumnWidth = "100px";
                        data.Columns.FirstOrDefault(x => x.Name == "NomePaciente").DisplayName = "Paciente";
                        data.Columns.FirstOrDefault(x => x.Name == "Sexo").AllowVisible = false;
                        data.Columns.FirstOrDefault(x => x.Name == "Idade").AllowVisible = false;
                        data.Columns.FirstOrDefault(x => x.Name == "Risco").AllowVisible = false;
                        data.Columns.FirstOrDefault(x => x.Name == "Servico").ColumnWidth = "50px";
                        data.Columns.FirstOrDefault(x => x.Name == "Servico").DisplayName = "Serviço";
                        data.Columns.FirstOrDefault(x => x.Name == "Prioridade").DisplayName = "Prioridade";
                    }
                    return Json(data);
                }
                else
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(StatusCodes.Status400BadRequest);
                }
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }
        
    }
}
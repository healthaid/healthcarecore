﻿using HealthCare.Infra.Common.Extensions;
using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.CQRS.Write.Commands;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;


namespace HealthCare.WebApi.Controllers
{
    [EnableCors("AllowCors")]
    [Route("api/[controller]")]
    public class SetorClinicaController : Controller
    {

        private readonly ICommandBus bus;
        private readonly ISetorClinicaDao read;

        public SetorClinicaController(ICommandBus bus, ISetorClinicaDao read)
        {
            this.bus = bus;
            this.read = read;
        }

        [HttpGet("[action]")]
        public IActionResult ListarSetorClinicas()
        {
            ListResult result = read.ListaSetorClinica();
            var data = result.ToListResult();
            if (data.Columns.Count != 0)
            {
                data.Columns.FirstOrDefault(x => x.Name == "Id").AllowVisible = false;
                data.Columns.FirstOrDefault(x => x.Name == "Id").DisplayName = "Id do SetorClinica";
                data.Columns.FirstOrDefault(x => x.Name == "Descricao").ColumnWidth = "200px";
                data.Columns.FirstOrDefault(x => x.Name == "Descricao").DisplayName = "Nome";
            }
            return Json(data);
        }

        [HttpPost("[action]")]
        public IActionResult SalvarSetorClinica([FromBody]SaveSetorClinicaCommand model)
        {
            try
            {
                object objReturned = bus.SendWithReturn(model);
                if (objReturned.GetType() == typeof(Validation))
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(((Validation)objReturned).Messages);
                }
                return Json(objReturned);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpDelete("[action]")]
        public IActionResult DeleteSetorClinica(string id)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(id) == false)
                {
                    DeleteSetorClinicaCommand command = new DeleteSetorClinicaCommand();
                    command.SetorClinicaId = Guid.Parse(id);
                    object objReturned = bus.SendWithReturn(command);
                    if (objReturned.GetType() != typeof(Validation))
                        return Json(StatusCodes.Status200OK);
                    else
                    {
                        HttpContext.Response.StatusCode = StatusCodes.Status304NotModified;
                        return Json(((Validation)objReturned).Messages);
                    }
                }
                else
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(StatusCodes.Status400BadRequest);
                }
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpGet("[action]")]
        public IActionResult GetSetorClinicaById(string id)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(id) == false)
                {
                    SetorClinicaReadModel paciente = read.GetSetorClinicaById(Guid.Parse(id));
                    return Json(paciente);
                }
                else
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(StatusCodes.Status400BadRequest);
                }
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }
    }
}
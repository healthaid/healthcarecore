﻿using HealthCare.Infra.Common.Extensions;
using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.CQRS.Read.Interfaces.Atendimento;
using HealthCare.Infra.CQRS.Write.Commands;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;


namespace HealthCare.WebApi.Controllers
{
    [EnableCors("AllowCors")]
    [Route("api/[controller]")]
    public class MovimentacaoLeitoController : Controller
    {
        private readonly ICommandBus bus;
        private readonly IMovimentacaoLeitoDto read;

        public MovimentacaoLeitoController(ICommandBus _bus, IMovimentacaoLeitoDto _read)
        {
            this.bus = _bus;
            this.read = _read;
        }

        [HttpGet("[action]")]
        public IActionResult ListarLeitosOcupados(Guid unidadeId)
        {
            try
            {
                ListResult result = read.ListarLeitosOcupados(unidadeId);
                var data = result.ToListResult();
                if (data.Columns.Count != 0)
                {
                    data.Columns.FirstOrDefault(x => x.Name == "Id").AllowVisible = false;
                    data.Columns.FirstOrDefault(x => x.Name == "Paciente").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "Paciente").DisplayName = "Nome do Paciente";
                    data.Columns.FirstOrDefault(x => x.Name == "Enfermaria").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "Enfermaria").DisplayName = "Enfermaria";
                    data.Columns.FirstOrDefault(x => x.Name == "Leito").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "Leito").DisplayName = "Leito";
                    data.Columns.FirstOrDefault(x => x.Name == "PacienteId").AllowVisible = false;
                }
                return Json(data);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpGet("[action]")]
        public IActionResult GetMovimentacaoLeito(Guid id)
        {
            try
            {
                var movimentacao = read.GetMovimentacaoLeito(id);

                return Json(movimentacao);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpGet("[action]")]
        public IActionResult GetUltimaMovimentacaoPorLeito(Guid id)
        {
            try
            {
                var movimentacao = read.GetUltimaMovimentacaoPorLeito(id);

                return Json(movimentacao);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpGet("[action]")]
        public IActionResult GetLeitoCorrentePorAtendimento(Guid id)
        {
            try
            {
                var leitoCorrente = read.GetLeitoCorrentePorAtendimento(id);

                return Json(leitoCorrente);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpPost("[action]")]
        public IActionResult SalvarIntenacao([FromBody]SaveInternacaoCommand model)
        {
            try
            {
                object objReturned = bus.SendWithReturn(model);
                if (objReturned.GetType() == typeof(Validation))
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(((Validation)objReturned).Messages);
                }
                return Json(objReturned);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpPost("[action]")]
        public IActionResult SalvarMovimentacaoLeito([FromBody]SaveMovimentacaoLeitoCommand model)
        {
            try
            {
                object objReturned = bus.SendWithReturn(model);
                if (objReturned.GetType() == typeof(Validation))
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(((Validation)objReturned).Messages);
                }
                return Json(objReturned);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpPost("[action]")]
        public IActionResult SalvarPermuta([FromBody]SavePermutaCommand model)
        {
            try
            {
                object objReturned = bus.SendWithReturn(model);
                if (objReturned.GetType() == typeof(Validation))
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(((Validation)objReturned).Messages);
                }
                return Json(objReturned);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpPost("[action]")]
        public IActionResult SalvarTransferenciaLeit([FromBody]SaveTransferenciaLeitoCommand model)
        {
            try
            {
                object objReturned = bus.SendWithReturn(model);
                if (objReturned.GetType() == typeof(Validation))
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(((Validation)objReturned).Messages);
                }
                return Json(objReturned);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }
        [HttpGet("[action]")]
        public IActionResult ListarObservacaoDiaria(string inicio, string fim, Guid unidadeId)
        {
            try
            {
                DateTime dinicio = new DateTime(Convert.ToInt32(inicio.Substring(6, 4)), Convert.ToInt32(inicio.Substring(3, 2)), Convert.ToInt32(inicio.Substring(0, 2)));
                DateTime dfim = new DateTime(Convert.ToInt32(fim.Substring(6, 4)), Convert.ToInt32(fim.Substring(3, 2)), Convert.ToInt32(fim.Substring(0, 2)));
                ListResult result = read.ListarObservacaoDiaria(dinicio, dfim, unidadeId);
                var data = result.ToListResult();

                return Json(data);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }


       
        //public IActionResult ObterAtendimento(string acolhimentoId)
        //{
        //    try
        //    {
        //        if (string.IsNullOrWhiteSpace(acolhimentoId) == false)
        //        {
        //            AtendimentoReadModel atendimento = read.ObterAtendimentoPorAcolhimento(Guid.Parse(acolhimentoId));
        //            return Json(atendimento);
        //        }
        //        else
        //        {
        //            HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
        //            return Json(StatusCodes.Status400BadRequest);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
        //        return Json(ex.InnerException);
        //    }
        //}
    }
}
﻿using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.CQRS.Write.Commands;
using HealthCare.Infra.CQRS.Write.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace HealthCare.WebApi.Controllers
{
    [EnableCors("AllowCors")]
    [Route("api/[controller]")]
    public class ClinicaController : Controller
    {
        private readonly ICommandBus bus;
        private readonly IClinicaDao read;

        public ClinicaController(ICommandBus bus, IClinicaDao read)
        {
            this.bus = bus;
            this.read = read;
        }

        [HttpGet("[action]")]
        public IActionResult ListarClinica()
        {
            try
            {
                return Json(read.ListarClinicas().AsQueryable());
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json( ex.InnerException);
            }
        }

        [HttpPost("[action]")]
        public IActionResult CadastrarClinica([FromBody]ClinicaWriteModel model)
        {
            try
            {
                CreateClinicaCommand c = new CreateClinicaCommand();
                c.Nome = model.Nome;
                bus.Send(c);
                return Json(StatusCodes.Status200OK);
            }
            catch(Exception ex)
            {
                return Json(ex.InnerException);
            }
        }
    }
}

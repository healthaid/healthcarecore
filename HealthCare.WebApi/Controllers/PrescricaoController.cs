﻿using HealthCare.Infra.Common.Extensions;
using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.CQRS.Read.Interfaces.Atendimento;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.CQRS.Write.Commands;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;


namespace HealthCare.WebApi.Controllers
{
    [EnableCors("AllowCors")]
    [Route("api/[controller]")]
    public class PrescricaoController : Controller
    {
        private readonly ICommandBus bus;
        private readonly IAtendimentoDto readAtendimento;
        private readonly IPrescricaoDto read;

        private readonly IMedicamentoDto readMedicamento;
        private readonly IAdministracaoMedicamentoDto readAdministracaoMedicamento;
        private readonly IUnidadeMedicamentoDto readUnidadeMedicamento;

        public PrescricaoController(ICommandBus _bus, IAtendimentoDto _readAtendimento, IPrescricaoDto _read, IMedicamentoDto _readMedicamento, IAdministracaoMedicamentoDto _readAdministracaoMedicamento, IUnidadeMedicamentoDto _readUnidadeMedicamento)
        {
            this.bus = _bus;
            this.readAtendimento = _readAtendimento;
            this.read = _read;
            this.readMedicamento = _readMedicamento;
            this.readAdministracaoMedicamento = _readAdministracaoMedicamento;
            this.readUnidadeMedicamento = _readUnidadeMedicamento;
        }

        [HttpPost("[action]")]
        public IActionResult SalvarPrescricao([FromBody]SavePrescricaoCommand model)
        {
            try
            {
                object objReturned = bus.SendWithReturn(model);
                if (objReturned.GetType() == typeof(Validation))
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(((Validation)objReturned).Messages);
                }
                return Json(objReturned);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpPost("[action]")]
        public IActionResult RealizarMedicacao([FromBody]SalaMedicamentoCommand model)
        {
            try
            {
                object objReturned = bus.SendWithReturn(model);
                if (objReturned.GetType() == typeof(Validation))
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(((Validation)objReturned).Messages);
                }

                return Json(objReturned);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpGet("[action]")]
        public IActionResult ObterPrescricaoPorId(string id)
        {
            try
            {
                PrescricaoReadModel receita = read.ObterPrescricaoPorId(Guid.Parse(id));
                return Json(receita);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpGet("[action]")]
        public IActionResult ListarPrescricaoPorAtendimento(string atendimentoId)
        {
            try
            {
                ListResult result = read.ListarPrescricoesPorAtendimentoId(Guid.Parse(atendimentoId));
                var data = result.ToListResult();

                if (data.Columns.Count != 0)
                {
                    data.Columns.FirstOrDefault(x => x.Name == "Id").AllowVisible = false;
                    data.Columns.FirstOrDefault(x => x.Name == "Medicamento").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "Medicamento").DisplayName = "Medicamento";
                    data.Columns.FirstOrDefault(x => x.Name == "Dose").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "Dose").DisplayName = "Dose";
                    data.Columns.FirstOrDefault(x => x.Name == "Unidade").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "Unidade").DisplayName = "Unidade";
                    data.Columns.FirstOrDefault(x => x.Name == "ViaAdministracao").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "ViaAdministracao").DisplayName = "Via de Administração";
                    data.Columns.FirstOrDefault(x => x.Name == "Intervalo").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "Intervalo").DisplayName = "Intervalo";
                    data.Columns.FirstOrDefault(x => x.Name == "StatusPorEscrito").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "StatusPorEscrito").DisplayName = "Status";
                    data.Columns.FirstOrDefault(x => x.Name == "ObservacaoEnfermagem").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "ObservacaoEnfermagem").DisplayName = "Observação Técnica";
                    data.Columns.FirstOrDefault(x => x.Name == "Duracao").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "Duracao").DisplayName = "Duração";
                }
                return Json(data);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpDelete("[action]")]
        public IActionResult DeletePrescricao(string id)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(id) == false)
                {
                    DeletePrescricaoCommand command = new DeletePrescricaoCommand();
                    command.DeleteId = Guid.Parse(id);
                    object objReturned = bus.SendWithReturn(command);
                    if (objReturned.GetType() != typeof(Validation))
                        return Json(new ResponseMessage(StatusCodes.Status200OK));
                    else
                    {
                        HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                        return Json(((Validation)objReturned).Messages);
                    }
                }
                else
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(StatusCodes.Status400BadRequest);
                }
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpGet("[action]")]
        public IActionResult ListarPrescricoesPendentes(string unidadeId)
        {
            try
            {
                ListResult result = read.ListarPrescricoesPendentes(Guid.Parse(unidadeId));
                var data = result.ToListResult();

                if (data.Columns.Count != 0)
                {
                    data.Columns.FirstOrDefault(x => x.Name == "Id").AllowVisible = false;
                    data.Columns.FirstOrDefault(x => x.Name == "Nome").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "Nome").DisplayName = "Paciente";
                    data.Columns.FirstOrDefault(x => x.Name == "Data").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "Data").DisplayName = "Data";
                    data.Columns.FirstOrDefault(x => x.Name == "Profissional").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "Profissional").DisplayName = "Solicitado em";
                }
                return Json(data);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpGet("[action]")]
        public IActionResult ListarPrescricoesPendentesAtendimentosFechados(string unidadeId, string competencia)
        {
            try
            {
                ListResult result = read.ListarPrescricoesPendentesAtendimentosFechados(Guid.Parse(unidadeId), competencia);
                var data = result.ToListResult();

                if (data.Columns.Count != 0)
                {
                    data.Columns.FirstOrDefault(x => x.Name == "Id").AllowVisible = false;
                    data.Columns.FirstOrDefault(x => x.Name == "Nome").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "Nome").DisplayName = "Paciente";
                    data.Columns.FirstOrDefault(x => x.Name == "Data").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "Data").DisplayName = "Data";
                    data.Columns.FirstOrDefault(x => x.Name == "Profissional").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "Profissional").DisplayName = "Solicitado em";
                }
                return Json(data);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpGet("[action]")]
        public IActionResult ListarPrescricoesPendentesPorAtendimento(Guid atendimentoId)
        {
            try
            {
                ListResult result = read.ListarPrescricoesPendentesPorAtendimento(atendimentoId);
                var data = result.ToListResult();

                if (data.Columns.Count != 0)
                {
                    data.Columns.FirstOrDefault(x => x.Name == "Id").AllowVisible = false;
                    data.Columns.FirstOrDefault(x => x.Name == "ObservacaoMedico").AllowVisible = false;
                    data.Columns.FirstOrDefault(x => x.Name == "codigoBoletim").AllowVisible = false;
                    data.Columns.FirstOrDefault(x => x.Name == "idade").AllowVisible = false;
                    data.Columns.FirstOrDefault(x => x.Name == "NomePaciente").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "NomePaciente").DisplayName = "Paciente";
                    data.Columns.FirstOrDefault(x => x.Name == "Duracao").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "Duracao").DisplayName = "Duração";
                    data.Columns.FirstOrDefault(x => x.Name == "HoraSolitiacao").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "HoraSolitiacao").DisplayName = "Solicitado em";
                    data.Columns.FirstOrDefault(x => x.Name == "Medicamento").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "Medicamento").DisplayName = "Medicamento";
                    data.Columns.FirstOrDefault(x => x.Name == "Dose").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "Dose").DisplayName = "Dose";
                    data.Columns.FirstOrDefault(x => x.Name == "Unidade").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "Unidade").DisplayName = "Unidade";
                    data.Columns.FirstOrDefault(x => x.Name == "ViaAdministracao").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "ViaAdministracao").DisplayName = "Via de Administração";
                    data.Columns.FirstOrDefault(x => x.Name == "Intervalo").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "Intervalo").DisplayName = "Intervalo";
                }
                return Json(data);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }
        
        [HttpGet("[action]")]
        public IActionResult ListarMedicamentos(string descricao)
        {
            return Json(readMedicamento.ListarTodosMedicamentos(descricao).AsQueryable().Select(x => x.Nome).ToArray());
        }
        
        [HttpGet("[action]")]
        public IActionResult ListarAdministracaoMedicamentos(string descricao)
        {
            return Json(readAdministracaoMedicamento.ListarTodosAdministracaoMedicamentos(descricao).AsQueryable().Select(x => x.Nome).ToArray());
        }

        [HttpGet("[action]")]
        public IActionResult ListarUnidadeMedida(string descricao)
        {
            return Json(readUnidadeMedicamento.ListarTodosUnidadeMedicamentos(descricao).AsQueryable().Select(x => x.Nome).ToArray());
        }

        [HttpGet("[action]")]
        public IActionResult ListarUnidadeMedidaAll()
        {
            var lista = readUnidadeMedicamento.ListarTodosUnidadeMedicamentos().AsQueryable().Select(x => x.Nome).ToArray();
            return Json(lista);
        }

        [HttpGet("[action]")]
        public IActionResult ListarAdministracaoMedicamentosAll()
        {
            return Json(readAdministracaoMedicamento.ListarTodosAdministracaoMedicamentos().AsQueryable().Select(x => x.Nome).ToArray());
        }

        [HttpGet("[action]")]
        public IActionResult ListarProducaoEnfermagem(int mes, int ano, string unidadeId)
        {
            ListResult result = read.ListarProducaoEnfermagem(mes, ano, Guid.Parse(unidadeId));
            var data = result.ToListResult();
            return Json(data);
        }

        [HttpGet("[action]")]
        public IActionResult ImpressaoPrescricao(string atendimentoId)
        {
            try
            {
                var data = read.ImpressaoPrescricao(Guid.Parse(atendimentoId));
                return Json(data);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }


    }
}
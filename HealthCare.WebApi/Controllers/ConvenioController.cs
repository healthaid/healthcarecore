﻿using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.CQRS.Write.Commands;
using HealthCare.Service.CQRS.Write.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace HealthCare.WebApi.Controllers
{
    [EnableCors("AllowCors")]
    [Route("api/[controller]")]
    public class ConvenioController : Controller
    {
        private readonly ICommandBus bus;
        private readonly IConvenioDao read;

        public ConvenioController(ICommandBus bus, IConvenioDao read)
        {
            this.bus = bus;
            this.read = read;
        }

        [HttpPost("[action]")]
        public IActionResult CadastrarConvenio([FromBody]ConvenioWriteModel model)
        {
            try
            {
                CreateConvenioCommand c = new CreateConvenioCommand();
                c.Acomocadao = model.Acomocadao;
                c.NumeroCarteira = model.NumeroCarteira;
                c.PacienteId = model.PacienteId;
                c.PlanoId = model.PlanoId;
                c.Validade = model.Validade;
                bus.Send(c);
                return Json(StatusCodes.Status200OK);
            }
            catch (Exception ex)
            {
                return Json(ex.InnerException);
            }
        }

        [HttpGet("[action]")]
        public IActionResult ListarConvenio(Guid pacienteId)
        {
            try
            {
                return Json((from c in read.ListarConveniosPorPaciente(pacienteId)
                                                                          select new ConvenioWriteModel
                                                                          {
                                                                              Id = c.Id,
                                                                              NumeroCarteira = c.NumeroCarteira,
                                                                              Acomocadao = c.Acomocadao,
                                                                              PacienteId = c.Paciente.Id,
                                                                              PlanoId = c.Plano.Id,
                                                                              PacienteNome = c.Paciente.Nome,
                                                                              PlanoNome = c.Plano.Nome
                                                                          }).AsQueryable());
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpPut("[action]")]
        public IActionResult AtualizarConvenio([FromBody]ConvenioWriteModel model)
        {
            try
            {
                UpdateConvenioCommand c = new UpdateConvenioCommand();
                c.Id = model.Id;
                c.Acomocadao = model.Acomocadao;
                c.NumeroCarteira = model.NumeroCarteira;
                c.PacienteId = model.PacienteId;
                c.PlanoId = model.PlanoId;
                c.Validade = model.Validade;

                bus.Send(c);
                return Json(StatusCodes.Status200OK);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }
    }
}

﻿using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.CQRS.Write.Commands;
using HealthCare.Service.CQRS.Write.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace HealthCare.WebApi.Controllers
{
    [EnableCors("AllowCors")]
    [Route("api/[controller]")]
    public class PlanoController : Controller
    {

        private readonly ICommandBus bus;
        private readonly IPlanoDao read;


        public PlanoController(ICommandBus bus, IPlanoDao read)
        {
            this.bus = bus;
            this.read = read;
        }

        [HttpGet("[action]")]
        public IQueryable<PlanoWriteModel> ListarPlanos()
        {
            return (from a in read.ListarPlano()
                    select new PlanoWriteModel
                    {
                       Id = a.Id
                     , Nome = a.Nome
                     , Ativo = a.Ativo
                    }).AsQueryable();

        }

        [HttpPost("[action]")]
        public void CadastrarPlano([FromBody]PlanoWriteModel model)
        {
            CreatePlanoCommand c = new CreatePlanoCommand();
            c.Nome = model.Nome;
            c.Ativo = model.Ativo;
            bus.Send(c);
        }

        [HttpPut("[action]")]
        public void AtualizarPlano([FromBody]PlanoWriteModel model)
        {
            UpdatePlanoCommand c = new UpdatePlanoCommand();
            c.Id = model.Id;
            c.Nome = model.Nome;
            c.Ativo = model.Ativo;
            bus.Send(c);
        }
    }
}

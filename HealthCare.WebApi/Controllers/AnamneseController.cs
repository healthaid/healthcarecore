﻿using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.CQRS.Write.Commands;
using HealthCare.Infra.CQRS.Write.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HealthCare.WebApi.Controllers
{
    [EnableCors("AllowCors")]
    [Route("api/[controller]")]
    public class AnamneseController : Controller
    {
        private readonly ICommandBus bus;
        private readonly ICiap2Dao readCiap2;
        private readonly IAnamneseDao read;

        public AnamneseController(ICommandBus bus, ICiap2Dao readCiap2, IAnamneseDao read)
        {
            this.bus = bus;
            this.readCiap2 = readCiap2;
            this.read = read;
        }

        [HttpGet("[action]")]
        public IActionResult GetAnamnesePorAtendimentoId(Guid idAtendimento)
        {
            var R = read.ObterAnamnesePorAtendimento(idAtendimento);
            return Json(R);
        }

        [HttpGet("[action]")]
        public IList<string> ListarCiaps()
        {
            return readCiap2.ListarCiaps().AsQueryable().Select(x => x.Descricao).ToArray();
        }

        [HttpGet("[action]")]
        public Array ListarCiap2PorDescricao(string descricao)
        {
            try
            {
                return readCiap2.ListarCiaps(descricao).AsQueryable().Select(x => string.Format("{0}-{1}", x.Codigo, x.Descricao)).ToArray();
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpPost("[action]")]
        public void SalvarAnamnese([FromBody]AnameseWriteModel model)
        {
            if (model.Id == Guid.Empty)
            {
                CreateAnamneseCommand createCommand = new CreateAnamneseCommand();
                createCommand.AtendimentoId = model.AtendimentoId;
                createCommand.Ciap2Id = model.Ciad2Id;
                createCommand.QueixaPrincipal = model.QueixaPrincipal;
                createCommand.Descricao = model.Descricao;
                bus.Send(createCommand);
            }
            else
            {
                UpdateAnanmneseCommand updateCommand = new UpdateAnanmneseCommand();
                updateCommand.AtendimentoId = model.AtendimentoId;
                updateCommand.Ciap2Id = model.Ciad2Id;
                updateCommand.QueixaPrincipal = model.QueixaPrincipal;
                updateCommand.Descricao = model.Descricao;
                bus.Send(updateCommand);
            }
        }

        [HttpGet("[action]")]
        public IQueryable<AnamneseReadViewModel> ListarAnamnese()
        {
            return read.ListarAnamneses().AsQueryable();
        }


        [HttpGet("[action]")]
        public AnamneseReadViewModel ListarAnamnesePorAtendimentoId(Guid atendimentoId)
        {
            return read.ObterAnamnesePorAtendimento(atendimentoId);
        }
    }
}
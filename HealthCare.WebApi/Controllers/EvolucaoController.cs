﻿using HealthCare.Infra.Common.Extensions;
using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.CQRS.Read.Implementation;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.CQRS.Read.Interfaces.Atendimento;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.CQRS.Write.Commands;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HealthCare.WebApi.Controllers
{
    [EnableCors("AllowCors")]
    [Route("api/[controller]")]
    public class EvolucaoController : Controller
    {
        private readonly ICommandBus bus;            
        private readonly IEvolucaoDto read;
        private readonly ICidDao readCid;

        public EvolucaoController(ICommandBus _bus, IEvolucaoDto _read, ICidDao _readCid)
        {
            this.bus = _bus;    
            this.read = _read;
            this.readCid = _readCid;
        }

        [HttpPost("[action]")]
        public IActionResult SalvarEvolucao([FromBody]SaveEvolucaoCommand model)
        {
            try
            {
                object objReturned = bus.SendWithReturn(model);
                if (objReturned.GetType() == typeof(Validation))
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(((Validation)objReturned).Messages);
                }
                return Json(objReturned);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpGet("[action]")]
        public IActionResult ObterEvolucaoPorId(string id)
        {
            try
            {
                EvolucaoReadModel receita = read.GetById(Guid.Parse(id));
                return Json(receita);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpGet("[action]")]
        public IActionResult GetCidsParaCheckout(string id)
        {
            try
            {
                IEnumerable<CIDReadViewModel> lista = readCid.GetCidsParaCheckout(Guid.Parse(id));
                return Json(lista);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpGet("[action]")]
        public IActionResult ListarEvolucaoPorAtendimento(string id)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(id))
                {
                    ListResult result = read.ListarEvolucaoPorAtendimento(Guid.Parse(id));
                    var data = result.ToListResult();
                    data.AllowActions = false;
                    if (data != null && data.Columns.Count != 0)
                    {
                        data.Columns.FirstOrDefault(x => x.Name == "Id").AllowVisible = false;
                        data.Columns.FirstOrDefault(x => x.Name == "DataHoraEvolucao").ColumnWidth = "100px";
                        data.Columns.FirstOrDefault(x => x.Name == "DataHoraEvolucao").DisplayName = "Data";
                        data.Columns.FirstOrDefault(x => x.Name == "Descricao").ColumnWidth = "50px";
                        data.Columns.FirstOrDefault(x => x.Name == "Descricao").DisplayName = "Descrição";
                        data.Columns.FirstOrDefault(x => x.Name == "DiagnosticoUm").DisplayName = "CID";
                        data.Columns.FirstOrDefault(x => x.Name == "Procedimento").ColumnWidth = "100px";
                        data.Columns.FirstOrDefault(x => x.Name == "Procedimento").DisplayName = "Procedimento";
                        //data.Columns.FirstOrDefault(x => x.Name == "Quantidade").ColumnWidth = "50px";
                        //data.Columns.FirstOrDefault(x => x.Name == "Quantidade").DisplayName = "Quantidade";
                        data.Columns.FirstOrDefault(x => x.Name == "Profissional").DisplayName = "Profissional";
                    }

                    return Json(data);
                }
                else
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(string.Empty);
                }
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        

        [HttpGet("[action]")]
        public IActionResult ListarEvolucaoMedica(string inicio, string fim, Guid UnidadeId)
        {
            DateTime dinicio = new DateTime(Convert.ToInt32(inicio.Substring(6, 4)), Convert.ToInt32(inicio.Substring(3, 2)), Convert.ToInt32(inicio.Substring(0, 2)));
            DateTime dfim = new DateTime(Convert.ToInt32(fim.Substring(6, 4)), Convert.ToInt32(fim.Substring(3, 2)), Convert.ToInt32(fim.Substring(0, 2)));
            ListResult result = read.ListarEvolucaoMedica(dinicio, dfim, UnidadeId);
            var data = result.ToListResult();

            return Json(data);
        }
    }
}
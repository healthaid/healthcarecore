﻿using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.CQRS.Read.Interfaces.Atendimento;
using HealthCare.Infra.CQRS.Write.Commands;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace HealthCare.WebApi.Controllers
{
    [EnableCors("AllowCors")]
    [Route("api/[controller]")]
    public class AtestadoController : Controller
    {
        private readonly ICommandBus bus;
        private readonly IAtestadoDao read;
        private readonly ITemplatesAtestadoDto readTemplate;
        private readonly ICidDao readCid;


        public AtestadoController(ICommandBus bus, IAtestadoDao read, ITemplatesAtestadoDto readTemplate, ICidDao readCid)
        {
            this.bus = bus;
            this.read = read;
            this.readTemplate = readTemplate;
            this.readCid = readCid;
        }

        [HttpGet("[action]")]
        public IActionResult GetAtestadoPorAtendimentoId(Guid idAtendimento)
        {
            try
            {
                return Json(read.ObterAtestadoPorAtendimento(idAtendimento));
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex);
            }
        }

        [HttpGet("[action]")]
        public IActionResult GetAtestadoPorId(Guid id)
        {
            try
            {
                return Json(read.ObterAtestado(id));
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex);
            }
        }

        [HttpGet("[action]")]
        public IActionResult GetAtestadoReportPorAtendimentoId(Guid atendimentoId)
        {
            try
            {
                return Json(read.ObterReportPorAtendimento(atendimentoId));
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex);
            }
        }

        [HttpPost("[action]")]
        public IActionResult SalvarAtestado([FromBody]CreateAtestadoCommand model)
        {
            try
            {
                if (model.AtestadoId == Guid.Empty)
                {
                    bus.Send(model);
                    return Json(StatusCodes.Status200OK);
                }
                else
                {
                    UpdateAtestadoCommand updateCommand = new UpdateAtestadoCommand();
                    updateCommand.AtestadoId = model.AtestadoId;
                    updateCommand.Descricao = model.Descricao;
                    updateCommand.CodigoCid = model.CodigoCid;
                    updateCommand.Dias = model.Dias;
                    bus.Send(updateCommand);
                    return Json(StatusCodes.Status200OK);
                }
            }
            catch (Exception ex)
            {
                return Json(new ResponseMessage(StatusCodes.Status500InternalServerError, ex));
            }
        }

        [HttpPost("[action]")]
        public IActionResult AtualizarAtestado([FromBody]UpdateAtestadoCommand model)
        {
            try
            {
                UpdateAtestadoCommand updateCommand = new UpdateAtestadoCommand();
                updateCommand.Id = model.Id;
                updateCommand.Descricao = model.Descricao;
                updateCommand.CodigoCid = model.CodigoCid;
                bus.Send(updateCommand);
                return Json(StatusCodes.Status200OK);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex);
            }
        }

        [HttpGet("[action]")]
        public IActionResult ListarTodosCids(string descricao)
        {
            try
            {
                return Json(readCid.ListarTodosCids(descricao).AsQueryable().Select(x => x.Codigo + " - " + x.Descricao).ToArray());
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex);
            }
        }
    }
}
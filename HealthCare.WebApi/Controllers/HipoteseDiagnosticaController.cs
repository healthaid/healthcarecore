﻿using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.CQRS.Write.Commands;
using HealthCare.Infra.CQRS.Write.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace HealthCare.WebApi.Controllers
{
    [EnableCors("AllowCors")]
    [Route("api/[controller]")]
    public class HipoteseDiagnosticaController : Controller
    {
        private readonly ICommandBus bus;
        private readonly ICidDao readCid;
        private readonly IHipoteseDiagnosticaDao read;

        public HipoteseDiagnosticaController(ICommandBus bus, ICidDao readCid, IHipoteseDiagnosticaDao read)
        {
            this.bus = bus;
            this.readCid = readCid;
            this.read = read;
        }

        [HttpGet("[action]")]
        public Array ListarCids()
        {
            return readCid.ListarCids().Select(x => x.Descricao).ToArray();
        }

        [HttpGet("[action]")]
        public Array ListarCidPorDescricao(string descricao)
        {
            return readCid.ListarCids().Where(x => x.Descricao.Contains(descricao)).Select(x => x.Codigo + " - " + x.Descricao).ToArray();
        }

        [HttpPost("[action]")]
        public void SalvarHipoteseDiagnostica([FromBody]HipoteseDiagnosticaWriteModel model)
        {
            if (model.Id == Guid.Empty)
            {
                CreateHipoteseDiagnosticaCommand createCommand = new CreateHipoteseDiagnosticaCommand();
                createCommand.AtendimentoId = model.AtendimentoId;
                createCommand.Observacao = model.Observacao;
                createCommand.Cids = model.Cids;
                bus.Send(createCommand);
            }
            else
            {
                UpdateHipoteseDiagnosticaCommand updateCommand = new UpdateHipoteseDiagnosticaCommand();
                updateCommand.Id = model.Id;
                updateCommand.AtendimentoId = model.AtendimentoId;
                updateCommand.Observacao = model.Observacao;
                updateCommand.Cids = model.Cids;
                bus.Send(updateCommand);
            }
        }

        [HttpGet("[action]")]
        public HipoteseDiagnosticaReadViewModel ObterHipoteseDiagnosticaPorAtendimento(Guid idAtendimento)
        {
            return read.ObterHipoteseDiagnosticaPorAtendimento(idAtendimento);
        }

        [HttpGet("[action]")]
        public HipoteseDiagnosticaReadViewModel ObterHipoteseDiagnosticaPorId(Guid Id)
        {
            return read.ObterHipoteseDiagnostica(Id);
        }
    }
}
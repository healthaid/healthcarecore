﻿using HealthCare.Infra.Common.Extensions;
using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.CQRS.Read.Interfaces.Atendimento;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.CQRS.Write.Commands;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace HealthCare.WebApi.Controllers
{
    [EnableCors("AllowCors")]
    [Route("api/[controller]")]
    public class ServicoSocialController : Controller
    {
        private readonly ICommandBus bus;
        private readonly IServicoSocialDto read;

        public ServicoSocialController(ICommandBus bus, IServicoSocialDto read)
        {
            this.bus = bus;
            this.read = read;
        }
        
        [HttpPost("[action]")]
        public IActionResult SalvarServicoSocial([FromBody]CreateServicoSocialCommand model)
        {
            try
            {
                object objReturned = bus.SendWithReturn(model);
                if (objReturned.GetType() == typeof(Validation))
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(((Validation)objReturned).Messages);
                }
                return Json(objReturned);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpGet("[action]")]
        public IActionResult ObterServicoSocial(string acolhimentoId)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(acolhimentoId) == false)
                {
                    ServicoSocialReadModel ServicoSocial = read.ObterServicoSocialPorAcolhimento(Guid.Parse(acolhimentoId));
                    return Json(ServicoSocial);
                }
                else
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(StatusCodes.Status400BadRequest);
                }
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex);
            }
        }

        [HttpGet("[action]")]
        public IActionResult ListarServicoSocialsDoDia(Guid idUnidade, Guid idProfissional)
        {
            ListResult result = read.ListarServicoSocial(idUnidade, idProfissional);
            var data = result.ToListResult();

            if (data.Columns.Count != 0)
            {
                data.Columns.FirstOrDefault(x => x.Name == "Id").AllowVisible = false;
                data.Columns.FirstOrDefault(x => x.Name == "AcolhimentoId").AllowVisible = false;
                data.Columns.FirstOrDefault(x => x.Name == "FimAtendimento").AllowVisible = false;
                data.Columns.FirstOrDefault(x => x.Name == "Checkout").AllowVisible = false;
                data.Columns.FirstOrDefault(x => x.Name == "Observacao").AllowVisible = false;
                data.Columns.FirstOrDefault(x => x.Name == "InicioAtendimento").ColumnWidth = "50px";
                data.Columns.FirstOrDefault(x => x.Name == "InicioAtendimento").DisplayName = "Hora do ServicoSocial";
                data.Columns.FirstOrDefault(x => x.Name == "Nome").ColumnWidth = "50px";
                data.Columns.FirstOrDefault(x => x.Name == "Nome").DisplayName = "Paciente";
                data.Columns.FirstOrDefault(x => x.Name == "Sexo").ColumnWidth = "50px";
                data.Columns.FirstOrDefault(x => x.Name == "Sexo").DisplayName = "Sexo";
                data.Columns.FirstOrDefault(x => x.Name == "Idade").ColumnWidth = "50px";
                data.Columns.FirstOrDefault(x => x.Name == "Idade").DisplayName = "Idade";
            }
            return Json(data);
        }
    }
}

﻿using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.CQRS.Write.Commands;
using HealthCare.Infra.CQRS.Write.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;

namespace HealthCare.WebApi.Controllers
{
    [EnableCors("AllowCors")]
    [Route("api/[controller]")]
    public class ImagemAtendimentoController : Controller
    {
        private readonly ICommandBus bus;
        private readonly IImagemDao readImagem;
        private readonly IImagemAtendimentoDao read;

        public ImagemAtendimentoController(ICommandBus bus, IImagemDao readImagem, IImagemAtendimentoDao read)
        {
            this.bus = bus;
            this.readImagem = readImagem;
            this.read = read;
        }

        [HttpGet("[action]")]
        public ImagemAtendimentoReadViewModel ListarImagemsPorImagemAtendimento(Guid idImagemAtendimento)
        {
            var imagem = read.ObterImagemAtendimentoPorId(idImagemAtendimento);
            return imagem;
        }

        [HttpPost("[action]")]
        public void SalvarImagemAtendimento([FromBody]ImagemAtendimentoWriteModel model)
        {
                CreateImagemAtendimentoCommand createCommand = new CreateImagemAtendimentoCommand();
                createCommand.AtendimentoId = model.AtendimentoId;
                createCommand.Observacao = model.Observacao;
                createCommand.Imagens = model.Imagens;
                bus.Send(createCommand);
        }

        [HttpPut("[action]")]
        public void AtualizarImagemAtendimento([FromBody]ImagemAtendimentoWriteModel model)
        {

            UpdateImagemAtendimentoCommand updateCommand = new UpdateImagemAtendimentoCommand();
            updateCommand.Id = model.Id;
            updateCommand.AtendimentoId = model.AtendimentoId;
            updateCommand.Observacao = model.Observacao;
            updateCommand.Imagens = model.Imagens;
            bus.Send(updateCommand);

        }
        
        [HttpGet("[action]")]
        public ImagemAtendimentoReadViewModel ObterImagemAtendimentoPorId(Guid Id)
        {
            return read.ObterImagemAtendimentoPorId(Id);
        }
    }
}
﻿using HealthCare.Infra.Common.Extensions;
using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.CQRS.Read.Interfaces.Cadastro;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.CQRS.Write.Commands;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HealthCare.WebApi.Controllers
{
    [EnableCors("AllowCors")]
    [Route("api/[controller]")]
    public class ProfissionalController : Controller
    {

        private readonly ICommandBus bus;
        private readonly IProfissionalDao read;
        private readonly IUnidadeDao readUnidade;
        private readonly ICboDao readCbo;
        private readonly IRoleDao roleDao;
        private readonly IUserDao userDao;

        public ProfissionalController(ICommandBus bus, IProfissionalDao read, IUnidadeDao readUnidade, ICboDao readCbo, IRoleDao roleDao, IUserDao userDao)
        {
            this.bus = bus;
            this.read = read;
            this.readUnidade = readUnidade;
            this.readCbo = readCbo;
            this.roleDao = roleDao;
            this.userDao = userDao;
        }


        [HttpGet("[action]")]
        public IList<ProfissionalReadModel> ListarProfissionais(string unidade)
        {
            try
            {
                return read.ListarProfissional(Guid.Parse(unidade));
            }
            catch (Exception)
            {
                throw;
            }
        }
        
        [HttpGet("[action]")]
        public IActionResult ListarProfissionals(string filtro, string unidade)
        {
            ListResult result = read.ListarProfissional(filtro, Guid.Parse(unidade));
            var data = result.ToListResult();
            if (data.Columns.Count != 0)
            {
                data.Columns.FirstOrDefault(x => x.Name == "Id").AllowVisible = false;
                data.Columns.FirstOrDefault(x => x.Name == "Id").DisplayName = "Id do Profissional";
                data.Columns.FirstOrDefault(x => x.Name == "Nome").ColumnWidth = "200px";
                data.Columns.FirstOrDefault(x => x.Name == "Nome").DisplayName = "Nome";
                data.Columns.FirstOrDefault(x => x.Name == "Sexo").DisplayName = "Sexo";
                data.Columns.FirstOrDefault(x => x.Name == "CNS").DisplayName = "Cns";
                data.Columns.FirstOrDefault(x => x.Name == "Nascimento").DisplayName = "Data Nascimento";
            }
            return Json(data);
        }

        [HttpPost("[action]")]
        public IActionResult SalvarProfissional([FromBody]CreateProfissionalCommand model)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(model.CPF))
                    model.CPF = model.CPF.Replace(".", "").Replace("-", "");
                object objReturned = bus.SendWithReturn(model);
                if (objReturned.GetType() == typeof(Validation))
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(((Validation)objReturned).Messages);
                }
                return Json(objReturned);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpPost("[action]")]
        public IActionResult ConfigurarPrimeiroAcesso([FromBody]FirstAcessCommand command)
        {
            try
            {
                bus.Send(command);
                SetUserFirstLoginFalse setCommand = new SetUserFirstLoginFalse();
                setCommand.UserId = command.UsuarioId;
                setCommand.UserName = command.UserName;
                bus.Send(setCommand);
                var secretKey = "GQDstcKsx0NHjPOuXOYg5MbeJ1XT0uFiwDVvVBrk";
                var unixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                var now = Math.Round((DateTime.UtcNow.AddMinutes(30) - unixEpoch).TotalSeconds);
                UserReadViewModel user = this.userDao.GetUserById(setCommand.UserId, Guid.Empty);
                if (user != null)
                {
                    user.Anonymous = false;
                    var payload = new Dictionary<string, object>()
                    {
                        {"user", user },
                        {"exp", now }
                    };
                    // string token = JWT.JsonWebToken.Encode(payload, secretKey, JWT.JwtHashAlgorithm.HS256);
                    return Json(string.Empty);
                }
                HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                return Json(StatusCodes.Status400BadRequest);

            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpGet("[action]")]
        public IActionResult ListarUnidadesCombo()
        {
            try
            {
                ListResult result = readUnidade.ListarTodasUnidades();
                var data = result.ToListResult();
                if (data.Columns.Count != 0)
                {
                    data.Columns.FirstOrDefault(x => x.Name == "Id").ColumnWidth = "200px";
                    data.Columns.FirstOrDefault(x => x.Name == "Id").DisplayName = "Sigla";
                    data.Columns.FirstOrDefault(x => x.Name == "NomeUnidade").ColumnWidth = "200px";
                    data.Columns.FirstOrDefault(x => x.Name == "NomeUnidade").DisplayName = "Nome";

                }
                return Json(data);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpDelete("[action]")]
        public IActionResult DeleteProfissional(string ProfissionalId)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(ProfissionalId) == false)
                {
                    DeleteProfissionalCommand command = new DeleteProfissionalCommand();
                    command.ProfissionalId = Guid.Parse(ProfissionalId);
                    object objReturned = bus.SendWithReturn(command);
                    if (objReturned.GetType() != typeof(Validation))
                        return Json(StatusCodes.Status200OK);
                    else
                    {
                        return Json(((Validation)objReturned).Messages);
                    }
                }
                else
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(StatusCodes.Status400BadRequest);
                }
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpGet("[action]")]
        public IActionResult GetProfissionalById(string ProfissionalId)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(ProfissionalId) == false)
                {
                    ProfissionalReadModel Profissional = read.GetProfissionalById(Guid.Parse(ProfissionalId));
                    return Json(Profissional);
                }
                else
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(StatusCodes.Status400BadRequest);
                }
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpGet("[action]")]
        public IActionResult ListarCBOs(string filtro)
        {

            var cbos = readCbo.ListarCbos(filtro).Select(x => x.Codigo + " - " + x.Descricao).ToArray();

            return Json(cbos);
        }

        [HttpGet("[action]")]
        public IActionResult ListarRoles()
        {
            try
            {
                List<RoleReadModel> roles = this.roleDao.GetRoles();
                return Json(roles);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpGet("[action]")]
        public IActionResult GetUserByProfissionalId(string ProfissionalId)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(ProfissionalId) == false)
                {
                    UserReadViewModel Profissional = userDao.GetUserByProfissionalId(Guid.Parse(ProfissionalId));
                    return Json(Profissional);
                }
                else
                    return Json(StatusCodes.Status400BadRequest);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpGet("[action]")]
        public IActionResult ProfissionalEMedico(string profissionalId)
        {
            return Json(read.ProfissionalEMedico(Guid.Parse(profissionalId)));
        }

        [HttpGet("[action]")]
        public IList<ProfissionalReadModel> ListarProfissionaisPorCbo(string cbo)
        {
            try
            {
                List<string> cbos = new List<string>();
                cbos.Add(cbo);
                return read.ListarProfissionaisPorCbos(cbos);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                throw ex;
            }
        }
    }
}
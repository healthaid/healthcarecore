﻿using HealthCare.Infra.Common.Extensions;
using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.CQRS.Read.Interfaces.Atendimento;
using HealthCare.Infra.CQRS.Write.Commands;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace HealthCare.WebApi.Controllers
{
    [EnableCors("AllowCors")]
    [Route("api/[controller]")]
    public class MotivoCheckoutController : Controller
    {

        private readonly ICommandBus bus;
        private readonly IMotivoCheckoutDto read;
        private readonly ITipoMotivoChekoutDto readTipoMotivo;

        public MotivoCheckoutController(ICommandBus bus, IMotivoCheckoutDto read, ITipoMotivoChekoutDto _readTipoMotivo)
        {
            this.bus = bus;
            this.read = read;
            this.readTipoMotivo = _readTipoMotivo;
        }
        
        [HttpGet("[action]")]
        public IActionResult ListarMotivosPorTipo(string tipoId)
        {
            ListResult result = read.ListarMotivosPorTipo(Guid.Parse(tipoId));
            var data = result.ToListResult();
            if (data.Columns.Count != 0)
            {
                data.Columns.FirstOrDefault(x => x.Name == "Id").AllowVisible = false;
                data.Columns.FirstOrDefault(x => x.Name == "Id").DisplayName = "Id do MotivoCheckout";
                data.Columns.FirstOrDefault(x => x.Name == "Descricao").ColumnWidth = "200px";
                data.Columns.FirstOrDefault(x => x.Name == "Descricao").DisplayName = "Nome";
            }
            return Json(data);
        }

        [HttpGet("[action]")]
        public IActionResult ListarMotivoCheckouts()
        {
            ListResult result = read.ListarMotivosAtivos();
            var data = result.ToListResult();
            if (data.Columns.Count != 0)
            {
                data.Columns.FirstOrDefault(x => x.Name == "Id").AllowVisible = false;
                data.Columns.FirstOrDefault(x => x.Name == "Id").DisplayName = "Id do MotivoCheckout";
                data.Columns.FirstOrDefault(x => x.Name == "Descricao").ColumnWidth = "200px";
                data.Columns.FirstOrDefault(x => x.Name == "Descricao").DisplayName = "Nome";
            }
            return Json(data);
        }

        [HttpGet("[action]")]
        public IActionResult ListarTiposCheckout()
        {
            ListResult result = readTipoMotivo.ListarTipos();
            var data = result.ToListResult();
            if (data.Columns.Count != 0)
            {
                data.Columns.FirstOrDefault(x => x.Name == "Id").AllowVisible = false;
                data.Columns.FirstOrDefault(x => x.Name == "Id").DisplayName = "Id do MotivoCheckout";
                data.Columns.FirstOrDefault(x => x.Name == "Descricao").ColumnWidth = "200px";
                data.Columns.FirstOrDefault(x => x.Name == "Descricao").DisplayName = "Nome";
            }
            return Json(data);
        }

        [HttpPost("[action]")]
        public IActionResult SalvarMotivoCheckout([FromBody]SaveMotivoCheckoutCommand model)
        {
            try
            {
                object objReturned = bus.SendWithReturn(model);
                if (objReturned.GetType() == typeof(Validation))
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(((Validation)objReturned).Messages);
                }
                return Json(objReturned);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpDelete("[action]")]
        public IActionResult DeleteMotivoCheckout(string id)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(id) == false)
                {
                    DeleteMotivoCheckoutCommand command = new DeleteMotivoCheckoutCommand();
                    command.MotivoCheckoutId = Guid.Parse(id);
                    object objReturned = bus.SendWithReturn(command);
                    if (objReturned.GetType() != typeof(Validation))
                        return Json(StatusCodes.Status200OK);
                    else
                    {
                        return Json(((Validation)objReturned).Messages);
                    }
                }
                else
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(StatusCodes.Status400BadRequest);
                }
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }
    }
}
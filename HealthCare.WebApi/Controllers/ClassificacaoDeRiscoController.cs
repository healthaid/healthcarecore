﻿using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.CQRS.Read.Interfaces.Atendimento;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.CQRS.Write.Commands;
using HealthCare.Infra.CQRS.Write.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Net;
using System.Net.Http;

namespace HealthCare.WebApi.Controllers
{
    [EnableCors("AllowCors")]
    [Route("api/[controller]")]
    public class ClassificacaoDeRiscoController : Controller
    {
        private readonly ICommandBus bus;
        private readonly IAcolhimentoDTO readAcolhimento;
        private readonly IClassificacaoDeRiscoDto read;

        public ClassificacaoDeRiscoController(ICommandBus _bus, IAcolhimentoDTO _readAcolhimento, IClassificacaoDeRiscoDto _read)
        {
            this.bus = _bus;
            this.readAcolhimento = _readAcolhimento;
            this.read = _read;
        }

        [HttpPost("[action]")]
        public IActionResult SalvarClassificacaoDeRisco([FromBody]SaveClassificacaoDeRiscoCommand model)
        {
            try
            {
                object objReturned = bus.SendWithReturn(model);
                if (objReturned.GetType() == typeof(Validation))
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(((Validation)objReturned).Messages);
                }
                return Json(objReturned);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpPost("[action]")]
        public IActionResult AtualizarClassificacaoPaciente([FromBody]AtualizarClassificacaoRiscoCommand model)
        {
            try
            {
                object objReturned = bus.SendWithReturn(model);
                if (objReturned.GetType() == typeof(Validation))
                {
                    return Json(((Validation)objReturned).Messages);
                }
                return Json(StatusCodes.Status200OK);
            }
            catch (Exception ex)
            {
                return Json(ex.InnerException);
            }
        }

        [HttpGet("[action]")]
        public IActionResult ObterPacientePorAcolhimento(string idAcolhimento)
        {
            try
            {
                return Json(readAcolhimento.ObterPacientePorAcolhimento(Guid.Parse(idAcolhimento)));
            }
            catch (Exception ex)
            {
                return Json(ex.InnerException);
            }
        }

        [HttpGet("[action]")]
        public IActionResult ObterClassificacaoDeRiscoPorAcolhimento(string idAcolhimento)
        {
            try
            {
                return Json(readAcolhimento.GetAcolhimentoWithClassificacaoRico(Guid.Parse(idAcolhimento)));
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpGet("[action]")]
        public IActionResult ClassificacoesDeRisco()
        {
            try
            {
                return Json(read.ClassificacoesDeRisco());
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }
    }
}
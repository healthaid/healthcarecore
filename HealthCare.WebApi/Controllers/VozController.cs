﻿using HealthCare.Infra.Common.Extensions;
using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.CQRS.Read.Interfaces.Voz;
using HealthCare.Infra.CQRS.Write.Commands;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HealthCare.WebApi.Controllers
{
    [EnableCors("AllowCors")]
    [Route("api/[controller]")]
    public class VozController : Controller
    {
        private readonly IVozCommandDao vozCommandDao;

        private readonly ICommandBus bus;

        public VozController(IVozCommandDao vozCommand, ICommandBus bus)
        {
            this.vozCommandDao = vozCommand;
            this.bus = bus;
        }

        [HttpGet]
        [Route("ChamarPaciente")]
        public  IActionResult ChamarPaciente(Guid unidadeId)
        {
            try
            {
                var data =  vozCommandDao.GetPacientesParaSeremChamados(unidadeId);
                return Json(data);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpGet]
        [Route("UltimosPacientesAtendidos")]
        public IActionResult UltimosPacientesAtendidos(string unidadeId)
        {
            try
            {
                if (!string.IsNullOrEmpty(unidadeId))
                {
                    ListResult result = vozCommandDao.GetUltimosPacientesChamados(Guid.Parse(unidadeId));
                    var data = result.ToListResult();
                    if (data.Columns.Count != 0)
                    {

                        data.Columns.FirstOrDefault(x => x.Name == "NomePaciente").ColumnWidth = "90px";
                        data.Columns.FirstOrDefault(x => x.Name == "NomePaciente").DisplayName = "Paciente";
                        data.Columns.FirstOrDefault(x => x.Name == "LocalAtendimento").ColumnWidth = "90px";
                        data.Columns.FirstOrDefault(x => x.Name == "LocalAtendimento").DisplayName = "Local";
                        data.Columns.FirstOrDefault(x => x.Name == "Servico").ColumnWidth = "90px";
                    }
                    return Json(data);
                }
                else
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(StatusCodes.Status400BadRequest);
                }
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpPost]
        [Route("InserirChamadaPainel")]
        public IActionResult InserirChamadaPainel([FromBody]InsertItemPainelCommand command)
        {
            try
            {
                if (command != null)
                {
                    bus.Send(command);
                    return Json(StatusCodes.Status200OK);
                }
                else
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(StatusCodes.Status400BadRequest);
                }
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpPost]
        [Route("UpdateItensPainel")]
        public IActionResult UpdateItensPainel([FromBody]UpdateItensPainelCommand command)
        {
            try
            {
                if (command != null)
                {
                    bus.Send(command);
                    return Json(StatusCodes.Status200OK);
                }
                else
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(StatusCodes.Status400BadRequest);
                }
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }
    }
}

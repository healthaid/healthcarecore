﻿using AutoMapper;
using HealtCare.Infra.Common.Utils;
using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.CQRS.Read.Interfaces.Cadastro;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.CQRS.Write.Commands;
using HealthCare.Service.CQRS.Write.Commands;
using HealthCare.Service.CQRS.Write.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;

namespace HealthCare.WebApi.Controllers
{
    [EnableCors("AllowCors")]
    [Route("api/[controller]")]
    public class LoginController : Controller
    {
        private readonly ICommandBus bus;
        private readonly IUserDao read;
        private readonly ILocalAtendimentoDao localAtendimentoDao;
        private readonly ILogger<LoginController> logger;

        public LoginController(ICommandBus bus, IUserDao read, ILocalAtendimentoDao localAtendimentoDao, IMapper mapper, ILogger<LoginController> logger)
        {
            this.bus = bus;
            this.read = read;
            this.localAtendimentoDao = localAtendimentoDao;
            ViewModelUtils.mapper = mapper;
            this.logger = logger;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public IActionResult Authenticate([FromBody]AuthenticateViewWriteModel viewModel)
        {
            try
            {
                logger.LogInformation("Começando autenticação");
                //var secretKey = "GQDstcKsx0NHjPOuXOYg5MbeJ1XT0uFiwDVvVBrk";
                //var unixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                //var now = Math.Round((DateTime.UtcNow.AddMinutes(30) - unixEpoch).TotalSeconds);
                UserReadViewModel user = read.VerifyIfUserExists(viewModel.UserName, viewModel.Password);
                if (user != null)
                {
                    //user.Anonymous = false;
                    //var payload = new Dictionary<string, object>()
                    //{
                    //    {"user", user },
                    //    {"exp", now }
                    //};
                    //string token = JWT.JsonWebToken.Encode(payload, secretKey, JWT.JwtHashAlgorithm.HS256);
                    logger.LogInformation("Terminando autenticação");
                    return Json(user);
                }
                else
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status403Forbidden;
                    return Json("Usuário não existe");
                }

            }
            catch (Exception ex)
            {
                logger.LogInformation("Erro na autenticação: " + ex.Message + " /n " + ex.InnerException);
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.Message);
            }
        }

        [HttpGet("[action]")]
        public IActionResult ValidateToken(string userId, string idDelegacaoSelecionada)
        {
            try
            {
                logger.LogInformation("Começando validação do token");
                if (string.IsNullOrWhiteSpace(userId) == false)
                {
                    //var secretKey = "GQDstcKsx0NHjPOuXOYg5MbeJ1XT0uFiwDVvVBrk";
                    //var unixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                    //string jsonPayload = JWT.JsonWebToken.Decode(token, secretKey);
                    //Dictionary<string, object> dictonary = new JavaScriptSerializer().Deserialize<Dictionary<string, object>>(jsonPayload);
                    //var user = dictonary["user"];
                    //var now = Math.Round((DateTime.UtcNow.AddMinutes(30) - unixEpoch).TotalSeconds);
                    //Guid id = Guid.Parse(((Dictionary<string, object>)user)["Id"].ToString());
                    UserReadViewModel userGet;
                    if (!string.IsNullOrWhiteSpace(idDelegacaoSelecionada))
                    {
                        Guid idDelegacao = Guid.Parse(idDelegacaoSelecionada);
                        if (idDelegacao != Guid.Empty)
                        {
                            userGet = read.GetUserById(Guid.Parse(userId), Guid.Parse(idDelegacaoSelecionada));
                        }
                        else
                            userGet = read.GetUserById(Guid.Parse(userId));
                    }
                    else
                        userGet = read.GetUserById(Guid.Parse(userId));

                    //userGet.Anonymous = false;
                    //var payload = new Dictionary<string, object>()
                    //{
                    //    {"user", user },
                    //    {"exp", now }
                    //};
                    //string newToken = JWT.JsonWebToken.Encode(payload, secretKey, JWT.JwtHashAlgorithm.HS256);
                    logger.LogInformation("Terminando validação do token");
                    return Json(userGet);

                }
                else
                {
                    return Json(new UserReadViewModel() { Anonymous = true });
                }
            }
            //catch (SignatureVerificationException ex)
            //{
            //    return Request.CreateResponse(HttpStatusCode.OK, new UserReadViewModel() { Anonymous = true });
            //}
            catch (Exception ex)
            {
                logger.LogInformation("Erro validação do token: " + ex.Message + " /n " + ex.ToString());
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.Message);
            }
        }


        /// <summary>
        /// Api para inserir um usuário no sistema.
        /// </summary>
        /// <param name="model">Modelo para a inserção dos dados</param>
        /// <returns>Retorna uma </returns>
        [HttpPost("[action]")]
        public IActionResult Insert([FromBody]CreateUserCommand model)
        {
            try
            {
                object objReturned = bus.SendWithReturn(model);
                if (objReturned.GetType() == typeof(Validation))
                    return Json(new ResponseMessage(StatusCodes.Status200OK));
                else
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(((Validation)objReturned).Messages);
                }
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpPost("[action]")]
        public IActionResult RedefinePassword([FromBody]RedefineUserPasswordCommand command)
        {
            try
            {
                if (command != null)
                {
                    this.bus.Send(command);
                    return Json(StatusCodes.Status200OK);
                }
                else
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(StatusCodes.Status400BadRequest);
                }
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpPost("[action]")]
        public IActionResult ResetPassword([FromBody]ResetPasswordCommand command)
        {
            try
            {
                if (command.UserName != null)
                {
                    this.bus.Send(command);
                    return Json(StatusCodes.Status200OK);
                }
                else
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(StatusCodes.Status400BadRequest);
                }
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpPost("[action]")]
        public IActionResult UpdateUserProfile([FromBody]UpdateUserCommand command)
        {
            try
            {
                if (command != null)
                {
                    object obj = this.bus.SendWithReturn(command);
                    if (obj is Validation)
                    {
                        HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                        return Json(((Validation)obj).Messages);
                    }
                    else
                        return Json(obj);
                }
                else
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(StatusCodes.Status400BadRequest);
                }
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpGet("[action]")]
        public IActionResult ListarTodosLocaisAtendimentos()
        {
            try
            {
                List<LocalAtendimentoViewModel> lista = localAtendimentoDao.LocaisAtendimentos();
                return Json(lista);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

    }
}

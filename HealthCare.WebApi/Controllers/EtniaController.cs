﻿using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.CQRS.Write.Commands;
using HealthCare.Service.CQRS.Write.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace HealthCare.WebApi.Controllers
{
    [EnableCors("AllowCors")]
    [Route("api/[controller]")]
    public class EtniaController : Controller
    {
        private readonly ICommandBus bus;
        private readonly IEtniaDao read;

        public EtniaController(ICommandBus bus, IEtniaDao read)
        {
            this.bus = bus;
            this.read = read;
        }

        [HttpGet("[action]")]
        public IQueryable<EtniaWriteModel> ListarEtnias()
        {
            return (from a in read.ListarEtnias()
                    select new EtniaWriteModel
                    {
                        Descricao = a.Descricao
                    }).AsQueryable();

        }

        [HttpPost("[action]")]
        public IActionResult CadastrarEtnia([FromBody]EtniaWriteModel model)
        {
            CreateEtniaCommand c = new CreateEtniaCommand();
            c.Descricao = model.Descricao;
            c.Ativo = model.Ativo;
            bus.Send(c);
            return Json(StatusCodes.Status200OK);
        }
    }
}

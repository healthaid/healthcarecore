﻿using HealthCare.Infra.Common.Extensions;
using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.CQRS.Read.Interfaces.Atendimento;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.CQRS.Write.Commands;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace HealthCare.WebApi.Controllers
{
    [EnableCors("AllowCors")]
    [Route("api/[controller]")]
    public class ExameController : Controller
    {

        private readonly ICommandBus bus;
        private readonly ITipoExameDto readTipoExame;
        private readonly IExameDto readExame;
        private readonly IExameAtendimentoDto read;

        public ExameController(ICommandBus bus, ITipoExameDto readTipoExame, IExameDto readExame, IExameAtendimentoDto readExameAtendimento)
        {
            this.bus = bus;
            this.readTipoExame = readTipoExame;
            this.readExame = readExame;
            this.read = readExameAtendimento;
        }

        [HttpPost("[action]")]
        public IActionResult SalvarExameAtendimento([FromBody]SaveExameAtendimentoCommand model)
        {
            try
            {
                object objReturned = bus.SendWithReturn(model);
                if (objReturned.GetType() == typeof(Validation))
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(((Validation)objReturned).Messages);
                }
                return Json(objReturned);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpPost("[action]")]
        public IActionResult RealizarExameCommand([FromBody]RealizarExameCommand model)
        {
            try
            {
                object objReturned = bus.SendWithReturn(model);
                if (objReturned.GetType() == typeof(Validation))
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(((Validation)objReturned).Messages);
                }
                return Json(objReturned);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpGet("[action]")]
        public IActionResult ObterExameAtendimentoPorId(string id)
        {
            try
            {
                ExameAtendimentoReadModel receita = read.GetExameAtendimentoById(Guid.Parse(id));
                return Json(receita);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpGet("[action]")]
        public IActionResult ListarExameAtendimentoPorAtendimento(string atendimentoId)
        {
            try
            {
                ListResult result = read.ListarExameAtendimentosByAtendimentoId(Guid.Parse(atendimentoId));
                var data = result.ToListResult();

                if (data.Columns.Count != 0)
                {
                    data.Columns.FirstOrDefault(x => x.Name == "Id").AllowVisible = false;
                    data.Columns.FirstOrDefault(x => x.Name == "Descricao").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "Descricao").DisplayName = "Exame";
                }
                return Json(data);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpGet("[action]")]
        public IActionResult ListarExameAtendimentosByAtendimentoId(string atendimentoId, string definicao, string origem)
        {
            try
            {
                ListResult result = read.ListarExameAtendimentosByAtendimentoId(Guid.Parse(atendimentoId), definicao, origem);
                var data = result.ToListResult();

                if (data.Columns.Count != 0)
                {
                    data.Columns.FirstOrDefault(x => x.Name == "Id").AllowVisible = false;
                    data.Columns.FirstOrDefault(x => x.Name == "Descricao").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "Descricao").DisplayName = "Exame";
                    data.Columns.FirstOrDefault(x => x.Name == "Profissional").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "Profissional").DisplayName = "Profissional";
                    data.Columns.FirstOrDefault(x => x.Name == "Executado").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "Executado").DisplayName = "Executado";
                    data.Columns.FirstOrDefault(x => x.Name == "Observacao").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "Observacao").DisplayName = "Observacao";
                    data.Columns.FirstOrDefault(x => x.Name == "ObservacaoTecnica").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "ObservacaoTecnica").DisplayName = "Observacao Técnica";
                }
                return Json(data);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpGet("[action]")]
        public IActionResult ListarExameAtendimentosPendente(string definicao)
        {
            try
            {
                ListResult result = read.ListarExameAtendimentosPendente(definicao);
                var data = result.ToListResult();

                if (data.Columns.Count != 0)
                {
                    data.Columns.FirstOrDefault(x => x.Name == "Id").AllowVisible = false;
                    data.Columns.FirstOrDefault(x => x.Name == "Nome").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "Nome").DisplayName = "Nome";
                    data.Columns.FirstOrDefault(x => x.Name == "Data").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "Data").DisplayName = "Data";
                    data.Columns.FirstOrDefault(x => x.Name == "Profissional").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "Profissional").DisplayName = "Profissional";
                }
                return Json(data);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpGet("[action]")]
        public IActionResult ListarExameAtendimentosPendentePorAtendimento(string definicao, string atendimentoId)
        {
            try
            {
                ListResult result = read.ListarExameAtendimentosPendentePorAtendimento(definicao, Guid.Parse(atendimentoId));
                var data = result.ToListResult();

                if (data.Columns.Count != 0)
                {
                    data.Columns.FirstOrDefault(x => x.Name == "Id").AllowVisible = false;
                    data.Columns.FirstOrDefault(x => x.Name == "Exame").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "Exame").DisplayName = "Exame";
                    data.Columns.FirstOrDefault(x => x.Name == "DataSolicitacao").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "DataSolicitacao").DisplayName = "Solicitado em";
                    data.Columns.FirstOrDefault(x => x.Name == "ObservacaoMedico").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "ObservacaoMedico").DisplayName = "Observacação";
                    data.Columns.FirstOrDefault(x => x.Name == "Paciente").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "Paciente").DisplayName = "Paciente";
                    data.Columns.FirstOrDefault(x => x.Name == "Procedimento").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "Procedimento").DisplayName = "Procedimento";
                    data.Columns.FirstOrDefault(x => x.Name == "Profissional").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "Profissional").DisplayName = "Profissional";
                    data.Columns.FirstOrDefault(x => x.Name == "ObservacaoTecnica").ColumnWidth = "50px";
                    data.Columns.FirstOrDefault(x => x.Name == "ObservacaoTecnica").DisplayName = "Observação Técnica";
                }
                return Json(data);
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpDelete("[action]")]
        public IActionResult DeleteExameAtendimento(string id)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(id) == false)
                {
                    DeleteExameAtendimentoCommand command = new DeleteExameAtendimentoCommand();
                    command.ExameAtendimentoId = Guid.Parse(id);
                    object objReturned = bus.SendWithReturn(command);
                    if (objReturned.GetType() != typeof(Validation))
                    {
                        return Json(StatusCodes.Status200OK);
                    }
                    else
                    {
                        return Json(((Validation)objReturned).Messages);
                    }
                }
                else
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                    return Json(StatusCodes.Status400BadRequest);
                }
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
                return Json(ex.InnerException);
            }
        }

        [HttpGet("[action]")]
        public Array ListarExames(string descricao, string tipo)
        {
            try
            {
                return readExame.ListarExames(descricao, tipo).AsQueryable().Select(x => x.Descricao).ToArray();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet("[action]")]
        public IActionResult ListarExamesRaioX(string inicio, string fim, Guid unidadeId)
        {
            try
            {
                DateTime dinicio = new DateTime(Convert.ToInt32(inicio.Substring(6, 4)), Convert.ToInt32(inicio.Substring(3, 2)), Convert.ToInt32(inicio.Substring(0, 2)));
                DateTime dfim = new DateTime(Convert.ToInt32(fim.Substring(6, 4)), Convert.ToInt32(fim.Substring(3, 2)), Convert.ToInt32(fim.Substring(0, 2)));
                ListResult result = read.ListarExamesRaioX(dinicio, dfim, unidadeId);
                var data = result.ToListResult();

                return Json(data);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}

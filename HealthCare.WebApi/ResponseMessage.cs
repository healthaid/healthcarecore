﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HealthCare.WebApi
{
    public class ResponseMessage
    {
        public ResponseMessage(int httpStatusCode, object data)
        {
            this.Data = data;
            this.Status = httpStatusCode;
        }

        public ResponseMessage(int httpStatusCode)
        {
            this.Status = httpStatusCode;
        }

        public ResponseMessage() { }

        public object Data { get; set; }

        public int Status { get; set; }

    }
}

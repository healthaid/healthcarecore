﻿using AutoMapper;
using HealthCare.Data.Messaging;
using HealthCare.Data.Messaging.Implementation;
using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.CQRS.Messaging.Handling;
using HealthCare.Infra.CQRS.Read.Configuration;
using HealthCare.Infra.CQRS.Read.Implementation;
using HealthCare.Infra.CQRS.Read.Implementation.Atendimento;
using HealthCare.Infra.CQRS.Read.Implementation.Cadastro;
using HealthCare.Infra.CQRS.Read.Implementation.ServicoSocial;
using HealthCare.Infra.CQRS.Read.Implementation.Voz;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.CQRS.Read.Interfaces.Atendimento;
using HealthCare.Infra.CQRS.Read.Interfaces.Cadastro;
using HealthCare.Infra.CQRS.Read.Interfaces.Voz;
using HealthCare.Infra.CQRS.Serialization;
using HealthCare.Infra.CQRS.Write.Commands.CommandsHandlers;
using HealthCare.Infra.CQRS.Write.Configuration;
using HealthCare.Infra.Data.Context;
using HealthCare.Service.CQRS.Read.Implementation.Cadastro;
using HealthCare.Service.CQRS.Read.Implementation.Faturamento;
using HealthCare.Service.CQRS.Read.Implementation.Http;
using HealthCare.Service.CQRS.Read.Interfaces.Cadastro;
using HealthCare.Service.CQRS.Read.Interfaces.Faturamento;
using HealthCare.Service.CQRS.Read.Interfaces.Http;
using HealthCare.Service.CQRS.Write.Commands.CommandsHandlers;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace HealthCare.WebApi
{
    public static class RegisterDi
    {
        public static void Register(IServiceCollection services, IConfigurationRoot configuration)
        {
            try
            {
                //services.AddTransient<ITextSerializer, JsonTextSerializer>();
                //services.AddTransient<IMessageSender, MessageSender>(t => new MessageSender("HealthAid", "Commands", configuration.GetConnectionString("MySQLConnection")));
                //services.AddTransient<IMessageReceiver, MessageReceiver>();
                //services.AddTransient<ICommandBus, CommandBus>();
                //services.AddTransient<ICommandBus>(t => new SynchronousCommandBusDecorator(t.GetService<CommandBus>(), t));
                //services.AddTransient<ICommandHandlerRegistry>( t => (SynchronousCommandBusDecorator)t.GetService<ICommandBus>());
                services.AddDbContext<HealthCareDbContext>(options => options.UseMySql(configuration.GetConnectionString("MySQLConnection")), ServiceLifetime.Transient);
                services.AddSingleton<IHttpClientFactory, HttpClientFactory>();
                #region Handlers

                services.AddTransient<ICommandHandler, PacienteHandler>();
                services.AddTransient<ICommandHandler, ProfissionalHandler>();
                services.AddTransient<ICommandHandler, ServicoHandler>();
                services.AddTransient<ICommandHandler, EtniaHandler>();
                services.AddTransient<ICommandHandler, ConvenioHandler>();
                services.AddTransient<ICommandHandler, EnderecoHandler>();
                services.AddTransient<ICommandHandler, DocumentoHandler>();
                services.AddTransient<ICommandHandler, ExameHandler>();
                services.AddTransient<ICommandHandler, ClinicaHandler>();
                services.AddTransient<ICommandHandler, ResultadoExameHandler>();
                services.AddTransient<ICommandHandler, AnamneseEventoHandler>();
                services.AddTransient<ICommandHandler, HipoteseDiagnosticaHandler>();
                services.AddTransient<ICommandHandler, AtestadoHandler>();
                services.AddTransient<ICommandHandler, TemplatesAtestadoHandler>();
                services.AddTransient<ICommandHandler, UserHandler>();
                services.AddTransient<ICommandHandler, UnidadeHandler>();
                services.AddTransient<ICommandHandler, AcolhimentoHandler>();
                services.AddTransient<ICommandHandler, ClassficacaoDeRiscoHandler>();
                services.AddTransient<ICommandHandler, SinaisVitaisHandler>();
                services.AddTransient<ICommandHandler, AtendimentoHandler>();
                services.AddTransient<ICommandHandler, MotivoCheckoutHandler>();
                services.AddTransient<ICommandHandler, ServicoSocialHandler>();
                services.AddTransient<ICommandHandler, CheckoutHandler>();
                services.AddTransient<ICommandHandler, ReceitaMedicaHandler>();
                services.AddTransient<ICommandHandler, PrescricaoHandler>();
                services.AddTransient<ICommandHandler, ExameAtendimentoHandler>();
                services.AddTransient<ICommandHandler, EnfermariaHandler>();
                services.AddTransient<ICommandHandler, LeitoHandler>();
                services.AddTransient<ICommandHandler, MovimentacaoLeitoHandler>();
                services.AddTransient<ICommandHandler, InternacaoHandler>();
                services.AddTransient<ICommandHandler, EvolucaoHandler>();
                services.AddTransient<ICommandHandler, EvolucaoEnfermagemHandler>();
                services.AddTransient<ICommandHandler, LiquidosAdministradosEnfermagemHandler>();
                services.AddTransient<ICommandHandler, LiquidosEliminadosEnfermagemHandler>();
                services.AddTransient<ICommandHandler, ProcedimentosEnfermagemHandler>();
                services.AddTransient<ICommandHandler, PainelCommandHandler>();
                services.AddTransient<ICommandHandler, DelegacaoProfissionalHandler>();

                #endregion

                #region DAOs

                services.AddTransient<IPacienteDao, PacienteDto>();
                services.AddTransient<IClassificacaoDeRiscoDto, ClassficacaoDeRiscoDto>();
                services.AddTransient<IMotivoCheckoutDto, MotivoCheckoutDto>();
                services.AddTransient<ITipoMotivoChekoutDto, TipoMotivoCheckoutDto>();
                services.AddTransient<IProfissionalDao, ProfissionalDto>();
                services.AddTransient<IServicoDao, ServicoDto>();
                services.AddTransient<IEtniaDao, EtniaDto>();
                services.AddTransient<ICidadeDao, CidadeDto>();
                services.AddTransient<IBairroDao, BairroDto>();
                services.AddTransient<ICNSDao, CNSDto>();
                services.AddTransient<IConvenioDao, ConvenioDto>();
                services.AddTransient<IEnderecoDao, EnderecoDto>();
                services.AddTransient<IEscolaridadeDao, EscolaridadeDto>();
                services.AddTransient<IEstadoCivilDao, EstadoCivilDto>();
                services.AddTransient<INaturalidadeDao, NaturalidadeDto>();
                services.AddTransient<IPlanoDao, PlanoDto>();
                services.AddTransient<IProfissaoDao, ProfissaoDto>();
                services.AddTransient<IUFDao, UFDto>();
                services.AddTransient<IClinicaDao, ClinicaDto>();
                services.AddTransient<IElementoExameDAO, ElementoExameDto>();
                services.AddTransient<ICiap2Dao, Ciap2Dto>();
                services.AddTransient<IAnamneseDao, AnamneseDto>();
                services.AddTransient<ICidDao, CidDto>();
                services.AddTransient<IHipoteseDiagnosticaDao, HipoteseDiagnosticaDto>();
                services.AddTransient<IImagemAtendimentoDao, ImagemAtendimentoDto>();
                services.AddTransient<IImagemDao, ImagemDto>();
                services.AddTransient<IUserDao, UserDao>();
                services.AddTransient<IAtestadoDao, AtestadoDto>();
                services.AddTransient<ITemplatesAtestadoDto, TemplatesAtestadoDto>();
                services.AddTransient<IUnidadeDao, UnidadeDto>();
                services.AddTransient<IRoleDao, RoleDao>();
                services.AddTransient<IServicoSocialDto, ServicoSocialDto>();
                services.AddTransient<IProcedimentoDto, ProcedimentoDto>();
                services.AddTransient<IReceitaMedicaDto, ReceitaMedicaDto>();
                services.AddTransient<IPrescricaoDto, PrescricaoDto>();
                services.AddTransient<IExameAtendimentoDto, ExameAtendimentoDto>();
                services.AddTransient<ITipoExameDto, TipoExameDto>();
                services.AddTransient<IExameDto, ExameDto>();
                services.AddTransient<IMedicamentoDto, MedicamentoDto>();
                services.AddTransient<IAdministracaoMedicamentoDto, AdministracaoMedicamentoDto>();
                services.AddTransient<IUnidadeMedicamentoDto, UnidadeMedicamentoDto>();
                services.AddTransient<IPaisDao, PaisDto>();
                services.AddTransient<IEnfermariaDao, EnfermariaDto>();
                services.AddTransient<ILeitoDao, LeitoDto>();
                services.AddTransient<IMovimentacaoLeitoDto, MovimentacaoLeitoDto>();
                services.AddTransient<IEvolucaoDto, EvolucaoDto>();
                services.AddTransient<IEvolucaoEnfermagemDto, EvolucaoEnfermagemDto>();
                services.AddTransient<ILiquidosAdministradosDto, LiquidosAdministradosDto>();
                services.AddTransient<ILiquidosEliminadosDto, LiquidosEliminadosDto>();
                services.AddTransient<IVozCommandDao, VozCommandDao>();
                services.AddTransient<ILocalAtendimentoDao, LocalAtendimentoDao>();
                services.AddTransient<IAcolhimentoDTO, AcolhimentoDto>();
                services.AddTransient<ICboDao, CboDto>();
                services.AddTransient<IAtendimentoDto, AtendimentoDto>();
                services.AddTransient<IFaturamentoDao, FaturamentoDao>();
                services.AddTransient<IDelegacaoProfissionalDao, DelegacaoProfissionalDto>();

                #endregion

                var config = new MapperConfiguration(c =>
                {
                    c.AddProfile<DomainToReadViewModel>();
                    c.AddProfile<ReadViewModelToDomain>();
                    c.AddProfile<CommandToDomainMappingProfile>();
                });
                services.AddSingleton<IMapper>(s => config.CreateMapper());

                var serializer = new JsonTextSerializer();
                services.AddTransient<ITextSerializer>(t => new JsonTextSerializer());
                MessageSender m = new MessageSender("HealthAid", "Commands", configuration.GetConnectionString("MySQLConnection"));
                services.AddSingleton<IMessageSender>(m);
                var commandBus = new CommandBus(m, serializer);
                var synchronousCommandBus = new SynchronousCommandBusDecorator(commandBus, services.BuildServiceProvider());
                services.AddSingleton<ICommandBus>(synchronousCommandBus);
                services.AddSingleton<ICommandHandlerRegistry>(synchronousCommandBus);

                var sp = services.BuildServiceProvider();
                var service = (SynchronousCommandBusDecorator)sp.GetService<ICommandHandlerRegistry>();
                foreach (var commandHandler in sp.GetServices<ICommandHandler>())
                {
                    service.Register(commandHandler);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

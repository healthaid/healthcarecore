﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NLog.Extensions.Logging;
using NLog.Web;
using Swashbuckle.AspNetCore.Swagger;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace HealthCare.WebApi
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
            env.ConfigureNLog("nlog.config");
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options => options.AddPolicy("AllowCors", builder =>
        {
            builder
            .WithOrigins("http://tellus.brazilsouth.cloudapp.azure.com/")
            //.WithOrigins("http://localhost:4456") //AllowSpecificOrigins;
            //.WithOrigins("http://localhost:4456", "http://localhost:4457") 
            //AllowMultipleOrigins;
            .AllowAnyOrigin() //AllowAllOrigins;

            //.WithMethods("GET") //AllowSpecificMethods;
            //.WithMethods("GET", "PUT") //AllowSpecificMethods;
            //.WithMethods("GET", "PUT", "POST") 
            //AllowSpecificMethods;
            .WithMethods("GET", "PUT", "POST", "DELETE", "OPTIONS")
            //AllowSpecificMethods;
            //.AllowAnyMethod() //AllowAllMethods;
            //.WithHeaders("Accept", 
            //"Content-type", "Origin", "X-Custom-Header");  
            //AllowSpecificHeaders;
            .AllowAnyHeader(); //AllowAllHeaders;
        }));

            services.AddMvc().AddJsonOptions(jsonOptions =>
            {
                jsonOptions.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;
                jsonOptions.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                jsonOptions.SerializerSettings.ContractResolver = new Newtonsoft.Json.Serialization.DefaultContractResolver();
            });

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            //services.AddAutoMapper(typeof(Startup));

            RegisterDi.Register(services, Configuration);
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Tellus Saúde", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseMiddleware<RequestLoggingMiddleware>();

            //app.UseCors(builder => builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader().AllowCredentials());
            app.UseMvc();


            loggerFactory.AddNLog();
            app.AddNLogWeb();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            app.UseCors("AllowCors");
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "V1 Docs");
            });
        }
    }


    public class RequestLoggingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<RequestLoggingMiddleware> _logger;


        public RequestLoggingMiddleware(RequestDelegate next, ILogger<RequestLoggingMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }


        public async Task Invoke(HttpContext context)
        {
            var request = context.Request;

            //get the request body and put it back for the downstream items to read
            var stream = request.Body;// currently holds the original stream                    
            var originalContent = new StreamReader(stream).ReadToEnd();
            var notModified = true;
            try
            {
                if (!string.IsNullOrWhiteSpace(originalContent))
                {
                    KeyValuePair<string, Microsoft.Extensions.Primitives.StringValues> usuarioPair = new KeyValuePair<string, Microsoft.Extensions.Primitives.StringValues>();
                    usuarioPair = context.Request.Headers.FirstOrDefault(x => x.Key == "objusuario");
                    if (usuarioPair.Value != System.Guid.Empty)
                    {
                        var usuario = string.Format(@"""CommandUsuarioId""" + ":" + @"""{0}""" + ",", usuarioPair.Value);
                        string modificado = originalContent.Insert(1, usuario);
                        //replace request stream to downstream handlers
                        var requestContent = new StringContent(modificado, System.Text.Encoding.UTF8, "application/json");
                        stream = await requestContent.ReadAsStreamAsync();//modified stream
                        notModified = false;
                    }
                }
            }
            catch
            {
                //No-op or log error
            }
            if (notModified)
            {
                //put original data back for the downstream to read
                var requestData = System.Text.Encoding.UTF8.GetBytes(originalContent);
                stream = new MemoryStream(requestData);
            }

            request.Body = stream;

            await _next.Invoke(context);
         
        }
    }
}




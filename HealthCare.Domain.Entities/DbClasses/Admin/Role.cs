﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace HealthCare.Domain.Entities.DbClasses.Admin
{
    public class Role : IdentityRole<Guid, UserRole, RoleClaim>
    {
        public Role()
        {

        }

        public Role(string roleName) : base(roleName)
        {
            this.Name = roleName;
        }
    }
}

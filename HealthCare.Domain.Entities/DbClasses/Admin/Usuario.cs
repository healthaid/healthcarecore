﻿using HealthCare.Domain.Entities.DbClasses.Atendimento;
using HealthCare.Domain.Entities.DbClasses.Cadastro;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace HealthCare.Domain.Entities.DbClasses.Admin
{
    public class Usuario : IdentityUser<Guid, UserClaim, UserRole, UserLogin>
    {
        public Usuario()
        {
            this.Assinaturas = new List<Assinatura>();
            this.Laudos = new List<Laudo>();
        }


        public string Nome { get; set; }
        public string Sobrenome { get; set; }
        public string Senha { get; set; }
        public string ConfirmacaoSenha { get; set; }
        public DateTime? DataCadastro { get; set; }
        public Profissional Profissional { get; set; }
        public string Documento { get; set; }
        public string TipoDocumento { get; set; }
        public bool PrimeiroLogin { get; set; }
        public List<Assinatura> Assinaturas { get; set; }
        public List<Laudo> Laudos { get; set; }
    }
}

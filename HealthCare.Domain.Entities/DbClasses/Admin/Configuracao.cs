﻿using HealthCare.Domain.Entities.Definitions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HealthCare.Domain.Entities.DbClasses.Admin
{
    public class Configuracao : BaseModel, IEntity
    {
        public Guid Id {get; set;}
        public int TempoSessao { get; set; }
        public int MaxTentativasLogin { get; set; }
        public int MaxTempoTrocarSenha { get; set; }
        public bool SenhaForte { get; set; }
        public int SequencialOS { get; set; }
        public string Linha1TituloRelatorio { get; set; }
        public string Linha2TituloRelatorio { get; set; }
        public string Linha3TituloRelatorio { get; set; }
        public string Linha4TituloRelatorio { get; set; }
        public byte[] LogotipoRelatorio { get; set; }
        public byte[] AssinaturaRelatorio { get; set; }
        public byte[] RowVersion { get; set; }
        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (MaxTentativasLogin > 10)
                yield return new ValidationResult("O máximo de tentativas de Login não deve ser superior a 10.");

            if (MaxTempoTrocarSenha > 6)
                yield return new ValidationResult("O máximo de tempo para troca de senha não deve ser superior a 6 meses.");
        }
    }
}

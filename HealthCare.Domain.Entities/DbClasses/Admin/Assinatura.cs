﻿using HealthCare.Domain.Entities.Definitions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace HealthCare.Domain.Entities.DbClasses.Admin
{
    public class Assinatura : BaseModel, IEntity
    {
        public Assinatura()
        {
            this.Usuario = new Usuario();
        }
        public Guid Id { get; set; }
        public byte[] Imagem { get; set; }
        public Usuario Usuario{get;set; }
        public byte[] RowVersion { get; set; }
        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            return new Collection<ValidationResult>();
        }

    }
}

﻿using HealthCare.Domain.Entities.Definitions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HealthCare.Domain.Entities.DbClasses.Admin
{
    public class Commands : BaseModel, IEntity
    {
        public string Body { get; private set; }
        public string CorrelationId { get; private set; }
        public DateTime? DeliveryDate { get; private set; }
        public byte[] RowVersion
        {
            get;
            set;
        }
        public override IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> Validate(System.ComponentModel.DataAnnotations.ValidationContext validationContext)
        {
            throw new NotImplementedException();
        }
        public Guid Id
        {
            get;
            set;
        }
        public Guid UsuarioId { get; set; }
    }
}

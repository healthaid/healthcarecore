﻿using HealthCare.Domain.Entities.Definitions;
using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace HealthCare.Domain.Entities.DbClasses.Cadastro
{
    public class ProfissionalServico : BaseModel
    {
        public Profissional Profissional { get; set; }

        public Servico Servico { get; set; }

        public Guid ProfissionalId { get; set; }

        public Guid ServicoId { get; set; }

        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            throw new NotImplementedException();
        }
    }
}

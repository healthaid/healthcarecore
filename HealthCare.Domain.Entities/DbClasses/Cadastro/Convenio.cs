﻿using HealthCare.Domain.Entities.Definitions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace HealthCare.Domain.Entities.DbClasses.Cadastro
{
    public class Convenio : BaseModel, IEntity
    {
        public Convenio()
        {
            Plano = new Plano();
            Paciente = new Paciente();
        }
        public string NumeroCarteira { get; set; }
        public string Acomocadao { get; set; }
        public DateTime Validade { get; set; }
        public Paciente Paciente { get; set; }
        public Plano Plano { get; set; }
        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            return new Collection<ValidationResult>();
        }
        public Guid Id { get; set; }
        public byte[] RowVersion { get; set; }
    }
}

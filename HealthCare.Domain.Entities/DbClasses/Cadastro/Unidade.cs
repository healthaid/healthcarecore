﻿using HealthCare.Domain.Entities.Definitions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HealthCare.Domain.Entities.DbClasses.Cadastro
{
    public class Unidade : BaseModel
    {
      
        public Guid Id { get; set; }
        public string NomeUnidade { get; set; }
        public string NomeFantasia{ get; set; }
        public string CodigoUnidade { get; set; }
        public string Sigla { get; set; }
        public string CNES { get; set; }
        public string Cnpj { get; set; }
        public DateTime DataAcreditacao { get; set; }
        public string Situacao { get; set; }
        public IEnumerable<Profissional> Profissionais { get; set; }
        public byte[] RowVersion { get; set; }
        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            return new List<ValidationResult>();
        }
        

        public Endereco Endereco { get; set; }

        //public bool IsDeleted { get; set; }
        //public bool Ativo { get; set; }
    }
}
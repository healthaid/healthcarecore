﻿using HealthCare.Domain.Entities.DbClasses.Atendimento;
using HealthCare.Domain.Entities.Definitions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HealthCare.Domain.Entities.DbClasses.Cadastro
{
    public class Servico : BaseModel
    {
        public string Descricao { get; set; }
        public Guid Id { get; set; }
        
        public List<ProfissionalServico> ProfissionalServicos { get; set; }
        public List<Acolhimento> Acolhimentos { get; set; }
        
        public bool IsDeleted { get; set; }
        
        public byte[] RowVersion { get; set; }
        
        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            return new List<ValidationResult>();
        }

    }

}

﻿using HealthCare.Domain.Entities.DbClasses.Admin;
using HealthCare.Domain.Entities.Definitions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace HealthCare.Domain.Entities.DbClasses.Cadastro
{
    public class DelegacaoProfissional : BaseModel
    {
        public Guid Id { get; set; }

        public Usuario Delegador { get; set; }
        public Usuario Delegado { get; set; }
        public DateTime DataDelegacao { get; set; }
        public DateTime? DataDestituicao { get; set; }

        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            return new Collection<ValidationResult>();
        }
    }
}
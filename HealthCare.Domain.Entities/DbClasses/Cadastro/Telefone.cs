﻿using HealthCare.Domain.Entities.Definitions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace HealthCare.Domain.Entities.DbClasses.Cadastro
{
    public class Telefone : BaseModel, IEntity
    {
        public Telefone()
        {
            TipoTelefone = new TipoTelefone();
            Paciente = new Paciente();
        }
        public Guid Id { get; set; }
        public byte[] RowVersion { get; set; }
        public string Numero { get; set; }
        public TipoTelefone TipoTelefone { get; set; }
        public Paciente Paciente { get; set; }
        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            return new Collection<ValidationResult>();
        }
    }
}

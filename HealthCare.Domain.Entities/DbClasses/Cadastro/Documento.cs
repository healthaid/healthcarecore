﻿using HealthCare.Domain.Entities.Definitions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace HealthCare.Domain.Entities.DbClasses.Cadastro
{
    public class Documento : BaseModel, IEntity
    {
        public Documento()
        {
            this.Paciente = new Paciente();
        }
        public Guid Id { get; set; }
        public byte[] RowVersion { get; set; }
        public string TipoDocumento { get; set; }
        public string Valor { get; set; }
        public Paciente Paciente { get; set; }
        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            return new Collection<ValidationResult>();
        }
    }
}

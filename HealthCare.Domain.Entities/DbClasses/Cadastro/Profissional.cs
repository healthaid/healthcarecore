﻿using HealthCare.Domain.Entities.Definitions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HealthCare.Domain.Entities.DbClasses.Cadastro
{
    public class Profissional : BaseModel
    {
        public string Nome { get; set; }
        public List<ProfissionalServico> ProfissionalServicos { get; set; }
        public Guid Id { get; set; }
        public string CPF { get; set; }
        public string CNS { get; set; }
        public string Sexo { get; set; }
        public string NomePai { get; set; }
        public string NomeMae { get; set; }
        public DateTime? Nascimento { get; set; }
        public string Naturalidade { get; set; }
        public string Nacionalidade { get; set; }
        public Unidade Unidade { get; set; }
        public SetorClinica SetorClinica { get; set; }
        public Cbo Cbo { get; set; }
        public bool IsDeleted { get; set; }
        public string CRM { get; set; }

        public byte[] RowVersion { get; set; }
        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            return new List<ValidationResult>();
        }
    }
}

﻿namespace HealthCare.Domain.Entities.DbClasses.Cadastro
{
    public enum TipoLeito
    {
        SalaAmarela = 1,
        SalaVermelha = 2,
        SemClassificacao = 3
    }

}

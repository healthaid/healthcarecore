﻿using HealthCare.Domain.Entities.Definitions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace HealthCare.Domain.Entities.DbClasses.Cadastro
{
    public class Cbo : BaseModel, IEntity
    {
        public string Codigo { get; set; }
        public string Descricao { get; set; }


        public Guid Id { get; set; }

        public byte[] RowVersion { get; set; }

        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            return new Collection<ValidationResult>();
        }
    }
}

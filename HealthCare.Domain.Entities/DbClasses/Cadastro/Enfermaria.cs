﻿using HealthCare.Domain.Entities.Definitions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace HealthCare.Domain.Entities.DbClasses.Cadastro
{
    public class Enfermaria : BaseModel
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public bool Ativo { get; set; }
        public string Local { get; set; }
        public Unidade Unidade { get; set; }
        public List<Leito> Leitos { get; set; }
        public byte[] RowVersion { get; set; }
        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            return new Collection<ValidationResult>();
        }
        
    }
}

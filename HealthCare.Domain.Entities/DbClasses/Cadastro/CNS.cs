﻿using HealthCare.Domain.Entities.Definitions;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace HealthCare.Domain.Entities.DbClasses.Cadastro
{
    public class CNS : BaseModel
    {
        public string Numero { get; set; }
        public bool Principal { get; set; }
        public bool Ativo { get; set; }
        public System.Guid Id { get; set; }
        public Paciente Paciente { get; set; }
        public byte[] RowVersion { get; set; }
        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            return new Collection<ValidationResult>();
        }
        
    }
}
﻿using HealthCare.Domain.Entities.Definitions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace HealthCare.Domain.Entities.DbClasses.Cadastro
{
    /// <summary>
    /// Classe que representa a tabela endereço
    /// </summary>
    public class Endereco : BaseModel, IEntity
    {
        /// <summary>
        /// Identificador do registro
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Cep
        /// </summary>
        public string CEP { get; set; }

        /// <summary>
        /// Nome da rua
        /// </summary>
        public string Logradouro { get; set; }

        /// <summary>
        /// Numero da casa
        /// </summary>
        public string Numero { get; set; }
    
        /// <summary>
        /// Complemento do endereço
        /// </summary>
        public string Complemento { get; set; }

        /// <summary>
        /// Data de Modificação da linha
        /// </summary>
        public byte[] RowVersion { get; set; }

        /// <summary>
        /// Bairro
        /// </summary>
        public string Bairro { get; set; }

        /// <summary>
        /// Municipio
        /// </summary>
        public string Cidade { get; set; }

        /// <summary>
        /// Estado
        /// </summary>
        public string UF { get; set; }

        /// <summary>
        /// Codigo ibge do municipio
        /// </summary>
        public string CodigoIbgeCidade { get; set; }

        /// <summary>
        /// Codigo do Logradouro (Rua, AVenida, etc...)
        /// </summary>
        public string CodigoLogradouro { get; set; }

        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            return new Collection<ValidationResult>();
        }
    }
}

﻿using HealthCare.Domain.Entities.DbClasses.Atendimento;
using HealthCare.Domain.Entities.Definitions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace HealthCare.Domain.Entities.DbClasses.Cadastro
{

    public class Leito : BaseModel
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public bool Ativo { get; set; }
        public SetorClinica Clinica { get; set; }
        public Enfermaria Enfermaria { get; set; }
        public List<MovimentacaoLeito> Movimentacoes { get; set; }
        public bool Ocupado { get; set; }
        public TipoLeito TipoLeito { get; set; }
        public byte[] RowVersion { get; set; }
        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            return new Collection<ValidationResult>();
        }
    }
}

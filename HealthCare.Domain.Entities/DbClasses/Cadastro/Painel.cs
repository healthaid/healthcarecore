﻿using HealthCare.Domain.Entities.Definitions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HealthCare.Domain.Entities.DbClasses.Cadastro
{
    public class Painel : BaseModel
    {
        public Guid Id { get; set; }

        public string NomePaciente { get; set; }

        public string LocalAtendimento { get; set; }

        public bool Atendido { get; set; }

        public string Servico { get; set; }

        public Guid UnidadeId { get; set; }

        public DateTime DataInsercao { get; set; }

        public byte[] RowVersion { get; set; }


        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            return new List<ValidationResult>();
        }
    }
}

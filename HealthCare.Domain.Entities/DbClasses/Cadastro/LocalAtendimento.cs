﻿using HealthCare.Domain.Entities.Definitions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HealthCare.Domain.Entities.DbClasses.Cadastro
{
    public class LocalAtendimento : BaseModel
    {
        public Guid Id { get; set; }

        public string Local { get; set; }

        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            return new List<ValidationResult>();
        }
    }
}

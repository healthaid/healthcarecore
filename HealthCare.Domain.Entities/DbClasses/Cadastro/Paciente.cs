﻿using HealthCare.Domain.Entities.Definitions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HealthCare.Domain.Entities.DbClasses.Cadastro
{
    /// <summary>
    /// Representa a tabela Paciente
    /// </summary>
    public class Paciente : BaseModel
    {
        /// <summary>
        /// Construtor
        /// </summary>
        public Paciente()
        {
            
        }

        /// <summary>
        /// Codigo gerado automaticamente para o paciente
        /// </summary>
        public string CodigoPaciente { get; set; }

        /// <summary>
        /// Identificador do registro
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Nome do paciente
        /// </summary>
        public string Nome { get; set; }

        /// <summary>
        /// Nome da mae do Paciente
        /// </summary>
        public string NomeMae { get; set; }

        /// <summary>
        /// Data de nascimento
        /// </summary>
        public DateTime? DataNascimento { get; set; }

        /// <summary>
        /// Sexo do paciente
        /// </summary>
        public string Sexo { get; set; }

        /// <summary>
        /// Email do paciente
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Foto do paciente
        /// </summary>
        public byte Foto { get; set; }

        /// <summary>
        /// Cpf do paciente
        /// </summary>
        public string CPF { get; set; }

        /// <summary>
        /// Rg do paciente
        /// </summary>
        public string RG { get; set; }

        /// <summary>
        /// Cartão Nacional de saúde do paciente
        /// </summary>
        public string Cns { get; set; }

        /// <summary>
        /// Etinia do paciente
        /// </summary>
        public string Etinia { get; set; }

        /// <summary>
        /// Profissão do Paciente
        /// </summary>
        public string Profissao { get; set; }

        /// <summary>
        /// Estado civil do paciente
        /// </summary>
        public string EstadoCivil { get; set; }

        /// <summary>
        /// Se o paciente está em obito ou não
        /// </summary>
        public bool Obito { get; set; }

        /// <summary>
        /// Se o paciente está ativo ou não
        /// </summary>
        public bool Ativo { get; set; }

        /// <summary>
        /// Endereco do paciente
        /// </summary>
        public Endereco Endereco { get; set; }

        /// <summary>
        /// Telefone celular do paciente
        /// </summary>
        public string TelefoneCelular { get; set; }

        /// <summary>
        /// Telefone fixo do paciente
        /// </summary>
        public string TelefoneFixo { get; set; }

        /// <summary>
        /// Telefone do trabalho
        /// </summary>
        public string TelefoneTrabalho { get; set; }

        /// <summary>
        /// Raça
        /// </summary>
        public string Raca { get; set; }

        public Unidade Unidade { get; set; }

        [NotMapped]
        public int? IdadeEmAnos
        {
            get
            {
                return CalculaIdade();
            }
        }

        [NotMapped]
        public string IdadePorExtenso
        {
            get
            {
                return CalulaIdade(this.DataNascimento);
            }
        }

        /// <summary>
        /// País de origem do paciente
        /// </summary>
        public string Nacionalidade { get; set; }

        /// <summary>
        /// Se o paciente está deletado ou não 
        /// </summary>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Sinaliza se o paciente está apenas com o cadastro de acolhimento
        /// </summary>
        public bool PendenteCadastro { get; set; }

        /// <summary>
        /// Booleano que define se o paciente é prioridade ou não
        /// </summary>
        //public bool Prioridade { get; set; }

        /// <summary>
        /// Data da modificação do registro
        /// </summary>
        public byte[] RowVersion { get; set; }

        /// <summary>
        /// Municipio de nascimento do paciente
        /// </summary>
        public Cidade Naturalidade { get; set; }

        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            return new List<ValidationResult>();
        }



        private int? CalculaIdade()
        {
            if (this.DataNascimento != null)
            {
                int idade = DateTime.Today.Year - this.DataNascimento.Value.Year;
                if (DateTime.Today < this.DataNascimento.Value.AddYears(idade)) idade--;

                return idade;
            }
            else
                return null;
        }

        private string CalulaIdade(DateTime? dataNascimento)
        {
            string idade = string.Empty;
            if (dataNascimento.HasValue)
            {
                // Calculate the age.
                var idadeCalculada = DateTime.Today.Year - dataNascimento.Value.Year;

                if (dataNascimento > DateTime.Today.AddYears(-idadeCalculada))
                    idadeCalculada--;

                if (idadeCalculada < 1)
                    idade = "Menos de 1 ano.";
                else if (idadeCalculada == 1)
                    idade = string.Format("{0} ano.", idadeCalculada.ToString());
                else
                    idade = string.Format("{0} anos.", idadeCalculada.ToString());
            }

            return idade;
        }

    }
}

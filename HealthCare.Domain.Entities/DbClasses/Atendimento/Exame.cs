﻿using HealthCare.Domain.Entities.Definitions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HealthCare.Domain.Entities.DbClasses.Atendimento
{
    public class Exame : BaseModel
    {
        public Guid Id { get; set; }
        public string Descricao { get; set; }
        public string Sexo { get; set; }
        //[NotMapped]
        public string Procedimento { get; set; }
        //[NotMapped]
        public string Classificacao { get; set; }
        //public string TipoExame { get; set; }
        public Exame ExamePai { get; set; }
        public TipoExame TipoExame { get; set; }

        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            return new List<ValidationResult>();
        }
    }
}

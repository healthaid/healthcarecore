﻿using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Domain.Entities.Definitions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace HealthCare.Domain.Entities.DbClasses.Atendimento
{
    public class Prescricao : BaseModel, IEntity
    {
        public Guid Id { get; set; }
        public byte[] RowVersion { get; set; }
        public string Dose { get; set; }
        public string Intervalo { get; set; }
        public string Duracao { get; set; }
        public string Unidade { get; set; }
        public string ViaAdministracao { get; set; }
        public string Medicamento { get; set; }
        public Atendimento Atendimento { get; set; }
        public DateTime EnvioEnfermagem { get; set; }
        public string ObservacaoMedico { get; set; }
        public string StatusEnfermagem { get; set; }
        public DateTime? RetornoEnfermagem { get; set; }        
        public string ObservacaoEnfermagem { get; set; }
        public Profissional Enfermeiro { get; set; }
        public Profissional Medico { get; set; }
        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            return new Collection<ValidationResult>();
        }
    }
}

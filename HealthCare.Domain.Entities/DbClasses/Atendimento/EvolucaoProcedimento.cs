﻿using HealthCare.Domain.Entities.Definitions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HealthCare.Domain.Entities.DbClasses.Atendimento
{
    public class EvolucaoProcedimento : BaseModel
    {
        public Evolucao Evolucao { get; set; }

        public Procedimento Procedimento { get; set; }

        public Guid EvolucaoId { get; set; }

        public Guid ProcedimentoId { get; set; }

        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            throw new NotImplementedException();
        }
    }
}

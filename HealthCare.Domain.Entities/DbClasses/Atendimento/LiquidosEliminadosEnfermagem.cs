﻿using HealthCare.Domain.Entities.Definitions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HealthCare.Domain.Entities.DbClasses.Atendimento
{
    public class LiquidosEliminadosEnfermagem : BaseModel, IEntity
    {
        public Guid Id { get; set; }
        public byte[] RowVersion { get; set; }

        public EvolucaoEnfermagem evolucaoEnfermagem { get; set; }
        public LiquidosEliminados LiquidosEliminados { get; set; }

        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            return new List<ValidationResult>();
        }
    }
}

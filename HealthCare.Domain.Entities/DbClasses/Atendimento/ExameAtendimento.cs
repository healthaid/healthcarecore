﻿using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Domain.Entities.Definitions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HealthCare.Domain.Entities.DbClasses.Atendimento
{
    public class ExameAtendimento : BaseModel
    {
        public Guid Id { get; set; }
        public Atendimento Atendimento { get; set; }
        public string Exame { get; set; }
        public string CodigoProcedimento { get; set; }
        public string Procedimento { get; set; }
        public string ObservacaoMedico { get; set; }
        public DateTime DataSolicitacao { get; set; }
        public DateTime? DataExecucao { get; set; }
        public string ObservacaoExecutor { get; set; }
        public Profissional Executor { get; set; }
        public Profissional Medico { get; set; }
        public bool Realizado { get; set; }
        public string DefinicaoExame { get; set; }
        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            return new List<ValidationResult>();
        }
        
    }
}

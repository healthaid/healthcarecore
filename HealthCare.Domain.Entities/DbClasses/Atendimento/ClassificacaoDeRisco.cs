﻿using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Domain.Entities.Definitions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HealthCare.Domain.Entities.DbClasses.Atendimento
{
    public class ClassificacaoDeRisco : BaseModel, IEntity
    {
        public Guid Id { get; set; }
        public Acolhimento Acolhimento { get; set; }
        public SinaisVitais SinaisVitais { get; set; }
        public string NivelDeConsciencia { get; set; }
        public string Queixa { get; set; }
        public string CausaExterna { get; set; }
        public string DoencasPreexistentes { get; set; }
        public string MedicamentosEmUso { get; set; }
        public string Alergia { get; set; }
        public string Observacao { get; set; }
        public string EspecialidadeMedica { get; set; }
        public Risco Risco { get; set; }
        public Profissional Profissional { get; set; }
        public DateTime? DataClassificacao { get; set; }
        public byte[] RowVersion { get; set; }
        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            return new List<ValidationResult>();
        }
    }
}

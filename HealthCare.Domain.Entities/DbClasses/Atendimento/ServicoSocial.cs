﻿using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Domain.Entities.Definitions;

namespace HealthCare.Domain.Entities.DbClasses.Atendimento
{
    public class ServicoSocial : Atendimento, IEntity
    {
        public string Historico { get; set; }
        public string Intervencao { get; set; }
        public Profissional Profissional { get; set; }

    }
}

﻿using HealthCare.Domain.Entities.Definitions;

namespace HealthCare.Domain.Entities.DbClasses.Atendimento
{
    public class Ciap2_Cid10 : BaseModel, IEntity
    {
        public System.Guid Id { get; set; }

        public string CiapCodigo { get; set; }
        public string Cid { get; set; }
        public string CidFrequente { get; set; }

        public byte[] RowVersion { get; set; }

        public override System.Collections.Generic.IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> Validate(System.ComponentModel.DataAnnotations.ValidationContext validationContext)
        {
            throw new System.NotImplementedException();
        }
    }
}

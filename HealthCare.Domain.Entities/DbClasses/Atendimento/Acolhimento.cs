﻿using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Domain.Entities.Definitions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HealthCare.Domain.Entities.DbClasses.Atendimento
{
    public class Acolhimento : BaseModel, IEntity
    {

        public Acolhimento()
        {
            this.CodigoBoletim = "0";
        }
        public Guid Id { get; set; }
        public byte[] RowVersion { get; set; }
        public string CodigoBoletim { get; set; }
        public DateTime DataHora { get; set; }
        public Paciente Paciente { get; set; }
        public Profissional Profissional { get; set; }
        public Servico Servico { get; set; }
        public List<ClassificacaoDeRisco> Classificacao { get; set; }
        public List<Atendimento> Atendimento { get; set; }
        public int Status { get; set; }
        public bool Prioridade { get; set; }

        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            return new List<ValidationResult>();
        }
    }
}
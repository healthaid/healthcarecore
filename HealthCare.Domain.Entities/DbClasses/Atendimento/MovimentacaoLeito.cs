﻿using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Domain.Entities.Definitions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace HealthCare.Domain.Entities.DbClasses.Atendimento
{
    public class MovimentacaoLeito : BaseModel
    {
        public Guid Id { get; set; }
        public Clinica ClinicaAtual1 { get; set; }
        public Servico Clinica { get; set; }
        public Leito LeitoAtual { get; set; }
        public DateTime DataMovimentacao { get; set; }
        public Internacao Internacao { get; set; }
        public MovimentacaoLeito UltimaMovimentacao { get; set; }

        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            return new Collection<ValidationResult>();
        }

        
    }
}

﻿using HealthCare.Domain.Entities.Definitions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace HealthCare.Domain.Entities.DbClasses.Atendimento
{
    public class AtendimentoProcedimento : BaseModel
    {
        public Guid Id { get; set; }
        public Guid AtendimentoId { get; set; }
        public string CodigoProcedimento { get; set; }
        public string NomeProcedimento { get; set; }
        public string VLSA { get; set; }
        public int? QtDiasPermanencia { get; set; }
        public string VLSP { get; set; }

        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            return new Collection<ValidationResult>();
        }
    }
}

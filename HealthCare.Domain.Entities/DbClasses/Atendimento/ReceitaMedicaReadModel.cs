﻿using System;

namespace HealthCare.Domain.Entities.DbClasses.Atendimento
{
    public class ReceitaMedicaReadModel
    {
        public string Dose { get; set; }
        public string Intervalo { get; set; }
        public string Duracao { get; set; }
    }
}

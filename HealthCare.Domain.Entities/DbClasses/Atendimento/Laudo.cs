﻿using HealthCare.Domain.Entities.DbClasses.Admin;
using HealthCare.Domain.Entities.Definitions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;


namespace HealthCare.Domain.Entities.DbClasses.Atendimento
{
    public class Laudo : BaseModel, IEntity
    {
        public Laudo()
        {
            Exame = new ResultadoExame();
            Usuario = new Usuario();
        }
        public Guid Id { get; set; }
        public byte[] RowVersion { get; set; }
        public ResultadoExame Exame { get; set; }
        public Usuario Usuario { get; set; }
        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            return new Collection<ValidationResult>();
        }
    }
}

﻿using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Domain.Entities.Definitions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace HealthCare.Domain.Entities.DbClasses.Atendimento
{
    public class Atendimento : BaseModel, IEntity
    {

        public Atendimento()
        {
            this.Procedimentos = new List<AtendimentoProcedimento>();
        }

        public Guid Id { get; set; }
        public byte[] RowVersion { get; set; }
        public DateTime InicioAtendimento { get; set; }
        public DateTime? FimAtendimento { get; set; }
        public string ExameFisico { get; set; }
        public Checkout Checkout { get; set; }
        public Profissional Profissional { get; set; }
        public List<ResultadoExame> Exames { get; set; }
        public List<Anamnese> Anamnese { get; set; }
        public List<HipoteseDiagnostica> HipoteseDiagnostica { get; set; }
        public List<ImagemAtendimento> ImagemAtendimento { get; set; }
        public List<Atestados> Atestado { get; set; }
        public List<ReceitaMedica> Receita { get; set; }
        public Acolhimento Acolhimento { get; set; }
        public MotivoCheckout MotivoCheckout { get; set; }
        public List<AtendimentoProcedimento> Procedimentos { get; set; }
        public List<Internacao> Internacoes { get; set; }
        public CID PrimeiroDiagnostico { get; set; }
        public CID SegundoDiagnostico { get; set; }

        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            return new Collection<ValidationResult>();
        }
    }
}
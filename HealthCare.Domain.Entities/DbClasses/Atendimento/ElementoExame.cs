﻿using HealthCare.Domain.Entities.Definitions;
using System;

namespace HealthCare.Domain.Entities.DbClasses.Atendimento
{
    public class ElementoExame : BaseModel, IEntity
    {
        public Guid Id { get; set; }
        public string Descricao { get; set; }
        public Exame Exame { get; set; }
        public override System.Collections.Generic.IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> Validate(System.ComponentModel.DataAnnotations.ValidationContext validationContext)
        {
            throw new NotImplementedException();
        }
        public byte[] RowVersion { get; set; }
    }
}
﻿using HealthCare.Domain.Entities.Definitions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace HealthCare.Domain.Entities.DbClasses.Atendimento
{
    /// <summary>
    /// Classe que representa a tabela checkout do atendimento
    /// </summary>
    public class Checkout : BaseModel, IEntity
    {
        /// <summary>
        /// Identificador do registro
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Primeiro diagnostico
        /// </summary>
        public string DiagnosticoUm { get; set; }

        /// <summary>
        /// Segundo diagnostico
        /// </summary>
        public string DiagnosticoDois { get; set; }


        /// <summary>
        /// O motivo do checkout
        /// </summary>
        public MotivoCheckout MotivoCheckout { get; set; }

        /// <summary>
        /// Observação
        /// </summary>
        public string Observacao { get; set; }

        /// <summary>
        /// Timestamp do registro
        /// </summary>
        public byte[] RowVersion{ get; set; }

        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            return new Collection<ValidationResult>();
        }
    }
}

﻿using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Domain.Entities.Definitions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace HealthCare.Domain.Entities.DbClasses.Atendimento
{
    public class ResultadoExame : BaseModel, IEntity
    {
        public ResultadoExame()
        {
            Elementos = new List<ResultadoExameElemento>();
        }
        public Guid Id { get; set; }
        public byte[] RowVersion { get; set; }
        public List<Laudo> Laudo { get; set; }
        public List<ResultadoExameElemento> Elementos { get; set; }
        public Paciente Paciente { get; set; }
        public Atendimento Atendimento { get; set; }
        public Exame Exame { get; set; }
        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            return new Collection<ValidationResult>();
        }
    }
}

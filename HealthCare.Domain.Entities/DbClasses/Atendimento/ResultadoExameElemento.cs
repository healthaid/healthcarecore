﻿using HealthCare.Domain.Entities.Definitions;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace HealthCare.Domain.Entities.DbClasses.Atendimento
{
    public class ResultadoExameElemento : BaseModel, IEntity
    {
        public Guid Id { get; set; }
        public string Descricao { get; set; }
        public ResultadoExame ResultadoExame { get; set; }
        public ElementoExame ElementoExame { get; set; }
        public override System.Collections.Generic.IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> Validate(System.ComponentModel.DataAnnotations.ValidationContext validationContext)
        {
            return new Collection<ValidationResult>();
        }
        public byte[] RowVersion { get; set; }
    }
}

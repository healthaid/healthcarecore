﻿using HealthCare.Domain.Entities.Definitions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace HealthCare.Domain.Entities.DbClasses.Atendimento
{
    public class ProcedimentosEnfermagem : BaseModel, IEntity
    {
        public Guid Id { get; set; }
        public byte[] RowVersion { get; set; }
        public Procedimento Procedimento {get;set; }
        public int Quantidade { get; set; }
        public string Observacao { get; set; }
        public EvolucaoEnfermagem Evolucao { get; set; }

        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            return new List<ValidationResult>();
        }
    }
}

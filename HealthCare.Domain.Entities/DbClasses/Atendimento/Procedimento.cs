﻿using HealthCare.Domain.Entities.Definitions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace HealthCare.Domain.Entities.DbClasses.Atendimento
{
    public class Procedimento : BaseModel, IEntity
    {
        public Guid Id { get; set; }
        public string CO_PROCEDIMENTO { get; set; }
        public string NO_PROCEDIMENTO { get; set; }
        public string TP_COMPLEXIDADE { get; set; }
        public string TP_SEXO { get; set; }
        public int QT_MAXIMA_EXECUCAO { get; set; }
        public int QT_DIAS_PERMANENCIA { get; set; }
        public int QT_PONTOS { get; set; }
        public int VL_IDADE_MINIMA { get; set; }
        public int VL_IDADE_MAXIMA { get; set; }
        public decimal VL_SH { get; set; }
        public decimal VL_SA { get; set; }
        public decimal VL_SP { get; set; }
        public string CO_FINANCIAMENTO { get; set; }
        public string CO_RUBRICA { get; set; }
        public string DT_COMPETENCIA { get; set; }
        public string QT_TEMPO_PERMANENCIA { get; set; }

        public List<ProcedimentoCid> ProcedimentosCids { get; set; }
        public List<CID> Cid { get; set; }
        public List<EvolucaoProcedimento> EvolucaoProcedimentos { get; set; }

        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            return new Collection<ValidationResult>();
        }

        public byte[] RowVersion { get; set; }

    }
}

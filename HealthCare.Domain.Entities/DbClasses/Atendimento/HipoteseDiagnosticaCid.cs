﻿using HealthCare.Domain.Entities.Definitions;
using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace HealthCare.Domain.Entities.DbClasses.Atendimento
{
    public class HipoteseDiagnosticaCid : BaseModel
    {
        public Guid HipoteseDiagnosticaId { get; set; }

        public Guid CidId { get; set; }

        public HipoteseDiagnostica HipoteseDiagnostica { get; set; }

        public CID Cid { get; set; }

        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            throw new NotImplementedException();
        }
    }
}

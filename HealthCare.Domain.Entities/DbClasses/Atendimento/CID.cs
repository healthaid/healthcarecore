﻿using HealthCare.Domain.Entities.Definitions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HealthCare.Domain.Entities.DbClasses.Atendimento
{
    public class CID : BaseModel, IEntity
    {
        public Guid Id { get; set; }
        public string Codigo { get; set; }
        public string Descricao { get; set; }
        public string Agravo { get; set; }
        public string Sexo { get; set; }
        public string Estadio { get; set; }
        public string CamposIrradiados { get; set; }

        public List<ProcedimentoCid> ProcedimentosCids { get; set; }
        
        [NotMapped]
        public string CodigoDescricao
        {
            get
            {
                return Codigo + " - " + Descricao;
            }
        }

        public virtual List<HipoteseDiagnosticaCid> HipoteseDiagnosticaCids { get; set; }
        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            return new Collection<ValidationResult>();
        }
        public byte[] RowVersion { get; set; }
    }
}
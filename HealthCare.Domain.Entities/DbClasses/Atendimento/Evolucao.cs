﻿using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Domain.Entities.Definitions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace HealthCare.Domain.Entities.DbClasses.Atendimento
{
    public class Evolucao : BaseModel, IEntity
    {
        public Guid Id { get; set; }
        public byte[] RowVersion { get; set; }
        public DateTime DataHoraEvolucao { get; set; }
        public string Descricao { get; set; }
        public virtual List<EvolucaoProcedimento> EvolucaoProcedimentos { get; set; }
        public CID PrimeiroDiagnostico { get; set; }
        public CID SegundoDiagnostico { get; set; }
        public int Quantidade { get; set; }
        public Atendimento Atemdimento { get; set; }
        public Profissional Medico { get; set; }
        public int EvolucaoTipo { get; set; }

        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            return new Collection<ValidationResult>();
        }
    }
}

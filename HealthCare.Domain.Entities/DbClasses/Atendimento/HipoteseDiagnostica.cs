﻿using HealthCare.Domain.Entities.Definitions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace HealthCare.Domain.Entities.DbClasses.Atendimento
{
    public class HipoteseDiagnostica : BaseModel, IEntity
    {
        public Guid Id { get; set; }
        public List<HipoteseDiagnosticaCid> HipoteseDiagnosticaCids { get; set; }
        public Atendimento Atendimento { get; set; }
        public string Observacao { get; set; }
        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            return new Collection<ValidationResult>();
        }
        public byte[] RowVersion { get; set; }  
    }
}

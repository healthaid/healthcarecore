﻿using HealthCare.Domain.Entities.Definitions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HealthCare.Domain.Entities.DbClasses.Atendimento
{
    public class SinaisVitais : BaseModel, IEntity
    {
        public Guid Id { get; set; }
        public string Peso { get; set; }
        public string Pressao { get; set; }
        public string Pulso { get; set; }
        public string FrequenciaRespiratoria { get; set; }
        public string Temperatura { get; set; }
        public string Altura { get; set; }
        public string GlicemiaCapilar { get; set; }


        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            return new List<ValidationResult>();
        }

        public byte[] RowVersion { get; set; }
    }
}

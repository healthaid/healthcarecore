﻿using HealthCare.Domain.Entities.Definitions;
using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace HealthCare.Domain.Entities.DbClasses.Atendimento
{
    public class ProcedimentoCid : BaseModel
    {
        public Guid ProcedimentoId { get; set; }

        public Guid CidId { get; set; }

        public Procedimento Procedimento { get; set; }

        public CID Cid { get; set; }

        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            throw new NotImplementedException();
        }
    }
}

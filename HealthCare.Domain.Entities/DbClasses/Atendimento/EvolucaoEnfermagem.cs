﻿using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Domain.Entities.Definitions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HealthCare.Domain.Entities.DbClasses.Atendimento
{
    public class EvolucaoEnfermagem : BaseModel, IEntity
    {
        public Guid Id { get; set; }
        public byte[] RowVersion { get; set; }

        public MovimentacaoLeito MovimentacaoLeito { get; set; }
        public string NumAtendimento { get; set; }
        public string Evolucao { get; set; }
        public string ObservacaoSinaisVitais { get; set; }
        public string Peso { get; set; }
        public string Pressao { get; set; }
        public string Pulso { get; set; }
        public string FrequenciaRespitaroria { get; set; }
        public string Temperatura { get; set; }
        public List<LiquidosAdministradosEnfermagem> LiquidosAdministrados { get; set; }
        public List<LiquidosEliminadosEnfermagem> LiquidosEliminados { get; set; }
        public List<ProcedimentosEnfermagem> ProcedimentosEnfermagem { get; set; }
        public Profissional Profissional { get; set; }
        public DateTime HoraEvolucao { get; set; }

        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            return new List<ValidationResult>();
        }
    }
}

﻿using HealthCare.Domain.Entities.Definitions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace HealthCare.Domain.Entities.DbClasses.Atendimento
{
    public class Anamnese : BaseModel, IEntity
    {
        public Anamnese()
        {
            Atendimento = new Atendimento();
        }
        public Guid Id { get; set; }
        public string QueixaPrincipal { get; set; }
        public Atendimento Atendimento { get; set; }

        public string Descricao { get; set; }

        public Ciap2 Ciap2 { get; set; }
        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            return new Collection<ValidationResult>();
        }
        public byte[] RowVersion { get; set; }  
    }
}

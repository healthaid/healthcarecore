﻿using HealthCare.Domain.Definitions;
using HealthCare.Domain.Entities.Cadastro;
using HealthCare.Infra.CQRS.Messaging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HealthCare.Domain.Entities.Agenda
{
    public class Agenda : BaseModel, IEventPublisher
    {

        public Agenda()
        {
            AgendaEvento = new List<AgendaEvento>();
            HoraInicio = new DateTime(1753, 1, 1);
            HoraFim = new DateTime(1753, 1, 1);
        }
        public string Nome { get; set; }
        public DateTime HoraInicio { get; set; }
        public DateTime HoraFim { get; set; }
        public byte[] RowVersion { get; set; }
        public bool IsDeleted { get; set; }
        public Guid ProfissionalId { get; set; }
        public List<AgendaEvento> AgendaEvento { get; set; }
        public Profissional Profissional { get; set; }
        public Unidade Unidade { get; set; }
        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            return new List<ValidationResult>();
        }
        public Guid Id
        {
            get;
            set;
        }
        public IEnumerable<IEvent> Events
        {
            get
            {
                return null;
            }
        }
    }
}

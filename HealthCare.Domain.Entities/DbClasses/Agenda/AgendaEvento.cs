﻿using HealthCare.Domain.Definitions;
using HealthCare.Domain.Entities.Cadastro;
using HealthCare.Infra.CQRS.Messaging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HealthCare.Domain.Entities.DbClasses.Agenda
{
    public class AgendaEvento : BaseModel, IEntity
    {
        public Agenda.Agenda Agenda { get; set; }
        public Paciente Paciente { get; set; }
        public Servico Servico { get; set; }
        public DateTime HoraInicio { get; set; }
        public DateTime HoraFim { get; set; }
        public Guid Id
        {
            get;
            set;
        }
        public byte[] RowVersion
        {
            get;
            set;
        }
        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            return new List<ValidationResult>();
        }
    }
}

﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HealthCare.Domain.Entities.Definitions
{
    public abstract class BaseModel : IValidatableObject
    {

       public abstract IEnumerable<ValidationResult> Validate(ValidationContext validationContext);
    }
}

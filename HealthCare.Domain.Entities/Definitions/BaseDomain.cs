﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Domain.Entities.Definitions
{
    public abstract class BaseDomain : IValidatableObject
    {
        public Guid Id { get; set; }
        public string Descricao { get; set; }
        public bool Ativo { get; set; }
        public abstract IEnumerable<ValidationResult> Validate(ValidationContext validationContext);
    }
}

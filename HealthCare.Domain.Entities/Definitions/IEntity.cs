﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Domain.Entities.Definitions
{
    public interface IEntity
    {
        Guid Id { get; set; }

        byte[] RowVersion { get; set; }
    }
}

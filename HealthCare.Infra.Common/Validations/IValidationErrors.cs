﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealtCare.Infra.Common.Validations
{
    public interface IValidationErrors
    {
        List<string> Errors { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealtCare.Infra.Common.Validations
{
    public class GeneralErrors : ValidationErrors
    {
        public GeneralErrors(IEnumerable<string> errors)
            : base()
        {
            foreach (var err in errors)
            {
                Errors.Add(err);
            }
        }

        public GeneralErrors(string error)
        {
            Errors = new List<string>();
            Errors.Add(error);
        }
    }
}

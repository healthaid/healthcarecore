﻿using System;
using System.Collections.Generic;

namespace HealtCare.Infra.Common.Validations
{
    public class ValidationErrors : Exception, IValidationErrors
    {
        public List<string> Errors { get; set; }
        public ValidationErrors()
        {
            Errors = new List<string>();
        }

        public ValidationErrors(string error)
            : this()
        {
            Errors.Add(error);
        }
    }
}

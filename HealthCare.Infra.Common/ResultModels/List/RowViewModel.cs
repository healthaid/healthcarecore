﻿using System.Collections.Generic;

namespace HealthCare.Infra.Common.ResultModels.List
{
    /// <summary>
    /// Modelo da linha que será renderizada na tela
    /// </summary>
    public class RowViewModel
    {
        /// <summary>
        /// Nome da propriedade
        /// </summary>
        public Dictionary<string,object> Line { get; set; }

        /// <summary>
        /// Permite editar a linha
        /// </summary>
        public bool AllowEdit { get; set; }

        /// <summary>
        /// Permite excluir a linha
        /// </summary>
        public bool AllowDelete { get; set; }
    }
}

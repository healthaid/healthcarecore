﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.Common.ResultModels.List
{
    /// <summary>
    /// Model para a classe de colunas que estarão renderizadas na tabela
    /// </summary>
    public class ColumnViewModel
    {
        /// <summary>
        /// Se a coluna será visivel ou não 
        /// </summary>
        public bool AllowVisible { get; set; }

        /// <summary>
        /// Qual será o nome inteiro da coluna
        /// </summary>
        public string CustomData { get; set; }

        /// <summary>
        /// Nome da coluna 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Nome que será mostrado na tabela no html
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// Tamanho da coluna na tabela
        /// </summary>
        public string ColumnWidth { get; set; }

        /// <summary>
        /// Tipo da coluna Ex: string, int
        /// </summary>
        public string Type { get; set; }
    }
}

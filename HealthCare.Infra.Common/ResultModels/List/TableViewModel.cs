﻿using System.Collections.Generic;

namespace HealthCare.Infra.Common.ResultModels.List
{
    /// <summary>
    /// Modelo da tabela que será renderizada na tela
    /// </summary>
    public class TableViewModel
    {
        /// <summary>
        /// Colunas da tabela
        /// </summary>
        public List<ColumnViewModel> Columns { get; set; }

        /// <summary>
        /// Linhas da tabela
        /// </summary>
        public List<dynamic> Rows { get; set; }

        /// <summary>
        /// Propriedade que permite executar ações no registro
        /// </summary>
        public bool AllowActions { get; set; }

        /// <summary>
        /// Construtor da classe
        /// </summary>
        public TableViewModel()
        {  
            Columns = new List<ColumnViewModel>();
            Rows = new List<dynamic>(); 
            AllowActions = true;   
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace HealthCare.Infra.Common.Result
{
    public class ListResult
    {
        public List<dynamic> Rows { get; set; }
        public List<dynamic> Columns { get; set; }

        public static ListResult GetListResult<T>(List<T> source, string[] columns) where T : class
        {
            List<dynamic> propertyRows = new List<dynamic>();
            List<dynamic> localRows = new List<dynamic>();
            List<dynamic> localColumns = new List<dynamic>();
            if (source != null && source.Count > 0)
            {
                var type = source.FirstOrDefault().GetType();
                var properties = type.GetProperties();

                var selectedColumns = (from a in properties
                                       from b in columns
                                       where a.Name == b
                                       select new
                                       {
                                           CustomData = type.Name + a.Name,
                                           PropertyName = a.Name,
                                           Type = a.PropertyType.Name
                                       }).ToList();

                foreach (var item in selectedColumns)
                    localColumns.Add(item);


                foreach (var item in source)
                {
                    dynamic row = new Dictionary<string, object>();
                    foreach (var propertie in item.GetType().GetProperties())
                    {
                        var seletedPropertie = selectedColumns.Where(x => x.PropertyName == propertie.Name).FirstOrDefault();
                        if (seletedPropertie != null)
                        {
                            if (seletedPropertie.Type == typeof(DateTime).Name)
                                row[seletedPropertie.CustomData] = Convert.ToDateTime(item.GetType().GetProperty(seletedPropertie.PropertyName).GetValue(item)).ToString("dd/MM/yyyy");
                            else
                                if (seletedPropertie.Type == typeof(DateTime?).Name)
                            {
                                var data = item.GetType().GetProperty(seletedPropertie.PropertyName).GetValue(item);
                                if(data != null)
                                {
                                    row[seletedPropertie.CustomData] = Convert.ToDateTime(data).ToString("dd/MM/yyyy");
                                }
                                else
                                    row[seletedPropertie.CustomData] = string.Empty;
                            }
                            else
                                row[seletedPropertie.CustomData] = item.GetType().GetProperty(seletedPropertie.PropertyName).GetValue(item);
                        }
                    }

                    localRows.Add(row);
                }

                return new ListResult() { Columns = localColumns, Rows = localRows };
            }
            else
                return null;
        }
    }
}

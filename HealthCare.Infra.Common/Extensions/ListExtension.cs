﻿using HealthCare.Infra.Common.Result;
using HealthCare.Infra.Common.ResultModels.List;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.Common.Extensions
{
    public static class ListExtension
    {
        public static TableViewModel ToListResult(this ListResult source)
        {
            TableViewModel table = new TableViewModel();
            List<ColumnViewModel> columns = new List<ColumnViewModel>();
            List<RowViewModel> rows = new List<RowViewModel>();

            if (source != null)
            {
                foreach (var item in source.Columns)
                {
                    ColumnViewModel col = new ColumnViewModel();
                    col.AllowVisible = true;
                    col.ColumnWidth = "20px";
                    col.CustomData = item.CustomData;
                    col.Name = item.PropertyName;
                    col.Type = item.Type;
                    columns.Add(col);
                }

                table.Columns = columns;
                table.Rows = source.Rows;
            }
            return table;
        }

        public static IEnumerable<TSource> DistinctBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            HashSet<TKey> seenKeys = new HashSet<TKey>();
            foreach (TSource element in source)
            {
                if (seenKeys.Add(keySelector(element)))
                {
                    yield return element;
                }
            }
        }
    }
}

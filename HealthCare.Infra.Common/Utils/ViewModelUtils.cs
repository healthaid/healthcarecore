﻿using AutoMapper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace HealtCare.Infra.Common.Utils
{
    public static class ViewModelUtils
    {
        public static IMapper mapper;

        public static TViewModel ToViewModel<TModel, TViewModel>(TModel model)
        {
            return mapper.Map<TModel, TViewModel>(model);
        }

        public static IEnumerable<TViewModel> ToListViewModel<TModel, TViewModel>(IEnumerable<TModel> model)
        {
            foreach (var item in model)
                yield return mapper.Map<TModel, TViewModel>(item);
        }

        public static IEnumerable ToListViewModel<TModel>(IEnumerable<TModel> source)
        {
            Type destinationType = (from x in mapper.ConfigurationProvider.GetAllTypeMaps()
                                    where x.SourceType == typeof(TModel)
                                    select x.DestinationType).FirstOrDefault();

            foreach (var item in source)
            {
                if (destinationType != null)
                    yield return mapper.Map(item, typeof(TModel), destinationType);
            }
        }

        public static IEnumerable ToListViewModel<TModel>(IQueryable source)
        {
            Type destinationType = (from x in mapper.ConfigurationProvider.GetAllTypeMaps()
                                    where x.SourceType == typeof(TModel)
                                    select x.DestinationType).FirstOrDefault();

            foreach (var item in source)
            {
                if (destinationType != null)
                {
                    if (item.GetType().GetTypeInfo().IsGenericType)
                    {
                        var genericType = item.GetType().GetGenericArguments().FirstOrDefault();

                        if (genericType == typeof(TModel))
                        {
                            foreach (var genericItem in item as IList<TModel>)
                                yield return mapper.Map(genericItem, typeof(TModel), destinationType);
                        }
                    }
                    else
                        yield return mapper.Map(item, typeof(TModel), destinationType);
                }
            }
        }

        public static TModel ToModel<TViewModel, TModel>(TViewModel viewModel)
        {
            return mapper.Map<TViewModel, TModel>(viewModel);
        }

        public static dynamic ToModel<TViewModel>(TViewModel viewModel)
        {
            Type destinationType = (from x in mapper.ConfigurationProvider.GetAllTypeMaps()
                                    where x.SourceType == viewModel.GetType()
                                    select x.DestinationType).FirstOrDefault();

            return mapper.Map(viewModel, typeof(TViewModel), destinationType);
        }

        public static dynamic ToViewModel<TModel>(TModel model)
        {
            Type destinationType = (from x in mapper.ConfigurationProvider.GetAllTypeMaps()
                                    where x.SourceType == model.GetType()
                                    select x.DestinationType).FirstOrDefault();

            return mapper.Map(model, typeof(TModel), destinationType);
        }

        public static List<string> GetUnmapped<TViewModel>(TViewModel viewModel)
        {
            Type destinationType = (from x in mapper.ConfigurationProvider.GetAllTypeMaps()
                                    where x.SourceType == viewModel.GetType()
                                    select x.DestinationType).FirstOrDefault();

            var existingMaps = mapper.ConfigurationProvider.GetAllTypeMaps().First(x => x.SourceType.Equals(viewModel.GetType())
                && x.DestinationType.Equals(destinationType));

            var mappedProps = existingMaps.GetPropertyMaps().Select(x => x.SourceMember.Name.ToString());
            var orgProps = viewModel.GetType().GetProperties().Select(x => x.Name.ToString());

            return orgProps.Except(mappedProps).ToList();
        }
    }
}

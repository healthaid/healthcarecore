﻿using HealthCare.Domain.Entities.DbClasses.Admin;
using HealthCare.Domain.Entities.DbClasses.Atendimento;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace HealthCare.Infra.Data.Context.EntityConf.Admin
{
    public class ModuloAdminConfiguration
    {
        public static void Config(ModelBuilder model)
        {
            model.Entity<Usuario>().HasKey(u => u.Id);
            model.Entity<Usuario>().Property(u => u.Id).ValueGeneratedOnAdd();
            model.Entity<UserRole>().HasKey(r => new { r.RoleId, r.UserId });
            model.Entity<UserLogin>().HasKey(u => new { u.UserId, u.LoginProvider, u.ProviderKey });
            model.Entity<UserLogin>().Property(p => p.LoginProvider).IsUnicode(false);
            model.Entity<UserLogin>().Property(p => p.LoginProvider).HasMaxLength(196);
            model.Entity<UserLogin>().Property(p => p.ProviderKey).IsUnicode(false);
            model.Entity<UserLogin>().Property(p => p.ProviderKey).HasMaxLength(196);
            model.Entity<Usuario>().Property(p => p.UserName).IsUnicode(false);
            model.Entity<Usuario>().Property(p => p.Email).IsUnicode(false);
            model.Entity<Usuario>().Property(p => p.NormalizedUserName).IsUnicode(false);
            model.Entity<Usuario>().Property(p => p.NormalizedEmail).IsUnicode(false);
            model.Entity<Usuario>().Property(p => p.NormalizedUserName).HasMaxLength(250);
            model.Entity<Usuario>().Property(p => p.NormalizedEmail).HasMaxLength(250);
            model.Entity<Usuario>().Property(r => r.UserName).HasMaxLength(196);
            model.Entity<Usuario>().Property(r => r.Email).HasMaxLength(250);
            model.Entity<UserToken>().Property(p => p.LoginProvider).IsUnicode(false);
            model.Entity<UserToken>().Property(p => p.Name).IsUnicode(false);
            model.Entity<UserToken>().Property(r => r.LoginProvider).HasMaxLength(196);
            model.Entity<UserToken>().Property(r => r.Name).HasMaxLength(250);
            model.Entity<Role>().Property(r => r.NormalizedName).IsUnicode(false);
            model.Entity<Role>().Property(r => r.NormalizedName).HasMaxLength(250);
        }
    }
}

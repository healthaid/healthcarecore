﻿using HealthCare.Domain.Entities.DbClasses.Admin;
using HealthCare.Domain.Entities.DbClasses.Atendimento;
using HealthCare.Domain.Entities.DbClasses.Cadastro;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace HealthCare.Infra.Data.Context.EntityConf
{
    public static class BaseConfiguration
    {
        public static void Config(ModelBuilder model)
        {
            foreach (var entityTypes in model.Model.GetEntityTypes())
            {
                foreach (var item in entityTypes.GetProperties())
                {
                    if (item.ClrType == typeof(Guid))
                        item.ValueGenerated = Microsoft.EntityFrameworkCore.Metadata.ValueGenerated.OnAdd;
                    if (item.Name == "RowVersion")
                        item.IsConcurrencyToken = true;
                    if (item.ClrType == typeof(string))
                        item.SetMaxLength(1000);
                }
            }

            //model.Properties<Guid>()
            //      .Where(p => p.Name == "Id")
            //      .Configure(p => p.HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity));

            //model.Properties().Where(p => p.Name == "RowVersion").Configure(p => p.IsRowVersion());
            //model.Properties().Where(p => p.Name == "RowVersion").Configure(p => p.IsConcurrencyToken().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity));


            //model.Properties<string>().Configure(p => p.HasColumnType("varchar"));
            //model.Conventions.Remove<PluralizingTableNameConvention>();
            //model.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            //model.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            model.Entity<Commands>().Property(p => p.Body).HasMaxLength(4000);
            model.Entity<AtendimentoProcedimento>().HasKey(x => x.Id);
            //model.Entity<AtendimentoProcedimento>().Property(u => u.Id).ValueGeneratedOnAdd();
            //model.Entity<AtendimentoProcedimento>().HasKey(r => new { r.AtendimentoId, r.ProcedimentoId });

            model.Entity<ProcedimentoCid>().HasKey(bc => new { bc.CidId, bc.ProcedimentoId });

            model.Entity<ProcedimentoCid>()
                .HasOne(bc => bc.Procedimento)
                .WithMany(b => b.ProcedimentosCids)
                .HasForeignKey(bc => bc.ProcedimentoId);

            model.Entity<ProcedimentoCid>()
                .HasOne(bc => bc.Cid)
                .WithMany(c => c.ProcedimentosCids)
                .HasForeignKey(bc => bc.CidId);

            model.Entity<HipoteseDiagnosticaCid>().HasKey(bc => new { bc.CidId, bc.HipoteseDiagnosticaId });

            model.Entity<HipoteseDiagnosticaCid>()
                .HasOne(bc => bc.HipoteseDiagnostica)
                .WithMany(b => b.HipoteseDiagnosticaCids)
                .HasForeignKey(bc => bc.HipoteseDiagnosticaId);

            model.Entity<HipoteseDiagnosticaCid>()
                .HasOne(bc => bc.Cid)
                .WithMany(c => c.HipoteseDiagnosticaCids)
                .HasForeignKey(bc => bc.CidId);


            model.Entity<EvolucaoProcedimento>().HasKey(bc => new { bc.ProcedimentoId, bc.EvolucaoId });

            model.Entity<EvolucaoProcedimento>()
                .HasOne(bc => bc.Procedimento)
                .WithMany(b => b.EvolucaoProcedimentos)
                .HasForeignKey(bc => bc.ProcedimentoId);

            model.Entity<EvolucaoProcedimento>()
                .HasOne(bc => bc.Evolucao)
                .WithMany(c => c.EvolucaoProcedimentos)
                .HasForeignKey(bc => bc.EvolucaoId);


            model.Entity<ProfissionalServico>().HasKey(bc => new { bc.ProfissionalId, bc.ServicoId });

            model.Entity<ProfissionalServico>()
                .HasOne(bc => bc.Profissional)
                .WithMany(b => b.ProfissionalServicos)
                .HasForeignKey(bc => bc.ProfissionalId);

            model.Entity<ProfissionalServico>()
                .HasOne(bc => bc.Servico)
                .WithMany(c => c.ProfissionalServicos)
                .HasForeignKey(bc => bc.ServicoId);

        }
    }
}

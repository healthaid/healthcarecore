﻿using HealthCare.Domain.Entities.DbClasses.Admin;
using HealthCare.Domain.Entities.DbClasses.Atendimento;
using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Infra.Data.Context.EntityConf;
using HealthCare.Infra.Data.Context.EntityConf.Admin;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using System.Collections.Generic;

namespace HealthCare.Infra.Data.Context
{
    public class HealthCareDbContext : IdentityDbContext<Usuario, Role, Guid, UserClaim, UserRole, UserLogin, RoleClaim, UserToken>
    {

        public const string SchemaName = "HealthAid";
        private string connectionString = "";

        public HealthCareDbContext(DbContextOptions<HealthCareDbContext> options) : base(options)
        {
            try
            {
                this.connectionString = ((Microsoft.EntityFrameworkCore.Infrastructure.RelationalOptionsExtension)options.Extensions.ElementAt(1)).ConnectionString;

                Database.EnsureCreated();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public HealthCareDbContext() { }

        public IDbConnection DapperHelper
        {
            get
            {
                IDbConnection dapperConnection = new MySqlConnection(this.connectionString);
                return dapperConnection;
            }
        }

        ////Atributo necessário pois quando existe mais de um construtor o unity escolhe o que possui maior número de parametros.
        //public HealthCareDbContext() :
        //    base("MySqlHealthCareDbContext")
        //{
        //    //this.Configuration.ProxyCreationEnabled = false;
        //    //this.Configuration.LazyLoadingEnabled = false;
        //}

        #region DbSets
        public DbSet<Endereco> Endereco { get; set; }

        public DbSet<Commands> Commands { get; set; }

        public DbSet<Paciente> Paciente { get; set; }

        public DbSet<Event> Event { get; set; }

        public DbSet<UF> UF { get; set; }

        public DbSet<Cidade> Cidade { get; set; }

        public DbSet<Bairro> Bairro { get; set; }

        public DbSet<Atendimento> Atendimento { get; set; }
        public DbSet<AtendimentoProcedimento> AtendimentoProcedimento { get; set; }

        public DbSet<ResultadoExame> ResultadoExame { get; set; }

        public DbSet<Laudo> Laudo { get; set; }

        public DbSet<Assinatura> Assinatura { get; set; }

        public DbSet<Clinica> Clinica { get; set; }
        public DbSet<Ciap2> Ciap2 { get; set; }
        public DbSet<Anamnese> Anamnese { get; set; }
        public DbSet<ResultadoExameElemento> ResultadoExameElemento { get; set; }
        public DbSet<CID> CID { get; set; }
        public DbSet<HipoteseDiagnostica> HipoteseDiagnostica { get; set; }
        public DbSet<Imagem> Imagem { get; set; }
        public DbSet<ImagemAtendimento> ImagemAtendimento { get; set; }
        public DbSet<Atestados> Atestados { get; set; }
        public DbSet<TemplatesAtestado> TemplatesAtestado { get; set; }
        public DbSet<Servico> Servico { get; set; }
        public DbSet<Profissional> Profissional { get; set; }
        public DbSet<Unidade> Unidade { get; set; }
        public DbSet<Cbo> Cbo { get; set; }
        public DbSet<Ciap2_Cid10> Ciap2_Cid10 { get; set; }
        public DbSet<Acolhimento> Acolhimento { get; set; }
        public DbSet<ClassificacaoDeRisco> ClassificacaoDeRisco { get; set; }
        public DbSet<SinaisVitais> SinaisVitais { get; set; }
        public DbSet<MotivoCheckout> MotivoCheckout { get; set; }
        public DbSet<TipoMotivoChekout> TipoMotivoChekout { get; set; }
        public DbSet<SetorClinica> SetorClinica { get; set; }
        public DbSet<Risco> Risco { get; set; }
        public DbSet<ServicoSocial> ServicoSocial { get; set; }
        public DbSet<Procedimento> Procedimento { get; set; }
        public DbSet<ReceitaMedica> ReceitaMedica { get; set; }
        public DbSet<Prescricao> Prescricao { get; set; }
        public DbSet<Exame> Exame { get; set; }
        public DbSet<TipoExame> TipoExame { get; set; }
        public DbSet<ExameAtendimento> ExameAtendimento { get; set; }
        public DbSet<Medicamento> Medicamento { get; set; }
        public DbSet<UnidadeMedida> UnidadeMedida { get; set; }
        public DbSet<AdministracaoMedicamento> AdministracaoMedicamento { get; set; }
        public DbSet<Pais> Pais { get; set; }

        public DbSet<Enfermaria> Enfermaria { get; set; }
        public DbSet<Leito> Leito { get; set; }
        public DbSet<MovimentacaoLeito> MovimentacaoLeito { get; set; }
        public DbSet<Internacao> Internacao { get; set; }
        public DbSet<Evolucao> Evolucao { get; set; }

        public DbSet<ProcedimentosEnfermagem> ProcedimentosEnfermagem { get; set; }
        public DbSet<LiquidosAdministrados> LiquidosAdministrados { get; set; }
        public DbSet<LiquidosEliminados> LiquidosEliminados { get; set; }
        public DbSet<EvolucaoEnfermagem> EvolucaoEnfermagem { get; set; }

        public DbSet<LiquidosAdministradosEnfermagem> LiquidosAdministradosEnfermagem { get; set; }
        public DbSet<LiquidosEliminadosEnfermagem> LiquidosEliminadosEnfermagem { get; set; }

        public DbSet<Painel> Painel { get; set; }

        public DbSet<LocalAtendimento> LocalAtendimento { get; set; }

        public DbSet<ProcedimentoCid> ProcedimentoCid { get; set; }

        public DbSet<HipoteseDiagnosticaCid> HipoteseDiagnosticaCid { get; set; }

        public DbSet<ProfissionalServico> ProfissionalServico { get; set; }
        public DbSet<Checkout> Checkout { get; set; }
        public DbSet<DelegacaoProfissional> DelegacaoProfissional { get; set; }

        #endregion

        public override int SaveChanges()
        {
            try
            {
                return base.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public T Find<T>(Guid id) where T : class
        {
            return this.Set<T>().Find(id);
        }

        public IQueryable<T> Query<T>() where T : class
        {
            return this.Set<T>();
        }

        public IQueryable<T> Query<T>(params string[] includes) where T : class
        {
            var query = this.Set<T>().AsQueryable();
            foreach (string include in includes)
                query = query.Include(include);

            return query;
        }

        public IEnumerable<T> DapperQuery<T>(string sql,object param = null) where T : class
        {
            try
            {
                IDbConnection dapperConnection = new MySqlConnection(this.connectionString);
                return dapperConnection.Query<T>(sql, param);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public IEnumerable<TReturn> _DapperQuery<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TSeventh, TReturn>(string sql, object param = null) where TReturn : class
        //{
        //    try
        //    {
        //        IDbConnection dapperConnection = new MySqlConnection(this.connectionString);
        //        return dapperConnection.Query<TFirst,TSecond>>(sql,param);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            BaseConfiguration.Config(modelBuilder);
            ModuloAdminConfiguration.Config(modelBuilder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);

        }


    }
}

﻿using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.CQRS.Messaging.Handling;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Data.Sql
{
    public class SqlMessageLogHandler : IEventHandler<IEvent>, ICommandHandler<ICommand>
    {
        private SqlMessageLog log;

        public SqlMessageLogHandler(SqlMessageLog log)
        {
            this.log = log;
        }

        public void Handle(IEvent @event)
        {
            this.log.Save(@event);
        }

        public void Handle(ICommand command)
        {
            this.log.Save(command);
        }

        public object HandleWithReturn(ICommand command)
        {
            this.log.Save(command);
            return new Validation();
        }
    }
}

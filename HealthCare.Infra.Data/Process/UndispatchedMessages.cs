﻿using System;

namespace HealthCare.Data.Process
{
    public class UndispatchedMessages
    {
        protected UndispatchedMessages() { }

        public UndispatchedMessages(Guid id)
        {
            this.Id = id;
        }

        public Guid Id { get; set; }
        public string Commands { get; set; }
    }
}

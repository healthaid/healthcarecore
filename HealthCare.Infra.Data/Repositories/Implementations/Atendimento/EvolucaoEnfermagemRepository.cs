﻿using System;
using System.Linq;
using HealthCare.Data.Context;
using HealthCare.Data.Repositories.Definitions.Atendimento;
using HealthCare.Domain.Entities.Atendimento;
using HealthCare.Infra.Data.Repositories.Base;

namespace HealthCare.Data.Repositories.Implementations.Atendimento
{
    public class EvolucaoEnfermagemRepository : BaseRepository<EvolucaoEnfermagem>, IEvolucaoEnfermagemRepository
    {

        public EvolucaoEnfermagemRepository(HealthCareDbContext context)
            : base(context)
        { }

        public int? ObtemUltimoCodigoGerado()
        {
            if (this.context.EvolucaoEnfermagem.Count() > 0)
                return this.context.EvolucaoEnfermagem.ToList().Max(x => Convert.ToInt32(x.NumAtendimento));
            else
                return 0;
        }
    }
}

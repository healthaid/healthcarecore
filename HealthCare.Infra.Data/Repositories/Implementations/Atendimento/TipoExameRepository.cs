﻿using HealthCare.Data.Context;
using HealthCare.Data.Repositories.Definitions.Atendimento;
using HealthCare.Domain.Entities.Atendimento;
using HealthCare.Infra.Data.Repositories.Base;
namespace HealthCare.Data.Repositories.Implementations.Atendimento
{
    public class TipoExameRepository : BaseRepository<TipoExame>, ITipoExameRepository
    {
        public TipoExameRepository(HealthCareDbContext context)
            : base(context)
        { }


    }
}

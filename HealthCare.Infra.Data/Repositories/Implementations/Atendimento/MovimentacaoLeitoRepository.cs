﻿using HealthCare.Data.Context;
using HealthCare.Data.Repositories.Definitions.Cadastro;
using System.Linq;
using System.Collections.Generic;
using HealthCare.Domain.Entities.Cadastro;
using HealthCare.Infra.Data.Repositories.Base;
using System;

namespace HealthCare.Data.Repositories.Implementations.Cadastro
{
    public class MovimentacaoLeitoRepository : BaseRepository<MovimentacaoLeito>, IMovimentacaoLeitoRepository
    {

        public MovimentacaoLeitoRepository(HealthCareDbContext context)
            : base(context)
        { }

        public MovimentacaoLeito GetMovimentacaoLeito(Guid id)
        {
            return context.MovimentacaoLeito.Include("Internacao").Include("Clinica").Include("LeitoAtual").FirstOrDefault(x => x.Id == id);
        }

        public List<MovimentacaoLeito> ListarPacientesInternados()
        {
            return context.MovimentacaoLeito.Include("Internacao.Atendimento.Acolhimento.Paciente").Include("Internacao.Atendimento").ToList();
        }

        public MovimentacaoLeito GetUltimaMovimentacaoLeito(Guid atendimentoId)
        {
            return context.MovimentacaoLeito.Include("Internacao.Atendimento").Include("LeitoAtual").Where(x => x.Internacao.Atendimento.Id == atendimentoId).OrderByDescending(x => x.DataMovimentacao).FirstOrDefault();
        }
    }
}
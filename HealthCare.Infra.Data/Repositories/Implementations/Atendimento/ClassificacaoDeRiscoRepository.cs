﻿using HealthCare.Data.Context;
using HealthCare.Data.Repositories.Definitions.Atendimento;
using HealthCare.Domain.Entities.Atendimento;
using HealthCare.Infra.Data.Repositories.Base;
using System.Linq;

namespace HealthCare.Data.Repositories.Implementations.Atendimento
{
    public class ClassificacaoDeRiscoRepository : BaseRepository<ClassificacaoDeRisco>, IClassificacaoDeRiscoRepository
    {
        public ClassificacaoDeRiscoRepository(HealthCareDbContext context)
            : base(context)
        { }
        
        public ClassificacaoDeRisco GetClassificacaoRisco(System.Guid Id)
        {
            return this.context.ClassificacaoDeRisco.Include("SinaisVitais").Include("Acolhimento").Include("Risco").First(x => x.Id == Id);
        }
    }
}
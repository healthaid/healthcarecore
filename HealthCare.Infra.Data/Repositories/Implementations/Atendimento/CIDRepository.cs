﻿using System;
using System.Linq;
using System.Linq.Expressions;
using HealthCare.Data.Context;
using HealthCare.Data.Repositories.Definitions.Atendimento;
using HealthCare.Domain.Entities.Atendimento;
using HealthCare.Infra.Data.Repositories.Base;
namespace HealthCare.Data.Repositories.Implementations.Atendimento
{
    public class CIDRepository : BaseRepository<CID>, ICIDRepository
    {

        public CIDRepository(HealthCareDbContext context)
            : base(context)
        { }

        public CID ObtemCidPorCodigo(string codigo)
        {
            return this.context.Query<CID>().FirstOrDefault(x => x.Codigo == codigo);
        }
    }
}
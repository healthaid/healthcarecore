﻿using HealthCare.Data.Context;
using HealthCare.Data.Repositories.Definitions;
using HealthCare.Data.Repositories.Definitions.Atendimento;
using HealthCare.Infra.Data.Repositories.Base;
using HealthCare.Domain.Entities.Atendimento;
namespace HealthCare.Data.Repositories.Implementations.Atendimento
{
    public class LaudoRepository : BaseRepository<Laudo>, ILaudoRepository
    {
        public LaudoRepository(HealthCareDbContext context)
            : base(context)
        { }


    }
}

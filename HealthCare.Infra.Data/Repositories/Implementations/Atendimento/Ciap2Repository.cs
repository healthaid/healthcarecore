﻿using HealthCare.Data.Context;
using HealthCare.Data.Repositories.Definitions.Atendimento;
using HealthCare.Domain.Entities.Atendimento;
using HealthCare.Infra.Data.Repositories.Base;

namespace HealthCare.Data.Repositories.Implementations.Atendimento
{
    public class Ciap2Repository : BaseRepository<Ciap2>, ICiap2Repository
    {

        public Ciap2Repository(HealthCareDbContext context)
            : base(context)
        { }

    }
}

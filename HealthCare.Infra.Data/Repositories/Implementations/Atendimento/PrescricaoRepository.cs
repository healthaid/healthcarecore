﻿using HealthCare.Data.Context;
using HealthCare.Data.Repositories.Definitions.Atendimento;
using HealthCare.Data.Repositories.Definitions.Atendimento.Admin;
using HealthCare.Domain.Entities.Atendimento;
using HealthCare.Infra.Data.Repositories.Base;

namespace HealthCare.Data.Repositories.Implementations.Atendimento
{
    public class PrescricaoRepository : BaseRepository<Prescricao>, IPrescricaoRepository
    {
        public PrescricaoRepository(HealthCareDbContext context)
            : base(context)
        { }
    }
}
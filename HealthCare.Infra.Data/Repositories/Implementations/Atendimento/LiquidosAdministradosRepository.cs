﻿using HealthCare.Data.Context;
using HealthCare.Data.Repositories.Definitions.Atendimento;
using HealthCare.Domain.Entities.Atendimento;
using HealthCare.Infra.Data.Repositories.Base;

namespace HealthCare.Data.Repositories.Implementations.Atendimento
{
    public class LiquidosAdministradosRepository : BaseRepository<LiquidosAdministrados>, ILiquidosAdministradosRepository
    {

        public LiquidosAdministradosRepository(HealthCareDbContext context)
            : base(context)
        { }

    }
}

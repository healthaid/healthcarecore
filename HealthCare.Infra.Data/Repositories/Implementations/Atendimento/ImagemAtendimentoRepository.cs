﻿
using HealthCare.Data.Context;
using HealthCare.Data.Repositories.Definitions.Atendimento;
using HealthCare.Domain.Entities.Atendimento;
using HealthCare.Infra.Data.Repositories.Base;
using System;
namespace HealthCare.Data.Repositories.Implementations.Atendimento
{
    public class ImagemAtendimentoRepository : BaseRepository<ImagemAtendimento>, IImagemAtendimentoRepository
    {

        public ImagemAtendimentoRepository(HealthCareDbContext context)
            : base(context)
        { }

    }
}
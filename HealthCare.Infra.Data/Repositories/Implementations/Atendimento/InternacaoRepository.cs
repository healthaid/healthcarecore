﻿using HealthCare.Data.Context;
using HealthCare.Data.Repositories.Definitions.Cadastro;
using HealthCare.Domain.Entities;
using HealthCare.Domain.Entities.Atendimento;
using HealthCare.Domain.Entities.Cadastro;
using HealthCare.Infra.Data.Repositories.Base;
namespace HealthCare.Data.Repositories.Implementations.Cadastro
{
    public class InternacaoRepository : BaseRepository<Internacao>, IInternacaoRepository
    {

        public InternacaoRepository(HealthCareDbContext context)
            : base(context)
        { }

    }
}

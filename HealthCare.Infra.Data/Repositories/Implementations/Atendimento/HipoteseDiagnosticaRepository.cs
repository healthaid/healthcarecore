﻿
using HealthCare.Data.Context;
using HealthCare.Data.Repositories.Definitions.Atendimento;
using HealthCare.Domain.Entities.Atendimento;
using HealthCare.Infra.Data.Repositories.Base;
using System;
using System.Linq;
namespace HealthCare.Data.Repositories.Implementations.Atendimento
{
    public class HipoteseDiagnosticaRepository : BaseRepository<HipoteseDiagnostica>, IHipoteseDiagnosticaRepository
    {

        public HipoteseDiagnosticaRepository(HealthCareDbContext context)
            : base(context)
        { }

        public HipoteseDiagnostica GetByAtendimentoId(Guid atendimentoId)
        {
            return this.context.HipoteseDiagnostica.Include("Atendimento").FirstOrDefault(x => x.Atendimento.Id == atendimentoId);
        }
    }
}
﻿
using HealthCare.Data.Context;
using HealthCare.Data.Repositories.Definitions.Atendimento;
using HealthCare.Domain.Entities.Atendimento;
using HealthCare.Infra.Data.Repositories.Base;
using System;
using System.Linq;
namespace HealthCare.Data.Repositories.Implementations.Atendimento
{
    public class AcolhimentoRepository : BaseRepository<Acolhimento>, IAcolhimentoRepository
    {
        public AcolhimentoRepository(HealthCareDbContext context)
            : base(context)
        {
 
        }
        public int? ObtemUltimoCodigoGerado()
        {
            if (this.context.Acolhimento.Count() > 0)
                return this.context.Acolhimento.ToList().Max(x => Convert.ToInt32(x.CodigoBoletim));
            else
                return 0;
        }
    }
}
﻿using HealthCare.Data.Context;
using HealthCare.Data.Repositories.Definitions.Atendimento;
using HealthCare.Domain;
using HealthCare.Domain.Entities.Atendimento;
using HealthCare.Infra.Data.Repositories.Base;
namespace HealthCare.Data.Repositories.Implementations.Atendimento
{
    public class ResultadoElementoExameRepository : BaseRepository<ResultadoExame>, IResultadoElementoExameRepository
    {
        public ResultadoElementoExameRepository(HealthCareDbContext context)
            : base(context)
        { }

    }
}

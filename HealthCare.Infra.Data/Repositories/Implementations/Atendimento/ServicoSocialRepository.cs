﻿using HealthCare.Data.Context;
using HealthCare.Data.Repositories.Definitions;
using HealthCare.Data.Repositories.Definitions.Atendimento;
using HealthCare.Domain.Entities.Atendimento;
using HealthCare.Infra.Data.Repositories.Base;
using ServicoSocials = HealthCare.Domain.Entities.Atendimento;
namespace HealthCare.Data.Repositories.Implementations.Atendimento
{
    public class ServicoSocialRepository : BaseRepository<ServicoSocial>, IServicoSocialRepository
    {
        public ServicoSocialRepository(HealthCareDbContext context)
            : base(context)
        { }


    }
}

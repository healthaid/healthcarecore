﻿using HealthCare.Data.Context;
using HealthCare.Data.Repositories.Definitions.Atendimento;
using HealthCare.Domain.Entities.Atendimento;
using HealthCare.Infra.Data.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Data.Repositories.Implementations.Atendimento
{
    public class ProcedimentoRepository : BaseRepository<Procedimento>, IProcedimentoRepository
    {
        public ProcedimentoRepository(HealthCareDbContext context)
            : base(context)
        { }

        public Procedimento GetProcedimentoByCodigo(string codigoProcecimento)
        {
            return base.context.Procedimento.FirstOrDefault(x => x.CO_PROCEDIMENTO == codigoProcecimento);
        }
    }
}

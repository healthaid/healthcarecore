﻿using HealthCare.Data.Context;
using HealthCare.Data.Repositories.Definitions.Atendimento;
using HealthCare.Domain.Entities.Atendimento;
using HealthCare.Infra.Data.Repositories.Base;
namespace HealthCare.Data.Repositories.Implementations.Atendimento
{
    public class ExameRepository: BaseRepository<Exame>, IExameRepository
    {
        public ExameRepository(HealthCareDbContext context)
            : base(context)
        { }


    }
}

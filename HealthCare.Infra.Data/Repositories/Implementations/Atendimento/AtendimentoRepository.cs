﻿using HealthCare.Data.Context;
using HealthCare.Data.Repositories.Definitions.Atendimento;
using HealthCare.Infra.Data.Repositories.Base;
using System;
using System.Linq;
using Atendimentos = HealthCare.Domain.Entities.Atendimento;
namespace HealthCare.Data.Repositories.Implementations.Atendimento
{
    public class AtendimentoRepository : BaseRepository<Atendimentos.Atendimento>, IAtendimentoRepository
    {
        public AtendimentoRepository(HealthCareDbContext context)
            : base(context)
        { }

        public Atendimentos.Atendimento GetAtendimentoWithProcedimentos(Guid id)
        {
            return this.context.Atendimento.Include("Procedimentos").Where(x => x.Id == id).FirstOrDefault();
        }

    }
}

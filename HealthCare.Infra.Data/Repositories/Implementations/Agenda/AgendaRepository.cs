﻿using HealthCare.Data.Context;
using HealthCare.Data.Repositories.Definitions.Agenda;
using HealthCare.Infra.Data.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Data.Repositories.Implementations.Agenda
{
    public class AgendaRepository : BaseRepository<HealthCare.Domain.Entities.Agenda.Agenda>, IAgendaRepository
    {
        public AgendaRepository(HealthCareDbContext context)
            : base(context)
        { }

    }
}

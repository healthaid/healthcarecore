﻿using HealthCare.Data.Context;
using HealthCare.Data.Repositories.Definitions.Agenda;
using HealthCare.Domain.Entities;
using HealthCare.Infra.Data.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Data.Repositories.Implementations.Agenda
{
    public class AgendaEventoRepository : BaseRepository<AgendaEvento>, IAgendaEventoRepository
    {
        public AgendaEventoRepository(HealthCareDbContext context)
            : base(context)
        { }

        public AgendaEvento ObterAgendaEvento(Guid id)
        {
            var agendaEvento = this.context.AgendaEvento.Include("Agenda").FirstOrDefault(x => x.Id == id);
            return agendaEvento;
        }

        public bool ExisteAgendaEvento(DateTime horaFim, DateTime horaInicio, Guid profissionalId)
        {
            return this.context.AgendaEvento.Include("Agenda").FirstOrDefault(x => x.Agenda != null
                                                                                && x.HoraFim == horaFim
                                                                                && x.HoraInicio == horaInicio
                                                                                && x.Agenda.ProfissionalId == profissionalId) != null;
        }
    }
}

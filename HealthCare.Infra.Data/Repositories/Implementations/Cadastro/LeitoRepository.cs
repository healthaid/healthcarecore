﻿using System.Collections.Generic;
using System.Linq;
using HealthCare.Data.Context;
using HealthCare.Data.Repositories.Definitions.Cadastro;
using HealthCare.Domain.Entities.Cadastro;
using HealthCare.Infra.Data.Repositories.Base;
namespace HealthCare.Data.Repositories.Implementations.Cadastro
{
    public class LeitoRepository : BaseRepository<Leito>, ILeitoRepository
    {

        public LeitoRepository(HealthCareDbContext context)
            : base(context)
        { }

        /// <summary>
        /// Listar os leitos com suas movimentações
        /// </summary>
        /// <returns></returns>
        public IList<Leito> ListarLeitosComMovimentacao()
        {
            return context.Leito.Include("Movimentacoes").Include("Clinica").ToList();
        }
    }
}

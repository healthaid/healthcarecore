﻿using HealthCare.Data.Context;
using HealthCare.Data.Repositories.Definitions.Cadastro;
using HealthCare.Domain.Entities.Cadastro;
using HealthCare.Infra.Data.Repositories.Base;
namespace HealthCare.Data.Repositories.Implementations.Cadastro
{
    public class CidadeRepository : BaseRepository<Cidade>, ICidadeRepository
    {

        public CidadeRepository(HealthCareDbContext context)
            : base(context)
        { }

    }
}

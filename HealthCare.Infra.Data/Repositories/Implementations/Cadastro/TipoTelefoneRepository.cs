﻿using HealthCare.Data.Context;
using HealthCare.Data.Repositories.Definitions.Cadastro;
using HealthCare.Domain.Entities.Cadastro;
using HealthCare.Infra.Data.Repositories.Base;
using System;

namespace HealthCare.Data.Repositories.Implementations.Cadastro
{
    public class TipoTelefoneRepository : BaseRepository<TipoTelefone>, ITipoTelefoneRepository
    {

        public TipoTelefoneRepository(HealthCareDbContext context)
            : base(context)
        { }

    }
}

﻿using HealthCare.Data.Context;
using HealthCare.Data.Repositories.Definitions.Cadastro;
using HealthCare.Domain.Entities.Cadastro;
using HealthCare.Infra.Data.Repositories.Base;
using System;

namespace HealthCare.Data.Repositories.Implementations.Cadastro
{
    public class TelefoneRepository: BaseRepository<Telefone>, ITelefoneRepository
    {

        public TelefoneRepository(HealthCareDbContext context)
            : base(context)
        { }

    }
}

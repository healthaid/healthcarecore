﻿using System;
using System.Linq;
using HealthCare.Data.Context;
using HealthCare.Data.Repositories.Definitions.Cadastro;
using HealthCare.Domain.Entities;
using HealthCare.Domain.Entities.Cadastro;
using HealthCare.Infra.Data.Repositories.Base;
namespace HealthCare.Data.Repositories.Implementations.Cadastro
{
    public class EnfermariaRepository : BaseRepository<Enfermaria>, IEnfermariaRepository
    {

        public EnfermariaRepository(HealthCareDbContext context)
            : base(context)
        { }

        public Enfermaria GetEnfermariaComLeitos(Guid id)
        {
            return context.Enfermaria.Include("Leitos").FirstOrDefault(x => x.Id == id);
        }
    }
}

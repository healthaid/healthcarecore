﻿using HealthCare.Data.Context;
using HealthCare.Data.Repositories.Definitions.Cadastro;
using HealthCare.Domain.Entities;
using HealthCare.Domain.Entities.Cadastro;
using HealthCare.Infra.Data.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Data.Repositories.Implementations.Cadastro
{
    public class BairroRepository : BaseRepository<Bairro>, IBairroRepository
    {

        public BairroRepository(HealthCareDbContext context)
            : base(context)
        { }

    }
}

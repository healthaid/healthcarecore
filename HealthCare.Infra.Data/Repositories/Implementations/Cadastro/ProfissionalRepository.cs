﻿using HealthCare.Data.Context;
using HealthCare.Data.Repositories.Definitions.Cadastro;
using HealthCare.Domain.Entities.Cadastro;
using HealthCare.Infra.Data.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Data.Repositories.Implementations.Cadastro
{
    public class ProfissionalRepository : BaseRepository<Profissional>, IProfissionalRepository
    {

        public ProfissionalRepository(HealthCareDbContext context)
            : base(context)
        { }

        public override void Insert(HealthCare.Domain.Entities.Cadastro.Profissional entity)
        {
            base.Insert(entity);
        }

        public Profissional GetProfissionalComServicos(Guid id)
        {
            return this.context.Profissional.Include("Servicos").FirstOrDefault(x => x.Id == id);
        }
    }
}

﻿using HealthCare.Data.Context;
using HealthCare.Data.Repositories.Definitions.Cadastro;
using HealthCare.Domain.Entities.Cadastro;
using HealthCare.Infra.Data.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Data.Repositories.Implementations.Cadastro
{
    public class PacienteRepository : BaseRepository<Paciente>, IPacienteRepository
    {

        public PacienteRepository(HealthCareDbContext context)
            : base(context)
        { }

        public override void Insert(HealthCare.Domain.Entities.Cadastro.Paciente entity)
        {
            base.Insert(entity);
        }

        public int? ObtemUltimoCodigoGerado()
        {
            if (this.context.Paciente.Count() > 0)
                return this.context.Paciente.ToList().Max(x => Convert.ToInt32(x.CodigoPaciente));
            else
                return 0;
        }
    }
}

﻿using HealthCare.Data.Context;
using HealthCare.Data.Repositories.Definitions.Cadastro;
using HealthCare.Domain.Entities.Cadastro;
using HealthCare.Infra.Data.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Data.Repositories.Implementations.Cadastro
{
    public class ServicoRepository : BaseRepository<Servico>, IServicoRepository
    {

        public ServicoRepository(HealthCareDbContext context)
            : base(context)
        { }

        public override void Insert(HealthCare.Domain.Entities.Cadastro.Servico entity)
        {
            base.Insert(entity);
        }

        public Servico GetServicoByAtendimentoId(Guid atendimentoId)
        {
            return context.Atendimento.Include("Acolhimento.Servico").FirstOrDefault(x => x.Id == atendimentoId).Acolhimento.Servico;
        }

    }
}

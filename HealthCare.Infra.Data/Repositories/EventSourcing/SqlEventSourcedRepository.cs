﻿using HealthCare.Data.Context;
using HealthCare.Domain.Entities.Admin;
using HealthCare.Infra.CQRS;
using HealthCare.Infra.CQRS.EventSourcing;
using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.CQRS.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Data.Repositories.EventSourcing
{
    public class SqlEventSourcedRepository<T> : IEventSourcedRepository<T> where T : class, IEventSourced
    {
        private static readonly string sourceType = typeof(T).Name;
        private readonly IEventBus eventBus;
        private readonly ITextSerializer serializer;
        private readonly Func<EventStoreDbContext> contextFactory;
        private readonly Func<Guid, IEnumerable<IVersionedEvent>, T> entityFactory;

        public SqlEventSourcedRepository(IEventBus eventBus, ITextSerializer serializer, Func<EventStoreDbContext> contextFactory)
        {
            this.eventBus = eventBus;

            this.serializer = serializer;

            this.contextFactory = contextFactory;
            // TODO: could be replaced with a compiled lambda

            var constructor = typeof(T).GetConstructor(new[] { typeof(Guid), typeof(IEnumerable<IVersionedEvent>) });
            if (constructor == null)
            {
                throw new InvalidCastException("Type T must have a constructor with the following signature: .ctor(Guid, IEnumerable<IVersionedEvent>)");
            }
            this.entityFactory = (id, events) => (T)constructor.Invoke(new object[] { id, events });
        }

        public T Find(Guid id)
        {
            using (var context = this.contextFactory.Invoke())
            {
                var deserialized = context.Set<Event>()
                    .Where(x => x.Id == id && x.AggregateType == sourceType)
                    .OrderBy(x => x.Version)
                    .AsEnumerable()
                    .Select(this.Deserializer)
                    .AsCachedAnyEnumerable() ;
                if (deserialized.Any())
                    return entityFactory.Invoke(id, deserialized);
                return null;
            }
        }

        public T Get(Guid id)
        {
            var entity = this.Find(id);
            if(entity == null)
            {
                throw new EntityNotFoundException(id, sourceType);
            }
            return entity;
        }

        public void Save(T eventSourced, string correlationId)
        {
            var events = eventSourced.Events.ToArray();
            using(var context = this.contextFactory.Invoke())
            {
                var eventsSet = context.Set<Event>();
                foreach(var e in events)
                {
                    eventsSet.Add(this.Serialize(e, correlationId));
                }
                context.SaveChanges();
            }
        }

        private Event Serialize(IVersionedEvent e, string correlationId)
        {
            Event serialized;
            using(var writer = new StringWriter())
            {
                this.serializer.Serialize(writer, e);
                serialized = new Event
                {
                    Id = e.SourceId,
                    AggregateType = sourceType,
                    Version = e.Version,
                    Payload = writer.ToString(),
                    CorrelationId = correlationId
                };
            }
            return serialized;
        }

        private IVersionedEvent Deserializer(Event @event)
        {
            using(var reader = new StringReader(@event.Payload))
            {
                return (IVersionedEvent)this.serializer.Deserialize(reader);
            }
        }
    }
}

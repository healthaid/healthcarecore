﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace HealthCare.Infra.Data.Repositories.Base
{
    public interface IRepository<T>
    {
        IList<T> Find(Expression<Func<T, bool>> predicate);
        IList<T> Find(Expression<Func<T, bool>> predicate, Expression<Func<T, object>>[] fetchs);
        IList<T> GetAll();
        IList<T> GetAll(Expression<Func<T, object>>[] fetchs);
        T Get(object id);
        T Get(object id, Expression<Func<T, object>>[] fetchs);
        void Insert(T entity);
        void Update(T entity);
        void Delete(object id);
    }
}

﻿

using HealthCare.Data.Repositories.Definitions.Admin;
using HealthCare.Infra.Data.Repositories.Definitions.Admin;
namespace HealthCare.Infra.Data.Repositories.Base
{
    public interface IRepositoryFactory
    {
        IUsuarioRepository Usuario { get; }
        IConfiguracaoRepository Configuracao { get; }
    }
}

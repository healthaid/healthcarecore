﻿using HealthCare.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace HealthCare.Infra.Data.Repositories.Base
{
    public abstract class BaseRepository<T> : IRepository<T> where T : class
    {
        protected HealthCareDbContext context { get; private set; }

        protected BaseRepository(HealthCareDbContext databaseContext)
        {
            context = databaseContext;
        }

        protected DbSet<T> Entities { get { return context.Set<T>(); } }

        public virtual IList<T> GetAll()
        {
            return Entities.AsNoTracking().ToList();
        }

        public virtual IList<T> GetAll(Expression<Func<T, object>>[] fetchs)
        {
            var query = Entities;

            if (fetchs != null)
                foreach (Expression<Func<T, object>> fetch in fetchs)
                    query.Include(fetch).Load();
                
            return query.ToList();
        }

        public virtual T Get(object id)
        {
            return Entities.Find(id);
        }

        public virtual T Get(object id, Expression<Func<T, object>>[] fetchs)
        {
            var query = Entities;

            if (fetchs != null)
                foreach (Expression<Func<T, object>> fetch in fetchs)
                    query.Include(fetch).Load();

            return query.Find(id);
        }

        public virtual IList<T> Find(Expression<Func<T, bool>> predicate)
        {
            return Entities.Where(predicate).AsNoTracking().ToList();
        }

        public virtual IList<T> Find(Expression<Func<T, bool>> predicate, Expression<Func<T, object>>[] fetchs)
        {
            var query = Entities;

            if (fetchs != null)
                foreach (Expression<Func<T, object>> fetch in fetchs)
                    query.Include(fetch).Load();

            return query.Where(predicate).ToList();
        }

        public virtual void Delete(object id)
        {
            try
            {
                var toDelete = context.Set<T>().Find(id);
                context.Entry(toDelete).State = EntityState.Deleted;

                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual void Update(T entity)
        {
            try
            {
                context.Entry(entity).State = EntityState.Modified;

                context.SaveChanges();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public virtual void Insert(T entity)
        {
            try
            {
                context.Entry(entity).State = EntityState.Added;
                context.Set<T>().Add(entity);

                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

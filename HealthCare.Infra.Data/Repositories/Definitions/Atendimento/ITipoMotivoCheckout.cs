﻿using HealthCare.Domain.Entities.Atendimento;
using HealthCare.Infra.Data.Repositories.Base;
namespace HealthCare.Data.Repositories.Definitions.Atendimento
{
    public interface ITipoMotivoChekoutRepository : IRepository<TipoMotivoChekout>
    {
    }
}
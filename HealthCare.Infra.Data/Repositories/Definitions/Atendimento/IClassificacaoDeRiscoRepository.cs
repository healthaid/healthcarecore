﻿
using HealthCare.Domain.Entities.Atendimento;
using HealthCare.Infra.Data.Repositories.Base;
using System;
namespace HealthCare.Data.Repositories.Definitions.Atendimento
{
    public interface IClassificacaoDeRiscoRepository : IRepository<ClassificacaoDeRisco>
    {
        ClassificacaoDeRisco GetClassificacaoRisco(Guid Id);
    }   
}

﻿using HealthCare.Infra.Data.Repositories.Base;
using System;
using Atendimentos = HealthCare.Domain.Entities.Atendimento;
namespace HealthCare.Data.Repositories.Definitions.Atendimento
{
    public interface IAtendimentoRepository : IRepository<Atendimentos.Atendimento>
    {
        Atendimentos.Atendimento GetAtendimentoWithProcedimentos(Guid id);
    }
}

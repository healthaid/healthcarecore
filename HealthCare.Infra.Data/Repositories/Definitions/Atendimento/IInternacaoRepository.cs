﻿using HealthCare.Domain.Entities;
using HealthCare.Domain.Entities.Atendimento;
using HealthCare.Domain.Entities.Cadastro;
using HealthCare.Infra.Data.Repositories.Base;

namespace HealthCare.Data.Repositories.Definitions.Cadastro
{
    public interface IInternacaoRepository : IRepository<Internacao>
    {
    }
}
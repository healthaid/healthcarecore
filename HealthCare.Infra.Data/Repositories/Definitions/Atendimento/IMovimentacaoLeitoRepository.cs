﻿using System.Collections.Generic;
using HealthCare.Domain.Entities.Cadastro;
using HealthCare.Infra.Data.Repositories.Base;
using System;

namespace HealthCare.Data.Repositories.Definitions.Cadastro
{
    public interface IMovimentacaoLeitoRepository : IRepository<MovimentacaoLeito>
    {
        List<MovimentacaoLeito> ListarPacientesInternados();
        MovimentacaoLeito GetMovimentacaoLeito(Guid id);
        MovimentacaoLeito GetUltimaMovimentacaoLeito(Guid atendimentoId);
    }
}
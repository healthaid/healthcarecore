﻿
using HealthCare.Domain.Entities.Atendimento;
using HealthCare.Infra.Data.Repositories.Base;
namespace HealthCare.Data.Repositories.Definitions.Atendimento.Admin
{
    public interface IAdministracaoMedicamentoRepository : IRepository<AdministracaoMedicamento>
    {
    }
}

﻿using HealthCare.Data;
using HealthCare.Data.Context;
using HealthCare.Domain.Entities.Admin;
using HealthCare.Infra.Data.Repositories.Base;
using HealthCare.Infra.Data.Repositories.Definitions.Admin;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Data.Entity;
using System.Linq;

namespace HealthCare.Infra.Data.Repositories.Implementations.Admin
{
    public class UsuarioRepository : BaseRepository<Usuario>, IUsuarioRepository
    {
        public UsuarioRepository(HealthCareDbContext context)
            : base(context)
        {
        }

        public RoleStore<IdentityRole> GetRoleStore()
        {
            return new RoleStore<IdentityRole>(context);
        }

        public Usuario GetUserWithRoles(Guid id)
        {
            return this.context.Users.Include("Roles").Where(x => x.Id == id).FirstOrDefault();
        }
    }
}

﻿using HealthCare.Data.Context;
using HealthCare.Data.Repositories.Definitions.Admin;
using HealthCare.Domain.Entities.Admin;
using HealthCare.Infra.Data.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Data.Repositories.Implementations.Admin
{
    public class ConfiguracaoRepository : BaseRepository<Configuracao>, IConfiguracaoRepository
    {
        public ConfiguracaoRepository(HealthCareDbContext context)
            : base(context)
        { }

        
    }
}

﻿using HealthCare.Domain.Entities.Atendimento;
using HealthCare.Infra.Data.Repositories.Base;
using ServicoSocials = HealthCare.Domain.Entities;
namespace HealthCare.Data.Repositories.Definitions
{
    public interface IServicoSocialRepository : IRepository<ServicoSocial>
    {
    }
}

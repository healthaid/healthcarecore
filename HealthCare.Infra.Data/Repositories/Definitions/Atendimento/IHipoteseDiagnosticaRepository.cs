﻿
using HealthCare.Domain.Entities.Atendimento;
using HealthCare.Infra.Data.Repositories.Base;
using System;
namespace HealthCare.Data.Repositories.Definitions.Atendimento
{
    public interface IHipoteseDiagnosticaRepository : IRepository<HipoteseDiagnostica>
    {
        HipoteseDiagnostica GetByAtendimentoId(Guid id);
    }
}

﻿using HealthCare.Domain.Entities;
using HealthCare.Infra.Data.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Data.Repositories.Definitions.Agenda
{
    public interface IAgendaEventoRepository : IRepository<AgendaEvento>
    {
        AgendaEvento ObterAgendaEvento(Guid id);

        bool ExisteAgendaEvento(DateTime horaFim, DateTime horaInicio, Guid profissionalId);
    }
}

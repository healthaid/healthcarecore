﻿using HealthCare.Infra.Data.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Data.Repositories.Definitions.Agenda
{
    public interface IAgendaRepository : IRepository<HealthCare.Domain.Entities.Agenda.Agenda>
    {
    }
}

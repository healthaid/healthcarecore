﻿using HealthCare.Domain.Entities;
using HealthCare.Domain.Entities.Cadastro;
using HealthCare.Infra.Data.Repositories.Base;
using System;

namespace HealthCare.Data.Repositories.Definitions.Cadastro
{
    public interface IEnfermariaRepository : IRepository<Enfermaria>
    {
        Enfermaria GetEnfermariaComLeitos(Guid id);
    }
}
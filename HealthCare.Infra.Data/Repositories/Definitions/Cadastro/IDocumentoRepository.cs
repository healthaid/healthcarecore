﻿using HealthCare.Infra.Data.Repositories.Base;
using HealthCare.Domain.Entities.Cadastro;

namespace HealthCare.Data.Repositories.Definitions.Cadastro
{
    public interface IDocumentoRepository : IRepository<Documento>
    {
    }
}

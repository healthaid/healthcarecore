﻿using HealthCare.Domain.Entities.Cadastro;
using HealthCare.Infra.Data.Repositories.Base;
namespace HealthCare.Data.Repositories.Definitions.Cadastro
{
    public interface IEtniaRepository  : IRepository<Etnia>
    {
    }
}

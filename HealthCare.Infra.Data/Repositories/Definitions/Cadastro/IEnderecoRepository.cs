﻿using HealthCare.Domain.Entities;
using HealthCare.Infra.Data.Repositories.Base;

namespace HealthCare.Data.Repositories.Definitions.Cadastro
{
    public interface IEnderecoRepository : IRepository<Endereco>
    {
    }
}

﻿using HealthCare.Domain.Entities.Cadastro;
using HealthCare.Infra.Data.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Data.Repositories.Definitions.Cadastro
{
    public interface 
        IProfissionalRepository : IRepository<Profissional>
    {
        Profissional GetProfissionalComServicos(Guid id);
    }
}

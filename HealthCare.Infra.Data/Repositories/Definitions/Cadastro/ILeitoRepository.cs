﻿using HealthCare.Domain.Entities;
using HealthCare.Domain.Entities.Cadastro;
using HealthCare.Infra.Data.Repositories.Base;
using System.Collections.Generic;

namespace HealthCare.Data.Repositories.Definitions.Cadastro
{
    public interface ILeitoRepository : IRepository<Leito>
    {
        IList<Leito> ListarLeitosComMovimentacao();
    }
}
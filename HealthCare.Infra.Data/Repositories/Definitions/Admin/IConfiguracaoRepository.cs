﻿using HealthCare.Domain.Entities.DbClasses.Admin;
using HealthCare.Infra.Data.Repositories.Base;

namespace HealthCare.Data.Repositories.Definitions.Admin
{
    public interface IConfiguracaoRepository : IRepository<Configuracao>
    {
    }
}

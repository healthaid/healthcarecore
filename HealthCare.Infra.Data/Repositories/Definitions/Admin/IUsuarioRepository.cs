﻿using HealthCare.Domain.Entities.Admin;
using HealthCare.Infra.Data.Repositories.Base;
using Microsoft.AspNet.Identity.EntityFramework;
using System;

namespace HealthCare.Infra.Data.Repositories.Definitions.Admin
{
    public interface IUsuarioRepository : IRepository<Usuario>
    {
        //UserStore<Usuario> GetUserStore();
        RoleStore<IdentityRole> GetRoleStore();

        Usuario GetUserWithRoles(Guid id);
    }
}

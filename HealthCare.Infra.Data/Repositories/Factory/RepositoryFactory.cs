﻿

using System;
using HealthCare.Data.Context;
using HealthCare.Infra.Data.Repositories.Base;
using HealthCare.Infra.Data.Repositories.Definitions.Admin;

namespace HealthCare.Infra.Data.Repositories.Factory
{
    public partial class RepositoryFactory : IRepositoryFactory
    {
        private readonly HealthCareDbContext  _healthCareDbContext;

        public RepositoryFactory(HealthCareDbContext context)
        {
            _healthCareDbContext = context;
        }

       
        public void Save()
        {
            _healthCareDbContext.SaveChanges();
        }
    }
}

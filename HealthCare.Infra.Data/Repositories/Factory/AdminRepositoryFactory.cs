﻿
using HealthCare.Data.Repositories.Definitions.Admin;
using HealthCare.Data.Repositories.Definitions.Agenda;
using HealthCare.Data.Repositories.Definitions.Cadastro;
using HealthCare.Data.Repositories.Implementations.Admin;
using HealthCare.Data.Repositories.Implementations.Agenda;
using HealthCare.Data.Repositories.Implementations.Cadastro;
using HealthCare.Infra.Data.Repositories.Base;
using HealthCare.Infra.Data.Repositories.Definitions.Admin;
using HealthCare.Infra.Data.Repositories.Implementations.Admin;
namespace HealthCare.Infra.Data.Repositories.Factory
{
    public partial class RepositoryFactory : IRepositoryFactory
    {
        private IUsuarioRepository _usuarioRepository;
        private IConfiguracaoRepository _configuracaoRepository;
        private IPacienteRepository _pacienteRepository;
        private IAgendaRepository _agendaRepository;

        private IEnderecoRepository _enderecoRepository;
        private IBairroRepository _bairroRepository;


        #region Factory

        public IConfiguracaoRepository Configuracao
        {
            get { return _configuracaoRepository ?? (_configuracaoRepository = new ConfiguracaoRepository(_healthCareDbContext)); }
        }

        public IUsuarioRepository Usuario
        {
            get { return _usuarioRepository ?? (_usuarioRepository = new UsuarioRepository(_healthCareDbContext)); }
        }

        public IPacienteRepository Paciente
        {
            get { return _pacienteRepository ?? (_pacienteRepository = new PacienteRepository(_healthCareDbContext)); }
        }

        public IAgendaRepository Agenda
        {
            get { return _agendaRepository ?? (_agendaRepository = new AgendaRepository(_healthCareDbContext)); }
        }

        public IEnderecoRepository Endereco
        {
            get { return _enderecoRepository ?? (_enderecoRepository = new EnderecoRepository(_healthCareDbContext)); }
        }

        public IBairroRepository Bairro
        {
            get { return _bairroRepository ?? (_bairroRepository = new BairroRepository(_healthCareDbContext)); }
        }

        #endregion
    }
}

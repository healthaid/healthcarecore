﻿using HealthCare.Infra.Data.Context;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;

namespace HealthCare.Data.Messaging.Implementation
{
    public class MessageReceiver : IMessageReceiver
    {
        private readonly IDesignTimeDbContextFactory<HealthCareDbContext> connectionFactory;
        private readonly string name;
        private readonly string readQuery;
        private readonly string deleteQuery;
        private readonly TimeSpan pollDelay;
        private readonly object lockObject = new object();
        private CancellationTokenSource cancellationSource;

        public MessageReceiver(IDesignTimeDbContextFactory<HealthCareDbContext> connectionFactory, string name, string tableName)
            : this(connectionFactory, name, tableName, TimeSpan.FromMilliseconds(100))
        {
        }

        public MessageReceiver(IDesignTimeDbContextFactory<HealthCareDbContext> connectionFactory, string name, string tableName, TimeSpan pollDelay)
        {
            this.connectionFactory = connectionFactory;
            this.name = name;
            this.pollDelay = pollDelay;

            this.readQuery =
                string.Format(
                    CultureInfo.InvariantCulture,
                    @"SELECT TOP (1) 
                    {0}.[Id] AS [Id], 
                    {0}.[Body] AS [Body], 
                    {0}.[DeliveryDate] AS [DeliveryDate],
                    {0}.[CorrelationId] AS [CorrelationId]
                    FROM {0} WITH (UPDLOCK, READPAST)
                    WHERE ({0}.[DeliveryDate] IS NULL) OR ({0}.[DeliveryDate] <= @CurrentDate)
                    ORDER BY {0}.[Id] ASC",
                    tableName);
            this.deleteQuery =
                string.Format(
                   CultureInfo.InvariantCulture,
                   "DELETE FROM {0} WHERE Id = @Id",
                   tableName);
        }

        public event EventHandler<MessageReceivedEventArgs> MessageReceived = (sender, args) => { };

        public void Start()
        {
            lock (this.lockObject)
            {
                if (this.cancellationSource == null)
                {
                    this.cancellationSource = new CancellationTokenSource();
                    Task.Factory.StartNew(
                        () => this.ReceiveMessages(this.cancellationSource.Token),
                        this.cancellationSource.Token,
                        TaskCreationOptions.LongRunning,
                        TaskScheduler.Current);
                }
            }
        }

        public void Stop()
        {
            lock (this.lockObject)
            {
                using (this.cancellationSource)
                {
                    if (this.cancellationSource != null)
                    {
                        this.cancellationSource.Cancel();
                        this.cancellationSource = null;
                    }
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            this.Stop();
        }

        ~MessageReceiver()
        {
            Dispose(false);
        }

        /// <summary>
        /// Receives the messages in an endless loop.
        /// </summary>
        private void ReceiveMessages(CancellationToken cancellationToken)
        {
            while (!cancellationToken.IsCancellationRequested)
            {
                if (!this.ReceiveMessage())
                {
                    Thread.Sleep(this.pollDelay);
                }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Justification = "Does not contain user input.")]
        protected bool ReceiveMessage()
        {
            //using (var connection = this.connectionFactory.Create())
            //{
            //    var currentDate = GetCurrentDate();

            //    connection.Open();
            //    using (var transaction = connection.BeginTransaction(IsolationLevel.ReadCommitted))
            //    {
            //        try
            //        {
            //            long messageId = -1;
            //            Message message = null;

            //            using (var command = connection.CreateCommand())
            //            {
            //                command.Transaction = transaction;
            //                command.CommandType = CommandType.Text;
            //                command.CommandText = this.readQuery;
            //                ((SqlCommand)command).Parameters.Add("@CurrentDate", SqlDbType.DateTime).Value = currentDate;

            //                using (var reader = command.ExecuteReader())
            //                {
            //                    if (!reader.Read())
            //                    {
            //                        return false;
            //                    }

            //                    var body = (string)reader["Body"];
            //                    var deliveryDateValue = reader["DeliveryDate"];
            //                    var deliveryDate = deliveryDateValue == DBNull.Value ? (DateTime?)null : new DateTime?((DateTime)deliveryDateValue);
            //                    var correlationIdValue = reader["CorrelationId"];
            //                    var correlationId = (string)(correlationIdValue == DBNull.Value ? null : correlationIdValue);

            //                    message = new Message(body, deliveryDate, correlationId);
            //                    messageId = (long)reader["Id"];
            //                }
            //            }

            //            this.MessageReceived(this, new MessageReceivedEventArgs(message));

            //            using (var command = connection.CreateCommand())
            //            {
            //                command.Transaction = transaction;
            //                command.CommandType = CommandType.Text;
            //                command.CommandText = this.deleteQuery;
            //                ((SqlCommand)command).Parameters.Add("@Id", SqlDbType.BigInt).Value = messageId;

            //                command.ExecuteNonQuery();
            //            }

            //            transaction.Commit();
            //        }
            //        catch (Exception)
            //        {
            //            try
            //            {
            //                transaction.Rollback();
            //            }
            //            catch
            //            {
            //            }
            //            throw;
            //        }
            //    }
            //}


            return true;
        }

        protected virtual DateTime GetCurrentDate()
        {
            return DateTime.UtcNow;
        }

    }
}

﻿using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.Data.Context;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace HealthCare.Data.Messaging.Implementation
{

    //public class TestConnectionFactory : DbConnection
    //{

    //    public DbConnection CreateConnection(string nameOrConnectionString)
    //    {

    //        if (!string.IsNullOrEmpty(nameOrConnectionString))
    //            return new SqlConnection(nameOrConnectionString);
    //        else
    //            throw new ArgumentNullException("ConnectionString");
    //    }
    //}
    public class MessageSender : IMessageSender
    {
        private readonly string name;
        private readonly string insertQuery;
        private string connectionString;
        private readonly DbConnection connectionFactory;

        public MessageSender(string name, string tableName, string connectionString)
        {
            this.name = name;
            this.connectionString = connectionString;
            this.insertQuery = string.Format("INSERT INTO {0} (Id, Body, DeliveryDate, CorrelationId, RowVersion, UsuarioId) VALUES (@Id, @Body, @DeliveryDate, @CorrelationId ,@RowVersion, @UsuarioId)", tableName);
        }

        /// <summary>
        /// Sends the specified message.
        /// </summary>
        public void Send(Message message)
        {
            try
            {
                using (var connection = new MySqlConnection(this.connectionString))
                {
                    connection.Open();

                    InsertMessage(message, connection);

                    connection.Close();
                }
            }
            catch (Exception ex)
            {

                throw new Exception();
            }

        }

        /// <summary>
        /// Sends a batch of messages.
        /// </summary>
        public void Send(IEnumerable<Message> messages)
        {
            using (var scope = connectionFactory.BeginTransaction())
            {
                foreach (var message in messages)
                {
                    this.InsertMessage(message, connectionFactory);
                }

                scope.Commit();
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Justification = "Does not contain user input.")]
        private void InsertMessage(Message message, DbConnection connection)
        {

            using (var command = (MySqlCommand)connection.CreateCommand())
            {
                command.CommandText = this.insertQuery;
                command.CommandType = CommandType.Text;

                command.Parameters.Add("@Id", DbType.String).Value = message.Id;
                command.Parameters.Add("@Body", DbType.String).Value = message.Body;
                command.Parameters.Add("@DeliveryDate", DbType.DateTime).Value = message.DeliveryDate.HasValue ? (object)message.DeliveryDate.Value : DateTime.Now;
                command.Parameters.Add("@CorrelationId", DbType.String).Value = (object)message.CorrelationId ?? DBNull.Value;
                command.Parameters.Add("@RowVersion", DbType.Binary).Value = BitConverter.GetBytes(DateTime.Now.Date.Ticks);
                command.Parameters.Add("@UsuarioId",DbType.String).Value = message.UsuarioId;

                command.ExecuteNonQuery();
            }
        }

        public Validation SendWithReturn(Message message)
        {
            Validation v = new Validation();
            //try
            //{
            //    using (var connection = this.connectionFactory.CreateConnection())
            //    {
            //        connection.Open();

            //        InsertMessage(message, connection);

            //    }
            //}
            //catch (Exception ex)
            //{
            //    v.AddValidationMessage(new ValidationMessage(ex.Message));
            //    v.AddValidationMessage(new ValidationMessage(ex.InnerException.Message));
            //}
            return v;
        }
    }
}

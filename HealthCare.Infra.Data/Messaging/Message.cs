﻿using System;

namespace HealthCare.Data.Messaging
{
    public class Message
    {
        public Message(string body, string usuarioId = null, string messageId = null, DateTime? deliveryDate = null, string correlationId = null)
        {
            this.Body = body;
            this.DeliveryDate = deliveryDate;
            this.CorrelationId = correlationId;
            this.Id = messageId;
            this.UsuarioId = usuarioId;
        }

        public string Id { get; private set; }
        
        public string Body { get; private set; }

        public string CorrelationId { get; private set; }

        public DateTime? DeliveryDate { get; private set; }

        public string UsuarioId { get; private set; }
    }
}

﻿using HealthCare.Infra.CQRS.Messaging;
using System.Collections.Generic;

namespace HealthCare.Data.Messaging
{
    public interface IMessageSender
    {
        /// <summary>
        /// Sends the specified message.
        /// </summary>
        void Send(Message message);

        /// <summary>
        /// Sends a batch of messages.
        /// </summary>
        void Send(IEnumerable<Message> messages);

        /// <summary>
        /// Send Message with validation and return
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        Validation SendWithReturn(Message message);
    }
}

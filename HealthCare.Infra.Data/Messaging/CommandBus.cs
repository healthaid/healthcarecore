﻿using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.CQRS.Messaging.Handling;
using HealthCare.Infra.CQRS.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace HealthCare.Data.Messaging
{
    /// <summary>
    /// Classe que implementa a interface do bus
    /// </summary>
    public class CommandBus : ICommandBus
    {
        private readonly IMessageSender sender;
        private readonly ITextSerializer serializer;

        /// <summary>
        /// Inicializa uma nova instancia de <see cref="CommandBus"/> class.
        /// </summary>
        /// <param name="serializer">The serializer to use for the message body.</param>
        public CommandBus(IMessageSender sender, ITextSerializer serializer)
        {
            this.sender = sender;
            this.serializer = serializer;
        }

        public CommandBus() { }

        /// <summary>
        ///Envia um comando 
        /// </summary>
        public void Send(Envelope<ICommand> command)
        {
            var message = BuildMessage(command);

            this.sender.Send(message);
        }

        /// <summary>
        /// Envia uma lista de comandos
        /// </summary>
        /// <param name="commands">Lista de comandos</param>
        public void Send(IEnumerable<Envelope<ICommand>> commands)
        {
            var messages = commands.Select(command => BuildMessage(command));

            this.sender.Send(messages);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        private Message BuildMessage(Envelope<ICommand> command)
        {
            // TODO: should use the Command ID as a unique constraint when storing it.
            using (var payloadWriter = new StringWriter())
            {
                this.serializer.Serialize(payloadWriter, command.Body);
                return new Message(payloadWriter.ToString(), command.Body.CommandUsuarioId.ToString(), command.Body.Id.ToString(), command.Delay != TimeSpan.Zero ? (DateTime?)DateTime.UtcNow.Add(command.Delay) : null, command.CorrelationId);
            }
        }

        /// <summary>
        /// Envia o comando esperando um retorno 
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public object SendWithReturn(Envelope<ICommand> command)
        {
            var message = BuildMessage(command);
            return this.sender.SendWithReturn(message);
        }
    }
}

﻿namespace HealthCare.Data.Messaging
{
    public class MessageReceivedEventArgs
    {
        public MessageReceivedEventArgs(Message message)
        {
            this.Message = message;
        }

        public Message Message { get; private set; }
    }
}

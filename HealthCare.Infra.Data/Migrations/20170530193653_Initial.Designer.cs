﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using HealthCare.Infra.Data.Context;
using HealthCare.Domain.Entities.DbClasses.Cadastro;

namespace HealthCare.Infra.Data.Migrations
{
    [DbContext(typeof(HealthCareDbContext))]
    [Migration("20170530193653_Initial")]
    partial class Initial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.1");

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Admin.Assinatura", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<byte[]>("Imagem");

                    b.Property<byte[]>("RowVersion")
                        .IsConcurrencyToken();

                    b.Property<Guid?>("UsuarioId");

                    b.HasKey("Id");

                    b.HasIndex("UsuarioId");

                    b.ToTable("Assinatura");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Admin.Commands", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Body")
                        .HasMaxLength(4000);

                    b.Property<string>("CorrelationId")
                        .HasMaxLength(1000);

                    b.Property<DateTime?>("DeliveryDate");

                    b.Property<byte[]>("RowVersion")
                        .IsConcurrencyToken();

                    b.HasKey("Id");

                    b.ToTable("Commands");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Admin.Event", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("AggregateType")
                        .HasMaxLength(1000);

                    b.Property<string>("CorrelationId")
                        .HasMaxLength(1000);

                    b.Property<string>("Payload")
                        .HasMaxLength(1000);

                    b.Property<int>("Version");

                    b.HasKey("Id");

                    b.ToTable("Event");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Admin.Role", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken()
                        .HasMaxLength(1000);

                    b.Property<string>("Name")
                        .HasMaxLength(1000);

                    b.Property<string>("NormalizedName")
                        .HasMaxLength(250)
                        .IsUnicode(false);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .IsUnique()
                        .HasName("RoleNameIndex");

                    b.ToTable("AspNetRoles");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Admin.RoleClaim", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType")
                        .HasMaxLength(1000);

                    b.Property<string>("ClaimValue")
                        .HasMaxLength(1000);

                    b.Property<Guid>("RoleId")
                        .ValueGeneratedOnAdd();

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Admin.UserClaim", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType")
                        .HasMaxLength(1000);

                    b.Property<string>("ClaimValue")
                        .HasMaxLength(1000);

                    b.Property<Guid>("UserId")
                        .ValueGeneratedOnAdd();

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Admin.UserLogin", b =>
                {
                    b.Property<Guid>("UserId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("LoginProvider")
                        .HasMaxLength(196)
                        .IsUnicode(false);

                    b.Property<string>("ProviderKey")
                        .HasMaxLength(196)
                        .IsUnicode(false);

                    b.Property<string>("ProviderDisplayName")
                        .HasMaxLength(1000);

                    b.HasKey("UserId", "LoginProvider", "ProviderKey");

                    b.HasAlternateKey("LoginProvider", "ProviderKey");

                    b.ToTable("AspNetUserLogins");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Admin.UserRole", b =>
                {
                    b.Property<Guid>("RoleId");

                    b.Property<Guid>("UserId")
                        .ValueGeneratedOnAdd();

                    b.HasKey("RoleId", "UserId");

                    b.HasAlternateKey("RoleId");


                    b.HasAlternateKey("UserId", "RoleId");

                    b.ToTable("AspNetUserRoles");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Admin.UserToken", b =>
                {
                    b.Property<Guid>("UserId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("LoginProvider")
                        .HasMaxLength(196)
                        .IsUnicode(false);

                    b.Property<string>("Name")
                        .HasMaxLength(250)
                        .IsUnicode(false);

                    b.Property<string>("Value")
                        .HasMaxLength(1000);

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Admin.Usuario", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AccessFailedCount");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken()
                        .HasMaxLength(1000);

                    b.Property<string>("ConfirmacaoSenha")
                        .HasMaxLength(1000);

                    b.Property<DateTime?>("DataCadastro");

                    b.Property<string>("Documento")
                        .HasMaxLength(1000);

                    b.Property<string>("Email")
                        .HasMaxLength(250)
                        .IsUnicode(false);

                    b.Property<bool>("EmailConfirmed");

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("Nome")
                        .HasMaxLength(1000);

                    b.Property<string>("NormalizedEmail")
                        .HasMaxLength(250)
                        .IsUnicode(false);

                    b.Property<string>("NormalizedUserName")
                        .HasMaxLength(250)
                        .IsUnicode(false);

                    b.Property<string>("PasswordHash")
                        .HasMaxLength(1000);

                    b.Property<string>("PhoneNumber")
                        .HasMaxLength(1000);

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<bool>("PrimeiroLogin");

                    b.Property<Guid?>("ProfissionalId");

                    b.Property<string>("SecurityStamp")
                        .HasMaxLength(1000);

                    b.Property<string>("Senha")
                        .HasMaxLength(1000);

                    b.Property<string>("Sobrenome")
                        .HasMaxLength(1000);

                    b.Property<string>("TipoDocumento")
                        .HasMaxLength(1000);

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<string>("UserName")
                        .HasMaxLength(196)
                        .IsUnicode(false);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasName("UserNameIndex");

                    b.HasIndex("ProfissionalId");

                    b.ToTable("AspNetUsers");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.Acolhimento", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CodigoBoletim")
                        .HasMaxLength(1000);

                    b.Property<DateTime>("DataHora");

                    b.Property<Guid?>("PacienteId");

                    b.Property<bool>("Prioridade");

                    b.Property<Guid?>("ProfissionalId");

                    b.Property<byte[]>("RowVersion")
                        .IsConcurrencyToken();

                    b.Property<Guid?>("ServicoId");

                    b.Property<int>("Status");

                    b.HasKey("Id");

                    b.HasIndex("PacienteId");

                    b.HasIndex("ProfissionalId");

                    b.HasIndex("ServicoId");

                    b.ToTable("Acolhimento");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.AdministracaoMedicamento", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Nome")
                        .HasMaxLength(1000);

                    b.Property<byte[]>("RowVersion")
                        .IsConcurrencyToken();

                    b.HasKey("Id");

                    b.ToTable("AdministracaoMedicamento");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.Anamnese", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid?>("AtendimentoId");

                    b.Property<Guid?>("Ciap2Id");

                    b.Property<string>("Descricao")
                        .HasMaxLength(1000);

                    b.Property<string>("QueixaPrincipal")
                        .HasMaxLength(1000);

                    b.Property<byte[]>("RowVersion")
                        .IsConcurrencyToken();

                    b.HasKey("Id");

                    b.HasIndex("AtendimentoId");

                    b.HasIndex("Ciap2Id");

                    b.ToTable("Anamnese");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.Atendimento", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid?>("AcolhimentoId");

                    b.Property<Guid?>("CheckoutId");

                    b.Property<string>("Discriminator")
                        .IsRequired()
                        .HasMaxLength(1000);

                    b.Property<string>("ExameFisico")
                        .HasMaxLength(1000);

                    b.Property<DateTime?>("FimAtendimento");

                    b.Property<DateTime>("InicioAtendimento");

                    b.Property<Guid?>("MotivoCheckoutId");

                    b.Property<Guid?>("PrimeiroDiagnosticoId");

                    b.Property<Guid?>("ProfissionalId");

                    b.Property<byte[]>("RowVersion")
                        .IsConcurrencyToken();

                    b.Property<Guid?>("SegundoDiagnosticoId");

                    b.HasKey("Id");

                    b.HasIndex("AcolhimentoId");

                    b.HasIndex("CheckoutId");

                    b.HasIndex("MotivoCheckoutId");

                    b.HasIndex("PrimeiroDiagnosticoId");

                    b.HasIndex("ProfissionalId");

                    b.HasIndex("SegundoDiagnosticoId");

                    b.ToTable("Atendimento");

                    b.HasDiscriminator<string>("Discriminator").HasValue("Atendimento");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.AtendimentoProcedimento", b =>
                {
                    b.Property<Guid>("AtendimentoId");

                    b.Property<Guid>("ProcedimentoId")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid?>("AtendimentoId1");

                    b.HasKey("AtendimentoId", "ProcedimentoId");

                    b.HasAlternateKey("AtendimentoId");

                    b.HasIndex("AtendimentoId1");

                    b.ToTable("AtendimentoProcedimento");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.Atestados", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid?>("AtendimentoId");

                    b.Property<Guid?>("CIDId");

                    b.Property<string>("Descricao")
                        .HasMaxLength(1000);

                    b.Property<string>("Dias")
                        .HasMaxLength(1000);

                    b.Property<byte[]>("RowVersion")
                        .IsConcurrencyToken();

                    b.HasKey("Id");

                    b.HasIndex("AtendimentoId");

                    b.HasIndex("CIDId");

                    b.ToTable("Atestados");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.Checkout", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("DiagnosticoDois")
                        .HasMaxLength(1000);

                    b.Property<string>("DiagnosticoUm")
                        .HasMaxLength(1000);

                    b.Property<Guid?>("MotivoCheckoutId");

                    b.Property<string>("Observacao")
                        .HasMaxLength(1000);

                    b.Property<byte[]>("RowVersion")
                        .IsConcurrencyToken();

                    b.HasKey("Id");

                    b.HasIndex("MotivoCheckoutId");

                    b.ToTable("Checkout");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.Ciap2", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Categoria")
                        .HasMaxLength(1000);

                    b.Property<string>("Codigo")
                        .HasMaxLength(1000);

                    b.Property<string>("Componente")
                        .HasMaxLength(1000);

                    b.Property<string>("Descricao")
                        .HasMaxLength(1000);

                    b.Property<string>("Grupo")
                        .HasMaxLength(1000);

                    b.Property<bool>("Procedimento");

                    b.Property<byte[]>("RowVersion")
                        .IsConcurrencyToken();

                    b.HasKey("Id");

                    b.ToTable("Ciap2");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.Ciap2_Cid10", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CiapCodigo")
                        .HasMaxLength(1000);

                    b.Property<string>("Cid")
                        .HasMaxLength(1000);

                    b.Property<string>("CidFrequente")
                        .HasMaxLength(1000);

                    b.Property<byte[]>("RowVersion")
                        .IsConcurrencyToken();

                    b.HasKey("Id");

                    b.ToTable("Ciap2_Cid10");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.CID", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Agravo")
                        .HasMaxLength(1000);

                    b.Property<string>("CamposIrradiados")
                        .HasMaxLength(1000);

                    b.Property<string>("Codigo")
                        .HasMaxLength(1000);

                    b.Property<string>("Descricao")
                        .HasMaxLength(1000);

                    b.Property<string>("Estadio")
                        .HasMaxLength(1000);

                    b.Property<Guid?>("ProcedimentoId");

                    b.Property<byte[]>("RowVersion")
                        .IsConcurrencyToken();

                    b.Property<string>("Sexo")
                        .HasMaxLength(1000);

                    b.HasKey("Id");

                    b.HasIndex("ProcedimentoId");

                    b.ToTable("CID");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.ClassificacaoDeRisco", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid?>("AcolhimentoId");

                    b.Property<string>("Alergia")
                        .HasMaxLength(1000);

                    b.Property<string>("CausaExterna")
                        .HasMaxLength(1000);

                    b.Property<string>("DoencasPreexistentes")
                        .HasMaxLength(1000);

                    b.Property<string>("EspecialidadeMedica")
                        .HasMaxLength(1000);

                    b.Property<string>("MedicamentosEmUso")
                        .HasMaxLength(1000);

                    b.Property<string>("NivelDeConsciencia")
                        .HasMaxLength(1000);

                    b.Property<string>("Observacao")
                        .HasMaxLength(1000);

                    b.Property<string>("Queixa")
                        .HasMaxLength(1000);

                    b.Property<Guid?>("RiscoId");

                    b.Property<byte[]>("RowVersion")
                        .IsConcurrencyToken();

                    b.Property<Guid?>("SinaisVitaisId");

                    b.HasKey("Id");

                    b.HasIndex("AcolhimentoId");

                    b.HasIndex("RiscoId");

                    b.HasIndex("SinaisVitaisId");

                    b.ToTable("ClassificacaoDeRisco");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.ElementoExame", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Descricao")
                        .HasMaxLength(1000);

                    b.Property<Guid?>("ExameId");

                    b.Property<byte[]>("RowVersion")
                        .IsConcurrencyToken();

                    b.HasKey("Id");

                    b.HasIndex("ExameId");

                    b.ToTable("ElementoExame");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.Evolucao", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid?>("AtemdimentoId");

                    b.Property<DateTime>("DataHoraEvolucao");

                    b.Property<string>("Descricao")
                        .HasMaxLength(1000);

                    b.Property<int>("EvolucaoTipo");

                    b.Property<Guid?>("MedicoId");

                    b.Property<Guid?>("PrimeiroDiagnosticoId");

                    b.Property<Guid>("ProcedimentoId")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("Quantidade");

                    b.Property<byte[]>("RowVersion")
                        .IsConcurrencyToken();

                    b.Property<Guid?>("SegundoDiagnosticoId");

                    b.HasKey("Id");

                    b.HasIndex("AtemdimentoId");

                    b.HasIndex("MedicoId");

                    b.HasIndex("PrimeiroDiagnosticoId");

                    b.HasIndex("SegundoDiagnosticoId");

                    b.ToTable("Evolucao");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.EvolucaoEnfermagem", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Evolucao")
                        .HasMaxLength(1000);

                    b.Property<string>("FrequenciaRespitaroria")
                        .HasMaxLength(1000);

                    b.Property<DateTime>("HoraEvolucao");

                    b.Property<Guid?>("MovimentacaoLeitoId");

                    b.Property<string>("NumAtendimento")
                        .HasMaxLength(1000);

                    b.Property<string>("ObservacaoSinaisVitais")
                        .HasMaxLength(1000);

                    b.Property<string>("Peso")
                        .HasMaxLength(1000);

                    b.Property<string>("Pressao")
                        .HasMaxLength(1000);

                    b.Property<Guid?>("ProfissionalId");

                    b.Property<string>("Pulso")
                        .HasMaxLength(1000);

                    b.Property<byte[]>("RowVersion")
                        .IsConcurrencyToken();

                    b.Property<string>("Temperatura")
                        .HasMaxLength(1000);

                    b.HasKey("Id");

                    b.HasIndex("MovimentacaoLeitoId");

                    b.HasIndex("ProfissionalId");

                    b.ToTable("EvolucaoEnfermagem");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.EvolucaoProcedimento", b =>
                {
                    b.Property<Guid>("ProcedimentoId")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid>("EvolucaoId")
                        .ValueGeneratedOnAdd();

                    b.HasKey("ProcedimentoId", "EvolucaoId");

                    b.HasIndex("EvolucaoId");

                    b.ToTable("EvolucaoProcedimento");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.Exame", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Classificacao")
                        .HasMaxLength(1000);

                    b.Property<string>("Descricao")
                        .HasMaxLength(1000);

                    b.Property<Guid?>("ExamePaiId");

                    b.Property<string>("Procedimento")
                        .HasMaxLength(1000);

                    b.Property<string>("Sexo")
                        .HasMaxLength(1000);

                    b.Property<Guid?>("TipoExameId");

                    b.HasKey("Id");

                    b.HasIndex("ExamePaiId");

                    b.HasIndex("TipoExameId");

                    b.ToTable("Exame");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.ExameAtendimento", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid?>("AtendimentoId");

                    b.Property<DateTime?>("DataExecucao");

                    b.Property<DateTime>("DataSolicitacao");

                    b.Property<string>("DefinicaoExame")
                        .HasMaxLength(1000);

                    b.Property<Guid?>("ExameId");

                    b.Property<Guid?>("ExecutorId");

                    b.Property<Guid?>("MedicoId");

                    b.Property<string>("ObservacaoExecutor")
                        .HasMaxLength(1000);

                    b.Property<string>("ObservacaoMedico")
                        .HasMaxLength(1000);

                    b.Property<string>("Procedimento")
                        .HasMaxLength(1000);

                    b.Property<bool>("Realizado");

                    b.HasKey("Id");

                    b.HasIndex("AtendimentoId");

                    b.HasIndex("ExameId");

                    b.HasIndex("ExecutorId");

                    b.HasIndex("MedicoId");

                    b.ToTable("ExameAtendimento");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.HipoteseDiagnostica", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid?>("AtendimentoId");

                    b.Property<string>("Observacao")
                        .HasMaxLength(1000);

                    b.Property<byte[]>("RowVersion")
                        .IsConcurrencyToken();

                    b.HasKey("Id");

                    b.HasIndex("AtendimentoId");

                    b.ToTable("HipoteseDiagnostica");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.HipoteseDiagnosticaCid", b =>
                {
                    b.Property<Guid>("CidId")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid>("HipoteseDiagnosticaId")
                        .ValueGeneratedOnAdd();

                    b.HasKey("CidId", "HipoteseDiagnosticaId");

                    b.HasIndex("HipoteseDiagnosticaId");

                    b.ToTable("HipoteseDiagnosticaCid");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.Imagem", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<byte[]>("Image");

                    b.Property<Guid?>("ImagemAtendimentoId");

                    b.Property<byte[]>("RowVersion")
                        .IsConcurrencyToken();

                    b.HasKey("Id");

                    b.HasIndex("ImagemAtendimentoId");

                    b.ToTable("Imagem");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.ImagemAtendimento", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid?>("AtendimentoId");

                    b.Property<string>("Observacao")
                        .HasMaxLength(1000);

                    b.Property<byte[]>("RowVersion")
                        .IsConcurrencyToken();

                    b.HasKey("Id");

                    b.HasIndex("AtendimentoId");

                    b.ToTable("ImagemAtendimento");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.Internacao", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid?>("AtendimentoId");

                    b.Property<DateTime>("DataInternacao");

                    b.HasKey("Id");

                    b.HasIndex("AtendimentoId");

                    b.ToTable("Internacao");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.Laudo", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid?>("ExameId");

                    b.Property<byte[]>("RowVersion")
                        .IsConcurrencyToken();

                    b.Property<Guid?>("UsuarioId");

                    b.HasKey("Id");

                    b.HasIndex("ExameId");

                    b.HasIndex("UsuarioId");

                    b.ToTable("Laudo");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.LiquidosAdministrados", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Descricao")
                        .HasMaxLength(1000);

                    b.Property<byte[]>("RowVersion")
                        .IsConcurrencyToken();

                    b.HasKey("Id");

                    b.ToTable("LiquidosAdministrados");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.LiquidosAdministradosEnfermagem", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid?>("LiquidosAdministradosId");

                    b.Property<byte[]>("RowVersion")
                        .IsConcurrencyToken();

                    b.Property<Guid?>("evolucaoEnfermagemId");

                    b.HasKey("Id");

                    b.HasIndex("LiquidosAdministradosId");

                    b.HasIndex("evolucaoEnfermagemId");

                    b.ToTable("LiquidosAdministradosEnfermagem");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.LiquidosEliminados", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Descricao")
                        .HasMaxLength(1000);

                    b.Property<byte[]>("RowVersion")
                        .IsConcurrencyToken();

                    b.HasKey("Id");

                    b.ToTable("LiquidosEliminados");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.LiquidosEliminadosEnfermagem", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid?>("LiquidosEliminadosId");

                    b.Property<byte[]>("RowVersion")
                        .IsConcurrencyToken();

                    b.Property<Guid?>("evolucaoEnfermagemId");

                    b.HasKey("Id");

                    b.HasIndex("LiquidosEliminadosId");

                    b.HasIndex("evolucaoEnfermagemId");

                    b.ToTable("LiquidosEliminadosEnfermagem");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.Medicamento", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ATC")
                        .HasMaxLength(1000);

                    b.Property<string>("DDD")
                        .HasMaxLength(1000);

                    b.Property<string>("FormaFarmaceutica")
                        .HasMaxLength(1000);

                    b.Property<string>("Generico")
                        .HasMaxLength(1000);

                    b.Property<string>("Nome")
                        .HasMaxLength(1000);

                    b.Property<byte[]>("RowVersion")
                        .IsConcurrencyToken();

                    b.HasKey("Id");

                    b.ToTable("Medicamento");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.MotivoCheckout", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("Ativo");

                    b.Property<string>("Descricao")
                        .HasMaxLength(1000);

                    b.Property<byte[]>("RowVersion")
                        .IsConcurrencyToken();

                    b.Property<Guid?>("TipoId");

                    b.HasKey("Id");

                    b.HasIndex("TipoId");

                    b.ToTable("MotivoCheckout");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.MovimentacaoLeito", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid?>("ClinicaAtual1Id");

                    b.Property<Guid?>("ClinicaId");

                    b.Property<DateTime>("DataMovimentacao");

                    b.Property<Guid?>("InternacaoId");

                    b.Property<Guid?>("LeitoAtualId");

                    b.Property<Guid?>("UltimaMovimentacaoId");

                    b.HasKey("Id");

                    b.HasIndex("ClinicaAtual1Id");

                    b.HasIndex("ClinicaId");

                    b.HasIndex("InternacaoId");

                    b.HasIndex("LeitoAtualId");

                    b.HasIndex("UltimaMovimentacaoId");

                    b.ToTable("MovimentacaoLeito");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.Prescricao", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid?>("AtendimentoId");

                    b.Property<string>("Dose")
                        .HasMaxLength(1000);

                    b.Property<string>("Duracao")
                        .HasMaxLength(1000);

                    b.Property<Guid?>("EnfermeiroId");

                    b.Property<DateTime>("EnvioEnfermagem");

                    b.Property<string>("Intervalo")
                        .HasMaxLength(1000);

                    b.Property<string>("Medicamento")
                        .HasMaxLength(1000);

                    b.Property<Guid?>("MedicoId");

                    b.Property<string>("ObservacaoEnfermagem")
                        .HasMaxLength(1000);

                    b.Property<string>("ObservacaoMedico")
                        .HasMaxLength(1000);

                    b.Property<DateTime?>("RetornoEnfermagem");

                    b.Property<byte[]>("RowVersion")
                        .IsConcurrencyToken();

                    b.Property<string>("StatusEnfermagem")
                        .HasMaxLength(1000);

                    b.Property<string>("Unidade")
                        .HasMaxLength(1000);

                    b.Property<string>("ViaAdministracao")
                        .HasMaxLength(1000);

                    b.HasKey("Id");

                    b.HasIndex("AtendimentoId");

                    b.HasIndex("EnfermeiroId");

                    b.HasIndex("MedicoId");

                    b.ToTable("Prescricao");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.Procedimento", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CO_FINANCIAMENTO")
                        .HasMaxLength(1000);

                    b.Property<string>("CO_PROCEDIMENTO")
                        .HasMaxLength(1000);

                    b.Property<string>("CO_RUBRICA")
                        .HasMaxLength(1000);

                    b.Property<string>("DT_COMPETENCIA")
                        .HasMaxLength(1000);

                    b.Property<string>("NO_PROCEDIMENTO")
                        .HasMaxLength(1000);

                    b.Property<int>("QT_DIAS_PERMANENCIA");

                    b.Property<int>("QT_MAXIMA_EXECUCAO");

                    b.Property<int>("QT_PONTOS");

                    b.Property<string>("QT_TEMPO_PERMANENCIA")
                        .HasMaxLength(1000);

                    b.Property<byte[]>("RowVersion")
                        .IsConcurrencyToken();

                    b.Property<string>("TP_COMPLEXIDADE")
                        .HasMaxLength(1000);

                    b.Property<string>("TP_SEXO")
                        .HasMaxLength(1000);

                    b.Property<int>("VL_IDADE_MAXIMA");

                    b.Property<int>("VL_IDADE_MINIMA");

                    b.Property<decimal>("VL_SA");

                    b.Property<decimal>("VL_SH");

                    b.Property<decimal>("VL_SP");

                    b.HasKey("Id");

                    b.ToTable("Procedimento");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.ProcedimentoCid", b =>
                {
                    b.Property<Guid>("CidId")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid>("ProcedimentoId")
                        .ValueGeneratedOnAdd();

                    b.HasKey("CidId", "ProcedimentoId");

                    b.HasIndex("ProcedimentoId");

                    b.ToTable("ProcedimentoCid");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.ProcedimentosEnfermagem", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid?>("EvolucaoId");

                    b.Property<string>("Observacao")
                        .HasMaxLength(1000);

                    b.Property<Guid?>("ProcedimentoId");

                    b.Property<int>("Quantidade");

                    b.Property<byte[]>("RowVersion")
                        .IsConcurrencyToken();

                    b.HasKey("Id");

                    b.HasIndex("EvolucaoId");

                    b.HasIndex("ProcedimentoId");

                    b.ToTable("ProcedimentosEnfermagem");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.ReceitaMedica", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid?>("AtendimentoId");

                    b.Property<string>("Dose")
                        .HasMaxLength(1000);

                    b.Property<string>("Duracao")
                        .HasMaxLength(1000);

                    b.Property<string>("Intervalo")
                        .HasMaxLength(1000);

                    b.Property<string>("Medicamento")
                        .HasMaxLength(1000);

                    b.Property<byte[]>("RowVersion")
                        .IsConcurrencyToken();

                    b.Property<string>("Unidade")
                        .HasMaxLength(1000);

                    b.Property<string>("ViaAdministracao")
                        .HasMaxLength(1000);

                    b.HasKey("Id");

                    b.HasIndex("AtendimentoId");

                    b.ToTable("ReceitaMedica");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.ResultadoExame", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid?>("AtendimentoId");

                    b.Property<Guid?>("ExameId");

                    b.Property<Guid?>("PacienteId");

                    b.Property<byte[]>("RowVersion")
                        .IsConcurrencyToken();

                    b.HasKey("Id");

                    b.HasIndex("AtendimentoId");

                    b.HasIndex("ExameId");

                    b.HasIndex("PacienteId");

                    b.ToTable("ResultadoExame");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.ResultadoExameElemento", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Descricao")
                        .HasMaxLength(1000);

                    b.Property<Guid?>("ElementoExameId");

                    b.Property<Guid?>("ResultadoExameId");

                    b.Property<byte[]>("RowVersion")
                        .IsConcurrencyToken();

                    b.HasKey("Id");

                    b.HasIndex("ElementoExameId");

                    b.HasIndex("ResultadoExameId");

                    b.ToTable("ResultadoExameElemento");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.Risco", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Descricao")
                        .HasMaxLength(1000);

                    b.Property<int>("Prioridade");

                    b.Property<byte[]>("RowVersion")
                        .IsConcurrencyToken();

                    b.HasKey("Id");

                    b.ToTable("Risco");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.SinaisVitais", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Altura")
                        .HasMaxLength(1000);

                    b.Property<string>("FrequenciaRespiratoria")
                        .HasMaxLength(1000);

                    b.Property<string>("GlicemiaCapilar")
                        .HasMaxLength(1000);

                    b.Property<string>("Peso")
                        .HasMaxLength(1000);

                    b.Property<string>("Pressao")
                        .HasMaxLength(1000);

                    b.Property<string>("Pulso")
                        .HasMaxLength(1000);

                    b.Property<byte[]>("RowVersion")
                        .IsConcurrencyToken();

                    b.Property<string>("Temperatura")
                        .HasMaxLength(1000);

                    b.HasKey("Id");

                    b.ToTable("SinaisVitais");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.TemplatesAtestado", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid?>("AtestadoId");

                    b.Property<string>("Conteudo")
                        .HasMaxLength(1000);

                    b.Property<string>("Nome")
                        .HasMaxLength(1000);

                    b.Property<byte[]>("RowVersion")
                        .IsConcurrencyToken();

                    b.HasKey("Id");

                    b.HasIndex("AtestadoId");

                    b.ToTable("TemplatesAtestado");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.TipoExame", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Descricao")
                        .HasMaxLength(1000);

                    b.Property<Guid?>("TipoExamePaiId");

                    b.HasKey("Id");

                    b.HasIndex("TipoExamePaiId");

                    b.ToTable("TipoExame");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.TipoMotivoChekout", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("Ativo");

                    b.Property<string>("Descricao")
                        .HasMaxLength(1000);

                    b.Property<byte[]>("RowVersion")
                        .IsConcurrencyToken();

                    b.HasKey("Id");

                    b.ToTable("TipoMotivoChekout");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.UnidadeMedida", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Nome")
                        .HasMaxLength(1000);

                    b.Property<byte[]>("RowVersion")
                        .IsConcurrencyToken();

                    b.HasKey("Id");

                    b.ToTable("UnidadeMedida");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Cadastro.Bairro", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("Ativo");

                    b.Property<Guid?>("CidadeId");

                    b.Property<string>("Nome")
                        .HasMaxLength(1000);

                    b.Property<byte[]>("RowVersion")
                        .IsConcurrencyToken();

                    b.HasKey("Id");

                    b.HasIndex("CidadeId");

                    b.ToTable("Bairro");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Cadastro.Cbo", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Codigo")
                        .HasMaxLength(1000);

                    b.Property<string>("Descricao")
                        .HasMaxLength(1000);

                    b.Property<byte[]>("RowVersion")
                        .IsConcurrencyToken();

                    b.HasKey("Id");

                    b.ToTable("Cbo");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Cadastro.Cidade", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("Ativo");

                    b.Property<string>("Nome")
                        .HasMaxLength(1000);

                    b.Property<byte[]>("RowVersion")
                        .IsConcurrencyToken();

                    b.Property<Guid?>("UFId");

                    b.HasKey("Id");

                    b.HasIndex("UFId");

                    b.ToTable("Cidade");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Cadastro.Clinica", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Nome")
                        .HasMaxLength(1000);

                    b.Property<byte[]>("RowVersion")
                        .IsConcurrencyToken();

                    b.HasKey("Id");

                    b.ToTable("Clinica");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Cadastro.Endereco", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Bairro")
                        .HasMaxLength(1000);

                    b.Property<string>("CEP")
                        .HasMaxLength(1000);

                    b.Property<string>("Cidade")
                        .HasMaxLength(1000);

                    b.Property<string>("Complemento")
                        .HasMaxLength(1000);

                    b.Property<string>("Logradouro")
                        .HasMaxLength(1000);

                    b.Property<string>("Numero")
                        .HasMaxLength(1000);

                    b.Property<byte[]>("RowVersion")
                        .IsConcurrencyToken();

                    b.Property<string>("UF")
                        .HasMaxLength(1000);

                    b.HasKey("Id");

                    b.ToTable("Endereco");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Cadastro.Enfermaria", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("Ativo");

                    b.Property<string>("Local")
                        .HasMaxLength(1000);

                    b.Property<string>("Nome")
                        .HasMaxLength(1000);

                    b.Property<byte[]>("RowVersion")
                        .IsConcurrencyToken();

                    b.Property<Guid?>("UnidadeId");

                    b.HasKey("Id");

                    b.HasIndex("UnidadeId");

                    b.ToTable("Enfermaria");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Cadastro.Leito", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("Ativo");

                    b.Property<Guid?>("ClinicaId");

                    b.Property<Guid?>("EnfermariaId");

                    b.Property<string>("Nome")
                        .HasMaxLength(1000);

                    b.Property<bool>("Ocupado");

                    b.Property<byte[]>("RowVersion")
                        .IsConcurrencyToken();

                    b.Property<int>("TipoLeito");

                    b.HasKey("Id");

                    b.HasIndex("ClinicaId");

                    b.HasIndex("EnfermariaId");

                    b.ToTable("Leito");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Cadastro.LocalAtendimento", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Local")
                        .HasMaxLength(1000);

                    b.HasKey("Id");

                    b.ToTable("LocalAtendimento");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Cadastro.Paciente", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("Ativo");

                    b.Property<string>("CPF")
                        .HasMaxLength(1000);

                    b.Property<string>("Cns")
                        .HasMaxLength(1000);

                    b.Property<string>("CodigoPaciente")
                        .HasMaxLength(1000);

                    b.Property<DateTime?>("DataNascimento");

                    b.Property<string>("Email")
                        .HasMaxLength(1000);

                    b.Property<Guid?>("EnderecoId");

                    b.Property<string>("EstadoCivil")
                        .HasMaxLength(1000);

                    b.Property<string>("Etinia")
                        .HasMaxLength(1000);

                    b.Property<byte>("Foto");

                    b.Property<bool>("IsDeleted");

                    b.Property<string>("Nacionalidade")
                        .HasMaxLength(1000);

                    b.Property<Guid?>("NaturalidadeId");

                    b.Property<string>("Nome")
                        .HasMaxLength(1000);

                    b.Property<string>("NomeMae")
                        .HasMaxLength(1000);

                    b.Property<bool>("Obito");

                    b.Property<bool>("PendenteCadastro");

                    b.Property<string>("Profissao")
                        .HasMaxLength(1000);

                    b.Property<string>("RG")
                        .HasMaxLength(1000);

                    b.Property<byte[]>("RowVersion")
                        .IsConcurrencyToken();

                    b.Property<string>("Sexo")
                        .HasMaxLength(1000);

                    b.Property<string>("TelefoneCelular")
                        .HasMaxLength(1000);

                    b.Property<string>("TelefoneFixo")
                        .HasMaxLength(1000);

                    b.Property<string>("TelefoneTrabalho")
                        .HasMaxLength(1000);

                    b.Property<Guid?>("UnidadeId");

                    b.HasKey("Id");

                    b.HasIndex("EnderecoId");

                    b.HasIndex("NaturalidadeId");

                    b.HasIndex("UnidadeId");

                    b.ToTable("Paciente");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Cadastro.Painel", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("Atendido");

                    b.Property<DateTime>("DataInsercao");

                    b.Property<string>("LocalAtendimento")
                        .HasMaxLength(1000);

                    b.Property<string>("NomePaciente")
                        .HasMaxLength(1000);

                    b.Property<byte[]>("RowVersion")
                        .IsConcurrencyToken();

                    b.Property<string>("Servico")
                        .HasMaxLength(1000);

                    b.Property<Guid>("UnidadeId")
                        .ValueGeneratedOnAdd();

                    b.HasKey("Id");

                    b.ToTable("Painel");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Cadastro.Pais", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("Ativo");

                    b.Property<string>("CodigoSUS")
                        .HasMaxLength(1000);

                    b.Property<string>("Descricao")
                        .HasMaxLength(1000);

                    b.Property<byte[]>("RowVersion")
                        .IsConcurrencyToken();

                    b.HasKey("Id");

                    b.ToTable("Pais");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Cadastro.Profissional", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CNS")
                        .HasMaxLength(1000);

                    b.Property<string>("CPF")
                        .HasMaxLength(1000);

                    b.Property<string>("CRM")
                        .HasMaxLength(1000);

                    b.Property<Guid?>("CboId");

                    b.Property<bool>("IsDeleted");

                    b.Property<string>("Nacionalidade")
                        .HasMaxLength(1000);

                    b.Property<DateTime?>("Nascimento");

                    b.Property<string>("Naturalidade")
                        .HasMaxLength(1000);

                    b.Property<string>("Nome")
                        .HasMaxLength(1000);

                    b.Property<string>("NomeMae")
                        .HasMaxLength(1000);

                    b.Property<string>("NomePai")
                        .HasMaxLength(1000);

                    b.Property<byte[]>("RowVersion")
                        .IsConcurrencyToken();

                    b.Property<Guid?>("SetorClinicaId");

                    b.Property<string>("Sexo")
                        .HasMaxLength(1000);

                    b.Property<Guid?>("UnidadeId");

                    b.HasKey("Id");

                    b.HasIndex("CboId");

                    b.HasIndex("SetorClinicaId");

                    b.HasIndex("UnidadeId");

                    b.ToTable("Profissional");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Cadastro.ProfissionalServico", b =>
                {
                    b.Property<Guid>("ProfissionalId")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid>("ServicoId")
                        .ValueGeneratedOnAdd();

                    b.HasKey("ProfissionalId", "ServicoId");

                    b.HasIndex("ServicoId");

                    b.ToTable("ProfissionalServico");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Cadastro.Servico", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Descricao")
                        .HasMaxLength(1000);

                    b.Property<bool>("IsDeleted");

                    b.Property<byte[]>("RowVersion")
                        .IsConcurrencyToken();

                    b.HasKey("Id");

                    b.ToTable("Servico");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Cadastro.SetorClinica", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("Ativo");

                    b.Property<string>("Descricao")
                        .HasMaxLength(1000);

                    b.Property<byte[]>("RowVersion")
                        .IsConcurrencyToken();

                    b.HasKey("Id");

                    b.ToTable("SetorClinica");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Cadastro.UF", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("Ativo");

                    b.Property<string>("Descricao")
                        .HasMaxLength(1000);

                    b.Property<byte[]>("RowVersion")
                        .IsConcurrencyToken();

                    b.HasKey("Id");

                    b.ToTable("UF");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Cadastro.Unidade", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CNES")
                        .HasMaxLength(1000);

                    b.Property<string>("CodigoUnidade")
                        .HasMaxLength(1000);

                    b.Property<DateTime>("DataAcreditacao");

                    b.Property<Guid?>("EnderecoId");

                    b.Property<string>("NomeFantasia")
                        .HasMaxLength(1000);

                    b.Property<string>("NomeUnidade")
                        .HasMaxLength(1000);

                    b.Property<byte[]>("RowVersion")
                        .IsConcurrencyToken();

                    b.Property<string>("Sigla")
                        .HasMaxLength(1000);

                    b.Property<string>("Situacao")
                        .HasMaxLength(1000);

                    b.HasKey("Id");

                    b.HasIndex("EnderecoId");

                    b.ToTable("Unidade");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.ServicoSocial", b =>
                {
                    b.HasBaseType("HealthCare.Domain.Entities.DbClasses.Atendimento.Atendimento");

                    b.Property<string>("Historico")
                        .HasMaxLength(1000);

                    b.Property<string>("Intervencao")
                        .HasMaxLength(1000);

                    b.ToTable("ServicoSocial");

                    b.HasDiscriminator().HasValue("ServicoSocial");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Admin.Assinatura", b =>
                {
                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Admin.Usuario", "Usuario")
                        .WithMany("Assinaturas")
                        .HasForeignKey("UsuarioId");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Admin.RoleClaim", b =>
                {
                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Admin.Role")
                        .WithMany("Claims")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Admin.UserClaim", b =>
                {
                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Admin.Usuario")
                        .WithMany("Claims")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Admin.UserLogin", b =>
                {
                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Admin.Usuario")
                        .WithMany("Logins")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Admin.UserRole", b =>
                {
                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Admin.Role")
                        .WithMany("Users")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Admin.Usuario")
                        .WithMany("Roles")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Admin.Usuario", b =>
                {
                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Cadastro.Profissional", "Profissional")
                        .WithMany()
                        .HasForeignKey("ProfissionalId");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.Acolhimento", b =>
                {
                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Cadastro.Paciente", "Paciente")
                        .WithMany()
                        .HasForeignKey("PacienteId");

                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Cadastro.Profissional", "Profissional")
                        .WithMany()
                        .HasForeignKey("ProfissionalId");

                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Cadastro.Servico", "Servico")
                        .WithMany("Acolhimentos")
                        .HasForeignKey("ServicoId");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.Anamnese", b =>
                {
                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Atendimento.Atendimento", "Atendimento")
                        .WithMany("Anamnese")
                        .HasForeignKey("AtendimentoId");

                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Atendimento.Ciap2", "Ciap2")
                        .WithMany()
                        .HasForeignKey("Ciap2Id");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.Atendimento", b =>
                {
                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Atendimento.Acolhimento", "Acolhimento")
                        .WithMany("Atendimento")
                        .HasForeignKey("AcolhimentoId");

                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Atendimento.Checkout", "Checkout")
                        .WithMany()
                        .HasForeignKey("CheckoutId");

                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Atendimento.MotivoCheckout", "MotivoCheckout")
                        .WithMany()
                        .HasForeignKey("MotivoCheckoutId");

                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Atendimento.CID", "PrimeiroDiagnostico")
                        .WithMany()
                        .HasForeignKey("PrimeiroDiagnosticoId");

                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Cadastro.Profissional", "Profissional")
                        .WithMany()
                        .HasForeignKey("ProfissionalId");

                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Atendimento.CID", "SegundoDiagnostico")
                        .WithMany()
                        .HasForeignKey("SegundoDiagnosticoId");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.AtendimentoProcedimento", b =>
                {
                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Atendimento.Atendimento")
                        .WithMany("Procedimentos")
                        .HasForeignKey("AtendimentoId1");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.Atestados", b =>
                {
                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Atendimento.Atendimento", "Atendimento")
                        .WithMany("Atestado")
                        .HasForeignKey("AtendimentoId");

                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Atendimento.CID", "CID")
                        .WithMany()
                        .HasForeignKey("CIDId");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.Checkout", b =>
                {
                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Atendimento.MotivoCheckout", "MotivoCheckout")
                        .WithMany()
                        .HasForeignKey("MotivoCheckoutId");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.CID", b =>
                {
                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Atendimento.Procedimento")
                        .WithMany("Cid")
                        .HasForeignKey("ProcedimentoId");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.ClassificacaoDeRisco", b =>
                {
                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Atendimento.Acolhimento", "Acolhimento")
                        .WithMany("Classificacao")
                        .HasForeignKey("AcolhimentoId");

                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Atendimento.Risco", "Risco")
                        .WithMany()
                        .HasForeignKey("RiscoId");

                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Atendimento.SinaisVitais", "SinaisVitais")
                        .WithMany()
                        .HasForeignKey("SinaisVitaisId");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.ElementoExame", b =>
                {
                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Atendimento.Exame", "Exame")
                        .WithMany()
                        .HasForeignKey("ExameId");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.Evolucao", b =>
                {
                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Atendimento.Atendimento", "Atemdimento")
                        .WithMany()
                        .HasForeignKey("AtemdimentoId");

                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Cadastro.Profissional", "Medico")
                        .WithMany()
                        .HasForeignKey("MedicoId");

                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Atendimento.CID", "PrimeiroDiagnostico")
                        .WithMany()
                        .HasForeignKey("PrimeiroDiagnosticoId");

                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Atendimento.CID", "SegundoDiagnostico")
                        .WithMany()
                        .HasForeignKey("SegundoDiagnosticoId");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.EvolucaoEnfermagem", b =>
                {
                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Atendimento.MovimentacaoLeito", "MovimentacaoLeito")
                        .WithMany()
                        .HasForeignKey("MovimentacaoLeitoId");

                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Cadastro.Profissional", "Profissional")
                        .WithMany()
                        .HasForeignKey("ProfissionalId");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.EvolucaoProcedimento", b =>
                {
                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Atendimento.Evolucao", "Evolucao")
                        .WithMany("EvolucaoProcedimentos")
                        .HasForeignKey("EvolucaoId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Atendimento.Procedimento", "Procedimento")
                        .WithMany("EvolucaoProcedimentos")
                        .HasForeignKey("ProcedimentoId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.Exame", b =>
                {
                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Atendimento.Exame", "ExamePai")
                        .WithMany()
                        .HasForeignKey("ExamePaiId");

                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Atendimento.TipoExame", "TipoExame")
                        .WithMany()
                        .HasForeignKey("TipoExameId");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.ExameAtendimento", b =>
                {
                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Atendimento.Atendimento", "Atendimento")
                        .WithMany()
                        .HasForeignKey("AtendimentoId");

                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Atendimento.Exame", "Exame")
                        .WithMany()
                        .HasForeignKey("ExameId");

                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Cadastro.Profissional", "Executor")
                        .WithMany()
                        .HasForeignKey("ExecutorId");

                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Cadastro.Profissional", "Medico")
                        .WithMany()
                        .HasForeignKey("MedicoId");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.HipoteseDiagnostica", b =>
                {
                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Atendimento.Atendimento", "Atendimento")
                        .WithMany("HipoteseDiagnostica")
                        .HasForeignKey("AtendimentoId");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.HipoteseDiagnosticaCid", b =>
                {
                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Atendimento.CID", "Cid")
                        .WithMany("HipoteseDiagnosticaCids")
                        .HasForeignKey("CidId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Atendimento.HipoteseDiagnostica", "HipoteseDiagnostica")
                        .WithMany("HipoteseDiagnosticaCids")
                        .HasForeignKey("HipoteseDiagnosticaId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.Imagem", b =>
                {
                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Atendimento.ImagemAtendimento", "ImagemAtendimento")
                        .WithMany("Imagens")
                        .HasForeignKey("ImagemAtendimentoId");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.ImagemAtendimento", b =>
                {
                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Atendimento.Atendimento", "Atendimento")
                        .WithMany("ImagemAtendimento")
                        .HasForeignKey("AtendimentoId");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.Internacao", b =>
                {
                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Atendimento.Atendimento", "Atendimento")
                        .WithMany("Internacoes")
                        .HasForeignKey("AtendimentoId");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.Laudo", b =>
                {
                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Atendimento.ResultadoExame", "Exame")
                        .WithMany("Laudo")
                        .HasForeignKey("ExameId");

                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Admin.Usuario", "Usuario")
                        .WithMany("Laudos")
                        .HasForeignKey("UsuarioId");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.LiquidosAdministradosEnfermagem", b =>
                {
                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Atendimento.LiquidosAdministrados", "LiquidosAdministrados")
                        .WithMany()
                        .HasForeignKey("LiquidosAdministradosId");

                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Atendimento.EvolucaoEnfermagem", "evolucaoEnfermagem")
                        .WithMany("LiquidosAdministrados")
                        .HasForeignKey("evolucaoEnfermagemId");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.LiquidosEliminadosEnfermagem", b =>
                {
                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Atendimento.LiquidosEliminados", "LiquidosEliminados")
                        .WithMany()
                        .HasForeignKey("LiquidosEliminadosId");

                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Atendimento.EvolucaoEnfermagem", "evolucaoEnfermagem")
                        .WithMany("LiquidosEliminados")
                        .HasForeignKey("evolucaoEnfermagemId");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.MotivoCheckout", b =>
                {
                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Atendimento.TipoMotivoChekout", "Tipo")
                        .WithMany()
                        .HasForeignKey("TipoId");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.MovimentacaoLeito", b =>
                {
                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Cadastro.Clinica", "ClinicaAtual1")
                        .WithMany()
                        .HasForeignKey("ClinicaAtual1Id");

                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Cadastro.Servico", "Clinica")
                        .WithMany()
                        .HasForeignKey("ClinicaId");

                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Atendimento.Internacao", "Internacao")
                        .WithMany("Movimentacoes")
                        .HasForeignKey("InternacaoId");

                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Cadastro.Leito", "LeitoAtual")
                        .WithMany("Movimentacoes")
                        .HasForeignKey("LeitoAtualId");

                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Atendimento.MovimentacaoLeito", "UltimaMovimentacao")
                        .WithMany()
                        .HasForeignKey("UltimaMovimentacaoId");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.Prescricao", b =>
                {
                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Atendimento.Atendimento", "Atendimento")
                        .WithMany()
                        .HasForeignKey("AtendimentoId");

                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Cadastro.Profissional", "Enfermeiro")
                        .WithMany()
                        .HasForeignKey("EnfermeiroId");

                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Cadastro.Profissional", "Medico")
                        .WithMany()
                        .HasForeignKey("MedicoId");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.ProcedimentoCid", b =>
                {
                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Atendimento.CID", "Cid")
                        .WithMany("ProcedimentosCids")
                        .HasForeignKey("CidId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Atendimento.Procedimento", "Procedimento")
                        .WithMany("ProcedimentosCids")
                        .HasForeignKey("ProcedimentoId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.ProcedimentosEnfermagem", b =>
                {
                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Atendimento.EvolucaoEnfermagem", "Evolucao")
                        .WithMany("ProcedimentosEnfermagem")
                        .HasForeignKey("EvolucaoId");

                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Atendimento.Procedimento", "Procedimento")
                        .WithMany()
                        .HasForeignKey("ProcedimentoId");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.ReceitaMedica", b =>
                {
                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Atendimento.Atendimento", "Atendimento")
                        .WithMany("Receita")
                        .HasForeignKey("AtendimentoId");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.ResultadoExame", b =>
                {
                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Atendimento.Atendimento", "Atendimento")
                        .WithMany("Exames")
                        .HasForeignKey("AtendimentoId");

                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Atendimento.Exame", "Exame")
                        .WithMany()
                        .HasForeignKey("ExameId");

                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Cadastro.Paciente", "Paciente")
                        .WithMany()
                        .HasForeignKey("PacienteId");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.ResultadoExameElemento", b =>
                {
                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Atendimento.ElementoExame", "ElementoExame")
                        .WithMany()
                        .HasForeignKey("ElementoExameId");

                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Atendimento.ResultadoExame", "ResultadoExame")
                        .WithMany("Elementos")
                        .HasForeignKey("ResultadoExameId");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.TemplatesAtestado", b =>
                {
                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Atendimento.Atestados", "Atestado")
                        .WithMany("Template")
                        .HasForeignKey("AtestadoId");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Atendimento.TipoExame", b =>
                {
                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Atendimento.TipoExame", "TipoExamePai")
                        .WithMany()
                        .HasForeignKey("TipoExamePaiId");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Cadastro.Bairro", b =>
                {
                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Cadastro.Cidade", "Cidade")
                        .WithMany()
                        .HasForeignKey("CidadeId");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Cadastro.Cidade", b =>
                {
                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Cadastro.UF", "UF")
                        .WithMany()
                        .HasForeignKey("UFId");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Cadastro.Enfermaria", b =>
                {
                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Cadastro.Unidade", "Unidade")
                        .WithMany()
                        .HasForeignKey("UnidadeId");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Cadastro.Leito", b =>
                {
                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Cadastro.SetorClinica", "Clinica")
                        .WithMany()
                        .HasForeignKey("ClinicaId");

                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Cadastro.Enfermaria", "Enfermaria")
                        .WithMany("Leitos")
                        .HasForeignKey("EnfermariaId");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Cadastro.Paciente", b =>
                {
                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Cadastro.Endereco", "Endereco")
                        .WithMany()
                        .HasForeignKey("EnderecoId");

                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Cadastro.Cidade", "Naturalidade")
                        .WithMany()
                        .HasForeignKey("NaturalidadeId");

                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Cadastro.Unidade", "Unidade")
                        .WithMany()
                        .HasForeignKey("UnidadeId");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Cadastro.Profissional", b =>
                {
                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Cadastro.Cbo", "Cbo")
                        .WithMany()
                        .HasForeignKey("CboId");

                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Cadastro.SetorClinica", "SetorClinica")
                        .WithMany()
                        .HasForeignKey("SetorClinicaId");

                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Cadastro.Unidade", "Unidade")
                        .WithMany("Profissionais")
                        .HasForeignKey("UnidadeId");
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Cadastro.ProfissionalServico", b =>
                {
                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Cadastro.Profissional", "Profissional")
                        .WithMany("ProfissionalServicos")
                        .HasForeignKey("ProfissionalId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Cadastro.Servico", "Servico")
                        .WithMany("ProfissionalServicos")
                        .HasForeignKey("ServicoId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("HealthCare.Domain.Entities.DbClasses.Cadastro.Unidade", b =>
                {
                    b.HasOne("HealthCare.Domain.Entities.DbClasses.Cadastro.Endereco", "Endereco")
                        .WithMany()
                        .HasForeignKey("EnderecoId");
                });
        }
    }
}

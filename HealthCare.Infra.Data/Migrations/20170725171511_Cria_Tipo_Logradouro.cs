﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HealthCare.Infra.Data.Migrations
{
    public partial class Cria_Tipo_Logradouro : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AtendimentoProcedimento_Atendimento_AtendimentoId1",
                table: "AtendimentoProcedimento");

            migrationBuilder.DropUniqueConstraint(
                name: "AK_AtendimentoProcedimento_AtendimentoId",
                table: "AtendimentoProcedimento");

            migrationBuilder.DropIndex(
                name: "IX_AtendimentoProcedimento_AtendimentoId1",
                table: "AtendimentoProcedimento");

            migrationBuilder.CreateTable(
                name: "CodigoLogradouro",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    Codigo = table.Column<string>(maxLength: 1000, nullable: true),
                    Descricao = table.Column<string>(maxLength: 1000, nullable: true),
                    RowVersion = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CodigoLogradouro", x => x.Id);
                });

            migrationBuilder.AddColumn<Guid>(
                name: "CodigoLogradouroId",
                table: "Endereco",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Endereco_CodigoLogradouroId",
                table: "Endereco",
                column: "CodigoLogradouroId");

            migrationBuilder.AlterColumn<Guid>(
                name: "AtendimentoId1",
                table: "AtendimentoProcedimento",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true)
                .Annotation("MySql:ValueGeneratedOnAdd", true);

            migrationBuilder.AlterColumn<Guid>(
                name: "AtendimentoId",
                table: "AtendimentoProcedimento",
                nullable: false,
                oldClrType: typeof(Guid))
                .Annotation("MySql:ValueGeneratedOnAdd", true);

            migrationBuilder.CreateIndex(
                name: "IX_AtendimentoProcedimento_AtendimentoId",
                table: "AtendimentoProcedimento",
                column: "AtendimentoId");

            migrationBuilder.AddForeignKey(
                name: "FK_AtendimentoProcedimento_Atendimento_AtendimentoId",
                table: "AtendimentoProcedimento",
                column: "AtendimentoId",
                principalTable: "Atendimento",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Endereco_CodigoLogradouro_CodigoLogradouroId",
                table: "Endereco",
                column: "CodigoLogradouroId",
                principalTable: "CodigoLogradouro",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AtendimentoProcedimento_Atendimento_AtendimentoId",
                table: "AtendimentoProcedimento");

            migrationBuilder.DropForeignKey(
                name: "FK_Endereco_CodigoLogradouro_CodigoLogradouroId",
                table: "Endereco");

            migrationBuilder.DropIndex(
                name: "IX_Endereco_CodigoLogradouroId",
                table: "Endereco");

            migrationBuilder.DropIndex(
                name: "IX_AtendimentoProcedimento_AtendimentoId",
                table: "AtendimentoProcedimento");

            migrationBuilder.DropColumn(
                name: "CodigoLogradouroId",
                table: "Endereco");

            migrationBuilder.DropTable(
                name: "CodigoLogradouro");

            migrationBuilder.AlterColumn<Guid>(
                name: "AtendimentoId",
                table: "AtendimentoProcedimento",
                nullable: false,
                oldClrType: typeof(Guid))
                .OldAnnotation("MySql:ValueGeneratedOnAdd", true);

            migrationBuilder.AlterColumn<Guid>(
                name: "AtendimentoId1",
                table: "AtendimentoProcedimento",
                nullable: true,
                oldClrType: typeof(Guid))
                .OldAnnotation("MySql:ValueGeneratedOnAdd", true);

            migrationBuilder.AddUniqueConstraint(
                name: "AK_AtendimentoProcedimento_AtendimentoId",
                table: "AtendimentoProcedimento",
                column: "AtendimentoId");

            migrationBuilder.CreateIndex(
                name: "IX_AtendimentoProcedimento_AtendimentoId1",
                table: "AtendimentoProcedimento",
                column: "AtendimentoId1");

            migrationBuilder.AddForeignKey(
                name: "FK_AtendimentoProcedimento_Atendimento_AtendimentoId1",
                table: "AtendimentoProcedimento",
                column: "AtendimentoId1",
                principalTable: "Atendimento",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

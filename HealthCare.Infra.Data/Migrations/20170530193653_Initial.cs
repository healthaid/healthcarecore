﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HealthCare.Infra.Data.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Commands",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    Body = table.Column<string>(maxLength: 4000, nullable: true),
                    CorrelationId = table.Column<string>(maxLength: 1000, nullable: true),
                    DeliveryDate = table.Column<DateTime>(nullable: true),
                    RowVersion = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Commands", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Event",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    AggregateType = table.Column<string>(maxLength: 1000, nullable: true),
                    CorrelationId = table.Column<string>(maxLength: 1000, nullable: true),
                    Payload = table.Column<string>(maxLength: 1000, nullable: true),
                    Version = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Event", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    ConcurrencyStamp = table.Column<string>(maxLength: 1000, nullable: true),
                    Name = table.Column<string>(maxLength: 1000, nullable: true),
                    NormalizedName = table.Column<string>(unicode: false, maxLength: 250, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    LoginProvider = table.Column<string>(unicode: false, maxLength: 196, nullable: false),
                    Name = table.Column<string>(unicode: false, maxLength: 250, nullable: false),
                    Value = table.Column<string>(maxLength: 1000, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                });

            migrationBuilder.CreateTable(
                name: "AdministracaoMedicamento",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    Nome = table.Column<string>(maxLength: 1000, nullable: true),
                    RowVersion = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdministracaoMedicamento", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Ciap2",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    Categoria = table.Column<string>(maxLength: 1000, nullable: true),
                    Codigo = table.Column<string>(maxLength: 1000, nullable: true),
                    Componente = table.Column<string>(maxLength: 1000, nullable: true),
                    Descricao = table.Column<string>(maxLength: 1000, nullable: true),
                    Grupo = table.Column<string>(maxLength: 1000, nullable: true),
                    Procedimento = table.Column<bool>(nullable: false),
                    RowVersion = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ciap2", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Ciap2_Cid10",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    CiapCodigo = table.Column<string>(maxLength: 1000, nullable: true),
                    Cid = table.Column<string>(maxLength: 1000, nullable: true),
                    CidFrequente = table.Column<string>(maxLength: 1000, nullable: true),
                    RowVersion = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ciap2_Cid10", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "LiquidosAdministrados",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    Descricao = table.Column<string>(maxLength: 1000, nullable: true),
                    RowVersion = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LiquidosAdministrados", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "LiquidosEliminados",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    Descricao = table.Column<string>(maxLength: 1000, nullable: true),
                    RowVersion = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LiquidosEliminados", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Medicamento",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    ATC = table.Column<string>(maxLength: 1000, nullable: true),
                    DDD = table.Column<string>(maxLength: 1000, nullable: true),
                    FormaFarmaceutica = table.Column<string>(maxLength: 1000, nullable: true),
                    Generico = table.Column<string>(maxLength: 1000, nullable: true),
                    Nome = table.Column<string>(maxLength: 1000, nullable: true),
                    RowVersion = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Medicamento", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Procedimento",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    CO_FINANCIAMENTO = table.Column<string>(maxLength: 1000, nullable: true),
                    CO_PROCEDIMENTO = table.Column<string>(maxLength: 1000, nullable: true),
                    CO_RUBRICA = table.Column<string>(maxLength: 1000, nullable: true),
                    DT_COMPETENCIA = table.Column<string>(maxLength: 1000, nullable: true),
                    NO_PROCEDIMENTO = table.Column<string>(maxLength: 1000, nullable: true),
                    QT_DIAS_PERMANENCIA = table.Column<int>(nullable: false),
                    QT_MAXIMA_EXECUCAO = table.Column<int>(nullable: false),
                    QT_PONTOS = table.Column<int>(nullable: false),
                    QT_TEMPO_PERMANENCIA = table.Column<string>(maxLength: 1000, nullable: true),
                    RowVersion = table.Column<byte[]>(nullable: true),
                    TP_COMPLEXIDADE = table.Column<string>(maxLength: 1000, nullable: true),
                    TP_SEXO = table.Column<string>(maxLength: 1000, nullable: true),
                    VL_IDADE_MAXIMA = table.Column<int>(nullable: false),
                    VL_IDADE_MINIMA = table.Column<int>(nullable: false),
                    VL_SA = table.Column<decimal>(nullable: false),
                    VL_SH = table.Column<decimal>(nullable: false),
                    VL_SP = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Procedimento", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Risco",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    Descricao = table.Column<string>(maxLength: 1000, nullable: true),
                    Prioridade = table.Column<int>(nullable: false),
                    RowVersion = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Risco", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SinaisVitais",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    Altura = table.Column<string>(maxLength: 1000, nullable: true),
                    FrequenciaRespiratoria = table.Column<string>(maxLength: 1000, nullable: true),
                    GlicemiaCapilar = table.Column<string>(maxLength: 1000, nullable: true),
                    Peso = table.Column<string>(maxLength: 1000, nullable: true),
                    Pressao = table.Column<string>(maxLength: 1000, nullable: true),
                    Pulso = table.Column<string>(maxLength: 1000, nullable: true),
                    RowVersion = table.Column<byte[]>(nullable: true),
                    Temperatura = table.Column<string>(maxLength: 1000, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SinaisVitais", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TipoExame",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    Descricao = table.Column<string>(maxLength: 1000, nullable: true),
                    TipoExamePaiId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TipoExame", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TipoExame_TipoExame_TipoExamePaiId",
                        column: x => x.TipoExamePaiId,
                        principalTable: "TipoExame",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TipoMotivoChekout",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    Ativo = table.Column<bool>(nullable: false),
                    Descricao = table.Column<string>(maxLength: 1000, nullable: true),
                    RowVersion = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TipoMotivoChekout", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UnidadeMedida",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    Nome = table.Column<string>(maxLength: 1000, nullable: true),
                    RowVersion = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UnidadeMedida", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Cbo",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    Codigo = table.Column<string>(maxLength: 1000, nullable: true),
                    Descricao = table.Column<string>(maxLength: 1000, nullable: true),
                    RowVersion = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cbo", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Clinica",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    Nome = table.Column<string>(maxLength: 1000, nullable: true),
                    RowVersion = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Clinica", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Endereco",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    Bairro = table.Column<string>(maxLength: 1000, nullable: true),
                    CEP = table.Column<string>(maxLength: 1000, nullable: true),
                    Cidade = table.Column<string>(maxLength: 1000, nullable: true),
                    Complemento = table.Column<string>(maxLength: 1000, nullable: true),
                    Logradouro = table.Column<string>(maxLength: 1000, nullable: true),
                    Numero = table.Column<string>(maxLength: 1000, nullable: true),
                    RowVersion = table.Column<byte[]>(nullable: true),
                    UF = table.Column<string>(maxLength: 1000, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Endereco", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "LocalAtendimento",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    Local = table.Column<string>(maxLength: 1000, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LocalAtendimento", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Painel",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    Atendido = table.Column<bool>(nullable: false),
                    DataInsercao = table.Column<DateTime>(nullable: false),
                    LocalAtendimento = table.Column<string>(maxLength: 1000, nullable: true),
                    NomePaciente = table.Column<string>(maxLength: 1000, nullable: true),
                    RowVersion = table.Column<byte[]>(nullable: true),
                    Servico = table.Column<string>(maxLength: 1000, nullable: true),
                    UnidadeId = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Painel", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Pais",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    Ativo = table.Column<bool>(nullable: false),
                    CodigoSUS = table.Column<string>(maxLength: 1000, nullable: true),
                    Descricao = table.Column<string>(maxLength: 1000, nullable: true),
                    RowVersion = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Pais", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Servico",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    Descricao = table.Column<string>(maxLength: 1000, nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    RowVersion = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Servico", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SetorClinica",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    Ativo = table.Column<bool>(nullable: false),
                    Descricao = table.Column<string>(maxLength: 1000, nullable: true),
                    RowVersion = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SetorClinica", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UF",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    Ativo = table.Column<bool>(nullable: false),
                    Descricao = table.Column<string>(maxLength: 1000, nullable: true),
                    RowVersion = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UF", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    ClaimType = table.Column<string>(maxLength: 1000, nullable: true),
                    ClaimValue = table.Column<string>(maxLength: 1000, nullable: true),
                    RoleId = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CID",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    Agravo = table.Column<string>(maxLength: 1000, nullable: true),
                    CamposIrradiados = table.Column<string>(maxLength: 1000, nullable: true),
                    Codigo = table.Column<string>(maxLength: 1000, nullable: true),
                    Descricao = table.Column<string>(maxLength: 1000, nullable: true),
                    Estadio = table.Column<string>(maxLength: 1000, nullable: true),
                    ProcedimentoId = table.Column<Guid>(nullable: true),
                    RowVersion = table.Column<byte[]>(nullable: true),
                    Sexo = table.Column<string>(maxLength: 1000, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CID", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CID_Procedimento_ProcedimentoId",
                        column: x => x.ProcedimentoId,
                        principalTable: "Procedimento",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Exame",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    Classificacao = table.Column<string>(maxLength: 1000, nullable: true),
                    Descricao = table.Column<string>(maxLength: 1000, nullable: true),
                    ExamePaiId = table.Column<Guid>(nullable: true),
                    Procedimento = table.Column<string>(maxLength: 1000, nullable: true),
                    Sexo = table.Column<string>(maxLength: 1000, nullable: true),
                    TipoExameId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Exame", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Exame_Exame_ExamePaiId",
                        column: x => x.ExamePaiId,
                        principalTable: "Exame",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Exame_TipoExame_TipoExameId",
                        column: x => x.TipoExameId,
                        principalTable: "TipoExame",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MotivoCheckout",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    Ativo = table.Column<bool>(nullable: false),
                    Descricao = table.Column<string>(maxLength: 1000, nullable: true),
                    RowVersion = table.Column<byte[]>(nullable: true),
                    TipoId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MotivoCheckout", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MotivoCheckout_TipoMotivoChekout_TipoId",
                        column: x => x.TipoId,
                        principalTable: "TipoMotivoChekout",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Unidade",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    CNES = table.Column<string>(maxLength: 1000, nullable: true),
                    CodigoUnidade = table.Column<string>(maxLength: 1000, nullable: true),
                    DataAcreditacao = table.Column<DateTime>(nullable: false),
                    EnderecoId = table.Column<Guid>(nullable: true),
                    NomeFantasia = table.Column<string>(maxLength: 1000, nullable: true),
                    NomeUnidade = table.Column<string>(maxLength: 1000, nullable: true),
                    RowVersion = table.Column<byte[]>(nullable: true),
                    Sigla = table.Column<string>(maxLength: 1000, nullable: true),
                    Situacao = table.Column<string>(maxLength: 1000, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Unidade", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Unidade_Endereco_EnderecoId",
                        column: x => x.EnderecoId,
                        principalTable: "Endereco",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Cidade",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    Ativo = table.Column<bool>(nullable: false),
                    Nome = table.Column<string>(maxLength: 1000, nullable: true),
                    RowVersion = table.Column<byte[]>(nullable: true),
                    UFId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cidade", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Cidade_UF_UFId",
                        column: x => x.UFId,
                        principalTable: "UF",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ProcedimentoCid",
                columns: table => new
                {
                    CidId = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    ProcedimentoId = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProcedimentoCid", x => new { x.CidId, x.ProcedimentoId });
                    table.ForeignKey(
                        name: "FK_ProcedimentoCid_CID_CidId",
                        column: x => x.CidId,
                        principalTable: "CID",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProcedimentoCid_Procedimento_ProcedimentoId",
                        column: x => x.ProcedimentoId,
                        principalTable: "Procedimento",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ElementoExame",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    Descricao = table.Column<string>(maxLength: 1000, nullable: true),
                    ExameId = table.Column<Guid>(nullable: true),
                    RowVersion = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ElementoExame", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ElementoExame_Exame_ExameId",
                        column: x => x.ExameId,
                        principalTable: "Exame",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Checkout",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    DiagnosticoDois = table.Column<string>(maxLength: 1000, nullable: true),
                    DiagnosticoUm = table.Column<string>(maxLength: 1000, nullable: true),
                    MotivoCheckoutId = table.Column<Guid>(nullable: true),
                    Observacao = table.Column<string>(maxLength: 1000, nullable: true),
                    RowVersion = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Checkout", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Checkout_MotivoCheckout_MotivoCheckoutId",
                        column: x => x.MotivoCheckoutId,
                        principalTable: "MotivoCheckout",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Enfermaria",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    Ativo = table.Column<bool>(nullable: false),
                    Local = table.Column<string>(maxLength: 1000, nullable: true),
                    Nome = table.Column<string>(maxLength: 1000, nullable: true),
                    RowVersion = table.Column<byte[]>(nullable: true),
                    UnidadeId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Enfermaria", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Enfermaria_Unidade_UnidadeId",
                        column: x => x.UnidadeId,
                        principalTable: "Unidade",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Profissional",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    CNS = table.Column<string>(maxLength: 1000, nullable: true),
                    CPF = table.Column<string>(maxLength: 1000, nullable: true),
                    CRM = table.Column<string>(maxLength: 1000, nullable: true),
                    CboId = table.Column<Guid>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Nacionalidade = table.Column<string>(maxLength: 1000, nullable: true),
                    Nascimento = table.Column<DateTime>(nullable: true),
                    Naturalidade = table.Column<string>(maxLength: 1000, nullable: true),
                    Nome = table.Column<string>(maxLength: 1000, nullable: true),
                    NomeMae = table.Column<string>(maxLength: 1000, nullable: true),
                    NomePai = table.Column<string>(maxLength: 1000, nullable: true),
                    RowVersion = table.Column<byte[]>(nullable: true),
                    SetorClinicaId = table.Column<Guid>(nullable: true),
                    Sexo = table.Column<string>(maxLength: 1000, nullable: true),
                    UnidadeId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Profissional", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Profissional_Cbo_CboId",
                        column: x => x.CboId,
                        principalTable: "Cbo",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Profissional_SetorClinica_SetorClinicaId",
                        column: x => x.SetorClinicaId,
                        principalTable: "SetorClinica",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Profissional_Unidade_UnidadeId",
                        column: x => x.UnidadeId,
                        principalTable: "Unidade",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Bairro",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    Ativo = table.Column<bool>(nullable: false),
                    CidadeId = table.Column<Guid>(nullable: true),
                    Nome = table.Column<string>(maxLength: 1000, nullable: true),
                    RowVersion = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Bairro", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Bairro_Cidade_CidadeId",
                        column: x => x.CidadeId,
                        principalTable: "Cidade",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Paciente",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    Ativo = table.Column<bool>(nullable: false),
                    CPF = table.Column<string>(maxLength: 1000, nullable: true),
                    Cns = table.Column<string>(maxLength: 1000, nullable: true),
                    CodigoPaciente = table.Column<string>(maxLength: 1000, nullable: true),
                    DataNascimento = table.Column<DateTime>(nullable: true),
                    Email = table.Column<string>(maxLength: 1000, nullable: true),
                    EnderecoId = table.Column<Guid>(nullable: true),
                    EstadoCivil = table.Column<string>(maxLength: 1000, nullable: true),
                    Etinia = table.Column<string>(maxLength: 1000, nullable: true),
                    Foto = table.Column<byte>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Nacionalidade = table.Column<string>(maxLength: 1000, nullable: true),
                    NaturalidadeId = table.Column<Guid>(nullable: true),
                    Nome = table.Column<string>(maxLength: 1000, nullable: true),
                    NomeMae = table.Column<string>(maxLength: 1000, nullable: true),
                    Obito = table.Column<bool>(nullable: false),
                    PendenteCadastro = table.Column<bool>(nullable: false),
                    Profissao = table.Column<string>(maxLength: 1000, nullable: true),
                    RG = table.Column<string>(maxLength: 1000, nullable: true),
                    RowVersion = table.Column<byte[]>(nullable: true),
                    Sexo = table.Column<string>(maxLength: 1000, nullable: true),
                    TelefoneCelular = table.Column<string>(maxLength: 1000, nullable: true),
                    TelefoneFixo = table.Column<string>(maxLength: 1000, nullable: true),
                    TelefoneTrabalho = table.Column<string>(maxLength: 1000, nullable: true),
                    UnidadeId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Paciente", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Paciente_Endereco_EnderecoId",
                        column: x => x.EnderecoId,
                        principalTable: "Endereco",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Paciente_Cidade_NaturalidadeId",
                        column: x => x.NaturalidadeId,
                        principalTable: "Cidade",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Paciente_Unidade_UnidadeId",
                        column: x => x.UnidadeId,
                        principalTable: "Unidade",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Leito",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    Ativo = table.Column<bool>(nullable: false),
                    ClinicaId = table.Column<Guid>(nullable: true),
                    EnfermariaId = table.Column<Guid>(nullable: true),
                    Nome = table.Column<string>(maxLength: 1000, nullable: true),
                    Ocupado = table.Column<bool>(nullable: false),
                    RowVersion = table.Column<byte[]>(nullable: true),
                    TipoLeito = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Leito", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Leito_SetorClinica_ClinicaId",
                        column: x => x.ClinicaId,
                        principalTable: "SetorClinica",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Leito_Enfermaria_EnfermariaId",
                        column: x => x.EnfermariaId,
                        principalTable: "Enfermaria",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    ConcurrencyStamp = table.Column<string>(maxLength: 1000, nullable: true),
                    ConfirmacaoSenha = table.Column<string>(maxLength: 1000, nullable: true),
                    DataCadastro = table.Column<DateTime>(nullable: true),
                    Documento = table.Column<string>(maxLength: 1000, nullable: true),
                    Email = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    Nome = table.Column<string>(maxLength: 1000, nullable: true),
                    NormalizedEmail = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    NormalizedUserName = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    PasswordHash = table.Column<string>(maxLength: 1000, nullable: true),
                    PhoneNumber = table.Column<string>(maxLength: 1000, nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    PrimeiroLogin = table.Column<bool>(nullable: false),
                    ProfissionalId = table.Column<Guid>(nullable: true),
                    SecurityStamp = table.Column<string>(maxLength: 1000, nullable: true),
                    Senha = table.Column<string>(maxLength: 1000, nullable: true),
                    Sobrenome = table.Column<string>(maxLength: 1000, nullable: true),
                    TipoDocumento = table.Column<string>(maxLength: 1000, nullable: true),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    UserName = table.Column<string>(unicode: false, maxLength: 196, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUsers_Profissional_ProfissionalId",
                        column: x => x.ProfissionalId,
                        principalTable: "Profissional",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ProfissionalServico",
                columns: table => new
                {
                    ProfissionalId = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    ServicoId = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProfissionalServico", x => new { x.ProfissionalId, x.ServicoId });
                    table.ForeignKey(
                        name: "FK_ProfissionalServico_Profissional_ProfissionalId",
                        column: x => x.ProfissionalId,
                        principalTable: "Profissional",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProfissionalServico_Servico_ServicoId",
                        column: x => x.ServicoId,
                        principalTable: "Servico",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Acolhimento",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    CodigoBoletim = table.Column<string>(maxLength: 1000, nullable: true),
                    DataHora = table.Column<DateTime>(nullable: false),
                    PacienteId = table.Column<Guid>(nullable: true),
                    Prioridade = table.Column<bool>(nullable: false),
                    ProfissionalId = table.Column<Guid>(nullable: true),
                    RowVersion = table.Column<byte[]>(nullable: true),
                    ServicoId = table.Column<Guid>(nullable: true),
                    Status = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Acolhimento", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Acolhimento_Paciente_PacienteId",
                        column: x => x.PacienteId,
                        principalTable: "Paciente",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Acolhimento_Profissional_ProfissionalId",
                        column: x => x.ProfissionalId,
                        principalTable: "Profissional",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Acolhimento_Servico_ServicoId",
                        column: x => x.ServicoId,
                        principalTable: "Servico",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Assinatura",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    Imagem = table.Column<byte[]>(nullable: true),
                    RowVersion = table.Column<byte[]>(nullable: true),
                    UsuarioId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Assinatura", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Assinatura_AspNetUsers_UsuarioId",
                        column: x => x.UsuarioId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    ClaimType = table.Column<string>(maxLength: 1000, nullable: true),
                    ClaimValue = table.Column<string>(maxLength: 1000, nullable: true),
                    UserId = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    UserId = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    LoginProvider = table.Column<string>(unicode: false, maxLength: 196, nullable: false),
                    ProviderKey = table.Column<string>(unicode: false, maxLength: 196, nullable: false),
                    ProviderDisplayName = table.Column<string>(maxLength: 1000, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.UserId, x.LoginProvider, x.ProviderKey });
                    table.UniqueConstraint("AK_AspNetUserLogins_LoginProvider_ProviderKey", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    RoleId = table.Column<Guid>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.RoleId, x.UserId });
                    table.UniqueConstraint("AK_AspNetUserRoles_RoleId", x => x.RoleId);
                    table.UniqueConstraint("AK_AspNetUserRoles_UserId_RoleId", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Atendimento",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    AcolhimentoId = table.Column<Guid>(nullable: true),
                    CheckoutId = table.Column<Guid>(nullable: true),
                    Discriminator = table.Column<string>(maxLength: 1000, nullable: false),
                    ExameFisico = table.Column<string>(maxLength: 1000, nullable: true),
                    FimAtendimento = table.Column<DateTime>(nullable: true),
                    InicioAtendimento = table.Column<DateTime>(nullable: false),
                    MotivoCheckoutId = table.Column<Guid>(nullable: true),
                    PrimeiroDiagnosticoId = table.Column<Guid>(nullable: true),
                    ProfissionalId = table.Column<Guid>(nullable: true),
                    RowVersion = table.Column<byte[]>(nullable: true),
                    SegundoDiagnosticoId = table.Column<Guid>(nullable: true),
                    Historico = table.Column<string>(maxLength: 1000, nullable: true),
                    Intervencao = table.Column<string>(maxLength: 1000, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Atendimento", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Atendimento_Acolhimento_AcolhimentoId",
                        column: x => x.AcolhimentoId,
                        principalTable: "Acolhimento",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Atendimento_Checkout_CheckoutId",
                        column: x => x.CheckoutId,
                        principalTable: "Checkout",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Atendimento_MotivoCheckout_MotivoCheckoutId",
                        column: x => x.MotivoCheckoutId,
                        principalTable: "MotivoCheckout",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Atendimento_CID_PrimeiroDiagnosticoId",
                        column: x => x.PrimeiroDiagnosticoId,
                        principalTable: "CID",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Atendimento_Profissional_ProfissionalId",
                        column: x => x.ProfissionalId,
                        principalTable: "Profissional",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Atendimento_CID_SegundoDiagnosticoId",
                        column: x => x.SegundoDiagnosticoId,
                        principalTable: "CID",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ClassificacaoDeRisco",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    AcolhimentoId = table.Column<Guid>(nullable: true),
                    Alergia = table.Column<string>(maxLength: 1000, nullable: true),
                    CausaExterna = table.Column<string>(maxLength: 1000, nullable: true),
                    DoencasPreexistentes = table.Column<string>(maxLength: 1000, nullable: true),
                    EspecialidadeMedica = table.Column<string>(maxLength: 1000, nullable: true),
                    MedicamentosEmUso = table.Column<string>(maxLength: 1000, nullable: true),
                    NivelDeConsciencia = table.Column<string>(maxLength: 1000, nullable: true),
                    Observacao = table.Column<string>(maxLength: 1000, nullable: true),
                    Queixa = table.Column<string>(maxLength: 1000, nullable: true),
                    RiscoId = table.Column<Guid>(nullable: true),
                    RowVersion = table.Column<byte[]>(nullable: true),
                    SinaisVitaisId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClassificacaoDeRisco", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ClassificacaoDeRisco_Acolhimento_AcolhimentoId",
                        column: x => x.AcolhimentoId,
                        principalTable: "Acolhimento",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ClassificacaoDeRisco_Risco_RiscoId",
                        column: x => x.RiscoId,
                        principalTable: "Risco",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ClassificacaoDeRisco_SinaisVitais_SinaisVitaisId",
                        column: x => x.SinaisVitaisId,
                        principalTable: "SinaisVitais",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Anamnese",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    AtendimentoId = table.Column<Guid>(nullable: true),
                    Ciap2Id = table.Column<Guid>(nullable: true),
                    Descricao = table.Column<string>(maxLength: 1000, nullable: true),
                    QueixaPrincipal = table.Column<string>(maxLength: 1000, nullable: true),
                    RowVersion = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Anamnese", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Anamnese_Atendimento_AtendimentoId",
                        column: x => x.AtendimentoId,
                        principalTable: "Atendimento",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Anamnese_Ciap2_Ciap2Id",
                        column: x => x.Ciap2Id,
                        principalTable: "Ciap2",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AtendimentoProcedimento",
                columns: table => new
                {
                    AtendimentoId = table.Column<Guid>(nullable: false),
                    ProcedimentoId = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    AtendimentoId1 = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AtendimentoProcedimento", x => new { x.AtendimentoId, x.ProcedimentoId });
                    table.UniqueConstraint("AK_AtendimentoProcedimento_AtendimentoId", x => x.AtendimentoId);
                    table.ForeignKey(
                        name: "FK_AtendimentoProcedimento_Atendimento_AtendimentoId1",
                        column: x => x.AtendimentoId1,
                        principalTable: "Atendimento",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Atestados",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    AtendimentoId = table.Column<Guid>(nullable: true),
                    CIDId = table.Column<Guid>(nullable: true),
                    Descricao = table.Column<string>(maxLength: 1000, nullable: true),
                    Dias = table.Column<string>(maxLength: 1000, nullable: true),
                    RowVersion = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Atestados", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Atestados_Atendimento_AtendimentoId",
                        column: x => x.AtendimentoId,
                        principalTable: "Atendimento",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Atestados_CID_CIDId",
                        column: x => x.CIDId,
                        principalTable: "CID",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Evolucao",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    AtemdimentoId = table.Column<Guid>(nullable: true),
                    DataHoraEvolucao = table.Column<DateTime>(nullable: false),
                    Descricao = table.Column<string>(maxLength: 1000, nullable: true),
                    EvolucaoTipo = table.Column<int>(nullable: false),
                    MedicoId = table.Column<Guid>(nullable: true),
                    PrimeiroDiagnosticoId = table.Column<Guid>(nullable: true),
                    ProcedimentoId = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    Quantidade = table.Column<int>(nullable: false),
                    RowVersion = table.Column<byte[]>(nullable: true),
                    SegundoDiagnosticoId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Evolucao", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Evolucao_Atendimento_AtemdimentoId",
                        column: x => x.AtemdimentoId,
                        principalTable: "Atendimento",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Evolucao_Profissional_MedicoId",
                        column: x => x.MedicoId,
                        principalTable: "Profissional",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Evolucao_CID_PrimeiroDiagnosticoId",
                        column: x => x.PrimeiroDiagnosticoId,
                        principalTable: "CID",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Evolucao_CID_SegundoDiagnosticoId",
                        column: x => x.SegundoDiagnosticoId,
                        principalTable: "CID",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ExameAtendimento",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    AtendimentoId = table.Column<Guid>(nullable: true),
                    DataExecucao = table.Column<DateTime>(nullable: true),
                    DataSolicitacao = table.Column<DateTime>(nullable: false),
                    DefinicaoExame = table.Column<string>(maxLength: 1000, nullable: true),
                    ExameId = table.Column<Guid>(nullable: true),
                    ExecutorId = table.Column<Guid>(nullable: true),
                    MedicoId = table.Column<Guid>(nullable: true),
                    ObservacaoExecutor = table.Column<string>(maxLength: 1000, nullable: true),
                    ObservacaoMedico = table.Column<string>(maxLength: 1000, nullable: true),
                    Procedimento = table.Column<string>(maxLength: 1000, nullable: true),
                    Realizado = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExameAtendimento", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ExameAtendimento_Atendimento_AtendimentoId",
                        column: x => x.AtendimentoId,
                        principalTable: "Atendimento",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ExameAtendimento_Exame_ExameId",
                        column: x => x.ExameId,
                        principalTable: "Exame",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ExameAtendimento_Profissional_ExecutorId",
                        column: x => x.ExecutorId,
                        principalTable: "Profissional",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ExameAtendimento_Profissional_MedicoId",
                        column: x => x.MedicoId,
                        principalTable: "Profissional",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "HipoteseDiagnostica",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    AtendimentoId = table.Column<Guid>(nullable: true),
                    Observacao = table.Column<string>(maxLength: 1000, nullable: true),
                    RowVersion = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HipoteseDiagnostica", x => x.Id);
                    table.ForeignKey(
                        name: "FK_HipoteseDiagnostica_Atendimento_AtendimentoId",
                        column: x => x.AtendimentoId,
                        principalTable: "Atendimento",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ImagemAtendimento",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    AtendimentoId = table.Column<Guid>(nullable: true),
                    Observacao = table.Column<string>(maxLength: 1000, nullable: true),
                    RowVersion = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ImagemAtendimento", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ImagemAtendimento_Atendimento_AtendimentoId",
                        column: x => x.AtendimentoId,
                        principalTable: "Atendimento",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Internacao",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    AtendimentoId = table.Column<Guid>(nullable: true),
                    DataInternacao = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Internacao", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Internacao_Atendimento_AtendimentoId",
                        column: x => x.AtendimentoId,
                        principalTable: "Atendimento",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Prescricao",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    AtendimentoId = table.Column<Guid>(nullable: true),
                    Dose = table.Column<string>(maxLength: 1000, nullable: true),
                    Duracao = table.Column<string>(maxLength: 1000, nullable: true),
                    EnfermeiroId = table.Column<Guid>(nullable: true),
                    EnvioEnfermagem = table.Column<DateTime>(nullable: false),
                    Intervalo = table.Column<string>(maxLength: 1000, nullable: true),
                    Medicamento = table.Column<string>(maxLength: 1000, nullable: true),
                    MedicoId = table.Column<Guid>(nullable: true),
                    ObservacaoEnfermagem = table.Column<string>(maxLength: 1000, nullable: true),
                    ObservacaoMedico = table.Column<string>(maxLength: 1000, nullable: true),
                    RetornoEnfermagem = table.Column<DateTime>(nullable: true),
                    RowVersion = table.Column<byte[]>(nullable: true),
                    StatusEnfermagem = table.Column<string>(maxLength: 1000, nullable: true),
                    Unidade = table.Column<string>(maxLength: 1000, nullable: true),
                    ViaAdministracao = table.Column<string>(maxLength: 1000, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Prescricao", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Prescricao_Atendimento_AtendimentoId",
                        column: x => x.AtendimentoId,
                        principalTable: "Atendimento",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Prescricao_Profissional_EnfermeiroId",
                        column: x => x.EnfermeiroId,
                        principalTable: "Profissional",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Prescricao_Profissional_MedicoId",
                        column: x => x.MedicoId,
                        principalTable: "Profissional",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ReceitaMedica",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    AtendimentoId = table.Column<Guid>(nullable: true),
                    Dose = table.Column<string>(maxLength: 1000, nullable: true),
                    Duracao = table.Column<string>(maxLength: 1000, nullable: true),
                    Intervalo = table.Column<string>(maxLength: 1000, nullable: true),
                    Medicamento = table.Column<string>(maxLength: 1000, nullable: true),
                    RowVersion = table.Column<byte[]>(nullable: true),
                    Unidade = table.Column<string>(maxLength: 1000, nullable: true),
                    ViaAdministracao = table.Column<string>(maxLength: 1000, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReceitaMedica", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ReceitaMedica_Atendimento_AtendimentoId",
                        column: x => x.AtendimentoId,
                        principalTable: "Atendimento",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ResultadoExame",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    AtendimentoId = table.Column<Guid>(nullable: true),
                    ExameId = table.Column<Guid>(nullable: true),
                    PacienteId = table.Column<Guid>(nullable: true),
                    RowVersion = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ResultadoExame", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ResultadoExame_Atendimento_AtendimentoId",
                        column: x => x.AtendimentoId,
                        principalTable: "Atendimento",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ResultadoExame_Exame_ExameId",
                        column: x => x.ExameId,
                        principalTable: "Exame",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ResultadoExame_Paciente_PacienteId",
                        column: x => x.PacienteId,
                        principalTable: "Paciente",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TemplatesAtestado",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    AtestadoId = table.Column<Guid>(nullable: true),
                    Conteudo = table.Column<string>(maxLength: 1000, nullable: true),
                    Nome = table.Column<string>(maxLength: 1000, nullable: true),
                    RowVersion = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TemplatesAtestado", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TemplatesAtestado_Atestados_AtestadoId",
                        column: x => x.AtestadoId,
                        principalTable: "Atestados",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "EvolucaoProcedimento",
                columns: table => new
                {
                    ProcedimentoId = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    EvolucaoId = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EvolucaoProcedimento", x => new { x.ProcedimentoId, x.EvolucaoId });
                    table.ForeignKey(
                        name: "FK_EvolucaoProcedimento_Evolucao_EvolucaoId",
                        column: x => x.EvolucaoId,
                        principalTable: "Evolucao",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EvolucaoProcedimento_Procedimento_ProcedimentoId",
                        column: x => x.ProcedimentoId,
                        principalTable: "Procedimento",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "HipoteseDiagnosticaCid",
                columns: table => new
                {
                    CidId = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    HipoteseDiagnosticaId = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HipoteseDiagnosticaCid", x => new { x.CidId, x.HipoteseDiagnosticaId });
                    table.ForeignKey(
                        name: "FK_HipoteseDiagnosticaCid_CID_CidId",
                        column: x => x.CidId,
                        principalTable: "CID",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_HipoteseDiagnosticaCid_HipoteseDiagnostica_HipoteseDiagnosticaId",
                        column: x => x.HipoteseDiagnosticaId,
                        principalTable: "HipoteseDiagnostica",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Imagem",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    Image = table.Column<byte[]>(nullable: true),
                    ImagemAtendimentoId = table.Column<Guid>(nullable: true),
                    RowVersion = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Imagem", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Imagem_ImagemAtendimento_ImagemAtendimentoId",
                        column: x => x.ImagemAtendimentoId,
                        principalTable: "ImagemAtendimento",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MovimentacaoLeito",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    ClinicaAtual1Id = table.Column<Guid>(nullable: true),
                    ClinicaId = table.Column<Guid>(nullable: true),
                    DataMovimentacao = table.Column<DateTime>(nullable: false),
                    InternacaoId = table.Column<Guid>(nullable: true),
                    LeitoAtualId = table.Column<Guid>(nullable: true),
                    UltimaMovimentacaoId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MovimentacaoLeito", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MovimentacaoLeito_Clinica_ClinicaAtual1Id",
                        column: x => x.ClinicaAtual1Id,
                        principalTable: "Clinica",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MovimentacaoLeito_Servico_ClinicaId",
                        column: x => x.ClinicaId,
                        principalTable: "Servico",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MovimentacaoLeito_Internacao_InternacaoId",
                        column: x => x.InternacaoId,
                        principalTable: "Internacao",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MovimentacaoLeito_Leito_LeitoAtualId",
                        column: x => x.LeitoAtualId,
                        principalTable: "Leito",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MovimentacaoLeito_MovimentacaoLeito_UltimaMovimentacaoId",
                        column: x => x.UltimaMovimentacaoId,
                        principalTable: "MovimentacaoLeito",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Laudo",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    ExameId = table.Column<Guid>(nullable: true),
                    RowVersion = table.Column<byte[]>(nullable: true),
                    UsuarioId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Laudo", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Laudo_ResultadoExame_ExameId",
                        column: x => x.ExameId,
                        principalTable: "ResultadoExame",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Laudo_AspNetUsers_UsuarioId",
                        column: x => x.UsuarioId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ResultadoExameElemento",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    Descricao = table.Column<string>(maxLength: 1000, nullable: true),
                    ElementoExameId = table.Column<Guid>(nullable: true),
                    ResultadoExameId = table.Column<Guid>(nullable: true),
                    RowVersion = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ResultadoExameElemento", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ResultadoExameElemento_ElementoExame_ElementoExameId",
                        column: x => x.ElementoExameId,
                        principalTable: "ElementoExame",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ResultadoExameElemento_ResultadoExame_ResultadoExameId",
                        column: x => x.ResultadoExameId,
                        principalTable: "ResultadoExame",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "EvolucaoEnfermagem",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    Evolucao = table.Column<string>(maxLength: 1000, nullable: true),
                    FrequenciaRespitaroria = table.Column<string>(maxLength: 1000, nullable: true),
                    HoraEvolucao = table.Column<DateTime>(nullable: false),
                    MovimentacaoLeitoId = table.Column<Guid>(nullable: true),
                    NumAtendimento = table.Column<string>(maxLength: 1000, nullable: true),
                    ObservacaoSinaisVitais = table.Column<string>(maxLength: 1000, nullable: true),
                    Peso = table.Column<string>(maxLength: 1000, nullable: true),
                    Pressao = table.Column<string>(maxLength: 1000, nullable: true),
                    ProfissionalId = table.Column<Guid>(nullable: true),
                    Pulso = table.Column<string>(maxLength: 1000, nullable: true),
                    RowVersion = table.Column<byte[]>(nullable: true),
                    Temperatura = table.Column<string>(maxLength: 1000, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EvolucaoEnfermagem", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EvolucaoEnfermagem_MovimentacaoLeito_MovimentacaoLeitoId",
                        column: x => x.MovimentacaoLeitoId,
                        principalTable: "MovimentacaoLeito",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EvolucaoEnfermagem_Profissional_ProfissionalId",
                        column: x => x.ProfissionalId,
                        principalTable: "Profissional",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "LiquidosAdministradosEnfermagem",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    LiquidosAdministradosId = table.Column<Guid>(nullable: true),
                    RowVersion = table.Column<byte[]>(nullable: true),
                    evolucaoEnfermagemId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LiquidosAdministradosEnfermagem", x => x.Id);
                    table.ForeignKey(
                        name: "FK_LiquidosAdministradosEnfermagem_LiquidosAdministrados_LiquidosAdministradosId",
                        column: x => x.LiquidosAdministradosId,
                        principalTable: "LiquidosAdministrados",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_LiquidosAdministradosEnfermagem_EvolucaoEnfermagem_evolucaoEnfermagemId",
                        column: x => x.evolucaoEnfermagemId,
                        principalTable: "EvolucaoEnfermagem",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "LiquidosEliminadosEnfermagem",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    LiquidosEliminadosId = table.Column<Guid>(nullable: true),
                    RowVersion = table.Column<byte[]>(nullable: true),
                    evolucaoEnfermagemId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LiquidosEliminadosEnfermagem", x => x.Id);
                    table.ForeignKey(
                        name: "FK_LiquidosEliminadosEnfermagem_LiquidosEliminados_LiquidosEliminadosId",
                        column: x => x.LiquidosEliminadosId,
                        principalTable: "LiquidosEliminados",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_LiquidosEliminadosEnfermagem_EvolucaoEnfermagem_evolucaoEnfermagemId",
                        column: x => x.evolucaoEnfermagemId,
                        principalTable: "EvolucaoEnfermagem",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ProcedimentosEnfermagem",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("MySql:ValueGeneratedOnAdd", true),
                    EvolucaoId = table.Column<Guid>(nullable: true),
                    Observacao = table.Column<string>(maxLength: 1000, nullable: true),
                    ProcedimentoId = table.Column<Guid>(nullable: true),
                    Quantidade = table.Column<int>(nullable: false),
                    RowVersion = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProcedimentosEnfermagem", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProcedimentosEnfermagem_EvolucaoEnfermagem_EvolucaoId",
                        column: x => x.EvolucaoId,
                        principalTable: "EvolucaoEnfermagem",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProcedimentosEnfermagem_Procedimento_ProcedimentoId",
                        column: x => x.ProcedimentoId,
                        principalTable: "Procedimento",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Assinatura_UsuarioId",
                table: "Assinatura",
                column: "UsuarioId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_ProfissionalId",
                table: "AspNetUsers",
                column: "ProfissionalId");

            migrationBuilder.CreateIndex(
                name: "IX_Acolhimento_PacienteId",
                table: "Acolhimento",
                column: "PacienteId");

            migrationBuilder.CreateIndex(
                name: "IX_Acolhimento_ProfissionalId",
                table: "Acolhimento",
                column: "ProfissionalId");

            migrationBuilder.CreateIndex(
                name: "IX_Acolhimento_ServicoId",
                table: "Acolhimento",
                column: "ServicoId");

            migrationBuilder.CreateIndex(
                name: "IX_Anamnese_AtendimentoId",
                table: "Anamnese",
                column: "AtendimentoId");

            migrationBuilder.CreateIndex(
                name: "IX_Anamnese_Ciap2Id",
                table: "Anamnese",
                column: "Ciap2Id");

            migrationBuilder.CreateIndex(
                name: "IX_Atendimento_AcolhimentoId",
                table: "Atendimento",
                column: "AcolhimentoId");

            migrationBuilder.CreateIndex(
                name: "IX_Atendimento_CheckoutId",
                table: "Atendimento",
                column: "CheckoutId");

            migrationBuilder.CreateIndex(
                name: "IX_Atendimento_MotivoCheckoutId",
                table: "Atendimento",
                column: "MotivoCheckoutId");

            migrationBuilder.CreateIndex(
                name: "IX_Atendimento_PrimeiroDiagnosticoId",
                table: "Atendimento",
                column: "PrimeiroDiagnosticoId");

            migrationBuilder.CreateIndex(
                name: "IX_Atendimento_ProfissionalId",
                table: "Atendimento",
                column: "ProfissionalId");

            migrationBuilder.CreateIndex(
                name: "IX_Atendimento_SegundoDiagnosticoId",
                table: "Atendimento",
                column: "SegundoDiagnosticoId");

            migrationBuilder.CreateIndex(
                name: "IX_AtendimentoProcedimento_AtendimentoId1",
                table: "AtendimentoProcedimento",
                column: "AtendimentoId1");

            migrationBuilder.CreateIndex(
                name: "IX_Atestados_AtendimentoId",
                table: "Atestados",
                column: "AtendimentoId");

            migrationBuilder.CreateIndex(
                name: "IX_Atestados_CIDId",
                table: "Atestados",
                column: "CIDId");

            migrationBuilder.CreateIndex(
                name: "IX_Checkout_MotivoCheckoutId",
                table: "Checkout",
                column: "MotivoCheckoutId");

            migrationBuilder.CreateIndex(
                name: "IX_CID_ProcedimentoId",
                table: "CID",
                column: "ProcedimentoId");

            migrationBuilder.CreateIndex(
                name: "IX_ClassificacaoDeRisco_AcolhimentoId",
                table: "ClassificacaoDeRisco",
                column: "AcolhimentoId");

            migrationBuilder.CreateIndex(
                name: "IX_ClassificacaoDeRisco_RiscoId",
                table: "ClassificacaoDeRisco",
                column: "RiscoId");

            migrationBuilder.CreateIndex(
                name: "IX_ClassificacaoDeRisco_SinaisVitaisId",
                table: "ClassificacaoDeRisco",
                column: "SinaisVitaisId");

            migrationBuilder.CreateIndex(
                name: "IX_ElementoExame_ExameId",
                table: "ElementoExame",
                column: "ExameId");

            migrationBuilder.CreateIndex(
                name: "IX_Evolucao_AtemdimentoId",
                table: "Evolucao",
                column: "AtemdimentoId");

            migrationBuilder.CreateIndex(
                name: "IX_Evolucao_MedicoId",
                table: "Evolucao",
                column: "MedicoId");

            migrationBuilder.CreateIndex(
                name: "IX_Evolucao_PrimeiroDiagnosticoId",
                table: "Evolucao",
                column: "PrimeiroDiagnosticoId");

            migrationBuilder.CreateIndex(
                name: "IX_Evolucao_SegundoDiagnosticoId",
                table: "Evolucao",
                column: "SegundoDiagnosticoId");

            migrationBuilder.CreateIndex(
                name: "IX_EvolucaoEnfermagem_MovimentacaoLeitoId",
                table: "EvolucaoEnfermagem",
                column: "MovimentacaoLeitoId");

            migrationBuilder.CreateIndex(
                name: "IX_EvolucaoEnfermagem_ProfissionalId",
                table: "EvolucaoEnfermagem",
                column: "ProfissionalId");

            migrationBuilder.CreateIndex(
                name: "IX_EvolucaoProcedimento_EvolucaoId",
                table: "EvolucaoProcedimento",
                column: "EvolucaoId");

            migrationBuilder.CreateIndex(
                name: "IX_Exame_ExamePaiId",
                table: "Exame",
                column: "ExamePaiId");

            migrationBuilder.CreateIndex(
                name: "IX_Exame_TipoExameId",
                table: "Exame",
                column: "TipoExameId");

            migrationBuilder.CreateIndex(
                name: "IX_ExameAtendimento_AtendimentoId",
                table: "ExameAtendimento",
                column: "AtendimentoId");

            migrationBuilder.CreateIndex(
                name: "IX_ExameAtendimento_ExameId",
                table: "ExameAtendimento",
                column: "ExameId");

            migrationBuilder.CreateIndex(
                name: "IX_ExameAtendimento_ExecutorId",
                table: "ExameAtendimento",
                column: "ExecutorId");

            migrationBuilder.CreateIndex(
                name: "IX_ExameAtendimento_MedicoId",
                table: "ExameAtendimento",
                column: "MedicoId");

            migrationBuilder.CreateIndex(
                name: "IX_HipoteseDiagnostica_AtendimentoId",
                table: "HipoteseDiagnostica",
                column: "AtendimentoId");

            migrationBuilder.CreateIndex(
                name: "IX_HipoteseDiagnosticaCid_HipoteseDiagnosticaId",
                table: "HipoteseDiagnosticaCid",
                column: "HipoteseDiagnosticaId");

            migrationBuilder.CreateIndex(
                name: "IX_Imagem_ImagemAtendimentoId",
                table: "Imagem",
                column: "ImagemAtendimentoId");

            migrationBuilder.CreateIndex(
                name: "IX_ImagemAtendimento_AtendimentoId",
                table: "ImagemAtendimento",
                column: "AtendimentoId");

            migrationBuilder.CreateIndex(
                name: "IX_Internacao_AtendimentoId",
                table: "Internacao",
                column: "AtendimentoId");

            migrationBuilder.CreateIndex(
                name: "IX_Laudo_ExameId",
                table: "Laudo",
                column: "ExameId");

            migrationBuilder.CreateIndex(
                name: "IX_Laudo_UsuarioId",
                table: "Laudo",
                column: "UsuarioId");

            migrationBuilder.CreateIndex(
                name: "IX_LiquidosAdministradosEnfermagem_LiquidosAdministradosId",
                table: "LiquidosAdministradosEnfermagem",
                column: "LiquidosAdministradosId");

            migrationBuilder.CreateIndex(
                name: "IX_LiquidosAdministradosEnfermagem_evolucaoEnfermagemId",
                table: "LiquidosAdministradosEnfermagem",
                column: "evolucaoEnfermagemId");

            migrationBuilder.CreateIndex(
                name: "IX_LiquidosEliminadosEnfermagem_LiquidosEliminadosId",
                table: "LiquidosEliminadosEnfermagem",
                column: "LiquidosEliminadosId");

            migrationBuilder.CreateIndex(
                name: "IX_LiquidosEliminadosEnfermagem_evolucaoEnfermagemId",
                table: "LiquidosEliminadosEnfermagem",
                column: "evolucaoEnfermagemId");

            migrationBuilder.CreateIndex(
                name: "IX_MotivoCheckout_TipoId",
                table: "MotivoCheckout",
                column: "TipoId");

            migrationBuilder.CreateIndex(
                name: "IX_MovimentacaoLeito_ClinicaAtual1Id",
                table: "MovimentacaoLeito",
                column: "ClinicaAtual1Id");

            migrationBuilder.CreateIndex(
                name: "IX_MovimentacaoLeito_ClinicaId",
                table: "MovimentacaoLeito",
                column: "ClinicaId");

            migrationBuilder.CreateIndex(
                name: "IX_MovimentacaoLeito_InternacaoId",
                table: "MovimentacaoLeito",
                column: "InternacaoId");

            migrationBuilder.CreateIndex(
                name: "IX_MovimentacaoLeito_LeitoAtualId",
                table: "MovimentacaoLeito",
                column: "LeitoAtualId");

            migrationBuilder.CreateIndex(
                name: "IX_MovimentacaoLeito_UltimaMovimentacaoId",
                table: "MovimentacaoLeito",
                column: "UltimaMovimentacaoId");

            migrationBuilder.CreateIndex(
                name: "IX_Prescricao_AtendimentoId",
                table: "Prescricao",
                column: "AtendimentoId");

            migrationBuilder.CreateIndex(
                name: "IX_Prescricao_EnfermeiroId",
                table: "Prescricao",
                column: "EnfermeiroId");

            migrationBuilder.CreateIndex(
                name: "IX_Prescricao_MedicoId",
                table: "Prescricao",
                column: "MedicoId");

            migrationBuilder.CreateIndex(
                name: "IX_ProcedimentoCid_ProcedimentoId",
                table: "ProcedimentoCid",
                column: "ProcedimentoId");

            migrationBuilder.CreateIndex(
                name: "IX_ProcedimentosEnfermagem_EvolucaoId",
                table: "ProcedimentosEnfermagem",
                column: "EvolucaoId");

            migrationBuilder.CreateIndex(
                name: "IX_ProcedimentosEnfermagem_ProcedimentoId",
                table: "ProcedimentosEnfermagem",
                column: "ProcedimentoId");

            migrationBuilder.CreateIndex(
                name: "IX_ReceitaMedica_AtendimentoId",
                table: "ReceitaMedica",
                column: "AtendimentoId");

            migrationBuilder.CreateIndex(
                name: "IX_ResultadoExame_AtendimentoId",
                table: "ResultadoExame",
                column: "AtendimentoId");

            migrationBuilder.CreateIndex(
                name: "IX_ResultadoExame_ExameId",
                table: "ResultadoExame",
                column: "ExameId");

            migrationBuilder.CreateIndex(
                name: "IX_ResultadoExame_PacienteId",
                table: "ResultadoExame",
                column: "PacienteId");

            migrationBuilder.CreateIndex(
                name: "IX_ResultadoExameElemento_ElementoExameId",
                table: "ResultadoExameElemento",
                column: "ElementoExameId");

            migrationBuilder.CreateIndex(
                name: "IX_ResultadoExameElemento_ResultadoExameId",
                table: "ResultadoExameElemento",
                column: "ResultadoExameId");

            migrationBuilder.CreateIndex(
                name: "IX_TemplatesAtestado_AtestadoId",
                table: "TemplatesAtestado",
                column: "AtestadoId");

            migrationBuilder.CreateIndex(
                name: "IX_TipoExame_TipoExamePaiId",
                table: "TipoExame",
                column: "TipoExamePaiId");

            migrationBuilder.CreateIndex(
                name: "IX_Bairro_CidadeId",
                table: "Bairro",
                column: "CidadeId");

            migrationBuilder.CreateIndex(
                name: "IX_Cidade_UFId",
                table: "Cidade",
                column: "UFId");

            migrationBuilder.CreateIndex(
                name: "IX_Enfermaria_UnidadeId",
                table: "Enfermaria",
                column: "UnidadeId");

            migrationBuilder.CreateIndex(
                name: "IX_Leito_ClinicaId",
                table: "Leito",
                column: "ClinicaId");

            migrationBuilder.CreateIndex(
                name: "IX_Leito_EnfermariaId",
                table: "Leito",
                column: "EnfermariaId");

            migrationBuilder.CreateIndex(
                name: "IX_Paciente_EnderecoId",
                table: "Paciente",
                column: "EnderecoId");

            migrationBuilder.CreateIndex(
                name: "IX_Paciente_NaturalidadeId",
                table: "Paciente",
                column: "NaturalidadeId");

            migrationBuilder.CreateIndex(
                name: "IX_Paciente_UnidadeId",
                table: "Paciente",
                column: "UnidadeId");

            migrationBuilder.CreateIndex(
                name: "IX_Profissional_CboId",
                table: "Profissional",
                column: "CboId");

            migrationBuilder.CreateIndex(
                name: "IX_Profissional_SetorClinicaId",
                table: "Profissional",
                column: "SetorClinicaId");

            migrationBuilder.CreateIndex(
                name: "IX_Profissional_UnidadeId",
                table: "Profissional",
                column: "UnidadeId");

            migrationBuilder.CreateIndex(
                name: "IX_ProfissionalServico_ServicoId",
                table: "ProfissionalServico",
                column: "ServicoId");

            migrationBuilder.CreateIndex(
                name: "IX_Unidade_EnderecoId",
                table: "Unidade",
                column: "EnderecoId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Assinatura");

            migrationBuilder.DropTable(
                name: "Commands");

            migrationBuilder.DropTable(
                name: "Event");

            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "AdministracaoMedicamento");

            migrationBuilder.DropTable(
                name: "Anamnese");

            migrationBuilder.DropTable(
                name: "AtendimentoProcedimento");

            migrationBuilder.DropTable(
                name: "Ciap2_Cid10");

            migrationBuilder.DropTable(
                name: "ClassificacaoDeRisco");

            migrationBuilder.DropTable(
                name: "EvolucaoProcedimento");

            migrationBuilder.DropTable(
                name: "ExameAtendimento");

            migrationBuilder.DropTable(
                name: "HipoteseDiagnosticaCid");

            migrationBuilder.DropTable(
                name: "Imagem");

            migrationBuilder.DropTable(
                name: "Laudo");

            migrationBuilder.DropTable(
                name: "LiquidosAdministradosEnfermagem");

            migrationBuilder.DropTable(
                name: "LiquidosEliminadosEnfermagem");

            migrationBuilder.DropTable(
                name: "Medicamento");

            migrationBuilder.DropTable(
                name: "Prescricao");

            migrationBuilder.DropTable(
                name: "ProcedimentoCid");

            migrationBuilder.DropTable(
                name: "ProcedimentosEnfermagem");

            migrationBuilder.DropTable(
                name: "ReceitaMedica");

            migrationBuilder.DropTable(
                name: "ResultadoExameElemento");

            migrationBuilder.DropTable(
                name: "TemplatesAtestado");

            migrationBuilder.DropTable(
                name: "UnidadeMedida");

            migrationBuilder.DropTable(
                name: "Bairro");

            migrationBuilder.DropTable(
                name: "LocalAtendimento");

            migrationBuilder.DropTable(
                name: "Painel");

            migrationBuilder.DropTable(
                name: "Pais");

            migrationBuilder.DropTable(
                name: "ProfissionalServico");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "Ciap2");

            migrationBuilder.DropTable(
                name: "Risco");

            migrationBuilder.DropTable(
                name: "SinaisVitais");

            migrationBuilder.DropTable(
                name: "Evolucao");

            migrationBuilder.DropTable(
                name: "HipoteseDiagnostica");

            migrationBuilder.DropTable(
                name: "ImagemAtendimento");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "LiquidosAdministrados");

            migrationBuilder.DropTable(
                name: "LiquidosEliminados");

            migrationBuilder.DropTable(
                name: "EvolucaoEnfermagem");

            migrationBuilder.DropTable(
                name: "ElementoExame");

            migrationBuilder.DropTable(
                name: "ResultadoExame");

            migrationBuilder.DropTable(
                name: "Atestados");

            migrationBuilder.DropTable(
                name: "MovimentacaoLeito");

            migrationBuilder.DropTable(
                name: "Exame");

            migrationBuilder.DropTable(
                name: "Clinica");

            migrationBuilder.DropTable(
                name: "Internacao");

            migrationBuilder.DropTable(
                name: "Leito");

            migrationBuilder.DropTable(
                name: "TipoExame");

            migrationBuilder.DropTable(
                name: "Atendimento");

            migrationBuilder.DropTable(
                name: "Enfermaria");

            migrationBuilder.DropTable(
                name: "Acolhimento");

            migrationBuilder.DropTable(
                name: "Checkout");

            migrationBuilder.DropTable(
                name: "CID");

            migrationBuilder.DropTable(
                name: "Paciente");

            migrationBuilder.DropTable(
                name: "Profissional");

            migrationBuilder.DropTable(
                name: "Servico");

            migrationBuilder.DropTable(
                name: "MotivoCheckout");

            migrationBuilder.DropTable(
                name: "Procedimento");

            migrationBuilder.DropTable(
                name: "Cidade");

            migrationBuilder.DropTable(
                name: "Cbo");

            migrationBuilder.DropTable(
                name: "SetorClinica");

            migrationBuilder.DropTable(
                name: "Unidade");

            migrationBuilder.DropTable(
                name: "TipoMotivoChekout");

            migrationBuilder.DropTable(
                name: "UF");

            migrationBuilder.DropTable(
                name: "Endereco");
        }
    }
}

﻿using AutoMapper;
using HealthCare.Domain.Entities.DbClasses.Admin;
using HealthCare.Domain.Entities.DbClasses.Atendimento;
using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Service.CQRS.Read.Models;

namespace HealthCare.Infra.CQRS.Read.Configuration
{
    public class DomainToReadViewModel : Profile
    {
        public DomainToReadViewModel()
        {

            CreateMap<Paciente, PacienteReadModel>();
            CreateMap<Naturalidade, NaturalidadeViewModel>();
            CreateMap<Clinica, ClinicaViewModel>();
            CreateMap<ElementoExame, ElementoExameReadViewModel>();
            CreateMap<Ciap2, Ciap2ReadViewModel>();
            CreateMap<Anamnese, AnamneseReadViewModel>();
            CreateMap<CID, CIDReadViewModel>();
            CreateMap<HipoteseDiagnostica, HipoteseDiagnosticaReadViewModel>();
            CreateMap<Imagem, ImagemReadViewModel>();
            CreateMap<ImagemAtendimento, ImagemAtendimentoReadViewModel>();
            CreateMap<Atestados, AtestadoReadViewModel>();
            CreateMap<Profissional, ProfissionalReadModel>();
            CreateMap<Servico, ServicoReadModel>();
            CreateMap<Unidade, UnidadeReadModel>();
            CreateMap<Endereco, EnderecoReadViewModel>();
            CreateMap<UF, UFReadModel>();
            CreateMap<Cidade, CidadeReadModel>();
            CreateMap<Bairro, BairroReadModel>();
            CreateMap<Usuario, UserReadViewModel>();
            CreateMap<Role, RoleReadModel>();
            CreateMap<UserRole, UserRoleViewModel>();
            CreateMap<Cbo, CboReadModel>();
            CreateMap<ClassificacaoDeRisco, ClassificacaoDeRiscoReadModel>();
            CreateMap<SinaisVitais, SinaisVitaisReadModel>();
            CreateMap<Acolhimento, AcolhimentoReadModel>();
            CreateMap<Atendimento, AtendimentoReadModel>();
            CreateMap<MotivoCheckout, MotivoCheckoutModel>();
            CreateMap<SetorClinica, SetorClinicaReadModel>();
            CreateMap<Risco, RiscoReadModel>();
            CreateMap<TipoMotivoChekout, TipoMotivoChekoutReadModel>();
            CreateMap<ServicoSocial, ServicoReadModel>();
            CreateMap<Procedimento, ProcedimentoReadModel>();
            CreateMap<Checkout, CheckoutReadViewModel>();
            CreateMap<ReceitaMedica, ReceitaMedicaReadModel>();
            CreateMap<Prescricao, PrescricaoReadModel>();
            CreateMap<Exame, ExameReadModel>();
            CreateMap<TipoExame, TipoExameReadModel>();
            CreateMap<ExameAtendimento, ExameAtendimentoReadModel>();
            CreateMap<Medicamento, MedicamentoReadModel>();
            CreateMap<UnidadeMedida, UnidadeMedicamentoReadModel>();
            CreateMap<AdministracaoMedicamento, AdministracaoMedicamentoReadModel>();
            CreateMap<Pais, PaisReadModel>();
            CreateMap<Enfermaria, EnfermariaReadModel>();
            CreateMap<Leito, LeitoReadModel>();
            CreateMap<Leito, LeitosOcupadosReadModel>();
            CreateMap<Internacao, InternacaoReadModel>();
            CreateMap<MovimentacaoLeito, MovimentacaoLeitoReadModel>();
            CreateMap<Evolucao, EvolucaoReadModel>();
            CreateMap<EvolucaoEnfermagem, EvolucaoEnfermagemReadModel>();
            CreateMap<LiquidosAdministrados, LiquidosAdministradosReadModel>();
            CreateMap<LiquidosEliminados, LiquidosEliminadosReadModel>();
            CreateMap<Enfermaria, EnfermariaReadModel>();
            CreateMap<Painel, ChamadaPainelModel>();
            CreateMap<LocalAtendimento, LocalAtendimentoViewModel>();
            CreateMap<ProcedimentosEnfermagem, ProcedimentosEnfermagemReadModel>();
            CreateMap<AtendimentoProcedimento, AtendimentoProcedimentoReadModel>();
            CreateMap<DelegacaoProfissional, DelegacaoProfissionalViewModel>();
        }
    }
}


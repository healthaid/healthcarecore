﻿using AutoMapper;
using HealthCare.Domain.Entities.DbClasses.Admin;
using HealthCare.Domain.Entities.DbClasses.Atendimento;
using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Service.CQRS.Read.Models;

namespace HealthCare.Infra.CQRS.Read.Configuration
{
    public class ReadViewModelToDomain : Profile
    {
        public ReadViewModelToDomain()
        {
            CreateMap<PacienteReadModel, Paciente>();
            CreateMap<NaturalidadeViewModel, Naturalidade>();
            CreateMap<ClinicaViewModel, Clinica>();
            //CreateMap<AgendaReadViewModel, Agenda>();
            CreateMap<ElementoExameReadViewModel, ElementoExame>();
            CreateMap<AnamneseReadViewModel, Anamnese>();
            CreateMap<CIDReadViewModel, CID>();
            CreateMap<HipoteseDiagnosticaReadViewModel, HipoteseDiagnostica>();
            CreateMap<ImagemReadViewModel, Imagem>();
            CreateMap<ImagemAtendimentoReadViewModel, ImagemAtendimento>();
            CreateMap<AtestadoReadViewModel, Atestados>();
            CreateMap<ProfissionalReadModel, Profissional>();
            CreateMap<ServicoReadModel, Servico>();
            CreateMap<UnidadeReadModel, Unidade>();
            CreateMap<EnderecoReadViewModel, Endereco>();
            CreateMap<UserReadViewModel, Usuario>();
            CreateMap<UFReadModel, UF>();
            CreateMap<CidadeReadModel, Cidade>();
            CreateMap<BairroReadModel, Bairro>();
            //CreateMap<FilaAtendimentoViewModel, AgendaEvento>();
            CreateMap<RoleReadModel, Role>();
            CreateMap<UserRoleViewModel, UserRole>();
            CreateMap<CboReadModel, Cbo>();
            CreateMap<ClassificacaoDeRiscoReadModel, ClassificacaoDeRisco>();
            CreateMap<SinaisVitaisReadModel, SinaisVitais>();
            CreateMap<AcolhimentoReadModel, Acolhimento>();
            CreateMap<AtendimentoReadModel, Atendimento>();
            CreateMap<MotivoCheckoutModel, MotivoCheckout>();
            CreateMap<SetorClinicaReadModel, SetorClinica>();
            CreateMap<RiscoReadModel, Risco>();
            CreateMap<TipoMotivoChekoutReadModel, TipoMotivoChekout>();
            CreateMap<ServicoReadModel, ServicoSocial>();
            CreateMap<ProcedimentoReadModel, Procedimento>();
            CreateMap<CheckoutReadViewModel, Checkout>();
            CreateMap<ReceitaMedicaReadModel, ReceitaMedica>();
            CreateMap<PrescricaoReadModel, Prescricao>();
            CreateMap<ExameReadModel, Exame>();
            CreateMap<TipoExameReadModel, TipoExame>();
            CreateMap<ExameAtendimentoReadModel, ExameAtendimento>();
            CreateMap<MedicamentoReadModel, Medicamento>();
            CreateMap<UnidadeMedicamentoReadModel, UnidadeMedida>();
            CreateMap<AdministracaoMedicamentoReadModel, AdministracaoMedicamento>();
            CreateMap<PaisReadModel, Pais>();
            CreateMap<EnfermariaReadModel, Enfermaria>();
            CreateMap<LeitoReadModel, Leito>();
            CreateMap<LeitosOcupadosReadModel, Leito>();
            CreateMap<InternacaoReadModel, Internacao>();
            CreateMap<MovimentacaoLeitoReadModel, MovimentacaoLeito>();
            CreateMap<EvolucaoReadModel, Evolucao>();
            CreateMap<EvolucaoEnfermagemReadModel, EvolucaoEnfermagem>();
            CreateMap<LiquidosAdministradosReadModel, LiquidosAdministrados>();
            CreateMap<LiquidosEliminadosReadModel, LiquidosEliminados>();
            CreateMap<EnfermariaReadModel, Enfermaria>();
            CreateMap<ChamadaPainelModel, Painel>();
            CreateMap<LocalAtendimentoViewModel, LocalAtendimento>();
            CreateMap<ProcedimentosEnfermagemReadModel, ProcedimentosEnfermagem>();
            CreateMap<AtendimentoProcedimentoReadModel, AtendimentoProcedimento>();
            CreateMap<DelegacaoProfissionalViewModel, DelegacaoProfissional>();

        }
    }
}

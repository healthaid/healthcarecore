﻿using HealthCare.Infra.Common.Result;
using HealthCare.Service.CQRS.Read.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace HealthCare.Service.CQRS.Read.Interfaces.Cadastro
{
    public interface IDelegacaoProfissionalDao
    {
        ListResult ListarDelegacoesAtivas(Guid unidadeId);
        IList<DelegacaoProfissionalViewModel> ListarDelegacoesPorDelegado(Guid idProfissionalDelegado);
    }
}
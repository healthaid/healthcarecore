﻿using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Read.Models;
using System;

namespace HealthCare.Infra.CQRS.Read.Interfaces
{
    public interface ILeitoDao
    {
        ListResult ListaLeitos(bool ativo);
        LeitoReadModel GetLeitoById(Guid idLeito);
        ListResult ListarLeitosPorTipo(int tipoLeito);
        ListResult ListarLeitosOcupados();
        ListResult ListarLeitosDisponiveis();
        ListResult ListarLeitosDisponiveisPorEnfermaria(Guid unidadeId);
        ListResult ListarLeitosPorEnfermaria(Guid enfermariaId, bool? ativo);
        ListResult ListarLeitosOcupadosPorEnfermaria(Guid unidadeId);
        ListResult ListarLeitosOcupadosParaPermuta(Guid unidadeId, Guid pacienteSelecionadoId);
    }
}

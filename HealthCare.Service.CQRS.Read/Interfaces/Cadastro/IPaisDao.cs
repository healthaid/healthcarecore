﻿using HealthCare.Domain.Entities;
using HealthCare.Infra.Common.Result;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Interfaces
{
    public interface IPaisDao
    {
        ListResult ListaPaises();
        ListResult ListaPais(string nome);
    }
}

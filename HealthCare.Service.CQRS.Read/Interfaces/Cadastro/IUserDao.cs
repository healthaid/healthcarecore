﻿using HealthCare.Infra.CQRS.Read.Models;
using System;
using System.Collections.Generic;

namespace HealthCare.Infra.CQRS.Read.Interfaces.Cadastro
{
    public interface IUserDao
    {
        UserReadViewModel VerifyIfUserExists(string email, string password);

        List<RoleReadModel> GetRoles();

        UserReadViewModel GetUserById(Guid id);

        UserReadViewModel GetUserById(Guid id, Guid idDelegacao);

        UserReadViewModel GetUserByProfissionalId(Guid profissionalId);
    }
}

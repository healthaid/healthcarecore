﻿using HealthCare.Infra.Common.Result;
using System;

namespace HealthCare.Infra.CQRS.Read.Interfaces
{
    public interface IBairroDao
    {
        ListResult ListaBairros();
        ListResult ListarBairrosPorCidade(Guid idCidade);
    }
}

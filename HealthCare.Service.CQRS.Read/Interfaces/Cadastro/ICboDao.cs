﻿using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Read.Models;
using System;
using System.Collections.Generic;
using System.Linq;
namespace HealthCare.Infra.CQRS.Read.Interfaces.Cadastro
{
    public interface ICboDao
    {
        IList<CboReadModel> ListarCbos(string texto);
        CboReadModel ObterCboPorCodigo(string codigo);
        CboReadModel ObterCboPorId(Guid id);
        IList<CboReadModel> ListarTodasCbos();
    }
}

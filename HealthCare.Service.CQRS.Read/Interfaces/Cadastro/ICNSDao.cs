﻿using HealthCare.Domain.Entities.DbClasses.Cadastro;
using System;
using System.Collections.Generic;

namespace HealthCare.Infra.CQRS.Read.Interfaces
{
    public interface ICNSDao
    {
        IList<CNS> ListarCNSs();
        IList<CNS> ListarCNSPorPaciente(Guid pacienteId);
    }
}

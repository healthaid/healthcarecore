﻿using HealthCare.Domain.Entities.DbClasses.Cadastro;
using System;
using System.Collections.Generic;

namespace HealthCare.Infra.CQRS.Read.Interfaces
{
    public interface IConvenioDao
    {
        IList<Convenio> ListarConvenios();
        IList<Convenio> ListarConveniosPorPaciente(Guid pacienteId);
    }
}

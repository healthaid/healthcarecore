﻿using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Read.Models;
using System;
using System.Collections.Generic;

namespace HealthCare.Infra.CQRS.Read.Interfaces
{
    public interface ICidadeDao
    {
        ListResult ListaCidades();
        ListResult ListarCidadesporUF(Guid idUF);

        IList<CidadeReadModel> ListarCidadeporUF(Guid idUF);   
    }
}

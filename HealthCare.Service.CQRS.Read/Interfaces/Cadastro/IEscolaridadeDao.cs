﻿using HealthCare.Domain.Entities.DbClasses.Cadastro;
using System.Collections.Generic;

namespace HealthCare.Infra.CQRS.Read.Interfaces
{
    public interface IEscolaridadeDao
    {
        IList<Escolaridade> ListarEscolaridade();
    }
}

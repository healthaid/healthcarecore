﻿using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Infra.CQRS.Read.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Interfaces
{
    public interface IEnderecoDao
    {
        IList<Endereco> ListarEnderecos();
        Task<EnderecoViewModel> GetEnderecoByCepAsync(string cep);
    }
}

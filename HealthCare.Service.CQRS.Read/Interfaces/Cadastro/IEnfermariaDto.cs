﻿using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Read.Models;
using System;

namespace HealthCare.Infra.CQRS.Read.Interfaces
{
    public interface IEnfermariaDao
    {
        ListResult ListaEnfermarias(bool ativas, Guid unidadeId);
        EnfermariaReadModel GetEnfermariaById(Guid id);        
    }
}

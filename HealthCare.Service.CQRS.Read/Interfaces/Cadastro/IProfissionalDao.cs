﻿using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Read.Models;
using System;
using System.Collections.Generic;

namespace HealthCare.Infra.CQRS.Read.Interfaces
{
    public interface IProfissionalDao
    {
        IList<ProfissionalReadModel> ListarProfissional(Guid unidadeId);
        ListResult ListarProfissional(string filtro, Guid unidadeId);
        ProfissionalReadModel GetProfissionalUnidade(Guid profissionalId);
        ProfissionalReadModel GetProfissionalById(Guid ProfissionalId);
        ProfissionalReadModel GetUserByProfissionalId(Guid ProfissionalId);
        bool ProfissionalEMedico(Guid id);
        IList<ProfissionalReadModel> ListarProfissionaisPorCbos(IEnumerable<string> listaCbos);
    }
}

﻿using HealthCare.Infra.Common.ResultModels.List;
using HealthCare.Infra.CQRS.Read.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Interfaces.Cadastro
{
    public interface IRoleDao
    {
        List<RoleReadModel> GetRoles();
    }
}

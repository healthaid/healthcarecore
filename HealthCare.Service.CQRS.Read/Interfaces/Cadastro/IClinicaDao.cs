﻿
using HealthCare.Infra.CQRS.Read.Models;
using System.Collections.Generic;
namespace HealthCare.Infra.CQRS.Read.Interfaces
{
    public interface IClinicaDao
    {
        IList<ClinicaViewModel> ListarClinicas();
    }
}

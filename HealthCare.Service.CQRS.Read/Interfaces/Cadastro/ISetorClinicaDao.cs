﻿using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Read.Models;
using System;

namespace HealthCare.Infra.CQRS.Read.Interfaces
{
    public interface ISetorClinicaDao
    {
        ListResult ListaSetorClinica();
        SetorClinicaReadModel GetSetorClinicaById(Guid id);
    }
}

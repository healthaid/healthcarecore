﻿using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Read.Models;
using System;

namespace HealthCare.Infra.CQRS.Read.Interfaces
{
    public interface IReceitaMedicaDto
    {
        ReceitaMedicaReadModel ObterReceitaMedicaPorId(Guid id);
        ListResult ObterReceitasMedicaPorAtendimentoId(Guid atendimentoId);
        ListResult ImpressaoReceita(Guid atendimentoId);
    }
}

﻿using HealthCare.Infra.CQRS.Read.Models;
using System;
using System.Collections.Generic;
namespace HealthCare.Infra.CQRS.Read.Interfaces.Atendimento
{
    public interface ITipoExameDto
    {
        TipoExameReadModel GetTipoExameById(Guid id);
        IList<TipoExameReadModel> ListarTipoExames(string descricao);
    }
}

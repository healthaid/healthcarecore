﻿using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Read.Models;
using System;

namespace HealthCare.Infra.CQRS.Read.Interfaces.Atendimento
{
    public interface IEvolucaoDto
    {
        EvolucaoReadModel GetById(Guid id);
        ListResult ListarEvolucaoPorAtendimento(Guid id);
        ListResult ListarEvolucaoMedica(DateTime inicio, DateTime fim, Guid UnidadeId);
    }
}

﻿using HealthCare.Infra.CQRS.Read.Models;
using System;
using System.Collections.Generic;
namespace HealthCare.Infra.CQRS.Read.Interfaces
{
    public interface IImagemDao 
    {
        ImagemReadViewModel ObterImagemPorId(Guid id);
        IList<ImagemReadViewModel> ObterImagensPorImagemAtendimento(Guid idAtentimento);
    }
}

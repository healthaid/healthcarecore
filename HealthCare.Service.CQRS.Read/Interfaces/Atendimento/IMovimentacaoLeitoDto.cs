﻿using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Read.Models;
using System;

namespace HealthCare.Infra.CQRS.Read.Interfaces.Atendimento
{
    public interface IMovimentacaoLeitoDto
    {
        ListResult ListarLeitosOcupados(Guid unidadeId);
        MovimentacaoLeitoReadModel GetMovimentacaoLeito(Guid id);
        MovimentacaoLeitoReadModel GetUltimaMovimentacaoPorLeito(Guid id);
        MovimentacaoLeitoReadModel GetLeitoCorrentePorAtendimento(Guid id);
        ListResult ListarObservacaoDiaria(DateTime inicio, DateTime fim, Guid unidadeId);
    }
}

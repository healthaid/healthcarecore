﻿using HealthCare.Infra.CQRS.Read.Models;
using System;
using System.Collections.Generic;
namespace HealthCare.Infra.CQRS.Read.Interfaces
{
    public interface IAdministracaoMedicamentoDto
    {
        IList<AdministracaoMedicamentoReadModel> ListarTodosAdministracaoMedicamentos(string texto);

        IList<AdministracaoMedicamentoReadModel> ListarTodosAdministracaoMedicamentos();
    }
}

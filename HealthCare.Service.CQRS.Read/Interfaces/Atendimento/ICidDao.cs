﻿using HealthCare.Infra.CQRS.Read.Models;
using System;
using System.Collections.Generic;
namespace HealthCare.Infra.CQRS.Read.Interfaces
{
    public interface ICidDao
    {
        IList<CIDReadViewModel> ListarCids();
        CIDReadViewModel ObterCid(Guid Id);

        IList<CIDReadViewModel> ListarTodosCids(string texto);
        IEnumerable<CIDReadViewModel> GetCidsParaCheckout(Guid atendimentoId);
    }
}

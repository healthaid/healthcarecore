﻿using HealthCare.Infra.CQRS.Read.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Interfaces.Atendimento
{
    public interface IClassificacaoDeRiscoDto
    {
        ClassificacaoDeRiscoReadModel ObterClassificacaoDeRisco(Guid acolhimentoId);

        string[] ClassificacoesDeRisco();
    }
}
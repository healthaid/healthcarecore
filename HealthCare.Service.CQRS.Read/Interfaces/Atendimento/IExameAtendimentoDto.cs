﻿using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Read.Models;
using System;
using System.Collections.Generic;
namespace HealthCare.Infra.CQRS.Read.Interfaces.Atendimento
{
    public interface IExameAtendimentoDto
    {
        ExameAtendimentoReadModel GetExameAtendimentoById(Guid id);
        ListResult ListarExameAtendimentosByAtendimentoId(Guid id);
        ListResult ListarExameAtendimentosByAtendimentoId(Guid id, string definicao, string origem);
        ListResult ListarExameAtendimentosPendentePorAtendimento(string definicao, Guid atendimentoId);
        ListResult ListarExameAtendimentosPendente(string definicao);
        ListResult ListarExamesRaioX(DateTime inicio, DateTime fim, Guid unidadeId);
    }
}

﻿using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Read.Models;
using System;
using System.Collections.Generic;

namespace HealthCare.Infra.CQRS.Read.Interfaces
{
    public interface IServicoDao
    {
        IList<Servico> ListarServicos();
        ListResult ListarServicos(string filtro);
        IList<ServicoReadModel> ListarTodosServicos();
        IList<Servico> ListarServicosPorProfissional(Guid profissionalId);
        ServicoReadModel ObterServicoPorId(Guid id);

    }
}

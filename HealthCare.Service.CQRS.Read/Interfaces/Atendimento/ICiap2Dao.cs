﻿using HealthCare.Infra.CQRS.Read.Models;
using System;
using System.Collections.Generic;
namespace HealthCare.Infra.CQRS.Read.Interfaces
{
    public interface ICiap2Dao
    {
        IList<Ciap2ReadViewModel> ListarCiaps();
        Ciap2ReadViewModel ObterCiapPorAtendimento(Guid idAtendimento);
        IList<Ciap2ReadViewModel> ListarCiaps(string descricao);
    }
}

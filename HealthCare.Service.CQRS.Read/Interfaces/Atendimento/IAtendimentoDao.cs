﻿

using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Service.CQRS.Read.Models;
using System;
namespace HealthCare.Infra.CQRS.Read.Interfaces.Atendimento
{
    public interface IAtendimentoDto
    {
        ListResult ListarAtendimentosDoDia(Guid Unidade, Guid idProfissional, DateTime? dataInicio, DateTime? datafim);
        ListResult ListarAtendimentos(Guid Unidade, DateTime inicio, Guid idProfissional, DateTime? Fim = null);
        AtendimentoReadModel ObterAtendimentoPorAcolhimento(Guid idAcolhimeto);
        AtendimentoReadModel ObterAtendimentoPorId(Guid atendimentoId);
        BoletimReadModel ObterBoletimPorAcolhimento(Guid idAcolhimento);
        ListResult GetFilaParaAtendimento(Guid unidadeId);
        ListResult GetProcedimentosByAtendimento(Guid atendimentoId);
        ListResult GetProcedimentosByAtendimentoDataServico(DateTime inicio, DateTime fim, Guid servico);
        ListResult ListarInternacoes(Guid unidadeId);
        ListResult ListarInternacoesComAlta(Guid unidadeId);
        ListResult ListarSPAMunicipioBairroFaixaEtaria(DateTime inicio, DateTime fim, string municipio);
        ListResult ListarPacientesRegistradosNoDia(DateTime inicio, DateTime fim, Guid UnidadeId);
        ListResult ListarProducaoDiariaPorSetor(DateTime inicio, DateTime fim, Guid UnidadeId);
        ListResult ListarFilaEspera(Guid UnidadeId);
        ListResult ListarPendentesERealizados(DateTime inicio, DateTime fim, Guid UnidadeId);
        ListResult ListaNominalDeObitos(DateTime inicio, DateTime fim, Guid UnidadeId);
        ListResult EstatisticaAtendimentoPorProfissional(DateTime inicio, DateTime fim, Guid UnidadeId);
        ListResult EstatisticaAtendimentoTotalPorProfissional(DateTime inicio, DateTime fim, Guid UnidadeId);
        ListResult EstatisticaMensalProntoAtendimento(int mes, int ano, Guid UnidadeId);
        ListResult ListarDiagnosticosMaisAtendidos(DateTime inicio, DateTime fim, Guid UnidadeId);
        ListResult ListarProducaoResumoAtendimento(DateTime inicio, DateTime fim, Guid UnidadeId);
        ExtratoContaPacienteReadModel ExtratoContaPaciente(string codigoBoletim);
        ListResult ListarProntoAtendimentoTempoPermanencia(DateTime inicio, DateTime fim, Guid UnidadeId);
        ListResult ListarRegistroHoraUrgenciaClinica(DateTime inicio, DateTime fim, Guid UnidadeId);
        ListResult ListarSpaDiagnosticoSexoFaixaEtaria(DateTime inicio, DateTime fim, Guid UnidadeId);
        ListResult ListarPacientesAtendidosEspecialidadeRisco(DateTime inicio, DateTime fim, Guid UnidadeId);
        ListResult ListarPacientesInternadosMais24Horas(Guid UnidadeId, string paciente, Guid enfermariaId, Guid leitoId, DateTime? dataInternacao, Guid profissionalId);
        ListResult ListarCensoDiarioUrgenciaEmergencia(Guid UnidadeId);
        ListResult ListarProducaoDiariaPorExame(int mes, int ano, Guid UnidadeId, String classificacao);
        ListResult ListarEstatisticaProducaoExames(DateTime inicio, DateTime fim, Guid UnidadeId, String classificacao);
        ListResult ListarMapaDiarioLeitos(Guid UnidadeId);
        ListResult EstatistaOdontoProcedimentoDario(DateTime inicio, DateTime fim, Guid UnidadeId);
        ListResult GetAtendimentosByPaciente(Guid pacienteId, Guid unidadeId);
        AtendimentoReadModel ObterAtendimentoPorPaciente(Guid pacienteId);
        HistoricoAtendimentoReadModel GetHistoricoAtendimento(Guid AtendimentoId);
        ListResult ListarAtendimentosFinalizados(Guid Unidade, Guid idProfissional, DateTime? dataInicio, DateTime? datafim);
        ListResult ListarAtendimentosFinalizados(Guid Unidade, string filtro);
    }
}
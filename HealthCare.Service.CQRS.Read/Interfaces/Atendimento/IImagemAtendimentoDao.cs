﻿
using HealthCare.Infra.CQRS.Read.Models;
using System;
using System.Collections.Generic;
namespace HealthCare.Infra.CQRS.Read.Interfaces
{
    public interface IImagemAtendimentoDao
    {
        ImagemAtendimentoReadViewModel ObterImagemAtendimentoPorId(Guid id);
        IList<ImagemAtendimentoReadViewModel> ListarImagemAtendimento(Guid atendimentoId);
    }
}

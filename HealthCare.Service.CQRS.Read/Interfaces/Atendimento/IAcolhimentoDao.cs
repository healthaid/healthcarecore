﻿using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Read.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Interfaces.Atendimento
{
    public interface IAcolhimentoDTO
    {
        ListResult FilaAcolhimento(Guid profissionalId);

        PacienteReadModel ObterPacientePorAcolhimento(Guid acolhimentoId);

        AcolhimentoReadModel ObterAcolhimentoPorId(Guid acolhimentoId);

        AcolhimentoReadModel GetAcolhimentoWithClassificacaoRico(Guid acolhimentoId);
        ListResult GetAcolhimentosWithoutClassificacaoRisco(Guid unidadeId);

        ListResult GetClassificacaoRiscoList(Guid unidadeId);

        HeaderAtendimentoReadModel ObterInformacoesHeaderAtendimento(Guid acolhimentoId);
    }
}

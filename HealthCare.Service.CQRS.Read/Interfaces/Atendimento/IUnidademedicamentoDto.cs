﻿using HealthCare.Infra.CQRS.Read.Models;
using System;
using System.Collections.Generic;
namespace HealthCare.Infra.CQRS.Read.Interfaces
{
    public interface IUnidadeMedicamentoDto
    {
        IList<UnidadeMedicamentoReadModel> ListarTodosUnidadeMedicamentos(string texto);
        IList<UnidadeMedicamentoReadModel> ListarTodosUnidadeMedicamentos();
    }
}

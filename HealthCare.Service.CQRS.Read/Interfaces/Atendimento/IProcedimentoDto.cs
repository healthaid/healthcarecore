﻿using HealthCare.Infra.CQRS.Read.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Interfaces.Atendimento
{
    public interface IProcedimentoDto
    {
        List<ProcedimentoReadModel> ListarTodosProcedimentos(string texto);
        List<ProcedimentoReadModel> ListarProcedimentosPorCID(string codigo);
        List<ProcedimentoReadModel> ListarProcedimentosPorCID(string codigo, string texto);
    }
}

﻿
using HealthCare.Infra.CQRS.Read.Models;
using System;
using System.Collections.Generic;
namespace HealthCare.Infra.CQRS.Read.Interfaces
{
    public interface IAnamneseDao
    {
        IList<AnamneseReadViewModel> ListarAnamneses();
        AnamneseReadViewModel ObterAnamnese(Guid id);
        AnamneseReadViewModel ObterAnamnesePorAtendimento(Guid idAtendimento);
    }
}

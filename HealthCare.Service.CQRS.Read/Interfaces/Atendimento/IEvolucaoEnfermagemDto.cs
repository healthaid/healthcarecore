﻿using HealthCare.Infra.CQRS.Read.Models;
using System;
using HealthCare.Infra.Common.Result;

namespace HealthCare.Infra.CQRS.Read.Interfaces.Atendimento
{
    public interface IEvolucaoEnfermagemDto
    {
        EvolucaoEnfermagemReadModel GetEvolucaoEnfermagemById(Guid id);
        ListResult ListaHistoricoEnfermagem(Guid internacaoId);
        ListResult ListaLiquidosAdministrados(Guid evolucaoEnfermagemId);
        ListResult ListaLiquidosEliminados(Guid evolucaoEnfermagemId);
        ListResult ListaProcedimentosPorEvolucao(Guid evolucaoEnfermagemId);
    }
}

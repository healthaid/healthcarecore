﻿using HealthCare.Infra.CQRS.Read.Models;
using System;

namespace HealthCare.Infra.CQRS.Read.Interfaces.Atendimento
{
    public interface IAtestadoDao
    {
        AtestadoReadViewModel ObterAtestadoPorAtendimento(Guid idAtendimento);
        AtestadoReadViewModel ObterAtestado(Guid id);
        AtestadoReportReadModel ObterReportPorAtendimento(Guid idAtendimento);

    }
}

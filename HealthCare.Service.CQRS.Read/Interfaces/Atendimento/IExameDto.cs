﻿using HealthCare.Infra.CQRS.Read.Models;
using System;
using System.Collections.Generic;
namespace HealthCare.Infra.CQRS.Read.Interfaces.Atendimento
{
    public interface IExameDto
    {
        ExameReadModel GetExameById(Guid id);
        IList<ExameReadModel> ListarExames(string descricao, string tipo);
    }
}

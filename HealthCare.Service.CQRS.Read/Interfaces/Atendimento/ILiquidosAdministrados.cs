﻿using HealthCare.Infra.CQRS.Read.Models;
using System.Collections.Generic;

namespace HealthCare.Infra.CQRS.Read.Interfaces.Atendimento
{
    public interface ILiquidosAdministradosDto
    {
        List<LiquidosAdministradosReadModel> ListarLiquidosAdministrados();

    }
}

﻿using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Read.Models;
using System;
using System.Collections.Generic;
namespace HealthCare.Infra.CQRS.Read.Interfaces
{
    public interface IPrescricaoDto
    {
        PrescricaoReadModel ObterPrescricaoPorId(Guid id);
        ListResult ListarPrescricoesPorAtendimentoId(Guid atendimentoId);
        ListResult ListarPrescricoesPendentes(Guid UnidadeId);
        ListResult ListarPrescricoesPendentesPorAtendimento(Guid atendimentoId);
        ListResult ListarProducaoEnfermagem(int mes, int ano, Guid unidadeId);
        ListResult ImpressaoPrescricao(Guid atendimentoId);
        ListResult ListarPrescricoesPendentesAtendimentosFechados(Guid unidadeId, string competencia);
    }
}

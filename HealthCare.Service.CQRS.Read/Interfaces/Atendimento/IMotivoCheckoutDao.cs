﻿using HealthCare.Infra.Common.Result;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Interfaces.Atendimento
{
    public interface IMotivoCheckoutDto
    {
        ListResult ListarMotivosAtivos();
        ListResult ListarMotivosPorTipo(Guid tipoId);

    }
}

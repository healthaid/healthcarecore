﻿using HealthCare.Infra.CQRS.Read.Models;
using System;
namespace HealthCare.Infra.CQRS.Read.Interfaces
{
    public interface IHipoteseDiagnosticaDao 
    {
        HipoteseDiagnosticaReadViewModel ObterHipoteseDiagnosticaPorAtendimento(Guid atendimentoId);
        HipoteseDiagnosticaReadViewModel ObterHipoteseDiagnostica(Guid Id);
    }
}

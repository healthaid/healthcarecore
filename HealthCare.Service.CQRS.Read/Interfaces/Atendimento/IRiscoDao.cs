﻿using HealthCare.Infra.Common.Result;

namespace HealthCare.Infra.CQRS.Read.Interfaces
{
    public interface IRiscoDao
    {
        ListResult ListarRiscos();
    }
}

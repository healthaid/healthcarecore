﻿

using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Read.Models;
using System;
namespace HealthCare.Infra.CQRS.Read.Interfaces.Atendimento
{
    public interface IServicoSocialDto
    {
        ListResult ListarServicoSocial(Guid Unidade, Guid idProfissional);
        ListResult ListarServicoSocials(Guid Unidade, DateTime inicio, Guid idProfissional, DateTime? Fim = null);
        ServicoSocialReadModel ObterServicoSocialPorAcolhimento(Guid idAcolhimeto);
        BoletimReadModel ObterBoletimPorAcolhimento(Guid idAcolhimento);
    }
}

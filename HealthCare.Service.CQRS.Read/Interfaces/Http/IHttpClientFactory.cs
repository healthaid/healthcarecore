﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using static HealthCare.Service.CQRS.Read.Implementation.Voz.Synthesize;

namespace HealthCare.Service.CQRS.Read.Interfaces.Http
{
    public interface IHttpClientFactory
    {
        HttpClient ReturnHttpClient();

        InputOptions ReturnOptions();

        HttpClient ReturnTokenClient();

        HttpClient ReturnSingleHttpClient();
    }
}

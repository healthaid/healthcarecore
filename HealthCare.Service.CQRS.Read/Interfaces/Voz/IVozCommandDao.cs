﻿using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Read.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Interfaces.Voz
{
    public interface IVozCommandDao
    {
        List<ChamadaPainelModel> GetPacientesParaSeremChamados(Guid unidadeId);
        ListResult GetUltimosPacientesChamados(Guid unidadeId);
    }
}

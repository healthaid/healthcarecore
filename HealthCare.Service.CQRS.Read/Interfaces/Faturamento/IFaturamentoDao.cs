﻿using HealthCare.Service.CQRS.Read.Models;
using System;
using System.Collections.Generic;

namespace HealthCare.Service.CQRS.Read.Interfaces.Faturamento
{
    public interface IFaturamentoDao
    {
        List<FaturamentoModel> ListaFaturamento(string unidadeId, int mes, int ano);

        List<FaturamentoModel> ListaAtendimentosParaFaturamento(string unidadeId, int mes, int ano);

        List<FaturamentoModel> ListaParaFaturamentoPorPeriodo(string unidadeId, DateTime inicio, DateTime fim);


    }
}

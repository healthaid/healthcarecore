﻿using HealthCare.Domain.Entities;
using HealthCare.Domain.Entities.Agenda;
using HealthCare.Infra.CQRS.Read.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Interfaces
{
    public interface IAgendaDao
    {
        IList<AgendaReadViewModel> ListarAgendas(Guid unidadeId);
    }
}

﻿using HealthCare.Domain.Entities;
using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Read.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Interfaces
{
    public interface IAgendaEventoDao
    {
        IList<AgendaEvento> ListarEventos(Guid agendaId);
        IList<AgendaEvento> ListarEventosPorProfissional(Guid profissionalId);
        IList<AgendaEvento> ListarEventosPorPaciente(Guid pacienteId);
        IList<AgendaEvento> ListarEventosPorMes(int mes, int ano, Guid unidadeId);
        IList<AgendaEvento> ListarEventosPorAno(int ano, Guid unidadeId);

        AgendaEvento GetEventoPorId(Guid eventoId);
        ListResult ListarFilaProfissional(Guid profissionalId);
        IList<AgendaEvento> ListarEventosPorSemana(DateTime inicio, DateTime fim, Guid profissionalId);
    }
}

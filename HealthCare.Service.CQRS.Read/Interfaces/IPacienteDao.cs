﻿using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Read.Models;
using System;
using System.Collections.Generic;

namespace HealthCare.Infra.CQRS.Read.Interfaces
{
    public interface IPacienteDao
    {
        IList<PacienteReadModel> ListarPacientesExcuidos();
        ListResult ListarPacientes(string filtro, Guid unidadeId);
        IList<PacienteReadModel> ListarPacientesAtivos(Guid unidadeId);
        PacienteReadModel GetPacienteById(Guid pacienteId);
        PacienteReadModel GetPacienteByAtendimentoId(Guid atendimentoId);
    }
}

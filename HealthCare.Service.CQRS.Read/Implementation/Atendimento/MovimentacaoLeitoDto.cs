﻿using AutoMapper;
using HealtCare.Infra.Common.Utils;
using HealthCare.Domain.Entities.DbClasses.Atendimento;
using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Read.Interfaces.Atendimento;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HealthCare.Infra.CQRS.Read.Implementation
{
    public class MovimentacaoLeitoDto : IMovimentacaoLeitoDto
    {
        private HealthCareDbContext repository;
        private IMapper mapper;
        public MovimentacaoLeitoDto(HealthCareDbContext contextFactory, IMapper mapper)
        {
            this.repository = contextFactory;
            this.mapper = mapper;
            ViewModelUtils.mapper = mapper;
        }

        public MovimentacaoLeitoReadModel GetLeitoCorrentePorAtendimento(Guid id)
        {
            try
            {
                //var teste = repository.MovimentacaoLeito.Include("Internacao").Include("Internacao.Atendimento").Where(x => x.Internacao.Atendimento.Id == id).ToList().OrderByDescending(x => x.DataMovimentacao).FirstOrDefault();
                var movimentacao = ViewModelUtils.ToModel<MovimentacaoLeito, MovimentacaoLeitoReadModel>(repository.MovimentacaoLeito.Include("Internacao").Include("LeitoAtual").Include("Internacao.Atendimento")
                                  .Include("LeitoAtual.Enfermaria").Where(x => x.Internacao.Atendimento.Id == id).ToList().OrderByDescending(x => x.DataMovimentacao).FirstOrDefault());

                return movimentacao;
                //return ViewModelUtils.ToModel<MovimentacaoLeito, MovimentacaoLeitoReadModel>(movimentacao);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public MovimentacaoLeitoReadModel GetMovimentacaoLeito(Guid id)
        {
            try
            {
                //var movimentacao = repository.MovimentacaoLeito.Include("Clinica").Include("LeitoAtual.Enfermaria").Include("Internacao.Atendimento.Acolhimento.Paciente").ToList().FirstOrDefault(x => x.Id == id);
                var movimentacao = repository.MovimentacaoLeito.Include("Clinica").Include("LeitoAtual.Enfermaria").Include("Internacao.Atendimento.Acolhimento.Paciente").Include("Internacao.Atendimento.Acolhimento.Servico").ToList().FirstOrDefault(x => x.Id == id);
                //return Mapper.Map<MovimentacaoLeito, MovimentacaoLeitoReadModel>(movimentacao);
                return new MovimentacaoLeitoReadModel()
                {
                    Id = movimentacao.Id,
                    Paciente = movimentacao.Internacao.Atendimento.Acolhimento.Paciente.Nome,
                    CodigoBoletim = movimentacao.Internacao.Atendimento.Acolhimento.CodigoBoletim,
                    Leito = movimentacao.LeitoAtual.Nome,
                    LocalEnfermaria = movimentacao.LeitoAtual.Enfermaria.Local,
                    ClinicaDescricao = movimentacao.Clinica != null ? movimentacao.Clinica.Descricao : (movimentacao.Internacao.Atendimento.Acolhimento.Servico != null ? movimentacao.Internacao.Atendimento.Acolhimento.Servico.Descricao : ""),
                    ClinicaDestinoId = movimentacao.Clinica != null ? movimentacao.Clinica.Id : (movimentacao.Internacao.Atendimento.Acolhimento.Servico != null ? movimentacao.Internacao.Atendimento.Acolhimento.Servico.Id : Guid.Empty),
                    DataInternacao = movimentacao.Internacao.DataInternacao.ToString("dd/MM/yyyy"),
                    DataUltimaMovimentacao = movimentacao.UltimaMovimentacao != null ? movimentacao.UltimaMovimentacao.DataMovimentacao.ToString("dd/MM/yyyy") : movimentacao.DataMovimentacao.ToString("dd/MM/yyyy"),
                    Enfermaria = movimentacao.LeitoAtual.Enfermaria.Nome,
                    Idade = movimentacao.Internacao.Atendimento.Acolhimento.Paciente.IdadePorExtenso,
                    InternacaoId = movimentacao.Internacao.Id
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public MovimentacaoLeitoReadModel GetUltimaMovimentacaoPorLeito(Guid id)
        {
            try
            {
                var movimentacao = Mapper.Map<MovimentacaoLeito, MovimentacaoLeitoReadModel>(repository.MovimentacaoLeito.Include("Internacao")
                                  .Include("LeitoAtual.Enfermaria").Include("Internacao.Atendimento.Acolhimento.Paciente").Include("Internacao.Atendimento.Acolhimento").Include("LeitoAtual")
                                  .Where(x => x.LeitoAtual.Id == id).OrderByDescending(x => x.DataMovimentacao).FirstOrDefault());

                return movimentacao;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ListResult ListarLeitosOcupados(Guid unidadeId)
        {
            try
            {
                /// Leitos ocupados por enfermaria
                List<LeitosOcupadosReadModel> lista = (from x in repository.MovimentacaoLeito.Include("Internacao.Atendimento.Acolhimento.Paciente").Include("Internacao.Atendimento.Acolhimento.Profissional.Unidade").Include("LeitoAtual").Include("LeitoAtual.Enfermaria")
                                                               .Where(x => x.Internacao.Atendimento.FimAtendimento == null && x.LeitoAtual.Ocupado == true && x.Internacao.Atendimento.Acolhimento.Profissional.Unidade.Id == unidadeId)
                                                               .ToList()
                                                               .OrderByDescending(x => x.DataMovimentacao)
                                                               .GroupBy(e => e.Internacao.Id)
                                                               .Select(e => e.FirstOrDefault())
                                                       select new LeitosOcupadosReadModel()
                                                       {
                                                           Id = x.Id,
                                                           Paciente = x.Internacao.Atendimento.Acolhimento.Paciente.Nome,
                                                           Enfermaria = x.LeitoAtual.Enfermaria.Nome,
                                                           Leito = x.LeitoAtual.Nome,
                                                           PacienteId = x.Internacao.Atendimento.Acolhimento.Paciente.Id.ToString()
                                                       }).OrderBy(x => x.Enfermaria).OrderBy(x => x.Leito).ToList();


                return ListResult.GetListResult(lista, new string[] { "Id", "Paciente", "Enfermaria", "Leito", "PacienteId" });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ListResult ListarObservacaoDiaria(DateTime inicio, DateTime fim, Guid unidadeId)
        {
            try
            {
                fim = fim.AddDays(1);

                var listaInternacoes = from r in repository.Internacao.Include("Atendimento.Acolhimento.Paciente").Include("Movimentacoes.LeitoAtual.Enfermaria").Include("Atendimento.PrimeiroDiagnostico").Include("Atendimento.Checkout").Include("Atendimento.Acolhimento.Profissional.Unidade")
                                                           .Include("Movimentacoes.Clinica")
                                                           /// .Where(r => r.DataInternacao >= inicio && r.DataInternacao <= fim)
                                                           .Where(x => x.DataInternacao <= fim && x.Atendimento.FimAtendimento == null)
                                                           .ToList()
                                       where r.Atendimento.Acolhimento.Profissional.Unidade.Id == unidadeId
                                       && r.Atendimento.Checkout == null
                                       orderby r.DataInternacao
                                       select r;

                var lista = from r in listaInternacoes
                            group r by r.DataInternacao.ToString("dd/MM/yyyy") into grupoDia
                            select new ObservacaoDiariaReadModel()
                            {
                                Data = grupoDia.Key,
                                Internacoes = (from clinicas in grupoDia
                                               group clinicas by clinicas.Movimentacoes.OrderByDescending(x => x.DataMovimentacao).FirstOrDefault(x => x.Clinica != null).Clinica.Descricao into grupoClinica
                                               select new ObservacaoDiariaReadModel()
                                               {
                                                   Clinica = grupoClinica.Key,
                                                   Internacoes = (from i in grupoClinica
                                                                  select new ObservacaoDiariaReadModel()
                                                                  {
                                                                      Diagnostico = i.Atendimento.PrimeiroDiagnostico != null ? i.Atendimento.PrimeiroDiagnostico.Descricao : "",
                                                                      EnfermariaLeito = string.Format("{0}/{1}", i.Movimentacoes.OrderByDescending(x => x.DataMovimentacao).FirstOrDefault().LeitoAtual.Enfermaria.Nome, i.Movimentacoes.OrderByDescending(x => x.DataMovimentacao).FirstOrDefault().LeitoAtual.Nome),
                                                                      Local = i.Movimentacoes.OrderByDescending(x => x.DataMovimentacao).FirstOrDefault().LeitoAtual.Enfermaria.Local,
                                                                      EstadoClinico = "Observação",
                                                                      HoraInternacao = string.Format("{0}:{1}", i.DataInternacao.Hour.ToString(), i.DataInternacao.Minute.ToString()),
                                                                      Idade = i.Atendimento.Acolhimento.Paciente.IdadeEmAnos != null ? i.Atendimento.Acolhimento.Paciente.IdadeEmAnos.ToString() : "",
                                                                      Paciente = i.Atendimento.Acolhimento.Paciente.Nome,
                                                                      Sexo = i.Atendimento.Acolhimento.Paciente.Sexo,
                                                                      TempoDePermanencia = CalculaTempo(i.DataInternacao)
                                                                  }).ToList(),
                                                   TotalDaClinica = grupoClinica.Count().ToString()
                                               }).ToList(),
                                TotalDoDia = grupoDia.Count().ToString(),
                                TotalGeral = listaInternacoes.Count().ToString()
                            };

                return ListResult.GetListResult(lista.ToList(), new string[] { "Data", "Total", "TotalDoDia", "TotalDaClinica", "Internacoes", "Diagnostico", "EnfermariaLeito", "Local", "EstadoClinico", "HoraInternacao", "Idade", "Paciente", "Sexo", "TempoDePermanencia", "TotalGeral" });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string CalculaTempo(DateTime dataInicial)
        {
            string tempo = "";
            TimeSpan calculo = DateTime.Now.Subtract(dataInicial);
            if (calculo.Days > 0)
                tempo = string.Format("{0}d:", calculo.Days.ToString());

            tempo = tempo + string.Format("{0}h:", calculo.Hours.ToString());
            tempo = tempo + string.Format("{0}m", calculo.Minutes.ToString());

            return tempo;
        }
    }
}

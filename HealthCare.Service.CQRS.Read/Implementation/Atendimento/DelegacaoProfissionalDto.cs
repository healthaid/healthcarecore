﻿using HealtCare.Infra.Common.Utils;
using HealthCare.Domain.Entities.DbClasses.Admin;
using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.Data.Context;
using HealthCare.Service.CQRS.Read.Interfaces.Cadastro;
using Microsoft.EntityFrameworkCore;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using HealthCare.Service.CQRS.Read.Models;

namespace HealthCare.Service.CQRS.Read.Implementation.Cadastro
{
    public class DelegacaoProfissionalDto : IDelegacaoProfissionalDao
    {
        private HealthCareDbContext repository;

        public DelegacaoProfissionalDto(HealthCareDbContext contextFacotory)
        {
            repository = contextFacotory;
        }

        public ListResult ListarDelegacoesAtivas(Guid unidadeId)
        {
            var lista = repository.DelegacaoProfissional.Include("Delegador.Profissional.Cbo").Include("Delegado.Profissional.Cbo").Include("Delegado.Profissional.Unidade").Where(x => x.DataDestituicao == null && x.Delegado.Profissional.Unidade.Id == unidadeId);

            var listaDelegacao = from l in lista
                                 select new ListaDelegacaoProfissionalViewModel
                                 {
                                     Id = l.Id,
                                     CboDelegado = l.Delegado.Profissional.Cbo.Descricao,
                                     Delegado = l.Delegado.Profissional.Nome,
                                     CboDelegador = l.Delegador.Profissional.Cbo.Descricao,
                                     Delegador = l.Delegador.Profissional.Nome,
                                     DataDelegacao = l.DataDelegacao,
                                     DataDestituicao = l.DataDestituicao
                                 };

            return ListResult.GetListResult(listaDelegacao.ToList(), new string[] { "Id", "CboDelegado", "Delegado", "CboDelegador", "Delegador", "DataDelegacaoFormatada", "DataDestituicaoFormatada" });
        }

        public IList<DelegacaoProfissionalViewModel> ListarDelegacoesPorDelegado(Guid idProfissionalDelegado)
        {
            var lista = repository.DelegacaoProfissional.Include("Delegador").Include("Delegado").Where(x => x.Delegado.Id == idProfissionalDelegado && x.DataDestituicao == null).ToList();
            return ViewModelUtils.ToListViewModel<DelegacaoProfissional, DelegacaoProfissionalViewModel>(lista).ToList();
        }

    }
}
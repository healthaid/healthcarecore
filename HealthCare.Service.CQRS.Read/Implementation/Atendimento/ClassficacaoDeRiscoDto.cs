﻿using AutoMapper;
using HealtCare.Infra.Common.Utils;
using HealthCare.Domain.Entities.DbClasses.Atendimento;
using HealthCare.Infra.Common.Enums;
using HealthCare.Infra.CQRS.Read.Interfaces.Atendimento;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace HealthCare.Infra.CQRS.Read.Implementation.Atendimento
{
    public class ClassficacaoDeRiscoDto : IClassificacaoDeRiscoDto
    {
        private HealthCareDbContext repository;
        private IMapper mapper;

        public ClassficacaoDeRiscoDto(HealthCareDbContext contextFactory, IMapper mapper)
        {
            this.repository = contextFactory;
            this.mapper = mapper;
            ViewModelUtils.mapper = mapper;
        }

        public ClassificacaoDeRiscoReadModel ObterClassificacaoDeRisco(Guid acolhimentoId)
        {
            try
            {
                    return ViewModelUtils.ToModel<ClassificacaoDeRisco, ClassificacaoDeRiscoReadModel>
                                                  (repository.ClassificacaoDeRisco.Include("Acolhimento")
                                                                                  .Include("Acolhimento.Paciente")
                                                                                  .Include("Acolhimento.Servico")
                                                                                  .Include("SinaisVitais")
                                                                                  .Include("Risco")
                                                                                  .Include("Profissional")
                                                                                  .FirstOrDefault(x => x.Acolhimento.Id == acolhimentoId));
                
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public string[] ClassificacoesDeRisco()
        {
            return Enum.GetNames(typeof(eClassificacaoDeRisco));
        }
    }
}

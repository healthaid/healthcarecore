﻿
using AutoMapper;
using HealtCare.Infra.Common.Utils;
using HealthCare.Domain.Entities.DbClasses.Atendimento;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.Data.Context;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HealthCare.Infra.CQRS.Read.Implementation
{
    public class MedicamentoDto : IMedicamentoDto
    {
        private HealthCareDbContext repository;
        private IMapper mapper;
        public MedicamentoDto(HealthCareDbContext contextFactory, IMapper mapper)
        {
            this.repository = contextFactory;
            this.mapper = mapper;
            ViewModelUtils.mapper = mapper;
        }


        public IList<MedicamentoReadModel> ListarTodosMedicamentos(string texto)
        {
            try
            {
                return ViewModelUtils.ToListViewModel<Medicamento, MedicamentoReadModel>(repository.Query<Medicamento>()
                                                                                          .Where(x => x.Nome.Contains(texto)).Take(50)).ToList();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

    }
}

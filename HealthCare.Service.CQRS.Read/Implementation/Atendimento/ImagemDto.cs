﻿using AutoMapper;
using HealtCare.Infra.Common.Utils;
using HealthCare.Domain.Entities.DbClasses.Atendimento;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.Data.Context;
using System;
using System.Collections.Generic;
using System.Linq;


namespace HealthCare.Infra.CQRS.Read.Implementation
{
    public class ImagemDto : IImagemDao
    {
        private HealthCareDbContext repository;
        private IMapper mapper;
        public ImagemDto(HealthCareDbContext contextFactory, IMapper mapper)
        {
            this.repository = contextFactory;
            this.mapper = mapper;
            ViewModelUtils.mapper = mapper;
        }

        public ImagemReadViewModel ObterImagemPorId(System.Guid id)
        {
            try
            {
                return ViewModelUtils.ToViewModel<Imagem, ImagemReadViewModel>(repository.Query<Imagem>().FirstOrDefault(x => x.Id == id));
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public IList<ImagemReadViewModel> ObterImagensPorImagemAtendimento(System.Guid idAtentimento)
        {
            try
            {
                return ViewModelUtils.ToListViewModel<Imagem, ImagemReadViewModel>(repository.Query<Imagem>("ImagemAtendimento").Where(x => x.ImagemAtendimento.Id == idAtentimento)).ToList();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
﻿using AutoMapper;
using HealtCare.Infra.Common.Utils;
using HealthCare.Domain.Entities.DbClasses.Atendimento;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.Data.Context;
using System;
using System.Linq;


namespace HealthCare.Infra.CQRS.Read.Implementation
{
    public class HipoteseDiagnosticaDto : IHipoteseDiagnosticaDao
    {
        private HealthCareDbContext repository;
        private IMapper mapper;
        public HipoteseDiagnosticaDto(HealthCareDbContext contextFactory, IMapper mapper)
        {
            this.repository = contextFactory;
            this.mapper = mapper;
            ViewModelUtils.mapper = mapper;
        }

        public HipoteseDiagnosticaReadViewModel ObterHipoteseDiagnosticaPorAtendimento(Guid atendimentoId)
        {
            try
            {
                var r = ViewModelUtils.ToViewModel<HipoteseDiagnostica, HipoteseDiagnosticaReadViewModel>(repository.Query<HipoteseDiagnostica>("Atendimento", "Cids").FirstOrDefault(x => x.Atendimento.Id == atendimentoId));
                return r;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public HipoteseDiagnosticaReadViewModel ObterHipoteseDiagnostica(Guid Id)
        {
            try
            {
                return ViewModelUtils.ToViewModel<HipoteseDiagnostica, HipoteseDiagnosticaReadViewModel>(repository.Query<HipoteseDiagnostica>("Atendimento").FirstOrDefault(x => x.Id == Id));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
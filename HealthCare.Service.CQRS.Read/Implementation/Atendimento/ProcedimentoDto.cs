﻿using HealtCare.Infra.Common.Utils;
using HealthCare.Domain.Entities.DbClasses.Atendimento;
using HealthCare.Infra.CQRS.Read.Interfaces.Atendimento;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HealthCare.Infra.CQRS.Read.Implementation
{
    public class ProcedimentoDto : IProcedimentoDto
    {
        private HealthCareDbContext repository;
        public ProcedimentoDto(HealthCareDbContext contextFactory)
        {
            this.repository = contextFactory;
        }

        public List<ProcedimentoReadModel> ListarTodosProcedimentos(string texto)
        {
            try
            {
                List<ProcedimentoReadModel> lista = new List<ProcedimentoReadModel>();
                if (!string.IsNullOrWhiteSpace(texto))
                {
                    texto = texto.ToUpper();

                    lista =  ViewModelUtils.ToListViewModel<Procedimento, ProcedimentoReadModel>(repository.Query<Procedimento>()
                                                                                              .Where(x => (x.CO_PROCEDIMENTO.StartsWith("02") || x.CO_PROCEDIMENTO.StartsWith("03") || x.CO_PROCEDIMENTO.StartsWith("04") || x.CO_PROCEDIMENTO.StartsWith("05"))
                                                                                                       && (x.NO_PROCEDIMENTO.ToUpper().Contains(texto) || x.CO_PROCEDIMENTO.ToUpper().Contains(texto))).Take(50)).ToList();
                }

                return lista;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ProcedimentoReadModel> ListarProcedimentosPorCID(string codigo)
        {
            try
            {
                List<ProcedimentoReadModel> lprm = new List<ProcedimentoReadModel>();
                CID cid = repository.CID.Include("ProcedimentosCids").Include("ProcedimentosCids.Procedimento").Where(x => x.Codigo.Equals(codigo)).FirstOrDefault();
                if (cid.ProcedimentosCids != null && cid.ProcedimentosCids.Count > 0)
                {
                    foreach (ProcedimentoCid ITEM2 in cid.ProcedimentosCids.Where(x => (x.Procedimento.CO_PROCEDIMENTO.StartsWith("02") || x.Procedimento.CO_PROCEDIMENTO.StartsWith("03")
                                                                                     || x.Procedimento.CO_PROCEDIMENTO.StartsWith("04") || x.Procedimento.CO_PROCEDIMENTO.StartsWith("05"))))
                    {
                        ProcedimentoReadModel prm = new ProcedimentoReadModel();
                        prm.NO_PROCEDIMENTO = ITEM2.Procedimento.NO_PROCEDIMENTO.Trim();
                        prm.CO_PROCEDIMENTO = ITEM2.Procedimento.CO_PROCEDIMENTO.Trim();
                        lprm.Add(prm);
                    }
                }
                else
                    lprm = ViewModelUtils.ToListViewModel<Procedimento, ProcedimentoReadModel>(repository.Query<Procedimento>()
                                                                                     .Where(x => (x.CO_PROCEDIMENTO.StartsWith("02") || x.CO_PROCEDIMENTO.StartsWith("03")
                                                                                               || x.CO_PROCEDIMENTO.StartsWith("04") || x.CO_PROCEDIMENTO.StartsWith("05")))).ToList();

                return lprm;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ProcedimentoReadModel> ListarProcedimentosPorCID(string codigo, string texto)
        {
            try
            {
                List<ProcedimentoReadModel> lprm = new List<ProcedimentoReadModel>();

                CID cid = repository.CID.Include("ProcedimentosCids").Include("ProcedimentosCids.Procedimento").Where(x => x.Codigo.Equals(codigo)).FirstOrDefault();

                if (cid.ProcedimentosCids != null && cid.ProcedimentosCids.Count > 0)
                {
                    if (!string.IsNullOrWhiteSpace(texto))
                    {
                        texto = texto.ToUpper();
                        cid.ProcedimentosCids = cid.ProcedimentosCids.Where(x => (x.Procedimento.CO_PROCEDIMENTO.StartsWith("02") || x.Procedimento.CO_PROCEDIMENTO.StartsWith("03") || x.Procedimento.CO_PROCEDIMENTO.StartsWith("04") || x.Procedimento.CO_PROCEDIMENTO.StartsWith("05"))
                                                                              && (x.Procedimento.CO_PROCEDIMENTO.ToUpper().Contains(texto) || x.Procedimento.NO_PROCEDIMENTO.ToUpper().ToString().Contains(texto))).ToList();
                    }
                    else
                    {
                        cid.ProcedimentosCids = cid.ProcedimentosCids.Where(x => (x.Procedimento.CO_PROCEDIMENTO.StartsWith("02") || x.Procedimento.CO_PROCEDIMENTO.StartsWith("03")
                                                                               || x.Procedimento.CO_PROCEDIMENTO.StartsWith("04") || x.Procedimento.CO_PROCEDIMENTO.StartsWith("05"))).ToList();
                    }

                    foreach (var ITEM2 in cid.ProcedimentosCids)
                    {
                        ProcedimentoReadModel prm = new ProcedimentoReadModel();
                        prm.NO_PROCEDIMENTO = ITEM2.Procedimento.NO_PROCEDIMENTO.Trim();
                        prm.CO_PROCEDIMENTO = ITEM2.Procedimento.CO_PROCEDIMENTO.Trim();
                        lprm.Add(prm);
                    }
                }
                else
                    lprm = ViewModelUtils.ToListViewModel<Procedimento, ProcedimentoReadModel>(repository.Query<Procedimento>()
                                                                                                         .Where(x => (x.CO_PROCEDIMENTO.StartsWith("02") || x.CO_PROCEDIMENTO.StartsWith("03")
                                                                                                                   || x.CO_PROCEDIMENTO.StartsWith("04") || x.CO_PROCEDIMENTO.StartsWith("05")))).ToList();

                return lprm;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
﻿using AutoMapper;
using HealtCare.Infra.Common.Utils;
using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HealthCare.Infra.CQRS.Read.Implementation
{
    public class ServicoDto : IServicoDao
    {
        private HealthCareDbContext repository;
        private IMapper mapper;
        public ServicoDto(HealthCareDbContext contextFactory, IMapper mapper)
        {
            this.repository = contextFactory;
            this.mapper = mapper;
            ViewModelUtils.mapper = mapper;
        }

        public IList<Servico> ListarServicos()
        {
            try
            {
                return repository.Query<Servico>().Where(x => x.IsDeleted == false).ToList();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        
        public Common.Result.ListResult ListarServicos(string filtro)
        {
            try
            {
                var lista = ViewModelUtils.ToListViewModel<Servico, ServicoReadModel>(repository.Query<Servico>().Where(x => x.Descricao.Contains(filtro))).ToList();
                ListResult result = ListResult.GetListResult(lista, new string[] { "Id", "Descricao", "IsDeleted" });
                return result;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        
        public IList<ServicoReadModel> ListarTodosServicos()
        {
            try
            {
                return ViewModelUtils.ToListViewModel<Servico, ServicoReadModel>(repository.Query<Servico>().Where(x => x.IsDeleted == false)).ToList();
                //return ListResult.GetListResult(lista, new string[] { "Id", "Descricao" });
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }


        public IList<Servico> ListarServicosPorProfissional(Guid profissionalId)
        {
            try
            {
                //using (var repository = contextFactory.Invoke())
                //{
                //    var profissional = repository.Profissional.Include("Servicos").FirstOrDefault(x => x.Id == profissionalId);
                //    return profissional.Servicos;
                //}
                return new List<Servico>();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ServicoReadModel ObterServicoPorId(Guid id)
        {
            try
            {
                return ViewModelUtils.ToViewModel<Servico, ServicoReadModel>(repository.Find<Servico>(id));
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
﻿using AutoMapper;
using HealtCare.Infra.Common.Utils;
using HealthCare.Domain.Entities.DbClasses.Atendimento;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.Data.Context;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HealthCare.Infra.CQRS.Read.Implementation
{
    public class ImagemAtendimentoDto : IImagemAtendimentoDao
    {
        private HealthCareDbContext repository;
        private IMapper mapper;
        public ImagemAtendimentoDto(HealthCareDbContext contextFactory, IMapper mapper)
        {
            this.repository = contextFactory;
            this.mapper = mapper;
            ViewModelUtils.mapper = mapper;
        }

        public ImagemAtendimentoReadViewModel ObterImagemAtendimentoPorId(System.Guid id)
        {
            try
            {
                return ViewModelUtils.ToViewModel<ImagemAtendimento, ImagemAtendimentoReadViewModel>(repository.Query<ImagemAtendimento>().FirstOrDefault(x => x.Id == id));
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public IList<ImagemAtendimentoReadViewModel> ListarImagemAtendimento(System.Guid idAtentimento)
        {
            try
            {
                return ViewModelUtils.ToListViewModel<ImagemAtendimento, ImagemAtendimentoReadViewModel>(repository.Query<ImagemAtendimento>("Atendimento").Where(x => x.Atendimento.Id == idAtentimento)).ToList();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
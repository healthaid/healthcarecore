﻿
using AutoMapper;
using HealtCare.Infra.Common.Utils;
using HealthCare.Domain.Entities.DbClasses.Atendimento;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HealthCare.Infra.CQRS.Read.Implementation
{
    public class CidDto : ICidDao
    {
        private HealthCareDbContext repository;
        private IMapper mapper;

        public CidDto(HealthCareDbContext contextFactory, IMapper mapper)
        {
            this.repository = contextFactory;
            this.mapper = mapper;
            ViewModelUtils.mapper = mapper;
        }

        public IList<CIDReadViewModel> ListarTodosCids(string texto)
        {
            try
            {
                return ViewModelUtils.ToListViewModel<CID, CIDReadViewModel>(repository.Query<CID>()
                                                                                          .Where(x => x.Descricao.Contains(texto)
                                                                                                   || x.Codigo.Contains(texto)).Take(50)).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<CIDReadViewModel> ListarCids()
        {
            try
            {
                return ViewModelUtils.ToListViewModel<CID, CIDReadViewModel>(repository.Query<CID>()).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public CIDReadViewModel ObterCid(Guid Id)
        {
            try
            {
                return ViewModelUtils.ToListViewModel<CID, CIDReadViewModel>(repository.Query<CID>()).First(x => x.Id == Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<CIDReadViewModel> GetCidsParaCheckout(Guid atendimentoId)
        {
            try
            {
                List<CID> cids = new List<CID>();

                HealthCare.Domain.Entities.DbClasses.Atendimento.Atendimento atendimento = repository.Atendimento.Include("Internacoes").Include("PrimeiroDiagnostico").Include("SegundoDiagnostico").FirstOrDefault(x => x.Id == atendimentoId);

                if (atendimento.PrimeiroDiagnostico != null)
                    cids.Add(atendimento.PrimeiroDiagnostico);
                List<Evolucao> evolucoes = repository.Evolucao.Include("PrimeiroDiagnostico").Include("Atemdimento").Where(x => x.Atemdimento.Id == atendimentoId).ToList();
                if (evolucoes.Count > 0)
                    cids.Add(evolucoes.OrderByDescending(x => x.DataHoraEvolucao).FirstOrDefault().PrimeiroDiagnostico);
                else if (atendimento.SegundoDiagnostico != null)
                    cids.Add(atendimento.SegundoDiagnostico);

                return ViewModelUtils.ToListViewModel<CID, CIDReadViewModel>(cids);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }   
}

﻿using AutoMapper;
using HealtCare.Infra.Common.Utils;
using HealthCare.Domain.Entities.DbClasses.Atendimento;
using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Globalization;
using System.Linq;


namespace HealthCare.Infra.CQRS.Read.Implementation.Atendimento
{
    public class ReceitaMedicaDto : IReceitaMedicaDto
    {
        private HealthCareDbContext repository;
        private IMapper mapper;
        public ReceitaMedicaDto(HealthCareDbContext contextFactory, IMapper mapper)
        {
            this.repository = contextFactory;
            this.mapper = mapper;
            ViewModelUtils.mapper = mapper;
        }

        public ListResult ImpressaoReceita(Guid atendimentoId)
        {
            try
            {
                var lista = (from r in repository.ReceitaMedica.Include("Atendimento.Acolhimento.Paciente").Where(x => x.Atendimento.Id == atendimentoId).ToList().GroupBy(x => x.Atendimento)
                             select new ReceitaMedicaReadModel()
                             {
                                 Nome = r.Key.Acolhimento.Paciente.Nome,
                                 CPF = r.Key.Acolhimento.Paciente.CPF,
                                 Nascimento = r.Key.Acolhimento.Paciente.DataNascimento == null ? "" : r.Key.Acolhimento.Paciente.DataNascimento.Value.ToString("dd/MM/yyyy ", new CultureInfo("en-US")),
                                 ItensReceita = (from item in r
                                                 select new ReceitaMedicaReadModel
                                                 {
                                                     Dose = item.Dose,
                                                     Duracao = item.Duracao,
                                                     Intervalo = item.Intervalo,
                                                     Medicamento = item.Medicamento,
                                                     Unidade = item.Unidade,
                                                     ViaAdministracao = item.ViaAdministracao
                                                 }).ToList()
                             }).ToList();

                return ListResult.GetListResult(lista, new string[] { "Nome", "CPF", "Nascimento", "ItensReceita" });
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public ListResult ObterReceitasMedicaPorAtendimentoId(Guid atendimentoId)
        {
            try
            {
                var lista = ViewModelUtils.ToListViewModel<ReceitaMedica, ReceitaMedicaReadModel>(repository.ReceitaMedica.Include("Atendimento").Where(x => x.Atendimento.Id == atendimentoId)).ToList();
                ListResult result = ListResult.GetListResult(lista, new string[] { "Id", "Medicamento", "Dose", "Unidade", "ViaAdministracao", "Intervalo", "Duracao" });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        Models.ReceitaMedicaReadModel IReceitaMedicaDto.ObterReceitaMedicaPorId(Guid id)
        {
            try
            {
                return ViewModelUtils.ToModel<ReceitaMedica, ReceitaMedicaReadModel>(repository.ReceitaMedica.FirstOrDefault(x => x.Id == id));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

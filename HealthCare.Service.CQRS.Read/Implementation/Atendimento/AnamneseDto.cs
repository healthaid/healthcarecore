﻿using AutoMapper;
using HealtCare.Infra.Common.Utils;
using HealthCare.Domain.Entities.DbClasses.Atendimento;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.Data.Context;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HealthCare.Infra.CQRS.Read.Implementation
{
    public class AnamneseDto : IAnamneseDao
    {
        private HealthCareDbContext contextFactory;
        private IMapper mapper;
        public AnamneseDto(HealthCareDbContext contextFactory, IMapper mapper)
        {
            this.contextFactory = contextFactory;
            ViewModelUtils.mapper = mapper;
        }

        public IList<AnamneseReadViewModel> ListarAnamneses()
        {
            try
            {
                return ViewModelUtils.ToListViewModel<Anamnese, AnamneseReadViewModel>(contextFactory.Query<Anamnese>()).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public AnamneseReadViewModel ObterAnamnese(Guid id)
        {
            try
            {
                return ViewModelUtils.ToModel<Anamnese, AnamneseReadViewModel>(contextFactory.Query<Anamnese>().FirstOrDefault(x => x.Id == id));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public AnamneseReadViewModel ObterAnamnesePorAtendimento(Guid idAtendimento)
        {
            try
            {
                return ViewModelUtils.ToModel<Anamnese, AnamneseReadViewModel>(contextFactory.Query<Anamnese>().FirstOrDefault(x => x.Atendimento.Id == idAtendimento));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
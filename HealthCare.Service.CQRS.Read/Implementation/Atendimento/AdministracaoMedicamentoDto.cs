﻿
using AutoMapper;
using HealtCare.Infra.Common.Utils;
using HealthCare.Domain.Entities.DbClasses.Atendimento;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.Data.Context;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HealthCare.Infra.CQRS.Read.Implementation
{
    public class AdministracaoMedicamentoDto : IAdministracaoMedicamentoDto
    {
        private HealthCareDbContext repository;
        private IMapper mapper;
        public AdministracaoMedicamentoDto(HealthCareDbContext contextFactory, IMapper mapper)
        {
            this.repository = contextFactory;
            this.mapper = mapper;
            ViewModelUtils.mapper = mapper;
        }


        public IList<AdministracaoMedicamentoReadModel> ListarTodosAdministracaoMedicamentos(string texto)
        {
            try
            {

                return ViewModelUtils.ToListViewModel<AdministracaoMedicamento, AdministracaoMedicamentoReadModel>(repository.Query<AdministracaoMedicamento>()
                                                                                          .Where(x => x.Nome.Contains(texto)).Take(50)).ToList();
            }
            catch (Exception)
            {

                throw;
            }

        }


        public IList<AdministracaoMedicamentoReadModel> ListarTodosAdministracaoMedicamentos()
        {
            try
            {
                return ViewModelUtils.ToListViewModel<AdministracaoMedicamento, AdministracaoMedicamentoReadModel>(repository.AdministracaoMedicamento.ToList()).ToList();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }



    }
}

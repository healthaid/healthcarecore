﻿using AutoMapper;
using HealtCare.Infra.Common.Utils;
using HealthCare.Domain.Entities.DbClasses.Atendimento;
using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Infra.Common.Enums;
using HealthCare.Infra.Common.Extensions;
using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Read.Interfaces.Atendimento;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.Data.Context;
using HealthCare.Service.CQRS.Read.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Dapper;
using Microsoft.Extensions.Logging;

namespace HealthCare.Infra.CQRS.Read.Implementation.Atendimento
{
    public class AtendimentoDto : IAtendimentoDto
    {
        private HealthCareDbContext repository;
        private IMapper mapper;
        private ILogger<AtendimentoDto> logger;

        public AtendimentoDto(HealthCareDbContext contextFactory, IMapper mapper, ILogger<AtendimentoDto> logger)
        {
            this.repository = contextFactory;
            this.mapper = mapper;
            ViewModelUtils.mapper = mapper;
            this.logger = logger;
        }

        public AtendimentoReadModel ObterAtendimentoPorAcolhimento(Guid idAcolhimento)
        {
            try
            {
                var data = ViewModelUtils.ToViewModel<HealthCare.Domain.Entities.DbClasses.Atendimento.Atendimento, AtendimentoReadModel>(repository.Atendimento.Include("Acolhimento")
                                                                                                                            .Include("Acolhimento.Classificacao")
                                                                                                                            .Include("Acolhimento.Classificacao.Risco")
                                                                                                                            .Include("Acolhimento.Classificacao.SinaisVitais")
                                                                                                                            .Include("Acolhimento.Paciente")
                                                                                                                            .Include("Checkout")
                                                                                                                            .Include("HipoteseDiagnostica")
                                                                                                                            .Include("PrimeiroDiagnostico")
                                                                                                                            .Include("SegundoDiagnostico")
                                                                                                                            .Where(x => x.Acolhimento.Id == idAcolhimento).FirstOrDefault());
                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public BoletimReadModel ObterBoletimPorAcolhimento(Guid idAcolhimento)
        {
            try
            {
                var atendimento = repository.Atendimento.Where(x => x.Acolhimento.Id == idAcolhimento).Select(x => x).Include(x => x.Acolhimento).ThenInclude(x => x.Paciente).ThenInclude(x => x.Endereco).Include(x => x.Acolhimento.Paciente).ThenInclude(x => x.Naturalidade)
                                                .Include(x => x.Acolhimento.Profissional).ThenInclude(x => x.Unidade).ThenInclude(x => x.Endereco)
                                                .Include(x => x.Acolhimento.Classificacao).ThenInclude(x => x.Risco)
                                                .Include(x => x.Acolhimento.Classificacao).ThenInclude(x => x.SinaisVitais)
                                                .Include(x => x.Acolhimento.Servico)
                                                .Include(x => x.Anamnese).Include(x => x.HipoteseDiagnostica)
                                                .Include(x => x.PrimeiroDiagnostico).FirstOrDefault();
                BoletimReadModel b = new BoletimReadModel
                {
                    IdAtendimento = atendimento.Id,
                    dataAtendimento = atendimento.InicioAtendimento.ToString("dd/MM/yyyy HH:mm:ss", new CultureInfo("en-US")),
                    anamnese = atendimento.Anamnese.Count > 0 ? atendimento.Anamnese.FirstOrDefault().Descricao : "",
                    primeiroDiagnostico = atendimento.PrimeiroDiagnostico != null ? atendimento.PrimeiroDiagnostico.Codigo + " - " + atendimento.PrimeiroDiagnostico.Descricao : string.Empty,
                    exameFisico = atendimento.ExameFisico,
                    hipoteseDiagnostica = atendimento.HipoteseDiagnostica.Count > 0 ? atendimento.HipoteseDiagnostica.FirstOrDefault().Observacao : "",
                    queixaPrincipal = atendimento.Anamnese.Count > 0 ? (atendimento.Anamnese.FirstOrDefault().Ciap2 != null ? atendimento.Anamnese.FirstOrDefault().Ciap2.Descricao : atendimento.Anamnese.FirstOrDefault().Descricao) : "",
                    AcolhimentoReadModel = mapper.Map<HealthCare.Domain.Entities.DbClasses.Atendimento.Acolhimento, AcolhimentoReadModel>(atendimento.Acolhimento),
                    ClassificacaoDeRiscoReadModel = mapper.Map<HealthCare.Domain.Entities.DbClasses.Atendimento.ClassificacaoDeRisco, ClassificacaoDeRiscoReadModel>(atendimento.Acolhimento.Classificacao.FirstOrDefault()),
                };
                return b;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ListResult ListarAtendimentosDoDia(Guid idUnidade, Guid idProfissional, DateTime? dataInicio, DateTime? datafim)
        {
            try
            {

                if (datafim == null)
                    datafim = DateTime.Now;

                var lista = (from at in repository.Atendimento.Include("Acolhimento")
                                                                        .Include("Acolhimento.Paciente")
                                                                        .Include("Acolhimento.Paciente.Unidade")
                                                                        .Include("Checkout")
                                                                        .Include("Checkout.MotivoCheckout")
                                                                        .Include("Internacoes")
                                                                        .Where(x => x.Acolhimento.Paciente.Unidade.Id == idUnidade && x.Checkout == null
                                                                                 && x.Internacoes.Any() == false).WhereIf(dataInicio.HasValue, x => x.InicioAtendimento >= dataInicio).WhereIf(datafim.HasValue, x => x.InicioAtendimento <= datafim).ToList()
                             select new FilaAtendimentoViewModel
                             {
                                 Id = at.Id,
                                 InicioAtendimento = at.InicioAtendimento.ToString("dd/MM/yyyy HH:mm:ss", new CultureInfo("en-US")),
                                 Nome = at.Acolhimento.Paciente.Nome,
                                 Nascimento = at.Acolhimento.Paciente.DataNascimento,
                                 Idade = CalulaIdade(at.Acolhimento.Paciente.DataNascimento),
                                 Sexo = at.Acolhimento.Paciente.Sexo,
                                 Checkout = at.Checkout != null && at.Checkout.MotivoCheckout != null ? at.Checkout.MotivoCheckout.Descricao : string.Empty,
                                 FimAtendimento = at.FimAtendimento != null ? at.FimAtendimento.Value.ToString() : string.Empty,
                                 AcolhimentoId = at.Acolhimento.Id,
                                 Observacao = at.Checkout != null ? at.Checkout.Observacao : string.Empty,
                                 Boletim = at.Acolhimento.CodigoBoletim
                             }).OrderBy(x => x.InicioAtendimento).ToList();
                var expectionListServicoSocial = repository.ServicoSocial.Include("Acolhimento").Where(x => x.Acolhimento.Paciente.Unidade.Id == idUnidade && x.Checkout == null
                                                                         && x.InicioAtendimento >= DateTime.Today && x.Internacoes.Any() == false).Select(x => x.Id);

                lista = lista.Where(x => !expectionListServicoSocial.Contains(x.Id)).ToList();

                ListResult result = ListResult.GetListResult(lista, new string[] { "Id", "InicioAtendimento", "Nome", "Sexo", "Idade", "Checkout", "FimAtendimento", "AcolhimentoId", "Observacao", "Boletim" });
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public ListResult GetFilaParaAtendimento(Guid unidadeId)
        {
            try
            {
                logger.LogInformation("Iniciando consulta para a fila de atendimento");
                DateTime dtAnterior = DateTime.Today.AddDays(-1).AddHours(22);

                string sql = @"SELECT *
                                 FROM Acolhimento Ac
                           INNER JOIN Paciente P
                                   ON Ac.PacienteId = P.Id
                            INNER JOIN ClassificacaoDeRisco CR
                                   ON Ac.Id = CR.AcolhimentoId
                            INNER JOIN Risco R
                                   ON R.Id = CR.RiscoId
                           INNER JOIN Unidade U
                                   ON P.UnidadeId = U.Id
                            LEFT JOIN Servico S
                                   ON Ac.ServicoId = S.Id
                                WHERE Ac.Id NOT IN (SELECT AcolhimentoId from Atendimento)
                                  AND P.PendenteCadastro = false
                                  AND U.Id = @unidadeId
                                  AND Ac.DataHora >= @dataHora;";

                IEnumerable<Acolhimento> lista = repository.DapperHelper.Query<Acolhimento, Paciente, ClassificacaoDeRisco, Risco, Unidade,  Servico, Acolhimento>(
     sql,
     (acolhimento, paciente, classificacao, risco, unidade,  servico) =>
     {
         acolhimento.Servico = servico;
         if (classificacao != null)
         {
             acolhimento.Classificacao = new List<ClassificacaoDeRisco>();
             acolhimento.Classificacao.Add(classificacao);
             acolhimento.Classificacao.FirstOrDefault().Risco = risco;
         }
         //acolhimento.Profissional = profissional;
         //acolhimento.Profissional.Unidade = unidade;
         acolhimento.Paciente = paciente;
         return acolhimento;
     }, new { Pendente = 1, unidadeId = unidadeId.ToString(), dataHora = dtAnterior });

                var fila = (from a in lista
                            select new FilaAcolhimentoReadModel
                            {
                                Id = a.Id,
                                NomePaciente = a.Paciente.Nome,
                                Prioridade = a.Prioridade ? "Sim" : "Não",
                                PrioridadeBool = a.Prioridade,
                                HoraAcolhimento = a.DataHora.ToString("dd/MM/yyyy HH:mm:ss", new CultureInfo("en-US")),
                                Servico = a.Servico != null ? a.Servico.Descricao : string.Empty,
                                PacienteId = a.Paciente.Id,
                                Risco = ReturnHtmlRisco(a.Classificacao.FirstOrDefault().Risco),
                                Boletim = a.CodigoBoletim,
                                PrioridadeRisco = a.Classificacao != null ? a.Classificacao.FirstOrDefault().Risco.Prioridade : -1,
                            }).OrderBy(x => x.PrioridadeRisco).ThenByDescending(x => x.PrioridadeBool);

                logger.LogInformation("Terminada consulta para fila de atendimento");
                var teste = fila.ToList();
                return ListResult.GetListResult(fila.ToList(), new string[] { "Id", "HoraAcolhimento", "NomePaciente", "Sexo", "Idade", "PacienteId", "Risco", "Servico", "Prioridade", "Boletim", "TempoAtendimento" });

            }
            catch (Exception ex)
            {
                logger.LogInformation("Falha na consulta de atendimento" + ex.Message + "/n" + ex.InnerException);
                throw ex;
            }
        }

        private string ReturnHtmlRisco(Risco risco)
        {
            if (risco != null)
            {
                switch (risco.Descricao.ToUpper())
                {
                    case "AMARELO":
                        return "<div class='risco-amarelo'></div>";
                    case "AZUL":
                        return "<div class='risco-azul'></div>";
                    case "VERDE":
                        return "<div class='risco-verde'></div>";
                    case "VERMELHO":
                        return "<div class='risco-vermelho'></div>";
                    default:
                        return string.Empty;
                }
            }
            else
                return string.Empty;
        }

        private string CalulaIdade(DateTime? dataNascimento)
        {
            string idade = string.Empty;
            if (dataNascimento.HasValue)
            {
                // Calculate the age.
                var idadeCalculada = DateTime.Today.Year - dataNascimento.Value.Year;

                if (dataNascimento > DateTime.Today.AddYears(-idadeCalculada))
                    idadeCalculada--;

                if (idadeCalculada < 1)
                    idade = "Menos de 1 ano";
                else if (idadeCalculada == 1)
                    idade = string.Format("{0} ano.", idadeCalculada.ToString());
                else
                    idade = string.Format("{0} ano(s).", idadeCalculada.ToString());
            }

            return idade;
        }

        public ListResult ListarAtendimentos(Guid Unidade, DateTime inicio, Guid idProfissional, DateTime? Fim = null)
        {
            throw new NotImplementedException();
        }

        public AtendimentoReadModel ObterAtendimentoPorId(Guid atendimentoId)
        {
            try
            {

                var ret = ViewModelUtils.ToViewModel<HealthCare.Domain.Entities.DbClasses.Atendimento.Atendimento, AtendimentoReadModel>(
                                                repository.Atendimento.Where(x => x.Id == atendimentoId).Select(x => x).Include(x => x.Acolhimento).ThenInclude(x => x.Paciente).ThenInclude(x => x.Endereco).Include(x => x.Acolhimento.Paciente).ThenInclude(x => x.Naturalidade)
                                                .Include(x => x.Checkout).ThenInclude(x => x.MotivoCheckout).ThenInclude(x => x.Tipo)
                                                .Include(x => x.Profissional).ThenInclude(x => x.Cbo)
                                                .Include(x => x.Acolhimento.Classificacao).ThenInclude(x => x.Risco)
                                                .Include(x => x.Acolhimento.Classificacao).ThenInclude(x => x.SinaisVitais)
                                                .Include(x => x.Acolhimento.Servico)
                                                .Include(x => x.PrimeiroDiagnostico)
                                                .Include(x => x.Atestado)
                                                .Include(x => x.Anamnese).Include(x => x.HipoteseDiagnostica).FirstOrDefault());

                return ret;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ListResult GetProcedimentosByAtendimento(Guid atendimentoId)
        {
            try
            {


                List<ProcedimentoReadModel> procedimentos = (from ap in repository.AtendimentoProcedimento.Where(x => x.AtendimentoId == atendimentoId)
                                                             select new ProcedimentoReadModel()
                                                             {
                                                                 Id = ap.Id,
                                                                 CO_PROCEDIMENTO = ap.CodigoProcedimento,
                                                                 NO_PROCEDIMENTO = ap.NomeProcedimento,
                                                             }).ToList();

                return ListResult.GetListResult(procedimentos, new string[] { "Id", "CO_PROCEDIMENTO", "NO_PROCEDIMENTO" });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ListResult ListarInternacoes(Guid unidadeId)
        {

            var registros = (repository.MovimentacaoLeito.Include("Internacao.Atendimento.Acolhimento.Paciente").Include("Internacao.Atendimento.Checkout")
                                                                  .Include("Internacao.Atendimento.Acolhimento.Profissional.Unidade").Include("LeitoAtual.Enfermaria")
                            .Where(m => m.Internacao.Atendimento.FimAtendimento == null && m.Internacao.Atendimento.Checkout == null
                                     && m.Internacao.Atendimento.Acolhimento.Profissional.Unidade.Id == unidadeId)).ToList();

            var lista = (from m in registros
                         group m by m.Internacao.Atendimento.Acolhimento.Id into a
                         select new FilaDeObservacaoReadModel
                         {
                             acolhimentoId = a.Key,
                             Enfermaria = a.OrderByDescending(x => x.DataMovimentacao).FirstOrDefault().LeitoAtual.Enfermaria != null ? a.OrderByDescending(x => x.DataMovimentacao).FirstOrDefault().LeitoAtual.Enfermaria.Nome : "",
                             Leito = a.OrderByDescending(x => x.DataMovimentacao).FirstOrDefault().LeitoAtual.Enfermaria != null ? a.OrderByDescending(x => x.DataMovimentacao).FirstOrDefault().LeitoAtual.Nome : "",
                             Paciente = a.FirstOrDefault().Internacao.Atendimento.Acolhimento.Paciente.Nome
                         }).ToList(); //.OrderBy(x => x.Enfermaria).ThenBy(x => x.Leito).ToList();
            return ListResult.GetListResult(lista, new string[] { "acolhimentoId", "Enfermaria", "Leito", "Paciente" });

        }

        public ListResult ListarInternacoesComAlta(Guid unidadeId)
        {

            var registros = (repository.MovimentacaoLeito.Include("Internacao.Atendimento.Acolhimento.Paciente").Include("Internacao.Atendimento.Checkout")
                                                                  .Include("Internacao.Atendimento.Acolhimento.Profissional.Unidade").Include("LeitoAtual.Enfermaria")
                            .Where(m => m.Internacao.Atendimento.Checkout != null
                                     && m.Internacao.Atendimento.Acolhimento.Profissional.Unidade.Id == unidadeId)).ToList();

            var lista = (from m in registros
                         group m by m.Internacao.Atendimento.Acolhimento.Id into a
                         select new FilaDeObservacaoReadModel
                         {
                             acolhimentoId = a.Key,
                             Enfermaria = a.OrderByDescending(x => x.DataMovimentacao).FirstOrDefault().LeitoAtual.Enfermaria != null ? a.OrderByDescending(x => x.DataMovimentacao).FirstOrDefault().LeitoAtual.Enfermaria.Nome : "",
                             Leito = a.OrderByDescending(x => x.DataMovimentacao).FirstOrDefault().LeitoAtual.Enfermaria != null ? a.OrderByDescending(x => x.DataMovimentacao).FirstOrDefault().LeitoAtual.Nome : "",
                             Paciente = a.FirstOrDefault().Internacao.Atendimento.Acolhimento.Paciente.Nome,
                             CodigoBoletim = a.FirstOrDefault().Internacao.Atendimento.Acolhimento.CodigoBoletim
                         }).ToList(); //.OrderBy(x => x.Enfermaria).ThenBy(x => x.Leito).ToList();
            return ListResult.GetListResult(lista, new string[] { "acolhimentoId", "Enfermaria", "Leito", "Paciente", "CodigoBoletim" });

        }

        /// <summary>
        /// Relatórios
        /// </summary>
        public ListResult ListarPacientesRegistradosNoDia(DateTime inicio, DateTime fim, Guid UnidadeId)
        {
            try
            {
                fim = fim.AddDays(1);

                List<RptPacientesRegistradosReadModel> pacientesRegistrados = new List<RptPacientesRegistradosReadModel>();
                pacientesRegistrados.AddRange(from x in repository.Acolhimento.Include("Paciente").Include("Servico").Include("Profissional.Unidade")
                .Where(x => (x.DataHora >= inicio && x.DataHora <= fim) && (x.Profissional.Unidade.Id.Equals(UnidadeId)) && x.Paciente != null).ToList().OrderBy(x => x.DataHora).ThenBy(x => x.Paciente.Nome)
                                              group x by x.DataHora.ToString("MM/dd/yyyy", new CultureInfo("en-US")) into a
                                              select new RptPacientesRegistradosReadModel
                                              {
                                                  Data = Convert.ToDateTime(a.Key).ToString("dd/MM/yyyy", new CultureInfo("en-US")),
                                                  Atendimentos = (from at in a.ToList()
                                                                  select new RptPacientesRegistradosReadModel
                                                                  {
                                                                      CodigoBoletim = at.CodigoBoletim,
                                                                      Nome = at.Paciente.Nome.ToUpper(),
                                                                      DataNascimento = at.Paciente.DataNascimento != null ? at.Paciente.DataNascimento.Value.ToString("dd/MM/yyyy", new CultureInfo("en-US")) : null,
                                                                      Idade = at.Paciente.DataNascimento != null ? (DateTime.Now.Year - at.Paciente.DataNascimento.Value.Year).ToString() : null,
                                                                      Descricao = at.Servico != null ? at.Servico.Descricao.ToUpper() : null
                                                                  }),
                                                  Quantidade = a.ToList().Count()
                                              });

                return ListResult.GetListResult(pacientesRegistrados.ToList(), new string[] { "Data", "Atendimentos", "Quantidade" });
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public ListResult ListarProducaoDiariaPorSetor(DateTime inicio, DateTime fim, Guid UnidadeId)
        {
            fim = fim.AddDays(1);
            var producaoDiaria =
                (from p in ((from x in repository.Acolhimento.Include("Servico").Include("Profissional.Unidade")
                                 .Where(x => x.Servico.Descricao != null && (x.DataHora >= inicio && x.DataHora <= fim) && (x.Profissional.Unidade.Id.Equals(UnidadeId))).ToList()
                             group x by x.Servico.Descricao into p
                             select new ProducaoPorSetorReadModel
                             {
                                 Clinica = p.Key.ToUpper(),
                                 Quantidade = p.ToList().Count(),
                                 Setor = "ACOLHIMENTO"
                             })
                             .Union
                            (from x in repository.ServicoSocial
                                  .Where(x => x.InicioAtendimento >= inicio && x.InicioAtendimento <= fim).ToList().Take(1)
                             group x by x.Id into p
                             select new ProducaoPorSetorReadModel
                             {
                                 Clinica = "SERVIÇO SOCIAL",
                                 Quantidade = repository.ServicoSocial.Where(x => x.InicioAtendimento >= inicio && x.InicioAtendimento <= fim).Count(),
                                 Setor = "ACOLHIMENTO"
                             })
                             .Union
                            (from x in repository.Atendimento.Include("Acolhimento.Servico")
                                 .Where(x => x.InicioAtendimento >= inicio && x.InicioAtendimento <= fim && x.Acolhimento.Servico != null).ToList()
                             group x by x.Acolhimento.Servico.Descricao into p
                             select new ProducaoPorSetorReadModel
                             {
                                 Clinica = p.Key.ToUpper(),
                                 Quantidade = p.ToList().Count(),
                                 Setor = "ATENDIMENTO"
                             })
                             .Union
                             (from x in repository.ServicoSocial
                             .Where(x => x.InicioAtendimento >= inicio && x.InicioAtendimento <= fim && x.FimAtendimento != null).ToList().Take(1)
                              group x by x.Id into p
                              select new ProducaoPorSetorReadModel
                              {
                                  Clinica = "SERVIÇO SOCIAL",
                                  Quantidade = repository.ServicoSocial.Where(x => x.InicioAtendimento >= inicio && x.InicioAtendimento <= fim && x.FimAtendimento != null).Count(),
                                  Setor = "ACOLHIMENTO"
                              })
                             .Union
                             (from x in repository.Internacao.Include("Atendimento.Acolhimento.Servico").Include("Atendimento.Profissional.Unidade")
                             .Where(x => x.Atendimento.Acolhimento.Servico != null && x.DataInternacao > inicio && x.DataInternacao <= fim && x.Atendimento.Profissional.Unidade.Id == UnidadeId).ToList()
                              group x by x.Atendimento.Acolhimento.Servico.Descricao into p
                              select new ProducaoPorSetorReadModel
                              {
                                  Clinica = p.Key.ToUpper(),
                                  Quantidade = p.Count(),
                                  Setor = "Internação de Observação"
                              })
                              .Union
                              (from x in repository.ClassificacaoDeRisco.Include("Acolhimento.Servico")
                                  .Where(x => x.Acolhimento.DataHora > inicio && x.Acolhimento.DataHora <= fim && x.Acolhimento.Servico != null).ToList()
                               group x by x.Acolhimento.Servico.Descricao into p
                               select new ProducaoPorSetorReadModel
                               {
                                   Clinica = p.Key.ToUpper(),
                                   Quantidade = p.ToList().Count(),
                                   Setor = "Classificacao de Risco"
                               })).ToList()
                 group p by p.Setor into a
                 select new ProducaoPorSetorReadModel
                 {
                     Setor = a.Key,
                     ListaProducaoSetor = a.ToList(),
                     Quantidade = a.ToList().Sum(x => x.Quantidade),
                 }).ToList();


            return ListResult.GetListResult(producaoDiaria, new string[] { "Setor", "ListaProducaoSetor", "Quantidade", "Clinica" });
        }

        public ListResult ListarFilaEspera(Guid UnidadeId)
        {
            DateTime dtAnterior = DateTime.Today.AddDays(-1);
            var producaoDiaria =
               (from p in ((from x in repository.ServicoSocial.Include("Acolhimento.Paciente").Include("Acolhimento.Servico").Include("Acolhimento.Profissional.Unidade")
                              .Where(x => (x.FimAtendimento == null) && (x.Profissional.Unidade.Id.Equals(UnidadeId))).ToList()
                              .Select(x => new RptFilaDeEspera
                              {
                                  Paciente = x.Acolhimento.Paciente.Nome.ToUpper(),
                                  InicioAtendimento = x.InicioAtendimento.ToString("dd/MM/yyyy hh:mm:ss tt"),
                                  Risco = "",
                                  Tempo = CalculaTempo(x.InicioAtendimento).ToString(),
                                  Clinica = x.Acolhimento != null ? x.Acolhimento.Servico != null ? x.Acolhimento.Servico.Descricao.ToUpper() : "" : "",
                                  //CodigoBoletim = x.Acolhimento.CodigoBoletim,
                                  Agrupamento = "ASSISTENTE SOCIAL"
                              })
                            group x by x.Agrupamento into a
                            select new RptFilaDeEspera
                            {
                                ID = "1",
                                Agrupamento = a.Key.ToUpper(),
                                FilaDeEspera = a.ToList(),
                                TipoFila = "REGISTRO"
                            })
                            .Union
                            (from x in repository.Acolhimento.Include("Atendimento").Include("Profissional.Unidade").Include("Servico").Include("Classificacao.Risco")
                              .Where(x => (x.Atendimento.Count <= 0) && x.Paciente.PendenteCadastro == false && (x.Profissional.Unidade.Id.Equals(UnidadeId))
                                        && (x.DataHora >= DateTime.Today && x.DataHora > dtAnterior) && x.Classificacao.Any() == false).ToList()
                              .Select(x => new RptFilaDeEspera
                              {
                                  Paciente = x.Paciente.Nome.ToUpper(),
                                  InicioAtendimento = x.DataHora.ToString("dd/MM/yyyy hh:mm:ss"),
                                  Risco = "",
                                  Tempo = CalculaTempo(x.DataHora).ToString(),
                                  Clinica = x.Servico != null ? x.Servico.Descricao.ToUpper() : "",
                                  Agrupamento = "CLASSIFICAÇÃO DE RISCO"
                              })
                             group x by x.Agrupamento into a
                             select new RptFilaDeEspera
                             {
                                 ID = "1",
                                 Agrupamento = a.Key.ToUpper(),
                                 FilaDeEspera = a.ToList(),
                                 TipoFila = "REGISTRO"
                             })
                            .Union

                           (from x in repository.Acolhimento.Include("Paciente").Include("Classificacao").Include("Servico").Include("Atendimento").Include("Profissional.Unidade")
                                .Where(x => x.Atendimento.Count <= 0 && (x.Profissional.Unidade.Id.Equals(UnidadeId))).ToList()
                            group x by x.Servico.Descricao into p
                            select new RptFilaDeEspera
                            {
                                ID = "2",
                                Agrupamento = p.Key.ToUpper(),
                                FilaDeEspera = p.ToList().Select(x => new RptFilaDeEspera
                                {
                                    Paciente = x.Paciente.Nome.ToUpper(),
                                    InicioAtendimento = x.DataHora.ToString("dd/MM/yyyy hh:mm:ss"),
                                    Risco = "",
                                    Tempo = CalculaTempo(x.DataHora).ToString(),
                                    Clinica = x.Servico != null ? x.Servico.Descricao.ToUpper() : ""
                                }).ToList(),
                                TipoFila = "REGISTRO"
                            })
                            .Union
                           (from x in repository.Acolhimento.Include("Paciente").Include("Classificacao.Risco").Include("Servico").Include("Atendimento.Checkout").Include("Profissional.Unidade")
                                .Where(x => x.Atendimento.Any() == false && x.Profissional.Unidade.Id.Equals(UnidadeId)
                                       && x.Classificacao.Any() == true
                                       && x.Paciente.PendenteCadastro == false).ToList()
                            group x by x.Servico.Descricao into p
                            select new RptFilaDeEspera
                            {
                                ID = "3" + p.Key.Substring(0, 2),
                                Agrupamento = p.Key.ToUpper(),
                                FilaDeEspera = p.ToList().Select(x => new RptFilaDeEspera
                                {
                                    Paciente = x.Paciente.Nome.ToUpper(),
                                    InicioAtendimento = x.DataHora.ToString("dd/MM/yyyy hh:mm:ss"),
                                    Risco = x.Classificacao.FirstOrDefault() != null ? x.Classificacao.FirstOrDefault().Risco.Descricao : "",
                                    Tempo = CalculaTempo(x.DataHora).ToString(),
                                    Clinica = x.Servico != null ? x.Servico.Descricao.ToUpper() : "",
                                    CodigoBoletim = x.CodigoBoletim
                                }).ToList(),
                                TotalVermelho = p.ToList().Where(x => x.Classificacao.Count > 0 && x.Classificacao.FirstOrDefault().Risco.Descricao == "Vermelho").Count().ToString(),
                                TotalRiscoAzul = p.ToList().Where(x => x.Classificacao.Count > 0 && x.Classificacao.FirstOrDefault().Risco.Descricao == "Azul").Count().ToString(),
                                TotalAmarelo = p.ToList().Where(x => x.Classificacao.Count > 0 && x.Classificacao.FirstOrDefault().Risco.Descricao == "Amarelo").Count().ToString(),
                                TotalVerde = p.ToList().Where(x => x.Classificacao.Count > 0 && x.Classificacao.FirstOrDefault().Risco.Descricao == "Verde").Count().ToString(),
                                Total = p.ToList().Count.ToString(),
                                TipoFila = "ATENDIMENTO"
                            })
                            .Union
                            (from x in repository.ServicoSocial.Include("Acolhimento.Paciente").Include("Acolhimento.Servico").Include("Profissional.Unidade")
                                                              .Where(x => x.FimAtendimento == null && x.Acolhimento.Paciente.PendenteCadastro == false
                                                                       && x.Acolhimento.Profissional.Unidade.Id.Equals(UnidadeId)
                                                              ).ToList()
                                                              .Select(x => new RptFilaDeEspera
                                                              {
                                                                  Paciente = x.Acolhimento.Paciente.Nome.ToUpper(),
                                                                  InicioAtendimento = x.InicioAtendimento.ToString("dd/MM/yyyy hh:mm:ss"),
                                                                  Risco = "",
                                                                  Tempo = CalculaTempo(x.InicioAtendimento).ToString(),
                                                                  Clinica = x.Acolhimento.Servico != null ? x.Acolhimento.Servico.Descricao.ToUpper() : "",
                                                                  CodigoBoletim = x.Acolhimento.CodigoBoletim,
                                                                  Agrupamento = "ASSISTENTE SOCIAL"
                                                              })
                             group x by x.Agrupamento into a
                             select new RptFilaDeEspera
                             {
                                 ID = "4",
                                 Agrupamento = a.Key.ToUpper(),
                                 FilaDeEspera = a.ToList(),
                                 TipoFila = "ATENDIMENTO"
                             })).ToList()
                group p by p.TipoFila into a
                select new RptFilaDeEspera
                {
                    TipoFila = a.Key.ToUpper().ToUpper(),
                    FilaDeEspera = a.ToList()
                }).ToList();

            return ListResult.GetListResult(producaoDiaria, new string[] { "TipoFila", "FilaDeEspera" });
        }

        public ListResult ListarPendentesERealizados(DateTime inicio, DateTime fim, Guid UnidadeId)
        {

            fim = fim.AddDays(1);
            var pendentesRealizados = (from at in ((from a in repository.Atendimento.Include("Acolhimento.Paciente").Include("Acolhimento.Servico").Include("Checkout.MotivoCheckout.Tipo").Include("Acolhimento.Profissional.Unidade")
                                                                       .Where(a => (a.InicioAtendimento >= inicio && a.InicioAtendimento <= fim) && a.Profissional.Unidade.Id.Equals(UnidadeId) && a.Checkout == null).ToList().OrderBy(x => x.Acolhimento.DataHora).ThenBy(x => x.Acolhimento.Paciente.Nome)
                                                    select new RelPendentesRealizadosReadModel
                                                    {
                                                        StatusBoletim = "Pendentes",
                                                        Atendimento = ViewModelUtils.ToViewModel<HealthCare.Domain.Entities.DbClasses.Atendimento.Atendimento, AtendimentoReadModel>(a),
                                                        MotivoSaida = a.Checkout != null ? a.Checkout.MotivoCheckout.Descricao : ""
                                                    }).Union
                                                   (from a in repository.Atendimento.Include("Acolhimento.Paciente").Include("Acolhimento.Servico").Include("Checkout.MotivoCheckout.Tipo").Include("Acolhimento.Profissional.Unidade")
                                                                       .Where(a => (a.InicioAtendimento >= inicio && a.InicioAtendimento <= fim) && a.Profissional.Unidade.Id.Equals(UnidadeId) && a.Checkout != null).ToList().OrderBy(x => x.Acolhimento.DataHora).ThenBy(x => x.Acolhimento.Paciente.Nome)
                                                    select new RelPendentesRealizadosReadModel
                                                    {
                                                        StatusBoletim = "Realizados",
                                                        Atendimento = ViewModelUtils.ToViewModel<HealthCare.Domain.Entities.DbClasses.Atendimento.Atendimento, AtendimentoReadModel>(a),
                                                        MotivoSaida = a.Checkout != null ? a.Checkout.MotivoCheckout.Tipo.Descricao : ""
                                                    }))
                                       group at by at.StatusBoletim into x
                                       select new RelPendentesRealizadosReadModel()
                                       {
                                           StatusBoletim = x.Key,
                                           Atendimentos = x.ToList(),
                                           Totais = x.ToList().Where(y => y.StatusBoletim == y.StatusBoletim).Count().ToString()
                                       }).OrderBy(x => x.StatusBoletim).ToList();

            return ListResult.GetListResult(pendentesRealizados, new string[] { "StatusBoletim", "Atendimentos", "Totais" });
        }

        public ListResult ListaNominalDeObitos(DateTime inicio, DateTime fim, Guid UnidadeId)
        {

            fim = fim.AddDays(1);
            List<NominalObitosReadModel> listaNominal = (from a in repository.Atendimento.Include("Checkout.MotivoCheckout.Tipo")
                                                         .Include("Acolhimento.Paciente").Include("Profissional.Unidade").Include("PrimeiroDiagnostico")
                                                         .Where(a => (a.Checkout != null) && (a.Checkout.MotivoCheckout.Tipo.Descricao == "Em caso de óbito com necrópsia") // "Em caso de óbito com necrópsia"
                                                                  && (a.FimAtendimento >= inicio && a.FimAtendimento <= fim) && (a.Profissional.Unidade.Id.Equals(UnidadeId))).ToList()
                                                         select new NominalObitosReadModel()
                                                         {
                                                             Boletim = a.Acolhimento.CodigoBoletim,
                                                             Origem = "Urgência",
                                                             TipoDeSaida = a.Checkout.MotivoCheckout.Tipo.Descricao,
                                                             Nome = a.Acolhimento.Paciente.Nome,
                                                             Idade = a.Acolhimento.Paciente.IdadePorExtenso,
                                                             Diagnostico = a.PrimeiroDiagnostico != null ? a.PrimeiroDiagnostico.Descricao : "",
                                                             Sexo = a.Acolhimento.Paciente.Sexo,
                                                             Profissional = a.Profissional.Nome,
                                                             DataObito = a.FimAtendimento.Value.ToString("dd/MM/yyyy")
                                                         }).ToList();

            return ListResult.GetListResult(listaNominal, new string[] { "Boletim", "Origem", "TipoDeSaida", "Nome", "Idade", "Diagnostico", "Sexo", "Profissional", "DataObito" });
        }

        public ListResult EstatisticaAtendimentoPorProfissional(DateTime inicio, DateTime fim, Guid UnidadeId)
        {

            fim = fim.AddDays(1);
            var lista = (from a in repository.Atendimento.Include("Profissional.Unidade").Include("Acolhimento.Paciente").Include("Acolhimento.Servico")
                                                         .Include("Checkout.MotivoCheckout.Tipo")
                                                         .Where(x => (x.InicioAtendimento >= inicio && x.InicioAtendimento <= fim) && (x.Profissional.Unidade.Id.Equals(UnidadeId))).ToList()
                         group a by a.InicioAtendimento.ToString("dd/MM/yyyy", new CultureInfo("en-US")) into x
                         select new EstatisticaAtendimentoReadModel
                         {
                             Data = x.Key, // string.Format("{0}/{1}/{2}", x.Key.Day.ToString(), x.Key.Month.ToString(), x.Key.Year),                                                                                             //Data = Convert.ToDateTime(x.Key.ToString()).ToString("MM/dd/yyyy"),
                             AtendimentosPorProfissional = from at in x
                                                           group at by at.Profissional.Nome into p
                                                           select new EstatisticaAtendimentoReadModel
                                                           {
                                                               Profissional = p.Key.ToUpper(),
                                                               Atendimentos = ViewModelUtils.ToListViewModel<HealthCare.Domain.Entities.DbClasses.Atendimento.Atendimento, AtendimentoReadModel>(p.ToList()),
                                                               TotalProfissional = p.Count().ToString()
                                                           },
                             Total = x.Count().ToString()
                         }).ToList().OrderBy(x => x.Data).ToList();

            return ListResult.GetListResult(lista, new string[] { "Data", "AtendimentosPorProfissional", "Total" });

        }

        public ListResult EstatisticaAtendimentoTotalPorProfissional(DateTime inicio, DateTime fim, Guid UnidadeId)
        {
            fim = fim.AddDays(1);
            var lista =
                         (from a in repository.Atendimento.Include("Profissional.Unidade").Include("Acolhimento.Paciente").Include("Acolhimento.Servico")
                          .Where(x => (x.InicioAtendimento >= inicio && x.InicioAtendimento <= fim) && (x.Profissional.Unidade.Id.Equals(UnidadeId))).ToList()
                          group a by a.Profissional.Nome into x
                          select new EstatisticaAtendimentoReadModel
                          {
                              Profissional = x.Key,
                              TotalProfissional = x.Count().ToString()
                          }).ToList();

            return ListResult.GetListResult(lista, new string[] { "Profissional", "TotalProfissional" });
        }

        public ListResult EstatisticaMensalProntoAtendimento(int mes, int ano, Guid UnidadeId)
        {
            var lista = repository.Atendimento.Include("Acolhimento.Servico").Include("Checkout.MotivoCheckout.Tipo").Include("Acolhimento.Profissional.Unidade").Include("Internacoes")
                        .Where(x => x.Acolhimento.Servico != null && x.Checkout != null && (x.FimAtendimento.Value.Month == mes && x.FimAtendimento.Value.Year == ano) && (x.Profissional.Unidade.Id.Equals(UnidadeId)))
                        .OrderBy(x => x.FimAtendimento).ToList();


            var listaAtendimento = repository.Atendimento.Include("Acolhimento.Servico").Include("Checkout.MotivoCheckout.Tipo").Include("Acolhimento.Profissional.Unidade").Include("Internacoes")
                                   .Where(x => x.Acolhimento.Servico != null
                                           && (x.InicioAtendimento.Month == mes && x.InicioAtendimento.Year == ano)
                                           && x.Profissional.Unidade.Id.Equals(UnidadeId));


            var registros = (from a in lista
                             group a by a.Acolhimento.Servico.Descricao into x
                             select new EstatisticaMensalReadModel
                             {
                                 Especialidade = x.Key,
                                 Atendimentos = listaAtendimento.Where(y => y.Acolhimento.Servico.Descricao == x.Key).Count().ToString(),
                                 Altas = x.Sum(y => ChecaTipoCheckout(y.Checkout, "Em caso de Alta")).ToString(),
                                 Obitos = x.Sum(y => ChecaTipoCheckout(y.Checkout, "Em caso de óbito com necrópsia")).ToString(),
                                 Remocoes = x.Sum(y => ChecaTipoCheckout(y.Checkout, "Em caso de Transferência")).ToString(), //  Checa "Em caso de Transferência" : y.Checkout == null).Count().ToString(),
                                 Internacoes = x.Where(y => y.Internacoes.Count > 0 ? y.Internacoes.Count > 0 : y.Internacoes.Count == -1).Count().ToString(),
                             }).ToList();
            return ListResult.GetListResult(registros, new string[] { "Especialidade", "Atendimentos", "Altas", "Obitos", "Remocoes", "Internacoes" });

        }

        public ListResult EstatistaOdontoProcedimentoDario(DateTime inicio, DateTime fim, Guid UnidadeId)
        {
            try
            {
                fim = fim.AddDays(1);

                var aps = repository.AtendimentoProcedimento.ToList();

                /// atendimento procedimento

                var atendimentos = (from a in repository.Atendimento.Include("Acolhimento.Profissional.Unidade")
                                                                           .Where(x => x.InicioAtendimento >= inicio && x.InicioAtendimento <= fim && x.Acolhimento.Profissional.Unidade.Id == UnidadeId)
                                    from c in aps
                                    where a.Id == c.AtendimentoId
                                    select a).GroupBy(x => x.InicioAtendimento)
                                      .Select(a => new ProcedimentoAtendimentoReadModel
                                      {
                                          Data = a.Key.ToString(),
                                          Procedimentos = (from b in a
                                                           from p in aps
                                                           where b.Id == p.AtendimentoId
                                                           select new ProcedimentoAtendimentoReadModel
                                                           {
                                                               NomeProcedimento = p.CodigoProcedimento + " - " + p.NomeProcedimento,
                                                           }).GroupBy(x => x.NomeProcedimento).Select(b => new ProcedimentoAtendimentoReadModel
                                                           {
                                                               NomeProcedimento = b.Key.ToString(),
                                                               QuantidadeItem = b.Count()
                                                           }),

                                          QuantidadeTotal = a.Count()
                                      });





                //var lista = repository.Atendimento.Include("Acolhimento.Profissional.Unidade")
                //                      .Where(x => x.InicioAtendimento >= inicio && x.InicioAtendimento <= fim && x.Acolhimento.Profissional.Unidade.Id == UnidadeId)
                //                      .GroupBy(x => x.InicioAtendimento)
                //                      .Select(a => new ProcedimentoAtendimentoReadModel
                //                      {
                //                          Data = a.Key.ToString(),
                //                          Procedimentos = (from b in a
                //                                           from p in repository.AtendimentoProcedimento
                //                                           where b.Id == p.AtendimentoId
                //                                           select new ProcedimentoAtendimentoReadModel
                //                                           {
                //                                               NomeProcedimento = p.CodigoProcedimento + " - " + p.NomeProcedimento,
                //                                           }).GroupBy(x => x.NomeProcedimento).Select(b => new ProcedimentoAtendimentoReadModel
                //                                           {
                //                                               NomeProcedimento = b.Key.ToString(),
                //                                               QuantidadeItem = b.Count()
                //                                           }),
                //                          QuantidadeTotal = a.Count()
                //                      });

                return ListResult.GetListResult(atendimentos.ToList(), new string[] { "Data", "Procedimentos", "NomeProcedimento", "QuantidadeItem", "QuantidadeTotal" });
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public ListResult ListarDiagnosticosMaisAtendidos(DateTime inicio, DateTime fim, Guid UnidadeId)
        {
            try
            {
                fim = fim.AddDays(1);

                var diagnosticos = (from at in repository.Atendimento.Include("PrimeiroDiagnostico").Include("Profissional").Include("Profissional.Unidade")
                                               .Where(x => (x.InicioAtendimento >= inicio && x.InicioAtendimento <= fim)
                                                        && (x.PrimeiroDiagnostico != null)
                                                        && (x.Profissional.Unidade.Id == UnidadeId)
                                               ).ToList()
                                    group at by at.PrimeiroDiagnostico.CodigoDescricao into x
                                    select new DiagnosticosMaisAtendidosReadModel
                                    {
                                        Cid = x.Key,
                                        Quantidade = x.Count()
                                    }).ToList().OrderByDescending(x => x.Quantidade).ToList();

                return ListResult.GetListResult(diagnosticos, new string[] { "Cid", "Quantidade" });
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public ListResult ListarProducaoResumoAtendimento(DateTime inicio, DateTime fim, Guid UnidadeId)
        {
            fim = fim.AddDays(1);

            List<ProducaoResumoAtendimentoReadModel> registros = new List<ProducaoResumoAtendimentoReadModel>();

            IEnumerable<Acolhimento> acolhimentos = (from a in repository.Acolhimento.Include("Servico").Include("Paciente")
                                                               .Include("Classificacao.Risco").Include("Atendimento.Internacoes.Movimentacoes.LeitoAtual")
                                                               .Include("Atendimento.Checkout.MotivoCheckout.Tipo").Include("Profissional.Unidade")
                                                     where (a.DataHora >= inicio && a.DataHora <= fim && a.Paciente.Unidade.Id == UnidadeId)
                                                     select a).ToList().Where(x => x.Profissional.Unidade.Id.Equals(UnidadeId));

            /// PRÉ-ATENDIMENTO
            /// -------------------------------------------------------------------------------------------------------------
            registros.Add(new ProducaoResumoAtendimentoReadModel { Grupo = "PreAtendimento", Item = "ACOLHIMENTO", Quantidade = acolhimentos.Count() });
            registros.Add(new ProducaoResumoAtendimentoReadModel { Grupo = "PreAtendimento", Item = "URGÊNCIA", Quantidade = acolhimentos.Where(x => x.Atendimento != null).Count() });

            /// CLASSIFICAÇÃO DE RISCO
            /// -------------------------------------------------------------------------------------------------------------
            registros.Add(new ProducaoResumoAtendimentoReadModel { Grupo = "Classificação de Risco", Item = "Não Risco", Quantidade = acolhimentos.Where(x => x.Classificacao == null).Count() });
            registros.Add(new ProducaoResumoAtendimentoReadModel { Grupo = "Classificação de Risco", Item = "Azul", Quantidade = acolhimentos.Where(x => x.Classificacao.Count > 0 && x.Classificacao.FirstOrDefault().Risco.Descricao.Contains("Azul")).Count() });
            registros.Add(new ProducaoResumoAtendimentoReadModel { Grupo = "Classificação de Risco", Item = "Verde", Quantidade = acolhimentos.Where(x => x.Classificacao.Count > 0 && x.Classificacao.FirstOrDefault().Risco.Descricao.Contains("Verde")).Count() });
            registros.Add(new ProducaoResumoAtendimentoReadModel { Grupo = "Classificação de Risco", Item = "Amarelo", Quantidade = acolhimentos.Where(x => x.Classificacao.Count > 0 && x.Atendimento.Count == 0 && x.Classificacao.FirstOrDefault().Risco.Descricao.Contains("Amarelo")).Count() });
            registros.Add(new ProducaoResumoAtendimentoReadModel { Grupo = "Classificação de Risco", Item = "Amarelo Consultorio", Quantidade = acolhimentos.Where(x => x.Classificacao.Count > 0 && x.Atendimento.Count > 0 && x.Atendimento[0].Internacoes.Any() == false && x.Classificacao.FirstOrDefault().Risco.Descricao.Contains("Amarelo")).Count() });
            registros.Add(new ProducaoResumoAtendimentoReadModel { Grupo = "Classificação de Risco", Item = "Amarelo Observacao", Quantidade = acolhimentos.Where(x => x.Classificacao.Count > 0 && x.Atendimento.Count > 0 && x.Atendimento[0].Internacoes.Any() == true && x.Classificacao.FirstOrDefault().Risco.Descricao.Contains("Amarelo")).Count() });
            registros.Add(new ProducaoResumoAtendimentoReadModel { Grupo = "Classificação de Risco", Item = "Vermelho", Quantidade = acolhimentos.Where(x => x.Classificacao.Count > 0 && x.Classificacao.FirstOrDefault().Risco.Descricao.Contains("Vermelho")).Count() });

            /// ATENDIMENTOS INICIADOS NO PERÍODO
            /// -------------------------------------------------------------------------------------------------------------
            var atendimentosIniciados = repository.Atendimento.Include("Acolhimento.Servico").Where(x => x.InicioAtendimento >= inicio && x.InicioAtendimento <= fim);
            var i = atendimentosIniciados.Count();
            registros.Add(new ProducaoResumoAtendimentoReadModel { Grupo = "ATENDIMENTOS", Item = "PEDIATRIA", Quantidade = atendimentosIniciados.Where(x => x.Acolhimento.Servico != null && x.Acolhimento.Servico.Descricao == "Pediatria").Count() });
            registros.Add(new ProducaoResumoAtendimentoReadModel { Grupo = "ATENDIMENTOS", Item = "ODONTOLOGIA", Quantidade = atendimentosIniciados.Where(x => x.Acolhimento.Servico != null && x.Acolhimento.Servico.Descricao == "Odontologia").Count() });
            registros.Add(new ProducaoResumoAtendimentoReadModel { Grupo = "ATENDIMENTOS", Item = "ORTOPEDIA", Quantidade = atendimentosIniciados.Where(x => x.Acolhimento.Servico != null && x.Acolhimento.Servico.Descricao == "Ortopedia").Count() });
            registros.Add(new ProducaoResumoAtendimentoReadModel { Grupo = "ATENDIMENTOS", Item = "CLINICA MÉDICA", Quantidade = atendimentosIniciados.Where(x => x.Acolhimento.Servico != null && x.Acolhimento.Servico.Descricao == "Clínica Médica").Count() });
            registros.Add(new ProducaoResumoAtendimentoReadModel { Grupo = "ATENDIMENTOS", Item = "ASSISTENTE SOCIAL", Quantidade = repository.ServicoSocial.Where(x => x.InicioAtendimento >= inicio && x.InicioAtendimento <= inicio).Count() });


            /// Todos os atendimentos finalizados
            /// -------------------------------------------------------------------------------------------------------------
            var atendimentos = from a in repository.Atendimento.Include("Acolhimento.Servico").Include("Acolhimento.Paciente").Include("Internacoes.Movimentacoes.LeitoAtual").Include("Checkout.MotivoCheckout.Tipo").Include("Profissional.Unidade")
                               where a.Checkout != null && a.FimAtendimento >= inicio && a.FimAtendimento <= fim && a.Profissional.Unidade.Id == UnidadeId
                               select a;
            try
            {
                var atendimentosObservacao = acolhimentos.Where(x => x.Atendimento.Count > 0 && (x.Atendimento.FirstOrDefault().InicioAtendimento >= inicio && x.Atendimento.FirstOrDefault().InicioAtendimento <= fim));

                /// OBSERVAÇÃO
                /// -------------------------------------------------------------------------------------------------------------
                var observacoes = atendimentosObservacao.Where(x => x.Atendimento.Count > 0 && x.Atendimento.FirstOrDefault().Internacoes.Count > 0 && x.Paciente.IdadeEmAnos != null);

                registros.Add(new ProducaoResumoAtendimentoReadModel
                {
                    Grupo = "OBSERVAÇÃO",
                    Item = "Observação Amarelo Adulto",
                    Quantidade = observacoes.Where(x => x.Paciente.IdadeEmAnos >= 18
                                                     && x.Atendimento.Count > 0 && x.Atendimento.FirstOrDefault().Internacoes.FirstOrDefault().Movimentacoes.OrderByDescending(y => y.DataMovimentacao).FirstOrDefault().LeitoAtual.TipoLeito == Domain.Entities.DbClasses.Cadastro.TipoLeito.SalaAmarela).Count()
                });
                registros.Add(new ProducaoResumoAtendimentoReadModel
                {
                    Grupo = "OBSERVAÇÃO",
                    Item = "Observação Amarelo Pediatria",
                    Quantidade = observacoes.Where(x => x.Paciente.IdadeEmAnos < 18
                                                     && x.Atendimento.Count > 0 && x.Atendimento.FirstOrDefault().Internacoes.FirstOrDefault().Movimentacoes.OrderByDescending(y => y.DataMovimentacao).FirstOrDefault().LeitoAtual.TipoLeito == Domain.Entities.DbClasses.Cadastro.TipoLeito.SalaAmarela).Count()
                });
                registros.Add(new ProducaoResumoAtendimentoReadModel
                {
                    Grupo = "OBSERVAÇÃO",
                    Item = "Observação Vermelho Adulto",
                    Quantidade = observacoes.Where(x => x.Paciente.IdadeEmAnos >= 18
                                                     && x.Atendimento.Count > 0 && x.Atendimento.FirstOrDefault().Internacoes.FirstOrDefault().Movimentacoes.OrderByDescending(y => y.DataMovimentacao).FirstOrDefault().LeitoAtual.TipoLeito == Domain.Entities.DbClasses.Cadastro.TipoLeito.SalaVermelha).Count()
                });
                registros.Add(new ProducaoResumoAtendimentoReadModel
                {
                    Grupo = "OBSERVAÇÃO",
                    Item = "Observação Vermelho Pediatria",
                    Quantidade = observacoes.Where(x => x.Paciente.IdadeEmAnos < 18
                                                     && x.Atendimento.Count > 0 && x.Atendimento.FirstOrDefault().Internacoes.Count > 0 && x.Atendimento.FirstOrDefault().Internacoes.FirstOrDefault().Movimentacoes.OrderByDescending(y => y.DataMovimentacao).FirstOrDefault().LeitoAtual.TipoLeito == Domain.Entities.DbClasses.Cadastro.TipoLeito.SalaVermelha).Count()
                });

                /// ÓBITOS
                /// -------------------------------------------------------------------------------------------------------------
                registros.Add(new ProducaoResumoAtendimentoReadModel { Grupo = "ÓBITOS", Item = "PEDIATRIA", Quantidade = atendimentos.Where(x => x.Checkout != null && x.Checkout.MotivoCheckout.Tipo.Descricao == "Em caso de óbito com necrópsia" && x.Acolhimento.Servico.Descricao == "Pediatria").Count() });
                registros.Add(new ProducaoResumoAtendimentoReadModel { Grupo = "ÓBITOS", Item = "CLINICA MÉDICA", Quantidade = atendimentos.Where(x => x.Checkout != null && x.Checkout.MotivoCheckout.Tipo.Descricao == "Em caso de óbito com necrópsia" && x.Acolhimento.Servico.Descricao == "Clínica Médica").Count() });

                /// REMOÇÃO
                /// -------------------------------------------------------------------------------------------------------------
                registros.Add(new ProducaoResumoAtendimentoReadModel { Grupo = "REMOÇÃO", Item = "PEDIATRIA", Quantidade = atendimentos.Where(x => x.Checkout != null && x.Checkout.MotivoCheckout.Tipo.Descricao == "Em caso de Transferência" && x.Acolhimento.Servico.Descricao == "Pediatria").Count() });
                registros.Add(new ProducaoResumoAtendimentoReadModel { Grupo = "REMOÇÃO", Item = "CLINICA MÉDICA", Quantidade = atendimentos.Where(x => x.Checkout != null && x.Checkout.MotivoCheckout.Tipo.Descricao == "Em caso de Transferência" && x.Acolhimento.Servico.Descricao == "Clínica Médica").Count() });

                registros = (from p in registros
                             group p by p.Grupo into a
                             select new ProducaoResumoAtendimentoReadModel
                             {
                                 Grupo = a.Key.ToString(),
                                 ListaProducaoResumo = a.ToList()
                             }).ToList();
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return ListResult.GetListResult(registros, new string[] { "Grupo", "Item", "Quantidade", "ListaProducaoResumo" });
        }

        public ListResult ListarSPAMunicipioBairroFaixaEtaria(DateTime inicio, DateTime fim, string municipio)
        {
            try
            {
                var pivoted = new List<ListaSPAMunicipioBairroFaixaEtariaReadModel>();

                fim = fim.AddDays(1);

                var result = repository.Atendimento.Include("Acolhimento.Paciente.Endereco")
                       .Where(ac => ac.Acolhimento.Paciente.Endereco != null
                       && (ac.Acolhimento.Atendimento.FirstOrDefault().InicioAtendimento >= inicio && ac.Acolhimento.Atendimento.FirstOrDefault().InicioAtendimento <= fim)
                       && ac.Acolhimento.Paciente.Endereco.Cidade != null && ac.Acolhimento.Paciente.Endereco.Bairro != null
                       && ac.Acolhimento.Paciente.Endereco.Cidade == municipio ///.ToUpper().Contains(municipio)
                       ).ToList();

                var resultBairro = (from r in result.GroupBy(x => x.Acolhimento.Paciente.Endereco.Bairro)
                                    select new ListaSPAMunicipioBairroFaixaEtariaReadModel()
                                    {
                                        Bairro = r.Key.ToString(),
                                        Atendimentos = ViewModelUtils.ToListViewModel<HealthCare.Domain.Entities.DbClasses.Atendimento.Atendimento, AtendimentoReadModel>(result.Where(a => a.Acolhimento.Paciente.Endereco.Bairro == r.Key.ToString()).ToList())
                                    });

                foreach (var item in resultBairro)
                {
                    pivoted.Add(
                          new ListaSPAMunicipioBairroFaixaEtariaReadModel()
                          {
                              Bairro = item.Bairro,
                              Total = item.Atendimentos.Count(),
                              Municipio = municipio.ToUpper(),

                              MenorQue1Ano = result.Where(a => (a.Acolhimento.Paciente.Endereco.Bairro.Trim().Equals(item.Bairro.Trim()))
                              && (a.Acolhimento.Paciente.IdadeEmAnos < 1)).Count(),

                              UmaQuatro = result.Where(a => (a.Acolhimento.Paciente.Endereco.Bairro.Trim().Equals(item.Bairro.Trim()))
                              && (a.Acolhimento.Paciente.IdadeEmAnos >= 1) && (a.Acolhimento.Paciente.IdadeEmAnos <= 4)).Count(),

                              CincoaNove = result.Where(a => (a.Acolhimento.Paciente.Endereco.Bairro.Trim().Equals(item.Bairro.Trim()))
                              && (a.Acolhimento.Paciente.IdadeEmAnos >= 5) && (a.Acolhimento.Paciente.IdadeEmAnos <= 9)).Count(),

                              DezaCatorze = result.Where(a => (a.Acolhimento.Paciente.Endereco.Bairro.Trim().Equals(item.Bairro.Trim()))
                              && (a.Acolhimento.Paciente.IdadeEmAnos >= 10) && (a.Acolhimento.Paciente.IdadeEmAnos <= 14)).Count(),

                              QuinzeaDezenove = result.Where(a => (a.Acolhimento.Paciente.Endereco.Bairro.Trim().Equals(item.Bairro.Trim()))
                              && (a.Acolhimento.Paciente.IdadeEmAnos >= 15) && (a.Acolhimento.Paciente.IdadeEmAnos <= 19)).Count(),

                              Vinte = result.Where(a => (a.Acolhimento.Paciente.Endereco.Bairro.Trim().Equals(item.Bairro.Trim()))
                              && (a.Acolhimento.Paciente.IdadeEmAnos >= 20) && (a.Acolhimento.Paciente.IdadeEmAnos <= 29)).Count(),

                              Trinta = result.Where(a => (a.Acolhimento.Paciente.Endereco.Bairro.Trim().Equals(item.Bairro.Trim()))
                              && (a.Acolhimento.Paciente.IdadeEmAnos >= 30) && (a.Acolhimento.Paciente.IdadeEmAnos <= 39)).Count(),

                              Quarenta = result.Where(a => (a.Acolhimento.Paciente.Endereco.Bairro.Trim().Equals(item.Bairro.Trim()))
                              && (a.Acolhimento.Paciente.IdadeEmAnos >= 40) && (a.Acolhimento.Paciente.IdadeEmAnos <= 49)).Count(),

                              Cinquenta = result.Where(a => (a.Acolhimento.Paciente.Endereco.Bairro.Trim().Equals(item.Bairro.Trim()))
                              && (a.Acolhimento.Paciente.IdadeEmAnos >= 50) && (a.Acolhimento.Paciente.IdadeEmAnos <= 59)).Count(),

                              Sessenta = result.Where(a => (a.Acolhimento.Paciente.Endereco.Bairro.Trim().Equals(item.Bairro.Trim()))
                              && (a.Acolhimento.Paciente.IdadeEmAnos >= 60) && (a.Acolhimento.Paciente.IdadeEmAnos <= 69)).Count(),

                              Setenta = result.Where(a => (a.Acolhimento.Paciente.Endereco.Bairro.Trim().Equals(item.Bairro.Trim()))
                              && (a.Acolhimento.Paciente.IdadeEmAnos >= 70) && (a.Acolhimento.Paciente.IdadeEmAnos <= 79)).Count(),

                              MaiorQue80 = result.Where(a => (a.Acolhimento.Paciente.Endereco.Bairro.Trim().Equals(item.Bairro.Trim()))
                              && (a.Acolhimento.Paciente.IdadeEmAnos >= 80)).Count(),
                          });

                }


                return ListResult.GetListResult(pivoted,
                    new string[] {
                    "Municipio",
                    "Bairro",
                    "Total",
                    "MenorQue1Ano",
                    "UmaQuatro",
                    "CincoaNove",
                    "DezaCatorze",
                    "QuinzeaDezenove",
                    "Vinte",
                    "Trinta",
                    "Quarenta",
                    "Cinquenta",
                    "Sessenta",
                    "Setenta",
                    "MaiorQue80"
                    });
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private string CalculaTempo(DateTime dataInicial)
        {
            string tempo = "";
            TimeSpan calculo = DateTime.Now.Subtract(dataInicial);
            if (calculo.Days > 0)
                tempo = string.Format("{0}d:", calculo.Days.ToString());

            tempo = tempo + string.Format("{0}h:", calculo.Hours.ToString());
            tempo = tempo + string.Format("{0}m", calculo.Minutes.ToString());

            return tempo;
        }

        public ListResult GetProcedimentosByAtendimentoDataServico(DateTime inicio, DateTime fim, Guid servico)
        {
            try
            {

                var procedimentos = (from a in repository.Atendimento.Include("Acolhimento.Servico")
                                                 .Where(x => x.InicioAtendimento >= inicio && x.InicioAtendimento <= fim && x.Acolhimento.Servico.Id == servico)
                                     from c in repository.AtendimentoProcedimento
                                     where a.Id == c.AtendimentoId
                                     select new ProcedimentoAtendimentoReadModel()
                                     {
                                         Id = c.Id,
                                         CO_PROCEDIMENTO = c.CodigoProcedimento,
                                         NO_PROCEDIMENTO = c.NomeProcedimento,
                                         DataAtendimento = a.InicioAtendimento,
                                     }).ToList();


                var procGroupProcedimento = procedimentos.GroupBy(g => g.CO_PROCEDIMENTO).Distinct().ToList();

                var procGroupData = procedimentos.GroupBy(a => a.DataAtendimento).Select(grp => new ProcedimentoAtendimentoReadModel()
                {
                    DataAtendimento = grp.Key,
                    Procedimentos = grp.ToList(),
                    QuantidadeTotal = grp.Count()

                }).ToList();

                foreach (ProcedimentoAtendimentoReadModel item in procGroupData)
                {
                    foreach (ProcedimentoAtendimentoReadModel item2 in item.Procedimentos)
                    {
                        item2.QuantidadeItem = item.Procedimentos.Where(a => (a.CO_PROCEDIMENTO.Equals(item2.CO_PROCEDIMENTO) && (a.DataAtendimento.Equals(item2.DataAtendimento)))).Count();
                    }
                }

                return ListResult.GetListResult(procGroupData, new string[] { "DataAtendimento", "Procedimentos", "QuantidadeTotal", "QuantidadeItem" });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int ChecaTipoCheckout(Checkout c, string tipoEsperado)
        {
            if (c != null)
            {
                if (c.MotivoCheckout.Tipo.Descricao == tipoEsperado)
                    return 1;
                else
                    return 0;
            }
            else
                return 0;

        }

        public ListResult ListarProntoAtendimentoTempoPermanencia(DateTime inicio, DateTime fim, Guid UnidadeId)
        {
            var pivoted = new List<ListaProntoAtendimentoPorTempoPermanenciaReadModel>();
            fim = fim.AddDays(1);

            var listaAtendimentos = repository.Atendimento.Include("Acolhimento").Include("Checkout.MotivoCheckout.Tipo").Include("Internacoes")
               .Where(x => (x.FimAtendimento >= inicio && x.FimAtendimento <= fim) && (x.Profissional.Unidade.Id.Equals(UnidadeId))).OrderBy(x => x.InicioAtendimento).ToList();

            //var listaObitos = repository.Atendimento.Include("Acolhimento").Include("Checkout.MotivoCheckout.Tipo").Include("Internacoes")
            //   .Where(x => (x.FimAtendimento >= inicio && x.FimAtendimento <= fim) && (x.Profissional.Unidade.Id.Equals(UnidadeId))).OrderBy(x => x.InicioAtendimento).ToList();

            //Total com Acolhimento e atendimento Finalizado
            if (listaAtendimentos.Count > 0)
            {

                #region comCheckout
                var totalMenor01 = listaAtendimentos.Where(x => (x.FimAtendimento != null)
                && (x.FimAtendimento.Value.Subtract(x.InicioAtendimento).TotalHours <= 1)).ToList();

                var totalMenor02 = listaAtendimentos.Where(x => (x.FimAtendimento != null)
                && (x.FimAtendimento.Value.Subtract(x.InicioAtendimento).TotalHours <= 2) && (x.FimAtendimento.Value.Subtract(x.InicioAtendimento).TotalHours > 1)).ToList();

                var totalMenor03 = listaAtendimentos.Where(x => (x.FimAtendimento != null)
               && (x.FimAtendimento.Value.Subtract(x.InicioAtendimento).TotalHours <= 3) && (x.FimAtendimento.Value.Subtract(x.InicioAtendimento).TotalHours > 2)).ToList();

                var totalMenor04 = listaAtendimentos.Where(x => (x.FimAtendimento != null)
               && (x.FimAtendimento.Value.Subtract(x.InicioAtendimento).TotalHours <= 4) && (x.FimAtendimento.Value.Subtract(x.InicioAtendimento).TotalHours > 3)).ToList();

                var totalMenor05 = listaAtendimentos.Where(x => (x.FimAtendimento != null)
               && (x.FimAtendimento.Value.Subtract(x.InicioAtendimento).TotalHours <= 5) && (x.FimAtendimento.Value.Subtract(x.InicioAtendimento).TotalHours > 4)).ToList();

                var totalMenor06 = listaAtendimentos.Where(x => (x.FimAtendimento != null)
               && (x.FimAtendimento.Value.Subtract(x.InicioAtendimento).TotalHours <= 6) && (x.FimAtendimento.Value.Subtract(x.InicioAtendimento).TotalHours > 5)).ToList();

                var totalMenor07 = listaAtendimentos.Where(x => (x.FimAtendimento != null)
               && (x.FimAtendimento.Value.Subtract(x.InicioAtendimento).TotalHours <= 7) && (x.FimAtendimento.Value.Subtract(x.InicioAtendimento).TotalHours > 6)).ToList();

                var totalMenor08 = listaAtendimentos.Where(x => (x.FimAtendimento != null)
               && (x.FimAtendimento.Value.Subtract(x.InicioAtendimento).TotalHours <= 8) && (x.FimAtendimento.Value.Subtract(x.InicioAtendimento).TotalHours > 7)).ToList();

                var totalMaior24 = listaAtendimentos.Where(x => (x.FimAtendimento != null)
               && (x.FimAtendimento.Value.Subtract(x.InicioAtendimento).TotalHours > 8)).ToList();
                #endregion comCheckout

                #region semCheckout
                var totalMenor01SemCheckout = listaAtendimentos.Where(x => (x.FimAtendimento == null)
                && (DateTime.Now.Subtract(x.InicioAtendimento).TotalHours <= 1)).ToList();

                var totalMenor02SemCheckout = listaAtendimentos.Where(x => (x.FimAtendimento == null)
                && (DateTime.Now.Subtract(x.InicioAtendimento).TotalHours <= 2) && (DateTime.Now.Subtract(x.InicioAtendimento).TotalHours > 1)).ToList();

                var totalMenor03SemCheckout = listaAtendimentos.Where(x => (x.FimAtendimento == null)
               && (DateTime.Now.Subtract(x.InicioAtendimento).TotalHours <= 3) && (DateTime.Now.Subtract(x.InicioAtendimento).TotalHours > 2)).ToList();

                var totalMenor04SemCheckout = listaAtendimentos.Where(x => (x.FimAtendimento == null)
               && (DateTime.Now.Subtract(x.InicioAtendimento).TotalHours <= 4) && (DateTime.Now.Subtract(x.InicioAtendimento).TotalHours > 3)).ToList();

                var totalMenor05SemCheckout = listaAtendimentos.Where(x => (x.FimAtendimento == null)
               && (DateTime.Now.Subtract(x.InicioAtendimento).TotalHours <= 5) && (DateTime.Now.Subtract(x.InicioAtendimento).TotalHours > 4)).ToList();

                var totalMenor06SemCheckout = listaAtendimentos.Where(x => (x.FimAtendimento == null)
               && (DateTime.Now.Subtract(x.InicioAtendimento).TotalHours <= 6) && (DateTime.Now.Subtract(x.InicioAtendimento).TotalHours > 5)).ToList();

                var totalMenor07SemCheckout = listaAtendimentos.Where(x => (x.FimAtendimento == null)
               && (DateTime.Now.Subtract(x.InicioAtendimento).TotalHours <= 7) && (DateTime.Now.Subtract(x.InicioAtendimento).TotalHours > 6)).ToList();

                var totalMenor08SemCheckout = listaAtendimentos.Where(x => (x.FimAtendimento == null)
               && (DateTime.Now.Subtract(x.InicioAtendimento).TotalHours <= 8) && (DateTime.Now.Subtract(x.InicioAtendimento).TotalHours > 7)).ToList();

                var totalMaior24SemCheckout = listaAtendimentos.Where(x => (x.FimAtendimento == null)
               && (DateTime.Now.Subtract(x.InicioAtendimento).TotalHours > 8)).ToList();
                #endregion semCheckout

                int total = 0;
                int altas = 0;
                int obitos = 0;
                int internacoes = 0;
                int outros = 0;

                #region menor01
                //menor 01
                total = totalMenor01.Count() + totalMenor01SemCheckout.Count();
                altas = totalMenor01.Where(t => t.Checkout.MotivoCheckout.Tipo.Descricao.Contains("Alta")).Count();
                obitos = totalMenor01.Where(t => t.Checkout.MotivoCheckout.Tipo.Descricao.ToUpper().Contains("ÓBITO")).Count();
                internacoes = totalMenor01.Where(x => x.Internacoes.Count > 0).Count();
                outros = (totalMenor01.Where(t => t.Checkout == null).Count()
                    + (totalMenor01.Where(t => (!t.Checkout.MotivoCheckout.Tipo.Descricao.Contains("Alta")) && (!t.Checkout.MotivoCheckout.Tipo.Descricao.ToUpper().Contains("ÓBITO")) && (!t.Checkout.MotivoCheckout.Tipo.Descricao.ToUpper().Contains("ÓBITO")))).Count() + (totalMenor01SemCheckout.Count()));


                pivoted.Add(new ListaProntoAtendimentoPorTempoPermanenciaReadModel()
                {
                    DescricaoDasHoras = "00 A 01 HORAS",
                    Total = total,
                    Altas = altas,
                    Obitos = obitos,
                    Internacoes = internacoes,
                    Outros = outros
                });
                #endregion menor01

                #region menor02
                //menor 02
                total = 0;
                altas = 0;
                obitos = 0;
                internacoes = 0;
                outros = 0;

                total = totalMenor02.Count() + totalMenor02SemCheckout.Count();
                altas = totalMenor02.Where(t => t.Checkout.MotivoCheckout.Tipo.Descricao.Contains("Alta")).Count();
                obitos = totalMenor02.Where(t => t.Checkout.MotivoCheckout.Tipo.Descricao.ToUpper().Contains("ÓBITO")).Count();
                internacoes = totalMenor02.Where(x => x.Internacoes.Count > 0).Count();
                outros = (totalMenor02.Where(t => t.Checkout == null).Count()
                    + (totalMenor02.Where(t => (!t.Checkout.MotivoCheckout.Tipo.Descricao.Contains("Alta"))
                    && (!t.Checkout.MotivoCheckout.Tipo.Descricao.ToUpper().Contains("ÓBITO"))
                    && (!t.Checkout.MotivoCheckout.Tipo.Descricao.ToUpper().Contains("ÓBITO")))).Count() + (totalMenor02SemCheckout.Count()));


                pivoted.Add(new ListaProntoAtendimentoPorTempoPermanenciaReadModel()
                {
                    DescricaoDasHoras = "01 A 02 HORAS",
                    Total = total,
                    Altas = altas,
                    Obitos = obitos,
                    Internacoes = internacoes,
                    Outros = outros
                });
                #endregion menor02

                #region menor03
                //menor 03
                total = 0;
                altas = 0;
                obitos = 0;
                internacoes = 0;
                outros = 0;

                total = totalMenor03.Count() + totalMenor03SemCheckout.Count();
                altas = totalMenor03.Where(t => t.Checkout.MotivoCheckout.Tipo.Descricao.Contains("Alta")).Count();
                obitos = totalMenor03.Where(t => t.Checkout.MotivoCheckout.Tipo.Descricao.ToUpper().Contains("ÓBITO")).Count();
                internacoes = totalMenor03.Where(x => x.Internacoes.Count > 0).Count();
                outros = (totalMenor03.Where(t => t.Checkout == null).Count()
                    + (totalMenor03.Where(t => (!t.Checkout.MotivoCheckout.Tipo.Descricao.Contains("Alta"))
                    && (!t.Checkout.MotivoCheckout.Tipo.Descricao.ToUpper().Contains("ÓBITO"))
                    && (!t.Checkout.MotivoCheckout.Tipo.Descricao.ToUpper().Contains("ÓBITO")))).Count() + (totalMenor03SemCheckout.Count()));


                pivoted.Add(new ListaProntoAtendimentoPorTempoPermanenciaReadModel()
                {
                    DescricaoDasHoras = "02 A 03 HORAS",
                    Total = total,
                    Altas = altas,
                    Obitos = obitos,
                    Internacoes = internacoes,
                    Outros = outros
                });
                #endregion menor03

                #region menor04
                //menor 04
                total = 0;
                altas = 0;
                obitos = 0;
                internacoes = 0;
                outros = 0;

                total = totalMenor04.Count() + totalMenor04SemCheckout.Count();
                altas = totalMenor04.Where(t => t.Checkout.MotivoCheckout.Tipo.Descricao.Contains("Alta")).Count();
                obitos = totalMenor04.Where(t => t.Checkout.MotivoCheckout.Tipo.Descricao.ToUpper().Contains("ÓBITO")).Count();
                internacoes = totalMenor04.Where(x => x.Internacoes.Count > 0).Count();
                outros = (totalMenor04.Where(t => t.Checkout == null).Count()
                    + (totalMenor04.Where(t => (!t.Checkout.MotivoCheckout.Tipo.Descricao.Contains("Alta"))
                    && (!t.Checkout.MotivoCheckout.Tipo.Descricao.ToUpper().Contains("ÓBITO"))
                    && (!t.Checkout.MotivoCheckout.Tipo.Descricao.ToUpper().Contains("ÓBITO")))).Count() + (totalMenor04SemCheckout.Count()));


                pivoted.Add(new ListaProntoAtendimentoPorTempoPermanenciaReadModel()
                {
                    DescricaoDasHoras = "03 A 04 HORAS",
                    Total = total,
                    Altas = altas,
                    Obitos = obitos,
                    Internacoes = internacoes,
                    Outros = outros
                });
                #endregion menor04

                #region menor05
                //menor 05
                total = 0;
                altas = 0;
                obitos = 0;
                internacoes = 0;
                outros = 0;

                total = totalMenor05.Count() + totalMenor05SemCheckout.Count();
                altas = totalMenor05.Where(t => t.Checkout.MotivoCheckout.Tipo.Descricao.Contains("Alta")).Count();
                obitos = totalMenor05.Where(t => t.Checkout.MotivoCheckout.Tipo.Descricao.ToUpper().Contains("ÓBITO")).Count();
                internacoes = totalMenor05.Where(x => x.Internacoes.Count > 0).Count();
                outros = (totalMenor05.Where(t => t.Checkout == null).Count()
                    + (totalMenor05.Where(t => (!t.Checkout.MotivoCheckout.Tipo.Descricao.Contains("Alta"))
                    && (!t.Checkout.MotivoCheckout.Tipo.Descricao.ToUpper().Contains("ÓBITO"))
                    && (!t.Checkout.MotivoCheckout.Tipo.Descricao.ToUpper().Contains("ÓBITO")))).Count() + (totalMenor05SemCheckout.Count()));


                pivoted.Add(new ListaProntoAtendimentoPorTempoPermanenciaReadModel()
                {
                    DescricaoDasHoras = "04 A 05 HORAS",
                    Total = total,
                    Altas = altas,
                    Obitos = obitos,
                    Internacoes = internacoes,
                    Outros = outros
                });
                #endregion menor05

                #region menor06
                //menor 06
                total = 0;
                altas = 0;
                obitos = 0;
                internacoes = 0;
                outros = 0;

                total = totalMenor06.Count() + totalMenor06SemCheckout.Count();
                altas = totalMenor06.Where(t => t.Checkout.MotivoCheckout.Tipo.Descricao.Contains("Alta")).Count();
                obitos = totalMenor06.Where(t => t.Checkout.MotivoCheckout.Tipo.Descricao.ToUpper().Contains("ÓBITO")).Count();
                internacoes = totalMenor06.Where(x => x.Internacoes.Count > 0).Count();
                outros = (totalMenor06.Where(t => t.Checkout == null).Count()
                    + (totalMenor06.Where(t => (!t.Checkout.MotivoCheckout.Tipo.Descricao.Contains("Alta"))
                    && (!t.Checkout.MotivoCheckout.Tipo.Descricao.ToUpper().Contains("ÓBITO"))
                    && (!t.Checkout.MotivoCheckout.Tipo.Descricao.ToUpper().Contains("ÓBITO")))).Count() + (totalMenor06SemCheckout.Count()));


                pivoted.Add(new ListaProntoAtendimentoPorTempoPermanenciaReadModel()
                {
                    DescricaoDasHoras = "05 A 06 HORAS",
                    Total = total,
                    Altas = altas,
                    Obitos = obitos,
                    Internacoes = internacoes,
                    Outros = outros
                });
                #endregion menor06

                #region menor07
                //menor 07
                total = 0;
                altas = 0;
                obitos = 0;
                internacoes = 0;
                outros = 0;

                total = totalMenor07.Count() + totalMenor07SemCheckout.Count();
                altas = totalMenor07.Where(t => t.Checkout.MotivoCheckout.Tipo.Descricao.Contains("Alta")).Count();
                obitos = totalMenor07.Where(t => t.Checkout.MotivoCheckout.Tipo.Descricao.ToUpper().Contains("ÓBITO")).Count();
                internacoes = totalMenor07.Where(x => x.Internacoes.Count > 0).Count();
                outros = (totalMenor07.Where(t => t.Checkout == null).Count()
                    + (totalMenor07.Where(t => (!t.Checkout.MotivoCheckout.Tipo.Descricao.Contains("Alta"))
                    && (!t.Checkout.MotivoCheckout.Tipo.Descricao.ToUpper().Contains("ÓBITO"))
                    && (!t.Checkout.MotivoCheckout.Tipo.Descricao.ToUpper().Contains("ÓBITO")))).Count() + (totalMenor07SemCheckout.Count()));


                pivoted.Add(new ListaProntoAtendimentoPorTempoPermanenciaReadModel()
                {
                    DescricaoDasHoras = "06 A 07 HORAS",
                    Total = total,
                    Altas = altas,
                    Obitos = obitos,
                    Internacoes = internacoes,
                    Outros = outros
                });
                #endregion menor07

                #region menor08
                //menor 08
                total = 0;
                altas = 0;
                obitos = 0;
                internacoes = 0;
                outros = 0;

                total = totalMenor08.Count() + totalMenor08SemCheckout.Count();
                altas = totalMenor08.Where(t => t.Checkout.MotivoCheckout.Tipo.Descricao.Contains("Alta")).Count();
                obitos = totalMenor08.Where(t => t.Checkout.MotivoCheckout.Tipo.Descricao.ToUpper().Contains("ÓBITO")).Count();
                internacoes = totalMenor08.Where(x => x.Internacoes.Count > 0).Count();
                outros = (totalMenor08.Where(t => t.Checkout == null).Count()
                    + (totalMenor08.Where(t => (!t.Checkout.MotivoCheckout.Tipo.Descricao.Contains("Alta"))
                    && (!t.Checkout.MotivoCheckout.Tipo.Descricao.ToUpper().Contains("ÓBITO"))
                    && (!t.Checkout.MotivoCheckout.Tipo.Descricao.ToUpper().Contains("ÓBITO")))).Count() + (totalMenor08SemCheckout.Count()));


                pivoted.Add(new ListaProntoAtendimentoPorTempoPermanenciaReadModel()
                {
                    DescricaoDasHoras = "07 A 08 HORAS",
                    Total = total,
                    Altas = altas,
                    Obitos = obitos,
                    Internacoes = internacoes,
                    Outros = outros
                });
                #endregion menor08

                #region maior24
                //maior 24
                total = 0;
                altas = 0;
                obitos = 0;
                internacoes = 0;
                outros = 0;

                total = totalMaior24.Count() + totalMaior24SemCheckout.Count();
                altas = totalMaior24.Where(t => t.Checkout.MotivoCheckout.Tipo.Descricao.Contains("Alta")).Count();
                obitos = totalMaior24.Where(t => t.Checkout.MotivoCheckout.Tipo.Descricao.ToUpper().Contains("ÓBITO")).Count();
                internacoes = totalMaior24.Where(x => x.Internacoes.Count > 0).Count();
                outros = (totalMaior24.Where(t => t.Checkout == null).Count()
                    + (totalMaior24.Where(t => (!t.Checkout.MotivoCheckout.Tipo.Descricao.Contains("Alta"))
                    && (!t.Checkout.MotivoCheckout.Tipo.Descricao.ToUpper().Contains("ÓBITO"))
                    && (!t.Checkout.MotivoCheckout.Tipo.Descricao.ToUpper().Contains("ÓBITO")))).Count() + (totalMaior24SemCheckout.Count()));


                pivoted.Add(new ListaProntoAtendimentoPorTempoPermanenciaReadModel()
                {
                    DescricaoDasHoras = "MAIS QUE 24 HORAS",
                    Total = total,
                    Altas = altas,
                    Obitos = obitos,
                    Internacoes = internacoes,
                    Outros = outros
                });
                #endregion maior24
            }


            return ListResult.GetListResult(pivoted,
           new string[] {
                   "DescricaoDasHoras",
                    "Total",
                    "Altas",
                    "Obitos",
                    "Internacoes",
                    "Outros",
           });
        }

        public ListResult ListarRegistroHoraUrgenciaClinica(DateTime inicio, DateTime fim, Guid UnidadeId)
        {
            var pivoted = new List<RegistroHoraUrgenciaClinica>();
            var listapivoted = new List<RegistroHoraUrgenciaClinica>();
            var listaFinal = new List<RegistroHoraUrgenciaClinica>();
            var resultado = new List<RegistroHoraUrgenciaClinica>();

            fim = fim.AddDays(1);
            var lista = repository.Acolhimento.Include("Servico").Where(d => (d.DataHora >= inicio)
            && (d.DataHora <= fim) && (d.Profissional.Unidade.Id.Equals(UnidadeId))).ToList();

            var listaServicos = repository.Servico.ToList();

            foreach (var item in lista)
            {
                if ((item != null) && (item.Servico != null))
                {
                    #region SwitchCase
                    switch (item.DataHora.Hour)
                    {
                        case 0:
                            {
                                pivoted.Add(new RegistroHoraUrgenciaClinica()
                                {
                                    DescricaoClinica = item.Servico.Descricao,
                                    Data = new DateTime(item.DataHora.Year, item
                 .DataHora.Month, item.DataHora.Day),
                                    Hora_00 = 24

                                });
                                break;
                            };
                        case 1:
                            {
                                pivoted.Add(new RegistroHoraUrgenciaClinica()
                                {
                                    DescricaoClinica = item.Servico.Descricao,
                                    Data = new DateTime(item.DataHora.Year, item
                   .DataHora.Month, item.DataHora.Day),
                                    Hora_01 = item.DataHora.Hour

                                });
                            };
                            break;
                        case 2:
                            {
                                pivoted.Add(new RegistroHoraUrgenciaClinica()
                                {
                                    DescricaoClinica = item.Servico.Descricao,
                                    Data = new DateTime(item.DataHora.Year, item
                   .DataHora.Month, item.DataHora.Day),
                                    Hora_02 = item.DataHora.Hour

                                });
                            };
                            break;
                        case 3:
                            {
                                pivoted.Add(new RegistroHoraUrgenciaClinica()
                                {
                                    DescricaoClinica = item.Servico.Descricao,
                                    Data = new DateTime(item.DataHora.Year, item
                   .DataHora.Month, item.DataHora.Day),
                                    Hora_03 = item.DataHora.Hour

                                });
                            };
                            break;
                        case 4:
                            {
                                pivoted.Add(new RegistroHoraUrgenciaClinica()
                                {
                                    DescricaoClinica = item.Servico.Descricao,
                                    Data = new DateTime(item.DataHora.Year, item
                   .DataHora.Month, item.DataHora.Day),
                                    Hora_04 = item.DataHora.Hour

                                });
                            };
                            break;
                        case 5:
                            {
                                pivoted.Add(new RegistroHoraUrgenciaClinica()
                                {
                                    DescricaoClinica = item.Servico.Descricao,
                                    Data = new DateTime(item.DataHora.Year, item
                   .DataHora.Month, item.DataHora.Day),
                                    Hora_05 = item.DataHora.Hour

                                });
                            };
                            break;
                        case 6:
                            {
                                pivoted.Add(new RegistroHoraUrgenciaClinica()
                                {
                                    DescricaoClinica = item.Servico.Descricao,
                                    Data = new DateTime(item.DataHora.Year, item
                   .DataHora.Month, item.DataHora.Day),
                                    Hora_06 = item.DataHora.Hour

                                });
                            };
                            break;
                        case 7:
                            {
                                pivoted.Add(new RegistroHoraUrgenciaClinica()
                                {
                                    DescricaoClinica = item.Servico.Descricao,
                                    Data = new DateTime(item.DataHora.Year, item
                   .DataHora.Month, item.DataHora.Day),
                                    Hora_07 = item.DataHora.Hour

                                });
                            };
                            break;
                        case 8:
                            {
                                pivoted.Add(new RegistroHoraUrgenciaClinica()
                                {
                                    DescricaoClinica = item.Servico.Descricao,
                                    Data = new DateTime(item.DataHora.Year, item
                   .DataHora.Month, item.DataHora.Day),
                                    Hora_08 = item.DataHora.Hour

                                });
                            };
                            break;
                        case 9:
                            {
                                pivoted.Add(new RegistroHoraUrgenciaClinica()
                                {
                                    DescricaoClinica = item.Servico.Descricao,
                                    Data = new DateTime(item.DataHora.Year, item
                   .DataHora.Month, item.DataHora.Day),
                                    Hora_09 = item.DataHora.Hour

                                });
                            };
                            break;
                        case 10:
                            {
                                pivoted.Add(new RegistroHoraUrgenciaClinica()
                                {
                                    DescricaoClinica = item.Servico.Descricao,
                                    Data = new DateTime(item.DataHora.Year, item
                   .DataHora.Month, item.DataHora.Day),
                                    Hora_10 = item.DataHora.Hour

                                });
                            };
                            break;
                        case 11:
                            {
                                pivoted.Add(new RegistroHoraUrgenciaClinica()
                                {
                                    DescricaoClinica = item.Servico.Descricao,
                                    Data = new DateTime(item.DataHora.Year, item
                   .DataHora.Month, item.DataHora.Day),
                                    Hora_11 = item.DataHora.Hour

                                });
                            };
                            break;
                        case 12:
                            {
                                pivoted.Add(new RegistroHoraUrgenciaClinica()
                                {
                                    DescricaoClinica = item.Servico.Descricao,
                                    Data = new DateTime(item.DataHora.Year, item
                   .DataHora.Month, item.DataHora.Day),
                                    Hora_12 = item.DataHora.Hour

                                });
                            };
                            break;
                        case 13:
                            {
                                pivoted.Add(new RegistroHoraUrgenciaClinica()
                                {
                                    DescricaoClinica = item.Servico.Descricao,
                                    Data = new DateTime(item.DataHora.Year, item
                   .DataHora.Month, item.DataHora.Day),
                                    Hora_13 = item.DataHora.Hour

                                });
                            };
                            break;
                        case 14:
                            {
                                pivoted.Add(new RegistroHoraUrgenciaClinica()
                                {
                                    DescricaoClinica = item.Servico.Descricao,
                                    Data = new DateTime(item.DataHora.Year, item
                   .DataHora.Month, item.DataHora.Day),
                                    Hora_14 = item.DataHora.Hour

                                });
                            };
                            break;
                        case 15:
                            {
                                pivoted.Add(new RegistroHoraUrgenciaClinica()
                                {
                                    DescricaoClinica = item.Servico.Descricao,
                                    Data = new DateTime(item.DataHora.Year, item
                   .DataHora.Month, item.DataHora.Day),
                                    Hora_15 = item.DataHora.Hour

                                });
                            };
                            break;
                        case 16:
                            {
                                pivoted.Add(new RegistroHoraUrgenciaClinica()
                                {
                                    DescricaoClinica = item.Servico.Descricao,
                                    Data = new DateTime(item.DataHora.Year, item
                   .DataHora.Month, item.DataHora.Day),
                                    Hora_16 = item.DataHora.Hour

                                });
                            };
                            break;
                        case 17:
                            {
                                pivoted.Add(new RegistroHoraUrgenciaClinica()
                                {
                                    DescricaoClinica = item.Servico.Descricao,
                                    Data = new DateTime(item.DataHora.Year, item
                   .DataHora.Month, item.DataHora.Day),
                                    Hora_17 = item.DataHora.Hour

                                });
                            };
                            break;
                        case 18:
                            {
                                pivoted.Add(new RegistroHoraUrgenciaClinica()
                                {
                                    DescricaoClinica = item.Servico.Descricao,
                                    Data = new DateTime(item.DataHora.Year, item
                   .DataHora.Month, item.DataHora.Day),
                                    Hora_18 = item.DataHora.Hour

                                });
                            };
                            break;
                        case 19:
                            {
                                pivoted.Add(new RegistroHoraUrgenciaClinica()
                                {
                                    DescricaoClinica = item.Servico.Descricao,
                                    Data = new DateTime(item.DataHora.Year, item
                   .DataHora.Month, item.DataHora.Day),
                                    Hora_19 = item.DataHora.Hour

                                });
                            };
                            break;
                        case 20:
                            {
                                pivoted.Add(new RegistroHoraUrgenciaClinica()
                                {
                                    DescricaoClinica = item.Servico.Descricao,
                                    Data = new DateTime(item.DataHora.Year, item
                   .DataHora.Month, item.DataHora.Day),
                                    Hora_20 = item.DataHora.Hour

                                });
                            };
                            break;
                        case 21:
                            {
                                pivoted.Add(new RegistroHoraUrgenciaClinica()
                                {
                                    DescricaoClinica = item.Servico.Descricao,
                                    Data = new DateTime(item.DataHora.Year, item
                   .DataHora.Month, item.DataHora.Day),
                                    Hora_21 = item.DataHora.Hour

                                });
                            };
                            break;
                        case 22:
                            {
                                pivoted.Add(new RegistroHoraUrgenciaClinica()
                                {
                                    DescricaoClinica = item.Servico.Descricao,
                                    Data = new DateTime(item.DataHora.Year, item
                   .DataHora.Month, item.DataHora.Day),
                                    Hora_22 = item.DataHora.Hour

                                });
                            };
                            break;
                        case 23:
                            {
                                pivoted.Add(new RegistroHoraUrgenciaClinica()
                                {
                                    DescricaoClinica = item.Servico.Descricao,
                                    Data = new DateTime(item.DataHora.Year, item
                   .DataHora.Month, item.DataHora.Day),
                                    Hora_23 = item.DataHora.Hour

                                });
                            };
                            break;

                    }
                    #endregion
                }

            }

            listapivoted = pivoted.GroupBy(h => h.Data).Select(grp => new RegistroHoraUrgenciaClinica()
            {
                Data = grp.Key,
                ListaRegistros = grp.Distinct().ToList()
            }).Distinct().ToList();


            foreach (var item in listapivoted)
            {

                foreach (var registros in item.ListaRegistros.Distinct())
                {
                    int total_0 = item.ListaRegistros.Where(p => p.Hora_00 > 0).Where(p => p.DescricaoClinica.Equals(registros.DescricaoClinica)).Where(p => p.Data == registros.Data).Count();
                    int total_1 = item.ListaRegistros.Where(p => p.Hora_01 > 0).Where(p => p.DescricaoClinica.Equals(registros.DescricaoClinica)).Where(p => p.Data == registros.Data).Count();
                    int total_2 = item.ListaRegistros.Where(p => p.Hora_02 > 0).Where(p => p.DescricaoClinica.Equals(registros.DescricaoClinica)).Where(p => p.Data == registros.Data).Count();
                    int total_3 = item.ListaRegistros.Where(p => p.Hora_03 > 0).Where(p => p.DescricaoClinica.Equals(registros.DescricaoClinica)).Where(p => p.Data == registros.Data).Count();
                    int total_4 = item.ListaRegistros.Where(p => p.Hora_04 > 0).Where(p => p.DescricaoClinica.Equals(registros.DescricaoClinica)).Where(p => p.Data == registros.Data).Count();
                    int total_5 = item.ListaRegistros.Where(p => p.Hora_05 > 0).Where(p => p.DescricaoClinica.Equals(registros.DescricaoClinica)).Where(p => p.Data == registros.Data).Count();
                    int total_6 = item.ListaRegistros.Where(p => p.Hora_06 > 0).Where(p => p.DescricaoClinica.Equals(registros.DescricaoClinica)).Where(p => p.Data == registros.Data).Count();
                    int total_7 = item.ListaRegistros.Where(p => p.Hora_07 > 0).Where(p => p.DescricaoClinica.Equals(registros.DescricaoClinica)).Where(p => p.Data == registros.Data).Count();
                    int total_8 = item.ListaRegistros.Where(p => p.Hora_08 > 0).Where(p => p.DescricaoClinica.Equals(registros.DescricaoClinica)).Where(p => p.Data == registros.Data).Count();
                    int total_9 = item.ListaRegistros.Where(p => p.Hora_09 > 0).Where(p => p.DescricaoClinica.Equals(registros.DescricaoClinica)).Where(p => p.Data == registros.Data).Count();
                    int total_10 = item.ListaRegistros.Where(p => p.Hora_10 > 0).Where(p => p.DescricaoClinica.Equals(registros.DescricaoClinica)).Where(p => p.Data == registros.Data).Count();
                    int total_11 = item.ListaRegistros.Where(p => p.Hora_11 > 0).Where(p => p.DescricaoClinica.Equals(registros.DescricaoClinica)).Where(p => p.Data == registros.Data).Count();
                    int total_12 = item.ListaRegistros.Where(p => p.Hora_12 > 0).Where(p => p.DescricaoClinica.Equals(registros.DescricaoClinica)).Where(p => p.Data == registros.Data).Count();
                    int total_13 = item.ListaRegistros.Where(p => p.Hora_13 > 0).Where(p => p.DescricaoClinica.Equals(registros.DescricaoClinica)).Where(p => p.Data == registros.Data).Count();
                    int total_14 = item.ListaRegistros.Where(p => p.Hora_14 > 0).Where(p => p.DescricaoClinica.Equals(registros.DescricaoClinica)).Where(p => p.Data == registros.Data).Count();
                    int total_15 = item.ListaRegistros.Where(p => p.Hora_15 > 0).Where(p => p.DescricaoClinica.Equals(registros.DescricaoClinica)).Where(p => p.Data == registros.Data).Count();
                    int total_16 = item.ListaRegistros.Where(p => p.Hora_16 > 0).Where(p => p.DescricaoClinica.Equals(registros.DescricaoClinica)).Where(p => p.Data == registros.Data).Count();
                    int total_17 = item.ListaRegistros.Where(p => p.Hora_17 > 0).Where(p => p.DescricaoClinica.Equals(registros.DescricaoClinica)).Where(p => p.Data == registros.Data).Count();
                    int total_18 = item.ListaRegistros.Where(p => p.Hora_18 > 0).Where(p => p.DescricaoClinica.Equals(registros.DescricaoClinica)).Where(p => p.Data == registros.Data).Count();
                    int total_19 = item.ListaRegistros.Where(p => p.Hora_19 > 0).Where(p => p.DescricaoClinica.Equals(registros.DescricaoClinica)).Where(p => p.Data == registros.Data).Count();
                    int total_21 = item.ListaRegistros.Where(p => p.Hora_20 > 0).Where(p => p.DescricaoClinica.Equals(registros.DescricaoClinica)).Where(p => p.Data == registros.Data).Count();
                    int total_20 = item.ListaRegistros.Where(p => p.Hora_21 > 0).Where(p => p.DescricaoClinica.Equals(registros.DescricaoClinica)).Where(p => p.Data == registros.Data).Count();
                    int total_22 = item.ListaRegistros.Where(p => p.Hora_22 > 0).Where(p => p.DescricaoClinica.Equals(registros.DescricaoClinica)).Where(p => p.Data == registros.Data).Count();
                    int total_23 = item.ListaRegistros.Where(p => p.Hora_23 > 0).Where(p => p.DescricaoClinica.Equals(registros.DescricaoClinica)).Where(p => p.Data == registros.Data).Count();

                    listaFinal.Add(new RegistroHoraUrgenciaClinica()
                    {
                        Data = item.Data,
                        DescricaoClinica = registros.DescricaoClinica,
                        Hora_00 = total_0,
                        Hora_01 = total_1,
                        Hora_02 = total_2,
                        Hora_03 = total_3,
                        Hora_04 = total_4,
                        Hora_05 = total_5,
                        Hora_06 = total_6,
                        Hora_07 = total_7,
                        Hora_08 = total_8,
                        Hora_09 = total_9,
                        Hora_10 = total_10,
                        Hora_11 = total_11,
                        Hora_12 = total_12,
                        Hora_13 = total_13,
                        Hora_14 = total_14,
                        Hora_15 = total_15,
                        Hora_16 = total_16,
                        Hora_17 = total_17,
                        Hora_18 = total_18,
                        Hora_19 = total_19,
                        Hora_20 = total_20,
                        Hora_21 = total_21,
                        Hora_22 = total_22,
                        Hora_23 = total_23,
                        TotalClinicaDia = (total_0 + total_1 + total_2 + total_3 + total_4 + total_5 + total_6 + total_7 + total_8 + total_9 + total_10 + total_11 + total_12 + total_13 + total_14 + total_15 + total_16 + total_17 + total_18 + total_19 + total_20 + total_21 + total_22 + total_23)
                    });

                }
            }

            listaFinal = listaFinal.GroupBy(p => p.Data).Select(grp => new RegistroHoraUrgenciaClinica()
            {
                Data = grp.Key,
                ListaRegistros = grp.Distinct().ToList(),
            }).OrderBy(d => d.Data).Distinct().ToList();



            foreach (var item in listaFinal)
            {
                RegistroHoraUrgenciaClinica registro = new RegistroHoraUrgenciaClinica();
                registro.Data = item.Data;
                registro.DescricaoClinica = item.DescricaoClinica;
                registro.ListaRegistros = item.ListaRegistros.GroupBy(grp => grp.DescricaoClinica).Select(grp => grp.First()).ToList();
                resultado.Add(registro);
            }

            #region InicializacaoVariaveis
            DateTime? dataTemp = null;
            int somatorio = 0;

            int sumHora00 = 0;
            int sumHora01 = 0;
            int sumHora02 = 0;
            int sumHora03 = 0;
            int sumHora04 = 0;
            int sumHora05 = 0;
            int sumHora06 = 0;
            int sumHora07 = 0;
            int sumHora08 = 0;
            int sumHora09 = 0;
            int sumHora10 = 0;
            int sumHora11 = 0;
            int sumHora12 = 0;
            int sumHora13 = 0;
            int sumHora14 = 0;
            int sumHora15 = 0;
            int sumHora16 = 0;
            int sumHora17 = 0;
            int sumHora18 = 0;
            int sumHora19 = 0;
            int sumHora20 = 0;
            int sumHora21 = 0;
            int sumHora22 = 0;
            int sumHora23 = 0;

            #endregion

            foreach (var item in resultado)
            {
                if (dataTemp == null)
                {
                    dataTemp = item.Data;
                }
                if (dataTemp.Equals(item.Data))
                {
                    somatorio = somatorio + (item.ListaRegistros.Sum(p => p.TotalClinicaDia));
                    sumHora00 = sumHora00 + (item.ListaRegistros.Sum(p => p.Hora_00));
                    sumHora01 = sumHora01 + (item.ListaRegistros.Sum(p => p.Hora_01));
                    sumHora01 = sumHora02 + (item.ListaRegistros.Sum(p => p.Hora_02));
                    sumHora03 = sumHora03 + (item.ListaRegistros.Sum(p => p.Hora_03));
                    sumHora04 = sumHora04 + (item.ListaRegistros.Sum(p => p.Hora_04));
                    sumHora05 = sumHora05 + (item.ListaRegistros.Sum(p => p.Hora_05));
                    sumHora06 = sumHora06 + (item.ListaRegistros.Sum(p => p.Hora_06));
                    sumHora07 = sumHora07 + (item.ListaRegistros.Sum(p => p.Hora_07));
                    sumHora08 = sumHora08 + (item.ListaRegistros.Sum(p => p.Hora_08));
                    sumHora09 = sumHora09 + (item.ListaRegistros.Sum(p => p.Hora_09));
                    sumHora10 = sumHora10 + (item.ListaRegistros.Sum(p => p.Hora_10));
                    sumHora11 = sumHora11 + (item.ListaRegistros.Sum(p => p.Hora_11));
                    sumHora12 = sumHora12 + (item.ListaRegistros.Sum(p => p.Hora_12));
                    sumHora13 = sumHora13 + (item.ListaRegistros.Sum(p => p.Hora_13));
                    sumHora14 = sumHora14 + (item.ListaRegistros.Sum(p => p.Hora_14));
                    sumHora15 = sumHora15 + (item.ListaRegistros.Sum(p => p.Hora_15));
                    sumHora16 = sumHora16 + (item.ListaRegistros.Sum(p => p.Hora_16));
                    sumHora17 = sumHora17 + (item.ListaRegistros.Sum(p => p.Hora_17));
                    sumHora18 = sumHora18 + (item.ListaRegistros.Sum(p => p.Hora_18));
                    sumHora19 = sumHora19 + (item.ListaRegistros.Sum(p => p.Hora_19));
                    sumHora20 = sumHora20 + (item.ListaRegistros.Sum(p => p.Hora_20));
                    sumHora21 = sumHora21 + (item.ListaRegistros.Sum(p => p.Hora_21));
                    sumHora22 = sumHora22 + (item.ListaRegistros.Sum(p => p.Hora_22));
                    sumHora23 = sumHora23 + (item.ListaRegistros.Sum(p => p.Hora_23));
                }
                else
                {

                    dataTemp = item.Data;

                    somatorio = 0;
                    somatorio = somatorio + (item.ListaRegistros.Sum(p => p.TotalClinicaDia));

                    sumHora00 = 0;
                    sumHora01 = 0;
                    sumHora02 = 0;
                    sumHora03 = 0;
                    sumHora04 = 0;
                    sumHora05 = 0;
                    sumHora06 = 0;
                    sumHora07 = 0;
                    sumHora08 = 0;
                    sumHora09 = 0;
                    sumHora10 = 0;
                    sumHora11 = 0;
                    sumHora12 = 0;
                    sumHora13 = 0;
                    sumHora14 = 0;
                    sumHora15 = 0;
                    sumHora16 = 0;
                    sumHora17 = 0;
                    sumHora18 = 0;
                    sumHora19 = 0;
                    sumHora20 = 0;
                    sumHora21 = 0;
                    sumHora22 = 0;
                    sumHora23 = 0;

                    sumHora00 = sumHora00 + (item.ListaRegistros.Sum(p => p.Hora_00));
                    sumHora01 = sumHora01 + (item.ListaRegistros.Sum(p => p.Hora_01));
                    sumHora01 = sumHora02 + (item.ListaRegistros.Sum(p => p.Hora_02));
                    sumHora03 = sumHora03 + (item.ListaRegistros.Sum(p => p.Hora_03));
                    sumHora04 = sumHora04 + (item.ListaRegistros.Sum(p => p.Hora_04));
                    sumHora05 = sumHora05 + (item.ListaRegistros.Sum(p => p.Hora_05));
                    sumHora06 = sumHora06 + (item.ListaRegistros.Sum(p => p.Hora_06));
                    sumHora07 = sumHora07 + (item.ListaRegistros.Sum(p => p.Hora_07));
                    sumHora08 = sumHora08 + (item.ListaRegistros.Sum(p => p.Hora_08));
                    sumHora09 = sumHora09 + (item.ListaRegistros.Sum(p => p.Hora_09));
                    sumHora10 = sumHora10 + (item.ListaRegistros.Sum(p => p.Hora_10));
                    sumHora11 = sumHora11 + (item.ListaRegistros.Sum(p => p.Hora_11));
                    sumHora12 = sumHora12 + (item.ListaRegistros.Sum(p => p.Hora_12));
                    sumHora13 = sumHora13 + (item.ListaRegistros.Sum(p => p.Hora_13));
                    sumHora14 = sumHora14 + (item.ListaRegistros.Sum(p => p.Hora_14));
                    sumHora15 = sumHora15 + (item.ListaRegistros.Sum(p => p.Hora_15));
                    sumHora16 = sumHora16 + (item.ListaRegistros.Sum(p => p.Hora_16));
                    sumHora17 = sumHora17 + (item.ListaRegistros.Sum(p => p.Hora_17));
                    sumHora18 = sumHora18 + (item.ListaRegistros.Sum(p => p.Hora_18));
                    sumHora19 = sumHora19 + (item.ListaRegistros.Sum(p => p.Hora_19));
                    sumHora20 = sumHora20 + (item.ListaRegistros.Sum(p => p.Hora_20));
                    sumHora21 = sumHora21 + (item.ListaRegistros.Sum(p => p.Hora_21));
                    sumHora22 = sumHora22 + (item.ListaRegistros.Sum(p => p.Hora_22));
                    sumHora23 = sumHora23 + (item.ListaRegistros.Sum(p => p.Hora_23));
                }
                item.QuantidadeTotalDia = somatorio;

                item.totalHora00 = sumHora00;
                item.totalHora01 = sumHora01;
                item.totalHora02 = sumHora02;
                item.totalHora03 = sumHora03;
                item.totalHora04 = sumHora04;
                item.totalHora05 = sumHora05;
                item.totalHora06 = sumHora06;
                item.totalHora07 = sumHora07;
                item.totalHora08 = sumHora08;
                item.totalHora09 = sumHora09;
                item.totalHora10 = sumHora10;
                item.totalHora11 = sumHora11;
                item.totalHora12 = sumHora12;
                item.totalHora13 = sumHora13;
                item.totalHora14 = sumHora14;
                item.totalHora15 = sumHora15;
                item.totalHora16 = sumHora16;
                item.totalHora17 = sumHora17;
                item.totalHora18 = sumHora18;
                item.totalHora19 = sumHora19;
                item.totalHora20 = sumHora20;
                item.totalHora21 = sumHora21;
                item.totalHora22 = sumHora22;
                item.totalHora23 = sumHora23;

                item.Total_07_1859 = (sumHora07 + sumHora08 + sumHora09 + sumHora10 + sumHora11 + sumHora12 + sumHora13 + sumHora14 + sumHora15 + sumHora16 + sumHora17 + sumHora18);

                item.Total_19_0659 = (sumHora19 + sumHora20 + sumHora21 + sumHora22 + sumHora23 + sumHora00 + sumHora01 + sumHora02 + sumHora03 + sumHora04 + sumHora05 + sumHora06);
            }

            return ListResult.GetListResult(resultado, new string[] {
                "Data",
                "DescricaoClinica",
                "ListaRegistros",
                "QuantidadeTotalDia",
                "totalHora00",
                "totalHora01",
                "totalHora02",
                "totalHora03",
                "totalHora04",
                "totalHora05",
                "totalHora06",
                "totalHora07",
                "totalHora08",
                "totalHora09",
                "totalHora10",
                "totalHora11",
                "totalHora12",
                "totalHora13",
                "totalHora14",
                "totalHora15",
                "totalHora16",
                "totalHora17",
                "totalHora18",
                "totalHora19",
                "totalHora20",
                "totalHora21",
                "totalHora22",
                "totalHora23",
                "Total_07_1859",
                "Total_19_0659"
                      });


        }

        public ListResult ListarSpaDiagnosticoSexoFaixaEtaria(DateTime inicio, DateTime fim, Guid UnidadeId)
        {
            var registros = new List<SpaGrupoDiagnosticoSexoFaixaEtariaReadModel>();
            var pivoted = new List<SpaGrupoDiagnosticoSexoFaixaEtariaReadModel>();
            var listaErros = new List<ErroSpaGrupoDiagnosticoSexoFaixaEtaria>();
            fim = fim.AddDays(1);
            var listaAtendimentos = (from item in repository.Atendimento.Include("PrimeiroDiagnostico").Include("Acolhimento.Paciente")
                                               .Where(x => (x.InicioAtendimento >= inicio && x.InicioAtendimento <= fim) && (x.Profissional.Unidade.Id.Equals(UnidadeId)
                                                         && x.PrimeiroDiagnostico != null)).ToList()
                                     select new SpaGrupoDiagnosticoSexoFaixaEtariaReadModel()
                                     {
                                         CodigoCID = item.PrimeiroDiagnostico.Codigo,
                                         DescricaoCID = item.PrimeiroDiagnostico.Descricao,
                                         Idade = item.Acolhimento.Paciente.IdadeEmAnos == null ? 0 : (int)item.Acolhimento.Paciente.IdadeEmAnos,
                                         Sexo = item.Acolhimento.Paciente.Sexo,
                                         EhValido = true
                                     }).ToList();


            //        foreach (var item in listaAtendimentos)
            //        {
            //            SpaGrupoDiagnosticoSexoFaixaEtariaReadModel registro = new();
            //            try
            //            {

            //                registros.Add(registro);
            //            }
            //            catch (Exception ex)
            //            {
            //                listaErros.Add(new ErroSpaGrupoDiagnosticoSexoFaixaEtaria()
            //{
            //    AtendimentoId = item.Id.ToString(),
            //                    MensagemErro = ex.Message
            //                });
            //            }
            //        }

            //if (listaErros.Count > 0)
            //{
            //    //registros.Add(new SpaGrupoDiagnosticoSexoFaixaEtariaReadModel { ListaErros = listaErros, EhValido = false });
            //}


            registros = listaAtendimentos.GroupBy(p => p.DescricaoCID).Select(grp => new SpaGrupoDiagnosticoSexoFaixaEtariaReadModel()
            {
                DescricaoCID = grp.Key,
                ListaRegistros = grp.ToList(),
            }).OrderBy(d => d.CodigoCID).Distinct().ToList();



            foreach (var item in registros)
            {
                #region Feminino

                SpaGrupoDiagnosticoSexoFaixaEtariaReadModel reg = new SpaGrupoDiagnosticoSexoFaixaEtariaReadModel();

                reg.DescricaoCID = item.DescricaoCID.Trim();
                reg.CodigoCID = item.CodigoCID;
                reg.Sexo = "Feminino";
                reg.Idade = item.Idade;
                reg.Menor1ano = item.ListaRegistros.Where(r => (r.DescricaoCID.Trim().Equals(reg.DescricaoCID)) && (r.Sexo == reg.Sexo.Substring(0, 1)) && (r.Idade < 1)).Count();
                reg.De1a4 = item.ListaRegistros.Where(r => (r.DescricaoCID.Trim().Equals(reg.DescricaoCID)) && (r.Sexo == reg.Sexo.Substring(0, 1)) && (r.Idade >= 1) && (r.Idade <= 4)).Count();
                reg.De5a9 = item.ListaRegistros.Where(r => (r.DescricaoCID.Trim().Equals(reg.DescricaoCID)) && (r.Sexo == reg.Sexo.Substring(0, 1)) && (r.Idade >= 5) && (r.Idade <= 9)).Count();
                reg.De10a14 = item.ListaRegistros.Where(r => (r.DescricaoCID.Trim().Equals(reg.DescricaoCID)) && (r.Sexo == reg.Sexo.Substring(0, 1)) && (r.Idade >= 10) && (r.Idade <= 14)).Count();
                reg.De15a19 = item.ListaRegistros.Where(r => (r.DescricaoCID.Trim().Equals(reg.DescricaoCID)) && (r.Sexo == reg.Sexo.Substring(0, 1)) && (r.Idade >= 15) && (r.Idade <= 19)).Count();
                reg.De20a29 = item.ListaRegistros.Where(r => (r.DescricaoCID.Trim().Equals(reg.DescricaoCID)) && (r.Sexo == reg.Sexo.Substring(0, 1)) && (r.Idade >= 20) && (r.Idade <= 29)).Count();
                reg.De30a39 = item.ListaRegistros.Where(r => (r.DescricaoCID.Trim().Equals(reg.DescricaoCID)) && (r.Sexo == reg.Sexo.Substring(0, 1)) && (r.Idade >= 30) && (r.Idade <= 39)).Count();
                reg.De40a49 = item.ListaRegistros.Where(r => (r.DescricaoCID.Trim().Equals(reg.DescricaoCID)) && (r.Sexo == reg.Sexo.Substring(0, 1)) && ((r.Idade >= 40) && (r.Idade <= 49))).Count();
                reg.De50a59 = item.ListaRegistros.Where(r => (r.DescricaoCID.Trim().Equals(reg.DescricaoCID)) && (r.Sexo == reg.Sexo.Substring(0, 1)) && (r.Idade >= 50) && (r.Idade <= 59)).Count();
                reg.De60a69 = item.ListaRegistros.Where(r => (r.DescricaoCID.Trim().Equals(reg.DescricaoCID)) && (r.Sexo == reg.Sexo.Substring(0, 1)) && (r.Idade >= 60) && (r.Idade <= 69)).Count();
                reg.De70a79 = item.ListaRegistros.Where(r => (r.DescricaoCID.Trim().Equals(reg.DescricaoCID)) && (r.Sexo == reg.Sexo.Substring(0, 1)) && (r.Idade >= 70) && (r.Idade <= 79)).Count();
                reg.Maior80 = item.ListaRegistros.Where(r => (r.DescricaoCID.Trim().Equals(reg.DescricaoCID)) && (r.Sexo == reg.Sexo.Substring(0, 1)) && (r.Idade >= 80)).Count();
                reg.Total = (reg.Menor1ano + reg.De1a4 + reg.De5a9 + reg.De10a14 + reg.De15a19 + reg.De20a29 + reg.De30a39 + reg.De40a49 + reg.De50a59 + reg.De60a69 + reg.De70a79 + reg.Maior80);

                #endregion

                #region Masculino
                SpaGrupoDiagnosticoSexoFaixaEtariaReadModel regM = new SpaGrupoDiagnosticoSexoFaixaEtariaReadModel();

                regM.DescricaoCID = item.DescricaoCID.Trim();
                regM.CodigoCID = item.CodigoCID;
                regM.Sexo = "Masculino";
                regM.Idade = item.Idade;
                regM.Menor1ano = item.ListaRegistros.Where(r => (r.DescricaoCID.Trim().Equals(regM.DescricaoCID)) && (r.Sexo == regM.Sexo.Substring(0, 1)) && (r.Idade < 1)).Count();
                regM.De1a4 = item.ListaRegistros.Where(r => (r.DescricaoCID.Trim().Equals(regM.DescricaoCID)) && (r.Sexo == regM.Sexo.Substring(0, 1)) && (r.Idade >= 1) && (r.Idade <= 4)).Count();
                regM.De5a9 = item.ListaRegistros.Where(r => (r.DescricaoCID.Trim().Equals(regM.DescricaoCID)) && (r.Sexo == regM.Sexo.Substring(0, 1)) && (r.Idade >= 5) && (r.Idade <= 9)).Count();
                regM.De10a14 = item.ListaRegistros.Where(r => (r.DescricaoCID.Trim().Equals(regM.DescricaoCID)) && (r.Sexo == regM.Sexo.Substring(0, 1)) && (r.Idade >= 10) && (r.Idade <= 14)).Count();
                regM.De15a19 = item.ListaRegistros.Where(r => (r.DescricaoCID.Trim().Equals(regM.DescricaoCID)) && (r.Sexo == regM.Sexo.Substring(0, 1)) && (r.Idade >= 15) && (r.Idade <= 19)).Count();
                regM.De20a29 = item.ListaRegistros.Where(r => (r.DescricaoCID.Trim().Equals(regM.DescricaoCID)) && (r.Sexo == regM.Sexo.Substring(0, 1)) && (r.Idade >= 20) && (r.Idade <= 29)).Count();
                regM.De30a39 = item.ListaRegistros.Where(r => (r.DescricaoCID.Trim().Equals(regM.DescricaoCID)) && (r.Sexo == regM.Sexo.Substring(0, 1)) && (r.Idade >= 30) && (r.Idade <= 39)).Count();
                regM.De40a49 = item.ListaRegistros.Where(r => (r.DescricaoCID.Trim().Equals(regM.DescricaoCID)) && (r.Sexo == regM.Sexo.Substring(0, 1)) && ((r.Idade >= 40) && (r.Idade <= 49))).Count();
                regM.De50a59 = item.ListaRegistros.Where(r => (r.DescricaoCID.Trim().Equals(regM.DescricaoCID)) && (r.Sexo == regM.Sexo.Substring(0, 1)) && (r.Idade >= 50) && (r.Idade <= 59)).Count();
                regM.De60a69 = item.ListaRegistros.Where(r => (r.DescricaoCID.Trim().Equals(regM.DescricaoCID)) && (r.Sexo == regM.Sexo.Substring(0, 1)) && (r.Idade >= 60) && (r.Idade <= 69)).Count();
                regM.De70a79 = item.ListaRegistros.Where(r => (r.DescricaoCID.Trim().Equals(regM.DescricaoCID)) && (r.Sexo == regM.Sexo.Substring(0, 1)) && (r.Idade >= 70) && (r.Idade <= 79)).Count();
                regM.Maior80 = item.ListaRegistros.Where(r => (r.DescricaoCID.Trim().Equals(regM.DescricaoCID)) && (r.Sexo == regM.Sexo.Substring(0, 1)) && (r.Idade >= 80)).Count();
                regM.Total = (regM.Menor1ano + regM.De1a4 + regM.De5a9 + regM.De10a14 + regM.De15a19 + regM.De20a29 + regM.De30a39 + regM.De40a49 + regM.De50a59 + regM.De60a69 + regM.De70a79 + regM.Maior80);

                #endregion
                regM.TotalGrupo = reg.TotalGrupo = reg.Total + regM.Total;
                regM.TotalMenor1ano = reg.TotalMenor1ano = reg.Menor1ano + regM.Menor1ano;
                regM.TotalDe1a4 = reg.TotalDe1a4 = reg.De1a4 + regM.De1a4;
                regM.TotalDe5a9 = reg.TotalDe5a9 = reg.De5a9 + regM.De5a9;
                regM.TotalDe10a14 = reg.TotalDe10a14 = reg.De10a14 + regM.De10a14;
                regM.TotalDe15a19 = reg.TotalDe15a19 = reg.De15a19 + regM.De15a19;
                regM.TotalDe20a29 = reg.TotalDe20a29 = reg.De20a29 + regM.De20a29;
                regM.TotalDe30a39 = reg.TotalDe30a39 = reg.De30a39 + regM.De30a39;
                regM.TotalDe40a49 = reg.TotalDe40a49 = reg.De40a49 + regM.De40a49;
                regM.TotalDe50a59 = reg.TotalDe50a59 = reg.De50a59 + regM.De50a59;
                regM.TotalDe60a69 = reg.TotalDe60a69 = reg.De60a69 + regM.De60a69;
                regM.TotalDe70a79 = reg.TotalDe70a79 = reg.De70a79 + regM.De70a79;
                regM.TotalMaior80 = reg.TotalMaior80 = reg.Maior80 + regM.Maior80;

                if (reg.Total > 0)
                    pivoted.Add(reg);

                if (regM.Total > 0)
                    pivoted.Add(regM);
            }


            // pivoted = pivoted.GroupBy(p => p.DescricaoCID).Select(grp => grp.First()).ToList();
            pivoted = pivoted.GroupBy(p => p.DescricaoCID).Select(grp => new SpaGrupoDiagnosticoSexoFaixaEtariaReadModel()
            {
                DescricaoCID = grp.Key,
                ListaRegistros = grp.ToList(),
            }).OrderBy(d => d.CodigoCID).Distinct().ToList();

            pivoted = pivoted.GroupBy(p => p.DescricaoCID).Select(grp => grp.First()).ToList();


            foreach (var item in pivoted)
            {
                item.TotalGrupo = item.ListaRegistros.Where(p => p.DescricaoCID.Equals(item.DescricaoCID)).Select(p => p.TotalGrupo).FirstOrDefault();
                item.TotalMenor1ano = item.ListaRegistros.Where(p => p.DescricaoCID.Equals(item.DescricaoCID)).Select(p => p.TotalMenor1ano).FirstOrDefault();
                item.TotalDe1a4 = item.ListaRegistros.Where(p => p.DescricaoCID.Equals(item.DescricaoCID)).Select(p => p.TotalDe1a4).FirstOrDefault();
                item.TotalDe5a9 = item.ListaRegistros.Where(p => p.DescricaoCID.Equals(item.DescricaoCID)).Select(p => p.TotalDe5a9).FirstOrDefault();
                item.TotalDe10a14 = item.ListaRegistros.Where(p => p.DescricaoCID.Equals(item.DescricaoCID)).Select(p => p.TotalDe10a14).FirstOrDefault();
                item.TotalDe15a19 = item.ListaRegistros.Where(p => p.DescricaoCID.Equals(item.DescricaoCID)).Select(p => p.TotalDe15a19).FirstOrDefault();
                item.TotalDe20a29 = item.ListaRegistros.Where(p => p.DescricaoCID.Equals(item.DescricaoCID)).Select(p => p.TotalDe20a29).FirstOrDefault();
                item.TotalDe30a39 = item.ListaRegistros.Where(p => p.DescricaoCID.Equals(item.DescricaoCID)).Select(p => p.TotalDe30a39).FirstOrDefault();
                item.TotalDe40a49 = item.ListaRegistros.Where(p => p.DescricaoCID.Equals(item.DescricaoCID)).Select(p => p.TotalDe40a49).FirstOrDefault();
                item.TotalDe50a59 = item.ListaRegistros.Where(p => p.DescricaoCID.Equals(item.DescricaoCID)).Select(p => p.TotalDe50a59).FirstOrDefault();
                item.TotalDe60a69 = item.ListaRegistros.Where(p => p.DescricaoCID.Equals(item.DescricaoCID)).Select(p => p.TotalDe60a69).FirstOrDefault();
                item.TotalDe70a79 = item.ListaRegistros.Where(p => p.DescricaoCID.Equals(item.DescricaoCID)).Select(p => p.TotalDe70a79).FirstOrDefault();
                item.TotalMaior80 = item.ListaRegistros.Where(p => p.DescricaoCID.Equals(item.DescricaoCID)).Select(p => p.TotalMaior80).FirstOrDefault();
            }

            return ListResult.GetListResult(pivoted, new string[] { "CodigoCID", "DescricaoCID","TotalGrupo","TotalMenor1ano","TotalDe1a4", "TotalDe5a9",
                "TotalDe10a14","TotalDe15a19","TotalDe20a29","TotalDe30a39","TotalDe40a49"
                ,"TotalDe50a59","TotalDe60a69","TotalDe70a79","TotalMaior80", "Idade", "Sexo", "ListaRegistros", "ListaErros" });

        }

        /// <summary>
        /// Falta definição sobre lista de atendimentos para os médicos diferentes que atendem em uma possível troca de plantão
        /// </summary>
        /// <param name="inicio"></param>
        /// <param name="fim"></param>
        /// <param name="UnidadeId"></param>
        /// <returns></returns>
        public ListResult ListarPacientesAtendidosEspecialidadeRisco(DateTime inicio, DateTime fim, Guid UnidadeId)
        {

            fim = fim.AddDays(1);
            var listaAtendimentos = repository.Atendimento.Include("Acolhimento.Classificacao.Risco")
                .Where(x => (x.Acolhimento.DataHora >= inicio && x.Acolhimento.DataHora <= fim) && (x.Acolhimento.Profissional.Unidade.Id.Equals(UnidadeId))).ToList();


            return new ListResult();
        }

        public ListResult ListarPacientesInternadosMais24Horas(Guid UnidadeId, string paciente, Guid enfermariaId, Guid leitoId, DateTime? dataInternacao, Guid profissionalId)
        {

            List<PacientesInternadosMais24HorasReadModel> listaPacientesInternados = new List<PacientesInternadosMais24HorasReadModel>();
            var fim = DateTime.Now.AddHours(-24);
            var listaInternacao = repository.Internacao.Include("Atendimento.Profissional").Include("Atendimento.Acolhimento.Paciente").Include("Movimentacoes.LeitoAtual.Enfermaria")
                .Where(x => (x.Atendimento.Acolhimento.Profissional.Unidade.Id.Equals(UnidadeId))
                && (x.Atendimento.FimAtendimento == null) &&
                ((x.DataInternacao < fim)));

            if (listaInternacao.Count() > 0)
            {
                if (!string.IsNullOrWhiteSpace(paciente))
                    listaInternacao = listaInternacao.Where(x => x.Atendimento.Acolhimento.Paciente.Nome.ToUpper().Contains(paciente.ToUpper()));
                if (enfermariaId != Guid.Empty)
                    listaInternacao = listaInternacao.Where(x => x.Movimentacoes.OrderByDescending(y => y.DataMovimentacao).FirstOrDefault().LeitoAtual.Enfermaria.Id == enfermariaId);
                if (leitoId != Guid.Empty)
                    listaInternacao = listaInternacao.Where(x => x.Movimentacoes.OrderByDescending(y => y.DataMovimentacao).FirstOrDefault().LeitoAtual.Id == leitoId);
                if (dataInternacao != null)
                {
                    DateTime dataFim = dataInternacao.Value.AddDays(1);
                    listaInternacao = listaInternacao.Where(x => x.DataInternacao >= dataInternacao && x.DataInternacao <= dataFim);
                }

                if (profissionalId != Guid.Empty)
                    listaInternacao = listaInternacao.Where(x => x.Atendimento.Profissional.Id == profissionalId);
            }


            listaPacientesInternados = (from item in listaInternacao.ToList()
                                        select new PacientesInternadosMais24HorasReadModel()
                                        {
                                            NomePaciente = item.Atendimento.Acolhimento.Paciente.Nome,
                                            DataInternacao = item.DataInternacao.Day.ToString() + "/" + item.DataInternacao.Month.ToString() + "/" + item.DataInternacao.Year.ToString(),
                                            HoraInternacao = item.DataInternacao.ToString("HH:mm"),
                                            Medico = item.Atendimento.Profissional.Nome,
                                            Leito = item.Movimentacoes.OrderByDescending(x => x.DataMovimentacao).FirstOrDefault().LeitoAtual.Nome,
                                            Enfermaria = item.Movimentacoes.OrderByDescending(x => x.DataMovimentacao).FirstOrDefault().LeitoAtual.Enfermaria.Nome,
                                            TempoDecorrido = CalculaTempo(item.DataInternacao)
                                        }).ToList();

            //foreach (var item in listaInternacao)
            //{
            //    PacientesInternadosMais24HorasReadModel pac = new PacientesInternadosMais24HorasReadModel();


            //    listaPacientesInternados.Add(pac);
            //}


            listaPacientesInternados = listaPacientesInternados.OrderBy(x => x.DataInternacao).OrderBy(y => y.HoraInternacao).ToList();
            return ListResult.GetListResult(listaPacientesInternados, new string[] { "NomePaciente", "DataInternacao", "HoraInternacao", "Medico", "Leito", "Enfermaria", "TempoDecorrido" });
        }

        public ListResult ListarCensoDiarioUrgenciaEmergencia(Guid UnidadeId)
        {
            List<CensoDiariaReadModel> listaPacientesInternados = new List<CensoDiariaReadModel>();
            var listaInternacao = repository.Internacao.Include("Atendimento.Profissional").Include("Atendimento.Acolhimento.Paciente").Include("Atendimento.Acolhimento.Servico").Include("Movimentacoes.LeitoAtual.Enfermaria").Include("Movimentacoes.Clinica")
                .Where(x => (x.Atendimento.Acolhimento.Profissional.Unidade.Id.Equals(UnidadeId))
                && (x.Atendimento.FimAtendimento == null)).ToList();

            listaPacientesInternados = (from item in listaInternacao.OrderBy(x => x.DataInternacao)
                                        select new CensoDiariaReadModel()
                                        {
                                            CodigoPaciente = item.Atendimento.Acolhimento.Paciente.CodigoPaciente,
                                            NomePaciente = item.Atendimento.Acolhimento.Paciente.Nome,
                                            Idade = item.Atendimento.Acolhimento.Paciente.IdadeEmAnos == null ? "" : item.Atendimento.Acolhimento.Paciente.IdadeEmAnos.ToString(),
                                            Clinica = item.Movimentacoes.OrderByDescending(x => x.DataMovimentacao).FirstOrDefault().LeitoAtual.Clinica != null
                                                      ? item.Movimentacoes.OrderByDescending(x => x.DataMovimentacao).FirstOrDefault().LeitoAtual.Clinica.Descricao
                                                      : item.Atendimento.Acolhimento.Servico.Descricao,
                                            Dias = CalculaTempo(item.DataInternacao),
                                            Entrada = item.DataInternacao.ToString("dd/MM/yyyy ", new CultureInfo("en-US")),
                                            Enfermaria = item.Movimentacoes.FirstOrDefault().LeitoAtual.Enfermaria.Nome,
                                            Leito = item.Movimentacoes.FirstOrDefault().LeitoAtual.Nome
                                        }).ToList();
            return ListResult.GetListResult(listaPacientesInternados, new string[] { "CodigoPaciente", "NomePaciente", "Idade", "Clinica", "Dias", "Entrada", "Enfermaria", "Leito" });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="codigoBoletim"></param>
        /// <returns></returns>
        public ExtratoContaPacienteReadModel ExtratoContaPaciente(string codigoBoletim)
        {
            ExtratoContaPacienteReadModel extrato = new ExtratoContaPacienteReadModel();
            extrato.Procedimentos = new List<ExtratoPacienteProcedimentos>();

            var atendimento = repository.Atendimento.Include("Acolhimento.Paciente.Endereco").Include("PrimeiroDiagnostico").Include("SegundoDiagnostico")
                                                                  .Include("Profissional.Cbo").Include("Acolhimento.Servico").Include("Acolhimento.Profissional").Include("Procedimentos")
                                                                  .Include("Checkout.MotivoCheckout.Tipo")
                                                                   .FirstOrDefault(x => x.Acolhimento.CodigoBoletim == codigoBoletim);

            /// Dados do atendimento
            extrato.Atendimento = ViewModelUtils.ToModel<Domain.Entities.DbClasses.Atendimento.Atendimento, AtendimentoReadModel>(atendimento);

            /// Dados da internação
            var internacao = repository.Internacao.Include("Movimentacoes.Clinica").Include("Atendimento")
                                                  .Include("Movimentacoes.LeitoAtual.Enfermaria")
                                                  .FirstOrDefault(x => x.Atendimento.Id == atendimento.Id);

            extrato.DataInternacao = internacao.DataInternacao.ToString("dd/MM/yyyy");
            var ultimamovimentacao = internacao.Movimentacoes.OrderByDescending(x => x.DataMovimentacao).FirstOrDefault();
            if (ultimamovimentacao.Clinica != null)
                extrato.Clinica = ultimamovimentacao.Clinica.Descricao;
            else
                extrato.Clinica = atendimento.Acolhimento.Servico.Descricao;
            extrato.EnfermariaLeito = string.Format("{0}/{1}", ultimamovimentacao.LeitoAtual.Enfermaria.Nome, ultimamovimentacao.LeitoAtual.Nome);

            /// Procedimentos do Atendimento
            var procedimentos = from p in repository.AtendimentoProcedimento.Where(x => x.AtendimentoId == atendimento.Id)
                                select new ExtratoPacienteProcedimentos()
                                {
                                    Cbo = atendimento.Profissional.Cbo.Descricao,
                                    Procedimento = p.NomeProcedimento,
                                    Quantidade = 1,
                                    Valor = p.VLSA.ToString(),
                                    TipoProcedimento = p.QtDiasPermanencia < 9999 ? "H" : "P",
                                    ValorProfissional = p.VLSP.ToString()
                                };

            var todosPRocedimentosDaEvolucao = repository.Evolucao.Include("Atemdimento.Acolhimento").Where(x => x.Atemdimento.Acolhimento.CodigoBoletim == codigoBoletim);

            /// Procedimentos de Evolução
            var procedimentosDeEvolucao = from e in todosPRocedimentosDaEvolucao.GroupBy(x => x.Medico.Cbo)
                                          select new
                                          {
                                              cbo = e.Key.Descricao.ToString(),
                                              lista = todosPRocedimentosDaEvolucao.SelectMany(y => y.EvolucaoProcedimentos).Where(z => z.Evolucao.Medico.Cbo == e.Key)
                                          };
            //grp.SelectMany(x => x.EvolucaoProcedimentos)

            List<ExtratoPacienteProcedimentos> procedimentosEvoluacao = new List<ExtratoPacienteProcedimentos>();

            foreach (var procEvolucao in procedimentosDeEvolucao)
            {
                foreach (var item in procEvolucao.lista)
                {
                    ExtratoPacienteProcedimentos extratoPacienteProcedimento = new ExtratoPacienteProcedimentos();
                    extratoPacienteProcedimento.Cbo = procEvolucao.cbo;
                    extratoPacienteProcedimento.Procedimento = string.Format("{0} - {1}", item.Procedimento.CO_PROCEDIMENTO, item.Procedimento.NO_PROCEDIMENTO);
                    extratoPacienteProcedimento.Valor = item.Procedimento.VL_SA.ToString();
                    extratoPacienteProcedimento.ValorProfissional = item.Procedimento.VL_SP.ToString();
                    extratoPacienteProcedimento.TipoProcedimento = item.Procedimento.QT_DIAS_PERMANENCIA < 9999 ? "H" : "P";
                    procedimentosEvoluacao.Add(extratoPacienteProcedimento);
                }
            }

            extrato.Procedimentos.AddRange(from p in procedimentosEvoluacao
                                           group p by new
                                           {
                                               p.Procedimento,
                                               p.Cbo
                                           } into grp
                                           select new ExtratoPacienteProcedimentos
                                           {
                                               Cbo = grp.Key.Cbo,
                                               Procedimento = grp.Key.Procedimento,
                                               Quantidade = grp.Count(),
                                               Valor = (Convert.ToDecimal(grp.FirstOrDefault().Valor) + grp.Count()).ToString(),
                                               ValorProfissional = (Convert.ToDecimal(grp.FirstOrDefault().ValorProfissional) + grp.Count()).ToString(),
                                               TipoProcedimento = grp.FirstOrDefault().TipoProcedimento
                                           });

            /// Procedimentos de enfermagem
            //var procedimentosEnfermagem = from e in repository.ProcedimentosEnfermagem.Include("Evolucao.MovimentacaoLeito.Internacao.Atendimento.Acolhimento")
            //                                                                          .Include("Procedimento")
            //                              where e.Evolucao.MovimentacaoLeito.Internacao.Atendimento.Acolhimento.CodigoBoletim == codigoBoletim
            //                              group e by new
            //                              {
            //                                  e.Procedimento.CO_PROCEDIMENTO,
            //                                  e.Procedimento.NO_PROCEDIMENTO,
            //                                  e.Evolucao.Profissional.Cbo.Codigo
            //                              } into grp
            //                              select new ExtratoPacienteProcedimentos
            //                              {
            //                                  Procedimento = grp.Key.CO_PROCEDIMENTO + " - " + grp.Key.NO_PROCEDIMENTO,
            //                                  Cbo = grp.Key.Codigo,
            //                                  Valor = (grp.FirstOrDefault().Procedimento.VL_SH * grp.Count()).ToString(),
            //                                  ValorProfissional = (grp.FirstOrDefault().Procedimento.VL_SP * grp.Count()).ToString(),
            //                                  Quantidade = grp.Count(),
            //                                  TipoProcedimento = grp.FirstOrDefault().Procedimento.QT_DIAS_PERMANENCIA < 9999 ? "H" : "P"
            //                              };
            //extrato.Procedimentos.AddRange(procedimentosEnfermagem);

            /// Exames realizados
            var exames = from e in repository.ExameAtendimento.Include("Atendimento.Acolhimento").Include("Executor.Cbo").Where(x => x.Atendimento.Acolhimento.CodigoBoletim == codigoBoletim && x.Executor != null).ToList()
                         join p in repository.Procedimento on e.Procedimento equals p.CO_PROCEDIMENTO
                         group e by new
                         {
                             p.CO_PROCEDIMENTO,
                             p.NO_PROCEDIMENTO,
                             p.QT_DIAS_PERMANENCIA,
                             e.Executor.Cbo.Codigo,
                             p.VL_SA,
                             p.VL_SP
                         } into grp
                         select new ExtratoPacienteProcedimentos
                         {
                             Procedimento = grp.Key.CO_PROCEDIMENTO + " - " + grp.Key.NO_PROCEDIMENTO,
                             Cbo = grp.Key.Codigo,
                             Valor = (grp.Key.VL_SA * grp.Count()).ToString(),
                             ValorProfissional = (grp.Key.VL_SP * grp.Count()).ToString(),
                             Quantidade = grp.Count(),
                             TipoProcedimento = grp.Key.QT_DIAS_PERMANENCIA < 9999 ? "H" : "P"
                         };

            extrato.Procedimentos.AddRange(exames);

            if (extrato.Procedimentos.Count > 0)
            {
                /// TODO: Verifica que campos sao utilizados para o objeto extrato.ProcedimentoRealizado 
                //Guid id = procedimentos.FirstOrDefault().ProcedimentoId;
                //extrato.ProcedimentoSolicitado = ViewModelUtils.ToModel<Procedimento, ProcedimentoReadModel>(repository.Procedimento.FirstOrDefault(x => x.Id == id));
                //extrato.ProcedimentoRealizado = extrato.ProcedimentoSolicitado;
            }

            extrato.TotalServicosHospitalares = extrato.Procedimentos.Count > 0 ? extrato.Procedimentos.Sum(x => Convert.ToDecimal(x.Valor)).ToString() : "0,00";
            extrato.TotalServicosProfissionais = extrato.Procedimentos.Count > 0 ? extrato.Procedimentos.Sum(x => Convert.ToDecimal(x.ValorProfissional)).ToString() : "0,00";
            extrato.TotalAIH = (Convert.ToDecimal(extrato.TotalServicosHospitalares) + Convert.ToDecimal(extrato.TotalServicosProfissionais)).ToString();

            return extrato;
        }

        public ListResult ListarProducaoDiariaPorExame(int mes, int ano, Guid UnidadeId, String classificacao)
        {
            var lista = new List<ProducaoDiariaPorExameReadModel>();
            string tipo = "";
            CultureInfo info = new CultureInfo("pt-BR");
            DateTimeFormatInfo pt = info.DateTimeFormat;
            string mesExtenso = pt.GetMonthName(mes).ToLower();

            mesExtenso = mesExtenso.Replace(mesExtenso[0], mesExtenso.ToUpper()[0]);

            string anoMes = string.Concat(mesExtenso, "/", ano.ToString());

            if (classificacao.Equals("Diagnostico"))
            {
                tipo = structTipoExame.ExamesDeDiagnostico;
            }
            else if (classificacao.Equals("Laboratoriais"))
            {
                tipo = structTipoExame.ExamesLaboratoriais;
            }
            else if (classificacao.Equals("Radiologia"))
            {
                tipo = structTipoExame.ExamesDeImagem;
            }
            List<ExameAtendimento> listaexame = new List<ExameAtendimento>();

            if (String.IsNullOrEmpty(tipo))
            {
                listaexame = repository.ExameAtendimento.Include("Atendimento")
                     .Where(x => (((DateTime)x.DataExecucao).Month == mes && ((DateTime)x.DataExecucao).Year == ano) && (x.Atendimento.Profissional.Unidade.Id.Equals(UnidadeId))).ToList();
            }
            else
            {
                tipo = string.Format("20{0}", tipo);
                listaexame = repository.ExameAtendimento.Include("Atendimento")
                     .Where(x => (((DateTime)x.DataExecucao).Month == mes) && (((DateTime)x.DataExecucao).Year == ano) && (x.Atendimento.Profissional.Unidade.Id.Equals(UnidadeId)) && (x.CodigoProcedimento.StartsWith(tipo))).ToList();
            }

            foreach (var item in listaexame.Distinct())
            {
                ProducaoDiariaPorExameReadModel pde = new ProducaoDiariaPorExameReadModel();
                try
                {
                    pde.Descricao = item.Exame;

                    pde.AnoMes = anoMes;
                    pde.Dia01 = listaexame.Where(p => (p.Exame.Trim() == pde.Descricao.Trim()) && (((DateTime)p.DataExecucao).Day == 1)).Count();
                    pde.Dia02 = listaexame.Where(p => (p.Exame.Trim() == pde.Descricao.Trim()) && (((DateTime)p.DataExecucao).Day == 2)).Count();
                    pde.Dia03 = listaexame.Where(p => (p.Exame.Trim() == pde.Descricao.Trim()) && (((DateTime)p.DataExecucao).Day == 3)).Count();
                    pde.Dia04 = listaexame.Where(p => (p.Exame.Trim() == pde.Descricao.Trim()) && (((DateTime)p.DataExecucao).Day == 4)).Count();
                    pde.Dia05 = listaexame.Where(p => (p.Exame.Trim() == pde.Descricao.Trim()) && (((DateTime)p.DataExecucao).Day == 5)).Count();
                    pde.Dia06 = listaexame.Where(p => (p.Exame.Trim() == pde.Descricao.Trim()) && (((DateTime)p.DataExecucao).Day == 6)).Count();
                    pde.Dia07 = listaexame.Where(p => (p.Exame.Trim() == pde.Descricao.Trim()) && (((DateTime)p.DataExecucao).Day == 7)).Count();
                    pde.Dia08 = listaexame.Where(p => (p.Exame.Trim() == pde.Descricao.Trim()) && (((DateTime)p.DataExecucao).Day == 8)).Count();
                    pde.Dia09 = listaexame.Where(p => (p.Exame.Trim() == pde.Descricao.Trim()) && (((DateTime)p.DataExecucao).Day == 9)).Count();
                    pde.Dia10 = listaexame.Where(p => (p.Exame.Trim() == pde.Descricao.Trim()) && (((DateTime)p.DataExecucao).Day == 10)).Count();
                    pde.Dia11 = listaexame.Where(p => (p.Exame.Trim() == pde.Descricao.Trim()) && (((DateTime)p.DataExecucao).Day == 11)).Count();
                    pde.Dia12 = listaexame.Where(p => (p.Exame.Trim() == pde.Descricao.Trim()) && (((DateTime)p.DataExecucao).Day == 12)).Count();
                    pde.Dia13 = listaexame.Where(p => (p.Exame.Trim() == pde.Descricao.Trim()) && (((DateTime)p.DataExecucao).Day == 13)).Count();
                    pde.Dia14 = listaexame.Where(p => (p.Exame.Trim() == pde.Descricao.Trim()) && (((DateTime)p.DataExecucao).Day == 14)).Count();
                    pde.Dia15 = listaexame.Where(p => (p.Exame.Trim() == pde.Descricao.Trim()) && (((DateTime)p.DataExecucao).Day == 15)).Count();
                    pde.Dia16 = listaexame.Where(p => (p.Exame.Trim() == pde.Descricao.Trim()) && (((DateTime)p.DataExecucao).Day == 16)).Count();
                    pde.Dia17 = listaexame.Where(p => (p.Exame.Trim() == pde.Descricao.Trim()) && (((DateTime)p.DataExecucao).Day == 17)).Count();
                    pde.Dia18 = listaexame.Where(p => (p.Exame.Trim() == pde.Descricao.Trim()) && (((DateTime)p.DataExecucao).Day == 18)).Count();
                    pde.Dia19 = listaexame.Where(p => (p.Exame.Trim() == pde.Descricao.Trim()) && (((DateTime)p.DataExecucao).Day == 19)).Count();
                    pde.Dia20 = listaexame.Where(p => (p.Exame.Trim() == pde.Descricao.Trim()) && (((DateTime)p.DataExecucao).Day == 20)).Count();
                    pde.Dia21 = listaexame.Where(p => (p.Exame.Trim() == pde.Descricao.Trim()) && (((DateTime)p.DataExecucao).Day == 21)).Count();
                    pde.Dia22 = listaexame.Where(p => (p.Exame.Trim() == pde.Descricao.Trim()) && (((DateTime)p.DataExecucao).Day == 22)).Count();
                    pde.Dia23 = listaexame.Where(p => (p.Exame.Trim() == pde.Descricao.Trim()) && (((DateTime)p.DataExecucao).Day == 23)).Count();
                    pde.Dia24 = listaexame.Where(p => (p.Exame.Trim() == pde.Descricao.Trim()) && (((DateTime)p.DataExecucao).Day == 24)).Count();
                    pde.Dia25 = listaexame.Where(p => (p.Exame.Trim() == pde.Descricao.Trim()) && (((DateTime)p.DataExecucao).Day == 25)).Count();
                    pde.Dia26 = listaexame.Where(p => (p.Exame.Trim() == pde.Descricao.Trim()) && (((DateTime)p.DataExecucao).Day == 26)).Count();
                    pde.Dia27 = listaexame.Where(p => (p.Exame.Trim() == pde.Descricao.Trim()) && (((DateTime)p.DataExecucao).Day == 27)).Count();
                    pde.Dia28 = listaexame.Where(p => (p.Exame.Trim() == pde.Descricao.Trim()) && (((DateTime)p.DataExecucao).Day == 28)).Count();
                    pde.Dia29 = listaexame.Where(p => (p.Exame.Trim() == pde.Descricao.Trim()) && (((DateTime)p.DataExecucao).Day == 29)).Count();
                    pde.Dia30 = listaexame.Where(p => (p.Exame.Trim() == pde.Descricao.Trim()) && (((DateTime)p.DataExecucao).Day == 30)).Count();
                    pde.Dia31 = listaexame.Where(p => (p.Exame.Trim() == pde.Descricao.Trim()) && (((DateTime)p.DataExecucao).Day == 31)).Count();

                    pde.TotalMes = pde.Dia01 + pde.Dia02 + pde.Dia03 + pde.Dia04 + pde.Dia05 + pde.Dia06 + pde.Dia07 + pde.Dia08 + pde.Dia09 + pde.Dia10 + pde.Dia11 + pde.Dia12 + pde.Dia13 + pde.Dia14 + pde.Dia15 + pde.Dia16 + pde.Dia17 + pde.Dia18 + pde.Dia19 + pde.Dia20 + pde.Dia21 + pde.Dia22 + pde.Dia23 + pde.Dia24 + pde.Dia25 + pde.Dia26 + pde.Dia27 + pde.Dia28 + pde.Dia29 + pde.Dia30 + pde.Dia31;

                    pde.TotalDia01 = listaexame.Where(p => ((DateTime)p.DataExecucao).Day == 1).Count();
                    pde.TotalDia02 = listaexame.Where(p => ((DateTime)p.DataExecucao).Day == 2).Count();
                    pde.TotalDia03 = listaexame.Where(p => ((DateTime)p.DataExecucao).Day == 3).Count();
                    pde.TotalDia04 = listaexame.Where(p => ((DateTime)p.DataExecucao).Day == 4).Count();
                    pde.TotalDia05 = listaexame.Where(p => ((DateTime)p.DataExecucao).Day == 5).Count();
                    pde.TotalDia06 = listaexame.Where(p => ((DateTime)p.DataExecucao).Day == 6).Count();
                    pde.TotalDia07 = listaexame.Where(p => ((DateTime)p.DataExecucao).Day == 7).Count();
                    pde.TotalDia08 = listaexame.Where(p => ((DateTime)p.DataExecucao).Day == 8).Count();
                    pde.TotalDia09 = listaexame.Where(p => ((DateTime)p.DataExecucao).Day == 9).Count();
                    pde.TotalDia10 = listaexame.Where(p => ((DateTime)p.DataExecucao).Day == 10).Count();
                    pde.TotalDia11 = listaexame.Where(p => ((DateTime)p.DataExecucao).Day == 11).Count();
                    pde.TotalDia12 = listaexame.Where(p => ((DateTime)p.DataExecucao).Day == 12).Count();
                    pde.TotalDia13 = listaexame.Where(p => ((DateTime)p.DataExecucao).Day == 13).Count();
                    pde.TotalDia14 = listaexame.Where(p => ((DateTime)p.DataExecucao).Day == 14).Count();
                    pde.TotalDia15 = listaexame.Where(p => ((DateTime)p.DataExecucao).Day == 15).Count();
                    pde.TotalDia16 = listaexame.Where(p => ((DateTime)p.DataExecucao).Day == 16).Count();
                    pde.TotalDia17 = listaexame.Where(p => ((DateTime)p.DataExecucao).Day == 17).Count();
                    pde.TotalDia18 = listaexame.Where(p => ((DateTime)p.DataExecucao).Day == 18).Count();
                    pde.TotalDia19 = listaexame.Where(p => ((DateTime)p.DataExecucao).Day == 19).Count();
                    pde.TotalDia20 = listaexame.Where(p => ((DateTime)p.DataExecucao).Day == 20).Count();
                    pde.TotalDia21 = listaexame.Where(p => ((DateTime)p.DataExecucao).Day == 21).Count();
                    pde.TotalDia22 = listaexame.Where(p => ((DateTime)p.DataExecucao).Day == 22).Count();
                    pde.TotalDia23 = listaexame.Where(p => ((DateTime)p.DataExecucao).Day == 23).Count();
                    pde.TotalDia24 = listaexame.Where(p => ((DateTime)p.DataExecucao).Day == 24).Count();
                    pde.TotalDia25 = listaexame.Where(p => ((DateTime)p.DataExecucao).Day == 25).Count();
                    pde.TotalDia26 = listaexame.Where(p => ((DateTime)p.DataExecucao).Day == 26).Count();
                    pde.TotalDia27 = listaexame.Where(p => ((DateTime)p.DataExecucao).Day == 27).Count();
                    pde.TotalDia28 = listaexame.Where(p => ((DateTime)p.DataExecucao).Day == 28).Count();
                    pde.TotalDia29 = listaexame.Where(p => ((DateTime)p.DataExecucao).Day == 29).Count();
                    pde.TotalDia30 = listaexame.Where(p => ((DateTime)p.DataExecucao).Day == 30).Count();
                    pde.TotalDia31 = listaexame.Where(p => ((DateTime)p.DataExecucao).Day == 31).Count();


                    pde.TotalGeralMes = pde.TotalDia01 + pde.TotalDia02 + pde.TotalDia03 + pde.TotalDia04 + pde.TotalDia05 + pde.TotalDia06 + pde.TotalDia07 + pde.TotalDia08 + pde.TotalDia09 + pde.TotalDia10 + pde.TotalDia11 + pde.TotalDia12 + pde.TotalDia13 + pde.TotalDia14 + pde.TotalDia15 + pde.TotalDia16 + pde.TotalDia17 + pde.TotalDia18 + pde.TotalDia19 + pde.TotalDia20 + pde.TotalDia21 + pde.TotalDia22 + pde.TotalDia23 + pde.TotalDia24 + pde.TotalDia25 + pde.TotalDia26 + pde.TotalDia27 + pde.TotalDia28 + pde.TotalDia29 + pde.TotalDia30 + pde.TotalDia31;

                    string JaExiste = lista.Where(t => t.Descricao.Equals(pde.Descricao)).Select(j => j.Descricao).FirstOrDefault();

                    if (string.IsNullOrEmpty(JaExiste))
                        lista.Add(pde);
                }
                catch (Exception)
                {


                }
            }

            return ListResult.GetListResult(lista.Distinct().ToList(),
                new string[] { "Descricao", "AnoMes",
                "Dia01", "Dia02","Dia03","Dia04","Dia05", "Dia06","Dia07","Dia08","Dia09","Dia10","Dia11","Dia12","Dia13","Dia14","Dia15", "Dia16", "Dia17",
                "Dia18", "Dia19","Dia20","Dia21","Dia22","Dia23","Dia24","Dia25","Dia26","Dia27","Dia28","Dia29","Dia30","Dia31", "TotalMes",
                "TotalDia01", "TotalDia02","TotalDia03","TotalDia04","TotalDia05", "TotalDia06","TotalDia07","TotalDia08","TotalDia09","TotalDia10","TotalDia11","TotalDia12","TotalDia13","TotalDia14","TotalDia15", "TotalDia16", "TotalDia17", "TotalDia18", "TotalDia19","TotalDia20","TotalDia21","TotalDia22","TotalDia23","TotalDia24","TotalDia25","TotalDia26","TotalDia27","TotalDia28","TotalDia29","TotalDia30","TotalDia31",
                "TotalGeralMes" });
        }

        public ListResult ListarEstatisticaProducaoExames(DateTime inicio, DateTime fim, Guid UnidadeId, String classificacao)
        {
            try
            {
                var lista = new List<EstatisticaProducaoExamesReadModel>();
                List<ExameAtendimento> listaexame = new List<ExameAtendimento>();
                var final = fim.AddDays(1);
                string tipo = string.Empty;
                if (classificacao == "Diagnostico")
                {
                    tipo = "D";
                }
                else if (classificacao == "Laboratoriais")
                {
                    tipo = "L";
                }
                else if (classificacao == "Radiologia")
                {
                    tipo = "I";
                }

                if (String.IsNullOrEmpty(tipo))
                {
                    listaexame = repository.ExameAtendimento.Include("Atendimento")
                         .Where(x => x.DataExecucao != null && (x.DataSolicitacao >= inicio && x.DataSolicitacao < final) && (x.Atendimento.Profissional.Unidade.Id.Equals(UnidadeId))).ToList();
                }
                else
                {
                    listaexame = repository.ExameAtendimento.Include("Atendimento")
                         .Where(x => x.DataExecucao != null && (x.DataSolicitacao >= inicio && x.DataSolicitacao < final) && (x.Atendimento.Profissional.Unidade.Id.Equals(UnidadeId)) && (x.CodigoProcedimento.StartsWith(tipo))).ToList();
                }

                foreach (var item in listaexame)
                {
                    //TODO: MODELAGEM NÃO CONTEMPLA LAUDOS
                    EstatisticaProducaoExamesReadModel estatisticaRead = new EstatisticaProducaoExamesReadModel();
                    estatisticaRead.DescricaoExame = item.Exame;
                    estatisticaRead.TotalExamesSolicitados = listaexame.Where(p => (p.Exame.Equals(item.Exame))).Count();
                    estatisticaRead.PercentExamesDiaSolicitacao = Math.Round(Convert.ToDecimal((100 * listaexame.Where(p => (p.Exame.Equals(item.Exame)) && (p.DataSolicitacao != null)).Count()) / listaexame.Count()), 2);

                    string JaExiste = lista.Where(t => t.DescricaoExame.Equals(item.Exame)).Select(j => j.DescricaoExame).FirstOrDefault();

                    if (string.IsNullOrEmpty(JaExiste))
                        lista.Add(estatisticaRead);
                }
                return ListResult.GetListResult(lista.Distinct().ToList(),
                  new string[] { "DescricaoExame", "CodigoExame", "TotalExamesSolicitados", "TotalLaudoRealizados", "PercentExamesDiaSolicitacao", "TotalLaudoEmitidos" });

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public ListResult ListarMapaDiarioLeitos(Guid UnidadeId)
        {
            List<MapaDiarioLeitosReadModel> mapas = new List<MapaDiarioLeitosReadModel>();

            var listaLeitos = repository.Leito.Include("Clinica")
                .Include("Movimentacoes.LeitoAtual.Enfermaria")
                .Include("Enfermaria.Unidade")
                .Include("Movimentacoes.Internacao.Atendimento")
                .Include("Movimentacoes.Internacao.Atendimento.PrimeiroDiagnostico")
                //.Include("Movimentacoes.Internacao.Atendimento.Procedimentos")
                .Include("Movimentacoes.Internacao.Atendimento.Acolhimento.Servico")
                .Include("Movimentacoes.Internacao.Atendimento.Acolhimento.Paciente")
                .Where(u => u.Enfermaria.Unidade.Id == UnidadeId).ToList();

            foreach (var item in listaLeitos)
            {
                try
                {
                    MapaDiarioLeitosReadModel mapa = new MapaDiarioLeitosReadModel();
                    mapa.Enfermaria = item.Enfermaria.Nome;
                    mapa.Leito = item.Nome;
                    //Adicionando Enfermaria.Local até acerto de modelagem com a Clinica da Enfermaria/Leito
                    mapa.Clinica = item.Enfermaria.Local;
                    if (item.Clinica != null)
                        mapa.Clinica = item.Clinica.Descricao;

                    if (item.Movimentacoes.Count > 0 && item.Movimentacoes.OrderByDescending(a => a.DataMovimentacao).FirstOrDefault().Internacao.Atendimento.FimAtendimento == null)
                    {
                        if (item.Ocupado)
                        {
                            DateTime dataInternacao = item.Movimentacoes.OrderByDescending(d => d.DataMovimentacao).FirstOrDefault().Internacao.DataInternacao;
                            string data = dataInternacao.ToString("dd/MM/yyyy");
                            mapa.ClinicaOrigem = item.Movimentacoes.Where(x => x.Internacao.Atendimento.Acolhimento.Servico != null).OrderByDescending(d => d.DataMovimentacao).FirstOrDefault().Internacao.Atendimento.Acolhimento.Servico.Descricao;


                            if (data.Equals("01 / 01 / 0001"))
                                mapa.DataInternacao = null;
                            else
                                mapa.DataInternacao = data;

                            if (item.Movimentacoes.OrderByDescending(d => d.DataMovimentacao).FirstOrDefault().Internacao.Atendimento.PrimeiroDiagnostico != null)
                                mapa.Diagnostico = item.Movimentacoes.OrderByDescending(d => d.DataMovimentacao).FirstOrDefault().Internacao.Atendimento.PrimeiroDiagnostico.CodigoDescricao;

                            if (item.Movimentacoes.OrderByDescending(d => d.DataMovimentacao).FirstOrDefault().Internacao.Atendimento.Acolhimento.Paciente.IdadeEmAnos.HasValue)
                            {
                                mapa.Idade = item.Movimentacoes.OrderByDescending(d => d.DataMovimentacao).FirstOrDefault().Internacao.Atendimento.Acolhimento.Paciente.IdadeEmAnos.Value.ToString();
                            }
                            else
                                mapa.Idade = string.Empty;

                            mapa.Leito = item.Movimentacoes.OrderByDescending(d => d.DataMovimentacao).FirstOrDefault().LeitoAtual.Nome;
                            mapa.Paciente = item.Movimentacoes.OrderByDescending(d => d.DataMovimentacao).FirstOrDefault().Internacao.Atendimento.Acolhimento.Paciente.Nome;

                            int tempoDecorrido = DateTime.Now.Subtract(dataInternacao).Days;

                            try
                            {
                                Guid idAtendimento = item.Movimentacoes.OrderByDescending(d => d.DataMovimentacao).FirstOrDefault().Internacao.Atendimento.Id;

                                var atendimentosProcedimento = repository.AtendimentoProcedimento.Where(x => x.AtendimentoId == idAtendimento);

                                if (atendimentosProcedimento.Count() > 0)
                                {
                                    mapa.Procedimento = atendimentosProcedimento.FirstOrDefault().NomeProcedimento;
                                    mapa.TempoProcedimento = atendimentosProcedimento.FirstOrDefault().QtDiasPermanencia.ToString();
                                    if (!string.IsNullOrEmpty(mapa.TempoProcedimento))
                                    {
                                        if (tempoDecorrido > int.Parse(mapa.TempoProcedimento))
                                            mapa.TP = "X";
                                    }
                                }

                            }
                            catch (Exception ex)
                            {
                                throw ex;
                            }
                            mapa.Prontuario = item.Movimentacoes.OrderByDescending(d => d.DataMovimentacao).FirstOrDefault().Internacao.Atendimento.Acolhimento.CodigoBoletim;
                            mapa.Sexo = item.Movimentacoes.OrderByDescending(d => d.DataMovimentacao).FirstOrDefault().Internacao.Atendimento.Acolhimento.Paciente.Sexo;

                        }
                    }

                    mapas.Add(mapa);
                }
                catch (Exception ex)
                {
                    string gh = ex.Message;
                }

            }

            var mp = mapas.GroupBy(p => p.Clinica).Select(grp => new MapaDiarioLeitosReadModel()
            {
                Clinica = grp.Key,
                ListaRegistros = grp.ToList()
            }).OrderBy(c => c.Clinica).Distinct().ToList();


            return ListResult.GetListResult(mp.Distinct().ToList(),
              new string[] { "Clinica", "ListaRegistros" });

        }

        public ListResult GetAtendimentosByPaciente(Guid pacienteId, Guid unidadeId)
        {
            try
            {
                var lista = repository.Atendimento.Include("PrimeiroDiagnostico").Include("Checkout.MotivoCheckout").Include("Acolhimento.Paciente").Include("Profissional.Unidade")
                                      .Where(x => x.Acolhimento.Paciente.Id == pacienteId && x.FimAtendimento != null && x.Profissional.Unidade.Id == unidadeId)
                                      .OrderBy(x => x.InicioAtendimento).OrderByDescending(x => x.InicioAtendimento)
                                      .Select(x => new ListaBoletinsAtendidosReadModel
                                      {
                                          Id = x.Id,
                                          Diagnostico = x.PrimeiroDiagnostico.Descricao,
                                          InicioAtendimento = x.InicioAtendimento.ToString("dd/MM/yyyy HH:mm:ss", new CultureInfo("en-US")),
                                          FimAtendimento = x.FimAtendimento.Value.ToString("dd/MM/yyyy HH:mm:ss", new CultureInfo("en-US")),
                                          MotivoAlta = x.Checkout.MotivoCheckout.Descricao,
                                          Profissional = x.Profissional.Nome,
                                          AcolhimentoId = x.Acolhimento.Id
                                      }).ToList();

                return ListResult.GetListResult(lista, new string[] { "Id", "Diagnostico", "InicioAtendimento", "FimAtendimento", "MotivoAlta", "Profissional", "AcolhimentoId" });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public AtendimentoReadModel ObterAtendimentoPorPaciente(Guid pacienteId)
        {
            try
            {
                return ViewModelUtils.ToViewModel<HealthCare.Domain.Entities.DbClasses.Atendimento.Atendimento, AtendimentoReadModel>(repository.Atendimento.Include("Acolhimento")
                                                                                                                            .Include("Acolhimento.Classificacao")
                                                                                                                            .Include("Acolhimento.Classificacao.Risco")
                                                                                                                            .Include("Acolhimento.Classificacao.SinaisVitais")
                                                                                                                            .Include("Acolhimento.Paciente")
                                                                                                                            .Include("Checkout")
                                                                                                                            .Include("HipoteseDiagnostica")
                                                                                                                            .Include("PrimeiroDiagnostico")
                                                                                                                            .Include("SegundoDiagnostico")
                                                                                                                            .Where(x => x.Acolhimento.Paciente.Id == pacienteId).FirstOrDefault());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public HistoricoAtendimentoReadModel GetHistoricoAtendimento(Guid atendimentoId)
        {
            try
            {
                HistoricoAtendimentoReadModel historico = new HistoricoAtendimentoReadModel();

                historico.Atendimento = ViewModelUtils.ToViewModel<HealthCare.Domain.Entities.DbClasses.Atendimento.Atendimento, AtendimentoReadModel>(repository.Atendimento.Include("Profissional").Include("Acolhimento.Paciente").Include("Anamnese").Include("PrimeiroDiagnostico").Include("SegundoDiagnostico").Include("Receita").Include("Checkout.MotivoCheckout").FirstOrDefault(x => x.Id == atendimentoId));

                var receitas = from r in repository.ReceitaMedica.Include("Atendimento").Where(x => x.Atendimento.Id == atendimentoId)
                               select new
                               {
                                   Medicamento = r.Medicamento,
                                   Dose = r.Dose,
                                   ViaAdministracao = r.ViaAdministracao,
                                   Duracao = r.Duracao
                               };
                ListResult _receitas = ListResult.GetListResult(receitas.ToList(), new string[] { "Medicamento", "Dose", "ViaAdministracao", "Duracao" });
                historico.Receitas = _receitas.ToListResult();
                historico.Receitas.AllowActions = false;
                if (historico.Receitas.Columns.Count > 0)
                {
                    historico.Receitas.Columns.FirstOrDefault(x => x.Name == "Medicamento").DisplayName = "Medicamento";
                    historico.Receitas.Columns.FirstOrDefault(x => x.Name == "Dose").DisplayName = "Dose";
                    historico.Receitas.Columns.FirstOrDefault(x => x.Name == "ViaAdministracao").DisplayName = "Via de Administração";
                    historico.Receitas.Columns.FirstOrDefault(x => x.Name == "Duracao").DisplayName = "Duração";
                }

                /// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                var prescricoes = from p in ViewModelUtils.ToListViewModel<Prescricao, PrescricaoReadModel>(repository.Prescricao.Include("Atendimento.Profissional").Where(x => x.Atendimento.Id == atendimentoId))
                                  select new
                                  {
                                      Medicamento = p.Medicamento,
                                      Dose = p.Dose,
                                      ViaAdministracao = p.ViaAdministracao,
                                      StatusEnfermagem = (p.StatusEnfermagem == "true" ? "Realizado" : "Não Realizado")
                                  };
                ListResult _prescricoes = ListResult.GetListResult(prescricoes.ToList(), new string[] { "Medicamento", "Dose", "ViaAdministracao", "StatusEnfermagem" });
                historico.Prescricoes = _prescricoes.ToListResult();
                historico.Prescricoes.AllowActions = false;
                if (historico.Prescricoes.Columns.Count > 0)
                {
                    historico.Prescricoes.Columns.FirstOrDefault(x => x.Name == "Medicamento").DisplayName = "Medicamento";
                    historico.Prescricoes.Columns.FirstOrDefault(x => x.Name == "Dose").DisplayName = "Dose";
                    historico.Prescricoes.Columns.FirstOrDefault(x => x.Name == "ViaAdministracao").DisplayName = "Via Administração";
                    historico.Prescricoes.Columns.FirstOrDefault(x => x.Name == "StatusEnfermagem").DisplayName = "Status";
                }

                /// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                var exames = from e in repository.ExameAtendimento.Include("Atendimento").Where(x => x.Atendimento.Id == atendimentoId)
                             select new
                             {
                                 Exame = e.Exame,
                                 DataExecucao = e.DataExecucao != null ? e.DataExecucao.Value.ToString("dd/MM/yyyy HH:mm:ss", new CultureInfo("en-US")) : "",
                                 Realizado = (e.Realizado ? "Realizado" : "Não Realizado"),
                                 CodigoProcedimento = e.CodigoProcedimento
                             };
                ListResult _exames = ListResult.GetListResult(exames.ToList(), new string[] { "Exame", "DataExecucao", "Realizado", "CodigoProcedimento" });
                historico.Exames = _exames.ToListResult();
                historico.Exames.AllowActions = false;
                if (historico.Exames.Columns.Count > 0)
                {
                    historico.Exames.Columns.FirstOrDefault(x => x.Name == "Exame").DisplayName = "Exame";
                    historico.Exames.Columns.FirstOrDefault(x => x.Name == "DataExecucao").DisplayName = "Data de Execução";
                    historico.Exames.Columns.FirstOrDefault(x => x.Name == "Realizado").DisplayName = "Realizado";
                    historico.Exames.Columns.FirstOrDefault(x => x.Name == "CodigoProcedimento").DisplayName = "Código de Procedimento";
                }
                /// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                var procedimentos = from p in repository.AtendimentoProcedimento.Where(x => x.AtendimentoId == atendimentoId).ToList()
                                    select new
                                    {
                                        Procedimento = string.Format("{0} - {1}", p.CodigoProcedimento, p.NomeProcedimento)
                                    };

                ListResult _procedimentos = ListResult.GetListResult(procedimentos.ToList(), new string[] { "Procedimento" });
                historico.Procedimentos = _procedimentos.ToListResult();
                historico.Procedimentos.AllowActions = false;

                if (historico.Procedimentos.Columns.Count > 0)
                    historico.Procedimentos.Columns.FirstOrDefault(x => x.Name == "Procedimento").DisplayName = "Procedimento";

                /// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                var evolucoes = from e in repository.Evolucao.Include("Atemdimento").Where(x => x.Atemdimento.Id == atendimentoId)
                                select new
                                {
                                    DataHora = e.DataHoraEvolucao != null ? e.DataHoraEvolucao.ToString("dd/MM/yyyy HH:mm:ss", new CultureInfo("en-US")) : "",
                                    Descricao = e.Descricao
                                };

                ListResult _evolucoes = ListResult.GetListResult(evolucoes.ToList(), new string[] { "DataHora", "Descricao" });
                historico.Evolucoes = _evolucoes.ToListResult();
                historico.Evolucoes.AllowActions = false;
                int? i = historico.Evolucoes.Rows.Count;
                if (historico.Evolucoes.Columns.Count > 0)
                {
                    historico.Evolucoes.Columns.FirstOrDefault(x => x.Name == "DataHora").DisplayName = "Data da Evolução";
                    historico.Evolucoes.Columns.FirstOrDefault(x => x.Name == "Descricao").DisplayName = "Evolução";
                };


                return historico;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ListResult ListarAtendimentosFinalizados(Guid idUnidade, Guid profissionalId, DateTime? dataInicio, DateTime? datafim)
        {
            try
            {
                var lista = (from at in repository.Atendimento.Include("Acolhimento")
                                                                      .Include("Acolhimento.Paciente")
                                                                      .Include("Acolhimento.Paciente.Unidade")
                                                                      .Include("Checkout")
                                                                      .Include("Profissional")
                                                                      .Include("Checkout.MotivoCheckout")
                                                                      .Include("Internacoes")
                                                                      .Where(x => x.Acolhimento.Paciente.Unidade.Id == idUnidade && x.Checkout != null).WhereIf(dataInicio.HasValue, x => x.FimAtendimento >= dataInicio).WhereIf(datafim.HasValue, x => x.FimAtendimento <= datafim).ToList()
                             select new AtendimentoFechadoViewModel
                             {
                                 Id = at.Id,
                                 ProfissionalId = at.Profissional.Id,
                                 Profissional = at.Profissional.Nome,
                                 InicioAtendimento = at.InicioAtendimento.ToString("dd/MM/yyyy HH:mm:ss", new CultureInfo("en-US")),
                                 Nome = at.Acolhimento.Paciente.Nome,
                                 Nascimento = at.Acolhimento.Paciente.DataNascimento,
                                 Idade = CalulaIdade(at.Acolhimento.Paciente.DataNascimento),
                                 Sexo = at.Acolhimento.Paciente.Sexo,
                                 Checkout = at.Checkout != null && at.Checkout.MotivoCheckout != null ? at.Checkout.MotivoCheckout.Descricao : string.Empty,
                                 FimAtendimento = at.FimAtendimento != null ? at.FimAtendimento.Value.ToString() : string.Empty,
                                 AcolhimentoId = at.Acolhimento.Id,
                                 Observacao = at.Checkout != null ? at.Checkout.Observacao : string.Empty,
                                 Boletim = at.Acolhimento.CodigoBoletim
                             }).OrderBy(x => x.InicioAtendimento).ToList();


                var expectionListServicoSocial = repository.ServicoSocial.Include("Acolhimento").Where(x => x.Acolhimento.Paciente.Unidade.Id == idUnidade && x.Checkout == null
                                                                         && x.FimAtendimento >= DateTime.Today).Select(x => x.Id);

                lista = lista.Where(x => !expectionListServicoSocial.Contains(x.Id)).ToList();

                ListResult result = ListResult.GetListResult(lista, new string[] { "Id", "InicioAtendimento", "Nome", "Sexo", "Idade", "Checkout", "FimAtendimento", "AcolhimentoId", "Observacao", "Boletim", "ProfissionalId", "Profissional" });
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public ListResult ListarAtendimentosFinalizados(Guid idUnidade, string filtro)
        {
            try
            {
                long filtroPorCodigo = 0;

                IEnumerable<Domain.Entities.DbClasses.Atendimento.Atendimento> atendimentos = new List<Domain.Entities.DbClasses.Atendimento.Atendimento>();

                if (long.TryParse(filtro, out filtroPorCodigo))
                {
                    atendimentos = repository.Atendimento.Include("Acolhimento")
                                                         .Include("Acolhimento.Paciente")
                                                         .Include("Acolhimento.Paciente.Unidade")
                                                         .Include("Checkout")
                                                         .Include("Profissional")
                                                         .Include("Checkout.MotivoCheckout")
                                                         .Include("Internacoes")
                                                         .Where(x => x.Acolhimento.Paciente.Unidade.Id == idUnidade && x.Checkout != null && x.Acolhimento.CodigoBoletim == filtro);
                }
                else
                {
                    atendimentos = repository.Atendimento.Include("Acolhimento")
                                     .Include("Acolhimento.Paciente")
                                     .Include("Acolhimento.Paciente.Unidade")
                                     .Include("Checkout")
                                     .Include("Profissional")
                                     .Include("Checkout.MotivoCheckout")
                                     .Include("Internacoes")
                                     .Where(x => x.Acolhimento.Paciente.Unidade.Id == idUnidade && x.Checkout != null && x.Acolhimento.Paciente.Nome.Contains(filtro));
                }


                var lista = (from at in atendimentos
                             select new AtendimentoFechadoViewModel
                             {
                                 Id = at.Id,
                                 ProfissionalId = at.Profissional.Id,
                                 Profissional = at.Profissional.Nome,
                                 InicioAtendimento = at.InicioAtendimento.ToString("dd/MM/yyyy HH:mm:ss", new CultureInfo("en-US")),
                                 Nome = at.Acolhimento.Paciente.Nome,
                                 Nascimento = at.Acolhimento.Paciente.DataNascimento,
                                 Idade = CalulaIdade(at.Acolhimento.Paciente.DataNascimento),
                                 Sexo = at.Acolhimento.Paciente.Sexo,
                                 Checkout = at.Checkout != null && at.Checkout.MotivoCheckout != null ? at.Checkout.MotivoCheckout.Descricao : string.Empty,
                                 FimAtendimento = at.FimAtendimento != null ? at.FimAtendimento.Value.ToString() : string.Empty,
                                 AcolhimentoId = at.Acolhimento.Id,
                                 Observacao = at.Checkout != null ? at.Checkout.Observacao : string.Empty,
                                 Boletim = at.Acolhimento.CodigoBoletim
                             }).OrderBy(x => x.Nome).OrderByDescending(x => x.InicioAtendimento).ToList();


                var expectionListServicoSocial = repository.ServicoSocial.Include("Acolhimento").Where(x => x.Acolhimento.Paciente.Unidade.Id == idUnidade && x.Checkout == null
                                                                         && x.FimAtendimento >= DateTime.Today).Select(x => x.Id);

                lista = lista.Where(x => !expectionListServicoSocial.Contains(x.Id)).ToList();

                ListResult result = ListResult.GetListResult(lista, new string[] { "Id", "InicioAtendimento", "Nome", "Sexo", "Idade", "Checkout", "FimAtendimento", "AcolhimentoId", "Observacao", "Boletim", "ProfissionalId", "Profissional" });
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}

﻿
using AutoMapper;
using HealtCare.Infra.Common.Utils;
using HealthCare.Domain.Entities.DbClasses.Atendimento;
using HealthCare.Infra.CQRS.Read.Interfaces.Atendimento;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.Data.Context;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HealthCare.Infra.CQRS.Read.Implementation
{
    public class LiquidosAdministradosDto : ILiquidosAdministradosDto
    {
        private HealthCareDbContext repository;
        private IMapper mapper;
        public LiquidosAdministradosDto(HealthCareDbContext contextFactory, IMapper mapper)
        {
            this.repository = contextFactory;
            this.mapper = mapper;
            ViewModelUtils.mapper = mapper;
        }

        public List<LiquidosAdministradosReadModel> ListarLiquidosAdministrados()
        {
            try
            {
                return ViewModelUtils.ToListViewModel<LiquidosAdministrados, LiquidosAdministradosReadModel>(repository.Query<LiquidosAdministrados>()).ToList();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}

﻿using AutoMapper;
using HealtCare.Infra.Common.Utils;
using HealthCare.Domain.Entities.DbClasses.Atendimento;
using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Read.Interfaces.Atendimento;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Collections.Generic;

namespace HealthCare.Infra.CQRS.Read.Implementation
{
    public class EvolucaoDto : IEvolucaoDto
    {
        private HealthCareDbContext repository;
        private IMapper mapper;

        public EvolucaoDto(HealthCareDbContext contextFactory, IMapper mapper)
        {
            this.repository = contextFactory;
            this.mapper = mapper;
            ViewModelUtils.mapper = mapper;
        }

        public EvolucaoReadModel GetById(Guid id)
        {
            try
            {
                var evolucao = (from e in repository.Evolucao.Include("EvolucaoProcedimentos.Procedimento").Include("Atemdimento.Acolhimento.Profissional").Include("Atemdimento.Acolhimento.Paciente")
                                                              .Include("PrimeiroDiagnostico").Include("SegundoDiagnostico")
                                                             .Where(x => x.Id == id).ToList()
                                select new EvolucaoReadModel()
                                {
                                    Id = e.Id,
                                    DataHoraEvolucao = e.DataHoraEvolucao.ToString(),
                                    Descricao = e.Descricao,
                                    Procedimentos = e.EvolucaoProcedimentos.Count > 0 ? e.EvolucaoProcedimentos.Select(x => x.Procedimento.CO_PROCEDIMENTO + " - " + x.Procedimento.NO_PROCEDIMENTO) : new List<string>(),
                                    Quantidade = e.Quantidade,
                                    Profissional = ViewModelUtils.ToModel<Profissional, ProfissionalReadModel>(e.Medico).Nome,
                                    Atendimento = ViewModelUtils.ToModel<HealthCare.Domain.Entities.DbClasses.Atendimento.Atendimento, AtendimentoReadModel>(e.Atemdimento),
                                    PrimeiroDiagnostico = ViewModelUtils.ToModel<CID, CIDReadViewModel>(e.PrimeiroDiagnostico),
                                    SegundoDiagnostico = ViewModelUtils.ToModel<CID, CIDReadViewModel>(e.SegundoDiagnostico)
                                }).FirstOrDefault();

                return evolucao;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }



        public ListResult ListarEvolucaoPorAtendimento(Guid id)
        {
            try
            {
                var evolucoes = (from e in repository.Evolucao.Include("EvolucaoProcedimentos.Procedimento").Include("Medico")
                                                              .Include("PrimeiroDiagnostico")
                                                              .Include("Atemdimento.Acolhimento.Paciente")
                                                              .Where(x => x.Atemdimento.Id == id).ToList()
                                 select new EvolucaoReadModel()
                                 {
                                     DataHoraEvolucao = e.DataHoraEvolucao.ToString("dd/MM/yyyy HH:mm"),
                                     Atendimento = ViewModelUtils.ToModel<HealthCare.Domain.Entities.DbClasses.Atendimento.Atendimento, AtendimentoReadModel>(e.Atemdimento),
                                     Descricao = e.Descricao,
                                     Id = e.Id,
                                     Procedimento = ObtemProcedimentos(e.EvolucaoProcedimentos),
                                     Profissional = ViewModelUtils.ToModel<Profissional, ProfissionalReadModel>(e.Medico).Nome,
                                     DiagnosticoUm = e.PrimeiroDiagnostico != null ? e.PrimeiroDiagnostico.Descricao : ""
                                 }).ToList();


                return ListResult.GetListResult(evolucoes, new string[] { "Id", "Descricao", "Procedimento", "DataHoraEvolucao", "Profissional", "DiagnosticoUm" });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string ObtemProcedimentos(List<EvolucaoProcedimento> evolucaoProcedimentos)
        {
            if (evolucaoProcedimentos.Count > 0)
                return string.Join(";", evolucaoProcedimentos.Select(x => x.Procedimento.CO_PROCEDIMENTO + " - " + x.Procedimento.NO_PROCEDIMENTO).ToArray());
            else
                return string.Empty;
        }

        public ListResult ListarEvolucaoMedica(DateTime inicio, DateTime fim, Guid UnidadeId)
        {
            try
            {
                fim = fim.AddDays(1);

                var listaEvolucao = repository.Evolucao.Include("Atemdimento.Acolhimento.Paciente")
                                                            .Include("Atemdimento.Acolhimento.Servico")
                                                            .Include("Medico").Include("PrimeiroDiagnostico")
                                                            .Include("EvolucaoProcedimentos.Procedimento")
                                                            .Where(x => x.Atemdimento.Profissional.Unidade.Id == UnidadeId
                                                                     && x.DataHoraEvolucao >= inicio && x.DataHoraEvolucao <= fim).ToList();

                var evolucao = from e in listaEvolucao
                               group e by e.DataHoraEvolucao.ToString("dd/MM/yyyy") into data
                               select new RelatorioEvolucaoReadModel
                               {
                                   Data = data.Key.ToString(),
                                   Lista = from p in data
                                           group p by p.Medico.Nome into evolucoes
                                           select new RelatorioEvolucaoReadModel
                                           {
                                               Profissional = evolucoes.Key.ToString(),
                                               Lista = (from e in evolucoes
                                                        select new RelatorioEvolucaoReadModel
                                                        {
                                                            Boletim = e.Atemdimento.Acolhimento.CodigoBoletim,
                                                            CID = e.PrimeiroDiagnostico != null ? e.PrimeiroDiagnostico.Descricao : "",
                                                            Clinica = e.Atemdimento.Acolhimento.Servico.Descricao,
                                                            Paciente = ViewModelUtils.ToViewModel<Paciente, PacienteReadModel>(e.Atemdimento.Acolhimento.Paciente),
                                                            Data = e.DataHoraEvolucao.ToString("dd/MM/yyyy"),
                                                            HoraAtendimento = e.DataHoraEvolucao.ToString("HH:mm"),
                                                            Procedimentos = ViewModelUtils.ToListViewModel<Procedimento, ProcedimentoReadModel>(listaEvolucao.FirstOrDefault(a => a.Id == e.Id).EvolucaoProcedimentos.Select(p => p.Procedimento)),
                                                        }).OrderBy(x => x.HoraAtendimento)
                                           }
                               };

                return ListResult.GetListResult(evolucao.ToList(), new string[] { "Profissional", "Lista", "Boletim", "CID", "Clinica", "Paciente", "Data", "HoraAtendimento", "Procedimentos" });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

}

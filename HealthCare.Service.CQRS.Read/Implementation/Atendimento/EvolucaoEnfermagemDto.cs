﻿
using AutoMapper;
using HealtCare.Infra.Common.Utils;
using HealthCare.Domain.Entities.DbClasses.Atendimento;
using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Read.Interfaces.Atendimento;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace HealthCare.Infra.CQRS.Read.Implementation
{
    public class EvolucaoEnfermagemDto : IEvolucaoEnfermagemDto
    {
        private HealthCareDbContext repository;
        private IMapper mapper;
        public EvolucaoEnfermagemDto(HealthCareDbContext contextFactory, IMapper mapper)
        {
            this.repository = contextFactory;
            this.mapper = mapper;
            ViewModelUtils.mapper = mapper;
        }

        public EvolucaoEnfermagemReadModel GetEvolucaoEnfermagemById(Guid id)
        {
            try
            {
                return ViewModelUtils.ToViewModel<EvolucaoEnfermagem, EvolucaoEnfermagemReadModel>(repository.EvolucaoEnfermagem.Include("LiquidosAdministradosEnfermagem").Include("LiquidosEliminadosEnfermagem").Include("ProcedimentosEnfermagem")
                                                    .Include("MovimentacaoLeito.Internacao.Atendimento.Acolhimento.Paciente")
                                                    .FirstOrDefault(x => x.Id == id));
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public ListResult ListaHistoricoEnfermagem(Guid internacaoId)
        {
            try
            {
                var evolucoes = (from e in repository.EvolucaoEnfermagem.Include("LiquidosAdministrados").Include("LiquidosEliminados").Include("ProcedimentosEnfermagem")
                                                    .Include("MovimentacaoLeito.Internacao.Atendimento.Acolhimento.Paciente")
                                                    .Include("MovimentacaoLeito.Internacao.Atendimento.Profissional")
                                                    .Where(x => x.MovimentacaoLeito.Internacao.Id == internacaoId).ToList()
                                 select new EvolucaoEnfermagemReadModel
                                 {
                                     HoraEvolucao = e.HoraEvolucao,
                                     Evolucao = e.Evolucao,
                                     Profissional = ViewModelUtils.ToViewModel<Profissional, ProfissionalReadModel>(e.MovimentacaoLeito.Internacao.Atendimento.Acolhimento.Profissional)
                                 }).ToList();


                return ListResult.GetListResult(evolucoes, new string[] { "HoraEvolucao", "Evolucao", "NomeProfissional" });
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public ListResult ListaLiquidosAdministrados(Guid evolucaoEnfermagemId)
        {
            try
            {
                var liquidosAdministrados = (from l in repository.LiquidosAdministradosEnfermagem.Include("EvolucaoEnfermagem").Include("LiquidosAdministrados")
                                             where l.EvolucaoEnfermagem.Id == evolucaoEnfermagemId
                                             select new LiquidosAdministradosEnfermagemReadModel()
                                             {
                                                 Id = l.Id,
                                                 Descricao = l.LiquidosAdministrados.Descricao
                                             }).ToList();

                return ListResult.GetListResult(liquidosAdministrados, new string[] { "Id", "Descricao" });
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public ListResult ListaLiquidosEliminados(Guid evolucaoEnfermagemId)
        {
            try
            {
                var liquidosEliminados = (from l in repository.LiquidosEliminadosEnfermagem.Include("EvolucaoEnfermagem").Include("LiquidosAdministrados")
                                          where l.evolucaoEnfermagem.Id == evolucaoEnfermagemId
                                          select new LiquidosAdministradosEnfermagemReadModel()
                                          {
                                              Id = l.Id,
                                              Descricao = l.LiquidosEliminados.Descricao
                                          }).ToList();

                return ListResult.GetListResult(liquidosEliminados, new string[] { "Id", "Descricao" });
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public ListResult ListaProcedimentosPorEvolucao(Guid evolucaoEnfermagemId)
        {
            try
            {
                var liquidosEliminados = (from l in repository.ProcedimentosEnfermagem.Include("EvolucaoEnfermagem").Include("Procedimentos")
                                          where l.Evolucao.Id == evolucaoEnfermagemId
                                          select new ProcedimentosEnfermagemReadModel()
                                          {
                                              Id = l.Id,
                                              Observacao = l.Observacao,
                                              DescricaoProcedimento = l.Procedimento.NO_PROCEDIMENTO,
                                              Quantidade = l.Quantidade
                                          }).ToList();

                return ListResult.GetListResult(liquidosEliminados, new string[] { "Id", "DescricaoProcedimento", "Observacao", "Quantidade" });
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}

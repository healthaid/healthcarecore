﻿using AutoMapper;
using HealtCare.Infra.Common.Utils;
using HealthCare.Domain.Entities.DbClasses.Atendimento;
using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.Data.Context;
using System;
using System.Linq;


namespace HealthCare.Infra.CQRS.Read.Implementation.Atendimento
{
    public class RiscoDto : IRiscoDao
    {
        private Func<HealthCareDbContext> contextFactory;
        private IMapper mapper;
        public RiscoDto(Func<HealthCareDbContext> contextFactory, IMapper mapper)
        {
            this.contextFactory = contextFactory;
            this.mapper = mapper;
            ViewModelUtils.mapper = mapper;
        }

        public ListResult ListarRiscos()
        {
            using (var repository = contextFactory.Invoke())
            {
                var lista = ViewModelUtils.ToListViewModel<Risco, RiscoReadModel>(repository.Query<Risco>().OrderBy(x => x.Prioridade)).ToList();
                ListResult result = ListResult.GetListResult(lista, new string[] { "Id", "Descricao" });
                return result;
            }
        }
    }
}

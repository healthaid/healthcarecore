﻿using AutoMapper;
using HealtCare.Infra.Common.Utils;
using HealthCare.Domain.Entities.DbClasses.Atendimento;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.Data.Context;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HealthCare.Infra.CQRS.Read.Implementation
{
    public class UnidadeMedicamentoDto : IUnidadeMedicamentoDto
    {
        private HealthCareDbContext repository;
        private IMapper mapper;
        public UnidadeMedicamentoDto(HealthCareDbContext contextFactory, IMapper mapper)
        {
            this.repository = contextFactory;
            this.mapper = mapper;
            ViewModelUtils.mapper = mapper;
        }

        public IList<UnidadeMedicamentoReadModel> ListarTodosUnidadeMedicamentos()
        {
           try
            {
                return ViewModelUtils.ToListViewModel<UnidadeMedida, UnidadeMedicamentoReadModel>(repository.UnidadeMedida.ToList()).ToList();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public IList<UnidadeMedicamentoReadModel> ListarTodosUnidadeMedicamentos(string texto)
        {
            try
            {
                return ViewModelUtils.ToListViewModel<UnidadeMedida, UnidadeMedicamentoReadModel>(repository.Query<UnidadeMedida>()
                                                                                        .Where(x => x.Nome.Contains(texto)).Take(50)).ToList();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }


    }
}

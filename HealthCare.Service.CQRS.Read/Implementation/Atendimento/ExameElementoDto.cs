﻿using AutoMapper;
using HealtCare.Infra.Common.Utils;
using HealthCare.Domain.Entities.DbClasses.Atendimento;
using HealthCare.Infra.CQRS.Read.Interfaces.Atendimento;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.Data.Context;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HealthCare.Infra.CQRS.Read.Implementation
{
    public class ElementoExameDto : IElementoExameDAO
    {
        private HealthCareDbContext repository;
        private IMapper mapper;

        public ElementoExameDto(HealthCareDbContext contextFactory, IMapper mapper)
        {
            this.repository = contextFactory;
            this.mapper = mapper;
            ViewModelUtils.mapper = mapper;
        }

        public IList<ElementoExameReadViewModel> ListarPorExame(Guid exameId)
        {
            try
            {
                IList<ElementoExame> elementosExame = repository.Query<ElementoExame>("Exame").Where(x => x.Exame.Id == exameId).ToList();
                return ViewModelUtils.ToListViewModel<ElementoExame, ElementoExameReadViewModel>(elementosExame).ToList();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }


        public IQueryable<ElementoExameReadViewModel> ListarElementosExame(Guid exameId)
        {
            throw new NotImplementedException();
        }
    }
}

﻿using AutoMapper;
using HealtCare.Infra.Common.Utils;
using HealthCare.Domain.Entities.DbClasses.Atendimento;
using HealthCare.Infra.CQRS.Read.Interfaces.Atendimento;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.Data.Context;
using System;
using System.Collections.Generic;
using System.Linq;


namespace HealthCare.Infra.CQRS.Read.Implementation.Atendimento
{
    public class ExameDto : IExameDto
    {
        private HealthCareDbContext repository;
        private IMapper mapper;

        public ExameDto(HealthCareDbContext contextFactory, IMapper mapper)
        {
            this.repository = contextFactory;
            this.mapper = mapper;
            ViewModelUtils.mapper = mapper;
        }

        public ExameReadModel GetExameById(Guid id)
        {
            try
            {
                return ViewModelUtils.ToModel<Exame, ExameReadModel>(repository.Exame.FirstOrDefault(x => x.Id == id));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<ExameReadModel> ListarExames(string descricao, string tipo)
        {

            try
            {
                switch (tipo)
                {
                    case "APOIO":
                        var listApoio = (from a in repository.Exame
                                         where a.Descricao.Contains(descricao) && a.Classificacao == "D"
                                         select a).Distinct().ToList();
                        return ViewModelUtils.ToListViewModel<Exame, ExameReadModel>(listApoio).ToList();
                    case "LABORATORIAL":
                        var listLaboratorial = (from a in repository.Exame
                                                where a.Descricao.Contains(descricao) && a.Classificacao == "L"
                                                select a).Distinct().ToList();
                        return ViewModelUtils.ToListViewModel<Exame, ExameReadModel>(listLaboratorial).ToList();
                    case "IMAGEM":
                        var listImagem = (from a in repository.Exame
                                          where a.Descricao.Contains(descricao) && a.Classificacao == "I"
                                          select a).Distinct().ToList();
                        return ViewModelUtils.ToListViewModel<Exame, ExameReadModel>(listImagem).ToList();
                    default:
                        return ViewModelUtils.ToListViewModel<Exame, ExameReadModel>(repository.Exame.Where(x => x.Descricao.Contains(descricao))).ToList();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

﻿using AutoMapper;
using HealtCare.Infra.Common.Utils;
using HealthCare.Domain.Entities.DbClasses.Atendimento;
using HealthCare.Infra.CQRS.Read.Interfaces.Atendimento;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.Data.Context;
using System;
using System.Collections.Generic;
using System.Linq;


namespace HealthCare.Infra.CQRS.Read.Implementation.Atendimento
{
    public class TipoExameDto : ITipoExameDto
    {
        private HealthCareDbContext repository;
        private IMapper mapper;
        public TipoExameDto(HealthCareDbContext contextFactory, IMapper mapper)
        {
            this.repository = contextFactory;
            this.mapper = mapper;
            ViewModelUtils.mapper = mapper;
        }

        public TipoExameReadModel ObterTipoExamePorId(Guid id)
        {
            try
            {
                return ViewModelUtils.ToModel<TipoExame, TipoExameReadModel>(repository.TipoExame.FirstOrDefault(x => x.Id == id));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public TipoExameReadModel GetTipoExameById(Guid id)
        {
            try
            {
                return ViewModelUtils.ToModel<TipoExame, TipoExameReadModel>(repository.TipoExame.FirstOrDefault(x => x.Id == id));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<TipoExameReadModel> ListarTipoExames(string descricao)
        {
            try
            {
                return ViewModelUtils.ToListViewModel<TipoExame, TipoExameReadModel>(repository.TipoExame.Where(x => x.Descricao.Contains(descricao))).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

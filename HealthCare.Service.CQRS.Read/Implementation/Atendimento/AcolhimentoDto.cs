﻿using AutoMapper;
using Dapper;
using HealtCare.Infra.Common.Utils;
using HealthCare.Domain.Entities.DbClasses.Atendimento;
using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Read.Interfaces.Atendimento;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;


namespace HealthCare.Infra.CQRS.Read.Implementation.Atendimento
{
    public class AcolhimentoDto : IAcolhimentoDTO
    {
        private HealthCareDbContext contextFactory;
        private readonly IMapper _mapper;
        private readonly ILogger<AtendimentoDto> logger;

        public AcolhimentoDto(HealthCareDbContext contextFactory, IMapper mapper, ILogger<AtendimentoDto> logger)
        {
            this.contextFactory = contextFactory;
            this._mapper = mapper;
            ViewModelUtils.mapper = _mapper;
            this.logger = logger;
        }

        public ListResult FilaAcolhimento(Guid unidadeId)
        {
            try
            {
                logger.LogInformation("Iniciando consulta para a fila de acolhimento");
                DateTime dtAnterior = DateTime.Today.AddDays(-1);

                string sql = @"SELECT *
                                 FROM Acolhimento Ac
                           INNER JOIN Paciente P
                                   ON Ac.PacienteId = P.Id
                            LEFT JOIN ClassificacaoDeRisco CR
                                   ON Ac.Id = CR.AcolhimentoId
                            LEFT JOIN Risco R
                                   ON R.Id = CR.RiscoId
                           INNER JOIN Unidade U
                                   ON P.UnidadeId = U.Id
                           INNER JOIN Profissional PRO
                                   ON PRO.Id = Ac.ProfissionalId
                            LEFT JOIN Servico S
                                   ON Ac.ServicoId = S.Id
                                WHERE Ac.Id NOT IN (SELECT AcolhimentoId from Atendimento)
                                  AND P.PendenteCadastro = @Pendente
                                  AND U.Id = @unidadeId
                                  AND Ac.DataHora >= @dataHora
                             ORDER BY Ac.DataHora;";

                IEnumerable<Acolhimento> lista = contextFactory.DapperHelper.Query<Acolhimento, Paciente, ClassificacaoDeRisco, Risco, Unidade, Profissional, Servico, Acolhimento>(
                    sql,
                    (acolhimento, paciente, classificacao, risco, unidade, profissional, servico) =>
                    {
                        acolhimento.Servico = servico;
                        if (classificacao != null)
                        {
                            acolhimento.Classificacao = new List<ClassificacaoDeRisco>();
                            acolhimento.Classificacao.Add(classificacao);
                            acolhimento.Classificacao.FirstOrDefault().Risco = risco;
                        }
                        acolhimento.Profissional = profissional;
                        acolhimento.Profissional.Unidade = unidade;
                        acolhimento.Paciente = paciente;

                        return acolhimento;
                    }, new { Pendente = 1, unidadeId = unidadeId.ToString(), dataHora = dtAnterior });

                List<FilaAcolhimentoReadModel> listaModel = (from a in lista
                                                             select new FilaAcolhimentoReadModel
                                                             {
                                                                 Id = a.Id,
                                                                 HoraAcolhimento = a.DataHora.ToString("dd/MM/yyyy HH:mm"),
                                                                 NomePaciente = a.Paciente.Nome,
                                                                 Sexo = a.Paciente.Sexo != null ? (a.Paciente.Sexo == "F" ? "Feminino" : "Masculino") : "",
                                                                 PacienteId = a.Paciente.Id,
                                                                 Risco = a.Classificacao != null ? a.Classificacao.FirstOrDefault().Risco != null ? a.Classificacao.FirstOrDefault().Risco.Descricao : "" : "",
                                                                 Prioridade = a.Prioridade ? "Sim" : "Não",
                                                                 Servico = a.Servico != null ? a.Servico.Descricao : string.Empty,
                                                                 Boletim = a.CodigoBoletim
                                                             }).OrderByDescending(x => x.Prioridade).ToList();

                logger.LogInformation("Terminada a consulta para a fila de acolhimento");
                return ListResult.GetListResult(listaModel.ToList(), new string[] { "Id", "HoraAcolhimento", "NomePaciente", "Sexo", "Idade", "PacienteId", "Risco", "Servico", "Boletim", "Prioridade" });

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public PacienteReadModel ObterPacientePorAcolhimento(Guid acolhimentoId)
        {
            try
            {
                return ViewModelUtils.ToModel<Paciente, PacienteReadModel>(contextFactory.Acolhimento.Include("Paciente").FirstOrDefault(x => x.Id == acolhimentoId).Paciente);
            }
            catch (Exception)
            {
                throw;
            }

        }

        private string CalulaIdade(DateTime? dataNascimento)
        {
            string idade = string.Empty;
            if (dataNascimento.HasValue)
            {
                // Calculate the age.
                var idadeCalculada = DateTime.Today.Year - dataNascimento.Value.Year;

                if (dataNascimento > DateTime.Today.AddYears(-idadeCalculada))
                    idadeCalculada--;

                if (idadeCalculada < 1)
                    idade = "Menos de 1 ano.";
                else if (idadeCalculada == 1)
                    idade = string.Format("{0} ano.", idadeCalculada.ToString());
                else
                    idade = string.Format("{0} ano(s).", idadeCalculada.ToString());
            }

            return idade;
        }

        public AcolhimentoReadModel ObterAcolhimentoPorId(Guid acolhimentoId)
        {
            return ViewModelUtils.ToModel<Acolhimento, AcolhimentoReadModel>(contextFactory.Acolhimento.Include("Servico").Include("Paciente").FirstOrDefault(x => x.Id == acolhimentoId));
        }

        public AcolhimentoReadModel GetAcolhimentoWithClassificacaoRico(Guid acolhimentoId)
        {

            var acolhimento = ViewModelUtils.ToModel<Acolhimento, AcolhimentoReadModel>(contextFactory.Acolhimento.Include("Paciente").Include("Servico").Include("Classificacao").Include("Classificacao.Risco").Include("Classificacao.SinaisVitais").FirstOrDefault(x => x.Id == acolhimentoId));
            return acolhimento;
        }

        public ListResult GetAcolhimentosWithoutClassificacaoRisco(Guid unidadeId)
        {
            try
            {
                DateTime dtAnterior = DateTime.Today.AddDays(-1);

                var lista = (from a in contextFactory.Acolhimento.Include("Servico").Include("Atendimento").Include("Paciente").Include("Paciente.Unidade").Include("Profissional").Include("Classificacao").Include("Classificacao.Risco").ToList()
                           .Where(x => x.Atendimento.Count <= 0 && x.DataHora >= DateTime.Today && x.DataHora > dtAnterior && x.Profissional.Unidade.Id == unidadeId)
                           .OrderBy(x => x.DataHora).OrderBy(x => x.Paciente.IdadeEmAnos)
                             select new FilaAcolhimentoReadModel
                             {
                                 Id = a.Id,
                                 HoraAcolhimento = a.DataHora.ToString("dd/MM/yyyy HH:mm:ss", new CultureInfo("en-US")),
                                 NomePaciente = a.Paciente.Nome,
                                 Sexo = a.Paciente.Sexo != null ? (a.Paciente.Sexo == "F" ? "Feminino" : "Masculino") : "",
                                 //Idade = CalulaIdade(a.Paciente.DataNascimento),
                                 PacienteId = a.Paciente.Id,
                                 Risco = a.Classificacao.FirstOrDefault() != null ? a.Classificacao.FirstOrDefault().Risco != null ? a.Classificacao.FirstOrDefault().Risco.Descricao : "" : "",
                                 Prioridade = a.Prioridade ? "Sim" : "Não",
                                 Servico = a.Servico != null ? a.Servico.Descricao : string.Empty
                             }).OrderByDescending(x => x.Prioridade).ToList();

                return ListResult.GetListResult(lista, new string[] { "Id", "HoraAcolhimento", "NomePaciente", "Sexo", "Idade", "PacienteId", "Risco", "Servico", "Prioridade" });

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ListResult GetClassificacaoRiscoList(Guid unidadeId)
        {
            try
            {
                logger.LogInformation("Iniciando consulta de fila de classificação de risco");
                DateTime dtAnterior = DateTime.Today.AddDays(-1);

                string sql = @"SELECT *
                                 FROM Acolhimento Ac
                           INNER JOIN Paciente P
                                   ON Ac.PacienteId = P.Id
                           INNER JOIN Unidade U
                                   ON P.UnidadeId = U.Id
                           INNER JOIN Profissional PRO
                                   ON PRO.Id = Ac.ProfissionalId
                            LEFT JOIN Servico S
                                   ON Ac.ServicoId = S.Id
                                WHERE Ac.Id NOT IN (SELECT AcolhimentoId from ClassificacaoDeRisco)
                                  AND Ac.Id NOT IN (SELECT AcolhimentoId from Atendimento)
                                  AND P.PendenteCadastro = false
                                  AND U.Id = @unidadeId
                                  AND Ac.DataHora >= @dataHora
                             ORDER BY Ac.Prioridade, Ac.DataHora;";


                IEnumerable <Acolhimento> lista = contextFactory.DapperHelper.Query<Acolhimento, Paciente, Unidade, Profissional, Servico, Acolhimento>(
                    sql,
                    (acolhimento, paciente, unidade, profissional, servico) =>
                    {
                        acolhimento.Servico = servico;
                        acolhimento.Profissional = profissional;
                        acolhimento.Profissional.Unidade = unidade;
                        acolhimento.Paciente = paciente;
                        return acolhimento;
                    }, new { Pendente = 1, unidadeId = unidadeId.ToString(), dataHora = dtAnterior });

                List<FilaAcolhimentoReadModel> FilaParaClassificacao = (from a in lista
                                                                       select new FilaAcolhimentoReadModel
                                                                       {
                                                                           Id = a.Id,
                                                                           NomePaciente = a.Paciente.Nome,
                                                                           //Idade = a.Paciente.IdadePorExtenso,
                                                                           Prioridade = a.Prioridade ? "Sim" : "Não",
                                                                           HoraAcolhimento = a.DataHora.ToString("dd/MM/yyyy HH:mm:ss", new CultureInfo("en-US")),
                                                                           Servico = a.Servico != null ? a.Servico.Descricao : string.Empty,
                                                                           PacienteId = a.Paciente.Id
                                                                       }).ToList();

                logger.LogInformation("Terminada consulta de fila de classificação de risco");
                return ListResult.GetListResult(FilaParaClassificacao, new string[] { "Id", "HoraAcolhimento", "NomePaciente", "Sexo", "Idade", "PacienteId", "Risco", "Servico", "Prioridade" });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public HeaderAtendimentoReadModel ObterInformacoesHeaderAtendimento(Guid acolhimentoId)
        {
            var header = (from acolhimento in contextFactory.Acolhimento.Include("Paciente").Include("Atendimento").Where(x => x.Id == acolhimentoId).ToList()
                          select new HeaderAtendimentoReadModel()
                          {
                              CodigoBoletim = acolhimento.CodigoBoletim,
                              CodigoPaciente = acolhimento.Paciente.CodigoPaciente,
                              Nome = acolhimento.Paciente.Nome,
                              NomeMae = acolhimento.Paciente.NomeMae,
                              DataNascimento = acolhimento.Paciente.DataNascimento != null ? acolhimento.Paciente.DataNascimento.ToString() : string.Empty,
                              IdadePorExtenso = acolhimento.Paciente.IdadePorExtenso,
                              InicioAtendimento = acolhimento.Atendimento.FirstOrDefault() != null ? acolhimento.Atendimento.FirstOrDefault().InicioAtendimento.ToString("yyyy-MM-dd hh:mm:ss tt", CultureInfo.InvariantCulture) : string.Empty,
                              AtendimentoId = acolhimento.Atendimento.FirstOrDefault() != null ? acolhimento.Atendimento.FirstOrDefault().Id : Guid.Empty
                          }).FirstOrDefault();

            return header;
        }
    }
}

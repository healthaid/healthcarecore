﻿using AutoMapper;
using HealtCare.Infra.Common.Utils;
using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Read.Interfaces.Atendimento;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Globalization;
using System.Linq;

namespace HealthCare.Infra.CQRS.Read.Implementation.ServicoSocial
{
    public class ServicoSocialDto : IServicoSocialDto
    {
        private HealthCareDbContext repository;
        private IMapper mapper;
        public ServicoSocialDto(HealthCareDbContext contextFactory, IMapper mapper)
        {
            this.repository = contextFactory;
            this.mapper = mapper;
            ViewModelUtils.mapper = mapper;
        }

        public ServicoSocialReadModel ObterServicoSocialPorAcolhimento(Guid idAcolhimento)
        {
            try
            {

                return (from s in repository.ServicoSocial.Include("Acolhimento").Include("Checkout").Include("Acolhimento.Paciente")
                                                          .Include("Acolhimento.Paciente.Endereco").Where(x => x.Acolhimento.Id == idAcolhimento).ToList()
                        select new ServicoSocialReadModel
                        {
                            AcolhimentoId = s.Acolhimento.Id,
                            Historico = s.Historico,
                            HoraInicio = s.InicioAtendimento.ToString("dd/MM/yyyy  HH:mm:ss", new CultureInfo("en-US")),
                            Nome = s.Acolhimento.Paciente.Nome,
                            Idade = s.Acolhimento.Paciente.IdadePorExtenso,
                            Observacao = s.Checkout != null ? s.Checkout.Observacao : string.Empty,
                            PacienteId = s.Acolhimento.Paciente.Id,
                            CodBoletim = s.Acolhimento.CodigoBoletim,
                            DiagnosticoUm = s.Checkout != null ? s.Checkout.DiagnosticoUm : string.Empty,
                            DiagnosticoDois = s.Checkout != null ? s.Checkout.DiagnosticoDois : string.Empty,
                            Intervencao = s.Intervencao,
                            Id = s.Id,
                            Paciente = mapper.Map<Paciente, PacienteReadModel>(s.Acolhimento.Paciente)
                        }).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public BoletimReadModel ObterBoletimPorAcolhimento(Guid idAcolhimento)
        {
            try
            {
                var b = (from ServicoSocial in repository.ServicoSocial.Include("Acolhimento").Include("Acolhimento.Paciente").Include("Acolhimento.Paciente.Endereco")
                                                                   .Include("Profissional.Unidade").Include("Profissional.Unidade.Endereco")
                                                                   .Include("Acolhimento").Include("Acolhimento.Classificacao").Include("Acolhimento.Classificacao.SinaisVitais")
                                                                   .Include("Acolhimento.Classificacao.Risco")
                             .Where(x => x.Acolhimento.Id == idAcolhimento).ToList()
                         select new BoletimReadModel
                         {
                             dataAtendimento = ServicoSocial.InicioAtendimento.Date.ToString("dd/MM/yyyy hh:mm:ss tt"),
                             AcolhimentoReadModel = mapper.Map<HealthCare.Domain.Entities.DbClasses.Atendimento.Acolhimento, AcolhimentoReadModel>(ServicoSocial.Acolhimento),
                             ClassificacaoDeRiscoReadModel = mapper.Map<HealthCare.Domain.Entities.DbClasses.Atendimento.ClassificacaoDeRisco, ClassificacaoDeRiscoReadModel>(ServicoSocial.Acolhimento.Classificacao.FirstOrDefault()),
                         }).FirstOrDefault();

                return b;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string CalulaIdade(DateTime? dataNascimento)
        {
            string idade = string.Empty;
            if (dataNascimento.HasValue)
            {
                // Calculate the age.
                var idadeCalculada = DateTime.Today.Year - dataNascimento.Value.Year;

                if (dataNascimento > DateTime.Today.AddYears(-idadeCalculada))
                    idadeCalculada--;

                if (idadeCalculada < 1)
                    idade = "Menos de 1 ano.";
                else if (idadeCalculada == 1)
                    idade = string.Format("{0} ano.", idadeCalculada.ToString());
                else
                    idade = string.Format("{0} ano(s).", idadeCalculada.ToString());
            }

            return idade;
        }

        public ListResult ListarServicoSocials(Guid Unidade, DateTime inicio, Guid idProfissional, DateTime? Fim = null)
        {
            throw new NotImplementedException();
        }

        public ListResult ListarServicoSocial(Guid Unidade, Guid idProfissional)
        {
            try
            {
                var lista = (from at in repository.ServicoSocial.Include("Acolhimento")
                                                                        .Include("Acolhimento.Paciente")
                                                                        .Include("Acolhimento.Paciente.Unidade")
                                                                        .Include("Checkout")
                                                                        .Include("Checkout.MotivoCheckout")
                                                                        .Where(x => x.Acolhimento.Paciente.Unidade.Id == Unidade && x.Checkout == null).ToList()
                             select new ServicoSocialReadModel
                             {
                                 Id = at.Id,
                                 InicioAtendimento = at.InicioAtendimento.ToString("dd/MM/yyyy hh:mm:ss"),
                                 Nome = at.Acolhimento.Paciente.Nome,
                                 Nascimento = at.Acolhimento.Paciente.DataNascimento,
                                 Idade = CalulaIdade(at.Acolhimento.Paciente.DataNascimento),
                                 Sexo = at.Acolhimento.Paciente.Sexo,
                                 Checkout = at.Checkout != null && at.Checkout.MotivoCheckout != null ? at.Checkout.MotivoCheckout.Descricao : string.Empty,
                                 FimAtendimento = at.FimAtendimento != null ? at.FimAtendimento.Value.ToString() : string.Empty,
                                 AcolhimentoId = at.Acolhimento.Id,
                                 Observacao = at.Checkout != null ? at.Checkout.Observacao : string.Empty
                             }).ToList();

                ListResult result = ListResult.GetListResult(lista, new string[] { "Id", "InicioAtendimento", "Nome", "Sexo", "Idade", "Checkout", "FimAtendimento", "AcolhimentoId", "Observacao" });
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
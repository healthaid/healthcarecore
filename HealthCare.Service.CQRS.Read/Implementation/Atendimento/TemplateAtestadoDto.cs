﻿using AutoMapper;
using HealtCare.Infra.Common.Utils;
using HealthCare.Domain.Entities.DbClasses.Atendimento;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.Data.Context;
using System;
using System.Linq;


namespace HealthCare.Infra.CQRS.Read.Implementation
{
    public class TemplatesAtestadoDto : ITemplatesAtestadoDto
    {
        private HealthCareDbContext repository;
        private IMapper mapper;
        public TemplatesAtestadoDto(HealthCareDbContext contextFactory, IMapper mapper)
        {
            this.repository = contextFactory;
            this.mapper = mapper;
            ViewModelUtils.mapper = mapper;
        }

        public TemplatesAtestadoReadViewModel ObterTemplatesAtestado(Guid id)
        {
            try
            { 
                return ViewModelUtils.ToModel<TemplatesAtestado, TemplatesAtestadoReadViewModel>(repository.Query<TemplatesAtestado>().FirstOrDefault(x => x.Id == id));
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public TemplatesAtestadoReadViewModel ObterTemplatesAtestadoPorAtestado(Guid idAtestado)
        {
            try
            { 
                return ViewModelUtils.ToModel<TemplatesAtestado, TemplatesAtestadoReadViewModel>(repository.Query<TemplatesAtestado>().FirstOrDefault(x => x.Atestado.Id == idAtestado));
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
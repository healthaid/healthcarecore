﻿using AutoMapper;
using HealtCare.Infra.Common.Utils;
using HealthCare.Domain.Entities.DbClasses.Atendimento;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.Data.Context;
using System;
using System.Collections.Generic;
using System.Linq;


namespace HealthCare.Infra.CQRS.Read.Implementation
{
    public class Ciap2Dto : ICiap2Dao
    {
        private HealthCareDbContext contextFactory;
        private IMapper mapper;

        public Ciap2Dto(HealthCareDbContext contextFactory, IMapper mapper)
        {
            this.contextFactory = contextFactory;
            this.mapper = mapper;
            ViewModelUtils.mapper = mapper;
        }
        public IList<Ciap2ReadViewModel> ListarCiaps()
        {
            try
            {
                return ViewModelUtils.ToListViewModel<Ciap2, Ciap2ReadViewModel>(contextFactory.Query<Ciap2>()).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public IList<Ciap2ReadViewModel> ListarCiaps(string descricao)
        {
            var i = ViewModelUtils.ToListViewModel<Ciap2, Ciap2ReadViewModel>(contextFactory.Query<Ciap2>().Where(x => x.Descricao.Contains(descricao)).Take(10)).ToList();
            return i;
        }


        public Ciap2ReadViewModel ObterCiapPorAtendimento(System.Guid idAtendimento)
        {
            throw new NotImplementedException();
        }
    }
}

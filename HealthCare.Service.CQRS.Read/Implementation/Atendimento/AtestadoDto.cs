﻿using AutoMapper;
using HealtCare.Infra.Common.Utils;
using HealthCare.Domain.Entities.DbClasses.Atendimento;
using HealthCare.Infra.CQRS.Read.Interfaces.Atendimento;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Globalization;
using System.Linq;


namespace HealthCare.Infra.CQRS.Read.Implementation
{
    public class AtestadoDto : IAtestadoDao
    {
        private HealthCareDbContext repository;
        private IMapper mapper;
        public AtestadoDto(HealthCareDbContext contextFactory, IMapper mapper )
        {
            this.repository = contextFactory;
            this.mapper = mapper;
            ViewModelUtils.mapper = mapper;
        }

        public AtestadoReadViewModel ObterAtestado(Guid id)
        {
            try
            {
                return ViewModelUtils.ToModel<Atestados, AtestadoReadViewModel>(repository.Query<Atestados>().FirstOrDefault(x => x.Id == id));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public AtestadoReadViewModel ObterAtestadoPorAtendimento(Guid idAtendimento)
        {
            try
            {
                var atestado = from a in repository.Atestados.Include("Atendimento").Include("Atendimento.Acolhimento").Include("Atendimento.Acolhimento.Paciente").Include("CID").ToList()
                               where a.Atendimento.Id == idAtendimento
                               select new AtestadoReadViewModel()
                               {
                                   Id = a.Id,
                                   Descricao = a.Descricao,
                                   AtendimentoId = a.Atendimento.Id,
                                   Cid = a.CID != null ? string.Format("{0} - {1}", a.CID.Codigo, a.CID.Descricao) : "",
                                   Nome = a.Atendimento.Acolhimento.Paciente.Nome,
                                   CodigoBoletim = a.Atendimento.Acolhimento.CodigoBoletim,
                                   CPF = a.Atendimento.Acolhimento.Paciente.CPF,
                                   RG = a.Atendimento.Acolhimento.Paciente.RG,
                                   DataAtendimento = a.Atendimento.InicioAtendimento.ToString("d")
                               };

                return atestado.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public AtestadoReportReadModel ObterReportPorAtendimento(Guid idAtendimento)
        {
            try
            {
                AtestadoReportReadModel report = (from c in repository.Atendimento.Include("Acolhimento")
                                                            .Include("Acolhimento.Classificacao")
                                                            .Include("Acolhimento.Classificacao.Risco")
                                                            .Include("Acolhimento.Classificacao.SinaisVitais")
                                                            .Include("Acolhimento.Paciente")
                                                            .Include("Acolhimento.Paciente.Unidade")
                                                            .Include("Acolhimento.Paciente.Unidade.Endereco")
                                                            .Include("Checkout")
                                                            .Include("Procedimentos")
                                                            .Include("HipoteseDiagnostica")
                                                            .Include("PrimeiroDiagnostico")
                                                            .Include("Atestado")
                                                            .ToList()
                                                  where c.Id == idAtendimento
                                                  select new AtestadoReportReadModel
                                                  {
                                                      CodigoBoletim = c.Acolhimento.CodigoBoletim,
                                                      CPF = c.Acolhimento.Paciente.CPF,
                                                      DataNascimento = c.Acolhimento.Paciente.DataNascimento != null ? c.Acolhimento.Paciente.DataNascimento.Value.ToString("dd/MM/yyyy", new CultureInfo("en-US")) : "",
                                                      Nome = c.Acolhimento.Paciente.Nome,
                                                      RG = c.Acolhimento.Paciente.RG,
                                                      unidade = c.Acolhimento.Paciente.Unidade.NomeFantasia,
                                                      Dias = c.Atestado.Count > 0 ? c.Atestado.FirstOrDefault().Dias : "",
                                                      CID = c.PrimeiroDiagnostico != null ? c.PrimeiroDiagnostico.Codigo + " " + c.PrimeiroDiagnostico.Descricao : "",
                                                      AtestadoId = c.Atestado.Count > 0 ? c.Atestado.FirstOrDefault().Id.ToString() : "",
                                                      DataAtendimento = c.InicioAtendimento.ToString("dd/MM/yyyy", new CultureInfo("en-US")),
                                                      DataFimAtendimento = c.FimAtendimento != null ? c.FimAtendimento.Value.ToString("dd/MM/yyyy", new CultureInfo("en-US")) : "",
                                                      Descricao = c.Atestado.Count > 0 ? c.Atestado.FirstOrDefault().Descricao : "",
                                                  }).FirstOrDefault();

                return report;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
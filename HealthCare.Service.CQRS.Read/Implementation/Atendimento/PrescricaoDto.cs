﻿using AutoMapper;
using HealtCare.Infra.Common.Utils;
using HealthCare.Domain.Entities.DbClasses.Atendimento;
using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace HealthCare.Infra.CQRS.Read.Implementation.Atendimento
{
    public class PrescricaoDto : IPrescricaoDto
    {
        private HealthCareDbContext repository;
        private IMapper mapper;

        public PrescricaoDto(HealthCareDbContext contextFactory, IMapper mapper)
        {
            this.repository = contextFactory;
            this.mapper = mapper;
            ViewModelUtils.mapper = mapper;
        }

        public PrescricaoReadModel ObterPrescricaoPorId(Guid id)
        {
            try
            {
                return ViewModelUtils.ToModel<Prescricao, PrescricaoReadModel>(repository.Prescricao.Include("Atendimento.Acolhimento.Paciente").FirstOrDefault(x => x.Id == id));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ListResult ListarPrescricoesPorAtendimentoId(Guid atendimentoId)
        {
            try
            {
                var lista = ViewModelUtils.ToListViewModel<Prescricao, PrescricaoReadModel>(repository.Prescricao.Include("Atendimento").Where(x => x.Atendimento.Id == atendimentoId)).ToList();
                ListResult result = ListResult.GetListResult(lista, new string[] { "Id", "Medicamento", "Dose", "Unidade", "ViaAdministracao", "Intervalo", "StatusPorEscrito", "ObservacaoEnfermagem", "Duracao", "ObservacaoMedico" });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ListResult ImpressaoPrescricao(Guid atendimentoId)
        {
            try
            {
                var lista = (from p in repository.Prescricao.Include("Atendimento.Acolhimento.Paciente").Include("Atendimento.Profissional.Cbo").Where(x => x.Atendimento.Id == atendimentoId).ToList().GroupBy(x => x.Atendimento)
                             select new PrescricaoMedicaReadModel
                             {
                                 NomePaciente = p.Key.Acolhimento.Paciente.Nome,
                                 Nascimento = p.Key.Acolhimento.Paciente.DataNascimento != null ? p.Key.Acolhimento.Paciente.DataNascimento.Value.ToString("dd/MM/yyyy", new CultureInfo("en-US")) : "Não informado",
                                 Cpf = p.Key.Acolhimento.Paciente.CPF,
                                 Profissional = p.Key.Profissional.Nome,
                                 Crm = string.Format("CRM: {0}", p.Key.Profissional.CRM),
                                 Cbo = string.Format("{0} - {1}", p.Key.Profissional.Cbo.Codigo, p.Key.Profissional.Cbo.Descricao),
                                 ItensPrescricao = (from item in p
                                                    select new PrescricaoMedicaReadModel
                                                    {
                                                        Medicamento = item.Medicamento,
                                                        Dose = item.Dose,
                                                        Duracao = item.Duracao,
                                                        Unidade = item.Unidade,
                                                        ViaAdministracao = item.ViaAdministracao,
                                                        ObservacaoMedico = item.ObservacaoMedico,
                                                        Intervalo = item.Intervalo
                                                    }).ToList()
                             }).ToList();


                ListResult result = ListResult.GetListResult(lista, new string[] { "NomePaciente", "Nascimento", "Cpf", "ItensPrescricao", "Profissional", "Crm", "Cbo" });
                //ListResult result = ListResult.GetListResult(lista, new string[] { "Id", "Medicamento", "Dose", "Unidade", "ViaAdministracao", "Intervalo", "StatusPorEscrito", "ObservacaoEnfermagem", "Duracao", "ObservacaoMedico" });

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ListResult ListarPrescricoesPendentes(Guid unidadeId)
        {
            try
            {
                //var teste = repository.Prescricao.Include("Atendimento.Acolhimento.Profissional").Include("Atendimento.Acolhimento.Paciente").Include("Atendimento.Profissional").Include("Atendimento.Profissional.Unidade").Where(x => x.StatusEnfermagem != "true" && x.Atendimento.FimAtendimento == null && x.Atendimento.Acolhimento.Profissional.Unidade.Id == unidadeId).ToList().GroupBy(p => p.Atendimento.Id).Select(p => p.FirstOrDefault()).ToList();
                var lista = (from p in repository.Prescricao.Include("Atendimento.Acolhimento.Profissional").Include("Atendimento.Acolhimento.Paciente").Include("Atendimento.Profissional").Include("Atendimento.Profissional.Unidade").Where(x => x.StatusEnfermagem != "true" && x.Atendimento.FimAtendimento == null && x.Atendimento.Acolhimento.Profissional.Unidade.Id == unidadeId)
                             .ToList()
                             .GroupBy(p => p.Atendimento.Id)
                             .Select(p => p.FirstOrDefault())
                             select new FilaPrescricaoMedicaReadModel
                             {
                                 Id = p.Atendimento.Id,
                                 Nome = p.Atendimento.Acolhimento.Paciente.Nome,
                                 Data = p.Atendimento.InicioAtendimento.ToString("dd/MM/yyyy H:mm:ss tt"),
                                 Profissional = p.Atendimento.Profissional.Nome
                             }).OrderBy(x => x.Data).ToList();

                ListResult result = ListResult.GetListResult(lista, new string[] { "Id", "Nome", "Data", "Profissional" });

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ListResult ListarPrescricoesPendentesPorAtendimento(Guid atendimentoId)
        {
            try
            {

                var lista = repository.Prescricao.Include("Atendimento").Include("Atendimento.Acolhimento").Include("Atendimento.Acolhimento.Paciente")
                     .Where(p => p.StatusEnfermagem != "true" && p.Atendimento.Id == atendimentoId).ToList().OrderBy(x => x.EnvioEnfermagem).Select(p => new PrescricaoMedicaReadModel()
                     {

                         Id = p.Id,
                         Dose = p.Dose,
                         Duracao = p.Duracao,
                         HoraSolitiacao = p.EnvioEnfermagem.ToString("dd/MM/yyyy"),
                         Intervalo = p.Intervalo,
                         Medicamento = p.Medicamento,
                         NomePaciente = p.Atendimento.Acolhimento.Paciente.Nome,
                         ObservacaoMedico = p.ObservacaoMedico,
                         Unidade = p.Unidade,
                         ViaAdministracao = p.ViaAdministracao,
                         idade = p.Atendimento.Acolhimento.Paciente.IdadePorExtenso,
                         codigoBoletim = p.Atendimento.Acolhimento.CodigoBoletim
                     }).ToList();


                ListResult result = ListResult.GetListResult(lista, new string[] { "Id", "Medicamento", "Dose", "Duracao", "Unidade", "ViaAdministracao", "Intervalo", "ObservacaoMedico", "NomePaciente", "HoraSolitiacao", "idade", "codigoBoletim" });

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ListResult ListarProducaoEnfermagem(int mes, int ano, Guid unidadeId)
        {
            try
            {
                /// Quantidade referente a medicação
                var prescricoes = ViewModelUtils.ToListViewModel<Prescricao, PrescricaoReadModel>(
                                                                  repository.Prescricao.Include("Atendimento.Acolhimento.Profissional.Unidade").Include("Atendimento.Acolhimento.Paciente")
                                                                  .Where(x => x.RetornoEnfermagem.Value.Month == mes && x.RetornoEnfermagem.Value.Year == ano && x.Atendimento.Acolhimento.Profissional.Unidade.Id == unidadeId)
                                                                  .OrderBy(X => X.RetornoEnfermagem)).ToList();
                /// Quantidade referente aos procedimentos
                var procedimentos = ViewModelUtils.ToListViewModel<ProcedimentosEnfermagem, ProcedimentosEnfermagemReadModel>(
                                  repository.ProcedimentosEnfermagem.Include("Procedimento")
                                                                    .Include("Evolucao.MovimentacaoLeito.Internacao.Atendimento.Acolhimento.Paciente")
                                                                    .Where(p => (p.Procedimento.CO_PROCEDIMENTO == "0301100039" || p.Procedimento.CO_PROCEDIMENTO == "0214010015" ||
                                                                                 p.Procedimento.CO_PROCEDIMENTO == "0301100101" || p.Procedimento.CO_PROCEDIMENTO == "0301100144")
                                                                    ));

                procedimentos = procedimentos.Where(p => p.Evolucao.HoraEvolucao.Month == mes && p.Evolucao.HoraEvolucao.Year == ano);

                /// Agrupa as medicações
                var listarMedicacao = (from r in prescricoes
                                       group r by new
                                       {
                                           r.DataMedicacao,
                                           r.ViaAdministracao
                                       }
                                      into m
                                       select new PostoEnfermagemReadModel()
                                       {
                                           Procedimento = m.Key.ViaAdministracao,
                                           Data = m.Key.DataMedicacao.ToString(),
                                           TotalProcedimento = m.Count(),
                                           TotalPaciente = m.Select(x => x.Atendimento.Acolhimento.Paciente.Nome).Distinct().Count(),
                                       }).ToList();

                /// Agrupa os procedimentos
                var listarProcedimentos = (from p in procedimentos
                                           group p by new
                                           {
                                               p.Evolucao.HoraEvolucao,
                                               p.Procedimento.CO_PROCEDIMENTO,
                                               p.Procedimento.NO_PROCEDIMENTO
                                           } into pc
                                           select new PostoEnfermagemReadModel()
                                           {
                                               Procedimento = pc.Key.NO_PROCEDIMENTO,
                                               TotalProcedimento = pc.Count(),
                                               TotalPaciente = pc.Select(x => x.Evolucao.MovimentacaoLeito.Internacao.Atendimento.Acolhimento.Paciente.Nome).Distinct().Count(),
                                               //Data = pc.Key.HoraEvolucao.Date.ToString("yyyy/MM/dd")
                                           }).ToList();

                /// Unifica as listas
                listarMedicacao.AddRange(listarProcedimentos);

                /// Obtém os agrupamentos (Via de administração de medicação e procedimentos solicitados para o relatório)
                var procs = listarMedicacao.ToList().Select(x => x.Procedimento).Distinct();
                List<PostoEnfermagemReadModel> retorno = new List<PostoEnfermagemReadModel>();

                string smes = mes.ToString();

                if (mes.ToString().Length == 1)
                    smes = '0' + smes;

                int qtdDias = DateTime.DaysInMonth(ano, mes);
                /// Para cada agrupamento cria uma coluna para o dia daquele mÊs informando quantidade executada por procedimento e total de pacientes
                foreach (string grupo in procs)
                {
                    PostoEnfermagemReadModel agrupamento = new PostoEnfermagemReadModel();
                    agrupamento.Lista = new List<PostoEnfermagemReadModel>();
                    agrupamento.Procedimento = grupo;
                    for (int i = 1; i <= qtdDias; i++)
                    {

                        var dia = i.ToString().Length < 2 ? string.Format("0{0}", i.ToString()) : i.ToString();
                        var registro = listarMedicacao.FirstOrDefault(x => x.Procedimento == grupo && x.Data == string.Format("{0}/{1}/{2}", dia, smes, ano));
                        if (registro != null)
                            agrupamento.Lista.Add(registro);
                        else
                            agrupamento.Lista.Add(new PostoEnfermagemReadModel { Data = string.Format("{0}/{1}/{2}", i, mes, ano), TotalProcedimento = 0, TotalPaciente = 0 });
                    }
                    agrupamento.TotalPaciente = agrupamento.Lista.Sum(x => x.TotalPaciente);
                    agrupamento.TotalProcedimento = agrupamento.Lista.Sum(x => x.TotalProcedimento);
                    retorno.Add(agrupamento);
                }

                return ListResult.GetListResult(retorno, new string[] { "Procedimento", "TotalProcedimento", "TotalPaciente", "TotalPaciente", "TotalProcedimento", "Lista" });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ListResult ListarPrescricoesPendentesAtendimentosFechados(Guid unidadeId, string competencia)
        {
            try
            {
                int mes = Convert.ToInt32(competencia.Substring(0, 2));
                int ano = Convert.ToInt32(competencia.Substring(2, 4));

                var lista = (from p in repository.Prescricao.Include("Atendimento.Acolhimento.Profissional").Include("Atendimento.Acolhimento.Paciente")
                                                            .Include("Atendimento.Profissional").Include("Atendimento.Profissional.Unidade")
                             .Where(x => x.Atendimento.Acolhimento.Profissional.Unidade.Id == unidadeId
                                      && x.StatusEnfermagem != "true"
                                      && x.Atendimento.FimAtendimento != null 
                                      && x.Atendimento.FimAtendimento.Value.Month == mes && x.Atendimento.FimAtendimento.Value.Year == ano
                                      )
                             .ToList()
                             .GroupBy(p => p.Atendimento.Id)
                             .Select(p => p.FirstOrDefault())
                             select new FilaPrescricaoMedicaReadModel
                             {
                                 Id = p.Atendimento.Id,
                                 Nome = p.Atendimento.Acolhimento.Paciente.Nome,
                                 Data = p.Atendimento.InicioAtendimento.ToString("dd/MM/yyyy H:mm:ss tt"),
                                 Profissional = p.Atendimento.Profissional.Nome
                             }).OrderBy(x => x.Data).ToList();

                ListResult result = ListResult.GetListResult(lista, new string[] { "Id", "Nome", "Data", "Profissional" });

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

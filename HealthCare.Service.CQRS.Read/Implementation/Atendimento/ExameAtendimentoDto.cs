﻿using AutoMapper;
using HealtCare.Infra.Common.Utils;
using HealthCare.Domain.Entities.DbClasses.Atendimento;
using HealthCare.Infra.Common.Enums;
using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Read.Interfaces.Atendimento;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Globalization;
using System.Linq;

namespace HealthCare.Infra.CQRS.Read.Implementation.Atendimento
{
    public class ExameAtendimentoDto : IExameAtendimentoDto
    {
        private HealthCareDbContext repository;

        private IMapper mapper;

        public ExameAtendimentoDto(HealthCareDbContext contextFactory, IMapper mapper)
        {
            this.repository = contextFactory;
            this.mapper = mapper;
            ViewModelUtils.mapper = mapper;
        }

        public ExameAtendimentoReadModel GetExameAtendimentoById(Guid id)
        {
            try
            {
                return ViewModelUtils.ToModel<ExameAtendimento, ExameAtendimentoReadModel>(repository.ExameAtendimento.Include("Atendimento.Acolhimento.Paciente")
                                                                                                                      .Include("Atendimento.Acolhimento").FirstOrDefault(x => x.Id == id));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ListResult ListarExameAtendimentosByAtendimentoId(Guid id)
        {
            try
            {
                var lista = ViewModelUtils.ToListViewModel<ExameAtendimento, ExameAtendimentoReadModel>(repository.ExameAtendimento.Include("Atendimento").Where(x => x.Atendimento.Id == id)).ToList();
                ListResult result = ListResult.GetListResult(lista, new string[] { "Id", "Descricao" });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ListResult ListarExameAtendimentosPendente(string subgrupo)
        {
            try
            {
                var grupoSubgrupo = ConverteDefinicaoParaSubGrupo(subgrupo);

                var lista = (from e in repository.ExameAtendimento.Include("Atendimento").Include("Atendimento.Acolhimento").Include("Atendimento.Acolhimento.Paciente")
                                                                  .Include("Atendimento.Profissional")
                                                                  .Where(x => x.Realizado == false
                                                                            && !string.IsNullOrEmpty(x.CodigoProcedimento)
                                                                            && x.Atendimento != null
                                                                            && x.CodigoProcedimento.StartsWith(grupoSubgrupo)
                                                                            && x.Atendimento.FimAtendimento == null)
                                                                  .ToList()
                                                                  .GroupBy(e => e.Atendimento.Id)
                                                                  .Select(e => e.FirstOrDefault())
                             select new FilaExameAtendimentoReadModel()
                             {
                                 Id = e.Atendimento.Id,
                                 Nome = e.Atendimento.Acolhimento.Paciente.Nome,
                                 Data = e.DataSolicitacao.ToString("dd/MM/yyyy HH:mm:ss", new CultureInfo("en-US")),
                                 Profissional = e.Atendimento.Profissional.Nome
                             }).ToList();

                ListResult result = ListResult.GetListResult(lista, new string[] { "Id", "Nome", "Data", "Profissional" });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ListResult ListarExameAtendimentosPendentePorAtendimento(string subgrupo, Guid atendimentoId)
        {
            try
            {
                var grupoSubgrupo = ConverteDefinicaoParaSubGrupo(subgrupo);

                var lista = (from e in repository.ExameAtendimento.Include("Atendimento").Include("Atendimento.Acolhimento").Include("Atendimento.Acolhimento.Paciente")
                                                                 .Include("Atendimento.Profissional").ToList()
                             where e.Realizado == false
                                && !string.IsNullOrEmpty(e.CodigoProcedimento)
                                && e.CodigoProcedimento.StartsWith(grupoSubgrupo)
                                && e.Atendimento.FimAtendimento == null
                                && e.Atendimento != null
                                && e.Atendimento.Id == atendimentoId
                             select new ListaExameAtendimentoReadModel()
                             {
                                 DataSolicitacao = e.DataSolicitacao.ToString("dd/MM/yyyy HH:mm:ss", new CultureInfo("en-US")),
                                 Exame = e.Exame,
                                 Id = e.Id,
                                 ObservacaoMedico = e.ObservacaoMedico,
                                 Paciente = e.Atendimento.Acolhimento.Paciente.Nome,
                                 Procedimento = e.Procedimento,
                                 Profissional = e.Atendimento.Profissional.Nome,
                                 ObservacaoTecnica = e.ObservacaoExecutor
                             }).ToList();

                ListResult result = ListResult.GetListResult(lista, new string[] { "Id", "DataSolicitacao", "Exame", "ObservacaoMedico", "Paciente", "Procedimento", "Profissional", "ObservacaoTecnica" });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string ConverteDefinicaoParaSubGrupo(string subgrupo)
        {
            if (subgrupo == "M")
                subgrupo = structTipoExame.ExamesDeDiagnostico;
            else if (subgrupo == "I")
                subgrupo = structTipoExame.ExamesDeImagem;
            else
                subgrupo = structTipoExame.ExamesLaboratoriais;

            return subgrupo;
        }

        public ListResult ListarExameAtendimentosByAtendimentoId(Guid id, string subgrupo, string origem)
        {
            try
            {
                var grupoSubgrupo = ConverteDefinicaoParaSubGrupo(subgrupo);

                var lista = (from x in repository.ExameAtendimento.Include("Atendimento")
                             where x.Atendimento.Id == id
                                && !string.IsNullOrEmpty(x.CodigoProcedimento)
                                && x.CodigoProcedimento.StartsWith(grupoSubgrupo)
                             select new ListaExamesViewModel()
                             {
                                 Id = x.Id,
                                 Descricao = x.Exame,
                                 Profissional = x.Executor != null ? x.Executor.Nome : "",
                                 Executado = x.Realizado ? "Realizado" : "Aguardando Realização",
                                 Observacao = origem == "cadastro" ? x.ObservacaoExecutor : x.ObservacaoMedico,
                                 ObservacaoTecnica = x.ObservacaoExecutor
                             }).ToList();

                ListResult result = ListResult.GetListResult(lista, new string[] { "Id", "Descricao", "Profissional", "Executado", "Observacao", "ObservacaoTecnica" });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ListResult ListarExamesRaioX(DateTime inicio, DateTime fim, Guid unidadeId)
        {
            try
            {
                fim = fim.AddDays(1);

                int qtdExames = repository.ExameAtendimento.Count();

                var _exames = from e in repository.ExameAtendimento.Include("Atendimento.Profissional.Unidade").Include("Atendimento.Acolhimento.Servico").Where(X => X.Atendimento.Acolhimento.Servico != null).ToList()
                              where e.Atendimento.Acolhimento.Servico != null
                                 && e.Atendimento.Profissional.Unidade.Id == unidadeId
                                 && e.Exame != null
                                 && e.CodigoProcedimento.StartsWith(structTipoExame.ExamesDeImagem)
                                 && (e.Atendimento.InicioAtendimento >= inicio && e.Atendimento.InicioAtendimento <= fim)
                                 && e.Realizado == true
                              select e;

                var exames = (from e in _exames
                              group e by e.Atendimento.Acolhimento.Servico.Descricao into ex
                              select new RadiologiaClinicaExameReadModel
                              {
                                  Classificacao = ex.Key.ToString(),
                                  examesAtendimentos = (from r in ex.ToList()
                                                        group r by r.Exame into re
                                                        select new RadiologiaClinicaExameReadModel()
                                                        {
                                                            Descricao = re.Key,
                                                            Total = re.ToList().Count.ToString(),
                                                            TotalRelacaoClinica = Math.Round((Convert.ToDouble((re.ToList().Count() * 100)) / ex.Count()), 2).ToString() + "%",
                                                            TotalRelacaoUnidade = Math.Round((Convert.ToDouble((re.ToList().Count() * 100)) / qtdExames), 2).ToString() + "%",
                                                        }).ToList(),
                                  Total = ex.ToList().Count.ToString(),
                                  TotalRelacaoClinica = ((ex.ToList().Count() * 100) / _exames.Count()).ToString() + "%",
                                  TotalGeral = _exames.Count().ToString()
                              }).ToList();

                ListResult result = ListResult.GetListResult(exames, new string[] { "Classificacao", "examesAtendimentos", "Descricao", "Total", "TotalRelacaoClinica", "TotalRelacaoUnidade", "TotalGeral" });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

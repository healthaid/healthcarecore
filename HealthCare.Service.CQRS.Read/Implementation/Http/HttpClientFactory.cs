﻿using HealthCare.Service.CQRS.Read.Interfaces.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Http;
using HealthCare.Service.CQRS.Read.Implementation.Voz;
using HealthCare.Infra.Common.Enums;
using static HealthCare.Service.CQRS.Read.Implementation.Voz.Synthesize;

namespace HealthCare.Service.CQRS.Read.Implementation.Http
{
    public class HttpClientFactory : IHttpClientFactory
    {
        private HttpClient client;
        private InputOptions options;
        private HttpClient tokenClient;
        private HttpClient singleClient;
        private const string subscriptionKey = "3485097072e54248822089f7087dc992";
        public HttpClientFactory()
        {
            Inicialize();
        }

        private void Inicialize()
        {
            string requestUri = "https://speech.platform.bing.com/synthesize";
            HttpClientHandler handler = new HttpClientHandler();
            client = new HttpClient(handler);
            this.options = new InputOptions()
            {
                RequestUri = new Uri(requestUri),
                VoiceType = Gender.Male,
                Locale = "pt-BR",
                VoiceName = "Microsoft Server Speech Text to Speech Voice (pt-BR, Daniel, Apollo)",
            };
            foreach (var header in this.options.Headers)
            {
                client.DefaultRequestHeaders.TryAddWithoutValidation(header.Key, header.Value);
            }
            client.Timeout = TimeSpan.FromSeconds(30);
            tokenClient = new HttpClient();
            tokenClient.Timeout = TimeSpan.FromSeconds(20);
            singleClient = new HttpClient();
            this.tokenClient.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", subscriptionKey);

        }

        public HttpClient ReturnHttpClient()
        {
            return this.client;
        }

        public Voz.Synthesize.InputOptions ReturnOptions()
        {
            return this.options;
        }

        public HttpClient ReturnTokenClient()
        {
            return this.tokenClient;
        }

        public HttpClient ReturnSingleHttpClient()
        {
            return this.singleClient;
        }
    }
}

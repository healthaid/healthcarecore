﻿using BinaryFormatter;
using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Infra.Common.Enums;
using HealthCare.Infra.Common.Extensions;
using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Read.Interfaces.Voz;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.Data.Context;
using HealthCare.Service.CQRS.Read.Implementation.Voz;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using OpenTK.Audio.OpenAL;
using OpenTK.Audio;
using System.Runtime.InteropServices;
using System.Text;
using HealthCare.Service.CQRS.Read.Interfaces.Http;
using System.Threading.Tasks;
using System.Net.Http;
using HealthCare.Service.CQRS.Read.Implementation.Http;
using System.Xml.Linq;
using RestSharp;
using System.Net;
using Newtonsoft.Json;
using RestSharp.Extensions.MonoHttp;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.WebUtilities;

namespace HealthCare.Infra.CQRS.Read.Implementation.Voz
{
    public class VozCommandDao : IVozCommandDao
    {
        private HealthCareDbContext repository;
        private IHttpClientFactory client;

        public VozCommandDao(HealthCareDbContext contextFactory, IHttpClientFactory client)
        {
            this.repository = contextFactory;
            this.client = client;
        }

        public List<ChamadaPainelModel> GetPacientesParaSeremChamados(Guid unidadeId)
        {
            try
            {
                List<ChamadaPainelModel> chamadas = new List<ChamadaPainelModel>();
                var lista = repository.Painel.Where(x => (x.Atendido == false && x.UnidadeId == unidadeId)).ToList();
                foreach (Painel chamada in lista)
                {
                    ChamadaPainelModel model = new ChamadaPainelModel();

                    //model.Fala = await GetFala(chamada.NomePaciente + " " + chamada.LocalAtendimento);
                    model.LocalAtendimento = chamada.LocalAtendimento;
                    model.NomePaciente = chamada.NomePaciente;
                    model.IdItemPainel = chamada.Id;
                    chamadas.Add(model);
                }
                return chamadas;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //protected async Task<string> GetFala(string fala)
        //{
        //    try
        //    {
        //        var builder = new UriBuilder("http://10.99.99.221/vozapp/RetornarAudio?fala=" + fala);

        //        HttpResponseMessage response = await client.ReturnSingleHttpClient().GetAsync(builder.ToString());
        //        if (response.IsSuccessStatusCode)
        //        {
        //            Stream stream = await response.Content.ReadAsStreamAsync();
        //            using (var ms = new MemoryStream())
        //            {
        //                stream.CopyTo(ms);
        //                return (String.Concat("data:audio/wav;base64,", Convert.ToBase64String(ms.ToArray())));
        //            }
        //        }
        //        else
        //            return string.Empty;

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //}

        public ListResult GetUltimosPacientesChamados(Guid unidadeId)
        {
            try
            {

                var lista = repository.Painel.Where(x => x.Atendido == true && x.UnidadeId == unidadeId).OrderByDescending(x => x.DataInsercao).DistinctBy(x => x.NomePaciente).Take(10).ToList();
                return ListResult.GetListResult(lista, new string[] { "NomePaciente", "LocalAtendimento", "Servico" });

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected async Task<string> GetVozCommand(string fala)
        {
            Stream stream;
            stream = await RealizarChamadaFree(fala);
            if (stream == null)
                stream = await RealizarChamadaPaga(fala);
            if (stream != null)
            {
                var path = Path.Combine(Directory.GetCurrentDirectory(), $"{ Guid.NewGuid() }.wav");
                using (var ms = new MemoryStream())
                {
                    stream.CopyTo(ms);
                    return (String.Concat("data:audio/wav;base64,", Convert.ToBase64String(ms.ToArray())));
                }
            }
            else
                return string.Empty;
        }

        protected async Task<Stream> RealizarChamadaFree(string fala)
        {
            Authentication auth = new Authentication("3e240127726e4e049002cece9275a999", this.client.ReturnTokenClient());
            string token1;
            try
            {
                token1 = await auth.GetAccessToken();
                Console.WriteLine("Token: {0}\n", token1);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed authentication.");
                Console.WriteLine(ex.ToString());
                Console.WriteLine(ex.Message);
                return null;
            }
            try
            {
                return await Speak(CancellationToken.None, this.client, fala, token1);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected async Task<Stream> RealizarChamadaPaga(string fala)
        {
            Authentication auth = new Authentication("3485097072e54248822089f7087dc992", this.client.ReturnTokenClient());
            string token1;
            try
            {
                token1 = await auth.GetAccessToken();
                Console.WriteLine("Token: {0}\n", token1);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed authentication.");
                Console.WriteLine(ex.ToString());
                Console.WriteLine(ex.Message);
                return null;
            }

            try
            {
                return await Speak(CancellationToken.None, this.client, fala, token1);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Sends the specified text to be spoken to the TTS service and saves the response audio to a file.
        /// </summary>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns>A Task</returns>
        public async Task<Stream> Speak(CancellationToken cancellationToken, IHttpClientFactory client, string fala, string token)
        {
            try
            {

                var genderValue = "";
                switch (client.ReturnOptions().VoiceType)
                {
                    case Gender.Male:
                        genderValue = "Male";
                        break;
                    case Gender.Female:
                    default:
                        genderValue = "Female";
                        break;

                }

                if (this.client.ReturnHttpClient().DefaultRequestHeaders.Contains("Authorization"))
                    this.client.ReturnHttpClient().DefaultRequestHeaders.Remove("Authorization");

                this.client.ReturnHttpClient().DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "Bearer " + token);

                var request = new HttpRequestMessage(HttpMethod.Post, client.ReturnOptions().RequestUri)
                {
                    Content = new StringContent(GenerateSsml(client.ReturnOptions().Locale, genderValue, client.ReturnOptions().VoiceName, fala))
                };
                HttpResponseMessage response = await client.ReturnHttpClient().SendAsync(request, HttpCompletionOption.ResponseHeadersRead, cancellationToken);
                if (response.IsSuccessStatusCode)
                {
                    Stream stream = await response.Content.ReadAsStreamAsync();
                    return stream;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string GenerateSsml(string locale, string gender, string name, string text)
        {
            var ssmlDoc = new XDocument(
                              new XElement("speak",
                                  new XAttribute("version", "1.0"),
                                  new XAttribute(XNamespace.Xml + "lang", "en-US"),
                                  new XElement("voice",
                                      new XAttribute(XNamespace.Xml + "lang", locale),
                                      new XAttribute(XNamespace.Xml + "gender", gender),
                                      new XAttribute("name", name),
                                      text)));
            return ssmlDoc.ToString();
        }

    }
}

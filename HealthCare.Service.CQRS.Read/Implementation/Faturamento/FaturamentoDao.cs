﻿using HealthCare.Infra.Data.Context;
using HealthCare.Service.CQRS.Read.Interfaces.Faturamento;
using HealthCare.Service.CQRS.Read.Models;
using Microsoft.EntityFrameworkCore;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace HealthCare.Service.CQRS.Read.Implementation.Faturamento
{
    public class FaturamentoDao : IFaturamentoDao
    {
        private HealthCareDbContext repository;
        public FaturamentoDao(HealthCareDbContext contextFactory)
        {
            this.repository = contextFactory;
        }

        public List<FaturamentoModel> ListaFaturamento(string unidadeId, int mes, int ano)
        {
            List<FaturamentoModel> listaFaturamento = new List<FaturamentoModel>();
            int index = 0;
            try
            {

                string sql = @"select 
                           ate.Id as AtendimentoId,
                           c.Id as PrimeiroDiagnosticoCid,
                           c.Codigo as Cid10,
                           cid.Id as SegundoDiagnosticoCid,
                           u.Cnpj as CnpjUnidade,
                           pro.CNS as CnsProfissional,
                           pro.Id as ProfissionalId,
                           p.Nome as NomePaciente,
                           u.CNES as CnesUnidade,
                           p.Email as EmailPaciente,
                           p.TelefoneCelular as TelefonePaciente,
                           e.Bairro as BairroEnderecoPaciente,
                           e.Numero as NumeroPaciente,
                           e.Complemento as ComplementoPaciente,
                           e.Logradouro as LogradouroPaciente,
                           e.CodigoLogradouro as CodigoLogradouro,
                           e.CEP as CepPaciente,
                           e.CodigoIbgeCidade as CodigoIbge,
                           p.Sexo as SexoPaciente,
                           case ap.CodigoProcedimento when '0301100012' then 
                                (SELECT TOP 1 _Cbo.Codigo
                                   FROM Prescricao _Prescricao
                             INNER JOIN Profissional _Profissional
                                     ON _Prescricao.EnfermeiroId = _Profissional.Id
                             INNER JOIN Cbo _Cbo
                                     ON _Profissional.CboId = _Cbo.Id
								  WHERE _Prescricao.AtendimentoId = ate.Id
                               ORDER BY _Prescricao.RetornoEnfermagem
                                  )
                           else
                            cbo.Codigo end 
                           as CodigoCbo,
                           p.CNS as CNSPaciente,
                           null as IdProcedimento,
                           ate.FimAtendimento as DataAtendimento,
                           p.DataNascimento as NascimentoPaciente,
                           p.Nacionalidade as Nacionalidade,
                           p.Etinia as PacienteEtinia,
                           p.Raca as Raca,
                           ap.CodigoProcedimento,
                           pro.Nome as NomeProfissional
                           from Acolhimento a
                           inner join Paciente p on a.PacienteId = p.Id
                           inner join Atendimento ate on ate.AcolhimentoId = a.Id
                           inner join Profissional pro on pro.Id = ate.ProfissionalId
                           left  join Cbo cbo on cbo.Id = pro.CboId
                           inner join Unidade u on u.Id = pro.UnidadeId
                           left join Endereco e on e.Id = p.EnderecoId
                           left join CID c on c.Id = ate.PrimeiroDiagnosticoId
                           left join CID cid on cid.Id = ate.SegundoDiagnosticoId
                           inner join AtendimentoProcedimento ap on ate.Id = ap.AtendimentoId
                           where u.Id = @unidadeId
                           and month(ate.FimAtendimento) = @mes
                           and year(ate.FimAtendimento) = @ano

                           union 

                           select 
                           '' as AtendimentoId,
                           '' as PrimeiroDiagnosticoCid,
                           '' as Cid10,
                           '' as SegundoDiagnosticoCid,
                           u.Cnpj as CnpjUnidade,
                           pro.CNS as CnsProfissional,
                           pro.Id as ProfissionalId,
                           p.Nome as NomePaciente,
                           u.CNES as CnesUnidade,
                           p.Email as EmailPaciente,
                           p.TelefoneCelular as TelefonePaciente,
                           e.Bairro as BairroEnderecoPaciente,
                           e.Numero as NumeroPaciente,
                           e.Complemento as ComplementoPaciente,
                           e.Logradouro as LogradouroPaciente,
                           e.CodigoLogradouro as CodigoLogradouro,
                           e.CEP as CepPaciente,
                           e.CodigoIbgeCidade as CodigoIbge,
                           p.Sexo as SexoPaciente,
                           cbo.Codigo as CodigoCbo,
                           p.CNS as CNSPaciente,
                           null as IdProcedimento,
                           cr.DataClassificacao as DataAtendimento,
                           p.DataNascimento as NascimentoPaciente,
                           p.Nacionalidade as Nacionalidade,
                           p.Etinia as PacienteEtinia,
                           p.Raca as Raca,
                           '0301060118' as CodigoProcedimento,
                           pro.Nome as NomeProfissional
                           from Acolhimento a
                           inner join Paciente p on a.PacienteId = p.Id
                           inner join ClassificacaoDeRisco cr on a.Id = cr.AcolhimentoId
                           inner join Profissional pro on pro.Id = cr.ProfissionalId
                           left  join Cbo cbo on cbo.Id = pro.CboId
                           inner join Unidade u on u.Id = pro.UnidadeId
                           left join Endereco e on e.Id = p.EnderecoId
                           where u.Id = @unidadeId
                           and month(cr.DataClassificacao) = @mes
                           and year(cr.DataClassificacao) = @ano

                           union 

                           select 
                           '' as AtendimentoId,
                           '' as PrimeiroDiagnosticoCid,
                           '' as Cid10,
                           '' as SegundoDiagnosticoCid,
                           u.Cnpj as CnpjUnidade,
                           pro.CNS as CnsProfissional,
                           pro.Id as ProfissionalId,
                           p.Nome as NomePaciente,
                           u.CNES as CnesUnidade,
                           p.Email as EmailPaciente,
                           p.TelefoneCelular as TelefonePaciente,
                           e.Bairro as BairroEnderecoPaciente,
                           e.Numero as NumeroPaciente,
                           e.Complemento as ComplementoPaciente,
                           e.Logradouro as LogradouroPaciente,
                           e.CodigoLogradouro as CodigoLogradouro,
                           e.CEP as CepPaciente,
                           e.CodigoIbgeCidade as CodigoIbge,
                           p.Sexo as SexoPaciente,
                           cbo.Codigo as CodigoCbo,
                           p.CNS as CNSPaciente,
                           null as IdProcedimento,
                           cr.DataClassificacao as DataAtendimento,
                           p.DataNascimento as NascimentoPaciente,
                           p.Nacionalidade as Nacionalidade,
                           p.Etinia as PacienteEtinia,
                           p.Raca as Raca,
                           '0301010048' as CodigoProcedimento,
                           pro.Nome as NomeProfissional
                           from Acolhimento a
                           inner join Paciente p on a.PacienteId = p.Id
                           inner join ClassificacaoDeRisco cr on a.Id = cr.AcolhimentoId
                           inner join Profissional pro on pro.Id = cr.ProfissionalId
                           left  join Cbo cbo on cbo.Id = pro.CboId
                           inner join Unidade u on u.Id = pro.UnidadeId
                           left join Endereco e on e.Id = p.EnderecoId
                           where u.Id = @unidadeId
                           and month(cr.DataClassificacao) = @mes
                           and year(cr.DataClassificacao) = @ano

                           union 

                           select 
                           '' as AtendimentoId,
                           '' as PrimeiroDiagnosticoCid,
                           '' as Cid10,
                           '' as SegundoDiagnosticoCid,
                           u.Cnpj as CnpjUnidade,
                           pro.CNS as CnsProfissional,
                           pro.Id as ProfissionalId,
                           p.Nome as NomePaciente,
                           u.CNES as CnesUnidade,
                           p.Email as EmailPaciente,
                           p.TelefoneCelular as TelefonePaciente,
                           e.Bairro as BairroEnderecoPaciente,
                           e.Numero as NumeroPaciente,
                           e.Complemento as ComplementoPaciente,
                           e.Logradouro as LogradouroPaciente,
                           e.CodigoLogradouro as CodigoLogradouro,
                           e.CEP as CepPaciente,
                           e.CodigoIbgeCidade as CodigoIbge,
                           p.Sexo as SexoPaciente,
                           cbo.Codigo as CodigoCbo,
                           p.CNS as CNSPaciente,
                           null as IdProcedimento,
                           cr.DataClassificacao as DataAtendimento,
                           p.DataNascimento as NascimentoPaciente,
                           p.Nacionalidade as Nacionalidade,
                           p.Etinia as PacienteEtinia,
                           p.Raca as Raca,
                           '0301100039' as CodigoProcedimento,
                           pro.Nome as NomeProfissional
                           from Acolhimento a
                           inner join Paciente p on a.PacienteId = p.Id
                           inner join ClassificacaoDeRisco cr on a.Id = cr.AcolhimentoId
                           inner join Profissional pro on pro.Id = cr.ProfissionalId
                           left  join Cbo cbo on cbo.Id = pro.CboId
                           inner join Unidade u on u.Id = pro.UnidadeId
                           inner join SinaisVitais sv on cr.SinaisVitaisId = sv.Id
                           left join Endereco e on e.Id = p.EnderecoId
                           where u.Id = @unidadeId
                           and month(cr.DataClassificacao) = @mes
                           and year(cr.DataClassificacao) = @ano
                           and sv.Pressao is not null

                           union 

                           select 
                           '' as AtendimentoId,
                           '' as PrimeiroDiagnosticoCid,
                           '' as Cid10,
                           '' as SegundoDiagnosticoCid,
                           u.Cnpj as CnpjUnidade,
                           pro.CNS as CnsProfissional,
                           pro.Id as ProfissionalId,
                           p.Nome as NomePaciente,
                           u.CNES as CnesUnidade,
                           p.Email as EmailPaciente,
                           p.TelefoneCelular as TelefonePaciente,
                           e.Bairro as BairroEnderecoPaciente,
                           e.Numero as NumeroPaciente,
                           e.Complemento as ComplementoPaciente,
                           e.Logradouro as LogradouroPaciente,
                           e.CodigoLogradouro as CodigoLogradouro,
                           e.CEP as CepPaciente,
                           e.CodigoIbgeCidade as CodigoIbge,
                           p.Sexo as SexoPaciente,
                           cbo.Codigo as CodigoCbo,
                           p.CNS as CNSPaciente,
                           null as IdProcedimento,
                           cr.DataClassificacao as DataAtendimento,
                           p.DataNascimento as NascimentoPaciente,
                           p.Nacionalidade as Nacionalidade,
                           p.Etinia as PacienteEtinia,
                           p.Raca as Raca,
                           '0101040024' as CodigoProcedimento,
                           pro.Nome as NomeProfissional
                           from Acolhimento a
                           inner join Paciente p on a.PacienteId = p.Id
                           inner join ClassificacaoDeRisco cr on a.Id = cr.AcolhimentoId
                           inner join Profissional pro on pro.Id = cr.ProfissionalId
                           left  join Cbo cbo on cbo.Id = pro.CboId
                           inner join Unidade u on u.Id = pro.UnidadeId
                           inner join SinaisVitais sv on cr.SinaisVitaisId = sv.Id
                           left join Endereco e on e.Id = p.EnderecoId
                           where u.Id = @unidadeId
                           and month(cr.DataClassificacao) = @mes
                           and year(cr.DataClassificacao) = @ano
                           and (sv.Peso is not null and sv.Altura is not null)

                           union

                           select ate.Id as AtendimentoId,
                           c.Id as PrimeiroDiagnosticoCid,
                           c.Codigo as Cid10,
                           cid.Id as SegundoDiagnosticoCid,
                           u.Cnpj as CnpjUnidade,
                           pro.CNS as CnsProfissional,
                           pro.Id as ProfissionalId,
                           p.Nome as NomePaciente,
                           u.CNES as CnesUnidade,
                           p.Email as EmailPaciente,
                           p.TelefoneCelular as TelefonePaciente,
                           e.Bairro as BairroEnderecoPaciente,
                           e.Numero as NumeroPaciente,
                           e.Complemento as ComplementoPaciente,
                           e.Logradouro as LogradouroPaciente,
                           e.CodigoLogradouro as CodigoLogradouro,
                           e.CEP as CepPaciente,
                           e.CodigoIbgeCidade as CodigoIbge,
                           p.Sexo as SexoPaciente,
                           cbo.Codigo as CodigoCbo,
                           p.CNS as CNSPaciente,
                           null as IdProcedimento,
                           ate.FimAtendimento as DataAtendimento,
                           p.DataNascimento as NascimentoPaciente,
                           p.Nacionalidade as Nacionalidade,
                           p.Etinia as PacienteEtinia,
                           p.Raca as Raca,
                           EA.CodigoProcedimento as CodigoProcedimento,
                           pro.Nome as NomeProfissional
                           from Acolhimento a
                           inner join Paciente p on a.PacienteId = p.Id
                           inner join Atendimento ate on ate.AcolhimentoId = a.Id
                           inner join ExameAtendimento EA on ate.Id = EA.AtendimentoId
                           inner join Profissional pro on pro.Id = EA.ExecutorId
                           inner join Unidade u on u.Id = pro.UnidadeId
                           left join Endereco e on e.Id = p.EnderecoId
                           left join Cbo cbo on cbo.Id = pro.CboId 
                           left join CID c on c.Id = ate.PrimeiroDiagnosticoId
                           left join CID cid on cid.Id = ate.SegundoDiagnosticoId
                           
                           where  month(EA.DataExecucao) = @mes
                             and year(EA.DataExecucao) = @ano
                             and EA.DataExecucao is not null
                             and EA.CodigoProcedimento is not null;";

                MySqlParameter parameter1 = new MySqlParameter();
                parameter1.ParameterName = "unidadeId";
                parameter1.Value = unidadeId;
                MySqlParameter parameter2 = new MySqlParameter();
                parameter2.ParameterName = "mes";
                parameter2.Value = mes;
                MySqlParameter parameter3 = new MySqlParameter();
                parameter3.ParameterName = "ano";
                parameter3.Value = ano;
                var command = repository.Database.GetDbConnection().CreateCommand();
                command.CommandText = sql;
                command.Parameters.Add(parameter1);
                command.Parameters.Add(parameter2);
                command.Parameters.Add(parameter3);
                repository.Database.OpenConnection();



                using (var result = command.ExecuteReader())
                {
                    while (result.Read())
                    {

                        FaturamentoModel model = new FaturamentoModel();
                        model.AtendimentoId = result["AtendimentoId"] != "" ? Guid.Parse(result["AtendimentoId"].ToString()) : Guid.Empty;
                        model.NomePaciente = result["NomePaciente"].ToString();
                        model.BairoEnderecoPaciente = result["BairroEnderecoPaciente"].ToString();
                        model.CepPaciente = result["CepPaciente"].ToString();
                        model.CnesUnidade = result["CnesUnidade"].ToString();
                        model.CnpjUnidade = result["CnpjUnidade"].ToString();
                        model.CnsProfissional = result["CnsProfissional"].ToString();
                        model.CodigoCboProfissional = result["CodigoCbo"].ToString();
                        model.CodigoIbgeMunicipioPaciente = result["CodigoIbge"].ToString();
                        model.ComplementoEnderecoPaciente = result["ComplementoPaciente"].ToString();
                        model.EmailPaciente = result["EmailPaciente"].ToString();
                        model.SexoPaciente = result["SexoPaciente"].ToString();
                        model.TelefonePaciente = result["TelefonePaciente"].ToString();
                        model.CNSPaciente = result["CNSPaciente"].ToString();
                        model.DataAtendimento = result["DataAtendimento"].ToString();
                        model.NascimentoPaciente = result["NascimentoPaciente"].ToString();
                        model.Nacionalidade = result["Nacionalidade"].ToString();

                        int numeroEndereco = 0;
                        if (!string.IsNullOrWhiteSpace(result["NumeroPaciente"].ToString()) && Int32.TryParse(result["NumeroPaciente"].ToString(), out numeroEndereco))
                            model.NumeroEnderecoPaciente = numeroEndereco;

                        model.Competencia = mes >= 10 ? string.Format("{0}{1}", ano.ToString(), mes.ToString()) : string.Format("{0}0{1}", ano.ToString(), mes.ToString());

                        model.PacienteIdade = CalculaIdade(model.NascimentoPaciente);
                        model.PacienteEtinia = result["PacienteEtinia"].ToString();
                        model.PacienteRaca = result["Raca"].ToString();
                        model.LogradouroPaciente = result["LogradouroPaciente"].ToString();
                        model.CodigoLogradouroPaciente = result["CodigoLogradouro"].ToString();
                        model.CodigoProcedimento = result["CodigoProcedimento"].ToString();
                        model.Cid10 = result["Cid10"].ToString();
                        if (result["NomeProfissional"] != DBNull.Value)
                            model.NomeProfissional = result["NomeProfissional"].ToString();

                        model.CaracterDoAtendimento = "02"; /// Urgência

                        /// Informações para header
                        model.OrgaoDestino = "Estado do Maranhão";
                        model.IndicadorOrgaoDestino = "M";
                        model.NomeOrgaoResponsavel = "Prefeitura Municipal de Chapadinha";
                        model.SiglaOrgaoResponsavel = "PMC";
                        model.VersaoSistema = "1.0.0.0";


                        listaFaturamento.Add(model);
                        index = index + 1;
                    }

                }

                return listaFaturamento;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<FaturamentoModel> ListaAtendimentosParaFaturamento(string unidadeId, int mes, int ano)
        {
            List<FaturamentoModel> listaFaturamento = new List<FaturamentoModel>();
            int index = 0;
            try
            {
                string sql = @"select 
                           ate.Id as AtendimentoId,
                           c.Id as PrimeiroDiagnosticoCid,
                           c.Codigo as Cid10,
                           cid.Id as SegundoDiagnosticoCid,
                           u.Cnpj as CnpjUnidade,
                           pro.CNS as CnsProfissional,
                           pro.Id as ProfissionalId,
                           p.Nome as NomePaciente,
                           u.CNES as CnesUnidade,
                           p.Email as EmailPaciente,
                           p.TelefoneCelular as TelefonePaciente,
                           e.Bairro as BairroEnderecoPaciente,
                           e.Numero as NumeroPaciente,
                           e.Complemento as ComplementoPaciente,
                           e.Logradouro as LogradouroPaciente,
                           e.CodigoLogradouro as CodigoLogradouro,
                           e.CEP as CepPaciente,
                           e.CodigoIbgeCidade as CodigoIbge,
                           p.Sexo as SexoPaciente,
                           cbo.Codigo as CodigoCbo,
                           p.CNS as CNSPaciente,
                           null as IdProcedimento,
                           ate.FimAtendimento as DataAtendimento,
                           p.DataNascimento as NascimentoPaciente,
                           p.Nacionalidade as Nacionalidade,
                           p.Etinia as PacienteEtinia,
                           case I.DataInternacao when null then null  else datediff(ate.FimAtendimento, I.DataInternacao) end as diasInternacao,
                           ap.CodigoProcedimento
                           p.Raca as Raca
                           from Acolhimento a
                           inner join Paciente p on a.PacienteId = p.Id
                           inner join Atendimento ate on ate.AcolhimentoId = a.Id
                           inner join Profissional pro on pro.Id = ate.ProfissionalId
                           left  join Cbo cbo on cbo.Id = pro.CboId
                           inner join Unidade u on u.Id = pro.UnidadeId
                           left join Endereco e on e.Id = p.EnderecoId
                           left join CID c on c.Id = ate.PrimeiroDiagnosticoId
                           left join CID cid on cid.Id = ate.SegundoDiagnosticoId
                           left join AtendimentoProcedimento ap on ate.Id = ap.AtendimentoId
                           left join Procedimento pr on ap.ProcedimentoId = pr.Id
                           where u.Id = @unidadeId
                           and month(ate.FimAtendimento) = @mes
                           and year(ate.FimAtendimento) = @ano

                           union 

                           select 
                           NULL as AtendimentoId,
                           NULL as PrimeiroDiagnosticoCid,
                           NULL as Cid10,
                           NULL as SegundoDiagnosticoCid,
                           u.Cnpj as CnpjUnidade,
                           pro.CNS as CnsProfissional,
                           pro.Id as ProfissionalId,
                           p.Nome as NomePaciente,
                           u.CNES as CnesUnidade,
                           p.Email as EmailPaciente,
                           p.TelefoneCelular as TelefonePaciente,
                           e.Bairro as BairroEnderecoPaciente,
                           e.Numero as NumeroPaciente,
                           e.Complemento as ComplementoPaciente,
                           e.Logradouro as LogradouroPaciente,
                           e.CodigoLogradouro as CodigoLogradouro,
                           e.CEP as CepPaciente,
                           e.CodigoIbgeCidade as CodigoIbge,
                           p.Sexo as SexoPaciente,
                           cbo.Codigo as CodigoCbo,
                           p.CNS as CNSPaciente,
                           null as IdProcedimento,
                           cr.DataClassificacao as DataAtendimento,
                           p.DataNascimento as NascimentoPaciente,
                           p.Nacionalidade as Nacionalidade,
                           p.Etinia as PacienteEtinia,
                           p.Raca as Raca,
                           '0301060118' as CodigoProcedimento
                           from Acolhimento a
                           inner join Paciente p on a.PacienteId = p.Id
                           inner join ClassificacaoDeRisco cr on a.Id = cr.AcolhimentoId
                           inner join Profissional pro on pro.Id = cr.ProfissionalId
                           left  join Cbo cbo on cbo.Id = pro.CboId
                           inner join Unidade u on u.Id = pro.UnidadeId
                           left join Endereco e on e.Id = p.EnderecoId
                           where u.Id = @unidadeId
                           and month(cr.DataClassificacao) = @mes
                           and year(cr.DataClassificacao) = @ano


union

                           select ate.Id as AtendimentoId,
                           c.Id as PrimeiroDiagnosticoCid,
                           c.Codigo as Cid10,
                           cid.Id as SegundoDiagnosticoCid,
                           u.Cnpj as CnpjUnidade,
                           pro.CNS as CnsProfissional,
                           pro.Id as ProfissionalId,
                           p.Nome as NomePaciente,
                           u.CNES as CnesUnidade,
                           p.Email as EmailPaciente,
                           p.TelefoneCelular as TelefonePaciente,
                           e.Bairro as BairroEnderecoPaciente,
                           e.Numero as NumeroPaciente,
                           e.Complemento as ComplementoPaciente,
                           e.Logradouro as LogradouroPaciente,
                           e.CodigoLogradouro as CodigoLogradouro,
                           e.CEP as CepPaciente,
                           e.CodigoIbgeCidade as CodigoIbge,
                           p.Sexo as SexoPaciente,
                           cbo.Codigo as CodigoCbo,
                           p.CNS as CNSPaciente,
                           null as IdProcedimento,
                           ate.FimAtendimento as DataAtendimento,
                           p.DataNascimento as NascimentoPaciente,
                           p.Nacionalidade as Nacionalidade,
                           p.Etinia as PacienteEtinia,
                           p.Raca as Raca,
                           EA.CodigoProcedimento as CodigoProcedimento
                           from Acolhimento a
                           inner join Paciente p on a.PacienteId = p.Id
                           inner join Atendimento ate on ate.AcolhimentoId = a.Id
                           inner join ExameAtendimento EA on ate.Id = EA.AtendimentoId
                           inner join Profissional pro on pro.Id = EA.ExecutorId
                           inner join Unidade u on u.Id = pro.UnidadeId
                           left join Endereco e on e.Id = p.EnderecoId
                           left join Cbo cbo on cbo.Id = pro.CboId 
                           left join CID c on c.Id = ate.PrimeiroDiagnosticoId
                           left join CID cid on cid.Id = ate.SegundoDiagnosticoId
                          where  month(EA.DataExecucao) = @mes
                             and year(EA.DataExecucao) = @ano
                             and EA.DataExecucao is not null
                             and EA.CodigoProcedimento is not null;
";

                MySqlParameter parameter1 = new MySqlParameter();
                parameter1.ParameterName = "unidadeId";
                parameter1.Value = unidadeId;
                MySqlParameter parameter2 = new MySqlParameter();
                parameter2.ParameterName = "mes";
                parameter2.Value = mes;
                MySqlParameter parameter3 = new MySqlParameter();
                parameter3.ParameterName = "ano";
                parameter3.Value = ano;
                var command = repository.Database.GetDbConnection().CreateCommand();
                command.CommandText = sql;
                command.Parameters.Add(parameter1);
                command.Parameters.Add(parameter2);
                command.Parameters.Add(parameter3);
                repository.Database.OpenConnection();

                using (var result = command.ExecuteReader())
                {
                    while (result.Read())
                    {

                        FaturamentoModel model = new FaturamentoModel();
                        model.AtendimentoId = Guid.Parse(result["AtendimentoId"].ToString());
                        model.NomePaciente = result["NomePaciente"].ToString();
                        model.BairoEnderecoPaciente = result["BairroEnderecoPaciente"].ToString();
                        model.CepPaciente = result["CepPaciente"].ToString();
                        model.CnesUnidade = result["CnesUnidade"].ToString();
                        model.CnpjUnidade = result["CnpjUnidade"].ToString();
                        model.CnsProfissional = result["CnsProfissional"].ToString();
                        model.CodigoCboProfissional = result["CodigoCbo"].ToString();
                        model.CodigoIbgeMunicipioPaciente = result["CodigoIbge"].ToString();
                        model.ComplementoEnderecoPaciente = result["ComplementoPaciente"].ToString();
                        model.EmailPaciente = result["EmailPaciente"].ToString();
                        model.SexoPaciente = result["SexoPaciente"].ToString();
                        model.TelefonePaciente = result["TelefonePaciente"].ToString();
                        model.CNSPaciente = result["CNSPaciente"].ToString();
                        model.DataAtendimento = result["DataAtendimento"].ToString();
                        model.NascimentoPaciente = result["NascimentoPaciente"].ToString();
                        model.Nacionalidade = result["Nacionalidade"].ToString();


                        int numeroEndereco = 0;
                        if (!string.IsNullOrWhiteSpace(result["NumeroPaciente"].ToString()) && Int32.TryParse(result["NumeroPaciente"].ToString(), out numeroEndereco))
                            model.NumeroEnderecoPaciente = numeroEndereco;

                        model.Competencia = mes >= 10 ? string.Format("{0}{1}", ano.ToString(), mes.ToString()) : string.Format("{0}0{1}", ano.ToString(), mes.ToString());

                        model.PacienteIdade = CalculaIdade(model.NascimentoPaciente);
                        model.PacienteRaca = result["Raca"].ToString();
                        model.PacienteEtinia = TrataEtinia(result["PacienteEtinia"].ToString());
                        model.LogradouroPaciente = result["LogradouroPaciente"].ToString();
                        model.CodigoLogradouroPaciente = result["CodigoLogradouro"].ToString();
                        model.CodigoProcedimento = result["CodigoProcedimento"].ToString();
                        model.Cid10 = result["Cid10"].ToString();
                        model.DiasInternacao = result["diasInternacao"].ToString();


                        model.CaracterDoAtendimento = "02"; /// Urgência

                        /// Informações para header
                        model.OrgaoDestino = "Estado do Maranhão";
                        model.IndicadorOrgaoDestino = "M";
                        model.NomeOrgaoResponsavel = "Prefeitura Municipal de Chapadinha";
                        model.SiglaOrgaoResponsavel = "PMC";
                        model.VersaoSistema = "1.0.0.0";


                        listaFaturamento.Add(model);
                    }
                    index = index + 1;
                }

                return listaFaturamento;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<FaturamentoModel> ListaParaFaturamentoPorPeriodo(string unidadeId, DateTime inicio, DateTime fim)
        {
            List<FaturamentoModel> listaFaturamento = new List<FaturamentoModel>();
            int index = 0;
            fim = fim.AddDays(1);
            try
            {
                string sql = @"select a.Id as AcolhimentoId,
                           ate.Id as AtendimentoId,
                           c.Id as PrimeiroDiagnosticoCid,
                           c.Codigo as Cid10,
                           cid.Id as SegundoDiagnosticoCid,
                           u.Cnpj as CnpjUnidade,
                           pro.CNS as CnsProfissional,
                           pro.Id as ProfissionalId,
                           p.Nome as NomePaciente,
                           u.CNES as CnesUnidade,
                           p.Email as EmailPaciente,
                           p.TelefoneCelular as TelefonePaciente,
                           e.Bairro as BairroEnderecoPaciente,
                           e.Numero as NumeroPaciente,
                           e.Complemento as ComplementoPaciente,
                           e.Logradouro as LogradouroPaciente,
                           e.CodigoLogradouro as CodigoLogradouro,
                           e.CEP as CepPaciente,
                           e.CodigoIbgeCidade as CodigoIbge,
                           p.Sexo as SexoPaciente,
                           case ap.CodigoProcedimento when '0301100012' then 
                                (SELECT TOP 1 _Cbo.Codigo
                                   FROM Prescricao _Prescricao
                             INNER JOIN Profissional _Profissional
                                     ON _Prescricao.EnfermeiroId = _Profissional.Id
                             INNER JOIN Cbo _Cbo
                                     ON _Profissional.CboId = _Cbo.Id
								  WHERE _Prescricao.AtendimentoId = ate.Id
                               ORDER BY _Prescricao.RetornoEnfermagem
                                  )
                           else
                            cbo.Codigo end 
                           as CodigoCbo,
                           p.CNS as CNSPaciente,
                           null as IdProcedimento,
                           DATE_FORMAT(ate.FimAtendimento,'%d/%m/%Y') as DataAtendimento,
                           p.DataNascimento as NascimentoPaciente,
                           p.Nacionalidade as Nacionalidade,
                           p.Etinia as PacienteEtinia,
                           case I.DataInternacao when null then null  else datediff(ate.FimAtendimento, I.DataInternacao) end as diasInternacao,
                           p.Raca as Raca,
                           ap.CodigoProcedimento,
                           pro.Nome as NomeProfissional
                           from Acolhimento a
                           inner join Paciente p on a.PacienteId = p.Id
                           inner join Atendimento ate on ate.AcolhimentoId = a.Id
                           inner join Profissional pro on pro.Id = ate.ProfissionalId
                           left  join Cbo cbo on cbo.Id = pro.CboId
                           inner join Unidade u on u.Id = pro.UnidadeId
                           left join Endereco e on e.Id = p.EnderecoId
                           left join CID c on c.Id = ate.PrimeiroDiagnosticoId
                           left join CID cid on cid.Id = ate.SegundoDiagnosticoId
                           left join AtendimentoProcedimento ap on ate.Id = ap.AtendimentoId
                           left join Internacao I on ate.Id = I.AtendimentoId
                           where u.Id = @unidadeId
                            and ate.FimAtendimento >= @inicio
                            and ate.FimAtendimento <= @fim

                           union 

                           select a.Id as AcolhimentoId,
                           '' as AtendimentoId,
                           '' as PrimeiroDiagnosticoCid,
                           '' as Cid10,
                           '' as SegundoDiagnosticoCid,
                           u.Cnpj as CnpjUnidade,
                           pro.CNS as CnsProfissional,
                           pro.Id as ProfissionalId,
                           p.Nome as NomePaciente,
                           u.CNES as CnesUnidade,
                           p.Email as EmailPaciente,
                           p.TelefoneCelular as TelefonePaciente,
                           e.Bairro as BairroEnderecoPaciente,
                           e.Numero as NumeroPaciente,
                           e.Complemento as ComplementoPaciente,
                           e.Logradouro as LogradouroPaciente,
                           e.CodigoLogradouro as CodigoLogradouro,
                           e.CEP as CepPaciente,
                           e.CodigoIbgeCidade as CodigoIbge,
                           p.Sexo as SexoPaciente,
                           cbo.Codigo as CodigoCbo,
                           p.CNS as CNSPaciente,
                           NULL as IdProcedimento,
                           DATE_FORMAT(cr.DataClassificacao,'%d/%m/%Y') as DataAtendimento,
                           p.DataNascimento as NascimentoPaciente,
                           p.Nacionalidade as Nacionalidade,
                           p.Etinia as PacienteEtinia,
                           '' as diasInternacao,
                           p.Raca as Raca,
                           '0301060118' as CodigoProcedimento,
                           pro.Nome as NomeProfissional
                           from Acolhimento a
                           inner join Paciente p on a.PacienteId = p.Id
                           inner join ClassificacaoDeRisco cr on a.Id = cr.AcolhimentoId
                           inner join Profissional pro on pro.Id = cr.ProfissionalId
                           left  join Cbo cbo on cbo.Id = pro.CboId
                           inner join Unidade u on u.Id = pro.UnidadeId
                           left join Endereco e on e.Id = p.EnderecoId
                           where u.Id = @unidadeId
                           and cr.DataClassificacao >= @inicio
                           and cr.DataClassificacao <= @fim

                           union

                           select a.Id as AcolhimentoId,
                           '' as AtendimentoId,
                           '' as PrimeiroDiagnosticoCid,
                           '' as Cid10,
                           '' as SegundoDiagnosticoCid,
                           u.Cnpj as CnpjUnidade,
                           pro.CNS as CnsProfissional,
                           pro.Id as ProfissionalId,
                           p.Nome as NomePaciente,
                           u.CNES as CnesUnidade,
                           p.Email as EmailPaciente,
                           p.TelefoneCelular as TelefonePaciente,
                           e.Bairro as BairroEnderecoPaciente,
                           e.Numero as NumeroPaciente,
                           e.Complemento as ComplementoPaciente,
                           e.Logradouro as LogradouroPaciente,
                           e.CodigoLogradouro as CodigoLogradouro,
                           e.CEP as CepPaciente,
                           e.CodigoIbgeCidade as CodigoIbge,
                           p.Sexo as SexoPaciente,
                           cbo.Codigo as CodigoCbo,
                           p.CNS as CNSPaciente,
                           null as IdProcedimento,
                           cr.DataClassificacao as DataAtendimento,
                           p.DataNascimento as NascimentoPaciente,
                           p.Nacionalidade as Nacionalidade,
                           p.Etinia as PacienteEtinia,
                           '' as diasInternacao,
                           p.Raca as Raca,
                           '0301010048' as CodigoProcedimento,
                           pro.Nome as NomeProfissional
                           from Acolhimento a
                           inner join Paciente p on a.PacienteId = p.Id
                           inner join ClassificacaoDeRisco cr on a.Id = cr.AcolhimentoId
                           inner join Profissional pro on pro.Id = cr.ProfissionalId
                           left  join Cbo cbo on cbo.Id = pro.CboId
                           inner join Unidade u on u.Id = pro.UnidadeId
                           left join Endereco e on e.Id = p.EnderecoId
                           where u.Id = @unidadeId
                           and cr.DataClassificacao >= @inicio
                           and cr.DataClassificacao <= @fim


                           union

                           select a.Id as AcolhimentoId,
                           '' as AtendimentoId,
                           '' as PrimeiroDiagnosticoCid,
                           '' as Cid10,
                           '' as SegundoDiagnosticoCid,
                           u.Cnpj as CnpjUnidade,
                           pro.CNS as CnsProfissional,
                           pro.Id as ProfissionalId,
                           p.Nome as NomePaciente,
                           u.CNES as CnesUnidade,
                           p.Email as EmailPaciente,
                           p.TelefoneCelular as TelefonePaciente,
                           e.Bairro as BairroEnderecoPaciente,
                           e.Numero as NumeroPaciente,
                           e.Complemento as ComplementoPaciente,
                           e.Logradouro as LogradouroPaciente,
                           e.CodigoLogradouro as CodigoLogradouro,
                           e.CEP as CepPaciente,
                           e.CodigoIbgeCidade as CodigoIbge,
                           p.Sexo as SexoPaciente,
                           cbo.Codigo as CodigoCbo,
                           p.CNS as CNSPaciente,
                           null as IdProcedimento,
                           cr.DataClassificacao as DataAtendimento,
                           p.DataNascimento as NascimentoPaciente,
                           p.Nacionalidade as Nacionalidade,
                           p.Etinia as PacienteEtinia,
                           '' as diasInternacao,
                           p.Raca as Raca,
                           '0301100039' as CodigoProcedimento,
                           pro.Nome as NomeProfissional
                           from Acolhimento a
                           inner join Paciente p on a.PacienteId = p.Id
                           inner join ClassificacaoDeRisco cr on a.Id = cr.AcolhimentoId
                           inner join Profissional pro on pro.Id = cr.ProfissionalId
                           left  join Cbo cbo on cbo.Id = pro.CboId
                           inner join Unidade u on u.Id = pro.UnidadeId
                           inner join SinaisVitais sv on cr.SinaisVitaisId = sv.Id
                           left join Endereco e on e.Id = p.EnderecoId
                           where u.Id = @unidadeId
                           and cr.DataClassificacao >= @inicio
                           and cr.DataClassificacao <= @fim
                           and sv.Pressao is not null
    
                           union 

                           select a.Id as AcolhimentoId,
                           '' as AtendimentoId,
                           '' as PrimeiroDiagnosticoCid,
                           '' as Cid10,
                           '' as SegundoDiagnosticoCid,
                           u.Cnpj as CnpjUnidade,
                           pro.CNS as CnsProfissional,
                           pro.Id as ProfissionalId,
                           p.Nome as NomePaciente,
                           u.CNES as CnesUnidade,
                           p.Email as EmailPaciente,
                           p.TelefoneCelular as TelefonePaciente,
                           e.Bairro as BairroEnderecoPaciente,
                           e.Numero as NumeroPaciente,
                           e.Complemento as ComplementoPaciente,
                           e.Logradouro as LogradouroPaciente,
                           e.CodigoLogradouro as CodigoLogradouro,
                           e.CEP as CepPaciente,
                           e.CodigoIbgeCidade as CodigoIbge,
                           p.Sexo as SexoPaciente,
                           cbo.Codigo as CodigoCbo,
                           p.CNS as CNSPaciente,
                           null as IdProcedimento,
                           cr.DataClassificacao as DataAtendimento,
                           p.DataNascimento as NascimentoPaciente,
                           p.Nacionalidade as Nacionalidade,
                           p.Etinia as PacienteEtinia,
                           '' as diasInternacao,
                           p.Raca as Raca,
                           '0101040024' as CodigoProcedimento,
                           pro.Nome as NomeProfissional
                           from Acolhimento a
                           inner join Paciente p on a.PacienteId = p.Id
                           inner join ClassificacaoDeRisco cr on a.Id = cr.AcolhimentoId
                           inner join Profissional pro on pro.Id = cr.ProfissionalId
                           left  join Cbo cbo on cbo.Id = pro.CboId
                           inner join Unidade u on u.Id = pro.UnidadeId
                           inner join SinaisVitais sv on cr.SinaisVitaisId = sv.Id
                           left join Endereco e on e.Id = p.EnderecoId
                           where u.Id = @unidadeId
                           and cr.DataClassificacao >= @inicio
                           and cr.DataClassificacao <= @fim
                           and (sv.Peso is not null and sv.Altura is not null)

                           union

                           select a.Id as AcolhimentoId,
                           ate.Id as AtendimentoId,
                           c.Id as PrimeiroDiagnosticoCid,
                           c.Codigo as Cid10,
                           cid.Id as SegundoDiagnosticoCid,
                           u.Cnpj as CnpjUnidade,
                           pro.CNS as CnsProfissional,
                           pro.Id as ProfissionalId,
                           p.Nome as NomePaciente,
                           u.CNES as CnesUnidade,
                           p.Email as EmailPaciente,
                           p.TelefoneCelular as TelefonePaciente,
                           e.Bairro as BairroEnderecoPaciente,
                           e.Numero as NumeroPaciente,
                           e.Complemento as ComplementoPaciente,
                           e.Logradouro as LogradouroPaciente,
                           e.CodigoLogradouro as CodigoLogradouro,
                           e.CEP as CepPaciente,
                           e.CodigoIbgeCidade as CodigoIbge,
                           p.Sexo as SexoPaciente,
                           cbo.Codigo as CodigoCbo,
                           p.CNS as CNSPaciente,
                           null as IdProcedimento,
                           DATE_FORMAT(EA.DataExecucao,'%d/%m/%Y') as DataAtendimento,
                           p.DataNascimento as NascimentoPaciente,
                           p.Nacionalidade as Nacionalidade ,
                           p.Etinia as PacienteEtinia ,
                           '' as diasInternacao,
                           p.Raca as Raca,
                           EA.CodigoProcedimento as CodigoProcedimento,
                           pro.Nome as NomeProfissional
                           from Acolhimento a
                           inner join Paciente p on a.PacienteId = p.Id
                           inner join Atendimento ate on ate.AcolhimentoId = a.Id
                           inner join ExameAtendimento EA on ate.Id = EA.AtendimentoId
                           inner join Profissional pro on pro.Id = EA.ExecutorId
                           inner join Unidade u on u.Id = pro.UnidadeId
                           left join Endereco e on e.Id = p.EnderecoId
                           left join Cbo cbo on cbo.Id = pro.CboId 
                           left join CID c on c.Id = ate.PrimeiroDiagnosticoId
                           left join CID cid on cid.Id = ate.SegundoDiagnosticoId
                          where EA.DataExecucao >= @inicio
                            and EA.DataExecucao <= @fim
                             and EA.CodigoProcedimento is not null;
";

                MySqlParameter parameter1 = new MySqlParameter();
                parameter1.ParameterName = "unidadeId";
                parameter1.Value = unidadeId;
                MySqlParameter parameter2 = new MySqlParameter();
                parameter2.ParameterName = "inicio";
                parameter2.Value = inicio;
                MySqlParameter parameter3 = new MySqlParameter();
                parameter3.ParameterName = "fim";
                parameter3.Value = fim;
                var command = repository.Database.GetDbConnection().CreateCommand();
                command.CommandText = sql;
                command.Parameters.Add(parameter1);
                command.Parameters.Add(parameter2);
                command.Parameters.Add(parameter3);
                repository.Database.OpenConnection();
                using (var result = command.ExecuteReader())
                {
                    fim = fim.AddDays(-1);
                    while (result.Read())
                    {

                        FaturamentoModel model = new FaturamentoModel();
                        model.AcolhimentoId = Guid.Parse(result["AcolhimentoId"].ToString());
                        model.AtendimentoId = result["AtendimentoId"] != "" ? Guid.Parse(result["AtendimentoId"].ToString()) : Guid.Empty;
                        model.NomePaciente = result["NomePaciente"].ToString();
                        model.BairoEnderecoPaciente = result["BairroEnderecoPaciente"].ToString();
                        model.CepPaciente = result["CepPaciente"].ToString();
                        model.CnesUnidade = result["CnesUnidade"].ToString();
                        model.CnpjUnidade = result["CnpjUnidade"].ToString();
                        model.CnsProfissional = result["CnsProfissional"].ToString();
                        model.CodigoCboProfissional = result["CodigoCbo"].ToString();
                        model.CodigoIbgeMunicipioPaciente = result["CodigoIbge"].ToString();
                        model.ComplementoEnderecoPaciente = result["ComplementoPaciente"].ToString();
                        model.EmailPaciente = result["EmailPaciente"].ToString();
                        model.SexoPaciente = result["SexoPaciente"].ToString();
                        model.TelefonePaciente = result["TelefonePaciente"].ToString();
                        model.CNSPaciente = result["CNSPaciente"].ToString();
                        model.DataAtendimento = result["DataAtendimento"].ToString();
                        model.NascimentoPaciente = result["NascimentoPaciente"].ToString();
                        model.Nacionalidade = result["Nacionalidade"].ToString();

                        int numeroEndereco = 0;
                        if (!string.IsNullOrWhiteSpace(result["NumeroPaciente"].ToString()) && Int32.TryParse(result["NumeroPaciente"].ToString(), out numeroEndereco))
                            model.NumeroEnderecoPaciente = numeroEndereco;

                        model.Competencia = fim.Month >= 10 ? string.Format("{0}{1}", fim.Year.ToString(), fim.Month.ToString()) : string.Format("{0}0{1}", fim.Year.ToString(), fim.Month.ToString());

                        model.PacienteIdade = CalculaIdade(model.NascimentoPaciente);
                        model.PacienteRaca = result["Raca"].ToString();
                        model.PacienteEtinia = TrataEtinia(result["PacienteEtinia"].ToString());
                        model.LogradouroPaciente = result["LogradouroPaciente"].ToString();
                        model.CodigoLogradouroPaciente = result["CodigoLogradouro"].ToString();
                        model.CodigoProcedimento = result["CodigoProcedimento"].ToString();
                        model.Cid10 = result["Cid10"].ToString();
                        model.DiasInternacao = result["diasInternacao"].ToString();
                        model.NomeProfissional = result["NomeProfissional"].ToString();

                        model.CaracterDoAtendimento = "02"; /// Urgência

                        /// Informações para header
                        model.OrgaoDestino = "Estado do Maranhão";
                        model.IndicadorOrgaoDestino = "M";
                        model.NomeOrgaoResponsavel = "Prefeitura Municipal de Chapadinha";
                        model.SiglaOrgaoResponsavel = "PMC";
                        model.VersaoSistema = "1.0.0.0";

                        listaFaturamento.Add(model);
                    }
                    index = index + 1;
                }

                return listaFaturamento;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        private string TrataEtinia(string etnia)
        {
            switch (etnia)
            {
                case "B":
                    return "01";
                case "p":
                    return "03";
                case "N":
                    return "02";
                case "I":
                    return "05";
                case "A":
                    return "04";
                default:
                    return "99";
            }
        }

        private int CalculaIdade(string DataNascimento)
        {
            if (!string.IsNullOrWhiteSpace(DataNascimento))
            {
                var data = Convert.ToDateTime(DataNascimento);
                int idade = DateTime.Today.Year - data.Year;
                if (DateTime.Today < data.AddYears(idade)) idade--;

                return idade;
            }
            else
                return 0;
        }


    }
}
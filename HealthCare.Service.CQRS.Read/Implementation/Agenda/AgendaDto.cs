﻿using HealtCare.Infra.Common.Utils;
using HealthCare.Data.Context;
using HealthCare.Domain.Entities.Agenda;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.CQRS.Read.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HealthCare.Infra.CQRS.Read.Implementation
{
    public class AgendaDto : IAgendaDao
    {
        private Func<HealthCareDbContext> contextFactory;
        public AgendaDto(Func<HealthCareDbContext> contextFactory)
        {
            this.contextFactory = contextFactory;
        }

        public IList<AgendaReadViewModel> ListarAgendas(Guid unidadeId)
        {
            using (var repository = contextFactory.Invoke())
            {
                
                return ViewModelUtils.ToListViewModel<Agenda, AgendaReadViewModel>(repository.Agenda.Include("Unidade").Where(x => x.Unidade.Id == unidadeId)).ToList();
            }
        }
    }
}
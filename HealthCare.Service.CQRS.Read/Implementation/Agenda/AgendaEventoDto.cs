﻿using HealtCare.Infra.Common.Utils;
using HealthCare.Data.Context;
using HealthCare.Domain.Entities;
using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.CQRS.Read.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Implementation
{
    public class AgendaEventoDto : IAgendaEventoDao
    {
        private Func<HealthCareDbContext> contextFactory;
        public AgendaEventoDto(Func<HealthCareDbContext> contextFactory)
        {
            this.contextFactory = contextFactory;
        }

        public IList<AgendaEvento> ListarEventosPorMes(int mes, int ano, Guid unidadeId)
        {
            using (var repository = contextFactory.Invoke())
            {
                var eventos = repository.Query<AgendaEvento>("Agenda", "Agenda.Unidade", "Paciente", "Servico", "Agenda.Profissional")
                                                            .Where(x => x.HoraFim.Year == ano && x.HoraFim.Month == mes
                                                                     && x.Agenda.Unidade.Id == unidadeId).ToList();
                return eventos;
            }
        }

        public IList<AgendaEvento> ListarEventos(Guid agendaId)
        {
            using (var repository = contextFactory.Invoke())
            {
                return repository.Query<AgendaEvento>("Agenda", "Paciente", "Servico", "Agenda.Profissional").Where(x => x.Agenda.Id == agendaId).ToList();
            }
        }

        public ListResult ListarFilaProfissional(Guid profissionalId)
        {
            using (var repository = contextFactory.Invoke())
            {
                try
                {
                    var lista = repository.AgendaEvento.Include("Paciente").Include("Servico").Include("Agenda")
                                                     .Where(x => x.Agenda.ProfissionalId == profissionalId && x.HoraFim.Day == DateTime.Today.Day && x.Paciente != null)
                                                     .OrderBy(x => x.HoraInicio).ToList().Select(y => new FilaAtendimentoViewModel()
                                                     {
                                                         Id = y.Id,
                                                         HoraInicio = y.HoraInicio.ToString("HH:mm:ss"),
                                                         Descricao = y.Servico.Descricao,
                                                         Nome = y.Paciente.Nome
                                                     }).ToList();

                    return ListResult.GetListResult(lista, new string[] { "Descricao", "Nome", "Id", "HoraInicio" });
                }
                catch (Exception ex)
                {

                    throw;
                }
            }
        }

        public IList<AgendaEvento> ListarEventosPorProfissional(Guid profissionalId)
        {
            using (var repository = contextFactory.Invoke())
            {
                return repository.Query<AgendaEvento>("Agenda").Where(x => x.Agenda.Profissional.Id == profissionalId).ToList();
            }
        }

        public IList<AgendaEvento> ListarEventosPorPaciente(Guid pacienteId)
        {
            using (var repository = contextFactory.Invoke())
            {
                return repository.Query<AgendaEvento>("Paciente").Where(x => x.Paciente.Id == pacienteId).ToList();
            }
        }

        public IList<AgendaEvento> ListarEventosPorAno(int ano, Guid unidadeId)
        {
            using (var repository = contextFactory.Invoke())
            {
                var eventos = repository.Query<AgendaEvento>("Agenda", "Agenda.Unidade", "Paciente", "Servico", "Agenda.Profissional")
                                        .Where(x => x.HoraFim.Year == ano && x.HoraFim.Month >= DateTime.Now.Month
                                                 && x.Agenda.Unidade.Id == unidadeId).ToList();
                return eventos;
            }
        }

        public AgendaEvento GetEventoPorId(Guid eventoId)
        {
            using (var repository = contextFactory.Invoke())
            {
                return repository.Query<AgendaEvento>("Agenda", "Paciente", "Servico", "Agenda.Profissional").FirstOrDefault(x => x.Id == eventoId);
            }
        }

        public IList<AgendaEvento> ListarEventosPorSemana(DateTime inicio, DateTime fim, Guid profissionalId)
        {
            using (var repository = contextFactory.Invoke())
            {
                var eventos = repository.Query<AgendaEvento>("Agenda", "Agenda.Unidade", "Paciente", "Servico", "Agenda.Profissional")
                                                            .Where(x => x.HoraInicio >= inicio && x.HoraFim <= fim
                                                                     && x.Agenda.Profissional.Id == profissionalId).ToList();
                return eventos;
            }
        }
    }
}
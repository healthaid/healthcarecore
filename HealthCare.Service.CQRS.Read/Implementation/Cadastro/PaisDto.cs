﻿using HealtCare.Infra.Common.Utils;
using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.Data.Context;
using System;
using System.Linq;

namespace HealthCare.Infra.CQRS.Read.Implementation
{
    public class PaisDto : IPaisDao
    {
        private HealthCareDbContext repository;
        public PaisDto(HealthCareDbContext contextFactory)
        {
            this.repository = contextFactory;
        }

        public ListResult ListaPais(string nome)
        {
            try
            {
                var lista = ViewModelUtils.ToListViewModel<Pais, PaisReadModel>(repository.Query<Pais>()).Where(x => x.Descricao.Contains(nome.ToUpper())).OrderBy(x => x.Descricao).ToList();
                ListResult result = ListResult.GetListResult(lista, new string[] { "Id", "Descricao", "CodigoSUS" });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ListResult ListaPaises()
        {
            try
            {
                var lista = ViewModelUtils.ToListViewModel<Pais, PaisReadModel>(repository.Query<Pais>()).OrderBy(x => x.Descricao).ToList();
                ListResult result = ListResult.GetListResult(lista, new string[] { "Id", "Descricao", "CodigoSUS" });
                return result;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}

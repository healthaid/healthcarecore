﻿using HealtCare.Infra.Common.Utils;
using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.Data.Context;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HealthCare.Infra.CQRS.Read.Implementation
{
    public class NaturalidadeDto : INaturalidadeDao
    {
        private HealthCareDbContext repository;
        public NaturalidadeDto(HealthCareDbContext contextFactory)
        {
            this.repository = contextFactory;
        }
        public IList<NaturalidadeViewModel> ListarNaturalidade()
        {
            try
            {
                return ViewModelUtils.ToListViewModel<Naturalidade, NaturalidadeViewModel>(repository.Query<Naturalidade>().Where(x => x.Ativo == true)).ToList();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}

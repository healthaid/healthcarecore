﻿using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.Data.Context;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HealthCare.Infra.CQRS.Read.Implementation
{
    public class ProfissaoDto : IProfissaoDao
    {
        private HealthCareDbContext repository;
        public ProfissaoDto(HealthCareDbContext contextFactory)
        {
            this.repository = contextFactory;
        }


        public IList<Profissao> ListarProfissao()
        {
            try
            {
                return repository.Query<Profissao>().Where(x => x.Ativo == true).ToList();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

      
    }
}

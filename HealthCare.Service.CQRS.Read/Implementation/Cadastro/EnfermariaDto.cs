﻿using HealtCare.Infra.Common.Utils;
using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace HealthCare.Infra.CQRS.Read.Implementation
{
    public class EnfermariaDto : IEnfermariaDao
    {
        private HealthCareDbContext repository;
        public EnfermariaDto(HealthCareDbContext contextFactory)
        {
            this.repository = contextFactory;
        }
        public ListResult ListaEnfermarias(bool ativas,Guid unidadeId)
        {
            try
            {
                var lista = ViewModelUtils.ToListViewModel<Enfermaria, EnfermariaReadModel>(repository.Enfermaria.Include("Unidade").Where(x => x.Ativo == ativas && x.Unidade.Id == unidadeId)).ToList();
                ListResult result = ListResult.GetListResult(lista, new string[] { "Id", "Nome" });
                return result;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public EnfermariaReadModel GetEnfermariaById(Guid id)
        {
            try
            {
                return ViewModelUtils.ToViewModel<Enfermaria, EnfermariaReadModel>(repository.Enfermaria.FirstOrDefault(x => x.Id == id));
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}

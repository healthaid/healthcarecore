﻿using HealtCare.Infra.Common.Utils;
using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.Data.Context;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HealthCare.Infra.CQRS.Read.Implementation
{
    public class ClinicaDto : IClinicaDao
    {
        private Func<HealthCareDbContext> contextFactory;
        public ClinicaDto(Func<HealthCareDbContext> contextFactory)
        {
            this.contextFactory = contextFactory;
        }
        public IList<ClinicaViewModel> ListarClinicas()
        {
            using (var repository = contextFactory.Invoke())
            {
                return ViewModelUtils.ToListViewModel<Clinica, ClinicaViewModel>(repository.Query<Clinica>()).ToList();
            }
        }
    }
}

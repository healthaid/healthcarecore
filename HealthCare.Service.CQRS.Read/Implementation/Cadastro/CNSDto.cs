﻿using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.Data.Context;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HealthCare.Infra.CQRS.Read.Implementation
{
    public class CNSDto : ICNSDao
    {
        private HealthCareDbContext repository;
        public CNSDto(HealthCareDbContext contextFactory)
        {
            this.repository = contextFactory;
        }
        public IList<CNS> ListarCNSs()
        {
            try
            {
                return repository.Query<CNS>().Where(x => x.Ativo == true).ToList();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public IList<CNS> ListarCNSPorPaciente(Guid pacienteId)
        {
            try
            {
                return repository.Query<CNS>("Paciente").Where(x => x.Ativo == true && x.Paciente.Id == pacienteId).ToList();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}

﻿using HealtCare.Infra.Common.Utils;
using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.Data.Context;
using System;
using System.Linq;

namespace HealthCare.Infra.CQRS.Read.Implementation
{
    public class SetorClinicaDto : ISetorClinicaDao
    {
        private HealthCareDbContext repository;
        public SetorClinicaDto(HealthCareDbContext contextFactory)
        {
            this.repository = contextFactory;
        }

        public ListResult ListaSetorClinica()
        {
            try
            {
                var lista =  ViewModelUtils.ToListViewModel<SetorClinica, SetorClinicaReadModel>(repository.Query<SetorClinica>()).ToList();
                ListResult result = ListResult.GetListResult(lista, new string[] { "Id", "Descricao", "Ativo" });
                return result;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public SetorClinicaReadModel GetSetorClinicaById(Guid id)
        {
            try
            {
                return ViewModelUtils.ToViewModel<SetorClinica, SetorClinicaReadModel>(repository.SetorClinica.FirstOrDefault(x => x.Id == id));
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}

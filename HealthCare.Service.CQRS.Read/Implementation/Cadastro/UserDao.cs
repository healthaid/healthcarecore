﻿using AutoMapper;
using HealtCare.Infra.Common.Utils;
using HealthCare.Domain.Entities.DbClasses.Admin;
using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Infra.CQRS.Read.Interfaces.Cadastro;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.Data.Context;
using HealthCare.Service.CQRS.Read.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HealthCare.Infra.CQRS.Read.Implementation.Cadastro
{
    public class UserDao : IUserDao
    {
        private HealthCareDbContext _contextFactory;

        public UserDao(HealthCareDbContext contextFactory, IMapper mapper)
        {
            _contextFactory = contextFactory;
            ViewModelUtils.mapper = mapper;
        }

        public List<RoleReadModel> GetRoles()
        {
            try
            {
                return ViewModelUtils.ToListViewModel<Role, RoleReadModel>(_contextFactory.Roles.ToList()).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public UserReadViewModel GetUserById(Guid id)
        {
            try
            {
                Usuario user = _contextFactory.Users.Include("Profissional").Include("Profissional.Unidade").Include("Profissional.Cbo").Where(x => x.Id == id).FirstOrDefault();
                if (user != null)
                {
                    var roles = (from a in _contextFactory.Roles
                                 from b in a.Users
                                 where b.UserId == user.Id
                                 select a).ToList();
                    UserReadViewModel modelRead = new UserReadViewModel()
                    {
                        Id = user.Id,
                        Nome = user.Nome,
                        PrimeiroLogin = user.PrimeiroLogin,
                        Profissional = ViewModelUtils.ToViewModel<Profissional, ProfissionalReadModel>(user.Profissional),
                        UserName = user.UserName,
                        Roles = (from a in roles select a.Name).ToList(),
                        RolesReadModel = ViewModelUtils.ToListViewModel<Role, RoleReadModel>(roles),  // (from a in roles select a.Name).ToList(),
                        CBO = user.Profissional.Cbo != null ? string.Format("{0} - {1}", user.Profissional.Cbo.Codigo, user.Profissional.Cbo.Descricao) : "",
                        Documento = user.Documento,
                        TipoDocumento = user.TipoDocumento
                    };
                    return modelRead;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Método para recuperar o usuario com o perfil da delegacao que ele possui
        /// </summary>
        /// <param name="id"></param>
        /// <param name="idDelegacao"></param>
        /// <returns></returns>
        public UserReadViewModel GetUserById(Guid id, Guid idDelegacao)
        {
            try
            {
                Usuario user = _contextFactory.Users.Include("Profissional").Include("Profissional.Unidade").Include("Profissional.Cbo").Where(x => x.Id == id).FirstOrDefault();
                DelegacaoProfissional delegacao = _contextFactory.DelegacaoProfissional.Include("Delegado").Include("Delegador").Where(x => x.Id == idDelegacao).FirstOrDefault();

                if (delegacao != null)
                {
                    var roles = (from a in _contextFactory.Roles
                                 from b in a.Users
                                 where b.UserId == delegacao.Delegador.Id
                                 select a).ToList();
                    UserReadViewModel modelRead = new UserReadViewModel()
                    {
                        Id = user.Id,
                        Nome = user.Nome,
                        PrimeiroLogin = user.PrimeiroLogin,
                        Profissional = ViewModelUtils.ToViewModel<Profissional, ProfissionalReadModel>(user.Profissional),
                        Roles = (from a in roles select a.Name).ToList(),
                        RolesReadModel = ViewModelUtils.ToListViewModel<Role, RoleReadModel>(roles),  // (from a in roles select a.Name).ToList(),
                        CBO = user.Profissional.Cbo != null ? string.Format("{0} - {1}", user.Profissional.Cbo.Codigo, user.Profissional.Cbo.Descricao) : "",
                        UserName = user.UserName,
                        Documento = user.Documento,
                        TipoDocumento = user.TipoDocumento
                    };
                    return modelRead;
                }
                else
                    return new UserReadViewModel();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public UserReadViewModel VerifyIfUserExists(string email, string password)
        {
            try
            {
                Usuario user = _contextFactory.Users.Include("Profissional").Include("Profissional.Unidade").Include("Profissional.Cbo").Where(x => x.UserName == email && x.PasswordHash == password).FirstOrDefault();
                if (user != null)
                {
                    var roles = (from a in _contextFactory.Roles
                                 from b in a.Users
                                 where b.UserId == user.Id
                                 select a).ToList();
                    UserReadViewModel modelRead = new UserReadViewModel()
                    {
                        Id = user.Id,
                        Nome = user.Nome,
                        PrimeiroLogin = user.PrimeiroLogin,
                        Profissional = ViewModelUtils.ToViewModel<Profissional, ProfissionalReadModel>(user.Profissional),
                        Roles = (from a in roles select a.Name).ToList(),
                        RolesReadModel = ViewModelUtils.ToListViewModel<Role, RoleReadModel>(roles),  // (from a in roles select a.Name).ToList(),
                        CBO = user.Profissional.Cbo != null ? string.Format("{0} - {1}", user.Profissional.Cbo.Codigo, user.Profissional.Cbo.Descricao) : "",
                        UserName = user.UserName,
                        Documento = user.Documento,
                        TipoDocumento = user.TipoDocumento
                    };

                    var delegacoes = _contextFactory.DelegacaoProfissional.Include("Delegado").Include("Delegador").Where(x => x.DataDestituicao == null && x.Delegado.Id == user.Id).ToList();
                    if (delegacoes.Count != 0)
                    {
                        DelegacaoProfissional delegacaoProfissional = new DelegacaoProfissional();
                        delegacaoProfissional.Delegador = user;
                        delegacoes.Add(delegacaoProfissional);

                        modelRead.Delegacoes = ViewModelUtils.ToListViewModel<DelegacaoProfissional, DelegacaoProfissionalViewModel>(delegacoes);
                    }

                    return modelRead;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public UserReadViewModel GetUserByProfissionalId(Guid profissionalId)
        {
            try
            {
                Profissional p = new Profissional();

                var po = _contextFactory.Profissional.Include("Cbo").FirstOrDefault();

                Usuario user = _contextFactory.Users.Include("Profissional").Include("Profissional.Unidade")
                                               .Include("Profissional.ProfissionalServicos").Include("Profissional.Cbo")
                                               .Where(x => x.Profissional.Id == profissionalId).FirstOrDefault();
                if (user != null)
                {
                    var roles = (from a in _contextFactory.Roles
                                 from b in a.Users
                                 where b.UserId == user.Id
                                 select a).ToList();
                    UserReadViewModel modelRead = new UserReadViewModel()
                    {
                        Id = user.Id,
                        Nome = user.Nome,
                        Email = user.Email,
                        PrimeiroLogin = user.PrimeiroLogin,
                        UserName = user.UserName,
                        Profissional = ViewModelUtils.ToViewModel<Profissional, ProfissionalReadModel>(user.Profissional),
                        //Servicos = ViewModelUtils.ToListViewModel<Servico, ServicoReadModel>(user.Profissional.Servicos),
                        Roles = (from a in roles select a.Name).ToList(),
                        RolesReadModel = ViewModelUtils.ToListViewModel<Role, RoleReadModel>(roles),  // (from a in roles select a.Name).ToList(),
                        CBO = user.Profissional.Cbo != null ? string.Format("{0} - {1}", user.Profissional.Cbo.Codigo, user.Profissional.Cbo.Descricao) : "",
                        CRM = user.Profissional.CRM
                    };
                    return modelRead;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

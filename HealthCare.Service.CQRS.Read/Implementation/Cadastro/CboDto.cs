﻿using HealtCare.Infra.Common.Utils;
using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Infra.CQRS.Read.Interfaces.Cadastro;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.Data.Context;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HealthCare.Infra.CQRS.Read.Implementation.Cadastro
{
    public class CboDto : ICboDao
    {
        private HealthCareDbContext repository;
        public CboDto(HealthCareDbContext contextFactory)
        {
            this.repository = contextFactory;
        }

        public IList<CboReadModel> ListarTodasCbos()
        {
            return ViewModelUtils.ToListViewModel<Cbo, CboReadModel>(repository.Query<Cbo>()).ToList();
        }

        public CboReadModel ObterCboPorCodigo(string codigo)
        {
            try
            {
                return ViewModelUtils.ToModel<Cbo, CboReadModel>(repository.Query<Cbo>().FirstOrDefault(x => x.Codigo == codigo));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public CboReadModel ObterCboPorId(Guid id)
        {
            try
            {
                return ViewModelUtils.ToModel<Cbo, CboReadModel>(repository.Query<Cbo>().FirstOrDefault(x => x.Id == id));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        IList<CboReadModel> ICboDao.ListarCbos(string texto)
        {
            try
            {
                var lista = ViewModelUtils.ToListViewModel<Cbo, CboReadModel>(repository.Query<Cbo>()
                                          .Where(x => x.Descricao.ToUpper().Contains(texto.ToUpper())
                                                   || x.Codigo.Contains(texto)).Take(10)).ToList();
                return lista;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}
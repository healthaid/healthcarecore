﻿using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.Data.Context;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Newtonsoft.Json;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace HealthCare.Infra.CQRS.Read.Implementation
{
    public class EnderecoDto : IEnderecoDao
    {
        private HealthCareDbContext repository;
        public EnderecoDto(HealthCareDbContext contextFactory)
        {
            repository = contextFactory;
        }

        public async Task<EnderecoViewModel> GetEnderecoByCepAsync(string cep)
        {
            RestClient client = new RestClient("http://api.postmon.com.br/v1/cep/");
            RestRequest request = new RestRequest(cep, Method.GET);
            TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse>();

            RestRequestAsyncHandle handle = client.ExecuteAsync(
                request, r => taskCompletion.SetResult(r));

            RestResponse response = (RestResponse)(await taskCompletion.Task);


            if (response.StatusCode == HttpStatusCode.OK)
            {
                //JToken convert = (JToken)JsonConvert.DeserializeObject(response.Content);
                return JsonConvert.DeserializeObject<EnderecoViewModel>(response.Content);
            }
            else
                return null;

        }

        public IList<Endereco> ListarEnderecos()
        {
            try
            {
                return repository.Query<Endereco>().ToList();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}

﻿using AutoMapper;
using HealtCare.Infra.Common.Utils;
using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Infra.CQRS.Read.Interfaces.Cadastro;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.Data.Context;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HealthCare.Infra.CQRS.Read.Implementation.Cadastro
{
    public class LocalAtendimentoDao : ILocalAtendimentoDao
    {
        private HealthCareDbContext contextFactory;
        private IMapper mapper;
        public LocalAtendimentoDao(HealthCareDbContext contextFactory, IMapper mapper)
        {
            this.contextFactory = contextFactory;
            this.mapper = mapper;
            ViewModelUtils.mapper = mapper;
        }


        public List<LocalAtendimentoViewModel> LocaisAtendimentos()
        {
            try
            {
                return ViewModelUtils.ToListViewModel<LocalAtendimento, LocalAtendimentoViewModel>(contextFactory.Query<LocalAtendimento>()).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

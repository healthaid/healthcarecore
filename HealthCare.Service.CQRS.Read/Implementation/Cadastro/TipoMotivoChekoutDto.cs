﻿using AutoMapper;
using HealtCare.Infra.Common.Utils;
using HealthCare.Domain.Entities.DbClasses.Atendimento;
using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Read.Interfaces.Atendimento;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.Data.Context;
using System;
using System.Linq;


namespace HealthCare.Infra.CQRS.Read.Implementation.Cadastro
{
    public class TipoMotivoCheckoutDto : ITipoMotivoChekoutDto
    {
        private HealthCareDbContext repository;
        private IMapper mapper;

        public TipoMotivoCheckoutDto(HealthCareDbContext contextFactory, IMapper mapper)
        {
            this.repository = contextFactory;
            this.mapper = mapper;
            ViewModelUtils.mapper = mapper;
        }

        public ListResult ListarTipos()
        {
            try
            {
                var lista = ViewModelUtils.ToListViewModel<TipoMotivoChekout, TipoMotivoChekoutReadModel>(repository.TipoMotivoChekout.Where(x => x.Ativo == true)).ToList();
                ListResult result = ListResult.GetListResult(lista, new string[] { "Id", "Descricao" });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

    }
}

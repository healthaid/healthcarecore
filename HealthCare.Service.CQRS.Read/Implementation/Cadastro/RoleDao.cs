﻿using HealtCare.Infra.Common.Utils;
using HealthCare.Domain.Entities.DbClasses.Admin;
using HealthCare.Infra.CQRS.Read.Interfaces.Cadastro;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.Data.Context;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HealthCare.Infra.CQRS.Read.Implementation.Cadastro
{
    public class RoleDao : IRoleDao
    {
        private HealthCareDbContext repository;

        public RoleDao(HealthCareDbContext contextFactory)
        {
            repository = contextFactory;
        }

        public List<RoleReadModel> GetRoles()
        {
            try
            {
                return ViewModelUtils.ToListViewModel<Role, RoleReadModel>(repository.Roles).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}

﻿using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.Data.Context;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HealthCare.Infra.CQRS.Read.Implementation
{
    public class EstadoCivilDto : IEstadoCivilDao
    {
        private HealthCareDbContext repository;
        public EstadoCivilDto(HealthCareDbContext contextFactory)
        {
            this.repository = contextFactory;
        }
        public IList<EstadoCivil> ListarEstadoCivil()
        {
            try
            {
                return repository.Query<EstadoCivil>().Where(x => x.Ativo == true).ToList();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}

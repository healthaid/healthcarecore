﻿using AutoMapper;
using HealtCare.Infra.Common.Utils;
using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HealthCare.Infra.CQRS.Read.Implementation
{
    public class UnidadeDto : IUnidadeDao
    {
        private HealthCareDbContext repository;
        private IMapper mapper;
        public UnidadeDto(HealthCareDbContext contextFactory, IMapper mapper)
        {
            this.repository = contextFactory;
            this.mapper = mapper;
            ViewModelUtils.mapper = mapper;
        }

        public ListResult ListarUnidades(string filtro)
        {
            try
            {
                var lista = ViewModelUtils.ToListViewModel<Unidade, UnidadeReadModel>(repository.Query<Unidade>().Where(x => x.NomeFantasia.Contains(filtro))).ToList();
                ListResult result = ListResult.GetListResult(lista, new string[] { "Id", "NomeUnidade", "Sigla", "Situacao" });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ListResult ListarTodasUnidades()
        {
            try
            {
                var lista = ViewModelUtils.ToListViewModel<Unidade, UnidadeReadModel>(repository.Query<Unidade>()).ToList();
                ListResult result = ListResult.GetListResult(lista, new string[] { "Id", "NomeUnidade", "Sigla", "Situacao" });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public IList<Unidade> ListarUnidade()
        {
            try
            {
                var i = repository.Query<Unidade>().ToList();// .Where(x => x.).ToList();
                return i;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ListResult ListaUnidades()
        {
            try
            {
                var lista = ViewModelUtils.ToListViewModel<Unidade, UnidadeReadModel>(repository.Query<Unidade>()).ToList();
                ListResult result = ListResult.GetListResult(lista, new string[] { "Id", "Nome", "Sigla", "Ativo" });
                return result;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public UnidadeReadModel GetUnidadeById(Guid id)
        {
            try
            {
                //Unidade p = repository.Unidade.Include("Endereco").Where(x => x.Id == id && x.IsDeleted == false && x.Ativo == true).FirstOrDefault();
                //return ViewModelUtils.ToModel<Unidade, UnidadeReadModel>(repository.Query<Unidade>().Where(x => x.Id == id && x.IsDeleted == false && x.Ativo == true).FirstOrDefault());
                Unidade p = repository.Unidade.Include("Endereco").Where(x => x.Id == id).FirstOrDefault();
                return ViewModelUtils.ToModel<Unidade, UnidadeReadModel>(repository.Query<Unidade>().Where(x => x.Id == id).FirstOrDefault());
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}

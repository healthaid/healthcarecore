﻿using AutoMapper;
using HealtCare.Infra.Common.Utils;
using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HealthCare.Infra.CQRS.Read.Implementation
{
    public class LeitoDto : ILeitoDao
    {
        private HealthCareDbContext repository;
        public LeitoDto(HealthCareDbContext contextFactory)
        {
            this.repository = contextFactory;
        }

        public ListResult ListaLeitos(bool ativo)
        {
            try
            {
                var lista = ViewModelUtils.ToListViewModel<Leito, LeitoReadModel>(repository.Query<Leito>().Where(x => x.Ativo == ativo).OrderBy(x => x.Nome)).ToList();
                ListResult result = ListResult.GetListResult(lista, new string[] { "Id", "Nome", "TipoLeitoPorExtenso", "TipoLeito" });
                return result;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public LeitoReadModel GetLeitoById(Guid idLeito)
        {
            try
            {
                return ViewModelUtils.ToModel<Leito, LeitoReadModel>(repository.Leito.FirstOrDefault(x => x.Id == idLeito));
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public ListResult ListarLeitosPorTipo(int tipoLeito)
        {
            throw new NotImplementedException();
        }

        public ListResult ListarLeitosOcupados()
        {
            try
            {
                var lista = (from e in repository.Leito.Include("Enfermaria")
                             where e.Ativo == true
                                && e.Ocupado == true
                             select new LeitoReadModel
                             {
                                 Id = e.Id,
                                 Ativo = e.Ativo,
                                 Enfermaria = Mapper.Map<Enfermaria, EnfermariaReadModel>(e.Enfermaria),
                                 TipoLeito = (int)e.TipoLeito,
                                 Nome = e.Nome
                             }).OrderBy(x => x.Enfermaria.Nome).OrderBy(x => x.Nome).ToList();

                return ListResult.GetListResult(lista, new string[] { "Id", "Nome", "TipoLeitoPorExtenso", "TipoLeito", "Enfermaria" });
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public ListResult ListarLeitosDisponiveis()
        {
            try
            {
                var lista = (from e in repository.Leito.Include("Enfermaria").ToList()
                             where e.Ativo == true
                                && e.Ocupado == false
                             select new LeitoReadModel
                             {
                                 Id = e.Id,
                                 Ativo = e.Ativo,
                                 Enfermaria = Mapper.Map<Enfermaria, EnfermariaReadModel>(e.Enfermaria),
                                 TipoLeito = (int)e.TipoLeito,
                                 Nome = e.Nome
                             }).OrderBy(x => x.Enfermaria.Nome).OrderBy(x => x.Nome).ToList();

                return ListResult.GetListResult(lista, new string[] { "Id", "Nome", "TipoLeitoPorExtenso", "TipoLeito", "Enfermaria" });
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public ListResult ListarLeitosPorEnfermaria(Guid enfermariaId, bool? ativo)
        {
            try
            {
                var lista = ViewModelUtils.ToListViewModel<Leito, LeitoReadModel>(repository.Leito.Include("Enfermaria").Where(x => x.Enfermaria.Id == enfermariaId).OrderBy(x => x.Enfermaria.Nome).OrderBy(x => x.Nome)).ToList();

                if (ativo != null)
                    lista = lista.Where(x => x.Ativo == ativo).ToList();

                ListResult result = ListResult.GetListResult(lista, new string[] { "Id", "Nome", "TipoLeitoPorExtenso", "TipoLeito" });
                return result;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public ListResult ListarLeitosDisponiveisPorEnfermaria(Guid unidadeId)
        {
            try
            {
                var lista = (repository.Leito.Include("Enfermaria")
                                      .Where(x => x.Ativo == true && x.Ocupado == false && x.Enfermaria.Unidade.Id == unidadeId)
                                      .GroupBy(x => x.Enfermaria.Nome + " - " + x.Enfermaria.Local).ToList()
                                      .Select(grp => new LeitosDisponiveisReadModel
                                      {
                                          Descricao = grp.Key,
                                          Leitos = ViewModelUtils.ToListViewModel<Leito, LeitoReadModel>((grp.ToList()))
                                      }).OrderBy(x => x.Descricao).OrderBy(x => x.Descricao)).ToList();

                return ListResult.GetListResult(lista, new string[] { "Descricao", "Leitos" });
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public ListResult ListarLeitosOcupadosPorEnfermaria(Guid unidadeId)
        {
            try
            {

                List<LeitosOcupadosReadModel> lista1 = (from x in repository.MovimentacaoLeito.Include("Internacao.Atendimento.Acolhimento.Paciente").Include("Internacao").Include("LeitoAtual").Include("LeitoAtual.Enfermaria")
                                                               .Where(x => x.Internacao.Atendimento.FimAtendimento == null && x.LeitoAtual.Ocupado == true && x.LeitoAtual.Enfermaria.Unidade.Id == unidadeId)
                                                               .ToList()
                                                               .OrderByDescending(x => x.DataMovimentacao)
                                                               .GroupBy(e => e.Internacao.Id)
                                                               .Select(e => e.FirstOrDefault())
                                                        select new LeitosOcupadosReadModel()
                                                        {
                                                            Id = x.Id,
                                                            Paciente = x.Internacao.Atendimento.Acolhimento.Paciente.Nome,
                                                            Enfermaria = x.LeitoAtual.Enfermaria.Nome + " - " + x.LeitoAtual.Enfermaria.Local,
                                                            Leito = x.LeitoAtual.Nome
                                                        }).OrderBy(x => x.Enfermaria).OrderBy(x => x.Leito).ToList();

                var lista = (lista1.GroupBy(e => e.Enfermaria)
                                   .Select(grp => new LeitosAgrupadosOcupadosReadModel()
                                   {
                                       Descricao = grp.Key,
                                       Leitos = grp.ToList()
                                   })).ToList();
                return ListResult.GetListResult(lista, new string[] { "Descricao", "Leitos" });
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }


        public ListResult ListarPorEnfermaria()
        {
            try 
            {
                var lista = (repository.Leito.Include("Enfermaria").ToList()
                                      .Where(x => x.Ativo == true && x.Ocupado == false).OrderBy(x => x.Enfermaria).OrderBy(x => x.Nome)
                                      .GroupBy(x => x.Enfermaria.Nome)
                                      .Select(grp => new { enfermariaDescricao = grp.Key, leitos = grp.ToArray() })).ToList();
                return ListResult.GetListResult(lista, new string[] { "enfermariaDescricao", "leitos" });
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public ListResult ListarLeitosOcupadosParaPermuta(Guid unidadeId, Guid pacienteSelecionadoId)
        {
            try
            {

                List<LeitosOcupadosReadModel> lista1 = (from x in repository.MovimentacaoLeito.Include("Internacao.Atendimento.Acolhimento.Paciente").Include("Internacao").Include("LeitoAtual").Include("LeitoAtual.Enfermaria")
                                                               .Where(x => x.Internacao.Atendimento.FimAtendimento == null && x.LeitoAtual.Ocupado == true && x.LeitoAtual.Enfermaria.Unidade.Id == unidadeId && x.Internacao.Atendimento.Acolhimento.Paciente.Id != pacienteSelecionadoId)
                                                               .ToList()
                                                               .OrderByDescending(x => x.DataMovimentacao)
                                                               .GroupBy(e => e.Internacao.Id)
                                                               .Select(e => e.FirstOrDefault())
                                                        select new LeitosOcupadosReadModel()
                                                        {
                                                            Id = x.Id,
                                                            Paciente = x.Internacao.Atendimento.Acolhimento.Paciente.Nome,
                                                            Enfermaria = x.LeitoAtual.Enfermaria.Nome + " - " + x.LeitoAtual.Enfermaria.Local,
                                                            Leito = x.LeitoAtual.Nome
                                                        }).OrderBy(x => x.Enfermaria).OrderBy(x => x.Leito).ToList();

                var lista = (lista1.GroupBy(e => e.Enfermaria)
                                   .Select(grp => new LeitosAgrupadosOcupadosReadModel()
                                   {
                                       Descricao = grp.Key,
                                       Leitos = grp.ToList()
                                   })).ToList();
                return ListResult.GetListResult(lista, new string[] { "Descricao", "Leitos" });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

﻿using HealtCare.Infra.Common.Utils;
using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.Data.Context;
using System;
using System.Linq;

namespace HealthCare.Infra.CQRS.Read.Implementation
{
    public class BairroDto : IBairroDao
    {
        private HealthCareDbContext repository;
        public BairroDto(HealthCareDbContext contextFactory)
        {
            this.repository = contextFactory;
        }

        public ListResult ListaBairros()
        {
            try
            {
                var lista = ViewModelUtils.ToListViewModel<Bairro, BairroReadModel>(repository.Query<Bairro>()).ToList();
                ListResult result = ListResult.GetListResult(lista, new string[] { "Id", "Nome" });
                return result;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public ListResult ListarBairrosPorCidade(Guid idcidade)
        {
            try
            {
                var lista = ViewModelUtils.ToListViewModel<Bairro, BairroReadModel>(repository.Query<Bairro>().Where(x => x.Cidade.Id == idcidade)).ToList();
                ListResult result = ListResult.GetListResult(lista, new string[] { "Id", "Nome" });
                return result;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}

﻿using HealtCare.Infra.Common.Utils;
using HealthCare.Domain.Entities.DbClasses.Admin;
using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HealthCare.Infra.CQRS.Read.Implementation
{
    public class ProfissionalDto : IProfissionalDao
    {
        private HealthCareDbContext repository;
        public ProfissionalDto(HealthCareDbContext contextFactory)
        {
            this.repository = contextFactory;
        }

        public ListResult ListarProfissional(string filtro, Guid unidadeId)
        {
            try
            {
                var lista = ViewModelUtils.ToListViewModel<Profissional, ProfissionalReadModel>(repository.Profissional.Include("Unidade").Where(x => x.Nome.Contains(filtro) && x.Unidade.Id == unidadeId)).ToList();
                ListResult result = ListResult.GetListResult(lista, new string[] { "Id", "Nome", "Nascimento", "Sexo", "CNS" });
                return result;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<ProfissionalReadModel> ListarProfissional(Guid unidadeId)
        {
            try
            {
                var profissional = repository.Query<Profissional>().Where(x => x.IsDeleted == false && x.Unidade.Id == unidadeId).ToList();
                return ViewModelUtils.ToListViewModel<Profissional, ProfissionalReadModel>(profissional).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ProfissionalReadModel GetProfissionalById(Guid ProfissionalId)
        {
            try
            {
                Usuario usuario = repository.Users.Include("Profissional").Where(x => x.Profissional.Id == ProfissionalId && x.Profissional.IsDeleted == false).FirstOrDefault();
                ProfissionalReadModel profissional = new ProfissionalReadModel()
                {
                    CBO = usuario.Profissional.Cbo.Descricao,
                    CNS = usuario.Profissional.CNS,
                    CPF = usuario.Profissional.CPF,
                    Email = usuario.Email,
                    Id = usuario.Profissional.Id,
                    Nacionalidade = usuario.Profissional.Nacionalidade,
                    Nascimento = usuario.Profissional.Nascimento,
                    Naturalidade = usuario.Profissional.Naturalidade,
                    Nome = usuario.Profissional.Nome,
                    NomeMae = usuario.Profissional.NomeMae,
                    NomePai = usuario.Profissional.NomePai,
                    Sexo = usuario.Profissional.Sexo,
                    Unidade = ViewModelUtils.ToModel<Unidade, UnidadeReadModel>(usuario.Profissional.Unidade),
                    UserName = usuario.UserName
                };
                return profissional;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ProfissionalReadModel GetProfissionalUnidade(Guid profissionalId)
        {
            try
            {
                Profissional profissional = repository.Profissional.Include("Unidade").Where(x => x.Id == profissionalId).FirstOrDefault();
                return ViewModelUtils.ToViewModel<Profissional, ProfissionalReadModel>(profissional);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ProfissionalReadModel GetUserByProfissionalId(Guid ProfissionalId)
        {
            throw new NotImplementedException();
        }

        public bool ProfissionalEMedico(Guid id)
        {
            try
            {
                var profissional = repository.Profissional.Include("Cbo").FirstOrDefault(x => x.Id == id);
                if (profissional.Cbo != null)
                {
                    List<string> cbo = new List<string>();
                    cbo.Add("223119");
                    cbo.Add("223150");
                    cbo.Add("2231A1");
                    cbo.Add("2231A2");
                    cbo.Add("2231F8");
                    cbo.Add("2231F9");
                    cbo.Add("2231G1");
                    cbo.Add("223305");
                    cbo.Add("225103");
                    cbo.Add("225105");
                    cbo.Add("225106");
                    cbo.Add("225109");
                    cbo.Add("225110");
                    cbo.Add("225112");
                    cbo.Add("225115");
                    cbo.Add("225118");
                    cbo.Add("225120");
                    cbo.Add("225121");
                    cbo.Add("225122");
                    cbo.Add("225124");
                    cbo.Add("225125");
                    cbo.Add("225127");
                    cbo.Add("225130");
                    cbo.Add("225133");
                    cbo.Add("225135");
                    cbo.Add("225136");
                    cbo.Add("225139");
                    cbo.Add("225140");
                    cbo.Add("225142");
                    cbo.Add("225145");
                    cbo.Add("225148");
                    cbo.Add("225150");
                    cbo.Add("225151");
                    cbo.Add("225155");
                    cbo.Add("225160");
                    cbo.Add("225165");
                    cbo.Add("225170");
                    cbo.Add("225175");
                    cbo.Add("225180");
                    cbo.Add("225185");
                    cbo.Add("225190");
                    cbo.Add("225195");
                    cbo.Add("225203");
                    cbo.Add("225210");
                    cbo.Add("225215");
                    cbo.Add("225220");
                    cbo.Add("225225");
                    cbo.Add("225230");
                    cbo.Add("225235");
                    cbo.Add("225240");
                    cbo.Add("225245");
                    cbo.Add("225250");
                    cbo.Add("225255");
                    cbo.Add("225260");
                    cbo.Add("225265");
                    cbo.Add("225270");
                    cbo.Add("225275");
                    cbo.Add("225280");
                    cbo.Add("225285");
                    cbo.Add("225290");
                    cbo.Add("225295");
                    cbo.Add("225305");
                    cbo.Add("225310");
                    cbo.Add("225315");
                    cbo.Add("225320");
                    cbo.Add("225325");
                    cbo.Add("225330");
                    cbo.Add("225335");
                    cbo.Add("225345");
                    cbo.Add("225350");

                    if (cbo.FirstOrDefault(x => x == profissional.Cbo.Codigo) != null)
                        return true;
                    else
                        return false;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<ProfissionalReadModel> ListarProfissionaisPorCbos(IEnumerable<string> listaCbos)
        {
            try
            {
                string cbos = string.Join(",", listaCbos.ToArray());

                var list = repository.Profissional.Include(x => x.Cbo).Where(x => listaCbos.Any(y => y == x.Cbo.Codigo)).ToList();

                var listaRetorno = list.Select(x => new ProfissionalReadModel()
                {
                    Id = x.Id,
                    Nome = x.Nome,
                    CNS = x.CNS
                }).ToList();

                return listaRetorno;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
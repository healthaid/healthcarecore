﻿using HealtCare.Infra.Common.Utils;
using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Read.Interfaces.Atendimento;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;


namespace HealthCare.Infra.CQRS.Read.Implementation.Cadastro
{
    public class MotivoCheckoutDto : IMotivoCheckoutDto
    {
        private HealthCareDbContext repository;

        public MotivoCheckoutDto(HealthCareDbContext contextFactory)
        {
            this.repository = contextFactory;
        }

        public ListResult ListarMotivosAtivos()
        {
            try
            {
                var lista = ViewModelUtils.ToListViewModel<HealthCare.Domain.Entities.DbClasses.Atendimento.MotivoCheckout, MotivoCheckoutModel>(repository.MotivoCheckout.Where(x => x.Ativo == true)).ToList();
                ListResult result = ListResult.GetListResult(lista, new string[] { "Id", "Descricao" });
                return result;
            }
            catch (Exception)
            {
                throw;
            }

        }

        public ListResult ListarMotivosPorTipo(Guid tipoId)
        {
            try
            {
                var lista = ViewModelUtils.ToListViewModel<HealthCare.Domain.Entities.DbClasses.Atendimento.MotivoCheckout, MotivoCheckoutModel>
                                                          (repository.MotivoCheckout.Include("Tipo").Where(x => x.Ativo == true && x.Tipo.Id == tipoId)).ToList();
                ListResult result = ListResult.GetListResult(lista, new string[] { "Id", "Descricao" });
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}

﻿using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.Data.Context;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HealthCare.Infra.CQRS.Read.Implementation
{
    public class ConvenioDto : IConvenioDao
    {
        private HealthCareDbContext repository;
        public ConvenioDto(HealthCareDbContext contextFactory)
        {
            this.repository = contextFactory;
        }
        public IList<Convenio> ListarConvenios()
        {
            try
            {
                return repository.Query<Convenio>().ToList();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public IList<Convenio> ListarConveniosPorPaciente(Guid pacienteId)
        {
            try
            {
                return repository.Query<Convenio>("Paciente","Plano").Where(x => x.Paciente.Id == pacienteId).ToList();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}

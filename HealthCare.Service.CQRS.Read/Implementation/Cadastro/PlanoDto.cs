﻿using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.Data.Context;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HealthCare.Infra.CQRS.Read.Implementation
{
    public class PlanoDto : IPlanoDao
    {
        private HealthCareDbContext repository;
        public PlanoDto(HealthCareDbContext contextFactory)
        {
            this.repository = contextFactory;
        }
        public IList<Plano> ListarPlano()
        {
            try
            {
                return repository.Query<Plano>().Where(x => x.Ativo == true).ToList();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

    }
}

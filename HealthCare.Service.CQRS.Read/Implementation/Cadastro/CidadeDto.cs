﻿using HealtCare.Infra.Common.Utils;
using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HealthCare.Infra.CQRS.Read.Implementation
{
    public class CidadeDto : ICidadeDao
    {
        private HealthCareDbContext repository;
        public CidadeDto(HealthCareDbContext contextFactory)
        {
            this.repository = contextFactory;
        }

        public ListResult ListaCidades()
        {
            try
            {
                var lista = ViewModelUtils.ToListViewModel<Cidade, CidadeReadModel>(repository.Query<Cidade>()).ToList();
                ListResult result = ListResult.GetListResult(lista, new string[] { "Id", "Nome" });
                return result;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public ListResult ListarCidadesporUF(Guid idUF)
        {
            try
            {
                var lista = ViewModelUtils.ToListViewModel<Cidade, CidadeReadModel>(repository.Cidade.Include("UF").Where(x => x.UF.Id == idUF)).OrderBy(x => x.Nome).ToList();
                ListResult result = ListResult.GetListResult(lista, new string[] { "Id", "Nome" });
                return result;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public IList<CidadeReadModel> ListarCidadeporUF(Guid idUF)
        {
            try
            {
                var lista = ViewModelUtils.ToListViewModel<Cidade, CidadeReadModel>(repository.Cidade.Include("UF").Where(x => x.UF.Id == idUF)).OrderBy(x => x.Nome).ToList();
                return lista;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}

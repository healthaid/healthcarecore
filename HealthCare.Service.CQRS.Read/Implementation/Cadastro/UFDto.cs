﻿using HealtCare.Infra.Common.Utils;
using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.Data.Context;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HealthCare.Infra.CQRS.Read.Implementation
{
    public class UFDto : IUFDao
    {
        private HealthCareDbContext repository;
        public UFDto(HealthCareDbContext contextFactory)
        {
            this.repository = contextFactory;
        }
        public ListResult ListaUFs()
        {
            try
            {
                var lista = ViewModelUtils.ToListViewModel<UF, UFReadModel>(repository.Query<UF>()).OrderBy(x => x.Descricao).ToList();
                ListResult result = ListResult.GetListResult(lista, new string[] { "Id", "Descricao" });
                return result;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }


        public IList<UFReadModel> ListarUFs()
        {
            try
            {
                return ViewModelUtils.ToListViewModel<UF, UFReadModel>(repository.Query<UF>()).OrderBy(x => x.Descricao).ToList();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}

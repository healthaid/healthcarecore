﻿using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.Data.Context;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HealthCare.Infra.CQRS.Read.Implementation
{
    public class EscolaridadeDto : IEscolaridadeDao
    {
        private HealthCareDbContext repository;
        public EscolaridadeDto(HealthCareDbContext contextFactory)
        {
            this.repository = contextFactory;
        }
        public IList<Escolaridade> ListarEscolaridade()
        {
            try
            {
                return repository.Query<Escolaridade>().Where(x => x.Ativo == true).ToList();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}

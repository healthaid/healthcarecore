﻿using HealtCare.Infra.Common.Utils;
using HealthCare.Domain.Entities.DbClasses.Cadastro;
using HealthCare.Infra.Common.Result;
using HealthCare.Infra.CQRS.Read.Interfaces;
using HealthCare.Infra.CQRS.Read.Models;
using HealthCare.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HealthCare.Infra.CQRS.Read.Implementation
{
    public class PacienteDto : IPacienteDao
    {
        private HealthCareDbContext repository;

        public PacienteDto(HealthCareDbContext contextFactory)
        {
            this.repository = contextFactory;
        }

        public ListResult ListarPacientes(string filtro, Guid unidadeId)
        {
            try
            {
                int codigoPaciente;

                var lista = new List<PacienteReadModel>();
                if (int.TryParse(filtro, out codigoPaciente))
                {
                    lista = ViewModelUtils.ToListViewModel<Paciente, PacienteReadModel>(repository.Paciente.Include("Unidade")
                                                                                       .Include("Naturalidade").Include("Naturalidade.UF")
                                                                                       .Where(x => x.CodigoPaciente == filtro
                                                                                                 && x.IsDeleted == false && x.Unidade.Id == unidadeId && x.PendenteCadastro == false)).ToList();
                }
                else
                {
                    lista = ViewModelUtils.ToListViewModel<Paciente, PacienteReadModel>(repository.Paciente.Include("Unidade")
                                                                                                           .Include("Naturalidade").Include("Naturalidade.UF")
                                                                                                           .Where(x => (x.Nome.Contains(filtro) || x.NomeMae.Contains(filtro))
                                                                                                                     && x.IsDeleted == false && x.Unidade.Id == unidadeId && x.PendenteCadastro == false)).ToList();
                }

                ListResult result = ListResult.GetListResult(lista, new string[] { "Id", "Nome", "NomeMae", "DataNascimento" });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<PacienteReadModel> ListarPacientesExcuidos()
        {
            try
            {
                return ViewModelUtils.ToListViewModel<Paciente, PacienteReadModel>(repository.Query<Paciente>().Where(x => x.IsDeleted == true)).Cast<PacienteReadModel>().ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<PacienteReadModel> ListarPacientesAtivos(Guid unidadeId)
        {
            try
            {
                return ViewModelUtils.ToListViewModel<Paciente, PacienteReadModel>(repository.Paciente.Include("Unidade").Include("Naturalidade").Include("Naturalidade.UF").Where(x => x.IsDeleted == false && x.Unidade.Id == unidadeId)).Cast<PacienteReadModel>().ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public PacienteReadModel GetPacienteById(Guid pacienteId)
        {
            try
            {
                Paciente p = repository.Paciente.Include("Naturalidade").Include("Naturalidade.UF").Include("Endereco").Where(x => x.Id == pacienteId && x.IsDeleted == false && x.Ativo == true).FirstOrDefault();
                return ViewModelUtils.ToModel<Paciente, PacienteReadModel>(repository.Query<Paciente>().Where(x => x.Id == pacienteId && x.IsDeleted == false && x.Ativo == true).FirstOrDefault());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public PacienteReadModel GetPacienteByAtendimentoId(Guid atendimentoId)
        {
            try
            {
                Paciente p = repository.Atendimento.Include("Acolhimento").Include("Acolhimento.Paciente.Naturalidade").Include("Acolhimento.Paciente.Naturalidade.UF").Include("Acolhimento.Paciente").Include("Acolhimento.Paciente.Unidade").Include("Acolhimento.Paciente.Unidade.Endereco").FirstOrDefault(x => x.Id == atendimentoId).Acolhimento.Paciente;
                return ViewModelUtils.ToModel<Paciente, PacienteReadModel>(p);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class InternacaoReadModel
    {
        public Guid Id { get; set; }
        public DateTime DataInternacao { get; set; }
        public AtendimentoReadModel Atendimento { get; set; }
        //public List<MovimentacaoLeitoReadModel> Movimentacoes { get; set; }
        //public MovimentacaoLeitoReadModel UltimaMovimentacaoRegistrada
        //{
        //    get
        //    {
        //        if (this.Movimentacoes != null && this.Movimentacoes.Count > 0)
        //            return Movimentacoes.OrderByDescending(x => x.DataMovimentacao).FirstOrDefault();
        //        else
        //            return null;
        //    }
        //}
    }
}

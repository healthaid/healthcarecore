﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class EvolucaoReadModel
    {
        public Guid Id { get; set; }
        public string DataHoraEvolucao { get; set; }
        public string Descricao { get; set; }
        public string Procedimento { get; set; }
        public string DiagnosticoUm { get; set; }
        //public string DiagnosticoDois { get; set; }
        public int Quantidade { get; set; }
        public AtendimentoReadModel Atendimento { get; set; }
        public string Profissional { get; set; }
        public List<EvolucaoReadModel> Historico { get; set; }
        public IEnumerable<string> Procedimentos { get; set; }
        public CIDReadViewModel PrimeiroDiagnostico { get; set; }
        public CIDReadViewModel SegundoDiagnostico { get; set; }
    }
}

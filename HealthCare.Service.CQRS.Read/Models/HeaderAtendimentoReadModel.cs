﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class HeaderAtendimentoReadModel
    {
        /// <summary>
        /// Identificador do paciente
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Nome do paciente
        /// </summary>
        public string Nome { get; set; }

        /// <summary>
        /// Nome da mãe do paciente
        /// </summary>
        public string NomeMae { get; set; }

        /// <summary>
        /// Email do paciente
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Data de nascimento do paciente
        /// </summary>
        public string DataNascimento { get; set; }

        /// <summary>
        /// Principal telefone do paciente
        /// </summary>
        public string TelefoneCelular { get; set; }

        /// <summary>
        /// Telefone fixo de referencia do paciente
        /// </summary>
        public string TelefoneFixo { get; set; }

        /// <summary>
        /// Telefone do trabalho do paciente
        /// </summary>
        public string TelefoneTrabalho { get; set; }

        /// <summary>
        /// Endereco do paciente
        /// </summary>
        public EnderecoReadViewModel Endereco { get; set; }

        /// <summary>
        /// Sexo do paciente
        /// </summary>
        public string Sexo { get; set; }

        /// <summary>
        /// Numero da carteira do plano de saude 
        /// </summary>
        public string NumeroCarteira { get; set; }

        /// <summary>
        /// Nome do plano de saúde 
        /// </summary>
        public string Plano { get; set; }

        /// <summary>
        /// Acomodação do paciente pelo plano
        /// </summary>
        public string Acomodacao { get; set; }

        /// <summary>
        /// Validade do plano de saúde
        /// </summary>
        public DateTime? Validade { get; set; }


        /// <summary>
        /// Cpf do paciente
        /// </summary>
        public string CPF { get; set; }

        /// <summary>
        /// Rg do paciente
        /// </summary>
        public string RG { get; set; }

        /// <summary>
        /// Número do cartao nacional de saúde do paciente
        /// </summary>
        public string CNS { get; set; }

        /// <summary>
        /// Etinia do paciente
        /// </summary>
        public string Etinia { get; set; }

        /// <summary>
        /// Estado civil do paciente
        /// </summary>
        public string EstadoCivil { get; set; }

        /// <summary>
        /// Escolaridade do paciente
        /// </summary>
        public string Escolaridade { get; set; }

        /// <summary>
        /// Profissão do paciente
        /// </summary>
        public string Profissão { get; set; }

        /// <summary>
        /// Idade Por extenso do paciente
        /// </summary>
        public string IdadePorExtenso { get; set; }
        /// <summary>
        /// Estado de nascimento do paciente
        /// </summary>
        public string Naturalidade { get; set; }

        /// <summary>
        /// País de origem do paciente
        /// </summary>
        public string Nacionalidade { get; set; }

        /// <summary>
        /// Código de Paciente
        /// </summary>
        public string CodigoPaciente { get; set; }

        /// <summary>
        /// Número do boletim do atendimento
        /// </summary>
        public string CodigoBoletim { get; set; }

        /// <summary>
        /// Hora em que o atendimento foi iniciado
        /// </summary>
        public string InicioAtendimento { get; set; }


        /// <summary>
        /// Retorna as iniciais do nome do paciente
        /// </summary>
        public string IniciaisNomePaciente
        {
            get
            {
                var arrayNome = this.Nome.Split(' ').ToList();
                if (arrayNome.Count > 1)
                {
                    return string.Format("{0}{1}", arrayNome.First().First().ToString().ToUpper(), arrayNome[1].FirstOrDefault().ToString().ToUpper());
                }
                else
                    return arrayNome.FirstOrDefault().FirstOrDefault().ToString().ToUpper();
            }
        }

        /// <summary>
        /// Retorna o identificador do atendimento
        /// </summary>
        public Guid AtendimentoId { get; set; }
    }
}

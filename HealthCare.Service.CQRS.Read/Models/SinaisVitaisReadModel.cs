﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class SinaisVitaisReadModel
    {
        public Guid Id { get; set; }
        public string Peso { get; set; }
        public string Pressao { get; set; }
        public string Pulso { get; set; }
        public string FrequenciaRespiratoria { get; set; }
        public string Temperatura { get; set; }
        public string Altura { get; set; }
        public string GlicemiaCapilar { get; set; }
    }
}

﻿
using System;
namespace HealthCare.Infra.CQRS.Read.Models
{
    public class ImagemReadViewModel
    {
        public Guid Id { get; set; }
        public Byte[] Imagem { get; set; }
        public Guid ImagemAtendimentoId { get; set; }
    }
}

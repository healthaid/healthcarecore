﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class FilaDeObservacaoReadModel
    {
        public Guid acolhimentoId { get; set; }
        public string Paciente { get; set; }
        public string Leito { get; set; }
        public string Enfermaria { get; set; }
        public string CodigoBoletim { get; set; }
    }
}

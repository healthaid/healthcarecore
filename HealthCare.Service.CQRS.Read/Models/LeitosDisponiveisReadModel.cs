﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class LeitosDisponiveisReadModel
    {
        public string Descricao { get; set; }
        public IEnumerable<LeitoReadModel> Leitos { get; set; }
    }
}

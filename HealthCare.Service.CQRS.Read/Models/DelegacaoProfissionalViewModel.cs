﻿using HealthCare.Infra.CQRS.Read.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace HealthCare.Service.CQRS.Read.Models
{
    public class DelegacaoProfissionalViewModel
    {
        public Guid Id { get; set; }
        public UserReadViewModel Delegador { get; set; }
        public UserReadViewModel Delegado { get; set; }
        public DateTime DataDelegacao { get; set; }
        public DateTime? DataDestituicao { get; set; }

        public string DataDelegacaoFormatada
        {
            get
            {
                return this.DataDelegacao.ToString("dd/MM/yyyy HH:mm:ss", new CultureInfo("en-US"));
            }
        }
        public string DataDestituicaoFormatada
        {
            get
            {
                if (this.DataDestituicao != null)
                    return this.DataDestituicao.Value.ToString("dd/MM/yyyy HH:mm:ss", new CultureInfo("en-US"));
                else
                    return string.Empty;
            }
        }
    }

    public class ListaDelegacaoProfissionalViewModel
    {
        public Guid Id { get; set; }
        public string Delegador { get; set; }
        public string CboDelegador { get; set; }
        public string Delegado { get; set; }
        public string CboDelegado { get; set; }
        public DateTime DataDelegacao { get; set; }
        public DateTime? DataDestituicao { get; set; }

        public string DataDelegacaoFormatada
        {
            get
            {
                return this.DataDelegacao.ToString("dd/MM/yyyy HH:mm:ss", new CultureInfo("en-US"));
            }
        }
        public string DataDestituicaoFormatada
        {
            get
            {
                if (this.DataDestituicao != null)
                    return this.DataDestituicao.Value.ToString("dd/MM/yyyy HH:mm:ss", new CultureInfo("en-US"));
                else
                    return string.Empty;
            }
        }
    }
}
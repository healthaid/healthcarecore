﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class RptFilaDeEspera
    {
        public string ID { get; set; }
        public string TipoFila { get; set; }
        public string Agrupamento { get; set; }
        public string Paciente { get; set; }
        public string Risco { get; set; }
        public string InicioAtendimento { get; set; }
        public string Tempo { get; set; }
        public string Clinica { get; set; }
        public string CodigoBoletim { get; set; }
        public List<RptFilaDeEspera> FilaDeEspera { get; set; }
        public string Total { get; set; }
        public string TotalRiscoAzul { get; set; }
        public string TotalVerde { get; set; }
        public string TotalAmarelo { get; set; }
        public string TotalVermelho { get; set; }

    }
}

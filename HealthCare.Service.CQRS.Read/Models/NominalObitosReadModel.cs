﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class NominalObitosReadModel
    {
        public string Boletim { get; set; }
        public string Origem { get; set; }
        public string TipoDeSaida { get; set; }
        public string Nome { get; set; }
        public string Idade { get; set; }
        public string Diagnostico { get; set; }
        public string Sexo { get; set; }
        public string Profissional { get; set; }
        public string DataObito { get; set; }
    }
}

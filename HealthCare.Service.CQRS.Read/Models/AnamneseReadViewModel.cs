﻿using System;
namespace HealthCare.Infra.CQRS.Read.Models
{
    public class AnamneseReadViewModel
    {
        public Guid Id { get; set; }
        public string QueixaPrincipal { get; set; }
        public Guid Ciad2Id { get; set; }
        public string Descricao { get; set; }
        public Guid AtendimentoId { get; set; }
    }
}

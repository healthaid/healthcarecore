﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class ProcedimentosEnfermagemReadModel
    {
        public Guid Id { get; set; }
        public ProcedimentoReadModel Procedimento { get; set; }
        public EvolucaoEnfermagemReadModel Evolucao { get; set; }
        public string DescricaoProcedimento { get; set; }
        public int Quantidade { get; set; }
        public string Observacao { get; set; }
    }
}

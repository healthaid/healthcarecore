﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class RptPacientesRegistradosReadModel
    {
        public string Data { get; set; }
        public IEnumerable<RptPacientesRegistradosReadModel> Atendimentos { get; set; }
        public int Quantidade { get; set; }
        public string CodigoBoletim { get; set; }
        public string Nome { get; set; }
        public string DataNascimento { get; set; }
        public string Idade { get; set; }
        public string Descricao { get; set; }
        public string Paginacao { get; set; }
    }
}

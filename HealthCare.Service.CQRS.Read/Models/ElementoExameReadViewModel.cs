﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class ElementoExameReadViewModel
    {
        public Guid Id { get; set; }
        public string Valor { get; set; }
        public Guid ExameId { get; set; }
    }
}

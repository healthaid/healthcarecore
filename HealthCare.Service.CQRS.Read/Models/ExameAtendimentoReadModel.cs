﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class ExameAtendimentoReadModel
    {
        public Guid Id { get; set; }
        public AtendimentoReadModel Atebdimento { get; set; }
        //public ExameReadModel Exame { get; set; }
        public string Exame { get; set; }
        public string CodigoProcedimento { get; set; }
        public string Procedimento { get; set; }
        public string ObservacaoMedico { get; set; }
        public DateTime DataSolicitacao { get; set; }
        public DateTime? DataExecucao { get; set; }
        public string ObservacaoExecutor { get; set; }
        public ProfissionalReadModel Executor { get; set; }
        public bool Realizado { get; set; }
        public string DefinicaoExame { get; set; }
        public string DescricaoProcedimento
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(CodigoProcedimento) && !string.IsNullOrWhiteSpace(Exame))
                    return string.Format("{0} - {1}", CodigoProcedimento, Exame);
                else
                    return string.Empty;
            }
        }
    }
}

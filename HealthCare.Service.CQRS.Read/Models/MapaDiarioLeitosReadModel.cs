﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class MapaDiarioLeitosReadModel
    {
        public String Clinica { get; set; }
        public String Enfermaria { get; set; }
        public String Leito { get; set; }
        public String Prontuario { get; set; }
        public String Paciente { get; set; }
        public String Sexo { get; set; }
        public String Idade { get; set; }
        public String ClinicaOrigem { get; set; }
        public String Diagnostico { get; set; }
        public String Procedimento { get; set; }
        public String TempoProcedimento { get; set; }
        public String DataInternacao { get; set; }
        public String TP { get; set; }

        public IEnumerable<MapaDiarioLeitosReadModel> ListaRegistros { get; set; }

        public string DataInternacaoExibicao
        {
            get
            {
                return DataInternacao;
                //if (!string.IsNullOrWhiteSpace(DataInternacao))
                //    return Convert.ToDateTime(DataInternacao).ToString("dd/MM/yyyy HH:mm:ss", new CultureInfo("en-US"));
                //else
                //    return null;
            }
        }
    }
}

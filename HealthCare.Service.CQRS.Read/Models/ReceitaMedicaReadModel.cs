﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class ReceitaMedicaReadModel
    {
        public Guid Id { get; set; }
        public string Medicamento { get; set; }
        public string Dose { get; set; }
        public string ViaAdministracao { get; set; }
        public string Intervalo { get; set; }
        public string Duracao { get; set; }
        public string Nome { get; set; }
        public string CPF { get; set; }
        public string Nascimento { get; set; }
        public string Unidade { get; set; }
        public string StatusMedicacao { get; set; }
        
        public AtendimentoReadModel Atendimento { get; set; }
        public List<ReceitaMedicaReadModel> ItensReceita { get; set; }
    }
}

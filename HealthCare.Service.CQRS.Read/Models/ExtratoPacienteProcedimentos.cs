﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class ExtratoPacienteProcedimentos
    {
        public string Cbo { get; set; }
        public string Procedimento { get; set; }
        public string Valor { get; set; }
        public string ValorProfissional { get; set; }
        public string ValorConsolidado
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(Valor) && !string.IsNullOrWhiteSpace(ValorProfissional))
                    return (Convert.ToDecimal(Valor) + Convert.ToDecimal(ValorProfissional)).ToString();
                else
                    return "0,00";
            }
        }
        public int Quantidade { get; set; }
        public string TipoProcedimento { get; set; }
    }
}

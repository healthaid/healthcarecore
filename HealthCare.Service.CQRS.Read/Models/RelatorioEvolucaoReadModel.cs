﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class RelatorioEvolucaoReadModel
    {
        public string Data { get; set; }
        public string Profissional { get; set; }
        public string Boletim { get; set; }
        public string HoraAtendimento { get; set; }
        public string Clinica { get; set; }
        public string CID { get; set; }
        public PacienteReadModel Paciente { get; set; }
        public IEnumerable<ProcedimentoReadModel> Procedimentos { get; set; }
        public int TotalProfissional { get; set; }
        public IEnumerable<RelatorioEvolucaoReadModel> Lista { get; set; }
    }
}

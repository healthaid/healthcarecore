﻿using System;
namespace HealthCare.Infra.CQRS.Read.Models
{
    public class CboReadModel
    {
        public string Codigo { get; set; }
        public string Descricao { get; set; }
    }
}

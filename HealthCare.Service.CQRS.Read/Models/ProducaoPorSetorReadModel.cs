﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class ProducaoPorSetorReadModel
    {
        public string Clinica { get; set; }
        public int Quantidade { get; set; }
        public string Setor { get; set; }
        public List<ProducaoPorSetorReadModel> ListaProducaoSetor { get; set; }
    }
}

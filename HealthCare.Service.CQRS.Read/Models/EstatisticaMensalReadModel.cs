﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class EstatisticaMensalReadModel
    {
        public string Especialidade { get; set; }
        public string Atendimentos { get; set; }
        public string Altas { get; set; }
        public string Obitos { get; set; }
        public string Remocoes { get; set; }
        public string Internacoes { get; set; }
        public string Chegou { get; set; }
        public string Boletim { get; set; }
        public string Desistencia { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class ClassificacaoDeRiscoReadModel
    {
        public Guid Id { get; set; }
        public SinaisVitaisReadModel SinaisVitais { get; set; }
        public RiscoReadModel Risco { get; set; }
        public string NivelDeConsciencia { get; set; }
        public string Queixa { get; set; }
        public string CausaExterna { get; set; }
        public string DoencasPreexistentes { get; set; }
        public string MedicamentosEmUso { get; set; }
        public string Alergia { get; set; }
        public string Observacao { get; set; }
        public string EspecialidadeMedica { get; set; }
        public string Classificacao { get; set; }
        public ProfissionalReadModel Profissional { get; set; }
    }
}

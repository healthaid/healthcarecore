﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class ProcedimentoAtendimentoReadModel: ProcedimentoReadModel
    {
        public string Data { get; set; }
        public DateTime DataAtendimento { get; set; }
        public IEnumerable<ProcedimentoAtendimentoReadModel> Procedimentos { get; set; }
        public int QuantidadeTotal { get; set; }
        public int QuantidadeItem { get; set; }
        public string NomeProcedimento { get; set; }
    }
}

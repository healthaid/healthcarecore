﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class ErroSpaGrupoDiagnosticoSexoFaixaEtaria
    {
        public String AtendimentoId { get; set; }
        public String MensagemErro { get; set; }
    }
}

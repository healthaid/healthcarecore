﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class PacienteReadModel
    {
        /// <summary>
        /// Identificador do paciente
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Nome do paciente
        /// </summary>
        public string Nome { get; set; }

        /// <summary>
        /// Nome da mãe do paciente
        /// </summary>
        public string NomeMae { get; set; }

        /// <summary>
        /// Email do paciente
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Data de nascimento do paciente
        /// </summary>
        public DateTime? DataNascimento { get; set; }


        /// <summary>
        /// Data de nascimento formatada
        /// </summary>
        public string DataNascimentoFormatada
        {
            get
            {
                if (DataNascimento.HasValue)
                    return DataNascimento.Value.ToString("dd/MM/yyyy", new CultureInfo("en-US"));
                else
                    return string.Empty;
            }
        }

        /// <summary>
        /// Principal telefone do paciente
        /// </summary>
        public string TelefoneCelular { get; set; }

        /// <summary>
        /// Telefone fixo de referencia do paciente
        /// </summary>
        public string TelefoneFixo { get; set; }

        /// <summary>
        /// Telefone do trabalho do paciente
        /// </summary>
        public string TelefoneTrabalho { get; set; }

        /// <summary>
        /// Endereco do paciente
        /// </summary>
        public EnderecoReadViewModel Endereco { get; set; }

        /// <summary>
        /// Sexo do paciente
        /// </summary>
        public string Sexo { get; set; }

        /// <summary>
        /// Numero da carteira do plano de saude 
        /// </summary>
        public string NumeroCarteira { get; set; }

        /// <summary>
        /// Nome do plano de saúde 
        /// </summary>
        public string Plano { get; set; }

        /// <summary>
        /// Acomodação do paciente pelo plano
        /// </summary>
        public string Acomodacao { get; set; }

        /// <summary>
        /// Validade do plano de saúde
        /// </summary>
        public DateTime? Validade { get; set; }


        /// <summary>
        /// Cpf do paciente
        /// </summary>
        public string CPF { get; set; }

        /// <summary>
        /// Rg do paciente
        /// </summary>
        public string RG { get; set; }

        /// <summary>
        /// Número do cartao nacional de saúde do paciente
        /// </summary>
        public string CNS { get; set; }

        /// <summary>
        /// Etinia do paciente
        /// </summary>
        public string Etinia { get; set; }

        /// <summary>
        /// Estado civil do paciente
        /// </summary>
        public string EstadoCivil { get; set; }

        /// <summary>
        /// Escolaridade do paciente
        /// </summary>
        public string Escolaridade { get; set; }

        /// <summary>
        /// Profissão do paciente
        /// </summary>
        public string Profissao { get; set; }

        /// <summary>
        /// Idade Por extenso do paciente
        /// </summary>
        public string IdadePorExtenso { get; set; }
        /// <summary>
        /// Estado de nascimento do paciente
        /// </summary>
        public CidadeReadModel Naturalidade { get; set; }

        /// <summary>
        /// País de origem do paciente
        /// </summary>
        public string Nacionalidade { get; set; }

        /// <summary>
        /// Código de Paciente
        /// </summary>
        public string CodigoPaciente { get; set; }

        /// <summary>
        /// Raça
        /// </summary>
        public string Raca { get; set; }

        /// <summary>
        /// Calcula a idade do paciente
        /// </summary>
        public string IdadeEmAnos
        {
            get
            {
                return (DataNascimento != null ? (DateTime.Today.Year - DataNascimento.Value.Year).ToString() : "");
            }
        }


        public UnidadeReadModel Unidade { get; set; }

    }
}

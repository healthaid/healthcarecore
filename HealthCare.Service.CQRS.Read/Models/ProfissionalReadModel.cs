﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class ProfissionalReadModel
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public string CPF { get; set; }
        public string CNS { get; set; }
        public string Sexo { get; set; }
        public string NomePai { get; set; }
        public string NomeMae { get; set; }
        public string UserName { get; set; }
        public DateTime? Nascimento { get; set; }
        public string NascimentoFormatado
        {
            get
            {
                if (Nascimento.HasValue)
                    return Nascimento.Value.ToString("dd/MM/yyyy");
                else
                    return string.Empty;
            }
        }
        public string CBO { get; set; }
        public string Naturalidade { get; set; }
        public string Nacionalidade { get; set; }
        public string Email { get; set; }
        public string CRM { get; set; }
        public UnidadeReadModel Unidade { get; set; }
    }
}

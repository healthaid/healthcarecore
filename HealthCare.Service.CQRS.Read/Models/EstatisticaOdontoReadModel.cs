﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class EstatisticaOdontoReadModel
    {
        public string DataAtendimento { get; set; }
        public string Quantidade { get; set; }
        public int QuantidadeTotal { get; set; }
        public string Procedimento { get; set; }
        public IEnumerable<EstatisticaOdontoReadModel> EstatisticaAtendimento { get; set; }
    }
}

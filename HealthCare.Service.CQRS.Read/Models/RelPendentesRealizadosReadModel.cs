﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class RelPendentesRealizadosReadModel
    {
        public string StatusBoletim { get; set; }
        public AtendimentoReadModel Atendimento { get; set; }
        public IList<RelPendentesRealizadosReadModel> Atendimentos { get; set; }
        public string Totais { get; set; }
        public string MotivoSaida { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class ListaSPAMunicipioBairroFaixaEtariaReadModel
    {
        public string Municipio { get; set; }
        public string Bairro { get; set; }
        public int Total { get; set; }
        public int MenorQue1Ano { get; set; }
        public int UmaQuatro { get; set; }
        public int CincoaNove { get; set; }
        public int DezaCatorze { get; set; }
        public int QuinzeaDezenove { get; set; }
        public int Vinte { get; set; }
        public int Trinta { get; set; }
        public int Quarenta { get; set; }
        public int Cinquenta { get; set; }
        public int Sessenta { get; set; }
        public int Setenta { get; set; }
        public int MaiorQue80 { get; set; }
        public IEnumerable<AtendimentoReadModel> Atendimentos { get; set; }
    }
}

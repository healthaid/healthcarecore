﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    /// <summary>
    /// Modelo para a classe de role
    /// </summary>
    public class RoleReadModel
    {
        /// <summary>
        /// Identificador do registro
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Nome da role
        /// </summary>
        public string Name { get; set; }

        
    }
}

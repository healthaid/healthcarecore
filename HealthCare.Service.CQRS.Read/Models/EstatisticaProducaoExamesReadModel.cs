﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class EstatisticaProducaoExamesReadModel
    {
        public String CodigoExame { get; set; }
        public String DescricaoExame { get; set; }
        public Decimal TotalExamesSolicitados { get; set; }
        public Decimal TotalLaudoRealizados { get; set; }
        public Decimal TotalLaudoEmitidos { get; set; }
        public Decimal PercentExamesDiaSolicitacao { get; set; }


    }
}

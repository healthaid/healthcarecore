﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class CensoDiariaReadModel
    {
        public string CodigoPaciente { get; set; }
        public string NomePaciente { get; set; }
        public string Idade { get; set; }
        public string Leito { get; set; }
        public string Enfermaria { get; set; }
        public string Entrada { get; set; }
        public string Dias { get; set; }
        public string Clinica { get; set; }
    }
}


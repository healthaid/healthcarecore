﻿
using System;
namespace HealthCare.Infra.CQRS.Read.Models
{
    public class ExameReadModel
    {
        public Guid Id { get; set; }
        public string Descricao { get; set; }
        public string Sexo { get; set; }
        public string Procedimento { get; set; }
        public string Classificacao { get; set; }

        public ExameReadModel ExamePai { get; set; }
        public TipoExameReadModel TipoExame { get; set; }
    }
}

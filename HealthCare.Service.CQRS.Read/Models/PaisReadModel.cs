﻿using System;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class PaisReadModel
    {
        public Guid Id { get; set; }
        public string Descricao { get; set; }
        public string CodigoSUS { get; set; }
    }
}

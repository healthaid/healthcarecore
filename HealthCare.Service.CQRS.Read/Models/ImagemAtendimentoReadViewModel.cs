﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class ImagemAtendimentoReadViewModel
    {
        public Guid Id { get; set; }
        public string Observacao { get; set; }
        public List<ImagemReadViewModel> Imagens { get; set; }
    }
}

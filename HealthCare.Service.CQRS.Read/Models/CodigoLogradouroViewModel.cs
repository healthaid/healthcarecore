﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HealthCare.Service.CQRS.Read.Models
{
    public class CodigoLogradouroViewModel
    {
        public Guid Id { get; set; }
        public string Codigo { get; set; }
        public string Descricao { get; set; }
    }
}

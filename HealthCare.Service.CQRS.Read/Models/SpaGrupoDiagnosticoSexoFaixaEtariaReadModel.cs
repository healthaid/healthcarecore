﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class SpaGrupoDiagnosticoSexoFaixaEtariaReadModel
    {
        public String CodigoCID { get; set; }
        public String DescricaoCID { get; set; }
        public String Sexo { get; set; }
        public int Idade { get; set; }
        public bool EhValido { get; set; }

        public int Total { get; set; }
        public int Menor1ano { get; set; }
        public int De1a4 { get; set; }
        public int De5a9 { get; set; }
        public int De10a14 { get; set; }
        public int De15a19 { get; set; }
        public int De20a29 { get; set; }
        public int De30a39 { get; set; }
        public int De40a49 { get; set; }
        public int De50a59 { get; set; }
        public int De60a69 { get; set; }
        public int De70a79 { get; set; }
        public int Maior80 { get; set; }

        public int TotalGrupo { get; set; }
        public int TotalMenor1ano { get; set; }
        public int TotalDe1a4 { get; set; }
        public int TotalDe5a9 { get; set; }
        public int TotalDe10a14 { get; set; }
        public int TotalDe15a19 { get; set; }
        public int TotalDe20a29 { get; set; }
        public int TotalDe30a39 { get; set; }
        public int TotalDe40a49 { get; set; }
        public int TotalDe50a59 { get; set; }
        public int TotalDe60a69 { get; set; }
        public int TotalDe70a79 { get; set; }
        public int TotalMaior80 { get; set; }


        public IEnumerable<SpaGrupoDiagnosticoSexoFaixaEtariaReadModel> ListaRegistros { get; set; }
        public IEnumerable<ErroSpaGrupoDiagnosticoSexoFaixaEtaria> ListaErros { get; set; }
    }
}

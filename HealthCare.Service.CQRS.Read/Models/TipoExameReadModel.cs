﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class TipoExameReadModel
    {
        public Guid Id { get; set; }

        public string Descricao { get; set; }

        public TipoExameReadModel TipoExamePai { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class CheckoutReadViewModel
    {
        /// <summary>
        /// Identificador do registro
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Primeiro diagnostico
        /// </summary>
        public string DiagnosticoUm { get; set; }

        /// <summary>
        /// Segundo diagnostico
        /// </summary>
        public string DiagnosticoDois { get; set; }


        /// <summary>
        /// Procedimento que foi executado no atendimento
        /// </summary>
        public ProcedimentoReadModel Procedimento { get; set; }

        /// <summary>
        /// Observação
        /// </summary>
        public string Observacao { get; set; }

        /// <summary>
        /// Timestamp do registro
        /// </summary>
        public byte[] RowVersion { get; set; }

        public MotivoCheckoutModel MotivoCheckout { get; set; }

    }
}

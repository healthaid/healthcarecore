﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class FilaAcolhimentoReadModel
    {
        public Guid Id { get; set; }
        public string NomePaciente { get; set; }
        public string Sexo { get; set; }
        public DateTime? DataNascimento { get; set; }
        public string Idade
        {
            get
            {
                string idade = string.Empty;
                if (DataNascimento.HasValue)
                {
                    // Calculate the age.
                    var idadeCalculada = DateTime.Today.Year - DataNascimento.Value.Year;

                    if (DataNascimento > DateTime.Today.AddYears(-idadeCalculada))
                        idadeCalculada--;

                    if (idadeCalculada < 1)
                        idade = "Menos de 1 ano.";
                    else if (idadeCalculada == 1)
                        idade = string.Format("{0} ano.", idadeCalculada.ToString());
                    else
                        idade = string.Format("{0} ano(s).", idadeCalculada.ToString());
                }

                return idade;
            }
        }
        public string HoraAcolhimento { get; set; }
        public Guid PacienteId { get; set; }
        public string Risco { get; set; }
        public string Prioridade { get; set; }
        public bool PrioridadeBool { get; set; }
        public string Servico { get; set; }
        public string Boletim { get; set; }
        public int PrioridadeRisco { get; set; }
        public string TempoAtendimento
        {
            get
            {
                string tempo = "";
                //if (!string.IsNullOrWhiteSpace(HoraAcolhimento))
                //{
                //    TimeSpan calculo = DateTime.Now.Subtract(DateTime.ParseExact(HoraAcolhimento, "dd/MM/yyyy hh:mm:ss", CultureInfo.InvariantCulture));
                //    if (calculo.Days > 0)
                //        tempo = string.Format("{0}d:", calculo.Days.ToString());
                //    tempo = tempo + string.Format("{0}h:", calculo.Hours.ToString());
                //    tempo = tempo + string.Format("{0}m", calculo.Minutes.ToString());
                //}
                return tempo;
            }
        }
    }
}

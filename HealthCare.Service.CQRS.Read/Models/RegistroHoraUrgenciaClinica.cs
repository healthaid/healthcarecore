﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class RegistroHoraUrgenciaClinica
    {
        public String DescricaoClinica { get; set; }
        public DateTime Data { get; set; }
        public int Hora_00 { get; set; }
        public int Hora_01 { get; set; }
        public int Hora_02 { get; set; }
        public int Hora_03 { get; set; }
        public int Hora_04 { get; set; }
        public int Hora_05 { get; set; }
        public int Hora_06 { get; set; }
        public int Hora_07 { get; set; }
        public int Hora_08 { get; set; }
        public int Hora_09 { get; set; }
        public int Hora_10 { get; set; }
        public int Hora_11 { get; set; }
        public int Hora_12 { get; set; }
        public int Hora_13 { get; set; }
        public int Hora_14 { get; set; }
        public int Hora_15 { get; set; }
        public int Hora_16 { get; set; }
        public int Hora_17 { get; set; }
        public int Hora_18 { get; set; }
        public int Hora_19 { get; set; }
        public int Hora_20 { get; set; }
        public int Hora_21 { get; set; }
        public int Hora_22 { get; set; }
        public int Hora_23 { get; set; }

        public int totalHora00 { get; set; }
        public int totalHora01 { get; set; }
        public int totalHora02 { get; set; }
        public int totalHora03 { get; set; }
        public int totalHora04 { get; set; }
        public int totalHora05 { get; set; }
        public int totalHora06 { get; set; }
        public int totalHora07 { get; set; }
        public int totalHora08 { get; set; }
        public int totalHora09 { get; set; }
        public int totalHora10 { get; set; }
        public int totalHora11 { get; set; }
        public int totalHora12 { get; set; }
        public int totalHora13 { get; set; }
        public int totalHora14 { get; set; }
        public int totalHora15 { get; set; }
        public int totalHora16 { get; set; }
        public int totalHora17 { get; set; }
        public int totalHora18 { get; set; }
        public int totalHora19 { get; set; }
        public int totalHora20 { get; set; }
        public int totalHora21 { get; set; }
        public int totalHora22 { get; set; }
        public int totalHora23 { get; set; }

        public int Total_07_1859 { get; set; }
        public int Total_19_0659 { get; set; }


        public int TotalClinicaDia { get; set; }
        public int QuantidadeTotalDia { get; set; }

        public IEnumerable<RegistroHoraUrgenciaClinica> ListaRegistros { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class AtendimentoReadModel
    {
        public Guid Id { get; set; }
        public AcolhimentoReadModel Acolhimento { get; set; }
        public string InicioAtendimento { get; set; }
        public string FimAtendimento { get; set; }
        public string Observacao { get; set; }
        public string DiagnosticoUm { get; set; }
        public string DiagnosticoDois { get; set; }
        public string ExameFisico { get; set; }
        public DateTime Inicio { get; set; }
        public CheckoutReadViewModel Checkout { get; set; }
        public List<HipoteseDiagnosticaReadViewModel> HipoteseDiagnostica { get; set; }
        public List<AtestadoReadViewModel> Atestado { get; set; }
        public CIDReadViewModel PrimeiroDiagnostico { get; set; }
        public CIDReadViewModel SegundoDiagnostico { get; set; }
        public string TempoAtendimento
        {
            get
            {
                string tempo = "";
                if (!string.IsNullOrWhiteSpace(InicioAtendimento))
                {
                    TimeSpan calculo = DateTime.Now.Subtract(Convert.ToDateTime(InicioAtendimento));
                    if (calculo.Days > 0)
                        tempo = string.Format("{0}d:", calculo.Days.ToString());
                    tempo = tempo + string.Format("{0}h:", calculo.Hours.ToString());
                    tempo = tempo + string.Format("{0}m", calculo.Minutes.ToString());
                }
                return tempo;
            }
        }
        public string HoraAtendimento
        {
            get
            {
                string tempo = "";
                if (!string.IsNullOrWhiteSpace(InicioAtendimento))
                {
                    DateTime dataAtendimento = Convert.ToDateTime(InicioAtendimento);
                    tempo = string.Format("{0}h:{1}m", dataAtendimento.Hour.ToString(), dataAtendimento.Minute.ToString());
                }
                return tempo;
            }
        }
        public string InicioAtendimentoExibicao
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(InicioAtendimento))
                    return Convert.ToDateTime(InicioAtendimento).ToString("dd/MM/yyyy HH:mm:ss", new CultureInfo("en-US"));
                else
                    return null;
            }
        }
        public string CompetenciaAtual
        {
            get
            {
                if (DateTime.Now.Month.ToString().Count() < 2)
                {
                    if (DateTime.Now.Month == 1)
                        return (DateTime.Now.Year - 1).ToString() + "12";
                    else
                        return DateTime.Now.Year.ToString() + "0" + (DateTime.Now.Month - 1).ToString();
                }
                else
                    return DateTime.Now.Year.ToString() + (DateTime.Now.Month - 1).ToString();
            }
        }
        public string FimAtendimentoExibicao
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(FimAtendimento))
                    return Convert.ToDateTime(FimAtendimento).ToString("dd/MM/yyyy HH:mm:ss", new CultureInfo("en-US"));
                else
                    return null;
            }
        }

        public ProfissionalReadModel Profissional { get; set; }
    }
}

﻿using System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class PrescricaoMedicaReadModel
    {
        public Guid Id { get; set; }
        public string Dose { get; set; }
        public string Intervalo { get; set; }
        public string Duracao { get; set; }
        public string Unidade { get; set; }
        public string ViaAdministracao { get; set; }
        public string Medicamento { get; set; }
        public string ObservacaoMedico { get; set; }
        public string NomePaciente { get; set; }
        public string HoraSolitiacao { get; set; }
        public string codigoBoletim { get; set; }
        public string idade { get; set; }
        public string Cpf { get; set; }
        public string Profissional { get; set; }
        public string Crm { get; set; }
        public string Cbo { get; set; }
        public string Nascimento { get; set; }
        public string Executor { get; set; }

        public List<PrescricaoMedicaReadModel> ItensPrescricao { get; set; }

        public PrescricaoMedicaReadModel()
        {
            ItensPrescricao = new List<PrescricaoMedicaReadModel>();
        }
    }
}

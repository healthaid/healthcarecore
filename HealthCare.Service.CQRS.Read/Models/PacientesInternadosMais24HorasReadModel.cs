﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class PacientesInternadosMais24HorasReadModel
    {
        public String NomePaciente { get; set; }
        public String Leito { get; set; }
        public String Enfermaria { get; set; }
        public String DataInternacao { get; set; }
        public String HoraInternacao { get; set; }
        public String Medico { get; set; }
        public String TempoDecorrido { get; set; }
    }
}


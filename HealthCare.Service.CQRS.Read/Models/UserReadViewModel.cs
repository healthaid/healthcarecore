﻿using HealthCare.Infra.Common.Result;
using HealthCare.Infra.Common.ResultModels.List;
using HealthCare.Service.CQRS.Read.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    /// <summary>
    /// Modelo para a classe Usuario
    /// </summary>
    public class UserReadViewModel
    {
        /// <summary>
        /// Identificador do usuário
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Nome do usuário
        /// </summary>
        public string Nome { get; set; }

        /// <summary>
        /// E-mail do usuário
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Login do usuário
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Verifica se é o primeiro login do usuário
        /// </summary>
        public bool PrimeiroLogin { get; set; }

        /// <summary>
        /// Profissional associado com o usuarios
        /// </summary>
        public ProfissionalReadModel Profissional { get; set; }

        /// <summary>
        /// Se o usuário é anonimo ou não 
        /// </summary>
        public bool Anonymous { get; set; }

        /// <summary>
        /// Serviços do profissional
        /// </summary>
        public IEnumerable<ServicoReadModel> Servicos { get; set; }

        /// <summary>
        /// Lista de roles do usuário
        /// </summary>
        public List<string> Roles { get; set; }
        public IEnumerable<RoleReadModel> RolesReadModel { get; set; }
        public string CBO { get; set; }

        /// <summary>
        /// Documento/Matrícula do usuário
        /// </summary>
        public string Documento{ get; set; }

        /// <summary>
        /// Tipo de Documento ou Matrícula
        /// </summary>
        public string TipoDocumento { get; set; }

        /// <summary>
        /// CRM caso seja médico
        /// </summary>
        public string CRM { get; set; }

        /// <summary>
        /// Lista de delegações do usuário
        /// </summary>
        public IEnumerable<DelegacaoProfissionalViewModel> Delegacoes { get; set; }
    }
}

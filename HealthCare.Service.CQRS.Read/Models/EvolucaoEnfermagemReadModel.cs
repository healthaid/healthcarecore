﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class EvolucaoEnfermagemReadModel
    {
        public Guid EvolucaoEnfermagemId { get; set; }
        public MovimentacaoLeitoReadModel MovimentacaoLeito { get; set; }
        public ProfissionalReadModel Profissional { get; set; }
        //public string NomeProfissional { get; set; }
        public string NumAtendimento { get; set; }
        public string Evolucao { get; set; }
        public string ObservacaoSinaisVitais { get; set; }
        public string Peso { get; set; }
        public string Pressao { get; set; }
        public string Pulso { get; set; }
        public string FrequenciaRespitaroria { get; set; }
        public string Temperatura { get; set; }
        public DateTime HoraEvolucao { get; set; }
        public List<LiquidosAdministradosEnfermagemReadModel> LiquidosAdministrados { get; set; }
        public List<LiquidosEliminadosEnfermagemReadModel> LiquidosEliminados { get; set; }
        public List<ProcedimentosEnfermagemReadModel> ProcedimentosEnfermagem { get; set; }
        public string Data
        {
            get
            {
                return HoraEvolucao.ToString("dd/MM/yyyy");
            }
        }
    }
}

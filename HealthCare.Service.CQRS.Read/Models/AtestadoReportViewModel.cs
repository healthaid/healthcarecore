﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class AtestadoReportReadModel
    {
        public string unidade { get; set; }
        public string Nome { get; set; }
        public string CPF { get; set; }
        public string DataNascimento { get; set; }
        public string CodigoBoletim { get; set; }
        public string RG { get; set; }
        public string Descricao { get; set; }
        public string CID { get; set; }
        public string Dias { get; set; }
        public string AtestadoId { get; set; }
        public string DataAtendimento { get; set; }
        public string DataFimAtendimento { get; set; }
    }
}

﻿using System;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class UnidadeMedicamentoReadModel
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
    }
}

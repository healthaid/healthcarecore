﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class UserRoleViewModel
    {
        public UserReadViewModel User{ get; set; }

        public RoleReadModel Role { get; set; }
    }
}

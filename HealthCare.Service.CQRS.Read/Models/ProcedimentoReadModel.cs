﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class ProcedimentoReadModel
    {
        public Guid Id { get; set; }
        public string CO_PROCEDIMENTO { get; set; }
        public string NO_PROCEDIMENTO { get; set; }
        public string TP_COMPLEXIDADE { get; set; }
        public string TP_SEXO { get; set; }
        public int QT_MAXIMA_EXECUCAO { get; set; }
        public int QT_DIAS_PERMANENCIA { get; set; }
        public int QT_PONTOS { get; set; }
        public int VL_IDADE_MINIMA { get; set; }
        public int VL_IDADE_MAXIMA { get; set; }
        public decimal VL_SH { get; set; }
        public decimal VL_SA { get; set; }
        public decimal VL_SP { get; set; }
        public string CO_FINANCIAMENTO { get; set; }
        public string CO_RUBRICA { get; set; }
        public string DT_COMPETENCIA { get; set; }
        public string CO_ORIGEM { get; set; }

        public string DescricaoProcedimento
        {
            get
            {
                return string.Format("{0} - {1}", CO_PROCEDIMENTO, NO_PROCEDIMENTO);
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class ServicoReadModel
    {
        public Guid Id { get; set; }
        //[Required(AllowEmptyStrings = false, ErrorMessage = "Informe um nome!")]
        public string Descricao { get; set; }
        public bool IsDeleted { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class DiagnosticosMaisAtendidosReadModel
    {
        public string Codigo { get; set; }
        public string Cid { get; set; }
        public int Quantidade { get; set; }
    }
}

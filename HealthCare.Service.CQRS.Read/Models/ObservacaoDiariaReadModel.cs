﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class ObservacaoDiariaReadModel
    {
        public string Data { get; set; }
        public string Clinica { get; set; }
        public string Paciente { get; set; }
        public string Idade { get; set; }
        public string Sexo { get; set; }
        public string HoraInternacao { get; set; }
        public string Local { get; set; }
        public string EnfermariaLeito { get; set; }
        public string EstadoClinico { get; set; }
        public string Diagnostico { get; set; }
        public string TempoDePermanencia { get; set; }
        public List<ObservacaoDiariaReadModel> Internacoes { get; set; }
        public string TotalDaClinica { get; set; }
        public string TotalDoDia { get; set; }
        public string TotalGeral { get; set; }
    }
}

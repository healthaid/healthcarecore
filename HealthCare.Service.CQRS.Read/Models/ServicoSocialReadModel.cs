﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class ServicoSocialReadModel 
    {

        public Guid Id { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public string HoraInicio { get; set; }
        public string InicioAtendimento { get; set; }
        public DateTime? Nascimento { get; set; }
        public string Idade { get; set; }
        public string Sexo { get; set; }
        public string Checkout { get; set; }
        public string Observacao { get; set; }
        public string FimAtendimento { get; set; }
        public Guid AcolhimentoId { get; set; }
        public string Historico { get; set; }
        public string Intervencao { get; set; }
        public Guid PacienteId { get; set; }
        public string CodBoletim { get; set; }
        public string Procedimento{ get; set; }
        public string DiagnosticoUm { get; set; }
        public string DiagnosticoDois { get; set; }
        public PacienteReadModel Paciente { get; set; }

    }
}

﻿using HealthCare.Service.CQRS.Read.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    /// <summary>
    /// Modelo para a classe 
    /// </summary>
    public class EnderecoReadViewModel
    {
        /// <summary>
        /// Identificador do registro
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Cep
        /// </summary>
        public string CEP { get; set; }

        /// <summary>
        /// Nome da rua
        /// </summary>
        public string Logradouro { get; set; }

        /// <summary>
        /// Numero da casa
        /// </summary>
        public string Numero { get; set; }

        /// <summary>
        /// Complemento do endereço
        /// </summary>
        public string Complemento { get; set; }

        /// <summary>
        /// Bairro
        /// </summary>
        public string Bairro { get; set; }

        /// <summary>
        /// Municipio
        /// </summary>
        public string Cidade { get; set; }

        /// <summary>
        /// Estado
        /// </summary>
        public string UF { get; set; }

        /// <summary>
        /// Codigo IBGE do municipio
        /// </summary>
        public string codigo_ibge { get; set; }


        public string CodigoIbgeCidade { get; set; }
        /// <summary>
        /// Cidade
        /// </summary>
        public CidadeInfo Cidade_Info { get; set; }

        public string CodigoLogradouro { get; set; }

        /// <summary>
        /// Concatena informações do endereço 
        /// </summary>
        public string EnderecoPorExtenso
        {
            get
            {
                return string.Format("{0}, {1} {2}  {3}", Logradouro, Numero, Complemento, Cidade);
            }
        }
    }

    public class EnderecoViewModel : EnderecoReadViewModel
    {
        public string Estado { get; set; }
    }

    public class CidadeInfo
    {
        public string Codigo_Ibge { get; set; }
    }
}

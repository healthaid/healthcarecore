﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class BoletimReadModel
    {
        public Guid IdAtendimento { get; set; }
        public string dataAtendimento { get; set; }

        public string anamnese { get; set; }
        public string hipoteseDiagnostica { get; set; }
        public string queixaPrincipal { get; set; }
        public string exameFisico { get; set; }
        public string primeiroDiagnostico { get; set; }

        public AcolhimentoReadModel AcolhimentoReadModel { get; set; }
        public ClassificacaoDeRiscoReadModel ClassificacaoDeRiscoReadModel { get; set; }
    }
}

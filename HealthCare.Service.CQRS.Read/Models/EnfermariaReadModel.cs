﻿
using System;
namespace HealthCare.Infra.CQRS.Read.Models
{
    public class EnfermariaReadModel
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public bool Ativo { get; set; }
        public string Local { get; set; }
    }
}

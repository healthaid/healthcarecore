﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class Ciap2ReadViewModel
    {
        public string Id { get; set; }
        public string Descricao { get; set; }
        public string Codigo { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class ProducaoDiariaPorExameReadModel
    {
        public String Descricao { get; set; }
        public String AnoMes { get; set; }

        public int Dia01 { get; set; }
        public int Dia02 { get; set; }
        public int Dia03 { get; set; }
        public int Dia04 { get; set; }
        public int Dia05 { get; set; }
        public int Dia06 { get; set; }
        public int Dia07 { get; set; }
        public int Dia08 { get; set; }
        public int Dia09 { get; set; }
        public int Dia10 { get; set; }
        public int Dia11 { get; set; }
        public int Dia12 { get; set; }
        public int Dia13 { get; set; }
        public int Dia14 { get; set; }
        public int Dia15 { get; set; }
        public int Dia16 { get; set; }
        public int Dia17 { get; set; }
        public int Dia18 { get; set; }
        public int Dia19 { get; set; }
        public int Dia20 { get; set; }
        public int Dia21 { get; set; }
        public int Dia22 { get; set; }
        public int Dia23 { get; set; }
        public int Dia24 { get; set; }
        public int Dia25 { get; set; }
        public int Dia26 { get; set; }
        public int Dia27 { get; set; }
        public int Dia28 { get; set; }
        public int Dia29 { get; set; }
        public int Dia30 { get; set; }
        public int Dia31 { get; set; }
        public int TotalMes { get; set; }

        public int TotalDia01 { get; set; }
        public int TotalDia02 { get; set; }
        public int TotalDia03 { get; set; }
        public int TotalDia04 { get; set; }
        public int TotalDia05 { get; set; }
        public int TotalDia06 { get; set; }
        public int TotalDia07 { get; set; }
        public int TotalDia08 { get; set; }
        public int TotalDia09 { get; set; }
        public int TotalDia10 { get; set; }
        public int TotalDia11 { get; set; }
        public int TotalDia12 { get; set; }
        public int TotalDia13 { get; set; }
        public int TotalDia14 { get; set; }
        public int TotalDia15 { get; set; }
        public int TotalDia16 { get; set; }
        public int TotalDia17 { get; set; }
        public int TotalDia18 { get; set; }
        public int TotalDia19 { get; set; }
        public int TotalDia20 { get; set; }
        public int TotalDia21 { get; set; }
        public int TotalDia22 { get; set; }
        public int TotalDia23 { get; set; }
        public int TotalDia24 { get; set; }
        public int TotalDia25 { get; set; }
        public int TotalDia26 { get; set; }
        public int TotalDia27 { get; set; }
        public int TotalDia28 { get; set; }
        public int TotalDia29 { get; set; }
        public int TotalDia30 { get; set; }
        public int TotalDia31 { get; set; }
        public int TotalGeralMes { get; set; }




        public IEnumerable<ProducaoDiariaPorExameReadModel> ListaRegistros { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
namespace HealthCare.Infra.CQRS.Read.Models
{
    public class HipoteseDiagnosticaReadViewModel
    {
        public Guid Id { get; set; }
        public List<CIDReadViewModel> Cids { get; set; }
        public string Observacao { get; set; }
        public Guid AtendimentoId { get; set; }
    }
}

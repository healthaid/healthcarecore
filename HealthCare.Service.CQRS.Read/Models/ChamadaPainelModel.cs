﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class ChamadaPainelModel
    {
        public Guid IdItemPainel { get; set; }

        public string NomePaciente { get; set; }

        public string Service { get; set; }

        public string LocalAtendimento { get; set; }

        public string Fala { get; set; }
    }
}

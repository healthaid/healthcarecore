﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class LeitoReadModel
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public bool Ativo { get; set; }
        public EnfermariaReadModel Enfermaria { get; set; }
        public int TipoLeito { get; set; }
        public string TipoLeitoPorExtenso
        {
            get
            {
                switch (TipoLeito)
                {
                    case 1:
                        return "Sala Amarela";
                    case 2:
                        return "Sala Vermelha";
                    case 3:
                        return "Outros";
                    default:
                        return string.Empty;
                }
            }
        }
        public string EnfermariaLeito
        {
            get
            {
                if (this.Enfermaria != null)
                    return string.Format("{0}/{1}", this.Enfermaria.Nome, this.Nome);
                else
                    return string.Empty;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HealthCare.Service.CQRS.Read.Models
{
    public class AtendimentoProcedimentoReadModel
    {
        public Guid AtendimentoId { get; set; }
        public string CodigoProcedimento { get; set; }
        public string NomeProcedimento { get; set; }
        public string VLSA { get; set; }
        public int? QtDiasPermanencia { get; set; }
        public string VLSP { get; set; }
    }
}

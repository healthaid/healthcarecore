﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class RiscoReadModel
    {
        public Guid Id { get; set; }
        public string Descricao { get; set; }
        public int Prioridade { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HealthCare.Service.CQRS.Read.Models
{
    public class ListaBoletinsAtendidosReadModel
    {
        public Guid Id { get; set; }
        public string InicioAtendimento { get; set; }
        public string Profissional { get; set; }
        public string Diagnostico { get; set; }
        public string MotivoAlta { get; set; }
        public string FimAtendimento { get; set; }
        public Guid AcolhimentoId { get; set; }
    }
}

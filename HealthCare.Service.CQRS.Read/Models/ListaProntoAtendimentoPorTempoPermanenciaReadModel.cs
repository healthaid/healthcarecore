﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HealthCare.Infra.CQRS.Read.Models
{
    public class ListaProntoAtendimentoPorTempoPermanenciaReadModel
    {
        public String DescricaoDasHoras { get; set; }
        public int Total { get; set; }
        public int Altas { get; set; }
        public int Obitos { get; set; }
        public int Internacoes { get; set; }
        public int Outros { get; set; }
    }
}

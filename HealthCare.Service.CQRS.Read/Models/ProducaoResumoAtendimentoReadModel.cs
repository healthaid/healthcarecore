﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class ProducaoResumoAtendimentoReadModel
    {
        public string Grupo { get; set; }
        public string Item { get; set; }
        public int Quantidade { get; set; }
        public List<ProducaoResumoAtendimentoReadModel> ListaProducaoResumo { get; set; }
    }
}

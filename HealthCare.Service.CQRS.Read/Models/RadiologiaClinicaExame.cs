﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class RadiologiaClinicaExameReadModel
    {
        public string Classificacao { get; set; }
        public List<RadiologiaClinicaExameReadModel> examesAtendimentos { get; set; }
        public string Descricao { get; set; }
        public string Total { get; set; }
        public string TotalRelacaoClinica { get; set; }
        public string TotalRelacaoUnidade { get; set; }
        public string TotalGeral { get; set; }
    }
}

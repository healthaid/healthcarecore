﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class ExtratoContaPacienteReadModel
    {   
        public AtendimentoReadModel Atendimento { get; set; }
        public List<ExtratoPacienteProcedimentos> Procedimentos { get; set; }
        public ProcedimentoReadModel ProcedimentoRealizado { get; set; }
        public ProcedimentoReadModel ProcedimentoSolicitado { get; set; }
        public string DataInternacao { get; set; }
        public string Clinica { get; set; }
        public string EnfermariaLeito { get; set; }
        public string TotalServicosHospitalares { get; set; }
        public string TotalServicosProfissionais { get; set; }
        public string TotalAIH { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class UnidadeReadModel
    {
        public Guid Id { get; set; }
        public string NomeUnidade { get; set; }
        public string NomeFantasia { get; set; }
        public string CodigoUnidade { get; set; }
        public string Sigla { get; set; }
        public string CNES { get; set; }
        public string Cnpj { get; set; }
        public DateTime DataAcreditacao { get; set; }
        public string DataAcreditacaoFormatada
        {
            get
            {
                if (DataAcreditacao != null)
                    return DataAcreditacao.ToString("dd/MM/yyyy");
                else
                    return string.Empty;

            }
        }
        public string Situacao { get; set; }
        public Guid EnderecoId { get; set; }
        public EnderecoReadViewModel Endereco { get; set; }
    }
}
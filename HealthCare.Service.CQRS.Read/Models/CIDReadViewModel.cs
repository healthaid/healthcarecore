﻿using System;
using System.Collections.Generic;
namespace HealthCare.Infra.CQRS.Read.Models
{
    public class CIDReadViewModel
    {
        public Guid Id { get; set; }
        public string Descricao { get; set; }
        public string Codigo { get; set; }
        public List<ProcedimentoReadModel> Procedimentos { get; set; }
        public string DescCodigo
        {
            get
            {
                return string.Format("{0} - {1}", Codigo, Descricao);
            }
        }

    }
}


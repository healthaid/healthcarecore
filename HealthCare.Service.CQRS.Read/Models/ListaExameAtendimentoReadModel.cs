﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class ListaExameAtendimentoReadModel
    {
        public Guid Id { get; set; }
        public string Paciente { get; set; }
        public string DataSolicitacao { get; set; }
        public string Profissional { get; set; }
        public string Exame { get; set; }
        public string Procedimento { get; set; }
        public string ObservacaoMedico { get; set; }
        public string ObservacaoTecnica { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class PostoEnfermagemReadModel
    {
        public string Procedimento { get; set; }
        public int TotalProcedimento { get; set; }
        public int TotalPaciente { get; set; }
        public string Data { get; set; }
        public int TotalMesProcedimento { get; set; }
        public int TotalMesPaciente { get; set; }
        public List<PrescricaoReadModel> Prescricao { get; set; }
        public List<PostoEnfermagemReadModel> Lista { get; set; }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class MovimentacaoLeitoReadModel
    {
        public Guid Id { get; set; }
        public DateTime DataMovimentacao { get; set; }
        public InternacaoReadModel Internacao { get; set; }
        public LeitoReadModel LeitoAtual { get; set; }
        public ServicoReadModel Clinica { get; set; }

        public string Paciente { get; set; }
        public string CodigoBoletim { get; set; }
        public string Leito { get; set; }
        public string LocalEnfermaria { get; set; }
        public string Enfermaria { get; set; }
        public string DataInternacao { get; set; }
        public string DataUltimaMovimentacao { get; set; }
        public string ClinicaDescricao { get; set; }
        public string Idade { get; set; }
        public string PacienteId { get; set; }
        public Guid InternacaoId { get; set; }
        public Guid ClinicaDestinoId { get; set; }
    }
}

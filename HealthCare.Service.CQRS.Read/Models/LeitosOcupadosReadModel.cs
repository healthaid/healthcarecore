﻿using System;
using System.Collections.Generic;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class LeitosOcupadosReadModel
    {
        public Guid Id { get; set; }
        public IEnumerable<MovimentacaoLeitoReadModel> Lista { get; set; }
        public string Paciente { get; set; }
        public string Leito { get; set; }
        public string Enfermaria { get; set; }
        public string PacienteId { get; set; }
    }
}

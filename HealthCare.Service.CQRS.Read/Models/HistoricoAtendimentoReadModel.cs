﻿using HealthCare.Infra.Common.Result;
using HealthCare.Infra.Common.ResultModels.List;
using HealthCare.Infra.CQRS.Read.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace HealthCare.Service.CQRS.Read.Models
{
    public class HistoricoAtendimentoReadModel
    {
        public AtendimentoReadModel Atendimento { get; set; }
        public TableViewModel Receitas { get; set; }
        public TableViewModel Prescricoes { get; set; }
        public TableViewModel Exames { get; set; }
        public TableViewModel Procedimentos { get; set; }
        public TableViewModel Evolucoes { get; set; }
    }
}

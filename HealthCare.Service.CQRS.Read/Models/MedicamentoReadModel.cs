﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class MedicamentoReadModel
    {
        public string Nome { get; set; }
        public string Generico { get; set; }
        public string FormaFarmaceutica { get; set; }
        public string DDD { get; set; }
        public string CodigoATC { get; set; }
    }
}

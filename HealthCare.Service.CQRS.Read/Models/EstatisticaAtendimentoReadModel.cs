﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class EstatisticaAtendimentoReadModel
    {
        public string Data { get; set; }
        public string Profissional { get; set; }
        public string Total { get; set; }
        public string TotalProfissional { get; set; }
        public IEnumerable<EstatisticaAtendimentoReadModel> AtendimentosPorProfissional { get; set; }
        public IEnumerable<EstatisticaAtendimentoReadModel> TotalPorProfissional { get; set; }
        public IEnumerable<AtendimentoReadModel> Atendimentos { get; set; }
    }
}

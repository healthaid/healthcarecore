﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class PrescricaoReadModel
    {
        public Guid Id { get; set; }
        public string Dose { get; set; }
        public string Intervalo { get; set; }
        public string Duracao { get; set; }
        public string Unidade { get; set; }
        public string ViaAdministracao { get; set; }
        public string Medicamento { get; set; }
        public Guid AtendimentoId { get; set; }
        public AtendimentoReadModel Atendimento { get; set; }
        public string ObservacaoMedico { get; set; }
        public string StatusEnfermagem { get; set; }
        public string StatusPorEscrito
        {
            get
            {
                return (StatusEnfermagem == "true" ? "Medicado" : "Aguardando Medicação");
            }
        }
        public string ObservacaoEnfermagem { get; set; }
        public DateTime? RetornoEnfermagem { get; set; }
        public string DataMedicacao
        {
            get
            {
                if (RetornoEnfermagem != null)
                    return RetornoEnfermagem.Value.ToString("dd/MM/yyyy");
                else
                    return string.Empty;
            }
        }
        public Guid ProfissionalId { get; set; }

    }
}

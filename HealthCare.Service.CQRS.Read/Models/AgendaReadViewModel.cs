﻿
using System;
using System.Collections.Generic;
namespace HealthCare.Infra.CQRS.Read.Models
{
    public class AgendaReadViewModel
    {
        public string Nome;
        public DateTime HoraInicio;
        public DateTime HoraFim;
        public Guid ProfissionalId;
        public DateTime HoraInicioIntervalo;
        public DateTime HoraFimIntervalo;
        public List<DayOfWeek> DiasDaSemana;
    }
}

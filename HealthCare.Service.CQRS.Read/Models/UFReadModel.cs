﻿using System;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class UFReadModel
    {
        public Guid Id { get; set; }
        public string Descricao { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class FilaAtendimentoViewModel
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public string HoraInicio { get; set; }
        public string InicioAtendimento { get; set; }
        public DateTime? Nascimento { get; set; }
        public string Idade { get; set; }
        public string Sexo { get; set; }
        public string Checkout { get; set; }
        public string Observacao { get; set; }
        public string FimAtendimento { get; set; }
        public string Boletim { get; set; }
        public Guid AcolhimentoId { get; set; }

    }
}

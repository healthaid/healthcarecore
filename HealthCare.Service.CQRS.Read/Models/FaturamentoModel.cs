﻿using System;
using System.Collections.Generic;

namespace HealthCare.Service.CQRS.Read.Models
{
    public class FaturamentoModel
    {
        public Guid AtendimentoId { get; set; }

        public List<Guid> ProcedimentoId { get; set; }

        public List<Guid> CidId { get; set; }

        public string CnpjUnidade { get; set; }

        public string CnsProfissional { get; set; }

        public string NomePaciente { get; set; }

        public int PacienteIdade { get; set; }

        public string CnesUnidade { get; set; }

        public string PacienteRaca { get; set; }

        public string PacienteEtinia { get; set; }

        public string Nacionalidade { get; set; }

        public string CodigoCboProfissional { get; set; }

        public string SexoPaciente { get; set; }

        public string CodigoIbgeMunicipioPaciente { get; set; }

        public string CepPaciente { get; set; }

        public string LogradouroPaciente { get; set; }

        public string ComplementoEnderecoPaciente { get; set; }

        public int? NumeroEnderecoPaciente { get; set; }

        public string BairoEnderecoPaciente { get; set; }

        public string TelefonePaciente { get; set; }

        public string EmailPaciente { get; set; }

        public string Competencia { get; set; }

        public string OrgaoDestino { get; set; }

        public string Dominio { get; set; }

        public string NomeOrgaoDestino { get; set; }

        public string IndicadorOrgaoDestino { get; set; }

        public string NomeOrgaoResponsavel { get; set; }

        public string SiglaOrgaoResponsavel { get; set; }

        public string VersaoSistema { get; set; }

        public string CaracterDoAtendimento { get; set; }

        public string CNSPaciente { get; set; }

        public string CodigoAreaEquipe { get; set; }

        public string CodigoClassificacao { get; set; }

        public string CodigoSequenciaEquipe { get; set; }

        public string CodigoServico { get; set; }

        public string DataAtendimento { get; set; }

        public string IdentificacaoNacional { get; set; }

        public string LinhaProducao { get; set; }

        public string NascimentoPaciente { get; set; }

        public string CodigoProcedimento { get; set; }

        public string Cid10 { get; set; }

        public string CodigoLogradouroPaciente { get; set; }

        public string DiasInternacao { get; set; }

        public string NomeProfissional { get; set; }

        public Guid AcolhimentoId{ get; set; }

    }
}

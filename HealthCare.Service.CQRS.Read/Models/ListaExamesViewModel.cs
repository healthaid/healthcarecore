﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class ListaExamesViewModel
    {
        public Guid Id { get; set; }
        public string Descricao { get; set; }
        public string Profissional { get; set; }
        public string Observacao { get; set; }
        public string Executado { get; set; }
        public string ObservacaoTecnica { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class AtestadoReadViewModel
    {
        public Guid Id { get; set; }
        public string Descricao { get; set; }
        public Guid AtendimentoId { get; set; }
        public string Cid { get; set; }
        public string Nome { get; set; }
        public string CodigoBoletim { get; set; }
        public string RG { get; set; }
        public string CPF { get; set; }
        public string DataAtendimento { get; set; }
        public string unidade { get; set; }
        public string logradouroUnidade { get; set; }
        public string bairroUnidade { get; set; }
        public string bairroCidade { get; set; }

    }
}

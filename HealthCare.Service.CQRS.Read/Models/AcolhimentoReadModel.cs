﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class AcolhimentoReadModel
    {
        public Guid Id { get; set; }
        public string CodigoBoletim { get; set; }
        public string DataHora { get; set; }
        public PacienteReadModel Paciente { get; set; }
        public List<ClassificacaoDeRiscoReadModel> Classificacao { get; set; }
        public ProfissionalReadModel Profissional { get; set; }
        public ServicoReadModel Servico { get; set; }
        public string DataFormatada
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(this.DataHora))
                    return Convert.ToDateTime(this.DataHora).ToString("dd/MM/yyyy hh:mm:ss", new CultureInfo("en-US"));
                else
                    return null;

            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCare.Infra.CQRS.Read.Models
{
    public class RelatorioMapaDeLeitosReadModel
    {
        public string Clinica { get; set; }
        public string EnfermariaLeito { get; set; }
        public string Prontuario { get; set; }
        public string Paciente { get; set; }
        public string Sexo { get; set; }
        public string Idade { get; set; }
        public string ClinicaDeOrigem { get; set; }
        public string DiagnosticoProcedimento { get; set; }
        public string TempoProcedimento { get; set; }
        public string TP { get; set; }
        public int LeitosExistentes { get; set; }
        public int LeitosOcupados { get; set; }
        public int LeitosForaDaClinica { get; set; }
        public List<RelatorioMapaDeLeitosReadModel> Lista { get; set; }
    }
}

﻿using HealthCare.Infra.CQRS.Messaging;
using HealthCare.Infra.CQRS.Messaging.Handling;
using System;
using System.Collections.Generic;

namespace HealthCare.Infra.CQRS.Process
{
    public interface IProcessManager
    {
        /// <summary>
        /// Gets the process manager identifier.
        /// </summary>
        Guid Id { get; }

        /// <summary>
        /// Gets a value indicating whether the process manager workflow is completed and the state can be archived.
        /// </summary>
        bool Completed { get; }

        /// <summary>
        /// Gets a collection of commands that need to be sent when the state of the process manager is persisted.
        /// </summary>
        IEnumerable<Envelope<ICommand>> Commands { get; }
    }
}

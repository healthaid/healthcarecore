﻿using HealthCare.Infra.CQRS.Messaging;
using System.Collections.Generic;

namespace HealthCare.Infra.CQRS.MessageLog
{
    public interface IEventLogReader
    {
        IEnumerable<IEvent> Query(QueryCriteria criteria);
    }
}

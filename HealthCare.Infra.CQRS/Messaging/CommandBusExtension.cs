﻿using HealthCare.Infra.CQRS.Messaging.Handling;
using System.Collections.Generic;
using System.Linq;

namespace HealthCare.Infra.CQRS.Messaging
{
    public static class CommandBusExtension
    {

        public static void Send(this ICommandBus bus, ICommand command)
        {
            bus.Send(new Envelope<ICommand>(command));
        }

        public static void Send(this ICommandBus bus, IEnumerable<ICommand> commands)
        {
            bus.Send(commands.Select(x => new Envelope<ICommand>(x)));
        }

    }
}

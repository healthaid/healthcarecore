﻿using System;

namespace HealthCare.Infra.CQRS.Messaging
{
    public interface IEvent
    {
        /// <summary>
        /// Gets the identifier of the source originating the event.
        /// </summary>
        Guid SourceId { get; }
    }
}

﻿using System;

namespace HealthCare.Infra.CQRS.Messaging.Handling
{
    public interface ICommand
    {
        /// <summary>
        /// Gets the command identifier.
        /// </summary>
        Guid Id { get; }

        /// <summary>
        /// Usuário que disparou o comando
        /// </summary>
        Guid CommandUsuarioId { get; set; }
    }
}

﻿namespace HealthCare.Infra.CQRS.Messaging
{
    public interface IManagerSessionProvider
    {
        string SessionId { get; }
    }
}

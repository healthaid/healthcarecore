﻿namespace HealthCare.Infra.CQRS.Messaging
{
    public class ValidationMessage
    {
        public string Message { get; set; }

        public ValidationMessage( string message)
        {
            Message = message;
        }
    }
}

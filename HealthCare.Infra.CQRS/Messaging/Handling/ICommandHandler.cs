﻿using System;
using HealthCare.Infra.CQRS.Messaging.Handling;

namespace HealthCare.Infra.CQRS.Messaging
{
    /// <summary>
    /// Marker interface that makes it easier to discover handlers via reflection.
    /// </summary>
    public interface ICommandHandler : IDisposable { }

    public interface ICommandHandler<T> : ICommandHandler
        where T : ICommand
    {
        /// <summary>
        /// Executa o handle do comando
        /// </summary>
        /// <param name="command">comando</param>
        void Handle(T command);

        /// <summary>
        /// Executa o handle do comando 
        /// </summary>
        /// <param name="command">Comando</param>
        /// <returns>Retorna qualquer objeto necessário</returns>
        object HandleWithReturn(T command);
    }
}

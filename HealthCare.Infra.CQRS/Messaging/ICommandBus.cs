﻿using HealthCare.Infra.CQRS.Messaging.Handling;
using System.Collections.Generic;

namespace HealthCare.Infra.CQRS.Messaging
{
    /// <summary>
    /// Interface que mantes os contratos para o envio de comandos para o bus
    /// </summary>
    public interface ICommandBus
    {
        /// <summary>
        /// Método para enviar apenas um comando
        /// </summary>
        /// <param name="command">Comando a ser enviado</param>
        void Send(Envelope<ICommand> command);

        /// <summary>
        /// Método para enviar uma lista de comandos
        /// </summary>
        /// <param name="commands">Lista de comandos a serem enviados</param>
        void Send(IEnumerable<Envelope<ICommand>> commands);

        /// <summary>
        /// Método para enviar os comandos esperando um retorno
        /// </summary>
        /// <param name="command">Comando a ser enviado</param>
        /// <returns></returns>
        object SendWithReturn(Envelope<ICommand> command);
    }
}

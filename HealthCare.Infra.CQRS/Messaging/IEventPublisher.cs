﻿using System.Collections.Generic;

namespace HealthCare.Infra.CQRS.Messaging
{
    public interface IEventPublisher
    {
        IEnumerable<IEvent> Events { get; }
    }
}

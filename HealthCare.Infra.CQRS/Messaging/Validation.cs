﻿using System.Collections.Generic;
using System.Text;

namespace HealthCare.Infra.CQRS.Messaging
{
    public class Validation
    {
        public bool IsValid { get { return ListMessages.Count == 0; } }

        public List<ValidationMessage> ListMessages{ get; set; }

        public string Messages
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                foreach(ValidationMessage v in ListMessages)
                    sb.AppendLine(v.Message);

                return sb.ToString();
            }
        }

        public Validation()
        {
            ListMessages = new List<ValidationMessage>();
        }

        public void AddValidationMessage(ValidationMessage message)
        {
            if (ListMessages == null)
                ListMessages = new List<ValidationMessage>();
            ListMessages.Add(message);
        }
    }
}

﻿namespace HealthCare.Infra.CQRS
{
    public interface IProcessor
    {
        void Start();
        void Stop();
    }
}

﻿using System;

namespace HealthCare.Infra.CQRS.Database
{
    public interface IDataContext<T> : IDisposable
      where T : IAggregateRoot
    {
        T Find(Guid id);

        void Save(T aggregate);
    }
}

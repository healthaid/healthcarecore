﻿using System;

namespace HealthCare.Infra.CQRS.Database
{
    public interface IAggregateRoot
    {
        Guid Id { get; }
    }
}

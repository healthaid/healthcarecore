﻿using HealthCare.Infra.CQRS.Messaging;

namespace HealthCare.Infra.CQRS.EventSourcing
{
    public interface IVersionedEvent : IEvent
    {
        /// <summary>
        /// Gets the version or order of the event in the stream.
        /// </summary>
        int Version { get; }
    }
}
